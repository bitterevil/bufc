﻿Imports System.Threading

Namespace My

    ' The following events are available for MyApplication:
    ' 
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.
    Partial Friend Class MyApplication

        Private Declare Auto Function AllocConsole Lib "kernel32.dll" () As Boolean
        Private oDatabase As cDatabaseLocal
        Private oImportAuto As cImportAuto
        Private oExportAuto As cExportAuto
        Private oPurgeAuto As cPurgeAuto

        Private Sub MyApplication_Startup(sender As Object, e As ApplicationServices.StartupEventArgs) Handles Me.Startup
            Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-GB")
            C_CALxMain()
        End Sub

        Public Sub C_CALxMain()
            Dim aArgs() As String = Environment.GetCommandLineArgs()
            If aArgs.Length > 1 Then
                Dim bImport As Boolean = False
                Dim bExport As Boolean = False
                Dim bPurge As Boolean = False

                Select Case aArgs(1).ToLower
                    Case "InDaily"
                        cCNVB.nVB_ImportPdt = 1
                        cCNVB.nVB_ImportAjp = 1
                        bImport = True
                    Case "In30Min"
                        cCNVB.nVB_ImportTnf = 1
                        bImport = True
                    Case "-export"
                        bExport = True
                    Case "-purge"
                        bPurge = True
                End Select

                If aArgs.Length = 3 AndAlso bExport Then  'PAN การ Export เฉพาะ  2016-06-09 
                    Select Case aArgs(2).ToLower
                        Case cCNVB.tVB_ZSDINT005
                            cCNVB.tVB_Export = cCNVB.tVB_ZSDINT005
                        Case cCNVB.tVB_ZSDINT006
                            cCNVB.tVB_Export = cCNVB.tVB_ZSDINT006
                        Case cCNVB.tVB_ZGLINT001
                            cCNVB.tVB_Export = cCNVB.tVB_ZGLINT001
                        Case cCNVB.tVB_ZGLINT002
                            cCNVB.tVB_Export = cCNVB.tVB_ZGLINT002
                    End Select
                End If


                If bImport = True Or bExport = True Or bPurge = True Then
                    cCNVB.bVB_Auto = True 'Set Status Auto Global

                    'Not use Console
                    'AllocConsole()

                    'Set Culture
                    Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-GB")

                    cLog.C_CALxWriteLogAuto("Start...")

                    'Default Eng
                    cCNVB.nVB_CutLng = 2
                    cCNSP.SP_LNGxSetDefAftChg()

                    Try
                        AdaConfig.cConfig.C_GETxConfigXml()
                        AdaConfig.cConfig.C_GETxConfigXmlUrs()

                        'Check Connection
                        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = True Then

                            oDatabase = New cDatabaseLocal
                            If cApp.C_CALaCheckTableLog.Count > 0 Then
                                cLog.C_CALxWriteLogAuto(cCNSP.SP_STRtToken(cCNMS.tMS_CN110, ";", cCNVB.nVB_CutConst, True))
                                End
                            End If

                            'Get Branch *CH 08-09-2017
                            cCNSP.SP_GETxVariable()

                            'Not check expired date '*CH 10-11-2014
                            'If cApp.C_CALxCheckExpire < 1 Then
                            '    End
                            'End If

                            'เช็คโปรแกรมรันมากกว่า 1 เครื่อง
                            'http://msdn.microsoft.com/en-us/library/ms130822%28v=sql.105%29.aspx
                            'syspro.dbid,program_name,name,hostname
                            'Dim tHostName As String = ""
                            'Dim tSql As String = "SELECT hostname FROM sys.sysprocesses syspro INNER JOIN sys.sysdatabases sysdb ON syspro.dbid=sysdb.dbid WHERE name='" & AdaConfig.cConfig.oConnSource.tCatalog &
                            '        "' AND program_name='" & My.Application.Info.ProductName.ToString & "' AND hostname<>HOST_NAME() ORDER BY hostname"

                            'Dim oDTHost = oDatabase.C_CALoExecuteReader(tSql)
                            'If oDTHost IsNot Nothing Then
                            '    If oDTHost.Rows.Count > 0 Then
                            '        tHostName = oDTHost.Rows(0)("hostname")
                            '    End If
                            'End If

                            'If tHostName.Trim.Length > 0 Then
                            '    cLog.C_CALxWriteLogAuto(String.Format(cCNSP.SP_STRtToken(cCNMS.tMS_CN107, ";", cCNVB.nVB_CutConst, True), tHostName.Trim))
                            '    Thread.Sleep(1000 * 10)
                            '    End
                            'End If

                        Else
                            Throw New Exception(cCNSP.SP_STRtToken(cCNMS.tMS_CN004, ";", cCNVB.nVB_CutConst, True))
                        End If

                        'Create Table Log
                        oDatabase.C_CALnExecuteNonQuery(cApp.tSQLCmdCheckTableTemp)

                        'perform auto
                        Select Case True
                            Case bImport
                                C_CALxImportAuto()
                            Case bExport
                                C_CALxExportAuto()
                            Case bPurge
                                C_CALxPurgeAuto() '*CH 20-01-2015
                        End Select
                        End
                    Catch ex As Exception
                        cLog.C_CALxWriteLogAuto(ex.Message)
                        Thread.Sleep(1000 * 10)
                        End
                    End Try

                    'Console.ReadKey()
                End If
            End If

        End Sub

        Private Sub C_CALxImportAuto()
            'Import Auto
            Dim dStartTime As Date = Now
            'Console.WriteLine("Import Start...")
            cLog.C_CALxWriteLogAuto("Import Auto Start [Start : " & Format(dStartTime, "HH:mm:ss") & "]...")
            oImportAuto = New cImportAuto
            oImportAuto.C_CALxProcess()

            Dim dEndTime As Date = Now
            Dim dDiffTime As TimeSpan = dEndTime - dStartTime
            'Console.WriteLine("Import Stop...")
            cLog.C_CALxWriteLogAuto("Import Auto Stop [End : " & Format(dEndTime, "HH:mm:ss") & "],[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]...")
            Thread.Sleep(1000 * 10)
            End
        End Sub

        Private Sub C_CALxExportAuto()
            'Export Auto
            Dim dStartTime As Date = Now
            'Console.WriteLine("Export Start...")
            cLog.C_CALxWriteLogAuto("Export Auto Start [Start : " & Format(dStartTime, "HH:mm:ss") & "]...")
            oExportAuto = New cExportAuto
            oExportAuto.C_CALxProcess()

            Dim dEndTime As Date = Now
            Dim dDiffTime As TimeSpan = dEndTime - dStartTime
            'Console.WriteLine("Export Stop...")
            cLog.C_CALxWriteLogAuto("Export Auto Stop [End : " & Format(dEndTime, "HH:mm:ss") & "],[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]...")
            Thread.Sleep(1000 * 10)
            End
        End Sub

        ''' <summary>
        ''' Purge log file
        ''' </summary>
        ''' <remarks>*CH 20-01-2015</remarks>
        Private Sub C_CALxPurgeAuto()
            Dim dStartTime As Date = Now
            cLog.C_CALxWriteLogAuto("Purge Auto Start [Start : " & Format(dStartTime, "HH:mm:ss") & "]...")
            oPurgeAuto = New cPurgeAuto
            oPurgeAuto.C_CALxProcess()

            Dim dEndTime As Date = Now
            Dim dDiffTime As TimeSpan = dEndTime - dStartTime
            cLog.C_CALxWriteLogAuto("Purge Auto Stop [End : " & Format(dEndTime, "HH:mm:ss") & "],[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]...")
            Thread.Sleep(1000 * 10)
        End Sub

    End Class

End Namespace

