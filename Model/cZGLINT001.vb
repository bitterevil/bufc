﻿Public Class cZGLINT001
    Property BUKRS As String = "1000"
    Property BLART As String = "IF"
    Property BLDAT As String = "" 'FDDateIns
    Property BUDAT As String = "" 'FDDateIns
    Property WAERS As String = "THB"
    Property XBLNR As String = "" 'FTLtxRemark
    Property BRNCH As String = "0000" 'Change 1000 to 0000 '*CH 15-09-2017
    Property BKTXT As String = "" 'V7 or D7 Show VAT, VX Show NON '*CH 12-10-2017
    Property BUZEI As String = "001"
    Property BSCHL As String = "40"
    Property HKONT As String = "" 'FTRsnOthName
    Property WRBTR As String = "" 'FCLtxAmt
    Property MWSKZ As String = "" 'FTRsnRemark
    Property WERKS As String = "" 'Branch Mapping 'Change Mapping Plant '*CH 15-09-2017
    Property SGTXT As String = "" 'Rsn.FTRsnName + CONVERT(VARCHAR(18), Ltx.FCLtxAmt) + Rsn.FTRsnRemark
    '*CH 12-10-2017
    Property FTLogCode As String = ""
    Property FNLtxType As String = ""
    Property FNLtxSeq As String = ""
    Property FCLtxAmt As String = ""
    Property FTRsnCode As String = ""
    Property FTLtxRemark As String = ""
    Property FTExpFileName As String = ""
    Property FTExpRunning As String = ""
End Class
