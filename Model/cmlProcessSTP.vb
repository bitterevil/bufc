﻿Imports System.ComponentModel.DataAnnotations
Public Class cmlProcessSTP
    Private bSTP_AlwUpdQtyNow As Boolean = True
    Private bSTP_AlwReMonthEnd As Boolean = False
    Private bSTP_AlwCost As Boolean = False
    Private bSTP_AlwInWha As Boolean = False
    Private bSTP_AlwStkCrd As Boolean = False
    Private tSTP_DocNo As String = ""
    Private tC_FTTimeUpd As String = ""
    Private tC_FTWhoUpd As String = ""
    Private tC_FTBchCode As String = ""
    Private tC_FTUsrCode As String = ""
    Private dC_FDDateUpd As Date = Now
    Private nC_PdtSet As Long
    Private nC_DecAmtForSav As Long
    Dim nSTP_CNPdtSet As Integer
    Dim tSTP_UserName As String
    ''' <summary>อนุญาตปรับสต้อกยอดคงเหลือขายได้</summary>
    Public Property STP_AlwUpdQtyNow() As Boolean
    Public Property STP_AlwReMonthEnd() As Boolean
    Public Property STP_DocNo() As String
    Public Property STP_AlwCost() As Boolean
    Public Property STP_AlwInWha() As Boolean
    Public Property STP_AlwStkCrd() As Boolean
    Public Property FDDateUpd() As Date
    Public Property FTTimeUpd() As String
    Public Property FTWhoUpd() As String

    Public Property FTUsrCode() As String
    Public Property FTBchCode() As String
    ''' <summary>การตัดสต๊อกสินค้าชุด</summary>
    Public Property PdtSet() As String
    ''' <summary>จำนวนหลักทศนิยม</summary>
    Public Property DecAmtForSav() As String
    Public Property STP_CNPdtSet() As Integer

End Class
