﻿
Public Class cTCNTPdtTnfHD
    Public Property FTBchCode() As String
    Public Property FTPthDocNo() As String
    Public Property FTPthDocType() As String
    Public Property FDPthDocDate() As String
    Public Property FTPthDocDate() As String = ""
    Public Property FTPthDocTime() As String
    Public Property FTPthVATInOrEx() As String
    Public Property FTDptCode() As String
    Public Property FTDepName() As String
    Public Property FTUsrCode() As String
    Public Property FTUsrName() As String
    Public Property FTSplCode() As String
    Public Property FTBchCodeTnf() As String
    Public Property FTPthWhFrm() As String
    Public Property FTPthWhTo() As String
    Public Property FTPthType() As String
    Public Property FTPthOther() As String
    Public Property FTCstCode() As String
    Public Property FTAreCode() As String
    Public Property FTSpnCode() As String
    Public Property FTPrdCode() As String
    Public Property FTWahCode() As String
    Public Property FTPthApvCode() As String
    Public Property FTShpCode() As String
    Public Property FNCspCode() As Integer
    Public Property FNPthCrTerm() As Integer
    Public Property FDPthDueDate() As String
    Public Property FTPthDueDate() As String = ""
    Public Property FTPthRefExt() As String
    Public Property FDPthRefExtDate() As String
    Public Property FTPthRefExtDate() As String = "" '*receive Json from View '*CH 07-10-2014
    Public Property FTPthRefInt() As String
    Public Property FDPthRefIntDate() As String
    Public Property FTPthRefIntDate() As String = "" ''*receive Json from View '*CH 07-10-2014
    Public Property FTPthRefAE() As String
    Public Property FDPthTnfDate() As String
    Public Property FTPthTnfDate() As String = "" ''*receive Json from View '*CH 07-10-2014
    Public Property FDPthBillDue() As String
    Public Property FTPthBillDue() As String = "" ''*receive Json from View '*CH 07-10-2014
    Public Property FTPthCtrName() As String
    Public Property FNPthDocPrint As Integer
    Public Property FCPthVATRate() As Double
    Public Property FCPthTotal() As Double
    Public Property FCPthNonVat() As Double
    Public Property FCPthB4DisChg() As Double
    Public Property FTPthDisChgTxt() As String
    Public Property FCPthDis() As Double
    Public Property FCPthChg() As Double
    Public Property FCPthAftDisChg() As Double
    Public Property FCPthVat() As Double
    Public Property FCPthVatable() As Double
    Public Property FCPthGrand() As Double
    Public Property FCPthRnd() As Double
    Public Property FCPthWpTax() As Double
    Public Property FCPthReceive() As Double
    Public Property FCPthChn() As Double
    Public Property FTPthGndText() As String
    Public Property FCPthLeft() As Double
    Public Property FCPthMnyCsh() As Double
    Public Property FCPthMnyChq() As Double
    Public Property FCPthMnyCrd() As Double
    Public Property FCPthMnyCtf() As Double
    Public Property FCPthMnyCpn() As Double
    Public Property FCPthMnyCls() As Double
    Public Property FCPthMnyCxx() As Double
    Public Property FCPthGndCN() As Double
    Public Property FCPthGndDN() As Double
    Public Property FCPthGndAE() As Double
    Public Property FCPthGndTH() As Double
    Public Property FTPthStaPaid() As String
    Public Property FTPthStaRefund() As String
    Public Property FTPthStaType() As String
    Public Property FTPthStaDoc() As String
    Public Property FTPthStaPrcDoc() As String
    Public Property FTPthStaPrcSpn() As String
    Public Property FTPthStaPrcCst() As String
    Public Property FTPthStaPrcGL() As String
    Public Property FTPthStaPost() As String
    Public Property FTPjcCode() As String
    Public Property FTAloCode() As String
    Public Property FTCcyCode() As String
    Public Property FCPthCcyExg() As Double
    Public Property FTPosCode() As String
    Public Property FTPthPosCN() As String
    Public Property FTLogCode() As String
    Public Property FTPthRmk() As String
    Public Property FNPthSign() As Integer
    Public Property FTPthCshOrCrd() As String
    Public Property FCPthPaid() As Double
    Public Property FTPthDstPaid() As String
    Public Property FTXbhDocNo() As String
    Public Property FTXphDocNo() As String
    Public Property FNPthStaDocAct() As Long
    Public Property FNPthStaRef() As Long
    Public Property FTPthDptCRef() As String
    Public Property FTPthDptNRef() As String
    Public Property FTPthStaVatSend() As String
    Public Property FTPthBchFrm() As String
    Public Property FTPthBchTo() As String
    Public Property FTRteCode() As String
    Public Property FCPthRteFac() As Double
    Public Property FTTrnCode() As String
    Public Property FDDateUpd() As String
    Public Property FTTimeUpd() As String
    Public Property FTWhoUpd() As String
    Public Property FDDateIns() As String
    Public Property FTTimeIns() As String
    Public Property FTWhoIns() As String
    Public Property FTBchName() As String = ""

End Class
