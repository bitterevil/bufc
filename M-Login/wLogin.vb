Option Explicit On

Public Class wLogin

#Region "Events"

#Region "Button"

    Private Sub ocmCancel_Click(sender As System.Object, e As System.EventArgs) Handles ocmCancel.Click
        Me.Close()
    End Sub

#End Region

#Region "Form"

    Private Sub wLogin_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        cCNSP.SP_FrmSetCapControl(Me)
    End Sub

    Private Sub ocmOk_Click(sender As System.Object, e As System.EventArgs) Handles ocmOk.Click
        If Me.otbCode.Text.Trim = "" Or otbPwd.Text.Trim = "" Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN010, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.otbCode.Focus()
            Me.DialogResult = Windows.Forms.DialogResult.None
            Exit Sub
        End If

        '��Ǩ�ͺ user
        If ((Me.otbCode.Text.Trim = AdaConfig.cConfig.oApplication.tUser) And (Me.otbPwd.Text.Trim = AdaConfig.cConfig.oApplication.tPass)) = True Then

            cLog.C_CALxWriteLog("wLogin > Ok")

            Me.DialogResult = Windows.Forms.DialogResult.OK

            'Get config AAlwCalCpn
            Dim oDatabase As New cDatabaseLocal
            Dim oDbTbl As New DataTable
            oDbTbl = oDatabase.C_CALoExecuteReader("SELECT CASE FTSysUsrValue WHEN '1' THEN '1' ELSE '0' END AS AAlwCalCpn FROM TSysConfig WHERE FTSysCode = 'AAlwCalCpn'")
            If oDbTbl IsNot Nothing AndAlso oDbTbl.Rows.Count > 0 Then cCNVB.tVB_CfgAlwCalCpn = oDbTbl.Rows(0)("AAlwCalCpn")

            'Set UserCode to Variable
            cCNVB.tVB_UserCode = Me.otbCode.Text.Trim
        Else
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN011, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.otbCode.Focus()
            Me.DialogResult = Windows.Forms.DialogResult.None
        End If

    End Sub

#End Region

#Region "TextBox"

    Private Sub otbCode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbCode.KeyDown
        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{Tab}")
        End If
    End Sub

    Private Sub otbPwd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbPwd.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.ocmOk.PerformClick()
        End If
    End Sub

#End Region

#End Region

End Class