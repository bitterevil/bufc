<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wLogin))
        Me.otbPwd = New System.Windows.Forms.TextBox()
        Me.otbCode = New System.Windows.Forms.TextBox()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.ocmOk = New System.Windows.Forms.Button()
        Me.opbLogin = New System.Windows.Forms.PictureBox()
        Me.olaPwd = New System.Windows.Forms.Label()
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.ogbForm = New System.Windows.Forms.GroupBox()
        Me.olaCode = New System.Windows.Forms.Label()
        CType(Me.opbLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnForm.SuspendLayout()
        Me.ogbForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'otbPwd
        '
        Me.otbPwd.Location = New System.Drawing.Point(96, 55)
        Me.otbPwd.Name = "otbPwd"
        Me.otbPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.otbPwd.Size = New System.Drawing.Size(206, 22)
        Me.otbPwd.TabIndex = 1
        '
        'otbCode
        '
        Me.otbCode.Location = New System.Drawing.Point(96, 21)
        Me.otbCode.Name = "otbCode"
        Me.otbCode.Size = New System.Drawing.Size(206, 22)
        Me.otbCode.TabIndex = 0
        '
        'ocmCancel
        '
        Me.ocmCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmCancel.Location = New System.Drawing.Point(202, 120)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(100, 30)
        Me.ocmCancel.TabIndex = 3
        Me.ocmCancel.Tag = "2;¡��ԡ;Cancel"
        Me.ocmCancel.Text = "Cancel"
        Me.ocmCancel.UseVisualStyleBackColor = True
        '
        'ocmOk
        '
        Me.ocmOk.Location = New System.Drawing.Point(96, 120)
        Me.ocmOk.Name = "ocmOk"
        Me.ocmOk.Size = New System.Drawing.Size(100, 30)
        Me.ocmOk.TabIndex = 2
        Me.ocmOk.Tag = "2;��ŧ;OK"
        Me.ocmOk.Text = "OK"
        Me.ocmOk.UseVisualStyleBackColor = True
        '
        'opbLogin
        '
        Me.opbLogin.Image = CType(resources.GetObject("opbLogin.Image"), System.Drawing.Image)
        Me.opbLogin.Location = New System.Drawing.Point(13, 86)
        Me.opbLogin.Name = "opbLogin"
        Me.opbLogin.Size = New System.Drawing.Size(72, 72)
        Me.opbLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.opbLogin.TabIndex = 4
        Me.opbLogin.TabStop = False
        '
        'olaPwd
        '
        Me.olaPwd.AutoSize = True
        Me.olaPwd.Location = New System.Drawing.Point(10, 58)
        Me.olaPwd.Name = "olaPwd"
        Me.olaPwd.Size = New System.Drawing.Size(68, 16)
        Me.olaPwd.TabIndex = 6
        Me.olaPwd.Tag = "2;���ʼ�ҹ;Password"
        Me.olaPwd.Text = "Password"
        '
        'opnForm
        '
        Me.opnForm.Controls.Add(Me.ogbForm)
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(0, 0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(335, 181)
        Me.opnForm.TabIndex = 8
        '
        'ogbForm
        '
        Me.ogbForm.BackColor = System.Drawing.Color.Transparent
        Me.ogbForm.Controls.Add(Me.olaPwd)
        Me.ogbForm.Controls.Add(Me.olaCode)
        Me.ogbForm.Controls.Add(Me.opbLogin)
        Me.ogbForm.Controls.Add(Me.otbPwd)
        Me.ogbForm.Controls.Add(Me.otbCode)
        Me.ogbForm.Controls.Add(Me.ocmCancel)
        Me.ogbForm.Controls.Add(Me.ocmOk)
        Me.ogbForm.Location = New System.Drawing.Point(12, 3)
        Me.ogbForm.Name = "ogbForm"
        Me.ogbForm.Size = New System.Drawing.Size(313, 168)
        Me.ogbForm.TabIndex = 0
        Me.ogbForm.TabStop = False
        '
        'olaCode
        '
        Me.olaCode.AutoSize = True
        Me.olaCode.Location = New System.Drawing.Point(10, 24)
        Me.olaCode.Name = "olaCode"
        Me.olaCode.Size = New System.Drawing.Size(37, 16)
        Me.olaCode.TabIndex = 5
        Me.olaCode.Tag = "2;�����;User"
        Me.olaCode.Text = "User"
        '
        'wLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmCancel
        Me.ClientSize = New System.Drawing.Size(335, 181)
        Me.Controls.Add(Me.opnForm)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wLogin"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;����к�;Login"
        Me.Text = "Login"
        Me.TopMost = True
        CType(Me.opbLogin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnForm.ResumeLayout(False)
        Me.ogbForm.ResumeLayout(False)
        Me.ogbForm.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents otbPwd As System.Windows.Forms.TextBox
    Friend WithEvents otbCode As System.Windows.Forms.TextBox
    Friend WithEvents ocmCancel As System.Windows.Forms.Button
    Friend WithEvents ocmOk As System.Windows.Forms.Button
    Friend WithEvents opbLogin As System.Windows.Forms.PictureBox
    Friend WithEvents olaPwd As System.Windows.Forms.Label
    Friend WithEvents opnForm As System.Windows.Forms.Panel
    Friend WithEvents ogbForm As System.Windows.Forms.GroupBox
    Friend WithEvents olaCode As System.Windows.Forms.Label
End Class
