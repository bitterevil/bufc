﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wSetting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wSetting))
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Connection"}, 0, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte)))
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Configurations"}, 0, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte)))
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Other", 0)
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("FTP", 0)
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Config Export", 0)
        Me.oimForm = New System.Windows.Forms.ImageList(Me.components)
        Me.opnTop = New System.Windows.Forms.Panel()
        Me.olaTitle = New System.Windows.Forms.Label()
        Me.opnLeft = New System.Windows.Forms.Panel()
        Me.olvMenu = New System.Windows.Forms.ListView()
        Me.opnright = New System.Windows.Forms.Panel()
        Me.opnFormDes = New System.Windows.Forms.Panel()
        Me.olaDesForm = New System.Windows.Forms.Label()
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.opnbottom = New System.Windows.Forms.Panel()
        Me.opnButton = New System.Windows.Forms.Panel()
        Me.ocmSave = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.opnRoot = New System.Windows.Forms.Panel()
        Me.opnTop.SuspendLayout()
        Me.opnFormDes.SuspendLayout()
        Me.opnbottom.SuspendLayout()
        Me.opnButton.SuspendLayout()
        Me.opnRoot.SuspendLayout()
        Me.SuspendLayout()
        '
        'oimForm
        '
        Me.oimForm.ImageStream = CType(resources.GetObject("oimForm.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.oimForm.TransparentColor = System.Drawing.Color.Transparent
        Me.oimForm.Images.SetKeyName(0, "Properties-icon.png")
        '
        'opnTop
        '
        Me.opnTop.BackColor = System.Drawing.Color.Transparent
        Me.opnTop.Controls.Add(Me.olaTitle)
        Me.opnTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.opnTop.Location = New System.Drawing.Point(0, 0)
        Me.opnTop.Name = "opnTop"
        Me.opnTop.Size = New System.Drawing.Size(684, 65)
        Me.opnTop.TabIndex = 1
        '
        'olaTitle
        '
        Me.olaTitle.AutoSize = True
        Me.olaTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaTitle.Location = New System.Drawing.Point(17, 18)
        Me.olaTitle.Name = "olaTitle"
        Me.olaTitle.Size = New System.Drawing.Size(221, 16)
        Me.olaTitle.TabIndex = 0
        Me.olaTitle.Tag = "2;การตั้งค่า SQL Server และที่ตั้งไฟล์;Setting SQL Server and Folder path."
        Me.olaTitle.Text = "Setting SQL Server and Folder path."
        '
        'opnLeft
        '
        Me.opnLeft.BackColor = System.Drawing.Color.Transparent
        Me.opnLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.opnLeft.Location = New System.Drawing.Point(0, 65)
        Me.opnLeft.Name = "opnLeft"
        Me.opnLeft.Size = New System.Drawing.Size(20, 406)
        Me.opnLeft.TabIndex = 2
        '
        'olvMenu
        '
        Me.olvMenu.Dock = System.Windows.Forms.DockStyle.Left
        Me.olvMenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olvMenu.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5})
        Me.olvMenu.Location = New System.Drawing.Point(20, 65)
        Me.olvMenu.Margin = New System.Windows.Forms.Padding(0, 2, 2, 2)
        Me.olvMenu.MultiSelect = False
        Me.olvMenu.Name = "olvMenu"
        Me.olvMenu.Size = New System.Drawing.Size(158, 406)
        Me.olvMenu.SmallImageList = Me.oimForm
        Me.olvMenu.TabIndex = 3
        Me.olvMenu.UseCompatibleStateImageBehavior = False
        Me.olvMenu.View = System.Windows.Forms.View.List
        '
        'opnright
        '
        Me.opnright.BackColor = System.Drawing.Color.Transparent
        Me.opnright.Dock = System.Windows.Forms.DockStyle.Right
        Me.opnright.Location = New System.Drawing.Point(664, 65)
        Me.opnright.Name = "opnright"
        Me.opnright.Size = New System.Drawing.Size(20, 406)
        Me.opnright.TabIndex = 6
        '
        'opnFormDes
        '
        Me.opnFormDes.BackColor = System.Drawing.Color.Transparent
        Me.opnFormDes.Controls.Add(Me.olaDesForm)
        Me.opnFormDes.Dock = System.Windows.Forms.DockStyle.Top
        Me.opnFormDes.Location = New System.Drawing.Point(178, 65)
        Me.opnFormDes.Name = "opnFormDes"
        Me.opnFormDes.Size = New System.Drawing.Size(486, 42)
        Me.opnFormDes.TabIndex = 4
        '
        'olaDesForm
        '
        Me.olaDesForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaDesForm.Location = New System.Drawing.Point(14, 5)
        Me.olaDesForm.Name = "olaDesForm"
        Me.olaDesForm.Size = New System.Drawing.Size(455, 37)
        Me.olaDesForm.TabIndex = 0
        '
        'opnForm
        '
        Me.opnForm.BackColor = System.Drawing.Color.Transparent
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(178, 107)
        Me.opnForm.Margin = New System.Windows.Forms.Padding(0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(486, 364)
        Me.opnForm.TabIndex = 5
        '
        'opnbottom
        '
        Me.opnbottom.BackColor = System.Drawing.Color.Transparent
        Me.opnbottom.Controls.Add(Me.opnButton)
        Me.opnbottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.opnbottom.Location = New System.Drawing.Point(0, 471)
        Me.opnbottom.Name = "opnbottom"
        Me.opnbottom.Size = New System.Drawing.Size(684, 77)
        Me.opnbottom.TabIndex = 7
        '
        'opnButton
        '
        Me.opnButton.Controls.Add(Me.ocmSave)
        Me.opnButton.Controls.Add(Me.ocmClose)
        Me.opnButton.Dock = System.Windows.Forms.DockStyle.Right
        Me.opnButton.Location = New System.Drawing.Point(422, 0)
        Me.opnButton.Name = "opnButton"
        Me.opnButton.Size = New System.Drawing.Size(262, 77)
        Me.opnButton.TabIndex = 1
        '
        'ocmSave
        '
        Me.ocmSave.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.ocmSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmSave.Location = New System.Drawing.Point(36, 23)
        Me.ocmSave.Name = "ocmSave"
        Me.ocmSave.Size = New System.Drawing.Size(100, 30)
        Me.ocmSave.TabIndex = 0
        Me.ocmSave.Tag = "2;บันทึก;Save"
        Me.ocmSave.Text = "Save"
        Me.ocmSave.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.DialogResult = System.Windows.Forms.DialogResult.No
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(142, 23)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose.TabIndex = 1
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'opnRoot
        '
        Me.opnRoot.Controls.Add(Me.opnForm)
        Me.opnRoot.Controls.Add(Me.opnFormDes)
        Me.opnRoot.Controls.Add(Me.opnright)
        Me.opnRoot.Controls.Add(Me.olvMenu)
        Me.opnRoot.Controls.Add(Me.opnLeft)
        Me.opnRoot.Controls.Add(Me.opnTop)
        Me.opnRoot.Controls.Add(Me.opnbottom)
        Me.opnRoot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnRoot.Location = New System.Drawing.Point(0, 0)
        Me.opnRoot.Name = "opnRoot"
        Me.opnRoot.Size = New System.Drawing.Size(684, 548)
        Me.opnRoot.TabIndex = 0
        '
        'wSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmClose
        Me.ClientSize = New System.Drawing.Size(684, 548)
        Me.Controls.Add(Me.opnRoot)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "wSetting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ตั้งค่า;Setting"
        Me.Text = "Setting"
        Me.opnTop.ResumeLayout(False)
        Me.opnTop.PerformLayout()
        Me.opnFormDes.ResumeLayout(False)
        Me.opnbottom.ResumeLayout(False)
        Me.opnButton.ResumeLayout(False)
        Me.opnRoot.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents oimForm As System.Windows.Forms.ImageList
    Friend WithEvents opnTop As System.Windows.Forms.Panel
    Friend WithEvents opnLeft As System.Windows.Forms.Panel
    Friend WithEvents olvMenu As System.Windows.Forms.ListView
    Friend WithEvents opnright As System.Windows.Forms.Panel
    Friend WithEvents opnFormDes As System.Windows.Forms.Panel
    Friend WithEvents opnForm As System.Windows.Forms.Panel
    Friend WithEvents opnbottom As System.Windows.Forms.Panel
    Friend WithEvents ocmClose As System.Windows.Forms.Button
    Friend WithEvents ocmSave As System.Windows.Forms.Button
    Friend WithEvents opnButton As System.Windows.Forms.Panel
    Friend WithEvents olaTitle As System.Windows.Forms.Label
    Friend WithEvents olaDesForm As System.Windows.Forms.Label
    Friend WithEvents opnRoot As System.Windows.Forms.Panel
End Class
