﻿Public Class cUsrFTP
    Property W_DATtHostName As String
        Get
            Return Me.otbHostName.Text
        End Get
        Set(value As String)
            Me.otbHostName.Text = value
        End Set
    End Property

    Property W_DATtUsrName As String
        Get
            Return Me.otbUsrName.Text
        End Get
        Set(value As String)
            Me.otbUsrName.Text = value
        End Set
    End Property

    Property W_DATtPwd As String
        Get
            Return Me.otbPwd.Text
        End Get
        Set(value As String)
            Me.otbPwd.Text = value
        End Set
    End Property

    Property W_DATtPortNum As String
        Get
            Return Me.otbPortNum.Text
        End Get
        Set(value As String)
            Me.otbPortNum.Text = value
        End Set
    End Property

    Property W_DATtInBound As String
        Get
            Return Me.otbInBound.Text
        End Get
        Set(value As String)
            Me.otbInBound.Text = value
        End Set
    End Property

    Property W_DATtOutBound As String
        Get
            Return Me.otbOutBound.Text
        End Get
        Set(value As String)
            Me.otbOutBound.Text = value
        End Set
    End Property

    Property W_DATtReconcile As String
        Get
            Return Me.otbReconcile.Text
        End Get
        Set(value As String)
            Me.otbReconcile.Text = value
        End Set
    End Property

    Property W_DATtCashTnf As String
        Get
            Return Me.otbCshTnf.Text
        End Get
        Set(value As String)
            Me.otbCshTnf.Text = value
        End Set
    End Property

    Property W_DATtExpense As String
        Get
            Return Me.otbExpense.Text
        End Get
        Set(value As String)
            Me.otbExpense.Text = value
        End Set
    End Property

    Property W_DATtFTPType As String
        Get
            Return Me.ocbFTPType.Text
        End Get
        Set(value As String)
            Me.ocbFTPType.Text = value
        End Set
    End Property

    Private Sub ocbFTPType_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ocbFTPType.KeyPress
        e.Handled = True
    End Sub
End Class
