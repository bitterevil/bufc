﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cUsrConnection
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cUsrConnection))
        Me.opnRoot = New System.Windows.Forms.TableLayoutPanel()
        Me.ogbForm = New System.Windows.Forms.GroupBox()
        Me.olaSecond = New System.Windows.Forms.Label()
        Me.olaTimeOut = New System.Windows.Forms.Label()
        Me.ondTimeOut = New System.Windows.Forms.NumericUpDown()
        Me.olaSrv = New System.Windows.Forms.Label()
        Me.olaUsr = New System.Windows.Forms.Label()
        Me.olaPwd = New System.Windows.Forms.Label()
        Me.olaDbName = New System.Windows.Forms.Label()
        Me.otbSrv = New System.Windows.Forms.TextBox()
        Me.otbUsr = New System.Windows.Forms.TextBox()
        Me.otbPwd = New System.Windows.Forms.TextBox()
        Me.ocbDbName = New System.Windows.Forms.ComboBox()
        Me.ocmDbName = New System.Windows.Forms.Button()
        Me.ogbMemDB = New System.Windows.Forms.GroupBox()
        Me.olaMemSecond = New System.Windows.Forms.Label()
        Me.olaMemTimeOut = New System.Windows.Forms.Label()
        Me.ondMemTimeOut = New System.Windows.Forms.NumericUpDown()
        Me.olaMemSrv = New System.Windows.Forms.Label()
        Me.olaMemUsr = New System.Windows.Forms.Label()
        Me.olaMemPwd = New System.Windows.Forms.Label()
        Me.olaMemDbName = New System.Windows.Forms.Label()
        Me.otbMemSrv = New System.Windows.Forms.TextBox()
        Me.otbMemUsr = New System.Windows.Forms.TextBox()
        Me.otbMemPwd = New System.Windows.Forms.TextBox()
        Me.ocbMemDbName = New System.Windows.Forms.ComboBox()
        Me.ocmMemDbName = New System.Windows.Forms.Button()
        Me.opnRoot.SuspendLayout()
        Me.ogbForm.SuspendLayout()
        CType(Me.ondTimeOut, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ogbMemDB.SuspendLayout()
        CType(Me.ondMemTimeOut, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnRoot
        '
        Me.opnRoot.ColumnCount = 2
        Me.opnRoot.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.opnRoot.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.opnRoot.Controls.Add(Me.ogbForm, 1, 0)
        Me.opnRoot.Controls.Add(Me.ogbMemDB, 1, 1)
        Me.opnRoot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnRoot.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.opnRoot.Location = New System.Drawing.Point(0, 0)
        Me.opnRoot.Name = "opnRoot"
        Me.opnRoot.RowCount = 2
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 162.0!))
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92.0!))
        Me.opnRoot.Size = New System.Drawing.Size(486, 327)
        Me.opnRoot.TabIndex = 9
        '
        'ogbForm
        '
        Me.ogbForm.BackColor = System.Drawing.Color.Transparent
        Me.ogbForm.Controls.Add(Me.olaSecond)
        Me.ogbForm.Controls.Add(Me.olaTimeOut)
        Me.ogbForm.Controls.Add(Me.ondTimeOut)
        Me.ogbForm.Controls.Add(Me.olaSrv)
        Me.ogbForm.Controls.Add(Me.olaUsr)
        Me.ogbForm.Controls.Add(Me.olaPwd)
        Me.ogbForm.Controls.Add(Me.olaDbName)
        Me.ogbForm.Controls.Add(Me.otbSrv)
        Me.ogbForm.Controls.Add(Me.otbUsr)
        Me.ogbForm.Controls.Add(Me.otbPwd)
        Me.ogbForm.Controls.Add(Me.ocbDbName)
        Me.ogbForm.Controls.Add(Me.ocmDbName)
        Me.ogbForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogbForm.Location = New System.Drawing.Point(13, 3)
        Me.ogbForm.Name = "ogbForm"
        Me.ogbForm.Size = New System.Drawing.Size(470, 156)
        Me.ogbForm.TabIndex = 1
        Me.ogbForm.TabStop = False
        Me.ogbForm.Tag = "2;ตั้งค่าฐานข้อมูล;Database Setting"
        Me.ogbForm.Text = "Database Setting"
        '
        'olaSecond
        '
        Me.olaSecond.AutoSize = True
        Me.olaSecond.Location = New System.Drawing.Point(236, 130)
        Me.olaSecond.Name = "olaSecond"
        Me.olaSecond.Size = New System.Drawing.Size(55, 16)
        Me.olaSecond.TabIndex = 12
        Me.olaSecond.Tag = "2;วินาที;Second"
        Me.olaSecond.Text = "Second"
        '
        'olaTimeOut
        '
        Me.olaTimeOut.AutoSize = True
        Me.olaTimeOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaTimeOut.Location = New System.Drawing.Point(17, 130)
        Me.olaTimeOut.Name = "olaTimeOut"
        Me.olaTimeOut.Size = New System.Drawing.Size(62, 16)
        Me.olaTimeOut.TabIndex = 11
        Me.olaTimeOut.Tag = "2;หมดเวลา;Time Out"
        Me.olaTimeOut.Text = "Time Out"
        '
        'ondTimeOut
        '
        Me.ondTimeOut.Location = New System.Drawing.Point(146, 128)
        Me.ondTimeOut.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.ondTimeOut.Minimum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.ondTimeOut.Name = "ondTimeOut"
        Me.ondTimeOut.Size = New System.Drawing.Size(84, 22)
        Me.ondTimeOut.TabIndex = 10
        Me.ondTimeOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ondTimeOut.Value = New Decimal(New Integer() {60, 0, 0, 0})
        '
        'olaSrv
        '
        Me.olaSrv.AutoSize = True
        Me.olaSrv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaSrv.Location = New System.Drawing.Point(17, 21)
        Me.olaSrv.Name = "olaSrv"
        Me.olaSrv.Size = New System.Drawing.Size(48, 16)
        Me.olaSrv.TabIndex = 6
        Me.olaSrv.Tag = "2;เซิร์ฟเวอร์;Server"
        Me.olaSrv.Text = "Server"
        '
        'olaUsr
        '
        Me.olaUsr.AutoSize = True
        Me.olaUsr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaUsr.Location = New System.Drawing.Point(17, 47)
        Me.olaUsr.Name = "olaUsr"
        Me.olaUsr.Size = New System.Drawing.Size(37, 16)
        Me.olaUsr.TabIndex = 7
        Me.olaUsr.Tag = "2;ผู้ใช้งาน;User"
        Me.olaUsr.Text = "User"
        '
        'olaPwd
        '
        Me.olaPwd.AutoSize = True
        Me.olaPwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaPwd.Location = New System.Drawing.Point(17, 74)
        Me.olaPwd.Name = "olaPwd"
        Me.olaPwd.Size = New System.Drawing.Size(68, 16)
        Me.olaPwd.TabIndex = 8
        Me.olaPwd.Tag = "2;รหัสผ่าน;Password"
        Me.olaPwd.Text = "Password"
        '
        'olaDbName
        '
        Me.olaDbName.AutoSize = True
        Me.olaDbName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaDbName.Location = New System.Drawing.Point(17, 101)
        Me.olaDbName.Name = "olaDbName"
        Me.olaDbName.Size = New System.Drawing.Size(68, 16)
        Me.olaDbName.TabIndex = 9
        Me.olaDbName.Tag = "2;ฐานข้อมูล;Database"
        Me.olaDbName.Text = "Database"
        '
        'otbSrv
        '
        Me.otbSrv.BackColor = System.Drawing.Color.White
        Me.otbSrv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbSrv.Location = New System.Drawing.Point(146, 18)
        Me.otbSrv.Name = "otbSrv"
        Me.otbSrv.Size = New System.Drawing.Size(306, 22)
        Me.otbSrv.TabIndex = 0
        '
        'otbUsr
        '
        Me.otbUsr.BackColor = System.Drawing.Color.White
        Me.otbUsr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbUsr.Location = New System.Drawing.Point(146, 44)
        Me.otbUsr.Name = "otbUsr"
        Me.otbUsr.Size = New System.Drawing.Size(306, 22)
        Me.otbUsr.TabIndex = 2
        '
        'otbPwd
        '
        Me.otbPwd.BackColor = System.Drawing.Color.White
        Me.otbPwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbPwd.Location = New System.Drawing.Point(146, 71)
        Me.otbPwd.Name = "otbPwd"
        Me.otbPwd.Size = New System.Drawing.Size(306, 22)
        Me.otbPwd.TabIndex = 3
        Me.otbPwd.UseSystemPasswordChar = True
        '
        'ocbDbName
        '
        Me.ocbDbName.BackColor = System.Drawing.Color.White
        Me.ocbDbName.Enabled = False
        Me.ocbDbName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocbDbName.FormattingEnabled = True
        Me.ocbDbName.Location = New System.Drawing.Point(146, 98)
        Me.ocbDbName.Name = "ocbDbName"
        Me.ocbDbName.Size = New System.Drawing.Size(279, 24)
        Me.ocbDbName.TabIndex = 4
        '
        'ocmDbName
        '
        Me.ocmDbName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmDbName.Image = CType(resources.GetObject("ocmDbName.Image"), System.Drawing.Image)
        Me.ocmDbName.Location = New System.Drawing.Point(426, 98)
        Me.ocmDbName.Name = "ocmDbName"
        Me.ocmDbName.Size = New System.Drawing.Size(26, 24)
        Me.ocmDbName.TabIndex = 5
        Me.ocmDbName.UseVisualStyleBackColor = True
        '
        'ogbMemDB
        '
        Me.ogbMemDB.Controls.Add(Me.olaMemSecond)
        Me.ogbMemDB.Controls.Add(Me.olaMemTimeOut)
        Me.ogbMemDB.Controls.Add(Me.ondMemTimeOut)
        Me.ogbMemDB.Controls.Add(Me.olaMemSrv)
        Me.ogbMemDB.Controls.Add(Me.olaMemUsr)
        Me.ogbMemDB.Controls.Add(Me.olaMemPwd)
        Me.ogbMemDB.Controls.Add(Me.olaMemDbName)
        Me.ogbMemDB.Controls.Add(Me.otbMemSrv)
        Me.ogbMemDB.Controls.Add(Me.otbMemUsr)
        Me.ogbMemDB.Controls.Add(Me.otbMemPwd)
        Me.ogbMemDB.Controls.Add(Me.ocbMemDbName)
        Me.ogbMemDB.Controls.Add(Me.ocmMemDbName)
        Me.ogbMemDB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogbMemDB.Location = New System.Drawing.Point(13, 165)
        Me.ogbMemDB.Name = "ogbMemDB"
        Me.ogbMemDB.Size = New System.Drawing.Size(470, 159)
        Me.ogbMemDB.TabIndex = 2
        Me.ogbMemDB.TabStop = False
        Me.ogbMemDB.Text = "Member Database Setting"
        Me.ogbMemDB.Visible = False
        '
        'olaMemSecond
        '
        Me.olaMemSecond.AutoSize = True
        Me.olaMemSecond.Location = New System.Drawing.Point(237, 131)
        Me.olaMemSecond.Name = "olaMemSecond"
        Me.olaMemSecond.Size = New System.Drawing.Size(55, 16)
        Me.olaMemSecond.TabIndex = 24
        Me.olaMemSecond.Tag = "2;วินาที;Second"
        Me.olaMemSecond.Text = "Second"
        '
        'olaMemTimeOut
        '
        Me.olaMemTimeOut.AutoSize = True
        Me.olaMemTimeOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaMemTimeOut.Location = New System.Drawing.Point(18, 131)
        Me.olaMemTimeOut.Name = "olaMemTimeOut"
        Me.olaMemTimeOut.Size = New System.Drawing.Size(62, 16)
        Me.olaMemTimeOut.TabIndex = 23
        Me.olaMemTimeOut.Tag = "2;Time Out;Time Out"
        Me.olaMemTimeOut.Text = "Time Out"
        '
        'ondMemTimeOut
        '
        Me.ondMemTimeOut.Location = New System.Drawing.Point(147, 129)
        Me.ondMemTimeOut.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.ondMemTimeOut.Minimum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.ondMemTimeOut.Name = "ondMemTimeOut"
        Me.ondMemTimeOut.Size = New System.Drawing.Size(84, 22)
        Me.ondMemTimeOut.TabIndex = 22
        Me.ondMemTimeOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ondMemTimeOut.Value = New Decimal(New Integer() {60, 0, 0, 0})
        '
        'olaMemSrv
        '
        Me.olaMemSrv.AutoSize = True
        Me.olaMemSrv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaMemSrv.Location = New System.Drawing.Point(18, 22)
        Me.olaMemSrv.Name = "olaMemSrv"
        Me.olaMemSrv.Size = New System.Drawing.Size(48, 16)
        Me.olaMemSrv.TabIndex = 18
        Me.olaMemSrv.Tag = ""
        Me.olaMemSrv.Text = "Server"
        '
        'olaMemUsr
        '
        Me.olaMemUsr.AutoSize = True
        Me.olaMemUsr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaMemUsr.Location = New System.Drawing.Point(18, 48)
        Me.olaMemUsr.Name = "olaMemUsr"
        Me.olaMemUsr.Size = New System.Drawing.Size(37, 16)
        Me.olaMemUsr.TabIndex = 19
        Me.olaMemUsr.Tag = "2;ผู้ใช้งาน;User"
        Me.olaMemUsr.Text = "User"
        '
        'olaMemPwd
        '
        Me.olaMemPwd.AutoSize = True
        Me.olaMemPwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaMemPwd.Location = New System.Drawing.Point(18, 75)
        Me.olaMemPwd.Name = "olaMemPwd"
        Me.olaMemPwd.Size = New System.Drawing.Size(68, 16)
        Me.olaMemPwd.TabIndex = 20
        Me.olaMemPwd.Tag = "2;รหัสผ่าน;Password"
        Me.olaMemPwd.Text = "Password"
        '
        'olaMemDbName
        '
        Me.olaMemDbName.AutoSize = True
        Me.olaMemDbName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaMemDbName.Location = New System.Drawing.Point(18, 102)
        Me.olaMemDbName.Name = "olaMemDbName"
        Me.olaMemDbName.Size = New System.Drawing.Size(68, 16)
        Me.olaMemDbName.TabIndex = 21
        Me.olaMemDbName.Tag = "2;ฐานข้อมูล;Database"
        Me.olaMemDbName.Text = "Database"
        '
        'otbMemSrv
        '
        Me.otbMemSrv.BackColor = System.Drawing.Color.White
        Me.otbMemSrv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbMemSrv.Location = New System.Drawing.Point(147, 19)
        Me.otbMemSrv.Name = "otbMemSrv"
        Me.otbMemSrv.Size = New System.Drawing.Size(306, 22)
        Me.otbMemSrv.TabIndex = 13
        '
        'otbMemUsr
        '
        Me.otbMemUsr.BackColor = System.Drawing.Color.White
        Me.otbMemUsr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbMemUsr.Location = New System.Drawing.Point(147, 45)
        Me.otbMemUsr.Name = "otbMemUsr"
        Me.otbMemUsr.Size = New System.Drawing.Size(306, 22)
        Me.otbMemUsr.TabIndex = 14
        '
        'otbMemPwd
        '
        Me.otbMemPwd.BackColor = System.Drawing.Color.White
        Me.otbMemPwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbMemPwd.Location = New System.Drawing.Point(147, 72)
        Me.otbMemPwd.Name = "otbMemPwd"
        Me.otbMemPwd.Size = New System.Drawing.Size(306, 22)
        Me.otbMemPwd.TabIndex = 15
        Me.otbMemPwd.UseSystemPasswordChar = True
        '
        'ocbMemDbName
        '
        Me.ocbMemDbName.BackColor = System.Drawing.Color.White
        Me.ocbMemDbName.Enabled = False
        Me.ocbMemDbName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocbMemDbName.FormattingEnabled = True
        Me.ocbMemDbName.Location = New System.Drawing.Point(147, 99)
        Me.ocbMemDbName.Name = "ocbMemDbName"
        Me.ocbMemDbName.Size = New System.Drawing.Size(279, 24)
        Me.ocbMemDbName.TabIndex = 16
        '
        'ocmMemDbName
        '
        Me.ocmMemDbName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmMemDbName.Image = CType(resources.GetObject("ocmMemDbName.Image"), System.Drawing.Image)
        Me.ocmMemDbName.Location = New System.Drawing.Point(427, 99)
        Me.ocmMemDbName.Name = "ocmMemDbName"
        Me.ocmMemDbName.Size = New System.Drawing.Size(26, 24)
        Me.ocmMemDbName.TabIndex = 17
        Me.ocmMemDbName.UseVisualStyleBackColor = True
        '
        'cUsrConnection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnRoot)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "cUsrConnection"
        Me.Size = New System.Drawing.Size(486, 327)
        Me.opnRoot.ResumeLayout(False)
        Me.ogbForm.ResumeLayout(False)
        Me.ogbForm.PerformLayout()
        CType(Me.ondTimeOut, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ogbMemDB.ResumeLayout(False)
        Me.ogbMemDB.PerformLayout()
        CType(Me.ondMemTimeOut, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents opnRoot As System.Windows.Forms.TableLayoutPanel
    Protected Friend WithEvents ogbForm As System.Windows.Forms.GroupBox
    Protected Friend WithEvents ocmDbName As System.Windows.Forms.Button
    Protected Friend WithEvents otbPwd As System.Windows.Forms.TextBox
    Protected Friend WithEvents ocbDbName As System.Windows.Forms.ComboBox
    Protected Friend WithEvents otbSrv As System.Windows.Forms.TextBox
    Protected Friend WithEvents otbUsr As System.Windows.Forms.TextBox
    Protected Friend WithEvents olaDbName As System.Windows.Forms.Label
    Protected Friend WithEvents olaPwd As System.Windows.Forms.Label
    Protected Friend WithEvents olaUsr As System.Windows.Forms.Label
    Protected Friend WithEvents olaSrv As System.Windows.Forms.Label
    Protected Friend WithEvents olaTimeOut As System.Windows.Forms.Label
    Friend WithEvents ondTimeOut As System.Windows.Forms.NumericUpDown
    Friend WithEvents olaSecond As System.Windows.Forms.Label
    Friend WithEvents ogbMemDB As System.Windows.Forms.GroupBox
    Friend WithEvents olaMemSecond As System.Windows.Forms.Label
    Protected Friend WithEvents olaMemTimeOut As System.Windows.Forms.Label
    Friend WithEvents ondMemTimeOut As System.Windows.Forms.NumericUpDown
    Protected Friend WithEvents olaMemSrv As System.Windows.Forms.Label
    Protected Friend WithEvents olaMemUsr As System.Windows.Forms.Label
    Protected Friend WithEvents olaMemPwd As System.Windows.Forms.Label
    Protected Friend WithEvents olaMemDbName As System.Windows.Forms.Label
    Protected Friend WithEvents otbMemSrv As System.Windows.Forms.TextBox
    Protected Friend WithEvents otbMemUsr As System.Windows.Forms.TextBox
    Protected Friend WithEvents otbMemPwd As System.Windows.Forms.TextBox
    Protected Friend WithEvents ocbMemDbName As System.Windows.Forms.ComboBox
    Protected Friend WithEvents ocmMemDbName As System.Windows.Forms.Button

End Class
