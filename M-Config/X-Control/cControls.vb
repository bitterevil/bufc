﻿Imports System.IO

Public Class cControls
    Private oW_ListCtl As New Hashtable
    Private oW_DesCtl As New Hashtable
    Private tW_CurDesClt As String = ""
    Public Shared Property tAppPath As String = Application.StartupPath
    Public Enum eType
        [Connection] = 1
        [Configuration] = 2
        [Other] = 3
        [sFTP] = 4 'Add sFTP Config '*CH 22-06-2017
        [sDefault] = 5
    End Enum

    Sub New()
        'Load Caption
        oW_DesCtl.Add(eType.Connection, Split("เลือกการเชื่อมต่อที่คุณต้องการปรับปรุง;Select the connection for which you want to update.", ";")(cCNVB.nVB_CutLng - 1))
        oW_DesCtl.Add(eType.Configuration, Split("เลือกไดเรกทอรีของแฟ้มข้อมูลและ Log;Select the location of Directory of file Data and file Log.", ";")(cCNVB.nVB_CutLng - 1))
        oW_DesCtl.Add(eType.Other, Split("Mapping;Mapping", ";")(cCNVB.nVB_CutLng - 1))
        oW_DesCtl.Add(eType.sFTP, Split("sFTP;sFTP", ";")(cCNVB.nVB_CutLng - 1)) 'Add sFTP Config '*CH 22-06-2017
        oW_DesCtl.Add(eType.sDefault, Split("Default;ค่าเริ่มต้น", ";")(cCNVB.nVB_CutLng - 1))

        'Load Instance Menu
        oW_ListCtl.Add(eType.Connection, New cUsrConnection)
        oW_ListCtl.Add(eType.Configuration, New cUsrConfiguration)
        oW_ListCtl.Add(eType.Other, New cUsrMapping)
        oW_ListCtl.Add(eType.sFTP, New cUsrFTP) 'Add sFTP Config '*CH 22-06-2017
        oW_ListCtl.Add(eType.sDefault, New cUsrWord)

        'Load Dts Config
        If File.Exists(Application.StartupPath & "\Config\AdaConfig.xml") = True Then
            Me.W_SETxDataCtl()
        End If

    End Sub

    Private Sub W_SetxCurDes(ByVal ptDes As String)
        Me.tW_CurDesClt = ptDes
    End Sub

    Public Function W_GETtCurDes() As String
        Return Me.tW_CurDesClt
    End Function

    Public Sub W_GETxCurCtl(ByVal poControl As Control, ByVal pnIndex As eType)
        If Not Me.oW_ListCtl(pnIndex) Is Nothing Then
            poControl.Controls.Clear()
            poControl.Controls.Add(Me.oW_ListCtl(pnIndex))
            Me.W_SetxCurDes(Me.oW_DesCtl(pnIndex))
            'CType(Me.oW_ListCtl.Item(eType.Condition), cUsrCondition).W_GEToConn(CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection))
        End If
    End Sub

    Public Sub W_SETxDataCtl()

        Dim oObjConn = oW_ListCtl(eType.Connection)
        'Set Connection
        With oObjConn
            .W_DATtServer = AdaConfig.cConfig.oConnSource.tServer
            .W_DATtUser = AdaConfig.cConfig.oConnSource.tUser
            .W_DATtPasswd = AdaConfig.cConfig.oConnSource.tPassword
            .W_DATtDbName = AdaConfig.cConfig.oConnSource.tCatalog
            .W_DATnTimeOut = AdaConfig.cConfig.oConnSource.nTimeOut
        End With

        'Member Database *CH 18-10-2014
        Dim oObjMemConn = oW_ListCtl(eType.Connection)
        'Set Connection
        With oObjMemConn
            .W_DATtMemServer = AdaConfig.cConfig.oMemConnSource.tServer
            .W_DATtMemUser = AdaConfig.cConfig.oMemConnSource.tUser
            .W_DATtMemPasswd = AdaConfig.cConfig.oMemConnSource.tPassword
            .W_DATtMemDbName = AdaConfig.cConfig.oMemConnSource.tCatalog
            .W_DATnMemTimeOut = AdaConfig.cConfig.oMemConnSource.nTimeOut
        End With

        Dim oObjConf = oW_ListCtl(eType.Configuration)
        'Set Config
        With oObjConf
            .W_DATtPathInbox = AdaConfig.cConfig.oConfigXml.tInbox
            .W_DATtPathBackup = AdaConfig.cConfig.oConfigXml.tBackup
            .W_DATtPathLog = AdaConfig.cConfig.oConfigXml.tLog
            .W_DATtPathOutbox = AdaConfig.cConfig.oConfigXml.tOutbox
        End With

        'sFTP '*CH 22-06-2017
        With oW_ListCtl(eType.sFTP)
            .W_DATtHostName = AdaConfig.cConfig.oConfigXml.tHostName
            .W_DATtUsrName = AdaConfig.cConfig.oConfigXml.tUserName
            .W_DATtPwd = AdaConfig.cConfig.oConfigXml.tPassword
            .W_DATtPortNum = AdaConfig.cConfig.oConfigXml.tPortNumber
            .W_DATtInBound = AdaConfig.cConfig.oConfigXml.tInFolder
            .W_DATtOutBound = AdaConfig.cConfig.oConfigXml.tOutFolder
            .W_DATtReconcile = AdaConfig.cConfig.oConfigXml.tReconcile
            .W_DATtCashTnf = AdaConfig.cConfig.oConfigXml.tCashTransfer
            .W_DATtExpense = AdaConfig.cConfig.oConfigXml.tExpense
            .W_DATtFTPType = AdaConfig.cConfig.oConfigXml.tFTPType
        End With

        Dim oUrsDefault = oW_ListCtl(eType.sDefault)
        With oUrsDefault
            .W_DATtPlant = AdaConfig.cConfig.oUrsDefault.tPlan
            .W_DATtLocation = AdaConfig.cConfig.oUrsDefault.tStorage
            .W_DATtRecive = AdaConfig.cConfig.oUrsDefault.tRecieve
            .W_DATtComcode = AdaConfig.cConfig.oUrsDefault.tCompCode
            .W_DATtShopCode = AdaConfig.cConfig.oUrsDefault.tShopCode
            .W_DATtShopCodeTran = AdaConfig.cConfig.oUrsDefault.tShopCodeTran
            .W_DATtSupplier = AdaConfig.cConfig.oUrsDefault.tSupplier
            .W_DATtReason = AdaConfig.cConfig.oUrsDefault.tReason
        End With

    End Sub

    Public Function W_DATbChkDb() As Boolean
        W_DATbChkDb = False
        Dim oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        Dim tConn As String = String.Format("Data Source={0};User ID={1};Password={2};Initial Catalog={3};Persist Security Info=True;Connect Timeout=15", _
                                          oObj.W_DATtServer, oObj.W_DATtUser, oObj.W_DATtPasswd, oObj.W_DATtDbName)
        Using oSQLConn As New SqlClient.SqlConnection(tConn)
            Try
                oSQLConn.Open()
                W_DATbChkDb = True
            Catch ex As Exception
            End Try
        End Using
    End Function

    Public Sub W_DATxSaveConfig()
        Try

            If Directory.Exists(tAppPath & "\Config") = False Then
                Directory.CreateDirectory(tAppPath & "\Config")
            End If
            Dim oXEAdaConfig As XElement =
                <config>
                    <Application User="EUrhNOJbKWw=" Pass="EUrhNOJbKWw=" Language="2"></Application>
                    <Connection Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></Connection>
                    <ConnectionMem Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></ConnectionMem>
                    <Separator Index="0" Define=""></Separator>
                    <Import Path="Import.xml"></Import>
                    <Export Path="Export.xml"></Export>
                    <FTP Path="sFTP.xml"></FTP>
                </config>
            Dim oAdaConfigXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEAdaConfig)
            oAdaConfigXml.Save(tAppPath & "\Config\AdaConfig.xml")
            oAdaConfigXml = Nothing
            oXEAdaConfig = Nothing

            Dim oObj
            oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)

            With AdaConfig.cConfig.oConnSource
                .tServer = oObj.W_DATtServer
                .tUser = oObj.W_DATtUser
                .tPassword = oObj.W_DATtPasswd
                .tCatalog = oObj.W_DATtDbName
                .nTimeOut = oObj.W_DATnTimeOut
            End With
            'Member Database *CH 18-10-2014
            With AdaConfig.cConfig.oMemConnSource
                .tServer = oObj.W_DATtMemServer
                .tUser = oObj.W_DATtMemUser
                .tPassword = oObj.W_DATtMemPasswd
                .tCatalog = oObj.W_DATtMemDbName
                .nTimeOut = oObj.W_DATnMemTimeOut
            End With

            oObj = CType(Me.oW_ListCtl.Item(eType.Other), cUsrMapping)
            With AdaConfig.cConfig.oSeparator
                .nIndex = oObj.W_GETnIndex
                .tDefine = oObj.W_GETtDefine
            End With

            AdaConfig.cConfig.C_SETbUpdateXml()


            Dim oXEAdaConfigUrs As XElement = <UserSetting>
                                                  <UserDefault Plant="XXX" StorageLocation="XXX" ReceivePlant="XXX" CompanyCode="XXX" ShopCode="XXX" ShopCodeTran="XXX" Supplier="XXX" Reason="XXX"></UserDefault>
                                              </UserSetting>

            Dim oAdaUrs As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEAdaConfigUrs)
            oAdaUrs.Save(tAppPath & "\Config\AdaUserDefault.xml")
            oAdaUrs = Nothing
            oAdaUrs = Nothing

            Dim oUrsDefault = CType(Me.oW_ListCtl.Item(eType.sDefault), cUsrWord)
            With AdaConfig.cConfig.oUrsDefault
                .tPlan = oUrsDefault.W_DATtPlant
                .tStorage = oUrsDefault.W_DATtLocation
                .tRecieve = oUrsDefault.W_DATtRecive
                .tCompCode = oUrsDefault.W_DATtComcode
                .tShopCode = oUrsDefault.W_DATtShopCode
                .tShopCodeTran = oUrsDefault.W_DATtShopCodeTran
                .tSupplier = oUrsDefault.W_DATtSupplier
                .tReason = oUrsDefault.W_DATtReason
            End With
            AdaConfig.cConfig.C_SETbUpdateXMLUsrDefault()




            'Set Configuration
            oObj = CType(Me.oW_ListCtl.Item(eType.Configuration), cUsrConfiguration)
            AdaConfig.cConfig.C_CALxUpdateImport(oObj.W_DATtPathInbox, oObj.W_DATtPathLog, oObj.W_DATtPathBackup)
            AdaConfig.cConfig.C_CALxUpdateExport(oObj.W_DATtPathOutbox)

            'Set sFTP Config '*CH 22-06-2017
            oObj = CType(Me.oW_ListCtl.Item(eType.sFTP), cUsrFTP)
            AdaConfig.cConfig.C_CALxUpdateFTP(oObj.W_DATtHostName, oObj.W_DATtUsrName, oObj.W_DATtPwd, oObj.W_DATtPortNum,
                                              oObj.W_DATtInBound, oObj.W_DATtOutBound, oObj.W_DATtReconcile,
                                              oObj.W_DATtCashTnf, oObj.W_DATtExpense, oObj.W_DATtFTPType)



        Catch ex As Exception
        End Try
    End Sub

    Public Function W_DATbConnChage() As Boolean
        W_DATbConnChage = True
        Dim oObj
        oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        If AdaConfig.cConfig.oConnSource.tServer = oObj.W_DATtServer And AdaConfig.cConfig.oConnSource.tUser = oObj.W_DATtUser And AdaConfig.cConfig.oConnSource.tPassword = oObj.W_DATtPasswd And AdaConfig.cConfig.oConnSource.tCatalog = oObj.W_DATtDbName And AdaConfig.cConfig.oConnSource.nTimeOut = oObj.W_DATnTimeOut Then
            W_DATbConnChage = False
        End If
    End Function

    'Member Database '*CH 18-10-2014
    Public Function W_DATbMemConnChage() As Boolean
        W_DATbMemConnChage = True
        Dim oObj
        oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        If AdaConfig.cConfig.oMemConnSource.tServer = oObj.W_DATtMemServer And AdaConfig.cConfig.oMemConnSource.tUser = oObj.W_DATtMemUser And AdaConfig.cConfig.oMemConnSource.tPassword = oObj.W_DATtMemPasswd And AdaConfig.cConfig.oMemConnSource.tCatalog = oObj.W_DATtMemDbName And AdaConfig.cConfig.oMemConnSource.nTimeOut = oObj.W_DATnMemTimeOut Then
            W_DATbMemConnChage = False
        End If
    End Function

    Public Function W_GEToMapping() As DataTable
        Dim oObj
        oObj = CType(Me.oW_ListCtl.Item(eType.Other), cUsrMapping)
        Return oObj.W_GEToMapping
    End Function

End Class

