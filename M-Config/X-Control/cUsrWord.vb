﻿Imports System.Text

Public Class cUsrWord
    Public Property W_DATtPlant() As String
        Get
            Return Me.otbPlant.Text
        End Get
        Set(ByVal value As String)
            Me.otbPlant.Text = value
        End Set
    End Property

    Public Property W_DATtLocation() As String
        Get
            Return Me.otbLocation.Text
        End Get
        Set(value As String)
            Me.otbLocation.Text = value
        End Set
    End Property

    Public Property W_DATtRecive() As String
        Get
            Return Me.otbRecive.Text
        End Get
        Set(value As String)
            Me.otbRecive.Text = value
        End Set
    End Property

    Public Property W_DATtComcode() As String
        Get
            Return Me.otbComp.Text
        End Get
        Set(value As String)
            Me.otbComp.Text = value
        End Set
    End Property

    Public Property W_DATtShopCode() As String
        Get
            Return Me.otbShop.Text
        End Get
        Set(value As String)
            Me.otbShop.Text = value
        End Set
    End Property

    Public Property W_DATtShopCodeTran() As String
        Get
            Return Me.otbShopTran.Text
        End Get
        Set(value As String)
            Me.otbShopTran.Text = value
        End Set
    End Property

    Public Property W_DATtSupplier() As String
        Get
            Return Me.ocbSupply.SelectedValue
        End Get
        Set(value As String)
            Me.ocbSupply.SelectedValue = value
        End Set
    End Property

    Public Property W_DATtReason() As String
        Get
            Return Me.ocbReason.SelectedValue
        End Get
        Set(value As String)
            Me.ocbReason.SelectedValue = value
        End Set
    End Property


    Public Sub W_LoadxDataSupplier()


        Try
            Dim oSql As New StringBuilder()
            Dim oDatabase As New cDatabaseLocal()
            Dim oDbDts As New DataSet()

            oSql.Clear()
            oSql.AppendLine("SELECT FTSplCode,FTSplname FROM TCNMSpl")
            oDbDts = oDatabase.W_DAToExcuteds(oSql.ToString())
            If oDbDts.Tables(0).Rows.Count > 0 Then
                With ocbSupply
                    .DisplayMember = "FTSplname"
                    .ValueMember = "FTSplCode"
                    .DataSource = oDbDts.Tables(0)
                End With
            End If

            If AdaConfig.cConfig.oUrsDefault.tSupplier <> "XXX" Then
                ocbSupply.SelectedValue = AdaConfig.cConfig.oUrsDefault.tSupplier
            End If


        Catch ex As Exception

        End Try

    End Sub

    Public Sub W_LoadxDataReason()


        Try
            Dim oSql As New StringBuilder()
            Dim oDatabase As New cDatabaseLocal()
            Dim oDbDts As New DataSet()

            oSql.Clear()
            oSql.AppendLine(" SELECT FTTrnCode,FTTrnName FROM TCNMTnfReason ")
            oSql.AppendLine(" WHERE FTTrnStain = N'1' AND FTTrnStaOut = N'0' ")
            oDbDts = oDatabase.W_DAToExcuteds(oSql.ToString())
            If oDbDts.Tables(0).Rows.Count > 0 Then
                With ocbReason
                    .DisplayMember = "FTTrnName"
                    .ValueMember = "FTTrnCode"
                    .DataSource = oDbDts.Tables(0)
                End With
            End If

            If AdaConfig.cConfig.oUrsDefault.tSupplier <> "XXX" Then
                ocbReason.SelectedValue = AdaConfig.cConfig.oUrsDefault.tReason
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub cUsrWord_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        W_LoadxDataSupplier()
        W_LoadxDataReason()

    End Sub
End Class
