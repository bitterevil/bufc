﻿Imports System.Text
Imports C1.Win.C1FlexGrid

Public Class cUsrMapping

    Private bW_Load As Boolean = False

    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.Dock = DockStyle.Fill
    End Sub

    Public Sub W_SETxInitGrid()
        '====================================
        'Cmt : Initail Grid (fed And Caption)
        '====================================
        'Me.ogdLog.Clear(ClearFlags.Content, Me.ogdLog.Rows.Fixed, 0, Me.ogdLog.Rows.Count - 1, Me.ogdLog.Cols.Count - 1)
        With Me.ogdMapping
            .AllowResizing = AllowResizingEnum.None
            .AllowSorting = AllowSortingEnum.None
            .AllowDragging = AllowDraggingEnum.None
            .AllowEditing = True
            .Cols("FNLNMSeqNo").Caption = IIf(cCNVB.nVB_CutLng = 2, "No.", "ลำดับ")
            .Cols("FNLNMSeqNo").AllowEditing = False
            .Cols("FNLNMSeqNo").Width = 35

            .Cols("FTLNMType").Caption = IIf(cCNVB.nVB_CutLng = 2, "Type", "ประเภท")
            .Cols("FTLNMType").AllowEditing = False
            .Cols("FTLNMType").Width = 100

            .Cols("FTLNMCode").Caption = IIf(cCNVB.nVB_CutLng = 2, "Code", "รหัส")
            .Cols("FTLNMCode").AllowEditing = False

            .Cols("FTLNMName").Caption = IIf(cCNVB.nVB_CutLng = 2, "Name", "ชื่อ")
            .Cols("FTLNMName").AllowEditing = False
            .Cols("FTLNMName").Width = 190

            .Cols("FTLNMNameOth").Caption = IIf(cCNVB.nVB_CutLng = 2, "Name", "ชื่อ")
            .Cols("FTLNMNameOth").AllowEditing = False
            .Cols("FTLNMNameOth").Width = 190


            .Cols("FTLNMDesc").Caption = IIf(cCNVB.nVB_CutLng = 2, "Description", "คำอธิบาย")
            .Cols("FTLNMDesc").AllowEditing = False
            .Cols("FTLNMDesc").Width = 85

            .Cols("FTLNMDefValue").Caption = IIf(cCNVB.nVB_CutLng = 2, "Default", "ค่าเริ่มต้น")
            .Cols("FTLNMDefValue").AllowEditing = False
            .Cols("FTLNMDefValue").Width = 70

            .Cols("FTLNMUsrValue").Caption = IIf(cCNVB.nVB_CutLng = 2, "Value", "กำหนดเอง")
            .Cols("FTLNMUsrValue").AllowEditing = True
            .Cols("FTLNMUsrValue").Width = 70

            .Cols("FNLNMSeqNo").Visible = True
            .Cols("FTLNMCode").Visible = False
            .Cols("FTLNMType").Visible = False
            .Cols("FTLNMEditLen").Visible = False

            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then
                '.Cols("FTLNMName").Visible = False
                '.Cols("FTLNMNameOth").Visible = True
                .Cols("FTLNMName").Visible = True
                .Cols("FTLNMNameOth").Visible = False
            Else
                .Cols("FTLNMName").Visible = True
                .Cols("FTLNMNameOth").Visible = False
            End If

        End With
        Me.bW_Load = True
    End Sub

    Private Sub cUsrMapping_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.otbOth.ReadOnly = True
        Me.otbOth.BackColor = Color.NavajoWhite
        Select Case AdaConfig.cConfig.oSeparator.nIndex
            Case 0
                Me.orbSeparator0.Checked = True
            Case 1
                Me.orbSeparator1.Checked = True
            Case 2
                Me.orbSeparator2.Checked = True
            Case 3
                Me.orbSeparator3.Checked = True
            Case 4
                Me.orbSeparator4.Checked = True
                Me.otbOth.Text = AdaConfig.cConfig.oSeparator.tDefine
                Me.otbOth.ReadOnly = False
                Me.otbOth.BackColor = Color.White
        End Select

        Try
            If AdaConfig.cConfig.C_CHKbSQLSourceConnect Then
                Dim oDatabase As New cDatabaseLocal
                Dim oTLNKMapping As DataTable
                'Dim tSql As String = "SELECT ROW_NUMBER() OVER(ORDER BY FTLNMType, FNLNMSeqNo) AS FNLNMSeqNo,FTLNMType, FTLNMCode, FTLNMName, FTLNMNameOth, FTLNMDesc, FTLNMDefValue, FTLNMUsrValue, FTLNMStaDisplay, FTLNMStaUse, FTLNMEditLen FROM TLNKMapping WHERE FTLNMStaDisplay='1' AND FTLNMStaUse='1' ORDER BY FTLNMType,FNLNMSeqNo"
                '  Dim tSql As String = "SELECT FNLNMSeqNo,FTLNMType, FTLNMCode, FTLNMName, FTLNMNameOth, FTLNMDesc, FTLNMDefValue, FTLNMUsrValue, FTLNMStaDisplay, FTLNMStaUse, FTLNMEditLen FROM TLNKMapping WHERE FTLNMStaDisplay='1' AND FTLNMStaUse='1' ORDER BY FTLNMType,FNLNMSeqNo"
                Dim tSql As String = "SELECT FNLNMSeqNo,FTLNMType, FTLNMCode, FTLNMName, FTLNMNameOth, FTLNMDesc, FTLNMDefValue, FTLNMUsrValue, FTLNMStaDisplay, FTLNMStaUse, FTLNMEditLen FROM TLNKMapping WHERE FTLNMStaDisplay='1' AND FTLNMStaUse='1' "
                oTLNKMapping = oDatabase.C_CALoExecuteReader(tSql)
                Me.ogdMapping.DataSource = oTLNKMapping
            End If
        Catch ex As Exception
        End Try

        Me.W_SETxInitGrid()
    End Sub

    Private Sub ogdMapping_SetupEditor(sender As Object, e As RowColEventArgs) Handles ogdMapping.SetupEditor

        With ogdMapping
            If e.Col = .Cols("FTLNMUsrValue").Index Then
                Dim oEditor As New TextBox
                .Cols("FTLNMUsrValue").Editor = oEditor
            End If
        End With
    End Sub

    Public ReadOnly Property W_GETnIndex() As Integer
        Get
            Select Case True
                Case Me.orbSeparator0.Checked
                    Return 0
                Case Me.orbSeparator1.Checked
                    Return 1
                Case Me.orbSeparator2.Checked
                    Return 2
                Case Me.orbSeparator3.Checked
                    Return 3
                Case Me.orbSeparator4.Checked
                    Return 4
            End Select
            Return 0
        End Get
    End Property

    Public ReadOnly Property W_GETtDefine() As String
        Get
            Return Me.otbOth.Text
        End Get
    End Property

    Private Sub otbOth_KeyPress(sender As Object, e As KeyPressEventArgs) Handles otbOth.KeyPress
        '   "" ' < >
    End Sub

    Public ReadOnly Property W_GEToMapping As DataTable
        Get
            Return CType(Me.ogdMapping.DataSource, DataTable)
        End Get
    End Property

    Private Sub cUsrJobs_SizeChanged(sender As Object, e As System.EventArgs) Handles Me.SizeChanged
        If Me.bW_Load = True Then
            With Me.ogdMapping
                If Me.Width < 500 Then
                    .Cols("FNLNMSeqNo").Width = 35
                    .Cols("FTLNMType").Width = 100
                    .Cols("FTLNMName").Width = 105
                    .Cols("FTLNMNameOth").Width = 190
                    .Cols("FTLNMDesc").Width = 150
                    .Cols("FTLNMDefValue").Width = 70
                    .Cols("FTLNMUsrValue").Width = 70
                Else
                    .Cols("FNLNMSeqNo").AllowResizing = True
                    .Cols("FTLNMType").AllowResizing = True
                    .Cols("FTLNMName").AllowResizing = True
                    .Cols("FTLNMNameOth").AllowResizing = True
                    .Cols("FTLNMDesc").AllowResizing = True
                    .Cols("FTLNMDefValue").AllowResizing = True
                    .Cols("FTLNMUsrValue").AllowResizing = True
                    Dim nSizeAll As Integer = .Size.Width - 19
                    Dim nSize50 As Integer = nSizeAll / 2
                    Dim nSize25 As Integer = nSize50 / 2
                    .Cols("FTLNMDesc").Width = nSize25
                    .Cols("FTLNMDefValue").Width = nSize25
                    .Cols("FTLNMUsrValue").Width = nSize25
                    .Cols("FTLNMName").Width = (nSizeAll - .Cols("FNLNMSeqNo").Width - .Cols("FTLNMDesc").Width - .Cols("FTLNMDefValue").Width - .Cols("FTLNMUsrValue").Width)
                    .Cols("FTLNMNameOth").Width = .Cols("FTLNMName").Width
                    .Cols("FNLNMSeqNo").AllowResizing = False
                    .Cols("FTLNMType").AllowResizing = False
                    .Cols("FTLNMName").AllowResizing = False
                    .Cols("FTLNMNameOth").AllowResizing = False
                    .Cols("FTLNMDesc").AllowResizing = False
                    .Cols("FTLNMDefValue").AllowResizing = False
                    .Cols("FTLNMUsrValue").AllowResizing = False
                End If
            End With
        End If
    End Sub

    Private Sub W_SETxSeparator(sender As Object, e As EventArgs) Handles orbSeparator0.CheckedChanged, orbSeparator1.CheckedChanged, orbSeparator2.CheckedChanged,
        orbSeparator3.CheckedChanged, orbSeparator4.CheckedChanged
        Try
            Me.otbOth.ReadOnly = True
            Me.otbOth.BackColor = Color.NavajoWhite
            Select Case True
                Case Me.orbSeparator4.Checked
                    Me.otbOth.ReadOnly = False
                    Me.otbOth.BackColor = Color.White
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ogdMapping_AfterEdit(sender As Object, e As RowColEventArgs) Handles ogdMapping.AfterEdit
        Dim oDatabase As New cDatabaseLocal
        Dim oSQL As New StringBuilder

        Try
            Dim oConGrid As C1FlexGrid = CType(sender, C1FlexGrid)
            Dim tGetUValue As String = ogdMapping(oConGrid.Row, oConGrid.Col)
            Dim tGetName As String = ogdMapping(oConGrid.Row, 4)
            oSQL.AppendLine("UPDATE TLNKMAPPING SET FTLNMUsrValue = '" & tGetUValue & "'")
            oSQL.AppendLine("WHERE FTLNMName ='" & tGetName & "' ")

            If oDatabase.C_CALnExecuteNonQuery(oSQL.ToString) = 1 Then
            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
