﻿Public Class cUsrConnection
    Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'Me.Dock = DockStyle.Fill
    End Sub

    'Private Sub cConnection_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
    '    Me.ogdForm.Cols(1).Width = Me.ogdForm.Size.Width - 2
    'End Sub

    Private Const tW_FmtSql As String = "Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};Connect Timeout={4}"

    Public Property W_DATtServer() As String
        Get
            Return Me.otbSrv.Text
        End Get
        Set(ByVal value As String)
            Me.otbSrv.Text = value
        End Set
    End Property

    Public Property W_DATtUser() As String
        Get
            Return Me.otbUsr.Text
        End Get
        Set(ByVal value As String)
            Me.otbUsr.Text = value
        End Set
    End Property

    Public Property W_DATtPasswd() As String
        Get
            Return Me.otbPwd.Text
        End Get
        Set(ByVal value As String)
            Me.otbPwd.Text = value
        End Set
    End Property

    Public Property W_DATtDbName() As String
        Get
            Return Me.ocbDbName.Text
        End Get
        Set(ByVal value As String)
            Me.ocbDbName.Text = value
        End Set
    End Property

    Public Property W_DATnTimeOut() As Integer
        Get
            Return Me.ondTimeOut.Value
        End Get
        Set(ByVal value As Integer)
            Me.ondTimeOut.Value = value
        End Set
    End Property

    'Member Database '*CH 18-10-2014
    Public Property W_DATtMemServer() As String
        Get
            Return Me.otbMemSrv.Text
        End Get
        Set(ByVal value As String)
            Me.otbMemSrv.Text = value
        End Set
    End Property

    Public Property W_DATtMemUser() As String
        Get
            Return Me.otbMemUsr.Text
        End Get
        Set(ByVal value As String)
            Me.otbMemUsr.Text = value
        End Set
    End Property

    Public Property W_DATtMemPasswd() As String
        Get
            Return Me.otbMemPwd.Text
        End Get
        Set(ByVal value As String)
            Me.otbMemPwd.Text = value
        End Set
    End Property

    Public Property W_DATtMemDbName() As String
        Get
            Return Me.ocbMemDbName.Text
        End Get
        Set(ByVal value As String)
            Me.ocbMemDbName.Text = value
        End Set
    End Property

    Public Property W_DATnMemTimeOut() As Integer
        Get
            Return Me.ondMemTimeOut.Value
        End Get
        Set(ByVal value As Integer)
            Me.ondMemTimeOut.Value = value
        End Set
    End Property

    Private Sub ocmDbName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmDbName.Click
        Dim oDataRow As DataRow()
        Dim oDataTable As New DataTable
        If Me.otbSrv.Text.Trim = "" Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN103, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Exit Sub
        End If
        Me.ocbDbName.Items.Clear()
        Me.ocbDbName.DropDownStyle = ComboBoxStyle.DropDownList
        Try
            Using oSqlConn = New SqlClient.SqlConnection(String.Format(tW_FmtSql, Me.otbSrv.Text.Trim, "Master", Me.otbUsr.Text.Trim, Me.otbPwd.Text.Trim, 5))
                oSqlConn.Open()
                oDataTable = oSqlConn.GetSchema("Databases")
                oDataRow = oDataTable.Select()
                'For nRow As Integer = 0 To oDataRow.Length - 1
                '    Me.ocbDbName.Items.Add(oDataRow(nRow).Item(0))
                'Next
                'เรียงตามชื่อ *CH 09-12-2014
                Dim oDbName = (From oItem In oDataRow Order By oItem.Item(0))
                For nRow As Integer = 0 To oDbName.Count - 1
                    Me.ocbDbName.Items.Add(oDbName(nRow).Item(0))
                Next
                Me.ocbDbName.Enabled = True
                oSqlConn.Close()
            End Using
        Catch ex As SqlClient.SqlException
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.ocbDbName.Enabled = False
        End Try
        oDataRow = Nothing
        oDataTable = Nothing
    End Sub

    Private Sub ocmMemDbName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmMemDbName.Click
        Dim oDataRow As DataRow()
        Dim oDataTable As New DataTable
        If Me.otbSrv.Text.Trim = "" Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN103, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Exit Sub
        End If
        Me.ocbMemDbName.Items.Clear()
        Me.ocbMemDbName.DropDownStyle = ComboBoxStyle.DropDownList
        Try
            Using oSqlConn = New SqlClient.SqlConnection(String.Format(tW_FmtSql, Me.otbMemSrv.Text.Trim, "Master", Me.otbMemUsr.Text.Trim, Me.otbMemPwd.Text.Trim, 5))
                oSqlConn.Open()
                oDataTable = oSqlConn.GetSchema("Databases")
                oDataRow = oDataTable.Select()
                'For nRow As Integer = 0 To oDataRow.Length - 1
                '    Me.ocbMemDbName.Items.Add(oDataRow(nRow).Item(0))
                'Next
                'เรียงตามชื่อ *CH 09-12-2014
                Dim oDbName = (From oItem In oDataRow Order By oItem.Item(0))
                For nRow As Integer = 0 To oDbName.Count - 1
                    Me.ocbMemDbName.Items.Add(oDbName(nRow).Item(0))
                Next
                Me.ocbMemDbName.Enabled = True
                oSqlConn.Close()
            End Using
        Catch ex As SqlClient.SqlException
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.ocbMemDbName.Enabled = False
        End Try
        oDataRow = Nothing
        oDataTable = Nothing
    End Sub
End Class
