﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cUsrConfiguration
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.opnRoot = New System.Windows.Forms.TableLayoutPanel()
        Me.ogbImport = New System.Windows.Forms.GroupBox()
        Me.opnImportLayout = New System.Windows.Forms.TableLayoutPanel()
        Me.ocmBrwLog = New System.Windows.Forms.Button()
        Me.olaInbox = New System.Windows.Forms.Label()
        Me.olaBackup = New System.Windows.Forms.Label()
        Me.ocmBrwInbox = New System.Windows.Forms.Button()
        Me.ocmBrwBackup = New System.Windows.Forms.Button()
        Me.olaLog = New System.Windows.Forms.Label()
        Me.otbBackup = New System.Windows.Forms.TextBox()
        Me.otbLog = New System.Windows.Forms.TextBox()
        Me.otbInbox = New System.Windows.Forms.TextBox()
        Me.ogbExport = New System.Windows.Forms.GroupBox()
        Me.opnExportLayout = New System.Windows.Forms.TableLayoutPanel()
        Me.olaOutbox = New System.Windows.Forms.Label()
        Me.ocmBrwOutbox = New System.Windows.Forms.Button()
        Me.otbOutbox = New System.Windows.Forms.TextBox()
        Me.ogbPurge = New System.Windows.Forms.GroupBox()
        Me.otbPurge = New System.Windows.Forms.TextBox()
        Me.olaDay = New System.Windows.Forms.Label()
        Me.olaPurge = New System.Windows.Forms.Label()
        Me.ofdForm = New System.Windows.Forms.FolderBrowserDialog()
        Me.ofdForm2 = New System.Windows.Forms.FolderBrowserDialog()
        Me.opnRoot.SuspendLayout()
        Me.ogbImport.SuspendLayout()
        Me.opnImportLayout.SuspendLayout()
        Me.ogbExport.SuspendLayout()
        Me.opnExportLayout.SuspendLayout()
        Me.ogbPurge.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnRoot
        '
        Me.opnRoot.BackColor = System.Drawing.Color.Transparent
        Me.opnRoot.ColumnCount = 2
        Me.opnRoot.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.opnRoot.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.opnRoot.Controls.Add(Me.ogbImport, 1, 0)
        Me.opnRoot.Controls.Add(Me.ogbExport, 1, 1)
        Me.opnRoot.Controls.Add(Me.ogbPurge, 1, 2)
        Me.opnRoot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnRoot.Location = New System.Drawing.Point(0, 0)
        Me.opnRoot.Name = "opnRoot"
        Me.opnRoot.RowCount = 3
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 160.0!))
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 101.0!))
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73.0!))
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnRoot.Size = New System.Drawing.Size(486, 327)
        Me.opnRoot.TabIndex = 0
        '
        'ogbImport
        '
        Me.ogbImport.BackColor = System.Drawing.Color.Transparent
        Me.ogbImport.Controls.Add(Me.opnImportLayout)
        Me.ogbImport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogbImport.Location = New System.Drawing.Point(13, 3)
        Me.ogbImport.Name = "ogbImport"
        Me.ogbImport.Size = New System.Drawing.Size(470, 154)
        Me.ogbImport.TabIndex = 0
        Me.ogbImport.TabStop = False
        Me.ogbImport.Tag = "2;นำเข้า;Import"
        Me.ogbImport.Text = "Import"
        '
        'opnImportLayout
        '
        Me.opnImportLayout.BackColor = System.Drawing.Color.Transparent
        Me.opnImportLayout.ColumnCount = 2
        Me.opnImportLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.opnImportLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.opnImportLayout.Controls.Add(Me.ocmBrwLog, 1, 5)
        Me.opnImportLayout.Controls.Add(Me.olaInbox, 0, 0)
        Me.opnImportLayout.Controls.Add(Me.olaBackup, 0, 2)
        Me.opnImportLayout.Controls.Add(Me.ocmBrwInbox, 1, 1)
        Me.opnImportLayout.Controls.Add(Me.ocmBrwBackup, 1, 3)
        Me.opnImportLayout.Controls.Add(Me.olaLog, 0, 4)
        Me.opnImportLayout.Controls.Add(Me.otbBackup, 0, 3)
        Me.opnImportLayout.Controls.Add(Me.otbLog, 0, 5)
        Me.opnImportLayout.Controls.Add(Me.otbInbox, 0, 1)
        Me.opnImportLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnImportLayout.Location = New System.Drawing.Point(3, 18)
        Me.opnImportLayout.Name = "opnImportLayout"
        Me.opnImportLayout.RowCount = 7
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnImportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnImportLayout.Size = New System.Drawing.Size(464, 133)
        Me.opnImportLayout.TabIndex = 1
        '
        'ocmBrwLog
        '
        Me.ocmBrwLog.Location = New System.Drawing.Point(434, 107)
        Me.ocmBrwLog.Margin = New System.Windows.Forms.Padding(0, 3, 3, 3)
        Me.ocmBrwLog.Name = "ocmBrwLog"
        Me.ocmBrwLog.Size = New System.Drawing.Size(27, 22)
        Me.ocmBrwLog.TabIndex = 6
        Me.ocmBrwLog.Text = "..."
        Me.ocmBrwLog.UseVisualStyleBackColor = True
        '
        'olaInbox
        '
        Me.olaInbox.AutoSize = True
        Me.olaInbox.Location = New System.Drawing.Point(3, 0)
        Me.olaInbox.Name = "olaInbox"
        Me.olaInbox.Size = New System.Drawing.Size(88, 16)
        Me.olaInbox.TabIndex = 0
        Me.olaInbox.Tag = "2;โฟลเดอร์นำเข้า : ;Folder Inbox : "
        Me.olaInbox.Text = "Folder Inbox :"
        '
        'olaBackup
        '
        Me.olaBackup.AutoSize = True
        Me.olaBackup.Location = New System.Drawing.Point(3, 44)
        Me.olaBackup.Name = "olaBackup"
        Me.olaBackup.Size = New System.Drawing.Size(102, 16)
        Me.olaBackup.TabIndex = 2
        Me.olaBackup.Tag = "2;โฟลเดอร์สำรอง : ;Folder Backup : "
        Me.olaBackup.Text = "Folder Backup :"
        '
        'ocmBrwInbox
        '
        Me.ocmBrwInbox.Location = New System.Drawing.Point(434, 19)
        Me.ocmBrwInbox.Margin = New System.Windows.Forms.Padding(0, 3, 3, 3)
        Me.ocmBrwInbox.Name = "ocmBrwInbox"
        Me.ocmBrwInbox.Size = New System.Drawing.Size(27, 22)
        Me.ocmBrwInbox.TabIndex = 4
        Me.ocmBrwInbox.Text = "..."
        Me.ocmBrwInbox.UseVisualStyleBackColor = True
        '
        'ocmBrwBackup
        '
        Me.ocmBrwBackup.Location = New System.Drawing.Point(434, 63)
        Me.ocmBrwBackup.Margin = New System.Windows.Forms.Padding(0, 3, 3, 3)
        Me.ocmBrwBackup.Name = "ocmBrwBackup"
        Me.ocmBrwBackup.Size = New System.Drawing.Size(27, 22)
        Me.ocmBrwBackup.TabIndex = 5
        Me.ocmBrwBackup.Text = "..."
        Me.ocmBrwBackup.UseVisualStyleBackColor = True
        '
        'olaLog
        '
        Me.olaLog.AutoSize = True
        Me.olaLog.Location = New System.Drawing.Point(3, 88)
        Me.olaLog.Name = "olaLog"
        Me.olaLog.Size = New System.Drawing.Size(79, 16)
        Me.olaLog.TabIndex = 6
        Me.olaLog.Tag = "2;โฟลเดอร์ประวัติ : ;Folder Log : "
        Me.olaLog.Text = "Folder Log :"
        '
        'otbBackup
        '
        Me.otbBackup.Location = New System.Drawing.Point(10, 63)
        Me.otbBackup.Margin = New System.Windows.Forms.Padding(10, 3, 0, 3)
        Me.otbBackup.Name = "otbBackup"
        Me.otbBackup.Size = New System.Drawing.Size(424, 22)
        Me.otbBackup.TabIndex = 9
        '
        'otbLog
        '
        Me.otbLog.Location = New System.Drawing.Point(10, 107)
        Me.otbLog.Margin = New System.Windows.Forms.Padding(10, 3, 0, 3)
        Me.otbLog.Name = "otbLog"
        Me.otbLog.Size = New System.Drawing.Size(424, 22)
        Me.otbLog.TabIndex = 10
        '
        'otbInbox
        '
        Me.otbInbox.Location = New System.Drawing.Point(10, 19)
        Me.otbInbox.Margin = New System.Windows.Forms.Padding(10, 3, 0, 3)
        Me.otbInbox.Name = "otbInbox"
        Me.otbInbox.Size = New System.Drawing.Size(424, 22)
        Me.otbInbox.TabIndex = 11
        '
        'ogbExport
        '
        Me.ogbExport.BackColor = System.Drawing.Color.Transparent
        Me.ogbExport.Controls.Add(Me.opnExportLayout)
        Me.ogbExport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogbExport.Location = New System.Drawing.Point(13, 163)
        Me.ogbExport.Name = "ogbExport"
        Me.ogbExport.Size = New System.Drawing.Size(470, 95)
        Me.ogbExport.TabIndex = 1
        Me.ogbExport.TabStop = False
        Me.ogbExport.Tag = "2;ส่งออก;Export"
        Me.ogbExport.Text = "Export"
        '
        'opnExportLayout
        '
        Me.opnExportLayout.BackColor = System.Drawing.Color.Transparent
        Me.opnExportLayout.ColumnCount = 2
        Me.opnExportLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.opnExportLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.opnExportLayout.Controls.Add(Me.olaOutbox, 0, 0)
        Me.opnExportLayout.Controls.Add(Me.ocmBrwOutbox, 1, 1)
        Me.opnExportLayout.Controls.Add(Me.otbOutbox, 0, 1)
        Me.opnExportLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnExportLayout.Location = New System.Drawing.Point(3, 18)
        Me.opnExportLayout.Name = "opnExportLayout"
        Me.opnExportLayout.RowCount = 2
        Me.opnExportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.opnExportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnExportLayout.Size = New System.Drawing.Size(464, 74)
        Me.opnExportLayout.TabIndex = 2
        '
        'olaOutbox
        '
        Me.olaOutbox.AutoSize = True
        Me.olaOutbox.Location = New System.Drawing.Point(3, 0)
        Me.olaOutbox.Name = "olaOutbox"
        Me.olaOutbox.Size = New System.Drawing.Size(98, 16)
        Me.olaOutbox.TabIndex = 0
        Me.olaOutbox.Tag = "2;โฟลเดอร์ส่งออก : ;Folder Outbox : "
        Me.olaOutbox.Text = "Folder Outbox :"
        '
        'ocmBrwOutbox
        '
        Me.ocmBrwOutbox.Location = New System.Drawing.Point(434, 19)
        Me.ocmBrwOutbox.Margin = New System.Windows.Forms.Padding(0, 3, 3, 3)
        Me.ocmBrwOutbox.Name = "ocmBrwOutbox"
        Me.ocmBrwOutbox.Size = New System.Drawing.Size(27, 22)
        Me.ocmBrwOutbox.TabIndex = 4
        Me.ocmBrwOutbox.Text = "..."
        Me.ocmBrwOutbox.UseVisualStyleBackColor = True
        '
        'otbOutbox
        '
        Me.otbOutbox.Location = New System.Drawing.Point(10, 19)
        Me.otbOutbox.Margin = New System.Windows.Forms.Padding(10, 3, 0, 3)
        Me.otbOutbox.Name = "otbOutbox"
        Me.otbOutbox.Size = New System.Drawing.Size(424, 22)
        Me.otbOutbox.TabIndex = 5
        '
        'ogbPurge
        '
        Me.ogbPurge.Controls.Add(Me.otbPurge)
        Me.ogbPurge.Controls.Add(Me.olaDay)
        Me.ogbPurge.Controls.Add(Me.olaPurge)
        Me.ogbPurge.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbPurge.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogbPurge.Location = New System.Drawing.Point(13, 264)
        Me.ogbPurge.Name = "ogbPurge"
        Me.ogbPurge.Size = New System.Drawing.Size(470, 67)
        Me.ogbPurge.TabIndex = 2
        Me.ogbPurge.TabStop = False
        Me.ogbPurge.Tag = "2;ล้าง;Purge"
        Me.ogbPurge.Text = "Purge"
        '
        'otbPurge
        '
        Me.otbPurge.Location = New System.Drawing.Point(282, 15)
        Me.otbPurge.Name = "otbPurge"
        Me.otbPurge.Size = New System.Drawing.Size(100, 22)
        Me.otbPurge.TabIndex = 2
        Me.otbPurge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'olaDay
        '
        Me.olaDay.AutoSize = True
        Me.olaDay.Location = New System.Drawing.Point(388, 18)
        Me.olaDay.Name = "olaDay"
        Me.olaDay.Size = New System.Drawing.Size(46, 16)
        Me.olaDay.TabIndex = 1
        Me.olaDay.Tag = "2;วัน;day(s)"
        Me.olaDay.Text = "day(s)"
        '
        'olaPurge
        '
        Me.olaPurge.AutoSize = True
        Me.olaPurge.Location = New System.Drawing.Point(6, 18)
        Me.olaPurge.Name = "olaPurge"
        Me.olaPurge.Size = New System.Drawing.Size(136, 16)
        Me.olaPurge.TabIndex = 0
        Me.olaPurge.Tag = "2;ระยะเวลาสำรองไฟล์ข้อมูล (วัน);Log file duration (day)"
        Me.olaPurge.Text = "Log file duration (day)"
        '
        'ofdForm
        '
        '
        'ofdForm2
        '
        '
        'cUsrConfiguration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnRoot)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "cUsrConfiguration"
        Me.Size = New System.Drawing.Size(486, 327)
        Me.opnRoot.ResumeLayout(False)
        Me.ogbImport.ResumeLayout(False)
        Me.opnImportLayout.ResumeLayout(False)
        Me.opnImportLayout.PerformLayout()
        Me.ogbExport.ResumeLayout(False)
        Me.opnExportLayout.ResumeLayout(False)
        Me.opnExportLayout.PerformLayout()
        Me.ogbPurge.ResumeLayout(False)
        Me.ogbPurge.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents opnRoot As System.Windows.Forms.TableLayoutPanel
    Protected Friend WithEvents ogbImport As System.Windows.Forms.GroupBox
    Protected Friend WithEvents opnImportLayout As System.Windows.Forms.TableLayoutPanel
    Protected Friend WithEvents olaInbox As System.Windows.Forms.Label
    Protected Friend WithEvents olaBackup As System.Windows.Forms.Label
    Protected Friend WithEvents ocmBrwInbox As System.Windows.Forms.Button
    Protected Friend WithEvents ocmBrwBackup As System.Windows.Forms.Button
    Friend WithEvents ofdForm As System.Windows.Forms.FolderBrowserDialog
    Protected Friend WithEvents ogbExport As System.Windows.Forms.GroupBox
    Protected Friend WithEvents ocmBrwLog As System.Windows.Forms.Button
    Protected Friend WithEvents olaLog As System.Windows.Forms.Label
    Protected Friend WithEvents opnExportLayout As System.Windows.Forms.TableLayoutPanel
    Protected Friend WithEvents olaOutbox As System.Windows.Forms.Label
    Protected Friend WithEvents ocmBrwOutbox As System.Windows.Forms.Button
    Protected Friend WithEvents ogbPurge As System.Windows.Forms.GroupBox
    Friend WithEvents otbPurge As System.Windows.Forms.TextBox
    Friend WithEvents olaDay As System.Windows.Forms.Label
    Friend WithEvents olaPurge As System.Windows.Forms.Label
    Friend WithEvents otbBackup As System.Windows.Forms.TextBox
    Friend WithEvents otbLog As System.Windows.Forms.TextBox
    Friend WithEvents otbOutbox As System.Windows.Forms.TextBox
    Friend WithEvents ofdForm2 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents otbInbox As System.Windows.Forms.TextBox

End Class
