﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cUsrFTP
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.opnLine1 = New System.Windows.Forms.Panel()
        Me.olaHost = New System.Windows.Forms.Label()
        Me.otbHostName = New System.Windows.Forms.TextBox()
        Me.otbUsrName = New System.Windows.Forms.TextBox()
        Me.otbPwd = New System.Windows.Forms.TextBox()
        Me.otbPortNum = New System.Windows.Forms.TextBox()
        Me.otbInBound = New System.Windows.Forms.TextBox()
        Me.otbOutBound = New System.Windows.Forms.TextBox()
        Me.olaUsrName = New System.Windows.Forms.Label()
        Me.olaPwd = New System.Windows.Forms.Label()
        Me.olaPort = New System.Windows.Forms.Label()
        Me.olaInBound = New System.Windows.Forms.Label()
        Me.olaOutBound = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.olaReCon = New System.Windows.Forms.Label()
        Me.otbReconcile = New System.Windows.Forms.TextBox()
        Me.otbCshTnf = New System.Windows.Forms.TextBox()
        Me.otbExpense = New System.Windows.Forms.TextBox()
        Me.olaCshTnf = New System.Windows.Forms.Label()
        Me.olaExpense = New System.Windows.Forms.Label()
        Me.olaFTPType = New System.Windows.Forms.Label()
        Me.ocbFTPType = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'opnLine1
        '
        Me.opnLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.opnLine1.Location = New System.Drawing.Point(13, 34)
        Me.opnLine1.Name = "opnLine1"
        Me.opnLine1.Size = New System.Drawing.Size(470, 1)
        Me.opnLine1.TabIndex = 0
        '
        'olaHost
        '
        Me.olaHost.AutoSize = True
        Me.olaHost.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaHost.Location = New System.Drawing.Point(21, 44)
        Me.olaHost.Name = "olaHost"
        Me.olaHost.Size = New System.Drawing.Size(76, 16)
        Me.olaHost.TabIndex = 1
        Me.olaHost.Tag = "2;ชื่อโฮสต์;Host Name"
        Me.olaHost.Text = "Host Name"
        '
        'otbHostName
        '
        Me.otbHostName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbHostName.Location = New System.Drawing.Point(159, 42)
        Me.otbHostName.Name = "otbHostName"
        Me.otbHostName.Size = New System.Drawing.Size(306, 22)
        Me.otbHostName.TabIndex = 2
        '
        'otbUsrName
        '
        Me.otbUsrName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbUsrName.Location = New System.Drawing.Point(159, 80)
        Me.otbUsrName.Name = "otbUsrName"
        Me.otbUsrName.Size = New System.Drawing.Size(306, 22)
        Me.otbUsrName.TabIndex = 3
        '
        'otbPwd
        '
        Me.otbPwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbPwd.Location = New System.Drawing.Point(159, 118)
        Me.otbPwd.Name = "otbPwd"
        Me.otbPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.otbPwd.Size = New System.Drawing.Size(306, 22)
        Me.otbPwd.TabIndex = 4
        '
        'otbPortNum
        '
        Me.otbPortNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbPortNum.Location = New System.Drawing.Point(159, 156)
        Me.otbPortNum.Name = "otbPortNum"
        Me.otbPortNum.Size = New System.Drawing.Size(306, 22)
        Me.otbPortNum.TabIndex = 5
        '
        'otbInBound
        '
        Me.otbInBound.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbInBound.Location = New System.Drawing.Point(159, 272)
        Me.otbInBound.Name = "otbInBound"
        Me.otbInBound.Size = New System.Drawing.Size(306, 22)
        Me.otbInBound.TabIndex = 6
        Me.otbInBound.Visible = False
        '
        'otbOutBound
        '
        Me.otbOutBound.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbOutBound.Location = New System.Drawing.Point(159, 319)
        Me.otbOutBound.Name = "otbOutBound"
        Me.otbOutBound.Size = New System.Drawing.Size(306, 22)
        Me.otbOutBound.TabIndex = 7
        Me.otbOutBound.Visible = False
        '
        'olaUsrName
        '
        Me.olaUsrName.AutoSize = True
        Me.olaUsrName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaUsrName.Location = New System.Drawing.Point(21, 83)
        Me.olaUsrName.Name = "olaUsrName"
        Me.olaUsrName.Size = New System.Drawing.Size(77, 16)
        Me.olaUsrName.TabIndex = 8
        Me.olaUsrName.Tag = "2;ชื่อผู้ใช้;User Name"
        Me.olaUsrName.Text = "User Name"
        '
        'olaPwd
        '
        Me.olaPwd.AutoSize = True
        Me.olaPwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaPwd.Location = New System.Drawing.Point(21, 121)
        Me.olaPwd.Name = "olaPwd"
        Me.olaPwd.Size = New System.Drawing.Size(68, 16)
        Me.olaPwd.TabIndex = 9
        Me.olaPwd.Tag = "2;รหัสผ่าน;Password"
        Me.olaPwd.Text = "Password"
        '
        'olaPort
        '
        Me.olaPort.AutoSize = True
        Me.olaPort.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaPort.Location = New System.Drawing.Point(21, 159)
        Me.olaPort.Name = "olaPort"
        Me.olaPort.Size = New System.Drawing.Size(83, 16)
        Me.olaPort.TabIndex = 10
        Me.olaPort.Tag = "2;พอร์ต;Port Number"
        Me.olaPort.Text = "Port Number"
        '
        'olaInBound
        '
        Me.olaInBound.AutoSize = True
        Me.olaInBound.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaInBound.Location = New System.Drawing.Point(21, 284)
        Me.olaInBound.Name = "olaInBound"
        Me.olaInBound.Size = New System.Drawing.Size(99, 16)
        Me.olaInBound.TabIndex = 11
        Me.olaInBound.Tag = "2;โฟลเดอร์ขาเข้า;Folder InBound"
        Me.olaInBound.Text = "Folder InBound"
        Me.olaInBound.Visible = False
        '
        'olaOutBound
        '
        Me.olaOutBound.AutoSize = True
        Me.olaOutBound.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaOutBound.Location = New System.Drawing.Point(21, 322)
        Me.olaOutBound.Name = "olaOutBound"
        Me.olaOutBound.Size = New System.Drawing.Size(109, 16)
        Me.olaOutBound.TabIndex = 12
        Me.olaOutBound.Tag = "2;โฟลเดอร์ขาออก;Folder OutBound"
        Me.olaOutBound.Text = "Folder OutBound"
        Me.olaOutBound.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(13, 186)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(470, 1)
        Me.Panel1.TabIndex = 1
        '
        'olaReCon
        '
        Me.olaReCon.AutoSize = True
        Me.olaReCon.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaReCon.Location = New System.Drawing.Point(21, 205)
        Me.olaReCon.Name = "olaReCon"
        Me.olaReCon.Size = New System.Drawing.Size(95, 16)
        Me.olaReCon.TabIndex = 14
        Me.olaReCon.Tag = "2;โฟลเดอร์ FTP Outbound;FTP Outbound"
        Me.olaReCon.Text = "FTP Outbound"
        '
        'otbReconcile
        '
        Me.otbReconcile.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbReconcile.Location = New System.Drawing.Point(159, 202)
        Me.otbReconcile.Name = "otbReconcile"
        Me.otbReconcile.Size = New System.Drawing.Size(306, 22)
        Me.otbReconcile.TabIndex = 13
        '
        'otbCshTnf
        '
        Me.otbCshTnf.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbCshTnf.Location = New System.Drawing.Point(159, 240)
        Me.otbCshTnf.Name = "otbCshTnf"
        Me.otbCshTnf.Size = New System.Drawing.Size(306, 22)
        Me.otbCshTnf.TabIndex = 15
        '
        'otbExpense
        '
        Me.otbExpense.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbExpense.Location = New System.Drawing.Point(159, 289)
        Me.otbExpense.Name = "otbExpense"
        Me.otbExpense.Size = New System.Drawing.Size(306, 22)
        Me.otbExpense.TabIndex = 16
        Me.otbExpense.Visible = False
        '
        'olaCshTnf
        '
        Me.olaCshTnf.AutoSize = True
        Me.olaCshTnf.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaCshTnf.Location = New System.Drawing.Point(21, 243)
        Me.olaCshTnf.Name = "olaCshTnf"
        Me.olaCshTnf.Size = New System.Drawing.Size(85, 16)
        Me.olaCshTnf.TabIndex = 17
        Me.olaCshTnf.Tag = "2;โฟลเดอร์  FTP Inboound;Folder FTP Inboound"
        Me.olaCshTnf.Text = "FTP Inbound"
        '
        'olaExpense
        '
        Me.olaExpense.AutoSize = True
        Me.olaExpense.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaExpense.Location = New System.Drawing.Point(21, 292)
        Me.olaExpense.Name = "olaExpense"
        Me.olaExpense.Size = New System.Drawing.Size(103, 16)
        Me.olaExpense.TabIndex = 18
        Me.olaExpense.Tag = "2;โฟลเดอร์ค่าใช้จ่าย;Folder Expense"
        Me.olaExpense.Text = "Folder Expense"
        Me.olaExpense.Visible = False
        '
        'olaFTPType
        '
        Me.olaFTPType.AutoSize = True
        Me.olaFTPType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaFTPType.Location = New System.Drawing.Point(21, 2)
        Me.olaFTPType.Name = "olaFTPType"
        Me.olaFTPType.Size = New System.Drawing.Size(69, 16)
        Me.olaFTPType.TabIndex = 19
        Me.olaFTPType.Tag = "2;ประเภท FTP;Type FTP"
        Me.olaFTPType.Text = "Type FTP"
        '
        'ocbFTPType
        '
        Me.ocbFTPType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ocbFTPType.FormattingEnabled = True
        Me.ocbFTPType.Items.AddRange(New Object() {"FTP", "sFTP"})
        Me.ocbFTPType.Location = New System.Drawing.Point(159, 2)
        Me.ocbFTPType.Name = "ocbFTPType"
        Me.ocbFTPType.Size = New System.Drawing.Size(306, 24)
        Me.ocbFTPType.TabIndex = 20
        '
        'cUsrFTP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ocbFTPType)
        Me.Controls.Add(Me.olaFTPType)
        Me.Controls.Add(Me.olaExpense)
        Me.Controls.Add(Me.olaCshTnf)
        Me.Controls.Add(Me.otbExpense)
        Me.Controls.Add(Me.otbCshTnf)
        Me.Controls.Add(Me.olaReCon)
        Me.Controls.Add(Me.otbReconcile)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.olaOutBound)
        Me.Controls.Add(Me.olaInBound)
        Me.Controls.Add(Me.olaPort)
        Me.Controls.Add(Me.olaPwd)
        Me.Controls.Add(Me.olaUsrName)
        Me.Controls.Add(Me.otbOutBound)
        Me.Controls.Add(Me.otbInBound)
        Me.Controls.Add(Me.otbPortNum)
        Me.Controls.Add(Me.otbPwd)
        Me.Controls.Add(Me.otbUsrName)
        Me.Controls.Add(Me.otbHostName)
        Me.Controls.Add(Me.olaHost)
        Me.Controls.Add(Me.opnLine1)
        Me.Name = "cUsrFTP"
        Me.Size = New System.Drawing.Size(486, 344)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents opnLine1 As Panel
    Friend WithEvents olaHost As Label
    Friend WithEvents otbHostName As TextBox
    Friend WithEvents otbUsrName As TextBox
    Friend WithEvents otbPwd As TextBox
    Friend WithEvents otbPortNum As TextBox
    Friend WithEvents otbInBound As TextBox
    Friend WithEvents otbOutBound As TextBox
    Friend WithEvents olaUsrName As Label
    Friend WithEvents olaPwd As Label
    Friend WithEvents olaPort As Label
    Friend WithEvents olaInBound As Label
    Friend WithEvents olaOutBound As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents olaReCon As Label
    Friend WithEvents otbReconcile As TextBox
    Friend WithEvents otbCshTnf As TextBox
    Friend WithEvents otbExpense As TextBox
    Friend WithEvents olaCshTnf As Label
    Friend WithEvents olaExpense As Label
    Friend WithEvents olaFTPType As Label
    Friend WithEvents ocbFTPType As ComboBox
End Class
