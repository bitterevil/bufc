﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cUsrMapping
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cUsrMapping))
        Me.opnRoot = New System.Windows.Forms.TableLayoutPanel()
        Me.ogdMapping = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ogbForm = New System.Windows.Forms.GroupBox()
        Me.otbOth = New System.Windows.Forms.TextBox()
        Me.orbSeparator4 = New System.Windows.Forms.RadioButton()
        Me.orbSeparator3 = New System.Windows.Forms.RadioButton()
        Me.orbSeparator2 = New System.Windows.Forms.RadioButton()
        Me.orbSeparator1 = New System.Windows.Forms.RadioButton()
        Me.orbSeparator0 = New System.Windows.Forms.RadioButton()
        Me.ofdForm = New System.Windows.Forms.FolderBrowserDialog()
        Me.opnRoot.SuspendLayout()
        CType(Me.ogdMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ogbForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnRoot
        '
        Me.opnRoot.BackColor = System.Drawing.Color.Transparent
        Me.opnRoot.ColumnCount = 2
        Me.opnRoot.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.opnRoot.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.opnRoot.Controls.Add(Me.ogdMapping, 1, 1)
        Me.opnRoot.Controls.Add(Me.ogbForm, 1, 0)
        Me.opnRoot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnRoot.Location = New System.Drawing.Point(0, 0)
        Me.opnRoot.Name = "opnRoot"
        Me.opnRoot.RowCount = 2
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.opnRoot.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 174.0!))
        Me.opnRoot.Size = New System.Drawing.Size(486, 327)
        Me.opnRoot.TabIndex = 0
        '
        'ogdMapping
        '
        Me.ogdMapping.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdMapping.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdMapping.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None
        Me.ogdMapping.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.ogdMapping.AutoGenerateColumns = False
        Me.ogdMapping.BackColor = System.Drawing.Color.White
        Me.ogdMapping.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdMapping.ColumnInfo = resources.GetString("ogdMapping.ColumnInfo")
        Me.ogdMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdMapping.ExtendLastCol = True
        Me.ogdMapping.HighLight = C1.Win.C1FlexGrid.HighLightEnum.WithFocus
        Me.ogdMapping.Location = New System.Drawing.Point(13, 58)
        Me.ogdMapping.Name = "ogdMapping"
        Me.ogdMapping.Rows.Count = 2
        Me.ogdMapping.Rows.DefaultSize = 21
        Me.ogdMapping.Rows.MaxSize = 21
        Me.ogdMapping.Rows.MinSize = 21
        Me.ogdMapping.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdMapping.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.ogdMapping.Size = New System.Drawing.Size(470, 266)
        Me.ogdMapping.StyleInfo = resources.GetString("ogdMapping.StyleInfo")
        Me.ogdMapping.TabIndex = 5
        Me.ogdMapping.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ogbForm
        '
        Me.ogbForm.BackColor = System.Drawing.Color.Transparent
        Me.ogbForm.Controls.Add(Me.otbOth)
        Me.ogbForm.Controls.Add(Me.orbSeparator4)
        Me.ogbForm.Controls.Add(Me.orbSeparator3)
        Me.ogbForm.Controls.Add(Me.orbSeparator2)
        Me.ogbForm.Controls.Add(Me.orbSeparator1)
        Me.ogbForm.Controls.Add(Me.orbSeparator0)
        Me.ogbForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogbForm.Location = New System.Drawing.Point(13, 3)
        Me.ogbForm.Name = "ogbForm"
        Me.ogbForm.Size = New System.Drawing.Size(470, 49)
        Me.ogbForm.TabIndex = 0
        Me.ogbForm.TabStop = False
        Me.ogbForm.Text = "Separator"
        '
        'otbOth
        '
        Me.otbOth.BackColor = System.Drawing.Color.NavajoWhite
        Me.otbOth.Location = New System.Drawing.Point(400, 20)
        Me.otbOth.MaxLength = 3
        Me.otbOth.Name = "otbOth"
        Me.otbOth.ReadOnly = True
        Me.otbOth.Size = New System.Drawing.Size(60, 22)
        Me.otbOth.TabIndex = 5
        Me.otbOth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'orbSeparator4
        '
        Me.orbSeparator4.AutoSize = True
        Me.orbSeparator4.Checked = True
        Me.orbSeparator4.Location = New System.Drawing.Point(340, 22)
        Me.orbSeparator4.Name = "orbSeparator4"
        Me.orbSeparator4.Size = New System.Drawing.Size(58, 20)
        Me.orbSeparator4.TabIndex = 4
        Me.orbSeparator4.TabStop = True
        Me.orbSeparator4.Text = "Other"
        Me.orbSeparator4.UseVisualStyleBackColor = True
        '
        'orbSeparator3
        '
        Me.orbSeparator3.AutoSize = True
        Me.orbSeparator3.Location = New System.Drawing.Point(227, 21)
        Me.orbSeparator3.Name = "orbSeparator3"
        Me.orbSeparator3.Size = New System.Drawing.Size(107, 20)
        Me.orbSeparator3.TabIndex = 3
        Me.orbSeparator3.Text = "Semi-Colon(;)"
        Me.orbSeparator3.UseVisualStyleBackColor = True
        '
        'orbSeparator2
        '
        Me.orbSeparator2.AutoSize = True
        Me.orbSeparator2.Location = New System.Drawing.Point(170, 21)
        Me.orbSeparator2.Name = "orbSeparator2"
        Me.orbSeparator2.Size = New System.Drawing.Size(51, 20)
        Me.orbSeparator2.TabIndex = 2
        Me.orbSeparator2.Text = "Tab"
        Me.orbSeparator2.UseVisualStyleBackColor = True
        '
        'orbSeparator1
        '
        Me.orbSeparator1.AutoSize = True
        Me.orbSeparator1.Location = New System.Drawing.Point(99, 21)
        Me.orbSeparator1.Name = "orbSeparator1"
        Me.orbSeparator1.Size = New System.Drawing.Size(65, 20)
        Me.orbSeparator1.TabIndex = 1
        Me.orbSeparator1.Text = "Pipe(|)"
        Me.orbSeparator1.UseVisualStyleBackColor = True
        '
        'orbSeparator0
        '
        Me.orbSeparator0.AutoSize = True
        Me.orbSeparator0.Location = New System.Drawing.Point(9, 21)
        Me.orbSeparator0.Name = "orbSeparator0"
        Me.orbSeparator0.Size = New System.Drawing.Size(84, 20)
        Me.orbSeparator0.TabIndex = 0
        Me.orbSeparator0.Text = "Comma(,)"
        Me.orbSeparator0.UseVisualStyleBackColor = True
        '
        'cUsrMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnRoot)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "cUsrMapping"
        Me.Size = New System.Drawing.Size(486, 327)
        Me.opnRoot.ResumeLayout(False)
        CType(Me.ogdMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ogbForm.ResumeLayout(False)
        Me.ogbForm.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents opnRoot As System.Windows.Forms.TableLayoutPanel
    Protected Friend WithEvents ogbForm As System.Windows.Forms.GroupBox
    Friend WithEvents ofdForm As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents otbOth As System.Windows.Forms.TextBox
    Friend WithEvents orbSeparator4 As System.Windows.Forms.RadioButton
    Friend WithEvents orbSeparator3 As System.Windows.Forms.RadioButton
    Friend WithEvents orbSeparator2 As System.Windows.Forms.RadioButton
    Friend WithEvents orbSeparator1 As System.Windows.Forms.RadioButton
    Friend WithEvents orbSeparator0 As System.Windows.Forms.RadioButton
    Friend WithEvents ogdMapping As C1.Win.C1FlexGrid.C1FlexGrid

End Class
