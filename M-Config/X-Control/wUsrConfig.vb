﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Public Class wUsrConfig
    Dim tPath As String
    Dim tSelect As String = ""
    <DllImport("mpr.dll")> _
    Public Shared Function WNetGetConnection(ByVal lpLocalName As String, ByVal lpRemoteName As StringBuilder, ByRef lpnLength As Integer) As Integer
    End Function
    Sub New()
        InitializeComponent()
    End Sub
    Private Sub wUsrConfig_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim tNameDrives(IO.DriveInfo.GetDrives().Length) As String
        Dim i As Integer = 0

        'Then to use it in a method:
        '  Dim tValue As String = My.Computer.FileSystem.SpecialDirectories.Desktop
        otvDir.Nodes.Clear()


        For Each drv In IO.DriveInfo.GetDrives()
            Dim UncPath As New StringBuilder(255)
            WNetGetConnection(drv.Name.Replace("\", ""), UncPath, UncPath.Capacity)
            ' MessageBox.Show(drv.VolumeLabel & " maps to " & UncPath.ToString)
            If drv.IsReady Then
                tNameDrives(i) = drv.VolumeLabel
                i = i + 1
            End If
        Next


        Dim tDrives As String() = Environment.GetLogicalDrives()
        ' tDrives(6) = tValue.ToString
        Dim myPC As TreeNode = otvDir.Nodes.Add("MYPC", "My Computer")

        i = 0
        For Each drive As String In tDrives
            Dim oTmpNode As TreeNode = myPC.Nodes.Add(drive, tNameDrives(i) & _
                                                      "  (" & Microsoft.VisualBasic.Left(drive, 1) & ")")

            Try
                If (Directory.GetDirectories(drive).Length <> 0) Then
                    oTmpNode.Nodes.Add("[EMPTY]", "")
                End If
            Catch ex As Exception
                Continue For
            End Try
            i = i + 1

        Next
    End Sub
    Private Sub DirectoryTree_BeforeExpand(sender As System.Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles otvDir.BeforeExpand
        Dim oTmpNode As TreeNode()
        Dim oNewNode As TreeNode

        oTmpNode = e.Node.Nodes.Find("[EMPTY]", False)

        If oTmpNode.Length > 0 Then e.Node.Nodes.Clear()

        If (Directory.Exists(e.Node.Name)) Then

            For Each dir As String In Directory.GetDirectories(e.Node.Name)

                oTmpNode = e.Node.Nodes.Find(dir, False)

                If oTmpNode.Length = 0 Then

                    oNewNode = e.Node.Nodes.Add(dir, dir)
                    '  oNewNode.Checked = e.Node.Checked
                    oNewNode.Tag = oNewNode.Checked
                    oNewNode.ImageIndex = 0


                    Try
                        If (Directory.GetDirectories(dir).Length <> 0) Then oNewNode.Nodes.Add("[EMPTY]", "")
                    Catch
                        Continue For
                    End Try
                    oNewNode = Nothing
                End If
            Next
            'For Each file As String In Directory.GetFiles(e.Node.Text)
            '    oNewNode = e.Node.Nodes.Add(file, file)
            '    oNewNode.Tag = oNewNode.Checked
            '    oNewNode.ImageIndex = 1
            'Next

        End If
    End Sub

    Private Sub otvDir_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles otvDir.AfterSelect
        tSelect = e.Node.FullPath ' คือ path ที่ถูก select
    End Sub

    Private Sub ocmCancel_Click(sender As Object, e As EventArgs) Handles ocmCancel.Click
        Me.Close()
    End Sub

    Private Sub ocmOk_Click(sender As Object, e As EventArgs) Handles ocmOk.Click
        If tSelect <> "" Then
            'cCNVB.tVB_PathName = tSelect
        End If
    End Sub
End Class