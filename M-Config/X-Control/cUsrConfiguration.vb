﻿Imports System.Data.SqlClient
Imports System.IO

Public Class cUsrConfiguration
    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        If File.Exists(Application.StartupPath & "\Config\AdaConfig.xml") = True Then

            Try
                'Get Purge day '*CH 20-01-2015
                Dim tSql As String = "SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FTPurge FROM TLNKMapping WHERE FTLNMCode='PURGE'"
                Dim oDatabase As New cDatabaseLocal
                Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql)
                If oDbTbl.Rows.Count > 0 Then
                    Me.otbPurge.Enabled = True
                    Me.otbPurge.Text = oDbTbl.Rows(0)("FTPurge")
                    cCNVB.tVB_PurgeDay = Me.otbPurge.Text
                Else
                    Me.otbPurge.Enabled = False
                End If


            Catch ex As Exception

            End Try

        End If
        ' Add any initialization after the InitializeComponent() call.
        Me.Dock = DockStyle.Fill
    End Sub

    Public Property W_DATtPathOutbox() As String
        Get
            Return Me.otbOutbox.Text
        End Get
        Set(ByVal value As String)
            Me.otbOutbox.Text = value
        End Set
    End Property

    Public Property W_DATtPathInbox() As String
        Get
            Return Me.otbInbox.Text
        End Get
        Set(ByVal value As String)
            Me.otbInbox.Text = value
        End Set
    End Property

    Public Property W_DATtPathLog() As String
        Get
            Return Me.otbLog.Text
        End Get
        Set(ByVal value As String)
            Me.otbLog.Text = value
        End Set
    End Property

    Public Property W_DATtPathBackup() As String
        Get
            Return Me.otbBackup.Text
        End Get
        Set(ByVal value As String)
            Me.otbBackup.Text = value
        End Set
    End Property

    Private Sub ocmBrwExp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmBrwInbox.Click

        If cCNVB.tVB_PathInBox <> "" Then
            cCNVB.bVB_Chk = cApp.CALxCheckSharedDirectory(cCNVB.tVB_PathInBox)
            If cCNVB.bVB_Chk = True Then
                ofdForm.SelectedPath = cCNVB.tVB_PathInBox
                ofdForm.ShowDialog()
                otbInbox.Text = ofdForm.SelectedPath
            ElseIf cCNVB.tVB_PathInBox <> "" Then
                Call cApp.CALxCheckDirectory(cCNVB.tVB_PathInBox)
                ofdForm.SelectedPath = cCNVB.tVB_PathInBox
                ofdForm.ShowDialog()
                otbInbox.Text = ofdForm.SelectedPath
            Else
                otbInbox.Focus()
            End If


        Else
            Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
            Me.ofdForm.Description = "Inbox Location..."
            Me.ofdForm.ShowDialog()
            If Me.ofdForm.SelectedPath <> "" Then
                Me.otbInbox.Text = Me.ofdForm.SelectedPath
            End If

        End If

        'Old Comment
        'Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
        'Me.ofdForm.Description = "Inbox Location..."
        'Me.ofdForm.ShowDialog()
        'If Me.ofdForm.SelectedPath <> "" Then
        '    Me.otbInbox.Text = Me.ofdForm.SelectedPath
        'End If

        'wUsrConfig.ShowDialog()
        ' Me.otbInbox.Text = cCNVB.tVB_PathName
    End Sub

    Private Sub ocmBrwLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmBrwLog.Click

        If cCNVB.tVB_PathLog <> "" Then
            cCNVB.bVB_Chk = cApp.CALxCheckSharedDirectory(cCNVB.tVB_PathLog)
            If cCNVB.bVB_Chk = True Then
                ofdForm.SelectedPath = cCNVB.tVB_PathLog
                ofdForm.ShowDialog()
                otbLog.Text = ofdForm.SelectedPath
            ElseIf cCNVB.tVB_PathLog <> "" Then
                Call cApp.CALxCheckDirectory(cCNVB.tVB_PathLog)
                ofdForm.SelectedPath = cCNVB.tVB_PathLog
                ofdForm.ShowDialog()
                otbLog.Text = ofdForm.SelectedPath
            Else
                otbLog.Focus()
            End If
        Else
            Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
            Me.ofdForm.Description = "Log Location..."
            Me.ofdForm.ShowDialog()
            If Me.ofdForm.SelectedPath <> "" Then
                Me.otbLog.Text = Me.ofdForm.SelectedPath
            End If

        End If
        '--------------------------------------------------------------------------------
        'Old comment 
        'Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
        'Me.ofdForm.Description = "Log Location..."
        'Me.ofdForm.ShowDialog()
        'If Me.ofdForm.SelectedPath <> "" Then
        '    Me.otbLog.Text = Me.ofdForm.SelectedPath
        'End If
    End Sub

    Private Sub ocmBrwBackup_Click(sender As System.Object, e As System.EventArgs) Handles ocmBrwBackup.Click

        If cCNVB.tVB_PathBackUp <> "" Then
            cCNVB.bVB_Chk = cApp.CALxCheckSharedDirectory(cCNVB.tVB_PathBackUp)
            If cCNVB.bVB_Chk = True Then
                ofdForm.SelectedPath = cCNVB.tVB_PathBackUp
                ofdForm.ShowDialog()
                otbBackup.Text = ofdForm.SelectedPath
            ElseIf cCNVB.tVB_PathBackUp <> "" Then
                Call cApp.CALxCheckDirectory(cCNVB.tVB_PathBackUp)
                ofdForm.SelectedPath = cCNVB.tVB_PathBackUp
                ofdForm.ShowDialog()
                otbBackup.Text = ofdForm.SelectedPath
            Else
                otbBackup.Focus()
            End If
        Else
            Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
            Me.ofdForm.Description = "Backup Location..."
            Me.ofdForm.ShowDialog()
            If Me.ofdForm.SelectedPath <> "" Then
                Me.otbBackup.Text = Me.ofdForm.SelectedPath
            End If

        End If
        'Old comment --------------------------------------------------------------------------
        'Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
        'Me.ofdForm.Description = "Backup Location..."
        'Me.ofdForm.ShowDialog()
        'If Me.ofdForm.SelectedPath <> "" Then
        '    Me.otbBackup.Text = Me.ofdForm.SelectedPath
        'End If
    End Sub

    Private Sub ocmBrwOutbox_Click(sender As System.Object, e As System.EventArgs) Handles ocmBrwOutbox.Click
        If cCNVB.tVB_PathOutBox <> "" Then
            cCNVB.bVB_Chk = cApp.CALxCheckSharedDirectory(cCNVB.tVB_PathOutBox)
            If cCNVB.bVB_Chk = True Then
                ofdForm.SelectedPath = cCNVB.tVB_PathOutBox
                ofdForm.ShowDialog()
                otbOutbox.Text = ofdForm.SelectedPath
            ElseIf cCNVB.tVB_PathOutBox <> "" Then
                Call cApp.CALxCheckDirectory(cCNVB.tVB_PathOutBox)
                ofdForm.SelectedPath = cCNVB.tVB_PathOutBox
                ofdForm.ShowDialog()
                otbOutbox.Text = ofdForm.SelectedPath
            Else
                otbOutbox.Focus()
            End If

        Else
            Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
            Me.ofdForm.Description = "Outbox Location..."
            Me.ofdForm.ShowDialog()
            If Me.ofdForm.SelectedPath <> "" Then
                Me.otbOutbox.Text = Me.ofdForm.SelectedPath
            End If
        End If
        'Old Comment --------------------------------------------
        'Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
        'Me.ofdForm.Description = "Outbox Location..."
        'Me.ofdForm.ShowDialog()
        'If Me.ofdForm.SelectedPath <> "" Then
        '    Me.otbOutbox.Text = Me.ofdForm.SelectedPath
        'End If
    End Sub

    Private Sub C_SETxPurgeDay(sender As System.Object, e As System.EventArgs) Handles otbPurge.TextChanged
        Dim nRec As Integer
        Dim tRead As String
        If IsNumeric(Me.otbPurge.Text) Then
            nRec = CInt(Me.otbPurge.Text)
            Select Case nRec
                Case Is > 999
                    Me.otbPurge.Text = "999"
                    Me.otbPurge.SelectionStart = Me.otbPurge.Text.Length
                Case Is < 1
                    Me.otbPurge.Text = "1"
                    Me.otbPurge.SelectionStart = Me.otbPurge.Text.Length
                Case Else
                    Me.otbPurge.Text = nRec
            End Select
            cCNVB.tVB_PurgeDay = Me.otbPurge.Text
        Else
            If Me.otbPurge.Text.Length < 1 Then
                Me.otbPurge.Text = "1"
            End If

            For nRead = 0 To Me.otbPurge.Text.Length - 1
                tRead = Mid(Me.otbPurge.Text, nRead + 1, 1)
                If Not IsNumeric(tRead) Then Me.otbPurge.Text = Replace(Me.otbPurge.Text, tRead, "")
            Next
            Me.otbPurge.SelectionStart = Me.otbPurge.Text.Length
        End If
    End Sub
    '------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 'COMMENT
    'Private Sub otbInbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles otbInbox.KeyPress
    '    cCNVB.tVB_PathInBox = otbInbox.Text
    '    If Asc(e.KeyChar) = 13 Then
    '        cCNVB.bVB_Chk = cApp.QuickBestGuessAboutAccessibilityOfNetworkPath(cCNVB.tVB_PathInBox)
    '        If cCNVB.bVB_Chk = True Then
    '            ofdForm2.SelectedPath = cCNVB.tVB_PathInBox
    '            'ofdForm2.ShowDialog()
    '            otbInbox.Text = ofdForm2.SelectedPath
    '            'Process.Start(FolderBrowserDialog1.SelectedPath)
    '        ElseIf cCNVB.tVB_PathInBox <> "" Then
    '            Call cApp.Main(cCNVB.tVB_PathInBox)
    '            ofdForm2.SelectedPath = cCNVB.tVB_PathInBox
    '            'ofdForm2.ShowDialog()
    '            otbInbox.Text = ofdForm2.SelectedPath
    '        Else
    '            otbInbox.Focus()
    '        End If

    '    End If

    'End Sub

    'Private Sub otbBackup_KeyPress(sender As Object, e As KeyPressEventArgs) Handles otbBackup.KeyPress
    '    cCNVB.tVB_PathBackUp = otbBackup.Text
    '    If Asc(e.KeyChar) = 13 Then
    '        cCNVB.bVB_Chk = cApp.QuickBestGuessAboutAccessibilityOfNetworkPath(cCNVB.tVB_PathBackUp)
    '        If cCNVB.bVB_Chk = True Then

    '            ofdForm2.SelectedPath = cCNVB.tVB_PathBackUp
    '            'ofdForm2.ShowDialog()
    '            otbBackup.Text = ofdForm2.SelectedPath
    '            'Process.Start(FolderBrowserDialog1.SelectedPath)
    '        ElseIf cCNVB.tVB_PathBackUp <> "" Then
    '            Call cApp.Main(cCNVB.tVB_PathBackUp)
    '            ofdForm2.SelectedPath = cCNVB.tVB_PathBackUp
    '            'ofdForm2.ShowDialog()
    '            otbBackup.Text = ofdForm2.SelectedPath
    '        Else
    '            otbBackup.Focus()
    '        End If

    '    End If

    'End Sub
    'Private Sub otbLog_KeyPress(sender As Object, e As KeyPressEventArgs) Handles otbLog.KeyPress
    '    cCNVB.tVB_PathLog = otbLog.Text
    '    If Asc(e.KeyChar) = 13 Then
    '        cCNVB.bVB_Chk = cApp.QuickBestGuessAboutAccessibilityOfNetworkPath(cCNVB.tVB_PathLog)
    '        If cCNVB.bVB_Chk = True Then

    '            ofdForm2.SelectedPath = cCNVB.tVB_PathLog
    '            ' ofdForm2.ShowDialog()
    '            otbLog.Text = ofdForm2.SelectedPath
    '            'Process.Start(FolderBrowserDialog1.SelectedPath)
    '        ElseIf cCNVB.tVB_PathLog <> "" Then
    '            Call cApp.Main(cCNVB.tVB_PathLog)
    '            ofdForm2.SelectedPath = cCNVB.tVB_PathLog
    '            ' ofdForm2.ShowDialog()
    '            otbLog.Text = ofdForm2.SelectedPath
    '        Else
    '            otbLog.Focus()
    '        End If

    '    End If

    'End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    'Private Sub ocmBrwUsr_Click(sender As System.Object, e As System.EventArgs)
    '    If AdaConfig.Config.W_CHKbSQLConnect = False Then
    '        cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
    '        Exit Sub
    '    End If

    '    Try
    '        Dim oDataTemp = New DataTable
    '        oDataTemp.TableName = "TSysUser"
    '        Dim tSql As String = "SELECT FTUsrCode,FTUsrName,FTDptCode FROM TSysUser"
    '        Using oSqlDA As New SqlDataAdapter(tSql, App.SQLConnectionString)
    '            oSqlDA.Fill(oDataTemp)
    '        End Using

    '        Dim aUser As New Dictionary(Of String, String)

    '        For Each oDataRow As DataRow In oDataTemp.Rows
    '            aUser.Add(oDataRow("FTUsrCode"), oDataRow("FTDptCode"))
    '        Next
    '        oDataTemp.Columns.Remove("FTDptCode")

    '        Dim oFrmBrw As New wBrwFilter
    '        With oFrmBrw
    '            .Tag = "2;ผู้ใช้งาน;User"
    '            .oW_Datatable = oDataTemp
    '            .ShowDialog()
    '            If .DialogResult = Windows.Forms.DialogResult.OK Then
    '                Me.otbUsr.Text = .tW_Result
    '                Me.otbUsr.Tag = aUser.Where(Function(c) c.Key = .tW_Result).FirstOrDefault.Value
    '            End If
    '            oDataTemp.Dispose()
    '            .Dispose()
    '        End With
    '    Catch ex As Exception
    '        cCNSP.SP_MSGnShowing(ex.Message, cCNEN.eEN_MSGStyle.nEN_MSGErr, True)
    '    End Try

    'End Sub

    
    '-------------------------------------------------------------------------------------------------------------------   'Comment

    'Private Sub otbOutbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles otbOutbox.KeyPress
    '    cCNVB.tVB_PathOutBox = otbOutbox.Text
    '    If Asc(e.KeyChar) = 13 Then
    '        cCNVB.bVB_Chk = cApp.QuickBestGuessAboutAccessibilityOfNetworkPath(cCNVB.tVB_PathOutBox)
    '        If cCNVB.bVB_Chk = True Then

    '            ofdForm2.SelectedPath = cCNVB.tVB_PathOutBox
    '            'ofdForm2.ShowDialog()
    '            otbOutbox.Text = ofdForm2.SelectedPath
    '            'Process.Start(FolderBrowserDialog1.SelectedPath)
    '        ElseIf cCNVB.tVB_PathOutBox <> "" Then
    '            Call cApp.Main(cCNVB.tVB_PathOutBox)
    '            ofdForm2.SelectedPath = cCNVB.tVB_PathOutBox
    '            ' ofdForm2.ShowDialog()
    '            otbOutbox.Text = ofdForm2.SelectedPath
    '        Else
    '            otbOutbox.Focus()
    '        End If


    '    End If

    'End Sub

    Private Sub otbInbox_TextChanged(sender As Object, e As EventArgs) Handles otbInbox.TextChanged
        cCNVB.tVB_PathInBox = otbInbox.Text
    End Sub
    Private Sub otbOutbox_TextChanged(sender As Object, e As EventArgs) Handles otbOutbox.TextChanged
        cCNVB.tVB_PathOutBox = otbOutbox.Text
    End Sub
    Private Sub otbLog_TextChanged(sender As Object, e As EventArgs) Handles otbLog.TextChanged
        cCNVB.tVB_PathLog = otbLog.Text
    End Sub

    Private Sub otbBackup_TextChanged(sender As Object, e As EventArgs) Handles otbBackup.TextChanged
        cCNVB.tVB_PathBackUp = otbBackup.Text
    End Sub

    Private Sub ofdForm_HelpRequest(sender As Object, e As EventArgs) Handles ofdForm.HelpRequest

    End Sub

    Private Sub opnRoot_Paint(sender As Object, e As PaintEventArgs) Handles opnRoot.Paint

    End Sub

    Private Sub olaInbox_Click(sender As Object, e As EventArgs) Handles olaInbox.Click

    End Sub

    Private Sub olaDay_Click(sender As Object, e As EventArgs) Handles olaDay.Click

    End Sub

    Private Sub opnExportLayout_Paint(sender As Object, e As PaintEventArgs) Handles opnExportLayout.Paint

    End Sub

    Private Sub ogbExport_Enter(sender As Object, e As EventArgs) Handles ogbExport.Enter

    End Sub

    Private Sub olaOutbox_Click(sender As Object, e As EventArgs) Handles olaOutbox.Click

    End Sub

    Private Sub ogbPurge_Enter(sender As Object, e As EventArgs) Handles ogbPurge.Enter

    End Sub

    Private Sub ogbImport_Enter(sender As Object, e As EventArgs) Handles ogbImport.Enter

    End Sub

    Private Sub olaLog_Click(sender As Object, e As EventArgs) Handles olaLog.Click

    End Sub

    Private Sub olaBackup_Click(sender As Object, e As EventArgs) Handles olaBackup.Click

    End Sub

    Private Sub opnImportLayout_Paint(sender As Object, e As PaintEventArgs) Handles opnImportLayout.Paint

    End Sub

    Private Sub olaPurge_Click(sender As Object, e As EventArgs) Handles olaPurge.Click

    End Sub

    Private Sub ofdForm2_HelpRequest(sender As Object, e As EventArgs) Handles ofdForm2.HelpRequest

    End Sub
End Class
