﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class cUsrWord
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.ogbMain = New System.Windows.Forms.GroupBox()
        Me.otbShop = New System.Windows.Forms.TextBox()
        Me.otbComp = New System.Windows.Forms.TextBox()
        Me.otbPlant = New System.Windows.Forms.TextBox()
        Me.otbLocation = New System.Windows.Forms.TextBox()
        Me.otbRecive = New System.Windows.Forms.TextBox()
        Me.olaShopCode = New System.Windows.Forms.Label()
        Me.olaCompanyCode = New System.Windows.Forms.Label()
        Me.olaRecive = New System.Windows.Forms.Label()
        Me.olaLocat = New System.Windows.Forms.Label()
        Me.olaPlan = New System.Windows.Forms.Label()
        Me.ogdTran = New System.Windows.Forms.GroupBox()
        Me.otbShopTran = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ogbTranfer = New System.Windows.Forms.GroupBox()
        Me.ocbReason = New System.Windows.Forms.ComboBox()
        Me.ocbSupply = New System.Windows.Forms.ComboBox()
        Me.olaReason = New System.Windows.Forms.Label()
        Me.olaSupply = New System.Windows.Forms.Label()
        Me.opnMain.SuspendLayout()
        Me.ogbMain.SuspendLayout()
        Me.ogdTran.SuspendLayout()
        Me.ogbTranfer.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 2
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.497942!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 96.50206!))
        Me.opnMain.Controls.Add(Me.ogbMain, 1, 1)
        Me.opnMain.Controls.Add(Me.ogdTran, 1, 0)
        Me.opnMain.Controls.Add(Me.ogbTranfer, 1, 2)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Margin = New System.Windows.Forms.Padding(4)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 3
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.38372!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77.61628!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 114.0!))
        Me.opnMain.Size = New System.Drawing.Size(648, 423)
        Me.opnMain.TabIndex = 1
        '
        'ogbMain
        '
        Me.ogbMain.Controls.Add(Me.otbShop)
        Me.ogbMain.Controls.Add(Me.otbComp)
        Me.ogbMain.Controls.Add(Me.otbPlant)
        Me.ogbMain.Controls.Add(Me.otbLocation)
        Me.ogbMain.Controls.Add(Me.otbRecive)
        Me.ogbMain.Controls.Add(Me.olaShopCode)
        Me.ogbMain.Controls.Add(Me.olaCompanyCode)
        Me.ogbMain.Controls.Add(Me.olaRecive)
        Me.ogbMain.Controls.Add(Me.olaLocat)
        Me.ogbMain.Controls.Add(Me.olaPlan)
        Me.ogbMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
        Me.ogbMain.Location = New System.Drawing.Point(26, 73)
        Me.ogbMain.Margin = New System.Windows.Forms.Padding(4)
        Me.ogbMain.Name = "ogbMain"
        Me.ogbMain.Padding = New System.Windows.Forms.Padding(4)
        Me.ogbMain.Size = New System.Drawing.Size(618, 231)
        Me.ogbMain.TabIndex = 4
        Me.ogbMain.TabStop = False
        Me.ogbMain.Text = "Config"
        '
        'otbShop
        '
        Me.otbShop.Location = New System.Drawing.Point(173, 194)
        Me.otbShop.Margin = New System.Windows.Forms.Padding(4)
        Me.otbShop.Name = "otbShop"
        Me.otbShop.Size = New System.Drawing.Size(329, 25)
        Me.otbShop.TabIndex = 9
        '
        'otbComp
        '
        Me.otbComp.Location = New System.Drawing.Point(173, 150)
        Me.otbComp.Margin = New System.Windows.Forms.Padding(4)
        Me.otbComp.Name = "otbComp"
        Me.otbComp.Size = New System.Drawing.Size(329, 25)
        Me.otbComp.TabIndex = 8
        '
        'otbPlant
        '
        Me.otbPlant.Location = New System.Drawing.Point(173, 21)
        Me.otbPlant.Margin = New System.Windows.Forms.Padding(4)
        Me.otbPlant.Name = "otbPlant"
        Me.otbPlant.Size = New System.Drawing.Size(329, 25)
        Me.otbPlant.TabIndex = 7
        '
        'otbLocation
        '
        Me.otbLocation.Location = New System.Drawing.Point(173, 64)
        Me.otbLocation.Margin = New System.Windows.Forms.Padding(4)
        Me.otbLocation.Name = "otbLocation"
        Me.otbLocation.Size = New System.Drawing.Size(329, 25)
        Me.otbLocation.TabIndex = 6
        '
        'otbRecive
        '
        Me.otbRecive.Location = New System.Drawing.Point(173, 107)
        Me.otbRecive.Margin = New System.Windows.Forms.Padding(4)
        Me.otbRecive.Name = "otbRecive"
        Me.otbRecive.Size = New System.Drawing.Size(329, 25)
        Me.otbRecive.TabIndex = 5
        '
        'olaShopCode
        '
        Me.olaShopCode.AutoSize = True
        Me.olaShopCode.Location = New System.Drawing.Point(56, 196)
        Me.olaShopCode.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.olaShopCode.Name = "olaShopCode"
        Me.olaShopCode.Size = New System.Drawing.Size(97, 20)
        Me.olaShopCode.TabIndex = 4
        Me.olaShopCode.Text = "Shop Code :"
        '
        'olaCompanyCode
        '
        Me.olaCompanyCode.AutoSize = True
        Me.olaCompanyCode.Location = New System.Drawing.Point(21, 154)
        Me.olaCompanyCode.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.olaCompanyCode.Name = "olaCompanyCode"
        Me.olaCompanyCode.Size = New System.Drawing.Size(126, 20)
        Me.olaCompanyCode.TabIndex = 3
        Me.olaCompanyCode.Text = "Company Code :"
        '
        'olaRecive
        '
        Me.olaRecive.AutoSize = True
        Me.olaRecive.Location = New System.Drawing.Point(35, 110)
        Me.olaRecive.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.olaRecive.Name = "olaRecive"
        Me.olaRecive.Size = New System.Drawing.Size(114, 20)
        Me.olaRecive.TabIndex = 2
        Me.olaRecive.Text = "Receive Plant :"
        '
        'olaLocat
        '
        Me.olaLocat.AutoSize = True
        Me.olaLocat.Location = New System.Drawing.Point(11, 68)
        Me.olaLocat.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.olaLocat.Name = "olaLocat"
        Me.olaLocat.Size = New System.Drawing.Size(139, 20)
        Me.olaLocat.TabIndex = 1
        Me.olaLocat.Text = "Storage Location :"
        '
        'olaPlan
        '
        Me.olaPlan.AutoSize = True
        Me.olaPlan.Location = New System.Drawing.Point(100, 25)
        Me.olaPlan.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.olaPlan.Name = "olaPlan"
        Me.olaPlan.Size = New System.Drawing.Size(53, 20)
        Me.olaPlan.TabIndex = 0
        Me.olaPlan.Text = "Plant :"
        '
        'ogdTran
        '
        Me.ogdTran.Controls.Add(Me.otbShopTran)
        Me.ogdTran.Controls.Add(Me.Label1)
        Me.ogdTran.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdTran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
        Me.ogdTran.Location = New System.Drawing.Point(26, 4)
        Me.ogdTran.Margin = New System.Windows.Forms.Padding(4)
        Me.ogdTran.Name = "ogdTran"
        Me.ogdTran.Padding = New System.Windows.Forms.Padding(4)
        Me.ogdTran.Size = New System.Drawing.Size(618, 61)
        Me.ogdTran.TabIndex = 5
        Me.ogdTran.TabStop = False
        Me.ogdTran.Text = "Config Filename / Header Transaction"
        '
        'otbShopTran
        '
        Me.otbShopTran.Location = New System.Drawing.Point(173, 30)
        Me.otbShopTran.Margin = New System.Windows.Forms.Padding(4)
        Me.otbShopTran.Name = "otbShopTran"
        Me.otbShopTran.Size = New System.Drawing.Size(329, 25)
        Me.otbShopTran.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(49, 33)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 20)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Shop Code :"
        '
        'ogbTranfer
        '
        Me.ogbTranfer.Controls.Add(Me.ocbReason)
        Me.ogbTranfer.Controls.Add(Me.ocbSupply)
        Me.ogbTranfer.Controls.Add(Me.olaReason)
        Me.ogbTranfer.Controls.Add(Me.olaSupply)
        Me.ogbTranfer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbTranfer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
        Me.ogbTranfer.Location = New System.Drawing.Point(25, 311)
        Me.ogbTranfer.Name = "ogbTranfer"
        Me.ogbTranfer.Size = New System.Drawing.Size(620, 109)
        Me.ogbTranfer.TabIndex = 6
        Me.ogbTranfer.TabStop = False
        Me.ogbTranfer.Text = "Default Tranfer in"
        '
        'ocbReason
        '
        Me.ocbReason.FormattingEnabled = True
        Me.ocbReason.Location = New System.Drawing.Point(174, 65)
        Me.ocbReason.Name = "ocbReason"
        Me.ocbReason.Size = New System.Drawing.Size(329, 26)
        Me.ocbReason.TabIndex = 3
        '
        'ocbSupply
        '
        Me.ocbSupply.FormattingEnabled = True
        Me.ocbSupply.Location = New System.Drawing.Point(174, 26)
        Me.ocbSupply.Name = "ocbSupply"
        Me.ocbSupply.Size = New System.Drawing.Size(329, 26)
        Me.ocbSupply.TabIndex = 2
        '
        'olaReason
        '
        Me.olaReason.AutoSize = True
        Me.olaReason.Location = New System.Drawing.Point(34, 71)
        Me.olaReason.Name = "olaReason"
        Me.olaReason.Size = New System.Drawing.Size(115, 20)
        Me.olaReason.TabIndex = 1
        Me.olaReason.Text = "Reason Code :"
        '
        'olaSupply
        '
        Me.olaSupply.AutoSize = True
        Me.olaSupply.Location = New System.Drawing.Point(33, 32)
        Me.olaSupply.Name = "olaSupply"
        Me.olaSupply.Size = New System.Drawing.Size(117, 20)
        Me.olaSupply.TabIndex = 0
        Me.olaSupply.Text = "Supplier Code :"
        '
        'cUsrWord
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnMain)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "cUsrWord"
        Me.Size = New System.Drawing.Size(648, 423)
        Me.opnMain.ResumeLayout(False)
        Me.ogbMain.ResumeLayout(False)
        Me.ogbMain.PerformLayout()
        Me.ogdTran.ResumeLayout(False)
        Me.ogdTran.PerformLayout()
        Me.ogbTranfer.ResumeLayout(False)
        Me.ogbTranfer.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents ogbMain As GroupBox
    Friend WithEvents otbShop As TextBox
    Friend WithEvents otbComp As TextBox
    Friend WithEvents otbPlant As TextBox
    Friend WithEvents otbLocation As TextBox
    Friend WithEvents otbRecive As TextBox
    Friend WithEvents olaShopCode As Label
    Friend WithEvents olaCompanyCode As Label
    Friend WithEvents olaRecive As Label
    Friend WithEvents olaLocat As Label
    Friend WithEvents olaPlan As Label
    Friend WithEvents ogdTran As GroupBox
    Friend WithEvents otbShopTran As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ogbTranfer As GroupBox
    Friend WithEvents ocbReason As ComboBox
    Friend WithEvents ocbSupply As ComboBox
    Friend WithEvents olaReason As Label
    Friend WithEvents olaSupply As Label
End Class
