﻿Imports System.Data.SqlClient
Imports System.IO

Public Class wSetting

    Property nW_IndexMenu As Integer = 0
    Property bW_Lock As Boolean = False

#Region "Initail"
    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.oW_UsrCtr = New cControls()
    End Sub
#End Region

#Region "Variable"

    Private oW_UsrCtr As cControls
    Private bW_Close As Boolean = False

#End Region

#Region "Sub And Function"

    Private Sub olvMenu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles olvMenu.SelectedIndexChanged
        For Each oItem As ListViewItem In Me.olvMenu.Items
            oItem.ForeColor = Color.Black
            oItem.BackColor = Color.White
            If oItem.Selected = True Then
                Dim nObj = CType(oItem.Index + 1, cControls.eType)
                Me.Cursor = Cursors.WaitCursor
                Me.oW_UsrCtr.W_GETxCurCtl(Me.opnForm, nObj)
                Me.Cursor = Cursors.Default
                Me.olaDesForm.Text = Me.oW_UsrCtr.W_GETtCurDes
                oItem.ForeColor = Color.White
                oItem.BackColor = Color.RoyalBlue
                Exit Sub
            End If
        Next
    End Sub

    Private Sub wSettingExport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Me.bW_Close = True Then
            Me.bW_Close = False
            e.Cancel = True
        End If
    End Sub

    Private Sub wCreateTask_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Me.bW_Lock = True Then
            Me.olvMenu.Enabled = False
        Else
            Me.olvMenu.Enabled = True
        End If

        cCNSP.SP_FrmSetCapControl(Me)
        If Me.olvMenu.Items.Count > 0 Then Me.olvMenu.Items(nW_IndexMenu).Selected = True

    End Sub
    Private Sub ocmSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmSave.Click
        If cCNSP.SP_MSGnShowing(cCNMS.tMS_CN108, cCNEN.eEN_MSGStyle.nEN_MSGYesNo) = MsgBoxResult.Yes Then
            '-------------------------------------------------------------------- CheckPath Folder True OR False   ***PAN 2016-05-16
            'Dim oFrmUsrcon As New cUsrConfiguration
            Dim bInbox As Boolean
            Dim bBackUp As Boolean
            Dim bLog As Boolean
            Dim bOutBox As Boolean
            'Check Path 
            bInbox = cApp.CALxCheckSavePath(cCNVB.tVB_PathInBox, "InBox")
            bBackUp = cApp.CALxCheckSavePath(cCNVB.tVB_PathBackUp, "BackUp")
            bLog = cApp.CALxCheckSavePath(cCNVB.tVB_PathLog, "Log")
            bOutBox = cApp.CALxCheckSavePath(cCNVB.tVB_PathOutBox, "OutBox")
            '-----------------------------------------------------------------------------------------------
            'Check Connection Db 
            If oW_UsrCtr.W_DATbChkDb = True Then

                If oW_UsrCtr.W_DATbConnChage = True Then
                    Me.DialogResult = Windows.Forms.DialogResult.Yes
                Else
                    Me.DialogResult = Windows.Forms.DialogResult.No
                End If

                'Member Database *CH 18-10-2014
                If oW_UsrCtr.W_DATbMemConnChage = True Then
                    Me.DialogResult = Windows.Forms.DialogResult.Yes
                Else
                    Me.DialogResult = Windows.Forms.DialogResult.No
                End If

                cLog.C_CALxWriteLog("wSetting > Save")

                'Save Config to Xml File
                If bInbox = True And bBackUp = True And bLog = True And bOutBox = True Then 'Check save (True All = Save)  ***PAN 2016-05-16
                    oW_UsrCtr.W_DATxSaveConfig()
                Else
                    cCNSP.SP_MSGnShowing(cCNMS.tMS_CN005, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
                End If
                '-----------------------------------------------------------------------------
                Try
                    Dim tSql As String = "UPDATE TLNKMapping SET FTLNMUsrValue='{0}' WHERE FTLNMCode='{1}'"
                    Dim oDatabase As New cDatabaseLocal
                    Dim oTLNKMapping = oW_UsrCtr.W_GEToMapping
                    oDatabase.C_CALnExecuteNonQuery(String.Format(tSql, cCNVB.tVB_PurgeDay, "PURGE"))

                    tSql &= " AND FTLNMType='{2}' AND FNLNMSeqNo={3}"
                    For Each oItem In oTLNKMapping.Rows
                        Try
                            oDatabase.C_CALnExecuteNonQuery(String.Format(tSql, oItem("FTLNMUsrValue"), oItem("FTLNMCode"), oItem("FTLNMType"), oItem("FNLNMSeqNo")))
                        Catch ex As Exception

                        End Try
                    Next
                Catch ex As Exception
                End Try

                'Reload Config
                AdaConfig.cConfig.C_SETbUpdateXml()
                AdaConfig.cConfig.C_SETbUpdateXMLUsrDefault()
                Me.bW_Close = False
                Me.Close()
                Exit Sub
            Else
                cCNSP.SP_MSGnShowing(cCNMS.tMS_CN104, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            End If
        End If

        Me.bW_Close = True

    End Sub

    Private Sub ocmClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmClose.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.bW_Close = False
        'Me.Close()
    End Sub

#End Region

End Class