﻿Imports System.Security.Cryptography
Imports C1.Win.C1FlexGrid
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Reflection
Imports System.Text
Imports AdaLinkSAPBUFC
Imports System.Net
Imports System.IO
Imports Renci.SshNet

Public Class cCNSP

#Region " Encrypt - Decrypt "

    Private Shared TripleDes As New TripleDESCryptoServiceProvider

    Private Shared Function TruncateHash( _
    ByVal ptkey As String, _
    ByVal piLength As Integer) _
    As Byte()

        Dim sha1 As New SHA1CryptoServiceProvider

        ' Hash the key.
        Dim keyBytes() As Byte = _
            System.Text.Encoding.Unicode.GetBytes(ptkey)
        Dim hash() As Byte = sha1.ComputeHash(keyBytes)

        ' Truncate or pad the hash.
        ReDim Preserve hash(piLength - 1)
        Return hash
    End Function

    Public Shared Function SP_EncryptData( _
    ByVal ptPlaintext As String, ByVal ptkey As String) _
    As String

        ' Convert the plaintext string to a byte array.
        Dim plaintextBytes() As Byte = _
            System.Text.Encoding.Unicode.GetBytes(ptPlaintext)

        ' Initialize the crypto provider.
        TripleDes.Key = TruncateHash(ptkey, TripleDes.KeySize \ 8)
        TripleDes.IV = TruncateHash("", TripleDes.BlockSize \ 8)

        ' Create the stream.
        Dim ms As New System.IO.MemoryStream
        ' Create the encoder to write to the stream.
        Dim encStream As New CryptoStream(ms, _
            TripleDes.CreateEncryptor(), _
            System.Security.Cryptography.CryptoStreamMode.Write)

        ' Use the crypto stream to write the byte array to the stream.
        encStream.Write(plaintextBytes, 0, plaintextBytes.Length)
        encStream.FlushFinalBlock()

        ' Convert the encrypted stream to a printable string.
        Return Convert.ToBase64String(ms.ToArray)
    End Function

    Public Shared Function SP_DecryptData( _
    ByVal ptEncryptedtext As String, ByVal ptkey As String) _
    As String

        ' Convert the encrypted text string to a byte array.

        '     Dim encryptedBytes() As Byte = Convert.FromBase64String(encryptedtext)

        ' Initialize the crypto provider.
        TripleDes.Key = TruncateHash(ptkey, TripleDes.KeySize \ 8)
        TripleDes.IV = TruncateHash("", TripleDes.BlockSize \ 8)

        Dim encryptedBytes() As Byte
        Try
            encryptedBytes = Convert.FromBase64String(ptEncryptedtext)
        Catch ex As Exception
            Return "Error"
        End Try

        ' Create the stream.
        Dim ms As New System.IO.MemoryStream
        ' Create the decoder to write to the stream.
        Dim decStream As New CryptoStream(ms, _
            TripleDes.CreateDecryptor(), _
            System.Security.Cryptography.CryptoStreamMode.Write)

        ' Use the crypto stream to write the byte array to the stream.
        decStream.Write(encryptedBytes, 0, encryptedBytes.Length)
        decStream.FlushFinalBlock()

        ' Convert the plaintext stream to a string.
        Return System.Text.Encoding.Unicode.GetString(ms.ToArray)

    End Function

#End Region

    Public Shared Sub SP_CNSENxSendKeysNumber(ByVal poSender As Object)
        '=======================================
        'Call : poSender = คือ Control ที่ต้องการ Click โดยนำค่า Values ใส่ใน Tag (ห้ามใส่ค่าอื่นใน Tag)
        'Cmt : ทำการ Sendkeys Number
        '=======================================

        If poSender.Tag = "" Then Exit Sub
        Dim tTemp As String
        tTemp = poSender.Tag
        SendKeys.Send("{" & tTemp & "}")
    End Sub

    Public Shared Function SP_CNKBbChkKeyAscii(ByRef pnKeyAscii As Integer, Optional ByVal pbIsDTPicker As Boolean = False) As Boolean
        '-----------------------------------------------------------------------------------------------
        '   Call:   pnKeyAscii(r) is any key ascii (by reference)
        '               pbIsDTPicker is optional if an object is DTPicker
        '   Cmt:   place this code in event KeyPress ของ Text box   เพื่อไม่รับอักขระ เขาเดี่ยว('), เขาคู่("), เส้นตั้ง(|)
        '              and place in event KeyPress of DTPicker for except vbKeyLeft ,vbKeyRight,vbKeyUp,vbKeyDown
        '-----------------------------------------------------------------------------------------------
        Dim bRet As Boolean = False

        If pbIsDTPicker Then
            If (pnKeyAscii <> Keys.Left) Or (pnKeyAscii <> Keys.Right) Or (pnKeyAscii <> Keys.Up) Or (pnKeyAscii <> Keys.Down) Then bRet = True 'left,right,up,down
        Else
            If (pnKeyAscii = 34) Or (pnKeyAscii = 39) Or (pnKeyAscii = 124) Or (pnKeyAscii = 91) Or (pnKeyAscii = 93) Then bRet = True ' [ ', ", | ]
        End If

        SP_CNKBbChkKeyAscii = bRet
    End Function

    Public Shared Function SP_MSGnShowing(ByVal ptMSG$, ByVal pnStyle As cCNEN.eEN_MSGStyle, _
                                      Optional ByVal pbNoCut As Boolean = False, Optional ByVal ptCaption$ = "", _
                                      Optional ByVal ptNextLine$ = "") As DialogResult
        '----------------------------------------------------------
        '   Call:   ptMSG is showing message
        '               pnStyle is type of button style
        '               pbNoCut is optional, True if do not want to cut message
        '               ptCaption is optional, if want to add on caption
        '               ptNextLine is optional, if want to add on message (with vbCrLf)
        '   Ret:    no. of pressed button
        '----------------------------------------------------------
        Dim tMsg$, tMsg2$
        Dim tAppTitle$
        Dim nPos%
        Dim iRet As DialogResult


        nPos = cCNVB.nVB_CutMsg

        Cursor.Current = Cursors.Default

        'caption as AppName
        If cCNVB.tVB_CNAppTitle = Nothing Or cCNVB.tVB_CNAppTitle = "" Then
            tAppTitle = Application.ProductName
        Else
            tAppTitle = cCNVB.tVB_CNAppTitle
        End If

        'add caption AppName + Add on caption
        If Trim(ptCaption) <> "" Then
            tAppTitle = tAppTitle & " : " & ptCaption
        End If

        tMsg2 = ptMSG
        If Not pbNoCut Then     'if false(default) parameter tMsg will be cut (Thai/Eng)
            tMsg2 = Split(tMsg2, ";")(nPos)
        End If

        tMsg = tMsg2

        'add next line
        If Trim(ptNextLine) <> "" Then
            tMsg = tMsg & vbCrLf & vbCrLf & Split(ptNextLine, ";")(nPos)
        End If

        Beep()

        Select Case pnStyle
            Case cCNEN.eEN_MSGStyle.nEN_MSGErr : iRet = MessageBox.Show(tMsg, tAppTitle, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Case cCNEN.eEN_MSGStyle.nEN_MSGWarn : iRet = MessageBox.Show(tMsg, tAppTitle, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Case cCNEN.eEN_MSGStyle.nEN_MSGInfo : iRet = MessageBox.Show(tMsg, tAppTitle, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Case cCNEN.eEN_MSGStyle.nEN_MSGOkCancel : iRet = MessageBox.Show(tMsg, tAppTitle, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            Case cCNEN.eEN_MSGStyle.nEN_MSGYesNo : iRet = MessageBox.Show(tMsg, tAppTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            Case cCNEN.eEN_MSGStyle.nEN_MSGYesNoCancel : iRet = MessageBox.Show(tMsg, tAppTitle, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
        End Select

        SP_MSGnShowing = iRet
    End Function

    Public Shared Sub SP_MSGShowError(Optional ByVal ptFncName$ = "")
        '----------------------------------------------------------
        '   Call:   ptFncName name of function 
        '   Cmt:   displays the error message with it's Err code
        '----------------------------------------------------------
        Dim tMsg As String = ""
        Dim tMsg2 As String = ""
        Dim tAppTitle As String = ""
        Dim nPos As Integer = 0

        Cursor.Current = Cursors.Default

        tMsg = Split(cCNMS.tMS_CN001, ";")(cCNVB.nVB_CutMsg)
        tMsg2 = Split(cCNMS.tMS_CN002, ";")(cCNVB.nVB_CutMsg)

        tMsg = tMsg & vbCrLf & vbCrLf                                                         'เว้น 1 บรรทัด
        tMsg = tMsg & tMsg2 & Err.Number & " " & ptFncName         'แสดง Err Number and Function at Call
        tMsg = tMsg & Err.Description & vbCrLf

        Beep()

        MessageBox.Show(tMsg, cCNVB.tVB_CNAppTitle, MessageBoxButtons.OK, MessageBoxIcon.Error)

    End Sub

    Public Shared Function SP_STRtToken(ByRef ptSource$, ByVal ptSep$, Optional ByVal piLoop% = 0, Optional ByVal pbRetOld As Boolean = False) As String
        '----------------------------------------------------------
        '   Call:   ptSource source string ("Com1;Com2")
        '               ptSep separator string (";")
        '               piLoop is optional if exactly know the position
        '               pbRetOld is optional if true source still the same, otherwise token out
        '   Ret:    first token from source string ("Com1")
        '               ptSource without first token ("Com2")  [depend on pbRet]
        '----------------------------------------------------------
        Dim nIndex As Integer = 0
        Dim nMaxLoop As Integer = 0
        Dim nLoop As Integer = 0
        Dim tRet As String = ""
        Dim tSrc As String = ""

        tSrc = ptSource
        If (piLoop = 0) Then
            nMaxLoop = 1
        Else
            nMaxLoop = piLoop
        End If

        For nLoop = 1 To nMaxLoop
            nIndex = InStr(UCase(tSrc), UCase(ptSep))
            If nIndex = 0 Then
                tRet = tSrc            'not found separator (n could be 1 or >1)
                tSrc = ""
            Else
                tRet = Left(tSrc, nIndex - 1)
                tSrc = Mid(tSrc, nIndex + Len(ptSep))
            End If
        Next nLoop

        If Not pbRetOld Then        'default pbRetOld = False, always token out
            ptSource = tSrc
        End If

        SP_STRtToken = tRet

    End Function

    Public Shared Sub SP_LNGxSetDefAftChg()
        '----------------------------------------------------------
        '   Call :
        '   Cmt :  set defalut after language change
        '----------------------------------------------------------
        'cut varaiable
        cCNVB.nVB_CutTag = SP_LNGnCutLng(cCNEN.eEN_CutCaption.nEN_Cut2Tag)
        cCNVB.nVB_CutMsg = SP_LNGnCutLng(cCNEN.eEN_CutCaption.nEN_Cut1Msg)
        cCNVB.nVB_CutConst = SP_LNGnCutLng(cCNEN.eEN_CutCaption.nEN_Cut3Const)

        'app title
        cCNVB.tVB_AppTitle = SP_STRtToken(cCNVB.tVB_AppProgram, ";", cCNVB.nVB_CutConst, True) & " " & cCNVB.tVB_AppVersion & " - " & SP_STRtToken(cCNCS.tCS_AppName, ";", cCNVB.nVB_CutConst, True)

    End Sub

    Public Shared Function SP_LNGnCutLng(ByVal pnCutType As Integer) As Integer        '??
        '----------------------------------------------------------
        '   Call:
        '   Cmt:    set position to cut string in SP_STRtToken, Tha=1, Eng=2
        '----------------------------------------------------------
        Dim nPos As Integer

        Select Case pnCutType
            Case cCNEN.eEN_CutCaption.nEN_Cut1Msg
                nPos = IIf(cCNVB.nVB_CutLng = 1, 0, 1)

            Case cCNEN.eEN_CutCaption.nEN_Cut2Tag
                nPos = IIf(cCNVB.nVB_CutLng = 1, 2, 3)

            Case cCNEN.eEN_CutCaption.nEN_Cut3Const
                nPos = IIf(cCNVB.nVB_CutLng = 1, 1, 2)

        End Select

        SP_LNGnCutLng = nPos
    End Function

    Public Shared Sub SP_FrmSetCapControl(ByVal pwForm As Form)
        '----------------------------------------------------------
        '   Call:   pwForm as form
        '   Cmt:   set caption of Form(Label, Button ...)
        '               show Thai or English
        '----------------------------------------------------------
        Dim oControl As Object

        Try
            'ปรับ caption ของฟอร์มหลัก
            If pwForm.Name = "wMain" Then       'general project
                If Left(pwForm.Tag, 1) = "2" Then pwForm.Text = cCNVB.tVB_AppTitle
            Else
                If Left(pwForm.Tag, 1) = "2" Then pwForm.Text = SP_STRtToken(pwForm.Tag, ";", cCNVB.nVB_CutTag, True)
            End If

            'เช็คแต่ละ control ในฟอร์ม (หาเจอบาง object เช่น toolbar,status )
            For Each oControl In pwForm.Controls
                'control ทั่วไป
                If Left(oControl.Tag, 1) = "2" Then
                    oControl.Text = SP_STRtToken(oControl.Tag, ";", cCNVB.nVB_CutTag, True)
                End If
                If oControl.Controls.Count > 0 Then
                    Call SP_FrmSetCapSubControl(oControl)
                End If

                'toolbar
                If oControl.GetType Is GetType(ToolStrip) Then
                    For Each oButton As ToolStripItem In CType(oControl, ToolStrip).Items
                        'If oButton.Tag <> "" Then
                        If Left(oButton.Tag, 1) = "2" Then
                            oButton.Text = SP_STRtToken(oButton.Tag, ";", cCNVB.nVB_CutTag, True)
                        End If
                    Next
                End If

                'Menu
                If oControl.GetType Is GetType(MenuStrip) Then
                    For Each oButton As ToolStripMenuItem In CType(oControl, MenuStrip).Items
                        'If oButton.Tag <> "" Then
                        If Left(oButton.Tag, 1) = "2" Then
                            oButton.Text = SP_STRtToken(oButton.Tag, ";", cCNVB.nVB_CutTag, True)
                            If oButton.GetType Is GetType(ToolStripMenuItem) Then
                                If oButton.DropDownItems.Count > 0 Then
                                    Call SP_FrmSetCapMenu(oButton)
                                End If
                            End If
                        End If

                    Next
                End If

                'Grid
                If oControl.GetType Is GetType(C1FlexGrid) Then
                    For Each oColumn As C1.Win.C1FlexGrid.Column In CType(oControl, C1FlexGrid).Cols
                        'If oButton.Tag <> "" Then
                        If Left(oColumn.UserData, 1) = "2" Then
                            oColumn.Caption = SP_STRtToken(oColumn.UserData, ";", cCNVB.nVB_CutTag, True)
                        End If
                    Next
                End If

            Next oControl

            ''caption ของ เมนู (ไม่เจอใน Form)
            'If Not MainMenuStrip Is Nothing Then
            '    For Each oItem As ToolStripMenuItem In pwForm.MainMenuStrip.Items
            '        If oItem.Tag <> "" Then
            '            oItem.Text = SP_STRtToken(oItem.Tag, ";", nW_CutTag, True)
            '            'If oItem.> 0 Then
            '            '    Call W_DATxFrmSetCapMenu(oItem)
            '            'End If
            '        End If
            '    Next
            'End If

            ''caption ของ เมนู (ไม่เจอใน Form)
            'If Not pwForm.ContextMenu Is Nothing Then
            '    For Each oItem As MenuItem In pwForm.ContextMenu.MenuItems
            '        If oItem.Tag <> "" Then
            '            oItem.Text = SP_STRtToken(oItem.Tag, ";", nW_CutTag, True)
            '            If oItem.MenuItems.Count > 0 Then
            '                Call W_DATxFrmSetCapMenu(oItem)
            '            End If
            '        End If
            '    Next
            'End If
            'Exit Sub

        Catch ex As Exception
            cCNSP.SP_MSGShowError(ex.Message)
        End Try
    End Sub

    Private Shared Sub SP_FrmSetCapMenu(ByVal poControl As ToolStripMenuItem)
        '------------------------------------------------
        '   Call:  ส่งตัว menu ที่มี menu ย่อยข้างใน
        '   Cmt:   ใช้ร่วมกันกับ control ไม่ได้เพราะ properties menu เปลี่ยนไป
        '------------------------------------------------
        Try
            For Each oSubCtrl As Object In poControl.DropDownItems
                'If oSubCtrl.Tag <> "" Then
                If Left(oSubCtrl.Tag, 1) = "2" Then
                    oSubCtrl.Text = SP_STRtToken(oSubCtrl.Tag, ";", cCNVB.nVB_CutTag, True)
                    If oSubCtrl.DropDownItems.Count > 0 Then
                        Call SP_FrmSetCapMenu(oSubCtrl)
                    End If
                End If
            Next

        Catch ex As Exception
            cCNSP.SP_MSGShowError(ex.Message)
        End Try


    End Sub

    Private Shared Sub SP_FrmSetCapSubControl(ByVal poControl As Control)
        '------------------------------------------------
        '   Call:  ส่งตัว control ที่มีตัว control อยู่ข้างในอีก
        '   Cmt: 
        '------------------------------------------------
        Dim oSubCtrl As Object
        Try
            For Each oSubCtrl In poControl.Controls
                If Left(oSubCtrl.Tag, 1) = "2" Then
                    oSubCtrl.Text = SP_STRtToken(oSubCtrl.Tag, ";", cCNVB.nVB_CutTag, True)
                End If
                If oSubCtrl.Controls.Count > 0 Then
                    Call SP_FrmSetCapSubControl(oSubCtrl)       'recursion
                End If

                'toolbar
                If oSubCtrl.GetType Is GetType(ToolStrip) Then
                    For Each oButton As ToolStripItem In CType(oSubCtrl, ToolStrip).Items
                        'If oButton.Tag <> "" Then
                        If Left(oButton.Tag, 1) = "2" Then
                            oButton.Text = SP_STRtToken(oButton.Tag, ";", cCNVB.nVB_CutTag, True)
                        End If
                    Next
                End If


                'Grid
                If oSubCtrl.GetType Is GetType(C1FlexGrid) Then
                    For Each oColumn As C1.Win.C1FlexGrid.Column In CType(oSubCtrl, C1FlexGrid).Cols
                        'If oButton.Tag <> "" Then
                        If Left(oColumn.UserData, 1) = "2" Then
                            oColumn.Caption = SP_STRtToken(oColumn.UserData, ";", cCNVB.nVB_CutTag, True)
                        End If
                    Next
                End If


            Next
        Catch ex As Exception
            cCNSP.SP_MSGShowError(ex.Message)
        End Try

    End Sub

    Public Shared Sub SP_ProcessKeyboard()
        '============================
        '[Ake add new : 15/06/2012]
        'Cmt : Get Porcess Keykeyboard 
        '===========================
        'Dim MyProcess As Process
        Dim tDirec As String = System.Environment.SystemDirectory
        Process.Start(tDirec & "\osk.exe")
    End Sub

    Public Shared Function SP_DATbConnect(ByRef poConn As SqlConnection) As Boolean ', ByVal ptSrc$, ByVal ptUsr$, ByVal ptPwd$, ByVal ptDB$
        '----------------------------------------------------------
        '   Call:  poConn as connection
        '              ptSrc is a server data source 
        '              ptUsr is a db user
        '              ptPwd is a db password
        '              ptDB is a db name
        '   Ret:   true, if database was successfully opened
        '----------------------------------------------------------
        Dim tCon$

        Try
            SP_DATbConnect = False

            If poConn Is Nothing Then
                poConn = New SqlConnection
            End If
            tCon = AdaConfig.cConfig.tSQLConnSourceString
            'tCon = "Data Source =" & ptSrc
            'tCon &= ";Initial Catalog=" & ptDB
            'tCon &= ";User ID=" & ptUsr
            'tCon &= ";Password =" & ptPwd

            If poConn.State = ConnectionState.Closed Then
                poConn.ConnectionString = tCon
                poConn.Open()
            End If

            If poConn.State = ConnectionState.Open Then
                SP_DATbConnect = True
            End If

            Exit Function
        Catch ex As Exception
            SP_DATbConnect = False
        End Try
    End Function

    'Public Shared Sub SP_SETxConfig(ByVal ptFile$)
    '    '--------------------------------------------------
    '    '   Call:   ptFile เป็น path และ ชื่อไฟล์ ของ xml เช่น C:\Program Files\AdaFC\AdaToolDotNet\AdaConfig.Xml เป็นต้น 
    '    '   Cmt:   show message
    '    '--------------------------------------------------
    '    AdaConfig.Config.W_SETxPathXml(ptFile)       'dll สำหรับอ่านค่าเริ่มต้นจากไฟล์ xml สำหรับการ connect DB

    '    'database connected
    '    cCNVB.tVB_DbSrc = AdaConfig.Config.Connection.tServer
    '    cCNVB.tVB_DbUser = AdaConfig.Config.Connection.tUser                 'ex. sa
    '    cCNVB.tVB_DbPwd = AdaConfig.Config.Connection.tPassword        'ex. adasoft
    '    cCNVB.tVB_DbName = AdaConfig.Config.Connection.tCatalog         'ex. AdaFC
    '    cCNVB.tVB_SrvPath = AdaConfig.Config.Update.tPath         'Ake add new 31/05/2012
    'End Sub

    Public Shared Function SP_SQLvExecute(ByVal ptSql As String, ByVal poConn As SqlConnection) As DataTable
        '======================================
        'Call : ptSql = sql Command
        '     : poConn = connnect
        'Cmt  : Fill Data ลงใน Data table 
        '======================================
        Dim oDB_Apt As New SqlDataAdapter
        Dim oDB_Dst As New DataSet
        Dim oDB_Dtb As New DataTable

        Dim oDB_Cmd As New SqlCommand

        SP_SQLvExecute = Nothing

        If (ptSql = "") Then Exit Function

        Try
            If poConn.ConnectionString = "" Then
                If Not cCNSP.SP_DATbConnect(poConn) Then
                    Exit Function
                End If
            End If

            If poConn.State = ConnectionState.Closed Then poConn.Open()

            oDB_Cmd.CommandType = CommandType.Text
            oDB_Cmd.CommandText = ptSql
            oDB_Cmd.Connection = poConn
            oDB_Cmd.CommandTimeout = 30

            oDB_Apt.SelectCommand = oDB_Cmd
            oDB_Apt.Fill(oDB_Dst, "Temp")
            oDB_Dtb = oDB_Dst.Tables("Temp")

            If Not oDB_Dtb Is Nothing Then
                SP_SQLvExecute = oDB_Dtb
            End If

        Catch ex As Exception
            cCNSP.SP_MSGShowError(ex.Message)
        End Try

    End Function

    Public Shared Function SP_CNSTRtNumToMnyTha(ByVal ptMonney As String) As String
        '----------------------------------------------------------
        '   Call:   ptMonney is string of money that want to convert
        '   Ret:   money in Tha wording ex. "หนึ่งพันห้าร้อยสิบเอ็ดบาทถ้วน"
        '   -------------------------------------------------------
        Dim tBath As String = ""
        Dim tValue(22) As String
        Dim tMonney As String = ""
        Dim tStang As String = ""
        Dim tDigit As String = ""
        Dim tTemp As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0

        SP_CNSTRtNumToMnyTha = "" '   Clear Function
        tTemp = ptMonney
        ptMonney = Left(Format(CDbl(ptMonney), "0000000000000000000.00"), 22) '   to set  format get 22 char
        tValue(1) = "" : tValue(2) = "สิบ" : tValue(3) = "ร้อย" : tValue(4) = "พัน"
        tValue(5) = "หมื่น" : tValue(6) = "แสน" : tValue(7) = "ล้าน" : tValue(8) = "สิบล้าน"
        tValue(9) = "ร้อยล้าน" : tValue(10) = "พันล้าน" : tValue(11) = "หมื่นล้าน"
        tValue(12) = "แสนล้าน" : tValue(13) = "ล้านล้าน" : tValue(14) = "สิบล้านล้าน" : tValue(15) = "ร้อยล้านล้าน"
        tValue(16) = "พันล้านล้าน" : tValue(17) = "หมื่นล้านล้าน" : tValue(18) = "แสนล้านล้าน" : tValue(19) = "ล้านล้านล้าน"
        tMonney = "" : j = 0 '   Clear parameter
        For i = Len(ptMonney) To 1 Step -1
            '   for i= max downto min do
            tDigit = Mid(Trim(ptMonney), i, 1)
            j = j + 1
            Select Case tDigit
                Case "1"
                    If j = 1 And Mid(Trim(tTemp), 2, 1) <> "." And Len(Trim(tTemp)) > 1 Then
                        tBath = "เอ็ด"
                    Else
                        If j <> 8 Then tBath = "หนึ่ง"
                    End If
                    If j = 2 Then tBath = ""

                Case "2"
                    If j = 2 Then
                        tBath = "ยี่"
                    Else
                        tBath = "สอง"
                    End If
                Case "3" : tBath = "สาม"
                Case "4" : tBath = "สี่"
                Case "5" : tBath = "ห้า"
                Case "6" : tBath = "หก"
                Case "7" : tBath = "เจ็ด"
                Case "8" : tBath = "แปด"
                Case "9" : tBath = "เก้า"
                Case "." : tStang = tMonney & "สตางค์" : tMonney = "" : j = 0
            End Select
            If tDigit <> "0" Then tMonney = tBath & tValue(j) & tMonney
            tBath = ""
        Next i
        If Trim(tMonney) = vbNullString Then tMonney = "ศูนย์"
        If tStang = "สตางค์" Then
            SP_CNSTRtNumToMnyTha = tMonney & "บาทถ้วน"
        Else
            SP_CNSTRtNumToMnyTha = tMonney & "บาท" & tStang
        End If
    End Function

    ''' <summary>
    ''' Get Variable
    ''' </summary>
    Public Shared Sub SP_GETxVariable()
        Dim oSql As New StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Try
            oSql.Append("SELECT Bch.FTBchHQ FROM TCNMBranch Bch INNER JOIN TCNMComp Cmp ON Bch.FTBchCode = Cmp.FTBchCode")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                If oDbTbl.Rows(0)("FTBchHQ") = "1" Then
                    cCNVB.bVB_BchHQ = True
                Else
                    cCNVB.bVB_BchHQ = False
                End If
            Else
                cCNVB.bVB_BchHQ = False
            End If

            'Get Comp Value
            oDbTbl = New DataTable
            oSql = New StringBuilder
            oSql.Append("SELECT TOP 1 FTBchCode,FCCmpVatAmt FROM TCNMComp")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                cCNVB.tVB_BchCode = oDbTbl.Rows(0)("FTBchCode")
                cCNVB.cVB_CmpVatAmt = Convert.ToDouble(oDbTbl.Rows(0)("FCCmpVatAmt"))
            End If

            'Get Mapping Purge
            oDbTbl = New DataTable
            oSql = New StringBuilder
            oSql.Append("SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FTPurge FROM TLNKMapping WHERE FTLNMCode='PURGE'")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                cCNVB.nVB_PurgeDay = Convert.ToInt32(oDbTbl.Rows(0)("FTPurge"))
            End If

        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Move XmlFile FTP To Archive
    ''' </summary>
    Public Shared Sub SP_GETxMoveXml2Archive()
        Dim oFtpReq As Net.FtpWebRequest
        Dim oFtpRes As Net.FtpWebResponse
        Dim oFtpFdrReq As Net.FtpWebRequest
        Dim oFtpFdrRes As Net.FtpWebResponse
        Dim oFile As New List(Of String)
        Dim tFtpUri As String = ""
        Dim tLine As String = ""
        Dim tFolderBK As String = "Archive"
        Dim atFolder As String() = {"MC", "Article", "Promotion"}
        Try
            'Loop Folder FTP OutBound *CH 14-09-2017
            For Each tFolder In atFolder
                'Get File
                oFile = SP_GEToListXmlFile(tFolder)
                tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tOutFolder & "/" & tFolder & "/"

                '#### Create Folder Archive ####
                Try
                    oFtpFdrReq = System.Net.WebRequest.Create(tFtpUri & tFolderBK)
                    oFtpFdrReq.Method = System.Net.WebRequestMethods.Ftp.MakeDirectory
                    oFtpFdrReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
                    oFtpFdrRes = oFtpFdrReq.GetResponse()

                    'Close
                    oFtpFdrRes.Close()
                Catch ex As Exception
                End Try
                '#### Create Folder Archive ####

                If oFile.Count > 0 Then
                    For Each oItem In oFile
                        'Check File Xml
                        If oItem.Length < 3 OrElse Right(oItem, 3).ToUpper <> "XML" Then Continue For

                        oFtpReq = System.Net.WebRequest.Create(tFtpUri & oItem)

                        'Specify That You Want To Download A File'
                        oFtpReq.Method = System.Net.WebRequestMethods.Ftp.Rename

                        'Specify Username & Password'
                        oFtpReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
                        'oFtpReq.RenameTo = tFtpUri & tFolderBK & "/" & oItem
                        oFtpReq.RenameTo = tFolderBK & "/" & oItem

                        'Response Object'
                        oFtpRes = oFtpReq.GetResponse()

                        'Close
                        oFtpRes.Close()

                        'Download XML File from sFTP '*CH 03-08-2017
                        cCNSP.SP_GETxDownloadXml4Imp(tFolder & "/" & tFolderBK, oItem)
                    Next
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & ";" & ex.Message)
        Finally
            oFtpReq = Nothing
            oFtpRes = Nothing
            oFtpFdrReq = Nothing
            oFtpFdrRes = Nothing
        End Try
    End Sub

    Public Shared Sub SP_GETxDownloadToInbox()
        Dim tFtpUri As String = ""
        Dim tSrcPath As String = ""    ' Path Server
        Dim tDesPath As String = ""    ' Path วางไฟล์
        Dim tReconcile As String = ""
        Try
            If AdaConfig.cConfig.oConfigXml.tReconcile = "" Then
                tReconcile = "/"
            Else
                tReconcile = AdaConfig.cConfig.oConfigXml.tReconcile
            End If
            tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & tReconcile
            Dim oFTPReq As FtpWebRequest = DirectCast(WebRequest.Create(tFtpUri), FtpWebRequest)
            oFTPReq.Credentials = New NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
            oFTPReq.Method = WebRequestMethods.Ftp.ListDirectory
            Dim oRes As FtpWebResponse = DirectCast(oFTPReq.GetResponse(), FtpWebResponse)
            Dim oStream As New StreamReader(oRes.GetResponseStream())
            Dim aoDir As New List(Of String)()
            Dim tFile As String = oStream.ReadLine()
            While Not String.IsNullOrEmpty(tFile)
                aoDir.Add(tFile)
                tFile = oStream.ReadLine()
            End While
            oStream.Close()

            Using oFTPClient As New WebClient()
                oFTPClient.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
                For i As Integer = 0 To aoDir.Count - 1
                    If aoDir(i).Contains(".") Then
                        tSrcPath = tFtpUri + aoDir(i).ToString()
                        tDesPath = AdaConfig.cConfig.oConfigXml.tInbox + "\" + aoDir(i).ToString()
                        oFTPClient.DownloadFile(tSrcPath, tDesPath)
                        SP_DELxFileFTP(aoDir(i).ToString())
                    End If
                Next
            End Using
        Catch ex As Exception
            MessageBox.Show("ไม่สามารถติดต่อ FTP Server ได้", "สถานะ", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Public Shared Sub SP_GETxDownloadToInboxsFTP()
        Dim tFtpUri As String = ""
        Dim tSrcPath As String = ""    ' Path Server
        Dim tDesPath As String = ""    ' Path วางไฟล์
        Dim tReconcile As String = ""
        Dim tHost As String = ""
        Dim tUsr As String = ""
        Dim tPwd As String = ""
        Dim nPort As Integer
        Dim tFileName As String = ""
        Dim oStream As Stream
        Try
            tHost = AdaConfig.cConfig.oConfigXml.tHostName
            tUsr = AdaConfig.cConfig.oConfigXml.tUserName
            tPwd = AdaConfig.cConfig.oConfigXml.tPassword
            nPort = AdaConfig.cConfig.oConfigXml.tPortNumber
            If AdaConfig.cConfig.oConfigXml.tReconcile = "" Then
                tReconcile = "/"
            Else
                tReconcile = AdaConfig.cConfig.oConfigXml.tReconcile
            End If

            Using osftp As New SftpClient(tHost, 22, tUsr, tPwd)
                osftp.Connect()
                Dim oFiles = osftp.ListDirectory(tReconcile)

                For Each oFile In oFiles

                    If Not oFile.Name.StartsWith(".") Then
                        tFileName = oFile.Name
                        oStream = File.OpenWrite(AdaConfig.cConfig.oConfigXml.tInbox + "\" + tFileName)
                        osftp.DownloadFile(tReconcile + tFileName, oStream)
                        osftp.DeleteFile(tReconcile + tFileName)
                        oStream.Close()
                    End If
                Next
            End Using
        Catch ex As Exception
            MessageBox.Show("ไม่สามารถติดต่อ sFTP Server ได้", "สถานะ", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Public Shared Sub SP_DELxFileFTP(ByVal ptFileName As String)
        Dim tFtpUri As String = ""
        Dim tReconcile As String = ""
        Try
            'Create Request
            If AdaConfig.cConfig.oConfigXml.tReconcile = "" Then
                tReconcile = "/"
            Else
                tReconcile = AdaConfig.cConfig.oConfigXml.tReconcile
            End If

            tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & tReconcile + ptFileName
            Dim oFTPReq As FtpWebRequest = DirectCast(WebRequest.Create(tFtpUri), FtpWebRequest)
            oFTPReq.Credentials = New NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
            oFTPReq.Method = System.Net.WebRequestMethods.Ftp.DeleteFile
            oFTPReq.GetResponse()
        Catch ex As Exception
            MessageBox.Show("ไม่สามารถติดต่อ FTP Server ได้", "สถานะ", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Public Shared Function SP_GETbUploadToFTP(ByVal ptFileName As String) As Boolean
        Dim tFtpUri As String = ""
        Dim tPathOutBox As String = ""    ' Path OutBox
        Dim tPath As String = ""

        Try

            If AdaConfig.cConfig.oConfigXml.tCashTransfer = "" Then
                tPath = "/"
            Else
                tPath = AdaConfig.cConfig.oConfigXml.tCashTransfer
            End If
            tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & tPath + ptFileName
            tPathOutBox = AdaConfig.cConfig.oConfigXml.tOutbox + "\" + ptFileName
            Dim oReq As FtpWebRequest = DirectCast(System.Net.WebRequest.Create(tFtpUri), FtpWebRequest)

            'Upload Properties
            oReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
            oReq.Method = System.Net.WebRequestMethods.Ftp.UploadFile

            'Read File
            Dim File() As Byte = System.IO.File.ReadAllBytes(tPathOutBox)

            'Upload
            Dim oStream As System.IO.Stream = oReq.GetRequestStream()
            oStream.Write(File, 0, File.Length)
            oStream.Close()
            oStream.Dispose()
            Return True
        Catch ex As Exception

            Throw New Exception(ex.Message)
            Return False

        End Try

    End Function


    Public Shared Function SP_GETbUploadToSFTP(ByVal ptFileName As String) As Boolean
        Dim tFtpUri As String = ""
        Dim tPathOutBox As String = ""    ' Path OutBox
        Dim tPath As String = ""
        Dim tHost As String = ""
        Dim tUsr As String = ""
        Dim tPwd As String = ""
        Dim nPort As Integer
        Dim oStream As Stream
        Try

            If AdaConfig.cConfig.oConfigXml.tCashTransfer = "" Then
                tPath = "/"
            Else
                tPath = AdaConfig.cConfig.oConfigXml.tCashTransfer
            End If

            tHost = AdaConfig.cConfig.oConfigXml.tHostName
            tUsr = AdaConfig.cConfig.oConfigXml.tUserName
            tPwd = AdaConfig.cConfig.oConfigXml.tPassword
            nPort = AdaConfig.cConfig.oConfigXml.tPortNumber
            Using osftp As New SftpClient(tHost, nPort, tUsr, tPwd)
                osftp.Connect()
                oStream = New FileStream(AdaConfig.cConfig.oConfigXml.tOutbox + "\" + ptFileName, FileMode.Open)
                osftp.UploadFile(oStream, tPath + ptFileName)
                oStream.Close()
                osftp.Disconnect()
            End Using

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        Finally
            tFtpUri = Nothing
            tPathOutBox = Nothing
            tPath = Nothing
            tHost = Nothing
            tUsr = Nothing
            tPwd = Nothing
            nPort = Nothing
            oStream = Nothing
        End Try

    End Function

    'Public Shared Sub SP_GETxDownloadXml4Imp(ByVal ptFile As String)
    '    Dim oFtpReq As Net.FtpWebRequest
    '    Dim oFtpRes As Net.FtpWebResponse
    '    'Dim oFtpDelReq As Net.FtpWebRequest
    '    'Dim oFtpDelRes As Net.FtpWebResponse
    '    Dim oStream As IO.Stream
    '    Dim oStreamRead As IO.StreamReader
    '    Dim oFile As New List(Of String)
    '    Dim tFtpUri As String = ""
    '    Dim tLine As String = ""
    '    Dim tFolderBK As String = "Archive"
    '    Dim atFolder As String() = {"MC/" & tFolderBK, "Article/" & tFolderBK, "Promotion/" & tFolderBK}
    '    Try
    '        'Loop Folder FTP OutBound *CH 14-09-2017
    '        For Each tFolder In atFolder
    '            'Get File
    '            'oFile = SP_GEToListXmlFile(tFolder)
    '            oFile.Add(ptFile)

    '            If oFile.Count > 0 Then
    '                For Each oItem In oFile
    '                    tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tOutFolder & "/" & tFolder & "/"
    '                    oFtpReq = System.Net.WebRequest.Create(tFtpUri & oItem)

    '                    'Specify That You Want To Download A File'
    '                    oFtpReq.Method = System.Net.WebRequestMethods.Ftp.DownloadFile

    '                    'Specify Username & Password'
    '                    oFtpReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)

    '                    'Response Object'
    '                    oFtpRes = oFtpReq.GetResponse()

    '                    'Incoming File Stream'
    '                    oStream = oFtpRes.GetResponseStream()

    '                    'Read File Stream Data'
    '                    oStreamRead = New IO.StreamReader(oStream)
    '                    Using oReader As IO.StreamReader = oStreamRead
    '                        ' Read one line from file
    '                        tLine = oReader.ReadToEnd
    '                    End Using
    '                    Using oWriter As IO.StreamWriter = IO.File.AppendText(AdaConfig.cConfig.oConfigXml.tInbox & "/" & oItem)
    '                        With oWriter
    '                            'If bHDFile = True Then
    '                            .WriteLine(tLine)
    '                            .Flush()
    '                            .Close()
    '                        End With
    '                    End Using

    '                    ''Delete file FTP
    '                    'oFtpDelReq = System.Net.WebRequest.Create(tFtpUri & oItem)
    '                    'oFtpDelReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
    '                    'oFtpDelReq.Method = System.Net.WebRequestMethods.Ftp.DeleteFile
    '                    'oFtpDelRes = oFtpDelReq.GetResponse

    '                    'Close
    '                    oFtpRes.Close()
    '                    oStream.Close()
    '                    'oFtpDelRes.Close()
    '                    oStreamRead.Close()
    '                Next
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & ";" & ex.Message)
    '    Finally
    '        oFtpReq = Nothing
    '        oFtpRes = Nothing
    '        oStream = Nothing
    '        oStreamRead = Nothing
    '    End Try
    'End Sub
    ''' <summary>
    ''' Download XmlFile from sFTP
    ''' </summary>
    Public Shared Sub SP_GETxDownloadXml4Imp(ByVal ptFolder As String, ByVal ptFile As String)
        Dim oFtpReq As Net.FtpWebRequest
        Dim oFtpRes As Net.FtpWebResponse
        Dim oStream As IO.Stream
        Dim oStreamRead As IO.StreamReader
        Dim oFile As New List(Of String)
        Dim tFtpUri As String = ""
        Dim tLine As String = ""
        Try
            'Get File
            oFile.Add(ptFile)

            If oFile.Count > 0 Then
                For Each oItem In oFile
                    tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tOutFolder & "/" & ptFolder & "/"
                    oFtpReq = System.Net.WebRequest.Create(tFtpUri & oItem)

                    'Specify That You Want To Download A File'
                    oFtpReq.Method = System.Net.WebRequestMethods.Ftp.DownloadFile

                    'Specify Username & Password'
                    oFtpReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)

                    'Response Object'
                    oFtpRes = oFtpReq.GetResponse()

                    'Incoming File Stream'
                    oStream = oFtpRes.GetResponseStream()

                    'Read File Stream Data'
                    oStreamRead = New IO.StreamReader(oStream)
                    Using oReader As IO.StreamReader = oStreamRead
                        ' Read one line from file
                        tLine = oReader.ReadToEnd
                    End Using
                    Using oWriter As IO.StreamWriter = IO.File.AppendText(AdaConfig.cConfig.oConfigXml.tInbox & "/" & oItem)
                        With oWriter
                            'If bHDFile = True Then
                            .WriteLine(tLine)
                            .Flush()
                            .Close()
                        End With
                    End Using

                    'Close
                    oFtpRes.Close()
                    oStream.Close()
                    oStreamRead.Close()
                Next
            End If
        Catch ex As Exception
            MessageBox.Show("ไม่สามารถติดต่อ FTP Server ได้", "สถานะ", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Finally
            oFtpReq = Nothing
            oFtpRes = Nothing
            oStream = Nothing
            oStreamRead = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Get List xml file from sFTP
    ''' </summary>
    ''' <returns>List(Of String)</returns>
    Private Shared Function SP_GEToListXmlFile(ByVal ptFolder As String) As List(Of String)
        Dim oFtpReq As Net.FtpWebRequest
        Dim oFtpRes As Net.FtpWebResponse
        Dim oStream As IO.Stream
        Dim oStreamRead As IO.StreamReader
        Dim oFile As New List(Of String)
        Dim tFtpUri As String = ""
        Dim tLine As String = ""
        'Dim atLine As String()
        Try
            tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tOutFolder & "/" & ptFolder & "/"

            'Get File
            oFtpReq = System.Net.WebRequest.Create(tFtpUri)
            oFtpReq.Method = System.Net.WebRequestMethods.Ftp.ListDirectory

            'Specify Username & Password'
            oFtpReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)

            'Response Object'
            oFtpRes = oFtpReq.GetResponse()

            'Incoming File Stream'
            oStream = oFtpRes.GetResponseStream()

            'Read File Stream Data'
            oStreamRead = New IO.StreamReader(oStream)
            Do
                tLine = oStreamRead.ReadLine
                If tLine Is Nothing Then Exit Do
                If tLine.Length < 1 Then Exit Do
                Dim atFile = tLine.Split(vbNewLine)
                For nItem = 0 To atFile.Count - 1
                    If atFile(nItem).Length > 0 Then
                        oFile.Add(atFile(nItem))
                    End If
                Next
            Loop

            'Close
            oFtpRes.Close()
            oStream.Close()
            oStreamRead.Close()
        Catch ex As Exception
            MessageBox.Show("ไม่สามารถติดต่อ FTP Server ได้", "สถานะ", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Finally
            oFtpReq = Nothing
            oFtpRes = Nothing
            oStream = Nothing
            oStreamRead = Nothing
        End Try
        Return oFile
    End Function

    ''' <summary>
    ''' Send XmlFile to FTP
    ''' </summary>
    ''' <param name="ptPathFile">Full Path</param>
    ''' <returns>Boolean True:Send Success, False:Error</returns>
    Public Shared Function SP_PRCbSendFile2FTP(ByVal ptPathFile As String) As Boolean
        Dim oFtpFdr As Net.FtpWebRequest
        Dim oFtpReq As Net.FtpWebRequest
        Dim oFtpRes As Net.FtpWebResponse
        Dim oStream As IO.Stream
        Dim tFtpFdr As String = ""
        Dim tFtpUri As String = ""
        Dim tFileName As String = ""
        Dim atfile() As Byte
        Dim bStaSend As Boolean = True
        Try
            'Check have file
            If IO.File.Exists(ptPathFile) Then
                tFileName = IO.Path.GetFileName(ptPathFile)
                Select Case cCNVB.tVB_Export
                    Case cCNVB.tVB_ZSDINT005 'Sale 
                        tFtpFdr = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tInFolder & "/Sale data"
                    Case cCNVB.tVB_ZSDINT006 'Reconcile
                        tFtpFdr = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tReconcile
                    Case cCNVB.tVB_ZGLINT001 'Expense
                        tFtpFdr = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tExpense
                    Case cCNVB.tVB_ZGLINT002 'Cash Tranfer
                        tFtpFdr = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tCashTransfer
                    Case Else
                        tFtpFdr = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tInFolder
                End Select

                '#### Create Folder ####
                Try
                    tFtpFdr = tFtpFdr.Replace("\", "/")
                    oFtpFdr = System.Net.WebRequest.Create(tFtpFdr)
                    oFtpFdr.Method = System.Net.WebRequestMethods.Ftp.MakeDirectory
                    oFtpFdr.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
                    oFtpRes = oFtpFdr.GetResponse()
                Catch ex As Exception
                End Try
                '#### Create Folder ####

                '#### Upload File ####
                tFtpUri = tFtpFdr & "/" & tFileName
                oFtpReq = System.Net.WebRequest.Create(tFtpUri)
                oFtpReq.Method = System.Net.WebRequestMethods.Ftp.UploadFile

                'Specify Username & Password'
                oFtpReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)

                'Locate File And Store It In Byte Array'
                atfile = IO.File.ReadAllBytes(ptPathFile)

                'Get File'
                oStream = oFtpReq.GetRequestStream()

                'Upload Each Byte'
                oStream.Write(atfile, 0, atfile.Length)

                'Close'
                oStream.Close()

                'Free Memory'
                oStream.Dispose()
            Else
                bStaSend = False
            End If
        Catch ex As Exception
            bStaSend = False
        Finally
            oFtpFdr = Nothing
            oFtpReq = Nothing
            oFtpRes = Nothing
            oStream = Nothing
        End Try
        Return bStaSend
    End Function

    ''' <summary>
    ''' Purge File Xml FROM FTP
    ''' </summary>
    Public Shared Sub SP_SETxPurgeBackup()
        Dim oFtpDelReq As Net.FtpWebRequest
        Dim oFtpDelRes As Net.FtpWebResponse
        Dim oFile As New List(Of String)
        Dim tFtpUri As String = ""
        Dim tFolderBK As String = "Archive"
        'Dim atFolder As String() = {"MC/" & tFolderBK, "Article/" & tFolderBK, "Promotion/" & tFolderBK}
        Dim atFolder As String() = {"Article/" & tFolderBK, "Promotion/" & tFolderBK}
        Dim nDateFile As Integer = 0
        Dim nDateChk As Integer = Convert.ToInt32(Now.ToString("yyyyMMdd")) - cCNVB.nVB_PurgeDay
        Try
            'Loop Folder FTP OutBound *CH 14-09-2017
            For Each tFolder In atFolder
                tFtpUri = "ftp://" & AdaConfig.cConfig.oConfigXml.tHostName & "/" & AdaConfig.cConfig.oConfigXml.tOutFolder & "/" & tFolder & "/"

                'Get File
                oFile = SP_GEToListXmlFile(tFolder)

                If oFile.Count > 0 Then
                    For Each tItem In oFile
                        'PROM_20170816_172805.xml
                        nDateFile = Convert.ToInt32(Left(Right(tItem, 19), 8))

                        If nDateFile < nDateChk Then
                            'Delete file FTP
                            oFtpDelReq = System.Net.WebRequest.Create(tFtpUri & tItem)
                            oFtpDelReq.Credentials = New System.Net.NetworkCredential(AdaConfig.cConfig.oConfigXml.tUserName, AdaConfig.cConfig.oConfigXml.tPassword)
                            oFtpDelReq.Method = System.Net.WebRequestMethods.Ftp.DeleteFile
                            oFtpDelRes = oFtpDelReq.GetResponse

                            'Close
                            oFtpDelRes.Close()
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            'Throw New Exception(ex.Message & ";" & ex.Message)
        Finally
            oFtpDelReq = Nothing
            oFtpDelRes = Nothing
        End Try
    End Sub

    Public Function SP_GETmAutoFmtCode(ByVal ptTableName As String, ByVal ptFedCode As String, ByVal ptDocType As String, ByVal ptBchCode As String, Optional ByVal poConnect As SqlClient.SqlConnection = Nothing, Optional ByVal poException As Exception = Nothing) As cGetFormatAutoTemplate
        Dim tSql As String = ""
        Dim oCon As New SqlClient.SqlConnection
        Dim oDBTbl As New DataTable
        Dim oAdp As SqlClient.SqlDataAdapter
        'Dim orsTemp As cmlGetFormatAutoTemplate
        If poConnect Is Nothing Then
            oCon.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
        Else
            oCon = poConnect
        End If
        Try
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oCon.State = ConnectionState.Closed Then oCon.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oCon)
                oCon = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oCon.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oCon = poConnect
                End If
                If oCon.State = ConnectionState.Closed Then oCon.Open()
            End Try
            '*******************************'
            tSql = "STP_CN_GETtAutoFmtCode '" & ptFedCode & "'"
            tSql = tSql & ",'" & ptTableName & "'"
            tSql = tSql & ",'" & ptDocType & "'"
            tSql = tSql & ",'" & ptBchCode & "'"
            oAdp = New SqlClient.SqlDataAdapter(tSql, oCon)
            oAdp.Fill(oDBTbl)

            If (oDBTbl IsNot Nothing) AndAlso (oDBTbl.Rows.Count > 0) Then
                Dim orsTemp = From it As DataRow In oDBTbl.Rows Select New cGetFormatAutoTemplate With {
                              .FTBchCode = it.Item("FTBchCode").ToString _
                              , .FTDocType = it.Item("FTDocType").ToString _
                              , .FTPKField = it.Item("FTPKField").ToString _
                              , .FTSatDocTypeName = it.Item("FTSatDocTypeName").ToString _
                              , .FTSatRetFmtAll = it.Item("FTSatRetFmtAll").ToString _
                              , .FTSatRetFmtChar = it.Item("FTSatRetFmtChar").ToString _
                              , .FTSatRetFmtDay = it.Item("FTSatRetFmtDay").ToString _
                              , .FTSatRetFmtMonth = it.Item("FTSatRetFmtMonth").ToString _
                              , .FTSatRetFmtNum = it.Item("FTSatRetFmtNum").ToString _
                              , .FTSatRetFmtYear = it.Item("FTSatRetFmtYear").ToString _
                              , .FTSatStaResetBill = it.Item("FTSatStaResetBill").ToString _
                              , .FTTableName = it.Item("FTTableName").ToString}
                Return orsTemp.FirstOrDefault
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        Finally
            If poConnect Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oCon IsNot Nothing AndAlso oCon.State = ConnectionState.Open Then oCon.Close()
                oCon = Nothing
            End If
            oDBTbl = Nothing
            oAdp = Nothing
        End Try
    End Function

    Function SP_GETtLastCode(ByVal pModel As cGetFormatAutoTemplate, Optional ByVal poConnect As SqlClient.SqlConnection = Nothing, Optional ByRef poException As Exception = Nothing) As String
        Dim tSql As String = ""
        Dim oCon As New SqlClient.SqlConnection
        Dim oDBTbl As New DataTable
        Dim oAdp As SqlClient.SqlDataAdapter
        If poConnect Is Nothing Then
            oCon.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
        Else
            oCon = poConnect
        End If
        Try
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oCon.State = ConnectionState.Closed Then oCon.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oCon)
                oCon = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oCon.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oCon = poConnect
                End If
                If oCon.State = ConnectionState.Closed Then oCon.Open()
            End Try
            '*******************************'
            Dim tResult As String

            With pModel
                tResult = ""
                tSql = "STP_CN_GETtMaxCode '" & .FTSatStaResetBill & "'"
                tSql = tSql & ",'" & .FTSatRetFmtChar & "'"
                tSql = tSql & ",'" & .FTSatRetFmtYear & "'"
                tSql = tSql & ",'" & .FTSatRetFmtMonth & "'"
                tSql = tSql & ",'" & .FTSatRetFmtDay & "'"
                tSql = tSql & ",'" & .FTSatRetFmtNum & "'"
                tSql = tSql & ",'" & .FTSatRetFmtAll & "'"
                tSql = tSql & ",'" & .FTBchCode & "'"
                tSql = tSql & ",'" & .FTPKField & "'"
                tSql = tSql & ",'" & .FTTableName & "'"
                tSql = tSql & ",'" & .FTDocType & "'"
                tSql = tSql & ",'" & .FTSatDocTypeName & "'"
                tSql = tSql & ",'" & tResult & "'"
                oAdp = New SqlClient.SqlDataAdapter(tSql, oCon)
                oAdp.Fill(oDBTbl)
            End With
            If oDBTbl IsNot Nothing AndAlso oDBTbl.Rows.Count > 0 Then
                tResult = oDBTbl.Rows(0).Item(0)
            End If
            Return tResult
        Catch ex As Exception
            poException = ex
            Return ""
        Finally
            If poConnect Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oCon IsNot Nothing AndAlso oCon.State = ConnectionState.Open Then oCon.Close()
                oCon = Nothing
            End If
            oAdp = Nothing
            oDBTbl = Nothing
        End Try
    End Function
End Class

'Ext Module
Module mCNSP

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNCHKcValidate(ByVal pcValue As String) As String
        SP_CNCHKcValidate = ""
        If Not pcValue Is Nothing Then
            SP_CNCHKcValidate = pcValue
        End If
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNCHKcValidate(ByVal pcValue As System.Nullable(Of Double)) As Double
        SP_CNCHKcValidate = 0
        If pcValue.HasValue = True Then
            SP_CNCHKcValidate = pcValue
        End If
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNCHKcValidate(ByVal pcValue As System.Nullable(Of Integer)) As String
        SP_CNCHKcValidate = ""
        If Not pcValue Is Nothing Then
            SP_CNCHKcValidate = pcValue
        End If
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNCHKcValidate(ByVal pcValue As Double) As Double
        SP_CNCHKcValidate = 0
        If pcValue > 0 Then
            SP_CNCHKcValidate = pcValue
        End If
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNCHKcStringtoDouble(ByVal ptValue As String) As Double
        SP_CNCHKcStringtoDouble = 0
        If ptValue.Length > 0 Then
            If IsNumeric(ptValue) Then
                SP_CNCHKcStringtoDouble = CDbl(ptValue)
            End If
        End If
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNSETtNum2Shw(ByVal pcValue As Double) As String
        SP_CNSETtNum2Shw = FormatNumber(pcValue, 2) 'App.oDefault.nDecPntMoney
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNSETtFmtddMMyyyy(ByVal pdValue As Date) As String
        SP_CNSETtFmtddMMyyyy = Format(pdValue, "dd/MM/yyyy")
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNSETtFmtHHmmss(ByVal pdValue As Date) As String
        SP_CNSETtFmtHHmmss = Format(pdValue, "HH:mm:ss")
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNSETtFmtDateAndTime(ByVal pdValue As Date) As String
        SP_CNSETtFmtDateAndTime = Format(pdValue, "dd/MM/yyyy HH:mm:ss")
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNSETtSQLFmtDateAndTime(ByVal pdValue As Date) As String
        SP_CNSETtSQLFmtDateAndTime = Format(pdValue, "yyyy-MM-dd HH:mm:ss")
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_DTEtStrSTD(ByVal pdDate As Date, Optional ByVal pbChrRet As Boolean = False, Optional ByVal ptTime$ = "", Optional ByVal pbRetWithTime As Boolean = False) As String
        '----------------------------------------------------------
        '   Call:   pdDate as selected date
        '   Ret:   string of date format "YYYY/MM/DD"
        '----------------------------------------------------------
        Dim tRet$
        SP_DTEtStrSTD = Format(Year(pdDate), "0000") & "/" & Format(Month(pdDate), "00") & "/" & Format(Microsoft.VisualBasic.Day(pdDate), "00")
        tRet = SP_DTEtStrSTD
        If pbChrRet Then
            If pbRetWithTime Then
                SP_DTEtStrSTD = "'" & tRet & " " & ptTime & "'"
            Else
                SP_DTEtStrSTD = "'" & tRet & "'"
            End If
        Else
            If pbRetWithTime Then
                SP_DTEtStrSTD = tRet & " " & ptTime
            End If
        End If
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function SP_CNSETtStrConvertDateTime(ByVal ptValue As String, Optional ptSplitDate As String = "-") As String
        Try
            'Dim tFmtDateTime As String = "{0}-{1}-{2} {3}:{4}:{5}"
            'Dim tDateTime As String = ptValue.Substring(0, 15).Replace("-", " ")
            'Dim tYear As String = tDateTime.Substring(0, 4)
            'Dim tMonth As String = tDateTime.Substring(4, 2)
            'Dim tDay As String = tDateTime.Substring(6, 2)
            'Dim tHour As String = tDateTime.Substring(9, 2)
            'Dim tMin As String = tDateTime.Substring(11, 2)
            'Dim tSec As String = tDateTime.Substring(13, 2)
            'tDateTime = String.Format(tFmtDateTime, tYear, tMonth, tDay, tHour, tMin, tSec)
            Dim tFmtDateTime As String = "{0}{6}{1}{6}{2} {3}:{4}:{5}"
            Dim tDateTime As String = ptValue.Substring(0, 14).Replace("-", "").Replace(" ", "")
            Dim tYear As String = tDateTime.Substring(0, 4)
            Dim tMonth As String = tDateTime.Substring(4, 2)
            Dim tDay As String = tDateTime.Substring(6, 2)
            Dim tHour As String = tDateTime.Substring(8, 2)
            Dim tMin As String = tDateTime.Substring(10, 2)
            Dim tSec As String = tDateTime.Substring(12, 2)
            'Check Date & time
            Dim dDateTime As New System.DateTime(CInt(tYear), CInt(tMonth), CInt(tDay), CInt(tHour), CInt(tMin), CInt(tSec), 0)

            tDateTime = String.Format(tFmtDateTime, tYear, tMonth, tDay, tHour, tMin, tSec, ptSplitDate)
            Return tDateTime
        Catch ex As Exception
            Return ""
        End Try
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function SP_CNSETtFormatFileSize(ByVal pcValue As Long) As String
        Dim aSizeTypes() As String = {"B", "KB", "MB", "GB"}
        Dim cLen As Decimal = pcValue
        Dim nSizeType As Integer = 0
        Do While cLen > 1024
            cLen = Decimal.Round(cLen / 1024, 2)
            nSizeType += 1
            If nSizeType >= aSizeTypes.Length - 1 Then Exit Do
        Loop

        Return cLen.ToString & " " & aSizeTypes(nSizeType)
    End Function

    'Public Shared Function FormatFileSize(ByVal FileSizeBytes As Long) As String
    '    Dim sizeTypes() As String = {"b", "Kb", "Mb", "Gb"}
    '    Dim Len As Decimal = FileSizeBytes
    '    Dim sizeType As Integer = 0
    '    Do While Len > 1024
    '        Len = Decimal.Round(Len / 1024, 2)
    '        sizeType += 1
    '        If sizeType >= sizeTypes.Length - 1 Then Exit Do
    '    Loop

    '    Dim Resp As String = Len.ToString & " " & sizeTypes(sizeType)
    '    Return Resp
    'End Function

    '<System.Runtime.CompilerServices.Extension()> _
    'Public Function SP_CNSETtNum2Shw(ByVal ptValue As String) As String
    '    SP_CNSETtNum2Shw = Format(CDbl(ptValue), "###,###,##0.##")
    'End Function

    'Public Function SP_FmtNum2Shw(ByVal ptNumber As String, Optional ByVal pDecPoint As Integer = 0 _
    '                            , Optional ByVal pbNoDot As Boolean = True) As String
    '    '----------------------------------------------------------
    '    '   Call:   ptNumber is all number datatype
    '    '   Cmt:   return format of number
    '    '----------------------------------------------------------
    '    Dim tFormat As String
    '    Dim cValue As Double

    '    If Not IsNumeric(ptNumber) Then
    '        cValue = 0
    '    Else
    '        cValue = CDbl(ptNumber)
    '    End If

    '    If pDecPoint < 1 Or pbNoDot Then
    '        tFormat = "##0"
    '    Else
    '        tFormat = "#,##0." & StrDup(pDecPoint, "0")
    '    End If

    '    SP_FmtNum2Shw = Format(cValue, tFormat)
    'End Function

    'Public Function SP_CNnRoundDiff(ByVal pcGrand As Double) As Double
    '    '----------------------------------------------------------
    '    '    'PUM 09/06/2010 
    '    '----------------------------------------------------------

    '    Dim tTotal$
    '    Dim tNum$
    '    Dim cDif As Double
    '    SP_CNnRoundDiff = 0

    '    Try
    '        'If cCNVB.nVB_CNPrnSlpRoundDiff = 0 Then
    '        '    SP_CNnRoundDiff = 0
    '        '    Exit Function
    '        'End If
    '        tTotal = SP_CNFMTcCurPoint(pcGrand)

    '        If App.oDefault.cAmtRound < 1 Then 'ตัด String
    '            tNum = Microsoft.VisualBasic.Right(tTotal, CStr(App.oDefault.cAmtRound).Length)
    '            If CDbl(tNum) = 0 Then Exit Function
    '            cDif = SP_CNDATcRoundDiff(tNum)
    '        Else
    '            tNum = Microsoft.VisualBasic.Right(tTotal, CStr(App.oDefault.cAmtRound).Length + 3)
    '            If CDbl(tNum) = 0 Then Exit Function
    '            cDif = SP_CNDATcRoundDiff(tNum)
    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    SP_CNnRoundDiff = FormatNumber(cDif, 2)

    'End Function

    'Public Function SP_CNFMTcCurPoint(ByRef pcValue As Object, Optional ByVal pbNoComma As Boolean = True) As String
    '    '----------------------------------------------------------
    '    '   Call:   pcValue as double value
    '    '   Cmt:   return format of number
    '    '----------------------------------------------------------
    '    Try
    '        'if start might be zero
    '        App.oDefault.nDecPntMoney = IIf(App.oDefault.nDecPntMoney = 0, 2, App.oDefault.nDecPntMoney)


    '        If pbNoComma Then
    '            SP_CNFMTcCurPoint = Format(pcValue, "########0." & StrDup(App.oDefault.nDecPntMoney, "0"))
    '        Else
    '            SP_CNFMTcCurPoint = Format(pcValue, "###,###,##0." & StrDup(App.oDefault.nDecPntMoney, "0"))
    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Function

    'Public Function SP_CNDATcRoundDiff(ByVal ptDiff$) As Double
    '    '----------------------------------------------------------
    '    '    'PUM 09/06/2010 คำนวนปัดเศษตาม Option
    '    '----------------------------------------------------------

    '    Dim nDif, cDivLef, cDivValue, cModValue, CMean As Double
    '    Dim nFlag As Integer

    '    SP_CNDATcRoundDiff = 0.0

    '    Try
    '        Select Case App.oDefault.tRoundingType
    '            Case Is = "1" 'ปัดเศษขึ้น
    '                cDivLef = Int(ptDiff / App.oDefault.cAmtRound)
    '                cDivValue = ptDiff / App.oDefault.cAmtRound
    '                cModValue = ptDiff Mod App.oDefault.cAmtRound

    '                If cModValue = 0 Then
    '                    nDif = 0
    '                Else
    '                    If cDivValue > 0 Then
    '                        cDivValue = 1
    '                        nFlag = cDivLef + cDivValue
    '                        nDif = (nFlag * App.oDefault.cAmtRound) - ptDiff
    '                    End If
    '                End If

    '            Case Is = "2" 'ปัดเศษลง
    '                cDivValue = ptDiff / App.oDefault.cAmtRound
    '                cModValue = ptDiff Mod App.oDefault.cAmtRound

    '                If cModValue = 0 Then
    '                    nDif = 0
    '                Else
    '                    If cDivValue < 1 Then
    '                        cDivValue = 0
    '                        nDif = cDivValue - ptDiff

    '                    Else
    '                        cDivLef = (cDivValue * App.oDefault.cAmtRound) - cModValue
    '                        nDif = (ptDiff - cDivLef) * -1
    '                    End If
    '                End If
    '            Case Is = "3" 'ค่ากลาง
    '                'Run 30/09/53
    '                CMean = App.oDefault.cAmtRound / 2
    '                cModValue = ptDiff Mod App.oDefault.cAmtRound 'CMean
    '                nFlag = Int(ptDiff / CMean)

    '                If cModValue = 0 Then
    '                    nDif = 0
    '                Else

    '                    Dim nB4Diff As Double = Int(ptDiff)
    '                    Dim nRndiff As Double = FormatNumber(CDbl(ptDiff) - nB4Diff, 2)

    '                    If App.oDefault.cAmtRound > 1 Then
    '                        nRndiff = CDbl(ptDiff)
    '                    End If

    '                    While True
    '                        If nRndiff < App.oDefault.cAmtRound Then
    '                            If nRndiff >= CMean Then
    '                                nFlag += 1

    '                            End If
    '                            Exit While
    '                        End If
    '                        nRndiff = FormatNumber(nRndiff - App.oDefault.cAmtRound, 2)
    '                    End While

    '                    If ptDiff > CMean Then
    '                        'nFlag += 1
    '                        nDif = (nFlag * CMean) - ptDiff
    '                    Else
    '                        nDif = (nFlag * CMean) - ptDiff
    '                    End If
    '                End If

    '        End Select

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    SP_CNDATcRoundDiff = nDif

    'End Function

    ''' <summary>
    ''' BulkCopy Datatable to SqlServer
    ''' </summary>
    ''' <param name="oData"></param>
    ''' <param name="oField"></param>
    ''' <returns></returns>
    Public Function C_COPtData(oData As DataTable, Optional oField As ArrayList = Nothing) As String
        Dim sqlBulkCopy As New SqlBulkCopy(AdaConfig.cConfig.tSQLConnSourceString, SqlBulkCopyOptions.KeepIdentity)
        Dim tMsg As String = ""
        Try
            sqlBulkCopy.DestinationTableName = oData.TableName
            sqlBulkCopy.BulkCopyTimeout = 3600000
            Dim nCount As Integer = 0
            For Each oCol In oData.Columns
                If oData.TableName <> "TCNMPdt" Then
                    'Nauy 2016-06-28
                    sqlBulkCopy.ColumnMappings.Add(oCol.ToString.Trim, oCol.ToString.Trim)
                Else
                    If oCol.ToString <> "FNLnkLine" And oCol.ToString <> "FNLnkStatus" And oCol.ToString <> "FTLnkLog" And oCol.ToString <> "FNLnkPrc" And oCol.ToString <> "FTChainRmk" And oCol.ToString <> "FTPbnName" Then
                        sqlBulkCopy.ColumnMappings.Add(oCol.ToString.Trim, oCol.ToString.Trim)
                    End If
                End If
                nCount = nCount + 1
            Next
            '[OPTIONAL]: Map the DataTable columns with that of the database table
            sqlBulkCopy.WriteToServer(oData)
        Catch ex As SqlException
            If ex.Message.Contains("Received an invalid column length from the bcp client for colid") Then
                Dim pattern As String = "\d+"
                Dim match As Match = Regex.Match(ex.Message.ToString(), pattern)
                Dim index = Convert.ToInt32(match.Value) - 1

                Dim fi As FieldInfo = GetType(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic Or BindingFlags.Instance)
                Dim sortedColumns = fi.GetValue(sqlBulkCopy)
                Dim items = DirectCast(sortedColumns.[GetType]().GetField("_items", BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(sortedColumns), [Object]())

                Dim itemdata As FieldInfo = items(index).[GetType]().GetField("_metadata", BindingFlags.NonPublic Or BindingFlags.Instance)
                Dim metadata = itemdata.GetValue(items(index))

                Dim column = metadata.[GetType]().GetField("column", BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(metadata)
                Dim length = metadata.[GetType]().GetField("length", BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(metadata)
                '  Throw New DataFormatException([String].Format("Column: {0} contains data with a length greater than: {1}", column, length))
            End If
        Catch ex As Exception
            tMsg = ex.ToString
        End Try
        Return tMsg
    End Function

    Public Sub SP_ALTxTBLTemp()
        '*EYE 59-03-07
        Dim oDatabase As New cDatabaseLocal
        Dim tSql As String
        tSql = " ALTER TABLE TLNKPdt ALTER Column FTPdtNameOth NVARCHAR(100);"

        oDatabase.C_CALoExecuteReader(tSql)
    End Sub
    Public Function C_ADDaStrField() As ArrayList
        '*EYE 59-03-07
        Dim tField As New ArrayList
        tField.Add("FTPdtCode")
        tField.Add("FTPdtName")
        tField.Add("FTPdtNameOth")
        tField.Add("FTPdtBarCode1")
        tField.Add("FTPdtBarCode2")
        tField.Add("FTPdtBarCode3")
        tField.Add("FTPdtStkCode")
        tField.Add("FCPdtStkFac")
        tField.Add("FTPdtStkControl")
        tField.Add("FCPdtQtyRet")
        tField.Add("FCPdtQtyWhs")
        tField.Add("FCPdtQtyDRet")
        tField.Add("FCPdtQtyDWhs")
        tField.Add("FCPdtQtyMRet")
        tField.Add("FCPdtQtyMWhs")
        tField.Add("FCPdtQtyOrdBuy")
        tField.Add("FCPdtCostAvg")
        tField.Add("FCPdtCostFiFo")
        tField.Add("FCPdtCostLast")
        tField.Add("FCPdtCostDef")
        tField.Add("FCPdtCostOth")
        tField.Add("FCPdtCostAmt")
        tField.Add("FCPdtCostStd")
        tField.Add("FTPgpChain")
        tField.Add("FTSplCode")
        tField.Add("FTUsrCode")
        tField.Add("FTPdtVatType")
        tField.Add("FCPdtMin")
        tField.Add("FCPdtMax")
        tField.Add("FTPdtSaleType")
        tField.Add("FTPdtSUnit")
        tField.Add("FCPdtSFactor")
        tField.Add("FTPdtMUnit")
        tField.Add("FCPdtMFactor")
        tField.Add("FTPdtLUnit")
        tField.Add("FCPdtLFactor")
        tField.Add("FTPdtGrade")
        tField.Add("FCPdtWeight")
        tField.Add("FTPszCode")
        tField.Add("FTClrCode")
        tField.Add("FTPtyCode")
        tField.Add("FTPdtPoint")
        tField.Add("FCPdtPointTime")
        tField.Add("FTPmhCodeS")
        tField.Add("FTPmhTypeS")
        tField.Add("FDPdtPmtSDateS")
        tField.Add("FDPdtPmtEDateS")
        tField.Add("FTPdtPmtTypeS")
        tField.Add("FTPmhCodeM")
        tField.Add("FTPmhTypeM")
        tField.Add("FDPdtPmtSDateM")
        tField.Add("FDPdtPmtEDateM")
        tField.Add("FTPdtPmtTypeM")
        tField.Add("FTPmhCodeL")
        tField.Add("FTPmhTypeL")
        tField.Add("FDPdtPmtSDateL")
        tField.Add("FDPdtPmtEDateL")
        tField.Add("FTPdtPmtTypeL")
        tField.Add("FTPdtConType")
        tField.Add("FTPdtSrn")
        tField.Add("FTPdtPlcCodeS")
        tField.Add("FTPdtPlcCodeM")
        tField.Add("FTPdtPlcCodeL")
        tField.Add("FTPdtAlwOrderS")
        tField.Add("FTPdtAlwOrderM")
        tField.Add("FTPdtAlwOrderL")
        tField.Add("FTPdtAlwBuyS")
        tField.Add("FTPdtAlwBuyM")
        tField.Add("FTPdtAlwBuyL")
        tField.Add("FCPdtRetPriS1")
        tField.Add("FCPdtRetPriM1")
        tField.Add("FCPdtRetPriL1")
        tField.Add("FCPdtRetPriS2")
        tField.Add("FCPdtRetPriM2")
        tField.Add("FCPdtRetPriL2")
        tField.Add("FCPdtRetPriS3")
        tField.Add("FCPdtRetPriM3")
        tField.Add("FCPdtRetPriL3")
        tField.Add("FCPdtWhsPriS1")
        tField.Add("FCPdtWhsPriM1")
        tField.Add("FCPdtWhsPriL1")
        tField.Add("FCPdtWhsPriS2")
        tField.Add("FCPdtWhsPriM2")
        tField.Add("FCPdtWhsPriL2")
        tField.Add("FCPdtWhsPriS3")
        tField.Add("FCPdtWhsPriM3")
        tField.Add("FCPdtWhsPriL3")
        tField.Add("FCPdtWhsPriS4")
        tField.Add("FCPdtWhsPriM4")
        tField.Add("FCPdtWhsPriL4")
        tField.Add("FCPdtWhsPriS5")
        tField.Add("FCPdtWhsPriM5")
        tField.Add("FCPdtWhsPriL5")
        tField.Add("FTPdtWhsDefUnit")
        tField.Add("FTPdtOthPurSplN1")
        tField.Add("FTPdtOthPurSplN2")
        tField.Add("FTPdtOthPurSplN3")
        tField.Add("FCPdtOthPurSplP1")
        tField.Add("FCPdtOthPurSplP2")
        tField.Add("FCPdtOthPurSplP3")
        tField.Add("FTPdtOthPurSplCmt")
        tField.Add("FTPdtOthSleSplN1")
        tField.Add("FTPdtOthSleSplN2")
        tField.Add("FTPdtOthSleSplN3")
        tField.Add("FCPdtOthSleSplP1")
        tField.Add("FCPdtOthSleSplP2")
        tField.Add("FCPdtOthSleSplP3")
        tField.Add("FTPdtOthSleSplCmt")
        tField.Add("FTAccCode")
        tField.Add("FTPdtPic")
        tField.Add("FTPdtSound")
        tField.Add("FTPdtStaDel")
        tField.Add("FTPdtType")
        tField.Add("FTPdtBarByGen")
        tField.Add("FTPdtStaSet")
        tField.Add("FTPdtStaSetPri")
        tField.Add("FTPdtStaSetShwDT")
        tField.Add("FTPdtStaActive")
        tField.Add("FTPdtRmk")
        tField.Add("FTPbnCode")
        tField.Add("FCPdtLeftPO")
        tField.Add("FTPdtTax")
        tField.Add("FTPmoCode")
        tField.Add("FTDcsCode")
        tField.Add("FTDepCode")
        tField.Add("FTClsCode")
        tField.Add("FTSclCode")
        tField.Add("FCPdtQtyNow")
        tField.Add("FTPdtArticle")
        tField.Add("FTPdtGrpControl")
        tField.Add("FTPdtShwTch")
        tField.Add("FTPdtNoDis")
        tField.Add("FCPdtVatRate")
        tField.Add("FTTcgCode")
        tField.Add("FTPdtNameShort")
        tField.Add("FTPdtNameShortEng")
        tField.Add("FTPdtStaAlwBuy")
        tField.Add("FTFixGonCode")
        tField.Add("FCPdtLifeCycle")
        tField.Add("FTPdtOrdDay")
        tField.Add("FTPdtOrdSun")
        tField.Add("FTPdtOrdMon")
        tField.Add("FTPdtOrdTue")
        tField.Add("FTPdtOrdWed")
        tField.Add("FTPdtOrdThu")
        tField.Add("FTPdtOrdFri")
        tField.Add("FTPdtOrdSat")
        tField.Add("FTPdtDim")
        tField.Add("FTPdtPkgDim")
        tField.Add("FCPdtLeadTime")
        tField.Add("FDPdtOrdStart")
        tField.Add("FDPdtOrdStop")
        tField.Add("FDPdtSaleStart")
        tField.Add("FDPdtSaleStop")
        tField.Add("FTPdtStaTouch")
        tField.Add("FTPdtStaAlwRepack")
        tField.Add("FTPdtGateWhe")
        tField.Add("FNPdtPalletSize")
        tField.Add("FNPdtPalletLev")
        tField.Add("FNPdtAge")
        tField.Add("FNPdtAgeB4Rcv")
        tField.Add("FNPdtAgeB4Snd")
        tField.Add("FTItyCode")
        tField.Add("FTPdtDeliveryBy")
        tField.Add("FTMcrCode")
        tField.Add("FTSlcItemCode")
        tField.Add("FTPdtPic4Slip")
        tField.Add("FDPdtMfg")
        tField.Add("FDPdtExp")
        tField.Add("FTPdtUnitSymbol")
        tField.Add("FTPdtLabSizeS")
        tField.Add("FNPdtLabQtyS")
        tField.Add("FTPdtLabSizeM")
        tField.Add("FNPdtLabQtyM")
        tField.Add("FTPdtLabSizeL")
        tField.Add("FNPdtLabQtyL")
        tField.Add("FTPdtAlwPickS")
        tField.Add("FTPdtAlwPickM")
        tField.Add("FTPdtAlwPickL")
        tField.Add("FTPdtStaFastF")
        tField.Add("FNPdtOthSystem")
        tField.Add("FDDateUpd")
        tField.Add("FTTimeUpd")
        tField.Add("FTWhoUpd")
        tField.Add("FDDateIns")
        tField.Add("FTTimeIns")
        tField.Add("FTWhoIns")
        tField.Add("FTPbnCode")

        Return tField
    End Function
    Public Function C_ADDaStrPdtBrandField() As ArrayList
        '*EYE 59-03-07
        Dim tField As New ArrayList
        tField.Add("FTPbnCode")
        tField.Add("FTPbnName")
        tField.Add("FDDateUpd")
        tField.Add("FTTimeUpd")
        tField.Add("FTWhoUpd")
        tField.Add("FDDateIns")
        tField.Add("FTTimeIns")
        tField.Add("FTWhoIns")
        Return tField
    End Function

    'Public Function C_GEToTCNMPdt() As DataTable
    '    Dim oDTTCNMPdt As DataTable
    '    Dim oDatabase As New cDatabaseLocal
    '    Dim oSB As New StringBuilder("")
    '    oSB.AppendLine("Select FTPdtCode, FTPdtName, FTPdtBarCode1 As FTIpdBarCode, FTPdtStkCode As FTIpdStkCode, FCPdtStkFac As FCIpdStkFac, ISNULL(FTPgpChain,'') AS FTPgpChain,FTPdtSUnit AS FTPunCode,FCPdtSFactor AS FCIpdFactor,FCPdtRetPriS1 AS FCIpdPriOld,1 AS FNIpdUnitType,FCPdtCostAvg*((100+(SELECT FCVatRate FROM TCNMVatRate WHERE FTVatCode IN (SELECT TOP 1 FTVatCode FROM TCNMComp)))/100) AS FCIpdCost,ISNULL(FTPdtArticle,'') AS FTPdtArticle,ISNULL(FTDcsCode,'') AS FTDcsCode,ISNULL(FTPszCode,'') AS FTPszCode,ISNULL(FTClrCode,'') AS FTClrCode,FTPdtNoDis FROM TCNMPdt WHERE ISNULL(FTPdtBarCode1,'')<>'' ")
    '    oSB.AppendLine("UNION")
    '    oSB.AppendLine("SELECT FTPdtCode,FTPdtName,FTPdtBarCode2 AS FTIpdBarCode,FTPdtStkCode AS FTIpdStkCode,FCPdtStkFac AS FCIpdStkFac,ISNULL(FTPgpChain,'') AS FTPgpChain,FTPdtMUnit AS FTPunCode,FCPdtMFactor AS FCIpdFactor,FCPdtRetPriM1 AS FCIpdPriOld,2 AS FNIpdUnitType,FCPdtCostAvg*((100+(SELECT FCVatRate FROM TCNMVatRate WHERE FTVatCode IN (SELECT TOP 1 FTVatCode FROM TCNMComp)))/100) AS FCIpdCost,ISNULL(FTPdtArticle,'') AS FTPdtArticle,ISNULL(FTDcsCode,'') AS FTDcsCode,ISNULL(FTPszCode,'') AS FTPszCode,ISNULL(FTClrCode,'') AS FTClrCode,FTPdtNoDis FROM TCNMPdt WHERE ISNULL(FTPdtBarCode2,'')<>''  ")
    '    oSB.AppendLine("UNION")
    '    oSB.AppendLine("SELECT FTPdtCode,FTPdtName,FTPdtBarCode3 AS FTIpdBarCode,FTPdtStkCode AS FTIpdStkCode,FCPdtStkFac AS FCIpdStkFac,ISNULL(FTPgpChain,'') AS FTPgpChain,FTPdtLUnit AS FTPunCode,FCPdtLFactor AS FCIpdFactor,FCPdtRetPriL1 AS FCIpdPriOld,3 AS FNIpdUnitType,FCPdtCostAvg*((100+(SELECT FCVatRate FROM TCNMVatRate WHERE FTVatCode IN (SELECT TOP 1 FTVatCode FROM TCNMComp)))/100) AS FCIpdCost,ISNULL(FTPdtArticle,'') AS FTPdtArticle,ISNULL(FTDcsCode,'') AS FTDcsCode,ISNULL(FTPszCode,'') AS FTPszCode,ISNULL(FTClrCode,'') AS FTClrCode,FTPdtNoDis FROM TCNMPdt WHERE ISNULL(FTPdtBarCode3,'')<>''  ")

    '    oDTTCNMPdt = oDatabase.C_CALoExecuteReader(oSB.ToString, "TCNMPdt")
    '    If oDTTCNMPdt Is Nothing AndAlso oDTTCNMPdt.Rows.Count = 0 Then
    '        Return Nothing
    '    End If
    '    Return oDTTCNMPdt
    'End Function
    'Public Function C_GEToOldPrice() As DataTable
    '    Dim oDBTBL As DataTable
    '    Dim oDatabase As New cDatabaseLocal
    '    Dim oSB As New StringBuilder("")
    '    oSB.AppendLine("select FCPdtRetPri1, FCPdtRetPri2, FCPdtRetPri3,FTPdtStkCode,FTpdtUnit")
    '    oSB.AppendLine("From dbo.SI_VCNMPdtByBar")
    '    oDBTBL = oDatabase.C_CALoExecuteReader(oSB.ToString, "dbo.SI_VCNMPdtByBar")
    '    If oDBTBL Is Nothing AndAlso oDBTBL.Rows.Count = 0 Then
    '        Return Nothing
    '    End If
    '    Return oDBTBL
    'End Function
    Public Function C_GEToBchMapping(ByVal poDB As DataTable) 'PAN 2016-05-27
        Dim oDatabase As New cDatabaseLocal
        Dim tBch As String = ""
        Dim tSql As String = "SELECT FTLNMUsrValue,FTLNMDefValue FROM TLNKMapping where FTLNMTYPE = '9' AND FTLNMStaUse = '1'"
        Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TLNKMapping")
        If oDbTbl IsNot Nothing Then
            If oDbTbl.Rows.Count > 0 Then
                tBch = oDbTbl(0)("FTLNMUsrValue")
            End If
        End If
        Return oDbTbl
    End Function

    Public Function C_GEToGrpMapping(ByVal poDB As DataTable) 'PAN 2016-05-27
        Dim oDatabase As New cDatabaseLocal
        Dim tGrp As String = ""
        Dim tSql As String = "SELECT FTLNMUsrValue,FTLNMDefValue FROM TLNKMapping where FTLNMTYPE = '10' AND FTLNMStaUse = '1'"
        Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TLNKMapping")
        If oDbTbl IsNot Nothing Then
            If oDbTbl.Rows.Count > 0 Then
                tGrp = oDbTbl(0)("FTLNMUsrValue")
            End If
        End If
        Return oDbTbl
    End Function

    ''' <summary>
    ''' Insert To Table from Datatable
    ''' </summary>
    ''' <param name="poData"></param>
    ''' <param name="ptTblName"></param>
    ''' <returns>Message Error</returns>
    Public Function SP_INStInsertFromDatatable(ByVal poData As DataTable, ByVal ptTblName As String) As String
        Dim tStr As String = ""
        Dim tMsg As String = ""
        Dim oDatabase As New cDatabaseLocal
        Try
            tStr = "INSERT INTO " & ptTblName & " (" & vbCrLf
            For Each oCol In poData.Columns
                tStr &= oCol.ToString.Trim & ","
            Next
            tStr = Left(tStr, tStr.Length - 1) & ")" & vbCrLf
            For nRow = 0 To poData.Rows.Count - 1
                If nRow = 0 Then
                    tStr &= "SELECT "
                Else
                    tStr = Left(tStr, tStr.Length - 1) & vbCrLf
                    tStr &= "UNION ALL SELECT "
                End If
                For nItem = 0 To poData.Rows(nRow).ItemArray.Count - 1
                    Dim tItem = poData.Rows(nRow)(nItem).ToString
                    If Not IsDBNull(tItem) Then
                        Select Case Left(poData.Columns(nItem).ToString, 2)
                            Case "FT"
                                tStr &= "'" & poData.Rows(nRow)(nItem) & "',"
                            Case "FD"
                                tStr &= "'" & Format(poData.Rows(nRow)(nItem), "yyyy/MM/dd") & "',"
                            Case "FC", "FN"
                                If tItem = "" Then
                                    tStr &= "NULL,"
                                Else
                                    tStr &= tItem & ","
                                End If
                        End Select
                    Else
                        tStr &= poData.Rows(nRow)(nItem) & ","
                    End If
                Next
            Next
            tStr = Left(tStr, tStr.Length - 1) & vbCrLf
            oDatabase.C_CALnExecuteNonQuery(tStr)
        Catch ex As Exception
            tMsg = ex.Message
        Finally
            oDatabase = Nothing
        End Try
        Return tMsg
    End Function

    ''' <summary>
    ''' Get Branch Mapping
    ''' </summary>
    Public Function SP_GETtBranchMap(ByVal ptBchCode As String, Optional ByVal ptBchMap As String = "") As String
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tBchMap As String = ""
        Try
            'Mapping
            oSql = New System.Text.StringBuilder
            If ptBchMap = "" Then
                oSql.AppendLine("SELECT FTLNMCode, CASE WHEN ISNULL(FTLNMUsrValue,'') = '' THEN ISNULL(FTLNMDefValue,'') ELSE ISNULL(FTLNMUsrValue,'') END Value")
                oSql.AppendLine("FROM TLNKMapping")
                oSql.AppendLine("WHERE FTLNMCode = 'STORENO' AND FTLNMDefValue = '" & ptBchCode & "'")
            Else
                oSql.AppendLine("SELECT FTLNMCode, CASE WHEN ISNULL(FTLNMDefValue,'') = '' THEN ISNULL(FTLNMUsrValue,'') ELSE ISNULL(FTLNMDefValue,'') END Value")
                oSql.AppendLine("FROM TLNKMapping")
                oSql.AppendLine("WHERE FTLNMCode = 'STORENO' AND FTLNMUsrValue = '" & ptBchMap & "'")
            End If
            oDbTbl = New DataTable
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                tBchMap = oDbTbl.Rows(0)("Value").ToString
            Else
                tBchMap = ""
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return tBchMap
    End Function

    ''' <summary>
    ''' Get Plant Mapping
    ''' </summary>
    Public Function SP_GETtPlantMap(ByVal ptBchCode As String, Optional ByVal ptBchMap As String = "") As String
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tBchMap As String = ""
        Try
            'Mapping
            oSql = New System.Text.StringBuilder
            If ptBchMap = "" Then
                oSql.AppendLine("SELECT FTLNMCode, CASE WHEN ISNULL(FTLNMUsrValue,'') = '' THEN ISNULL(FTLNMDefValue,'') ELSE ISNULL(FTLNMUsrValue,'') END Value")
                oSql.AppendLine("FROM TLNKMapping")
                oSql.AppendLine("WHERE FTLNMCode = 'PLANTNO' AND FTLNMDefValue = '" & ptBchCode & "'")
            Else
                oSql.AppendLine("SELECT FTLNMCode, CASE WHEN ISNULL(FTLNMDefValue,'') = '' THEN ISNULL(FTLNMUsrValue,'') ELSE ISNULL(FTLNMDefValue,'') END Value")
                oSql.AppendLine("FROM TLNKMapping")
                oSql.AppendLine("WHERE FTLNMCode = 'PLANTNO' AND FTLNMUsrValue = '" & ptBchMap & "'")
            End If
            oDbTbl = New DataTable
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                tBchMap = oDbTbl.Rows(0)("Value").ToString
            Else
                tBchMap = ""
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return tBchMap
    End Function

    Public Function SP_PRCxPdtQtyNow(ByVal ptTblName$, ByVal ptFldDocNo$, ByVal ptFldStkCode$, ByVal ptFldPdtLevel$ _
                                    , ByVal ptFldQtyAll$, ByVal ptDocNo$, ByVal ptSign$, Optional ByRef poException As Exception = Nothing _
                                    , Optional ByVal pbPdtSet As Boolean = False, Optional ByVal pbChkStaSet As Boolean = True _
                                    , Optional ByVal pbStaSetChild As Boolean = False) As Boolean
        '----------------------------------------------------------- 
        '   Call : 
        '   Description : 
        '   Developer : SOMJAI   '*KT 58-07-23 RQ1506-036 เพิ่มเงื่อนไขการกรณีสินค้าชุดที่เป็นสินค้าลูก ตาม Option 1(nVB_CNPdtSet = 1 ) 
        '   Date Create : 11/06/2014 :  10:35:08 
        '----------------------------------------------------------- 
        Dim oDatabase As New cDatabaseLocal
        Dim bResult As Boolean = True
        Try
            Dim tSql
            Dim nRet As Integer = 0
            Dim nRetry%

            tSql = "UPDATE TCNTPdtQtyNotApv WITH(ROWLOCK)"
            '    tSql = tSql & " Set FCPdtQtyNow = IsNull(QTY.FCPdtQtyNow, 0) " & ptSign & " IsNull(DT." & ptFldQtyAll & ", 0)" 

            '*KT 60-03-15 RQ1701-014,RQ1703-023  ระหว่างแก้ไขเรื่องค้างอนุมัติ พบสาเหตุของเรื่องคงเหลือขายได้ไม่ถูกต้อง เมื่อมีการตรวจนับน้อยกว่าจำนวนคงเหลือ(QtyDiff <0) 
            Select Case UCase(ptTblName)
                Case UCase("TCNTPdtChkSalHD"), UCase("TCNTPdtChkHD"), UCase("TCNTPdtChkSalDT"), UCase("TCNTPdtChkDT")
                    tSql = tSql & " Set FCPdtQtyNow = IsNull(QTY.FCPdtQtyNow, 0) " & ptSign & " IsNull(DT." & ptFldQtyAll & ", 0)"
                Case Else
                    tSql = tSql & " Set FCPdtQtyNow = IsNull(QTY.FCPdtQtyNow, 0) " & ptSign & " (CASE WHEN IsNull(DT." _
                           & ptFldQtyAll & ", 0) < 0 AND '" & ptSign & "' = '-' THEN IsNull(DT." & ptFldQtyAll & ", 0)*-1 ELSE IsNull(DT." _
                           & ptFldQtyAll & ", 0) END)"   '*Em 57-07-19  ถ้าค่าติดลบจะทำให้ QtyNow ไม่ถูกต้อง 
            End Select

            tSql = tSql & " FROM (SELECT " & ptFldStkCode & ",SUM(ISNULL(" & ptFldQtyAll & ",0))AS " & ptFldQtyAll
            tSql = tSql & "     FROM " & ptTblName & " WITH(NOLOCK)"
            tSql = tSql & "     WHERE " & ptFldDocNo & "  ='" & ptDocNo & "'"
            If pbChkStaSet Then
                If pbPdtSet Then
                    If cCNVB.nVB_CNPdtSet = 1 Then
                        If pbStaSetChild Then '*KT 58-07-23 RQ1506-036 
                            tSql = tSql & " AND (ISNULL(FTPdtStaSet,'1') = '3' AND " & ptFldPdtLevel & " = 1 )"
                        Else
                            tSql = tSql & " AND (ISNULL(FTPdtStaSet,'1') = '3' AND " & ptFldPdtLevel & " = 0 )"
                        End If
                    Else
                        tSql = tSql & " AND (ISNULL(FTPdtStaSet,'1') = '3' AND " & ptFldPdtLevel & " = 1 )"
                    End If
                Else
                    ' tSql = tSql & " AND ISNULL(FTPdtStaSet,'1') <> '3' " 
                    If cCNVB.nVB_CNPdtSet = 1 Then
                        tSql = tSql & " AND (ISNULL(FTPdtStaSet,'1') <> '3' OR (ISNULL(FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=0))"
                    Else
                        tSql = tSql & " AND (ISNULL(FTPdtStaSet,'1') <> '3' OR (ISNULL(FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=1))"
                    End If
                End If
            Else
                If cCNVB.nVB_CNPdtSet = 1 Then
                    tSql = tSql & " AND (ISNULL(FTPdtStaSet,'1') <> '3' OR (ISNULL(FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=0))"
                Else
                    tSql = tSql & " AND (ISNULL(FTPdtStaSet,'1') <> '3' OR (ISNULL(FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=1))"
                End If
            End If

            tSql = tSql & " GROUP BY " & ptFldStkCode & ") DT"
            tSql = tSql & " INNER JOIN TCNTPdtQtyNotApv QTY ON DT." & ptFldStkCode & " = QTY.FTPdtStkCode"
            tSql = tSql & " INNER JOIN TCNMPdt Pdt WITH(NOLOCK) ON DT." & ptFldStkCode & " = Pdt.FTPdtStkCode"
            tSql = tSql & " WHERE Pdt.FCPdtSFactor = 1 AND  (Pdt.FTPdtStkControl = '1') "

            '  nRet = SP_SQLvExecute(tSql)
            nRet = oDatabase.C_CALnExecuteNonQuery(tSql)
            nRetry = 0

            Do While nRet <> 0 And nRetry < 3
                Call SleepEx(1000, 0)
                nRet = oDatabase.C_CALnExecuteNonQuery(tSql)
                nRetry = nRetry + 1
            Loop

            tSql = "INSERT INTO TCNTPdtQtyNotApv WITH(ROWLOCK)(FTPdtStkCode,FCPdtDayEnd,FDPdtDayEnd,FCPdtQtyNow)"
            tSql = tSql & " SELECT DT." & ptFldStkCode & ",0,null,(SUM(DT." & ptFldQtyAll & ")*(" & ptSign & "1)) AS FCPdtQtyNow"
            tSql = tSql & " FROM " & ptTblName & " DT  WITH(NOLOCK) LEFT JOIN TCNTPdtQtyNotApv QTY ON DT." & ptFldStkCode & " = QTY.FTPdtStkCode"
            tSql = tSql & " INNER JOIN TCNMPdt Pdt  WITH(NOLOCK) ON DT." & ptFldStkCode & " = Pdt.FTPdtStkCode"
            tSql = tSql & " WHERE QTY.FTPdtStkCode Is Null"
            tSql = tSql & " AND DT." & ptFldDocNo & "  ='" & ptDocNo & "'"
            tSql = tSql & " AND Pdt.FCPdtSFactor = 1 AND  (Pdt.FTPdtStkControl = '1') "
            If pbChkStaSet Then
                If pbPdtSet Then
                    If cCNVB.nVB_CNPdtSet = 1 Then
                        If pbStaSetChild Then '*KT 58-07-23 RQ1506-036 
                            tSql = tSql & " AND (ISNULL(DT.FTPdtStaSet,'1') = '3' AND DT." & ptFldPdtLevel & " = 1 )"
                        Else
                            tSql = tSql & " AND (ISNULL(DT.FTPdtStaSet,'1') = '3' AND DT." & ptFldPdtLevel & " = 0 )"
                        End If
                    Else
                        tSql = tSql & " AND (ISNULL(DT.FTPdtStaSet,'1') = '3' AND DT." & ptFldPdtLevel & " = 1 )"
                    End If
                Else
                    'tSql = tSql & " AND ISNULL(DT.FTPdtStaSet,'1') <> '3' " 
                    If cCNVB.nVB_CNPdtSet = 1 Then
                        tSql = tSql & " AND (ISNULL(DT.FTPdtStaSet,'1') <> '3' OR (ISNULL(DT.FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=0))"
                    Else
                        tSql = tSql & " AND (ISNULL(DT.FTPdtStaSet,'1') <> '3' OR (ISNULL(DT.FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=1))"
                    End If
                End If
            Else
                If cCNVB.nVB_CNPdtSet = 1 Then
                    tSql = tSql & " AND (ISNULL(DT.FTPdtStaSet,'1') <> '3' OR (ISNULL(DT.FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=0))"
                Else
                    tSql = tSql & " AND (ISNULL(DT.FTPdtStaSet,'1') <> '3' OR (ISNULL(DT.FTPdtStaSet,'1') ='3' AND " & ptFldPdtLevel & "=1))"
                End If
            End If

            tSql = tSql & " GROUP BY DT." & ptFldStkCode & ""
            'nRet = SP_SQLvExecute(tSql)
            nRet = oDatabase.C_CALnExecuteNonQuery(tSql)
            nRetry = 0
            Do While nRet <> 0 And nRetry < 3
                Call SleepEx(1000, 0)
                ' nRet = SP_SQLvExecute(tSql)
                nRet = oDatabase.C_CALnExecuteNonQuery(tSql)
                nRetry = nRetry + 1
            Loop


            Return bResult
        Catch ex As Exception
            poException = ex
            Return False
        End Try

    End Function

    Declare Function SleepEx Lib "kernel32" (ByVal dwMilliseconds As Integer, ByVal bAlertable As Integer) As Integer

End Module