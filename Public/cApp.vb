﻿Imports System.IO
Imports System.Data.SqlClient

Public Class cApp

    Public Shared ReadOnly Property tSQLCmdLog As String
        Get
            Return "INSERT INTO TLNKLog (uid,FDDateTime, FNType, FTFileName, FTDescription, FTRefer) VALUES ('" & Guid.NewGuid.ToString & "',GETDATE(),{0},'{1}','{2}',SUBSTRING('{3}',1,255))"
        End Get
    End Property

    Public Shared ReadOnly Property tSQLCmdCheckTableTemp As String
        Get
            Dim tSql As String = ""
            tSql = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TLNKLog]') AND type in (N'U'))" & vbCrLf
            tSql &= "BEGIN" & vbCrLf
            tSql &= "CREATE TABLE [dbo].[TLNKLog](" & vbCrLf
            tSql &= "	[uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL," & vbCrLf
            tSql &= "	[FDDateTime] [datetime] ," & vbCrLf
            tSql &= "	[FNType] [int] NULL," & vbCrLf
            tSql &= "	[FTFileName] [varchar](200) COLLATE Thai_CI_AS NULL," & vbCrLf
            tSql &= "	[FTDescription] [varchar](MAX) COLLATE Thai_CI_AS NULL," & vbCrLf
            tSql &= "	[FTRefer][varchar](MAX) COLLATE Thai_CI_AS NULL" & vbCrLf
            tSql &= " CONSTRAINT [PK_TLNKLog] PRIMARY KEY CLUSTERED " & vbCrLf
            tSql &= "(" & vbCrLf
            tSql &= "            [uid] Asc" & vbCrLf
            tSql &= ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
            tSql &= ") ON [PRIMARY]" & vbCrLf
            tSql &= "END"
            Return tSql

        End Get
    End Property

    Public Shared ReadOnly Property C_CALaCheckTableLog As List(Of String)
        Get
            Dim tSql As String = ""
            tSql = "DECLARE @TableTemp TABLE( FTColumn VARCHAR(20),FTStatus VARCHAR(1))" & vbCrLf
            tSql &= "DECLARE @NextString NVARCHAR(255)" & vbCrLf
            tSql &= ", @Pos INT" & vbCrLf
            tSql &= ", @NextPos INT" & vbCrLf
            tSql &= ", @String NVARCHAR(255)" & vbCrLf
            tSql &= ", @Delimiter NVARCHAR(1)" & vbCrLf
            tSql &= ", @H VARCHAR(1)" & vbCrLf
            tSql &= "SET @String ='TLNKMapping" ',TLNKPdt,TLNKPdtAjpDT,TLNKPdtAjpHD,TLNKPdtBrand,TLNKPdtDetail,TLNKPdtModel,TLNKPdtType,TLNKSpl'" & vbCrLf
            tSql &= "SET @Delimiter = ','" & vbCrLf
            tSql &= "SET @String = @String + @Delimiter" & vbCrLf
            tSql &= "SET @Pos = CHARINDEX(@Delimiter,@String)" & vbCrLf
            tSql &= "WHILE (@Pos <> 0)" & vbCrLf
            tSql &= "BEGIN" & vbCrLf
            tSql &= "SET @NextString = SUBSTRING(@String,1,@Pos - 1)" & vbCrLf
            tSql &= "SET @H='0'" & vbCrLf
            tSql &= "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].['+@NextString+']') AND type in (N'U')) SET @H='1'" & vbCrLf
            tSql &= "INSERT INTO @TableTemp (FTColumn,FTStatus) values (@NextString,@H)" & vbCrLf
            tSql &= "SET @String = SUBSTRING(@String,@Pos+1,LEN(@String))" & vbCrLf
            tSql &= "SET @Pos = CHARINDEX(@Delimiter,@String)" & vbCrLf
            tSql &= "End" & vbCrLf
            tSql &= "SELECT * FROM @TableTemp WHERE FTStatus='0'"

            Dim aData As New List(Of String)
            Try
                Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                    oSQLConn.Open()
                    Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                    Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                    While oSQLRdWhe.Read()
                        aData.Add(oSQLRdWhe(0))
                    End While
                End Using
            Catch ex As Exception
            End Try
            Return aData
        End Get
    End Property

    Public Shared Function C_CALxCheckExpire() As Integer
        Dim oDatabase As New cDatabaseLocal
        Dim tSql As String = "SELECT FTLNMDefValue FROM TLNKMapping WHERE FTLNMCode='EZRO'"
        Dim oDTExpire As DataTable = oDatabase.C_CALoExecuteReader(tSql)
        If oDTExpire Is Nothing Then
            Return -1
        End If

        If oDTExpire.Rows.Count = 0 Then
            Return -2
        Else
            Try
                Dim tEnc As String = oDTExpire.Rows(0)("FTLNMDefValue")
                Dim tDec As String = cCNSP.SP_DecryptData(tEnc, AdaConfig.cCNCS.tCS_CNEncDec)
                If tDec.Length >= 9 Then
                    Dim tStatus As String = tDec.Substring(0, 1)
                    Dim tDateExpire As String = ""
                    Select Case tStatus.ToUpper
                        Case "Y"
                            tDateExpire = tDec.Substring(1, 8)
                            tDateExpire = tDateExpire.Insert(6, "-")
                            tDateExpire = tDateExpire.Insert(4, "-")
                            Dim nDayLeft As Long = DateDiff(DateInterval.Day, Now, CDate(tDateExpire))
                            If nDayLeft < 0 Then
                                'Expire
                                tSql = "UPDATE TLNKMapping SET FTLNMDefValue='" & cCNSP.SP_EncryptData(tDec.Replace("Y", "N"), AdaConfig.cCNCS.tCS_CNEncDec) & "' WHERE FTLNMCode='EZRO'"
                                oDatabase.C_CALnExecuteNonQuery(tSql)
                                Return 0
                            Else
                                Return 1
                            End If
                        Case Else
                            Return 0
                    End Select
                End If

            Catch ex As Exception
                Return -3
            End Try
            Return 0
        End If

        'Y20141115090102
        '2IliSedOfF9ZSexNlUUBwptIKDCn6YLev9Fbhtw5ze8=
    End Function

    Public Shared ReadOnly Property tSQLCmdUpdMapping As String
        Get
            Dim tSql As String = ""
            tSql = "UPDATE TLNKMapping SET FTLNMDefValue = 'C' WHERE FTLNMCode = 'SUFFIXREUSE'"
            Return tSql
        End Get
    End Property
    Public Shared Function CALxCheckSharedDirectory(ptPath As String) As Boolean 'Check SharedDrive ***PAN 2016-05-13
        If String.IsNullOrEmpty(ptPath) Then
            Return False
        End If
        Dim tPathRoot As String = Path.GetPathRoot(ptPath)
        If String.IsNullOrEmpty(tPathRoot) Then
            Return False
        End If
        Dim oPinfo As New ProcessStartInfo("net", "use " & tPathRoot)
        oPinfo.CreateNoWindow = True
        oPinfo.RedirectStandardOutput = True
        oPinfo.UseShellExecute = False
        Dim tOutput As String
        Using p As Process = Process.Start(oPinfo)
            tOutput = p.StandardOutput.ReadToEnd()
        End Using
        For Each line As String In tOutput.Split(ControlChars.Lf)
            If line.Contains("completed successfully") Then
                ' shareIsProbablyConnected
                cCNVB.bVB_ChkSave = True
                Return True

            End If
        Next

        Return False
    End Function
    Public Shared Function CALxCheckDirectory(ByVal ptArgs As String) As Boolean 'Check Directory ***PAN 2016-05-13
        'Dim path As String
        Dim oDirInfo As New IO.DirectoryInfo(ptArgs)
        If oDirInfo.Exists Then
            Return True
            'For Each Path In Directory.GetDirectories(ptArgs)
            '    If Directory.Exists(Path) Then
            '        Return True
            '    Else
            '        MsgBox("Directory [ " & ptArgs & "] is Not exits ")
            '        Return False
            '    End If
            'Next
        Else
            MsgBox("Directory [ " & ptArgs & "] is Not exits ")
            Return False
        End If
    End Function 'Main
    Public Shared Function CALxCheckSavePath(ByVal ptPath As String, ByVal ptName As String) As Boolean '**PAN 2016-05-16
        'Fix StartFolderApp
        ptPath = Application.StartupPath()
        Try
            If ptPath <> "" Then
                cCNVB.bVB_Chk = cApp.CALxCheckSharedDirectory(ptPath)
                If cCNVB.bVB_Chk = True Then
                    Return True
                ElseIf ptPath <> "" Then
                    cCNVB.bVB_ChkSave = cApp.CALxCheckDirectory(ptPath)
                    If cCNVB.bVB_ChkSave = True Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    MsgBox("Directory [ " & ptName & " ," & ptPath & "] is Not exits ")
                    Return False

                End If
            Else
                MsgBox("Directory [ " & ptPath & " ] Not found ")
                Return False

            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function
End Class
