﻿Public Class cCNCS

    Public Const tCS_AppName = "หน้าร้าน;Front"      'application name
    Public Const tCS_AppProgram = "2;เอด้าฟู๊ดคอร์ท;AdaFoodCourt"     'program name
    Public Const tCS_AppVersion = "1.0"             'program version

    'application
    Public Const tCS_CNEncDec = "AdaSoft"

    'register
    Public Const tCS_CNPwdNoUsr = "ADASOFT"
    Public Const tCS_CNFedHeadIns = "FDDateUpd,FTWhoUpd,FDDateIns,FTWhoIns)"

    Public Const tCS_CNBrwDocNo = "ค้นหา เลขที่เอกสาร;Doc No. Browser"
    'Column name
    Public Const tCS_CNColumnTha1 = "หมายเลขเอกสาร"
    Public Const tCS_CNColumnEng1 = "Documents Number"

    Public Const tCS_CNColumnTha2 = "หมายเลขบัตร;รหัสอ้างอิง;รหัสสมาชิก;ชื่อ;ชื่ออื่น"
    Public Const tCS_CNColumnEng2 = "Card No.;Ref.ID;Member No.;Name;Name Other"

    Public Const tCS_CNColumnTha3 = "หมายเลขเอกสาร;หมายเลขเอกสารอ้างอิง"
    Public Const tCS_CNColumnEng3 = "Documents Number;Document Reference"

    'Brw
    Public Const tCS_CNBrwCst = "ค้นหา สมาชิก;Member Browser"
    Public Const tCS_CNFilter = "ค้นหา...;Filter..."
    Public Const tCS_CNOk = "ตกลง;Ok"

    Public Const tCS_PRNCapForm = "พิมพ์ใบเสร็จ;Print Receipt"
    Public Const tCS_RePRNCapForm = "พิมพ์ใบกำกับภาษี;Tax Invoice"
    Public Const tCS_Exit = "ออก;Exit"
    Public Const tCS_Print = "พิมพ์;Print"
    Public Const tCS_Code = "รหัส;Code"
    Public Const tCS_Name = "ชื่อ;Name"
    Public Const tCS_Addr = "ที่อยู่;Address"
    Public Const tCS_DocNo = "เลขที่เอกสาร;Doc No."
    Public Const tCS_Balance = "ยอดรวม;Balance"
    Public Const tCS_Remark = "หมายเหตุ;Remark"
    Public Const tCS_RePrint = "พิมพ์ซ้ำใบเต็มรูป;RePrint"
    Public Const tCS_PrnFrmABB = "อ้างถึงใบอย่างย่อ;Print From ABB"
    Public Const tCS_PrnType = "ประเภทการพิมพ์;Print Type"
    Public Const tCS_PrnFormat = "รูปแบบฟอร์ม;Form Style"
    Public Const tCS_FrmA5 = "A5;A5"
    Public Const tCS_FrmA4 = "A4;A4"
    Public Const tC_PathList As String = ""

    'Caption Browse *CH 25-07-2017
    Public Const tCS_CapColumnTha As String = "รหัส;ชื่อ"
    Public Const tCS_CapColumnEng As String = "Code;Name"

End Class
