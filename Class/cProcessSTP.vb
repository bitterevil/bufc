﻿Imports System.Data.SqlClient
Imports System.Text
Imports AdaLinkSAPBUFC.Model

Public Class cProcessSTP
#Region "Call Store"

    Public Function DB_PRCb1DocApprove(ByVal poPrcSTP As cmlProcessSTP, ByVal peDocType As eDB_DocType,
                                       Optional ByRef poConnect As SqlConnection = Nothing,
                                       Optional ByRef poException As Exception = Nothing) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '       DELETE FROM TCNTmpPrcStkCard WHERE FTComNAME ='ADA42'
        '       EXEC STP_DOCxTACTVatDT1 '000','ADA42',2,'2','SS00012-000003','2012/02/21','11:53:00','Tee'
        '       EXEC STP_DOCxPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '       EXEC STP_PRCxUpdQtyNow 'ADA42','000','2012/02/21','11:53:00','Tee'
        '   Description :
        '   Developer : SOMJAI
        '   Modify for Web : SuperChuck 29-01-2013
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------

        Dim oCmd As New SqlCommand
        Dim oConn As New SqlConnection
        Try
            Dim tSql As New StringBuilder
            If poConnect Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnect
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnect
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            '*******************************'

            If Not (poPrcSTP.STP_AlwInWha Or poPrcSTP.STP_AlwStkCrd Or poPrcSTP.STP_AlwCost) Then
                Return True
            End If

            tSql.Length = 0
            tSql.Append("DELETE FROM TCNTmpPrcStkCard WHERE FTComName ='" & Environment.MachineName & "'")
            oCmd = Nothing
            oCmd = New SqlCommand(tSql.ToString, oConn)
            oCmd.ExecuteNonQuery()

            tSql.Length = 0
            tSql.Append("DELETE FROM TCNTmpPrcSrnCard WHERE FTComName ='" & Environment.MachineName & "'")
            oCmd = Nothing
            oCmd = New SqlCommand(tSql.ToString, oConn)
            oCmd.ExecuteNonQuery()

            If Not C_DOCbGenData(peDocType, poPrcSTP, poConnect) Then Return False
            If C_DOCbPrcApprove(poPrcSTP, poConnect) Then
                If poPrcSTP.STP_AlwReMonthEnd Then Call C_PRCxSTK_ReMonthEndByDoc(CDate(Now)) '*Tee 55-06-15 Stk Process
                Return True
            Else
                'Call DB_MSGShowError()
                Return False
            End If

        Catch ex As Exception
            poException = ex
            Return False
        Finally
            If poConnect Is Nothing Then
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd = Nothing
        End Try
    End Function

    Public Function C_DOCbGenData(ByVal peDocType As eDB_DocType, ByVal poSTP As cmlProcessSTP,
                                   Optional ByRef poConnect As SqlConnection = Nothing) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '   Description :
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  16:37:46
        '-----------------------------------------------------------
        Try
            C_DOCbGenData = False
            Select Case peDocType
                'Case eDB_DocType.nSTP_DOCxTCNTPdtAdjStk
                '    If Not DB_DOCbTCNTPdtAdjStk() Then
                '        'Call DB_MSGShowError()
                '        Exit Function
                '    End If
                'Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT1
                '    If Not DB_DOCbTCNTPdtTnfDT1() Then
                '        'Call DB_MSGShowError()
                '        Exit Function
                '    End If
                'Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT2
                '    If Not DB_DOCbTCNTPdtTnfDT2() Then
                '        'Call DB_MSGShowError()
                '        Exit Function
                '    End If
                'Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT3
                '    If Not DB_DOCbTCNTPdtTnfDT3() Then
                '        'Call DB_MSGShowError()
                '        Exit Function
                '    End If
                Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT4
                    If Not C_DOCbTCNTPdtTnfDT4(poSTP, poConnect) Then
                        'Call DB_MSGShowError()
                        Exit Function
                    End If
                    'Case eDB_DocType.nSTP_DOCxTACTPiDT1
                    '    If Not DB_DOCbTACTPiDT1() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTPtDT5CN
                    '    If Not DB_DOCbTACTPtDT5CN() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTPtDT7DN
                    '    If Not DB_DOCbTACTPtDT7DN() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTVatDT1
                    '    If Not DB_DOCbTACTVatDT1() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTVatDT5
                    '    If Not DB_DOCbTACTVatDT5() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTVatDT7
                    '    If Not DB_DOCbTACTVatDT7() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTVatDT9
                    '    If Not DB_DOCbTACTVatDT9() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTCsmDT1
                    '    If Not DB_DOCbTACTCsmDT1() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTCsmDT5
                    '    If Not DB_DOCbTACTCsmDT5() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTCsmDT7
                    '    If Not DB_DOCbTACTCsmDT7() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTSiDT1
                    '    If Not DB_DOCbTACTSiDT1() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTStDT5
                    '    If Not DB_DOCbTACTStDT5() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTACTStDT7
                    '    If Not DB_DOCbTACTStDT7() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If

                    'Case eDB_DocType.nSTP_DOCxTPSTSalDT1
                    '    If Not DB_DOCbTPSTSalDT1() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If
                    'Case eDB_DocType.nSTP_DOCxTPSTSalDT9
                    '    If Not DB_DOCbTPSTSalDT9() Then
                    '        'Call DB_MSGShowError()
                    '        Exit Function
                    '    End If

            End Select
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function C_DOCbTCNTPdtTnfDT4(ByVal poSTP As cmlProcessSTP,
                                          Optional ByRef poConnect As SqlConnection = Nothing) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTCNTPdtTnfDT4 '000','ADA42',7,2,'IU00012-000005','2012/02/09','13:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------

        Dim oCmd As New SqlCommand
        Dim oConn As New SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnect Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnect
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnect
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            '*******************************'
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTCNTPdtTnfDT4"

            oParam = New SqlParameter("ptBchCode", poSTP.FTBchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", poSTP.PdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", poSTP.DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", poSTP.STP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", poSTP.FTWhoUpd)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
            If poConnect Is Nothing Then
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        End Try
    End Function

    Private Function C_DOCbPrcApprove(ByVal poSTP As cmlProcessSTP, Optional ByRef poConnect As SqlConnection = Nothing) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC DB_DOCbPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '   Description :
        '   @ptComName varchar(50)
        '   @ptAlwCost  varchar(1)  --0 not calculate
        '   @ptAlwInWha  varchar(1)  --0 not calculate
        '   @ptAlwStkCrd  varchar(1)  --0 not calculate
        '   @pcDecPoint float
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(100)
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------

        Dim oCmd As New SqlCommand
        Dim oConn As New SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            If poConnect Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnect
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnect
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            '*******************************'

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxPrcApprove"

            '2
            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            '3
            oParam = New SqlParameter("ptAlwCost", IIf(poSTP.STP_AlwCost, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwInWha", IIf(poSTP.STP_AlwInWha, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwStkCrd", IIf(poSTP.STP_AlwStkCrd, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", poSTP.DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", poSTP.FTWhoUpd)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
            If poConnect Is Nothing Then
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        End Try
    End Function

    Private Function C_PRCxSTK_ReMonthEndByDoc(ByVal pdDate As Date,
                                                Optional ByRef poConnect As SqlConnection = Nothing) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '       DELETE FROM TCNTmpPrcStkCard WHERE FTComNAME ='ADA42'
        '       EXEC STP_DOCxTACTVatDT1 '000','ADA42',2,'2','SS00012-000003','2012/02/21','11:53:00','Tee'
        '       EXEC STP_DOCxPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '       EXEC STP_PRCxUpdQtyNow 'ADA42','000','2012/02/21','11:53:00','Tee'
        '   Description :
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------

        Dim oCmd As New SqlCommand
        Dim oConn As New SqlConnection
        Try
            'Dim tSql As StringBuilder
            Dim nRet As Long
            Dim oParam As SqlParameter
            If poConnect Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnect
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnect
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            '*******************************'
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_PRCxSTK_ReMonthEndByDoc"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptMeDate", Format(CDate(pdDate), "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            If poConnect Is Nothing Then
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        End Try
    End Function

    Public Function C_PRCb2UpdQtyNow(ByVal poPrcSTP As cmlProcessSTP, Optional ByRef poConnect As SqlConnection = Nothing,
                                     Optional ByRef poException As Exception = Nothing) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_PRCxUpdQtyNow 'ADA42','2012/02/14','11:53:00','Tee'
        '   Description :
        '       @ptComName varchar(50)
        '       @ptBchCode varchar(3)
        '       @ptDateTo varchar(10)
        '       @ptTime varchar(10)
        '       @ptWho varchar(100)AS
        '   Developer : SOMJAI
        '   Modify for Web : SuperChuck 29-01-2013
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------

        Dim oCmd As New SqlCommand
        Dim oConn As New SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            If poConnect Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnect
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnect
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            '*******************************'
            Call DB_PRCbPdtCostAmt(poPrcSTP, poConnect)     '*Tee 55-10-30 à¾×èÍãËé»ÃÐÁÇÅ¼ÅàÃçÇ¢Öé¹
            If Not poPrcSTP.STP_AlwUpdQtyNow Then Return False '*CH 29-01-2013 อนุญาตปรับสต้อกยอดคงเหลือขายได้
            If Not (poPrcSTP.STP_AlwInWha Or poPrcSTP.STP_AlwStkCrd) Then Return False
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_PRCxUpdQtyNow"

            oParam = New SqlParameter("ptBchCode", poPrcSTP.FTBchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", poPrcSTP.FTWhoUpd)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
            If poConnect Is Nothing Then
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        End Try
    End Function

    Public Function DB_PRCbPdtCostAmt(ByVal poPrcSTP As cmlProcessSTP, Optional ByRef poConnect As SqlConnection = Nothing) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_PRCxPdtCostAmt 'ADA42','2012/02/14','11:53:00','Tee'
        '   Description :
        '       @ptComName varchar(50)
        '       @ptDateTo varchar(10)
        '       @ptTime varchar(10)
        '       @ptWho varchar(100)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------

        Dim oCmd As New SqlCommand
        Dim oConn As New SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnect Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnect
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnect Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnect
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            '*******************************'
            If Not (poPrcSTP.STP_AlwStkCrd) Then Return False
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_PRCxPdtCostAmt"

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", poPrcSTP.FTWhoUpd)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
            If poConnect Is Nothing Then
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        End Try

    End Function
#End Region

#Region "Enum"
    Public Enum eDB_DocType
        nSTP_DOCxPdtFreePmt = 0
        nSTP_DOCxTCNTPdtAdjStk = 1

        nSTP_DOCxTCNTPdtTnfDT1 = 2
        nSTP_DOCxTCNTPdtTnfDT2 = 3
        nSTP_DOCxTCNTPdtTnfDT3 = 4
        nSTP_DOCxTCNTPdtTnfDT4 = 5

        nSTP_DOCxTACTPiDT1 = 6
        nSTP_DOCxTACTPtDT5CN = 7
        nSTP_DOCxTACTPtDT7DN = 8

        nSTP_DOCxTACTSiDT1 = 9
        nSTP_DOCxTACTStDT5 = 10
        nSTP_DOCxTACTStDT7 = 11

        nSTP_DOCxTACTCsmDT1 = 12
        nSTP_DOCxTACTCsmDT5 = 13
        nSTP_DOCxTACTCsmDT7 = 14

        nSTP_DOCxTACTVatDT1 = 15
        nSTP_DOCxTACTVatDT5 = 16
        nSTP_DOCxTACTVatDT7 = 17
        nSTP_DOCxTACTVatDT9 = 18
        nSTP_DOCxTPSTSalDT1 = 19
        nSTP_DOCxTPSTSalDT9 = 20
        'nSTP_DOCxTPSTSalDT = 21
        nSTP_DOCxTPSTSalDT4StkCrd = 21
        'nSTP_DOCxTPSTSalDT1PrcOpt3 = 13
        'nSTP_DOCxTPSTSalDT9PrcOpt3 = 14
        nSTP_DOCxTCNTCstGiftDT = 22

        '*CH 25-12-2013
        nSTP_DOCxTPSTSalDT1Day = 23
        nSTP_DOCxTPSTSalDT9Day = 24

        '*CH 27-13-2013
        nSTP_DOCxTACTVatDT1Day = 25
        nSTP_DOCxTACTVatDT9Day = 26
    End Enum
#End Region

    Dim bSTP_AlwCost As Boolean
    Dim bSTP_AlwInWha As Boolean
    Dim bSTP_AlwStkCrd As Boolean
    Dim bSTP_AlwReMonthEnd As Boolean       '*Tee 55-06-15 Fix Stk Process
    Dim bSTP_ReMonthEnd As Boolean          'อนุญาต RemonthEnd '*CH 22-05-2013
    Dim bSTP_CNAlwUpdQtyNow As Boolean      '*CH 20-05-2013 Alow Update Oty Now

    Dim tSTP_BchCode As String
    Dim tSTP_BchHQCode As String
    Dim tSTP_WahCode As String
    Dim tSTP_UserName As String
    Dim tSTP_DocNo As String
    Dim tSTP_StkCode As String
    Dim tSTP_ComName As String '*Wi 2014-01-03

    Dim nSTP_DecAmtForSav As Integer
    Dim nSTP_CNPdtSet As Integer

    Dim cSTP_VatRate As Double
    Dim cSTP_StkQty As Double
    Dim cSTP_CostIn As Double
    Dim cSTP_CostEx As Double
    Dim cSTP_Vatable As Double

    Dim bSTP_AlwChkSal As Boolean '*Wi 2013-12-11 ตรวจนับสต็อกระหว่างขาย
    Dim nSTP_PdtPriLevRet As Integer '*Wi 2013-12-11 ระดับราคาสินค้า

    Dim dSTP_MeDate As Date 'Date Start MonthEnd '*CH 20-05-2013

    '*CH 25-12-2013
    Dim tSTP_DateFrm As String
    Dim tSTP_DateTo As String
    Dim bSTP_StaPos As Boolean '*CH 27-12-2013

    Public Property STP_ComName As String
        ''Rmk : *Wi 2014-01-03 ชื่อเครื่อง
        Get
            Return tSTP_ComName
        End Get
        Set(ByVal ptComName As String)
            tSTP_ComName = ptComName
        End Set
    End Property

    Public Property STP_PdtPriLevRet As Integer
        ''Rmk : *Wi 2013-12-11 ระดับราคาสินค้า
        Get
            Return nSTP_PdtPriLevRet
        End Get
        Set(ByVal pnPdtPriLevRet As Integer)
            nSTP_PdtPriLevRet = pnPdtPriLevRet
        End Set
    End Property

    Public Property STP_AlwChkSal As Boolean
        ''Rmk : *Wi 2013-12-11 ตรวจนับสต็อกระหว่างขาย
        Get
            Return bSTP_AlwChkSal
        End Get
        Set(ByVal pbAlwChkSal As Boolean)
            bSTP_AlwChkSal = pbAlwChkSal
        End Set
    End Property

    Public Property STP_AlwCost() As Boolean
        Get
            Return bSTP_AlwCost
        End Get
        Set(ByVal pbAlwCost As Boolean)
            bSTP_AlwCost = pbAlwCost
        End Set
    End Property

    Public Property STP_AlwInWha() As Boolean
        Get
            Return bSTP_AlwInWha
        End Get
        Set(ByVal pbAlwInWha As Boolean)
            bSTP_AlwInWha = pbAlwInWha
        End Set
    End Property

    Public Property STP_AlwStkCrd() As Boolean
        Get
            Return bSTP_AlwStkCrd
        End Get
        Set(ByVal pbAlwStkCrd As Boolean)
            bSTP_AlwStkCrd = pbAlwStkCrd
        End Set
    End Property

    Public Property STP_BchCode() As String
        Get
            Return tSTP_BchCode
        End Get
        Set(ByVal ptBchCode As String)
            tSTP_BchCode = ptBchCode
        End Set
    End Property

    Public Property STP_BchHQCode() As String
        Get
            Return tSTP_BchHQCode
        End Get
        Set(ByVal ptBchHQCode As String)
            tSTP_BchHQCode = ptBchHQCode
        End Set
    End Property

    Public Property STP_DecAmtForSav() As Integer
        Get
            Return nSTP_DecAmtForSav
        End Get
        Set(ByVal pnDecAmtForSav As Integer)
            nSTP_DecAmtForSav = pnDecAmtForSav
        End Set
    End Property

    Public Property STP_UserName() As String
        Get
            Return tSTP_UserName
        End Get
        Set(ByVal ptUserName As String)
            tSTP_UserName = ptUserName
        End Set
    End Property

    Public Property STP_DocNo() As String
        Get
            Return tSTP_DocNo
        End Get
        Set(ByVal ptDocNo As String)
            tSTP_DocNo = ptDocNo
        End Set
    End Property

    Public Property STP_AdjStkpcVatRate() As Double
        Get
            Return cSTP_VatRate
        End Get
        Set(ByVal pcVatRate As Double)
            cSTP_VatRate = pcVatRate
        End Set
    End Property

    Public Property STP_CNPdtSet() As Integer
        Get
            Return nSTP_CNPdtSet
        End Get
        Set(ByVal pnCNPdtSet As Integer)
            nSTP_CNPdtSet = pnCNPdtSet
        End Set
    End Property

    Public Property STP_AlwReMonthEnd() As Boolean
        Get

        End Get
        Set(ByVal pbAlwReME As Boolean)
            bSTP_AlwReMonthEnd = pbAlwReME
        End Set
    End Property

    Public Property STP_ReMonthEnd() As Boolean
        Get

        End Get
        Set(ByVal pbAlwReME As Boolean)
            bSTP_ReMonthEnd = pbAlwReME
        End Set
    End Property

    Public Property STP_StkCode() As String
        Get

        End Get
        Set(ByVal ptStkCoce As String)
            tSTP_StkCode = ptStkCoce
        End Set
    End Property

    Public Property STP_StkQty() As Double
        Get
        End Get
        Set(ByVal pcStkQty As Double)
            cSTP_StkQty = pcStkQty
        End Set
    End Property

    Public Property STP_CostIn() As Double
        Get
        End Get
        Set(ByVal pcCostIn As Double)
            cSTP_CostIn = pcCostIn
        End Set
    End Property

    Public Property STP_CostEx() As Double
        Get
        End Get
        Set(ByVal pcCostEx As Double)
            cSTP_CostEx = pcCostEx
        End Set
    End Property

    Public Property STP_Vatable() As Double
        Get
        End Get
        Set(ByVal pcVatable As Double)
            cSTP_Vatable = pcVatable
        End Set
    End Property

    Public Property STP_WahCode() As String
        Get
        End Get
        Set(ByVal ptWahCode As String)
            tSTP_WahCode = ptWahCode
        End Set
    End Property

    'วันที่ทำ ยอดยกมา '*CH 20-05-2013
    Public Property STP_MonthEndDate() As Date
        Get
            Return dSTP_MeDate
        End Get
        Set(ByVal pdMeDate As Date)
            dSTP_MeDate = pdMeDate
        End Set
    End Property

    '*CH 20-05-2013 Alow Update Oty Now
    Public Property STP_AlwUpdQtyNow() As Boolean
        Get
        End Get
        Set(ByVal pbAlwUpdQtyNow As Boolean)
            bSTP_CNAlwUpdQtyNow = pbAlwUpdQtyNow
        End Set
    End Property

    '*CH 25-12-2013
    Public Property STP_DateFrm() As String
        Get
            Return tSTP_DateFrm
        End Get
        Set(ByVal ptDate As String)
            tSTP_DateFrm = ptDate
        End Set
    End Property
    Public Property STP_DateTo() As String
        Get
            Return tSTP_DateTo
        End Get
        Set(ByVal ptDate As String)
            tSTP_DateTo = ptDate
        End Set
    End Property

    '*CH 27-12-2013
    Public Property STP_StaPos() As Boolean
        Get
            Return bSTP_StaPos
        End Get
        Set(ByVal pbPos As Boolean)
            bSTP_StaPos = pbPos
        End Set
    End Property

    Public Sub Initial()
        tSTP_DocNo = ""
        cSTP_VatRate = 7
        bSTP_AlwCost = False
        bSTP_AlwInWha = False
        bSTP_AlwStkCrd = False
        'bSTP_StaPOS = False
        bSTP_AlwReMonthEnd = False '*Tee 55-06-15 Stk Process
    End Sub

    Public Function STP_MSGShowError(ByVal poConnection As SqlConnection) As String
        '-----------------------------------------------------------
        '   Call :
        '   Description :
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  11:57:10
        '-----------------------------------------------------------
        Try
            Dim oConn As New SqlClient.SqlConnection
            Dim odtTmp As New DataTable
            Dim odaTmp As New SqlDataAdapter
            Dim tSql As String
            Dim tMsg$
            Dim nRow As Long

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            tSql = "SELECT TOP 1 * FROM TSysPrcLog "
            tSql &= "WHERE FTComName='" & Environment.MachineName & "' "
            tSql &= "AND FDErrDate='" & Format(Now, "yyyy/MM/dd") & "' "
            tSql &= "ORDER BY FDErrDate DESC ,FTErrTime DESC"
            odaTmp = New SqlDataAdapter(tSql.ToString, oConn)
            odaTmp.Fill(odtTmp)

            tMsg = ""
            If odtTmp.Rows.Count > 0 Then
                For nRow = 0 To odtTmp.Rows.Count - 1
                    tMsg = tMsg & "ErrNo:" & odtTmp(nRow)("FNErrNo").ToString & "ErrProc:" & odtTmp(nRow)("FTErrFunction").ToString & "ErrDesc:" & odtTmp(nRow)("FTErrMsg").ToString & vbCr
                Next
            End If

            Return tMsg
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function STP_DOCbGenData(ByRef poConnection As SqlConnection, ByVal peDocType As eDB_DocType) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '   Description :
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  16:37:46
        '-----------------------------------------------------------
        Try
            STP_DOCbGenData = False

            Select Case peDocType
                Case eDB_DocType.nSTP_DOCxTCNTPdtAdjStk
                    If Not DB_DOCbTCNTPdtAdjStk(poConnection) Then '*Wi 2014-01-03
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT1
                    If Not STP_DOCbTCNTPdtTnfDT1(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT2
                    If Not STP_DOCbTCNTPdtTnfDT2(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT3
                    If Not STP_DOCbTCNTPdtTnfDT3(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTCNTPdtTnfDT4
                    If Not STP_DOCbTCNTPdtTnfDT4(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTPiDT1
                    If Not STP_DOCbTACTPiDT1(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTPtDT5CN
                    If Not STP_DOCbTACTPtDT5CN(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTPtDT7DN
                    If Not STP_DOCbTACTPtDT7DN(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTVatDT1
                    If Not STP_DOCbTACTVatDT1(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTVatDT5
                    '    If Not DB_DOCbTACTVatDT5() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTACTVatDT7
                    '    If Not DB_DOCbTACTVatDT7() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTACTVatDT9
                    If Not STP_DOCbTACTVatDT9(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTCsmDT1
                    '    If Not DB_DOCbTACTCsmDT1() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTACTCsmDT5
                    '    If Not DB_DOCbTACTCsmDT5() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTACTCsmDT7
                    '    If Not DB_DOCbTACTCsmDT7() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTACTSiDT1
                    '    If Not DB_DOCbTACTSiDT1() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTACTStDT5
                    '    If Not DB_DOCbTACTStDT5() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTACTStDT7
                    '    If Not DB_DOCbTACTStDT7() Then
                    '        Exit Function
                    '    End If

                Case eDB_DocType.nSTP_DOCxTPSTSalDT1
                    If Not STP_DOCbTPSTSalDT1(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTPSTSalDT9
                    '    If Not DB_DOCbTPSTSalDT9() Then
                    '        Exit Function
                    '    End If
                Case eDB_DocType.nSTP_DOCxTCNTCstGiftDT
                    If Not STP_DOCxTCNTCstGiftDT(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxPdtFreePmt
                    If Not STP_DOCxPdtFreePmt(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTPSTSalDT1Day     '*Tee 56-01-05 ประประมวลผลStock การขายให้ทำครั้งเดียว
                    If Not STP_DOCbTPSTSalDT1Day(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTPSTSalDT9Day     '*Tee 56-01-05 ประประมวลผลStock การขายให้ทำครั้งเดียว
                    If Not STP_DOCbTPSTSalDT9Day(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTVatDT1Day     '*Tee 56-01-05 ประประมวลผลStock การขายให้ทำครั้งเดียว
                    If Not STP_DOCbTACTVatDT1Day(poConnection) Then
                        Exit Function
                    End If
                Case eDB_DocType.nSTP_DOCxTACTVatDT9Day     '*Tee 56-01-05 ประประมวลผลStock การขายให้ทำครั้งเดียว
                    If Not STP_DOCbTACTVatDT9Day(poConnection) Then
                        Exit Function
                    End If
            End Select
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function STP_DOCxTCNTCstGiftDT(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTPSTSalDT9 '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If Not (bSTP_AlwInWha Or bSTP_AlwStkCrd Or bSTP_AlwCost) Then
                Return True
            End If

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTCNTCstGiftDT"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", "")
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", STP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try

    End Function

    ''' <summary>
    ''' Insert Data Product Free Pomotion to TCNTTmpPrcStkCode
    ''' </summary>
    ''' <param name="poConnection"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function STP_DOCxPdtFreePmt(ByVal poConnection As SqlConnection) As Boolean
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Dim tSql As String = ""
        Try
            If Not (bSTP_AlwInWha Or bSTP_AlwStkCrd Or bSTP_AlwCost) Then
                Return True
            End If

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            tSql = "INSERT INTO TCNTmpPrcStkCard(FTComName,FTBchCode,FTStkDocNo"
            tSql = tSql & ",FTStkType,FTPdtStkCode,FCStkQty"
            tSql = tSql & ",FTWahCode,FDStkDate,FCStkSetPrice"
            tSql = tSql & ",FCStkCostIn,FCStkCostEx,FCStkVatable"
            tSql = tSql & ",FDDateUpd,FTTimeUpd,FTWhoUpd"
            tSql = tSql & ",FDDateIns,FTTimeIns,FTWhoIns) "
            tSql = tSql & "VALUES ("
            tSql = tSql & "'" & Environment.MachineName & "','" & tSTP_BchCode & "','" & tSTP_DocNo & "'"
            tSql = tSql & ",'3','" & tSTP_StkCode & "'," & cSTP_StkQty
            tSql = tSql & ",'" & tSTP_WahCode & "','" & Format(Now, "yyyy/MM/dd") & "'," & 0
            tSql = tSql & "," & cSTP_CostIn & "," & cSTP_CostEx & "," & cSTP_Vatable
            tSql = tSql & ",'" & Format(Now, "yyyy/MM/dd") & "','" & Format(Now, "HH:mm:ss") & "','" & tSTP_UserName & "'"
            tSql = tSql & ",'" & Format(Now, "yyyy/MM/dd") & "','" & Format(Now, "HH:mm:ss") & "','" & tSTP_UserName & "')"

            oCmd = New SqlCommand(tSql, oConn)
            oCmd.ExecuteNonQuery()
            Return True

        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try

    End Function

    Private Function STP_DOCbTPSTSalDT1Day(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTPSTSalDT1Day '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTPSTSalDT1Day"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", cSTP_VatRate)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateFrm", tSTP_DateFrm)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", tSTP_DateTo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 8
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try
    End Function

    Private Function STP_DOCbTPSTSalDT9Day(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTPSTSalDT9Day '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTPSTSalDT9Day"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", cSTP_VatRate)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateFrm", tSTP_DateFrm)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", tSTP_DateTo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 8
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try
    End Function

    Private Function STP_DOCbTACTVatDT1Day(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC DB_DOCbTACTVatDT1Day '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTACTVatDT1Day"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPOS", IIf(bSTP_StaPos, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", cSTP_VatRate)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateFrm", tSTP_DateFrm)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", tSTP_DateTo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 8
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)


            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try
    End Function

    Private Function STP_DOCbTACTVatDT9Day(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC DB_DOCbTACTVatDT9Day '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTACTVatDT9Day"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPOS", IIf(bSTP_StaPos, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", cSTP_VatRate)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateFrm", tSTP_DateFrm)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", tSTP_DateTo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 8
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try
    End Function

    Private Function STP_DOCbTACTVatDT1(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTACTVatDT1 '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTACTVatDT1"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPOS", IIf(bSTP_StaPos, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", cSTP_VatRate)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", tSTP_DateTo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 8
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try
    End Function

    Private Function STP_DOCbTACTVatDT9(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTACTVatDT9 '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTACTVatDT9"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPOS", IIf(bSTP_StaPos, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", cSTP_VatRate)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", tSTP_DateTo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 8
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try
    End Function

    Private Function STP_DOCbTPSTSalDT1(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTPSTSalDT1 '000','ADA42',2,'SS00012-000002','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTPSTSalDT1"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", cSTP_VatRate)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", tSTP_DateTo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 10
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 8
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try
    End Function

    Private Function STP_PRCxSTK_ReMonthEndByDoc(ByVal poConnection As SqlConnection, ByVal pdDate As Date) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '       DELETE FROM TCNTmpPrcStkCard WHERE FTComNAME ='ADA42'
        '       EXEC STP_DOCxTACTVatDT1 '000','ADA42',2,'2','SS00012-000003','2012/02/21','11:53:00','Tee'
        '       EXEC STP_DOCxPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '       EXEC STP_PRCxUpdQtyNow 'ADA42','000','2012/02/21','11:53:00','Tee'
        '   Description 
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            'Dim tSql As StringBuilder
            Dim nRet As Long
            Dim oParam As SqlParameter

            If Not (bSTP_AlwInWha Or bSTP_AlwStkCrd Or bSTP_AlwCost) Then
                Return True
            End If

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_PRCxSTK_ReMonthEndByDoc"
            oCmd.CommandTimeout = 0

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptMeDate", Format(CDate(pdDate), "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
        End Try

    End Function

    Public Overloads Function STP_PRCb1DocApprove(ByRef poConnection As SqlConnection, ByRef poTrans As SqlTransaction _
                                        , ByVal peDocType As eDB_DocType) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '       DELETE FROM TCNTmpPrcStkCard WHERE FTComNAME ='ADA42'
        '       EXEC STP_DOCxTACTVatDT1 '000','ADA42',2,'2','SS00012-000003','2012/02/21','11:53:00','Tee'
        '       EXEC STP_DOCxPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '       EXEC STP_PRCxUpdQtyNow 'ADA42','000','2012/02/21','11:53:00','Tee'
        '   Description :
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        ''Lvl : 1
        ''Rmk : *Wi 2013-12-14 เพิ่ม poTrans เพื่อรับ Connection ที่เริ่ม Trans ไปแล้ว

        Dim oCmd As SqlCommand = Nothing
        Dim oConn As SqlConnection = Nothing
        Try
            Dim tSql As New StringBuilder

            If Not (bSTP_AlwInWha Or bSTP_AlwStkCrd Or bSTP_AlwCost) Then
                Return True
            End If

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            tSql.Length = 0
            tSql.Append("DELETE FROM TCNTmpPrcStkCard WHERE FTComName ='" & Environment.MachineName & "'")
            oCmd = oConn.CreateCommand()
            oCmd.Transaction = poTrans
            oCmd.CommandText = tSql.ToString
            oCmd.ExecuteNonQuery()

            tSql.Length = 0
            tSql.Append("DELETE FROM TCNTmpPrcSrnCard WHERE FTComName ='" & Environment.MachineName & "'")
            oCmd = oConn.CreateCommand()
            oCmd.Transaction = poTrans
            oCmd.CommandText = tSql.ToString
            oCmd.ExecuteNonQuery()

            If Not STP_DOCbGenData(oConn, peDocType) Then Return False
            If STP_DOCbPrcApprove(oConn) Then
                If bSTP_AlwReMonthEnd Then bSTP_ReMonthEnd = True : Call STP_PRCxSTK_ReMonthEndByDoc(oConn, CDate(Now)) '*Tee 55-06-15 Stk Process
                Return True
            Else
                'Call DB_MSGShowError()
                Return False
            End If

        Catch oE As Exception
            'Return False
            If poTrans IsNot Nothing Then
                poTrans.Rollback()
            End If
            Throw New Exception("cdaProcessSTP.STP_PRCb1DocApprove(), cannot approve document completely . " & oE.Message & vbNewLine)
        End Try

    End Function

    Public Overloads Function STP_PRCb1DocApprove(ByRef poDbCon As SqlConnection, ByVal peDocType As eDB_DocType) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '       DELETE FROM TCNTmpPrcStkCard WHERE FTComNAME ='ADA42'
        '       EXEC STP_DOCxTACTVatDT1 '000','ADA42',2,'2','SS00012-000003','2012/02/21','11:53:00','Tee'
        '       EXEC STP_DOCxPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '       EXEC STP_PRCxUpdQtyNow 'ADA42','000','2012/02/21','11:53:00','Tee'
        '   Description :
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '   Copy&Mod : *Wi 2014-03-01
        '----------------------------------------------------------- 
        Dim oCmd As SqlCommand
        Try
            Dim tSql As New StringBuilder
            If Not (bSTP_AlwInWha Or bSTP_AlwStkCrd Or bSTP_AlwCost) Then
                Return True
            End If

            tSql.Length = 0
            tSql.Append("DELETE FROM TCNTmpPrcStkCard WHERE FTComName ='" & Environment.MachineName & "'")
            oCmd = New SqlCommand(tSql.ToString, poDbCon)
            oCmd.ExecuteNonQuery()

            tSql.Length = 0
            tSql.Append("DELETE FROM TCNTmpPrcSrnCard WHERE FTComName ='" & Environment.MachineName & "'")
            oCmd = New SqlCommand(tSql.ToString, poDbCon)
            oCmd.ExecuteNonQuery()

            If Not STP_DOCbGenData(poDbCon, peDocType) Then Return False
            If STP_DOCbPrcApprove(poDbCon) Then
                If bSTP_AlwReMonthEnd Then bSTP_ReMonthEnd = True : Call STP_PRCxSTK_ReMonthEndByDoc(poDbCon, CDate(Now)) '*Tee 55-06-15 Stk Process
                Return True
            Else
                Return False
            End If
        Catch oE As Exception
            Throw New Exception("cdaProcessSTP.STP_PRCb1DocApprove(), cannot approve document completely . " & oE.Message & vbNewLine)
        Finally
            oCmd = Nothing
        End Try
    End Function


    Private Function STP_DOCbPrcApprove(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC DB_DOCbPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '   Description :
        '   @ptComName varchar(50)
        '   @ptAlwCost  varchar(1)  --0 not calculate
        '   @ptAlwInWha  varchar(1)  --0 not calculate
        '   @ptAlwStkCrd  varchar(1)  --0 not calculate
        '   @pcDecPoint float
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(100)
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxPrcApprove"

            '1
            oParam = New SqlParameter("ptStkDocNo", STP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            '2
            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            '3
            oParam = New SqlParameter("ptAlwCost", IIf(bSTP_AlwCost, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwInWha", IIf(bSTP_AlwInWha, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwStkCrd", IIf(bSTP_AlwStkCrd, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                If tSTP_BchCode <> tSTP_BchHQCode Then Call C_SYNxAddLogChange(oConn) '*Em 56-05-15  RQ1305-017  
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try

    End Function
    Private Sub C_SYNxAddLogChange(ByVal poConnection As SqlConnection)
        '-----------------------------------------------------------
        '*Em 56-05-15  RQ1305-017
        '   Call :EXEC STP_SYNxAddLogChange 'ADA42','1','1'
        '   Description :
        '         @ptComName varchar(50)
        '        ,@ptAlwInWha  varchar(1)
        '        ,@ptAlwStkCrd  varchar(1)
        '   Developer : Em
        '   Date Create : 15/05/2013 
        '-----------------------------------------------------------
        Try
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_SYNxAddLogChange"

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 50
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwInWha", IIf(bSTP_AlwInWha, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 1
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwStkCrd", IIf(bSTP_AlwStkCrd, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 1
            oCmd.Parameters.Add(oParam)

            oCmd.ExecuteNonQuery()

            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd = Nothing
        Catch ex As Exception

        End Try
    End Sub

    Public Function STP_PRCb1MAApprove(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :
        '       DELETE FROM TCNTmpPrcStkCard WHERE FTComNAME ='ADA42'
        '       EXEC STP_DOCxTACTVatDT1 '000','ADA42',2,'2','SS00012-000003','2012/02/21','11:53:00','Tee'
        '       EXEC STP_DOCxPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '       EXEC STP_PRCxUpdQtyNow 'ADA42','000','2012/02/21','11:53:00','Tee'
        '   Description :
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim tSql As New StringBuilder
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            tSql.Length = 0
            tSql.Append("DELETE FROM TCNTmpPrcStkCard WHERE FTComName ='" & Environment.MachineName & "'")
            oCmd = New SqlCommand(tSql.ToString, oConn)
            oCmd.ExecuteNonQuery()
            If Not STP_PRCbSTK_MonthEnd(oConn) Then Return False
            If STP_MAbPrcApprove(oConn) Then
                Return True
            Else
                'Call DB_MSGShowError()
                Return False
            End If

            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd = Nothing
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function STP_PRCbSTK_MonthEnd(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTCNTPdtTnfDT1 '000','ADA42',7,2,'IU00012-000005','2012/02/09','13:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_PRCxSTK_MonthEnd"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptMeDate", Format(CDate(dSTP_MeDate), "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDate", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Public Function STP_MAbPrcApprove(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC DB_MAbPrcApprove 'ADA42','0','1',2,'2012/02/21','18:53:00','Tee'
        '   Description :
        '   @ptComName varchar(50)
        '   @ptAlwCost  varchar(1)  --0 not calculate
        '   @ptAlwInWha  varchar(1)  --0 not calculate
        '   @ptAlwStkCrd  varchar(1)  --0 not calculate
        '   @pcDecPoint float
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(100)
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_MAxPrcApprove"

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwInWha", IIf(bSTP_AlwInWha, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwStkCrd", IIf(bSTP_AlwStkCrd, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptAlwCost", IIf(bSTP_AlwCost, "1", "0"))
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Public Function STP_PRCb2UpdQtyNow(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_PRCxUpdQtyNow 'ADA42','2012/02/14','11:53:00','Tee'
        '   Description :
        '       @ptComName varchar(50)
        '       @ptBchCode varchar(3)
        '       @ptDateTo varchar(10)
        '       @ptTime varchar(10)
        '       @ptWho varchar(100)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            Call STP_PRCbPdtCostAmt(oConn)     '*Tee 55-10-30 à¾×èÍãËé»ÃÐÁÇÅ¼ÅàÃçÇ¢Öé¹
            If Not bSTP_CNAlwUpdQtyNow Then Return False
            'If Not (bSTP_AlwInWha Or bSTP_AlwStkCrd) Then Return False 'Hide  '*Em 57-06-23  Food Project
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_PRCxUpdQtyNow"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            'Old *CH 13-11-2014
            'oParam = New SqlParameter("ptComName", Environment.MachineName)
            'oParam.Direction = ParameterDirection.Input
            'oParam.Size = 50
            'oParam.DbType = DbType.String
            'oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Public Function STP_PRCbPdtCostAmt(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_PRCxPdtCostAmt 'ADA42','2012/02/14','11:53:00','Tee'
        '   Description :
        '       @ptComName varchar(50)
        '       @ptDateTo varchar(10)
        '       @ptTime varchar(10)
        '       @ptWho varchar(100)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            If Not (bSTP_AlwStkCrd) Then Return False
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_PRCxPdtCostAmt"

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
            oCmd.Connection = Nothing
            oCmd = Nothing
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Private Function STP_DOCbTCNTPdtTnfDT1(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTCNTPdtTnfDT1 '000','ADA42',7,2,'IU00012-000005','2012/02/09','13:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTCNTPdtTnfDT1"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            oCmd.Connection = Nothing
            oCmd = Nothing
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Private Function STP_DOCbTCNTPdtTnfDT2(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTCNTPdtTnfDT2 '000','ADA42',7,2,'IU00012-000005','2012/02/09','13:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTCNTPdtTnfDT2"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            oCmd.Connection = Nothing
            oCmd = Nothing
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Private Function STP_DOCbTCNTPdtTnfDT3(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTCNTPdtTnfDT3 '000','ADA42',7,2,'IU00012-000005','2012/02/09','13:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTCNTPdtTnfDT3"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            oCmd.Connection = Nothing
            oCmd = Nothing
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Private Function STP_DOCbTCNTPdtTnfDT4(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTCNTPdtTnfDT4 '000','ADA42',7,2,'IU00012-000005','2012/02/09','13:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTCNTPdtTnfDT4"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            oCmd.Connection = Nothing
            oCmd = Nothing
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Private Function STP_DOCbTACTPiDT1(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTACTPiDT1 '000','ADA42',2,'PR00012-000001','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTACTPiDT1"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            oCmd.Connection = Nothing
            oCmd = Nothing
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Private Function STP_DOCbTACTPtDT5CN(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTACTPtDT5CN '000','ADA42',2,'PC00012-000001','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------

        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection
            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTACTPtDT5CN"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Private Function STP_DOCbTACTPtDT7DN(ByRef poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTACTPtDT7DN '000','ADA42',2,'PC00012-000001','2012/02/14','11:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter
            Dim oCmd As SqlCommand
            Dim oConn As New SqlClient.SqlConnection
            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTACTPtDT7DN"

            oParam = New SqlParameter("ptBchCode", tSTP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", Environment.MachineName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", nSTP_CNPdtSet)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 1
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int32
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDocNo", tSTP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 30
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 100
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
        End Try
    End Function

    Public Sub DB_PRCxAdjUpdCostByOpt(ByRef poConnection As SqlConnection)
        ''Lvl : 1
        ''Cmt : Copy จากโค้ด VB6
        ''Rmk : *Wi 2013-12-14
        '-----------------------------------------------------------
        '   Call :
        '   Description :*Tee 56-06-13 RQ1306-002
        '   Developer : SOMJAI
        '   Date Create : 17/06/2013 :  11:03:48
        '-----------------------------------------------------------
        Dim tOpt$, tFedCost$, tSql$
        Dim oDbCmd As SqlCommand
        Dim oConn As SqlConnection
        Dim oDbRdr As SqlDataReader
        Try
            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If
            '**** Error Max pool size *****' '*CH 31-07-2012
            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try

            tOpt = ""
            tSql = "SELECT FTSysUsrValue FROM TSysConfig WHERE FTSysCode='ACostAdj'"
            oDbCmd = oConn.CreateCommand()
            oDbCmd.CommandText = tSql
            oDbRdr = oDbCmd.ExecuteReader()
            If oDbRdr.HasRows Then
                While oDbRdr.Read()
                    tOpt = CStr(oDbRdr.Item("FTSysUsrValue"))
                End While
            End If
            oDbRdr.Close()

            '--1 ต้นทุนเฉลี่ย ,2 ต้นทุนสุดท้าย ,3 ต้นทุนมาตรฐาน คั่นด้วย [,]
            Select Case tOpt
                Case "1,2,3"
                    tFedCost = "CASE WHEN ISNULL(FCPdtCostAvg,0) > 0 THEN FCPdtCostAvg ELSE CASE WHEN ISNULL(FCPdtCostLast,0) > 0 THEN FCPdtCostLast ELSE FCPdtCostStd END END"
                Case "1,3,2"
                    tFedCost = "CASE WHEN ISNULL(FCPdtCostAvg,0) > 0 THEN FCPdtCostAvg ELSE CASE WHEN ISNULL(FCPdtCostStd,0) > 0 THEN FCPdtCostStd ELSE FCPdtCostLast END END"
                Case "2,1,3"
                    tFedCost = "CASE WHEN ISNULL(FCPdtCostLast,0) > 0 THEN FCPdtCostLast ELSE CASE WHEN ISNULL(FCPdtCostAvg,0) > 0 THEN FCPdtCostAvg ELSE FCPdtCostStd END END"
                Case "2,3,1"
                    tFedCost = "CASE WHEN ISNULL(FCPdtCostLast,0) > 0 THEN FCPdtCostLast ELSE CASE WHEN ISNULL(FCPdtCostStd,0) > 0 THEN FCPdtCostStd ELSE FCPdtCostAvg END END"
                Case "3,1,2"
                    tFedCost = "CASE WHEN ISNULL(FCPdtCostStd,0) > 0 THEN FCPdtCostStd ELSE CASE WHEN ISNULL(FCPdtCostAvg,0) > 0 THEN FCPdtCostAvg ELSE FCPdtCostLast END END"
                Case "3,2,1"
                    tFedCost = "CASE WHEN ISNULL(FCPdtCostStd,0) > 0 THEN FCPdtCostStd ELSE CASE WHEN ISNULL(FCPdtCostLast,0) > 0 THEN FCPdtCostLast ELSE FCPdtCostAvg END END"
                Case Else
                    tFedCost = "FCPdtCostAvg"
            End Select

            If bSTP_AlwChkSal Then
                '    --7.ตรวจนับสต้อก Option ระหว่างขาย(2)  สินค้าชุด-ลูก
                tSql = "UPDATE TCNTPdtStkCard"
                tSql = tSql & " SET FCStkCostIn= ROUND(PDT.FCIudCostEx * (100 + " & cSTP_VatRate & ") / 100," & nSTP_DecAmtForSav & ")"
                tSql = tSql & " ,FCStkCostEx= PDT.FCIudCostEx"
                tSql = tSql & " ,FCStkSetPrice= PDT.FCStkSetPrice"
                tSql = tSql & " FROM TCNTPdtChkSalDT DT INNER JOIN TCNTPdtSet PDS ON DT.FTPdtCode =PDS.FTPdtCode"
                tSql = tSql & " INNER JOIN(SELECT FTPdtCode,FTPdtStkCode," & tFedCost & " AS FCIudCostEx,FCPdtRetPriS" & nSTP_PdtPriLevRet & " AS FCStkSetPrice  FROM TCNMPDT ) PDT ON PDT.FTPdtCode = PDS.FTPstCode"
                tSql = tSql & " INNER JOIN TCNTPdtStkCard STK ON STK.FTStkDocNo =DT.FTIuhDocNo AND STK.FTBchCode =DT.FTBchCode AND STK.FTPdtStkCode=PDT.FTPdtStkCode"
                tSql = tSql & " WHERE  DT.FTIuhDocType = '2' AND DT.FTBchCode='" & tSTP_BchCode & "' AND DT.FTIuhDocNo ='" & tSTP_DocNo & "'"
                oDbCmd = oConn.CreateCommand()
                oDbCmd.CommandText = tSql
                oDbCmd.ExecuteNonQuery()
            Else
                '--8.ตรวจนับสต้อก Option ปิดร้านนับ(2)  สินค้าชุด-ลูก
                tSql = "UPDATE TCNTPdtStkCard"
                tSql = tSql & " SET FCStkCostIn= ROUND(PDT.FCIudCostEx * (100 + " & cSTP_VatRate & ") / 100," & nSTP_DecAmtForSav & ")"
                tSql = tSql & " ,FCStkCostEx= PDT.FCIudCostEx"
                tSql = tSql & " ,FCStkSetPrice= PDT.FCStkSetPrice"
                tSql = tSql & " FROM TCNTPdtChkDT DT INNER JOIN TCNTPdtSet PDS ON DT.FTPdtCode =PDS.FTPdtCode"
                tSql = tSql & " INNER JOIN(SELECT FTPdtCode,FTPdtStkCode," & tFedCost & " AS FCIudCostEx,FCPdtRetPriS" & nSTP_PdtPriLevRet & " AS FCStkSetPrice  FROM TCNMPDT ) PDT ON PDT.FTPdtCode = PDS.FTPstCode"
                tSql = tSql & " INNER JOIN TCNTPdtStkCard STK ON STK.FTStkDocNo =DT.FTIuhDocNo AND STK.FTBchCode =DT.FTBchCode AND STK.FTPdtStkCode=PDT.FTPdtStkCode"
                tSql = tSql & " WHERE  DT.FTIuhDocType = '2' AND DT.FTBchCode='" & tSTP_BchCode & "' AND DT.FTIuhDocNo ='" & tSTP_DocNo & "'"
                oDbCmd = oConn.CreateCommand()
                oDbCmd.CommandText = tSql
                oDbCmd.ExecuteNonQuery()
            End If
            Exit Sub
        Catch oE As Exception
            ''
            Throw New Exception("cdaProcess.DB_PRCxAdjUpdCostByOpt(), " & oE.Message & vbNewLine)
        Finally
            ''
            oDbCmd = Nothing
            oConn = Nothing
            tSql = Nothing
        End Try
    End Sub

    Private Function DB_DOCbTCNTPdtAdjStk(ByVal poConnection As SqlConnection) As Boolean
        '-----------------------------------------------------------
        '   Call :EXEC STP_DOCxTCNTPdtAdjStk '000','ADA42',7,2,'IU00012-000005','2012/02/09','13:53:00','Tee'
        '   Description :
        '   @ptBchCode varchar(30)
        '   @ptComName varchar(50)
        '   @pcVatRate float
        '   @pcDecPoint float
        '   @ptDocNo varchar(30)
        '   @ptDateTo varchar(10)
        '   @ptTime varchar(10)
        '   @ptWho varchar(255)AS
        '   Developer : SOMJAI
        '   Date Create : 22/02/2012 :  9:54:44
        '-----------------------------------------------------------
        Dim oCmd As New SqlCommand
        Dim oConn As New SqlClient.SqlConnection
        Try
            Dim nRet As Long
            Dim oParam As SqlParameter

            If poConnection Is Nothing Then
                oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            Else
                oConn = poConnection
            End If

            Try
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            Catch ex As Exception
                SqlClient.SqlConnection.ClearPool(oConn)
                oConn = New SqlClient.SqlConnection
                If poConnection Is Nothing Then
                    oConn.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
                Else
                    oConn = poConnection
                End If
                If oConn.State = ConnectionState.Closed Then oConn.Open()
            End Try
            oCmd = New SqlCommand
            oCmd.Connection = oConn
            oCmd.CommandTimeout = 0
            oCmd.CommandType = CommandType.StoredProcedure
            oCmd.CommandText = "STP_DOCxTCNTPdtAdjStk"

            'oCmd.Parameters.Append(oCmd.CreateParameter("ptBchCode", adVarChar, adParamInput, 3, tVB_CNBchCode))
            'oCmd.Parameters.Append(oCmd.CreateParameter("ptComName", adVarChar, adParamInput, 50, Environment.MachineName))
            'oCmd.Parameters.Append(oCmd.CreateParameter("ptStaPdtSet", adVarChar, adParamInput, 1, nVB_CNPdtSet))
            'oCmd.Parameters.Append(oCmd.CreateParameter("pcVatRate", adDouble, adParamInput, , cSTP_VatRate))
            'oCmd.Parameters.Append(oCmd.CreateParameter("pcDecPoint", adInteger, adParamInput, , nVB_CNDecAmtForSav))

            oParam = New SqlParameter("ptBchCode", STP_BchCode)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 3
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptComName", STP_ComName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 50
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptStaPdtSet", CStr(STP_CNPdtSet))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcVatRate", CDbl(cSTP_VatRate))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Double
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("pcDecPoint", nSTP_DecAmtForSav)
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.Int16
            oCmd.Parameters.Add(oParam)

            'oCmd.Parameters.Append(oCmd.CreateParameter("ptDocNo", adVarChar, adParamInput, 30, tSTP_DocNo))
            'oCmd.Parameters.Append(oCmd.CreateParameter("ptDateTo", adVarChar, adParamInput, 10, SP_DTEtStrSTD(Now)))
            'oCmd.Parameters.Append(oCmd.CreateParameter("ptTime", adVarChar, adParamInput, 10, Time$))
            'oCmd.Parameters.Append(oCmd.CreateParameter("ptWho", adVarChar, adParamInput, 100, IIf(tVB_CNUserAlwN = "", tVB_CNUserName, tVB_CNUserAlwN)))
            'oCmd.Parameters.Append(oCmd.CreateParameter("FNResult", adInteger, adParamOutput))

            oParam = New SqlParameter("ptDocNo", STP_DocNo)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 30
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            'oParam = New SqlParameter("ptDateTo", SP_DTEtStrSTD(Now))
            'oParam.Direction = ParameterDirection.Input
            'oParam.Size = 1
            'oParam.DbType = DbType.Int16
            'oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptDateTo", Format(Now, "yyyy/MM/dd"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            'oParam = New SqlParameter("ptTime", '')
            'oParam.Direction = ParameterDirection.Input
            'oParam.Size = 1
            'oParam.DbType = DbType.Int16
            'oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptTime", Format(Now, "HH:mm:ss"))
            oParam.Direction = ParameterDirection.Input
            oParam.DbType = DbType.String
            oParam.Size = 10
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("ptWho", tSTP_UserName)
            oParam.Direction = ParameterDirection.Input
            oParam.Size = 100
            oParam.DbType = DbType.String
            oCmd.Parameters.Add(oParam)

            oParam = New SqlParameter("FNResult", SqlDbType.Int)
            oParam.Direction = ParameterDirection.Output
            oCmd.Parameters.Add(oParam)
            oCmd.ExecuteNonQuery()

            nRet = oCmd.Parameters("FNResult").Value
            If (nRet = 0) Then
                DB_DOCbTCNTPdtAdjStk = True
            Else
                DB_DOCbTCNTPdtAdjStk = False
            End If

        Catch ex As Exception
            DB_DOCbTCNTPdtAdjStk = False
            Return False
        Finally
            oCmd.Connection = Nothing
            oCmd = Nothing
            If poConnection Is Nothing Then '**ป้องกัน Error Pool Size**' '*CH 10-07-2012
                If oConn IsNot Nothing AndAlso oConn.State = ConnectionState.Open Then oConn.Close()
                oConn = Nothing
            End If
        End Try

    End Function
End Class
