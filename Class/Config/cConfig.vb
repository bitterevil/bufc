﻿Imports System.IO

Namespace AdaConfig

    Public Class cConfig

        Public Shared Property bStaAuto As Boolean = False
        Public Shared Property tAppPath As String = Application.StartupPath
        Private Shared oXmlCfg As XElement
        Private Shared oXmlDefault As XElement

        Public Shared ReadOnly Property AdaConfigXml As String
            Get
                If File.Exists(tAppPath & "\Config\AdaConfig.xml") = False Then
                    Dim oFrmSetting As New wSetting()
                    oApplication = New oStrucApplication
                    If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    End If
                    'If Directory.Exists(tAppPath & "\Config") = False Then
                    '    Directory.CreateDirectory(tAppPath & "\Config")
                    'End If
                    'Dim oXEAdaConfig As XElement = _
                    '    <config>
                    '        <Application User="EUrhNOJbKWw=" Pass="EUrhNOJbKWw=" Language="2"></Application>
                    '        <Connection Server="(local)\SQL2012" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></Connection>
                    '        <ConnectionMem Server="(local)\SQL2012" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></ConnectionMem>
                    '        <Separator Index="0" Define=""></Separator>
                    '        <Import Path="Import.xml"></Import>
                    '        <Export Path="Export.xml"></Export>
                    '    </config>
                    'Dim oAdaConfigXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEAdaConfig)
                    'oAdaConfigXml.Save(tAppPath & "\Config\AdaConfig.xml")
                    'oAdaConfigXml = Nothing
                    'oXEAdaConfig = Nothing
                End If

                Return tAppPath & "\Config\AdaConfig.xml"
            End Get
        End Property

        'Load Config AdaConfig.xml
        Public Shared Sub C_GETxConfigXml()
            Try
                oXmlCfg = XElement.Load(AdaConfigXml)

                Dim oQryApp = From c In oXmlCfg.Elements("Application") Select c
                For Each oItem In oQryApp
                    With oApplication
                        .tUser = oItem.Attribute("User").Value.SP_DecryptData
                        .tPass = oItem.Attribute("Pass").Value.SP_DecryptData
                        Try
                            .nLanguage = CInt(oItem.Attribute("Language").Value)
                        Catch ex As Exception
                            .nLanguage = 2 'Default Eng=2
                        End Try
                    End With
                Next
                oQryApp = Nothing

                Dim oQryConnSource = From c In oXmlCfg.Elements("Connection") Select c
                For Each oItem In oQryConnSource
                    With oConnSource
                        .tServer = oItem.Attribute("Server").Value
                        .tUser = oItem.Attribute("User").Value.SP_DecryptData
                        .tPassword = oItem.Attribute("Password").Value.SP_DecryptData
                        .tCatalog = oItem.Attribute("Catalog").Value
                        .nTimeOut = oItem.Attribute("TimeOut").Value
                    End With
                Next
                oQryConnSource = From c In oXmlCfg.Elements("ConnectionMem") Select c
                For Each oItem In oQryConnSource
                    With oMemConnSource
                        .tServer = oItem.Attribute("Server").Value
                        .tUser = oItem.Attribute("User").Value.SP_DecryptData
                        .tPassword = oItem.Attribute("Password").Value.SP_DecryptData
                        .tCatalog = oItem.Attribute("Catalog").Value
                        .nTimeOut = oItem.Attribute("TimeOut").Value
                    End With
                Next
                oQryConnSource = Nothing

                Dim oQrySeparator = From c In oXmlCfg.Elements("Separator") Select c
                For Each oItem In oQrySeparator
                    With oSeparator
                        Try
                            .nIndex = CInt(oItem.Attribute("Index").Value)
                        Catch ex As Exception
                            .nIndex = 1
                        End Try
                        .tDefine = oItem.Attribute("Define").Value
                    End With
                Next
                oQrySeparator = Nothing

                Dim oQryImport = From c In oXmlCfg.Elements("Import") Select c
                For Each oItem In oQryImport
                    With PathXml
                        .tImport = tAppPath & "\Config\" & oItem.Attribute("Path").Value
                    End With
                Next
                oQryImport = Nothing

                Dim oQryExport = From c In oXmlCfg.Elements("Export") Select c
                For Each oItem In oQryExport
                    With PathXml
                        .tExport = tAppPath & "\Config\" & oItem.Attribute("Path").Value
                    End With
                Next
                oQryImport = Nothing

                'Path sFTP *CH 22-06-2017
                Dim oQryFTP = From c In oXmlCfg.Elements("FTP") Select c
                For Each oItem In oQryFTP
                    With PathXml
                        .tFTP = tAppPath & "\Config\" & oItem.Attribute("Path").Value
                    End With
                Next
                oQryImport = Nothing

            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub



        Public Shared Sub C_GETxConfigXmlUrs()
            Try
                oXmlDefault = XElement.Load(AdaUrsDefault)

                Dim oQryApp = From c In oXmlDefault.Elements("UserDefault") Select c
                For Each oItem In oQryApp
                    With oUrsDefault

                        .tPlan = oItem.Attribute("Plant").Value
                        .tStorage = oItem.Attribute("StorageLocation").Value
                        .tRecieve = oItem.Attribute("ReceivePlant").Value
                        .tCompCode = oItem.Attribute("CompanyCode").Value
                        .tShopCode = oItem.Attribute("ShopCode").Value
                        .tShopCodeTran = oItem.Attribute("ShopCodeTran").Value
                        .tSupplier = oItem.Attribute("Supplier").Value
                        .tReason = oItem.Attribute("Reason").Value
                    End With
                Next

                oQryApp = Nothing

            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub






        'Update User Default Setting
        Public Shared Function C_SETbUpdateXMLUsrDefault() As Boolean
            C_SETbUpdateXMLUsrDefault = False
            Try
                oXmlDefault = XElement.Load(AdaUrsDefault)
                Dim oQry = From c In oXmlDefault.Elements("UserDefault") Select c

                For Each oItem In oQry
                    With oUrsDefault
                        oItem.Attribute("Plant").Value = .tPlan
                        oItem.Attribute("StorageLocation").Value = .tStorage
                        oItem.Attribute("ReceivePlant").Value = .tRecieve
                        oItem.Attribute("CompanyCode").Value = .tCompCode
                        oItem.Attribute("ShopCode").Value = .tShopCode
                        oItem.Attribute("ShopCodeTran").Value = .tShopCodeTran
                        oItem.Attribute("Supplier").Value = .tSupplier
                        oItem.Attribute("Reason").Value = .tReason
                    End With
                Next

                oQry = Nothing
                oXmlDefault.Save(AdaUrsDefault)
                C_SETbUpdateXMLUsrDefault = True

            Catch ex As Exception

            End Try
        End Function

        Public Shared ReadOnly Property AdaUrsDefault As String
            Get
                If File.Exists(tAppPath & "\Config\AdaUserDefault.xml") = False Then
                    Dim oFrmSetting As New wSetting()
                    oApplication = New oStrucApplication

                    If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.OK Then

                    End If
                End If
                Return tAppPath & "\Config\AdaUserDefault.xml"
            End Get
        End Property


        'Update Config AdaConfig.xml
        Public Shared Function C_SETbUpdateXml() As Boolean
            C_SETbUpdateXml = False
            Try
                oXmlCfg = XElement.Load(AdaConfigXml)
                Try
                    Dim oQryApp = From c In oXmlCfg.Elements("Application") Select c
                    For Each oItem In oQryApp
                        With oApplication
                            oItem.Attribute("User").Value = .tUser.SP_EncryptData
                            oItem.Attribute("Pass").Value = .tPass.SP_EncryptData
                            oItem.Attribute("Language").Value = .nLanguage.ToString
                        End With
                    Next
                    oQryApp = Nothing
                Catch ex As Exception

                End Try

                Dim oQryConnSource = From c In oXmlCfg.Elements("Connection") Select c
                For Each oItem In oQryConnSource
                    With oConnSource
                        oItem.Attribute("Server").Value = .tServer
                        oItem.Attribute("User").Value = .tUser.SP_EncryptData
                        oItem.Attribute("Password").Value = .tPassword.SP_EncryptData
                        oItem.Attribute("Catalog").Value = .tCatalog
                        oItem.Attribute("TimeOut").Value = .nTimeOut
                    End With
                Next
                'Member Database *CH 18-10-2014
                oQryConnSource = From c In oXmlCfg.Elements("ConnectionMem") Select c
                For Each oItem In oQryConnSource
                    With oMemConnSource
                        oItem.Attribute("Server").Value = .tServer
                        oItem.Attribute("User").Value = .tUser.SP_EncryptData
                        oItem.Attribute("Password").Value = .tPassword.SP_EncryptData
                        oItem.Attribute("Catalog").Value = .tCatalog
                        oItem.Attribute("TimeOut").Value = .nTimeOut
                    End With
                Next
                oQryConnSource = Nothing

                Dim oQrySeparator = From c In oXmlCfg.Elements("Separator") Select c
                For Each oItem In oQrySeparator
                    With oSeparator
                        oItem.Attribute("Index").Value = .nIndex
                        oItem.Attribute("Define").Value = .tDefine
                    End With
                Next
                oQrySeparator = Nothing

                oXmlCfg.Save(AdaConfigXml)
                C_SETbUpdateXml = True
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Function

        Public Shared Function C_SETbUpdateLang() As Boolean
            C_SETbUpdateLang = False
            Try
                oXmlCfg = XElement.Load(AdaConfigXml)
                Dim oQryApp = From c In oXmlCfg.Elements("Application") Select c
                For Each oItem In oQryApp
                    With oApplication
                        oItem.Attribute("User").Value = .tUser.SP_EncryptData
                        oItem.Attribute("Pass").Value = .tPass.SP_EncryptData
                        oItem.Attribute("Language").Value = .nLanguage.ToString
                    End With
                Next
                oQryApp = Nothing

                oXmlCfg.Save(AdaConfigXml)
                C_SETbUpdateLang = True
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Function

        'Test SQL Connection Source
        Public Shared Function C_CHKbSQLSourceConnect() As Boolean
            C_CHKbSQLSourceConnect = False
            Try
                Using oConn As New SqlClient.SqlConnection(tSQLConnSourceString)
                    oConn.Open()
                    oConn.Close()
                    C_CHKbSQLSourceConnect = True
                End Using
            Catch ex As Exception
            End Try
        End Function


        'User Default
        Public Shared oUrsDefault As New oStrucUrsDefault

        Structure oStrucUrsDefault
            Dim tPlan As String
            Dim tStorage As String
            Dim tRecieve As String
            Dim tCompCode As String
            Dim tShopCode As String
            Dim tShopCodeTran As String
            Dim tSupplier As String
            Dim tReason As String
        End Structure


        'Default Config
        Public Shared oApplication As New oStrucApplication
        Structure oStrucApplication
            Dim tUser As String
            Dim tPass As String
            Dim nLanguage As Integer
        End Structure

        Public Shared oConnSource As New oStrucConnection
        Structure oStrucConnection
            Dim tServer As String
            Dim tUser As String
            Dim tPassword As String
            Dim tCatalog As String
            Dim nTimeOut As Integer
        End Structure

        '*CH 18-10-2014
        Public Shared oMemConnSource As New oStrucMemConnection
        Structure oStrucMemConnection
            Dim tServer As String
            Dim tUser As String
            Dim tPassword As String
            Dim tCatalog As String
            Dim nTimeOut As Integer
        End Structure

        Public Shared oSeparator As New oStrucSeparator
        Structure oStrucSeparator
            Dim nIndex As Integer
            Dim tDefine As String
        End Structure

        Public Shared PathXml As New oPath
        Structure oPath
            Dim tImport As String
            Dim tExport As String
            Dim tFTP As String
        End Structure

        'Upload promotion '*CH 22-12-2014
        Public Shared UploadPmt As New oUploadPmt
        Structure oUploadPmt
            Dim tPathPmt As String
            Dim tTypePmt As String
        End Structure

        Public Shared ReadOnly Property tSeparator As String
            Get
                Dim tChr As String = ""
                Select Case oSeparator.nIndex
                    Case 0
                        tChr = ","
                    Case 1
                        tChr = "|"
                    Case 2
                        tChr = vbTab
                    Case 3
                        tChr = ";"
                    Case 4
                        tChr = oSeparator.tDefine
                End Select
                Return tChr
            End Get
        End Property

        Public Shared ReadOnly Property oConfigXml As cConfigXml
            Get
                oConfigXml = New cConfigXml
                '==Import==
                Dim bCreateNew As Boolean = False

                Dim oXEImport As XElement = _
                <Config>
                    <Import Name="Inbox" Path="" Log="" Backup=""></Import>
                    <Import Name="Config" Split="5000" CountIndex="100000"></Import>
                </Config>

                If File.Exists(PathXml.tImport) = False Then
                    'ไม่พบ File Config สร้างใหม่
                    Dim aXml = (From c In oXEImport.Elements("Import") Select c).FirstOrDefault
                    aXml.Attribute("Path").Value = Application.StartupPath & "\Inbox"
                    aXml.Attribute("Log").SetValue(Application.StartupPath & "\Log")
                    aXml.Attribute("Backup").SetValue(Application.StartupPath & "\Backup")
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEImport)
                    oNewXml.Save(PathXml.tImport)
                    aXml = Nothing
                End If
                '-----------------------------------------------------------------------------------------------
                Dim oImportXml As XElement = XElement.Load(PathXml.tImport)
                Dim aImportDefaultXml = oXEImport.Elements("Import").Attributes.Where(Function(c) c.Name = "Name")
                Dim aImportFileXml = oImportXml.Elements("Import").Attributes.Where(Function(c) c.Name = "Name")
                Dim bUpdate As Boolean = False

                For Each oItemDef In aImportDefaultXml
                    Dim oItemChk = aImportFileXml.Where(Function(c) c.Value = oItemDef.Value)
                    If oItemChk.Count = 0 Then
                        Dim oXEInsert = oXEImport.Elements("Import").Attributes.Where(Function(c) c.Name = "Name" And c.Value = oItemDef.Value)
                        Dim oAttr As XElement
                        Select Case oItemDef.Value
                            Case "Inbox"
                                oAttr = <Import Name="Inbox" Path="" Log="" Backup=""></Import>
                                oImportXml.Add(oAttr)
                            Case "Config"
                                oAttr = <Import Name="Config" Split="5000" CountIndex="100000"></Import>
                                oImportXml.Add(oAttr)
                        End Select
                        bUpdate = True
                    End If
                Next

                If bUpdate = True Then
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oImportXml)
                    oNewXml.Save(PathXml.tImport)
                    oImportXml = XElement.Load(PathXml.tImport)
                End If

                Dim aEmImport = From c In oImportXml.Elements("Import") Select c
                'Inbox
                Dim oInbox = aEmImport.Where(Function(c) c.Attribute("Name") = "Inbox").FirstOrDefault
                Dim oConfig = aEmImport.Where(Function(c) c.Attribute("Name") = "Config").FirstOrDefault
                Dim oSkyOutbox = aEmImport.Where(Function(c) c.Attribute("Name") = "AdaSkyOutbox").FirstOrDefault

                With oConfigXml
                    .tInbox = oInbox.Attribute("Path").Value
                    .tLog = oInbox.Attribute("Log").Value
                    .tBackup = oInbox.Attribute("Backup").Value
                    .nSplit = oConfig.Attribute("Split").Value
                    .nCountIndex = oConfig.Attribute("CountIndex").Value
                    If .nSplit < 1000 Then
                        .nSplit = 1000
                    End If

                    .tTemp = Application.StartupPath & "\Temp"

                    'Check Folder
                    If Directory.Exists(.tInbox) = False Then Directory.CreateDirectory(.tInbox)
                    If Directory.Exists(.tLog) = False Then Directory.CreateDirectory(.tLog)
                    If Directory.Exists(.tBackup) = False Then Directory.CreateDirectory(.tBackup)
                    If Directory.Exists(.tTemp) = False Then Directory.CreateDirectory(.tTemp)
                End With

                '==Export==


                'ไม่พบ File Config สร้างใหม่
                Dim oXEExport As XElement = _
                <Config>
                    <Export Name="Outbox" Path=""></Export>
                </Config>
                If IO.File.Exists(PathXml.tExport) = False Then
                    Dim aXml = (From c In oXEExport.Elements("Export") Select c).FirstOrDefault
                    aXml.Attribute("Path").SetValue(Application.StartupPath & "\Outbox")
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEExport)
                    oNewXml.Save(PathXml.tExport)
                    aXml = Nothing
                End If
                '-----------------------------------------------------------------------------------------------
                Dim oExportXml As XElement = XElement.Load(PathXml.tExport)

                Dim aExportDefaultXml = oXEExport.Elements("Export").Attributes.Where(Function(c) c.Name = "Name")
                Dim aExportFileXml = oExportXml.Elements("Export").Attributes.Where(Function(c) c.Name = "Name")
                bUpdate = False

                For Each oItemDef In aExportDefaultXml
                    Dim oItemChk = aExportFileXml.Where(Function(c) c.Value = oItemDef.Value)
                    If oItemChk.Count = 0 Then
                        Dim oXEInsert = oXEImport.Elements("Export").Attributes.Where(Function(c) c.Name = "Name" And c.Value = oItemDef.Value)
                        Dim oAttr As XElement
                        Select Case oItemDef.Value
                            Case "Inbox"
                                oAttr = <Export Name="Outbox" Path=""></Export>
                                oExportXml.Add(oAttr)
                        End Select
                    End If
                Next

                If bUpdate = True Then
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oExportXml)
                    oNewXml.Save(PathXml.tExport)
                    oExportXml = XElement.Load(PathXml.tExport)
                End If

                Dim aEmExport = From c In oExportXml.Elements("Export") Select c
                'Outbox
                Dim oOutbox = aEmExport.Where(Function(c) c.Attribute("Name") = "Outbox").FirstOrDefault
                With oConfigXml
                    .tOutbox = oOutbox.Attribute("Path").Value
                    'Check Folder
                    If Directory.Exists(.tOutbox) = False Then Directory.CreateDirectory(.tOutbox)
                End With

                '==FTP=='
                Dim oXEFtp As XElement =
                <Config>
                    <FTP Name="Bound" InFolder="" OutFolder="" Reconcile=""></FTP>
                    <FTP Name="sFTP" HostName="" UserName="" Password="" PortNumber="" FTPType=""></FTP>
                    <FTP Name="Bound" CashTransfer="" Expense=""></FTP>
                </Config>

                If File.Exists(PathXml.tFTP) = False Then
                    'ไม่พบ File Config สร้างใหม่
                    Dim aXml = (From c In oXEFtp.Elements("FTP") Select c).FirstOrDefault
                    aXml.Attribute("InFolder").Value = "InBound"
                    aXml.Attribute("OutFolder").SetValue("OutBound")
                    aXml.Attribute("Reconcile").SetValue("Reconcile")
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEFtp)
                    oNewXml.Save(PathXml.tFTP)
                    aXml = Nothing
                End If
                '-----------------------------------------------------------------------------------------------
                Dim oFTPXml As XElement = XElement.Load(PathXml.tFTP)
                Dim aFTPFileXml = oFTPXml.Elements("FTP").Attributes.Where(Function(c) c.Name = "Name")

                Dim bAddBound As Boolean = True
                Dim bAddFTP As Boolean = True
                Dim bAddZGLINT As Boolean = True
                For Each oItemDef In aFTPFileXml
                    Select Case oItemDef.Value
                        Case "Bound" : bAddBound = False
                        Case "sFTP" : bAddFTP = False
                        Case "ZGLINT" : bAddZGLINT = False
                    End Select
                Next
                If bAddBound Then
                    Dim oAttr As XElement
                    oAttr = <FTP Name="Bound" InFolder="" OutFolder="" Reconcile=""></FTP>
                    oFTPXml.Add(oAttr)
                    bUpdate = True
                End If
                If bAddFTP Then
                    Dim oAttr As XElement
                    oAttr = <FTP Name="sFTP" HostName="" UserName="" Password="" PortNumber="" FTPType=""></FTP>
                    oFTPXml.Add(oAttr)
                    bUpdate = True
                End If
                If bAddZGLINT Then
                    Dim oAttr As XElement
                    oAttr = <FTP Name="ZGLINT" CashTransfer="" Expense=""></FTP>
                    oFTPXml.Add(oAttr)
                    bUpdate = True
                End If

                If bUpdate = True Then
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oFTPXml)
                    oNewXml.Save(PathXml.tFTP)
                    oFTPXml = XElement.Load(PathXml.tFTP)
                End If

                Dim aEmFTP = From c In oFTPXml.Elements("FTP") Select c
                Dim oBound = aEmFTP.Where(Function(c) c.Attribute("Name") = "Bound").FirstOrDefault
                Dim osFTP = aEmFTP.Where(Function(c) c.Attribute("Name") = "sFTP").FirstOrDefault
                Dim oZGLINT = aEmFTP.Where(Function(c) c.Attribute("Name") = "ZGLINT").FirstOrDefault

                With oConfigXml
                    .tInFolder = oBound.Attribute("InFolder").Value
                    .tOutFolder = oBound.Attribute("OutFolder").Value
                    .tReconcile = oBound.Attribute("Reconcile").Value
                    .tHostName = osFTP.Attribute("HostName").Value
                    .tUserName = osFTP.Attribute("UserName").Value
                    .tPassword = osFTP.Attribute("Password").Value
                    .tPortNumber = osFTP.Attribute("PortNumber").Value
                    .tFTPType = osFTP.Attribute("FTPType").Value
                    .tCashTransfer = oZGLINT.Attribute("CashTransfer").Value
                    .tExpense = oZGLINT.Attribute("Expense").Value
                End With

            End Get
        End Property

        Public Shared Sub C_CALxUpdateImport(ptInbox As String, ptLog As String, ptBackup As String)
            Try
                Dim oImportXml As XElement = XElement.Load(PathXml.tImport)
                Dim aEmImport = From c In oImportXml.Elements("Import") Select c
                'Inbox
                Dim oInbox = aEmImport.Where(Function(c) c.Attribute("Name") = "Inbox").FirstOrDefault
                oInbox.Attribute("Path").SetValue(ptInbox)
                oInbox.Attribute("Log").SetValue(ptLog)
                oInbox.Attribute("Backup").SetValue(ptBackup)

                oImportXml.Save(PathXml.tImport)
                oImportXml = Nothing
                aEmImport = Nothing
                oInbox = Nothing
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Public Shared Sub C_CALxUpdateExport(ptOutbox As String)
            Try
                Dim oExportXml As XElement = XElement.Load(PathXml.tExport)
                Dim aEmExport = From c In oExportXml.Elements("Export") Select c
                'Inbox
                Dim oOutbox = aEmExport.Where(Function(c) c.Attribute("Name") = "Outbox").FirstOrDefault
                oOutbox.Attribute("Path").SetValue(ptOutbox)
                oExportXml.Save(PathXml.tExport)
                oExportXml = Nothing
                aEmExport = Nothing
                oOutbox = Nothing
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        ''' <summary>
        ''' Update sFTP Config
        ''' </summary>
        ''' <param name="ptHostName"></param>
        ''' <param name="ptUserName"></param>
        ''' <param name="ptPassword"></param>
        ''' <param name="ptPortNumber"></param>
        ''' <param name="ptInFolder"></param>
        ''' <param name="ptOutFolder"></param>
        Public Shared Sub C_CALxUpdateFTP(ptHostName As String, ptUserName As String, ptPassword As String, ptPortNumber As String,
                                          ptInFolder As String, ptOutFolder As String, ByVal ptReconcileFolder As String,
                                          ByVal ptCashTnf As String, ByVal ptExpense As String, ByVal ptTypeFTP As String)
            Try
                Dim oFtpXml As XElement = XElement.Load(PathXml.tFTP)
                Dim aEmFtp = From oFtp In oFtpXml.Elements("FTP") Select oFtp
                'Bound
                Dim oBound = aEmFtp.Where(Function(c) c.Attribute("Name") = "Bound").FirstOrDefault
                oBound.Attribute("InFolder").SetValue(ptInFolder)
                oBound.Attribute("OutFolder").SetValue(ptOutFolder)
                oBound.Attribute("Reconcile").SetValue(ptReconcileFolder)
                oFtpXml.Save(PathXml.tFTP)

                'sFTP
                Dim osFTP = aEmFtp.Where(Function(c) c.Attribute("Name") = "sFTP").FirstOrDefault
                osFTP.Attribute("HostName").SetValue(ptHostName)
                osFTP.Attribute("UserName").SetValue(ptUserName)
                osFTP.Attribute("Password").SetValue(ptPassword)
                osFTP.Attribute("PortNumber").SetValue(ptPortNumber)
                osFTP.Attribute("FTPType").SetValue(ptTypeFTP)
                oFtpXml.Save(PathXml.tFTP)

                'ZGLINT
                Dim oZGLINT = aEmFtp.Where(Function(c) c.Attribute("Name") = "ZGLINT").FirstOrDefault
                oZGLINT.Attribute("CashTransfer").SetValue(ptCashTnf)
                oZGLINT.Attribute("Expense").SetValue(ptExpense)
                oFtpXml.Save(PathXml.tFTP)

                oFtpXml = Nothing
                aEmFtp = Nothing
                oBound = Nothing
                osFTP = Nothing
                oZGLINT = Nothing
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Public Shared ReadOnly Property tSQLConnSourceString As String
            Get
                Return String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};Connect Timeout={4};APP=" & My.Application.Info.ProductName.ToString, oConnSource.tServer, oConnSource.tCatalog, oConnSource.tUser, oConnSource.tPassword, 5) 'Connection.nTimeOut
            End Get
        End Property

        'Member Connection *CH 18-10-2014
        Public Shared ReadOnly Property tSQLMemConnSourceString As String
            Get
                Return String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};Connect Timeout={4};APP=" & My.Application.Info.ProductName.ToString, oMemConnSource.tServer, oMemConnSource.tCatalog, oMemConnSource.tUser, oMemConnSource.tPassword, 5) 'Connection.nTimeOut
            End Get
        End Property

        ''' <summary>
        ''' Config AdaLinkTCC 
        ''' </summary>
        ''' <value></value>
        ''' <returns>Path Config</returns>
        ''' <remarks>Add Path upload.xml *CH 22-12-2014</remarks>
        Public Shared ReadOnly Property AdaConfigUploadXml As String
            Get
                If File.Exists(tAppPath & "\Config\Upload.xml") = False Then
                    If Directory.Exists(tAppPath & "\Config") = False Then
                        Directory.CreateDirectory(tAppPath & "\Config")
                    End If
                    Dim oXEAdaConfig As XElement = _
                        <config>
                            <Upload Path="" Type="xls,xlsx"></Upload>
                        </config>
                    Dim oAdaConfigXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEAdaConfig)
                    oAdaConfigXml.Save(tAppPath & "\Config\Upload.xml")
                    oAdaConfigXml = Nothing
                    oXEAdaConfig = Nothing
                End If

                Return tAppPath & "\Config\Upload.xml"
            End Get
        End Property

        'Load Config AdaConfigUpload.xml
        Public Shared Sub C_GETxConfigUploadXml()
            Try
                oXmlCfg = XElement.Load(AdaConfigUploadXml)

                'Upload promotion '*CH 22-12-2014
                Dim oQryUpload = From c In oXmlCfg.Elements("Upload") Select c
                For Each oItem In oQryUpload
                    With UploadPmt
                        .tPathPmt = oItem.Attribute("Path").Value
                        .tTypePmt = oItem.Attribute("Type").Value
                    End With
                Next

                oQryUpload = Nothing

            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        'Update Config AdaConfigUpload.xml
        Public Shared Function C_SETbUpdateConfigUploadXml(ByVal ptPath As String) As Boolean
            C_SETbUpdateConfigUploadXml = False
            Try
                oXmlCfg = XElement.Load(AdaConfigUploadXml)

                Dim oQryUpload = From c In oXmlCfg.Elements("Upload") Select c
                For Each oItem In oQryUpload
                    oItem.Attribute("Path").Value = ptPath
                    oItem.Attribute("Type").Value = oItem.Attribute("Type").Value
                Next
                oQryUpload = Nothing

                oXmlCfg.Save(AdaConfigUploadXml)
                C_SETbUpdateConfigUploadXml = True
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Function
    End Class

End Namespace


