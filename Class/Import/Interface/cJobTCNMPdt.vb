﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Text

Public Class cJobTCNMPdt
    Inherits cBaseImport
    Property oC_TCNMPdt As New cTCNMPdt
    Property tC_Code As String = cImportTemplate.cTableCode.TCNMPdt
    Property tC_Table As String = "TCNMPdt"
    Property tC_TableTemp As String = "TLNKPdt"
    Private oTCNMPdtUnit As cJobTCNMPdtUnit
    Private tSpr As String = AdaConfig.cConfig.tSeparator
    Private tC_FullPath As String = ""
    Private tC_Name As String = ""
    Public tC_FileLog As String = ""
    Public tC_PathList As String = ""
    Public Shared aC_ArticleList As List(Of cArticleList)

    Public Overrides Sub C_GETxFile()
        aC_Detail.Clear()
        aC_DetailSub.Clear()
        Dim tFullName As String = ""
        Dim aFullName As String()
        Dim oDirInfoInbox As New DirectoryInfo(AdaConfig.cConfig.oConfigXml.tInbox)
        Dim oFiles As FileInfo() = oDirInfoInbox.GetFiles("*.txt")
        Dim oSortFiles = oFiles.OrderBy(Function(o) o.Name)

        For Each oItem In oSortFiles 'oFiles
            tFullName = Path.GetFileName(oItem.FullName)
            aFullName = tFullName.Split("_")
            If aFullName(3).ToUpper = "Material.txt" AndAlso aFullName.Count >= 3 Then
                Dim oFileDetail As New cBaseImport.cImportDetail
                oFileDetail.tC_Name = oItem.Name.ToUpper
                oFileDetail.tC_FullName = oItem.FullName
                Dim nMaxLen As Integer = oFileDetail.tC_Name.Length
                If Not nMaxLen > 20 Then
                    Continue For
                End If

                Dim tDateTime As String = ""
                tDateTime = SP_CNSETtStrConvertDateTime(aFullName(1) & aFullName(2))
                If IsDate(tDateTime) = True Then
                    oFileDetail.tC_Date = tDateTime
                    oFileDetail.nC_Status = 0
                Else
                    Continue For
                End If

                oFileDetail.nC_Length = oItem.Length
                oFileDetail.tC_Length = oItem.Length.SP_CNSETtFormatFileSize
                aC_Detail.Add(oFileDetail)
                aC_DetailSub.Add(oFileDetail)
            End If
        Next

        oFiles = Nothing
        oDirInfoInbox = Nothing
    End Sub

    Public Overrides Sub C_CALxProcess()
        'Dim tFileBak As String = ""
        Dim nFile As Integer = 1
        Dim nLastLang As Integer = 0 '1:thai, 2:Eng
        Dim tC_PdtList As String = ""
        Dim tC_PdtFile As String = ""
        Dim tFullPath As String = ""
        Dim tFullName As String = ""
        Dim oPrepareFile As New cPrepareFile
        Dim tMsgV As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLastLang = 1
        Dim oDatabase As New cDatabaseLocal
        Dim tMsg As String = ""
        Try
            Dim cNextVal As Double = 0
            Dim cProgress As Double = 0
            cNextVal = 100 / aC_Detail.Count

            For Each oItem In aC_Detail.Where(Function(c) c.nC_Select = 1 And c.nC_Status = 0)
                '   aC_ArticleList = New List(Of cArticleList)
                'เตรียมข้อมูล
                oPrepareFile.tFullPath = oItem.tC_FullName
                tFullPath = oPrepareFile.tFullPath
                '  oPrepareFile.C_CALxPrepareArticle()
                oPrepareFile.tFullName = oItem.tC_Name
                tFullName = oPrepareFile.tFullName
                cCNVB.tVB_FullFileList = oPrepareFile.tFullPath
                Dim aItemCol As String() = tFullName.Split("\")
                Dim aItemColList As String() = tFullName.Split("_")
                tC_PdtList = aItemColList(2).Replace(".txt", "")

                '*CH 20-12-2014
                cImportTemplate.tC_File = "" '" [" & nFile & "]"
                If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                tC_FileLog = String.Format(tC_Log, Path.GetFileName(oItem.tC_FullName)).Replace(".xml", ".txt")
                cCNVB.tVB_Log = tC_FileLog
                With oC_TCNMPdt
                    .tFileLog = tC_FileLog
                    .tTableTemp = tC_TableTemp
                    .tTable = tC_Table
                    .tFullPath = oItem.tC_FullName
                    .tDateNow = tC_DateNow
                    tC_FullPath = oItem.tC_FullName
                    tC_Name = oItem.tC_Name
                    Dim aItemColFile As String() = tC_Name.Split("_")
                    tC_PdtFile = aItemColFile(2).Replace(".XML", "")
                    If tC_PdtList.ToLower = tC_PdtFile.ToLower Then '**PAN 59-06-06 
                        nC_GridRow = nFile
                        oPrepareFile.tFullPath = oItem.tC_FullName
                        oPrepareFile.tC_Log = tC_FileLog

                        'เตรียมไฟล์ เข้าตาราง Temp
                        cImportTemplate.tC_File = " เตรียมข้อมูล; Prepare data".Split(";")(nLastLang)
                        If Not oPrepareFile.C_CALbPreparePdt() Then Exit For
                        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(30)

                        oPrepareFile.tFullPath = oItem.tC_FullName
                        .tFullPath = oItem.tC_FullName
                        .cNextVal = cNextVal
                        .cProgress = cProgress

                        'Check Article File 0 KB '*CH 2015-01-04
                        Dim oRdr = New StreamReader(oItem.tC_FullName, System.Text.Encoding.Default)
                        Dim bNotFile0Kb As Boolean = True
                        While True
                            If oRdr.ReadLine Is Nothing Then
                                bNotFile0Kb = False
                                Exit While
                            End If
                            Exit While
                        End While
                        oRdr.Dispose()
                        oRdr.Close()
                        oRdr = Nothing

                        If bNotFile0Kb Then
                            'Insert Product
                            .tFileLog = tC_FileLog
                            cImportTemplate.tC_File = " [" & nFile & "] " & " นำเข้าข้อมูล; Import data".Split(";")(nLastLang)
                            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(35)
                            If .C_CALbProcess() Then 'Import สำเร็จ
                                oItem.nC_Status = 1
                                aC_Detail(nFile - 1).nC_Status = 1
                                nC_StaUpload = 1
                            Else 'Import ไม่สำเร็จ
                                oItem.nC_Status = 2
                                aC_Detail(nFile - 1).nC_Status = 2
                                nC_StaUpload = 2
                            End If
                            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(90)

                        Else
                            tMsg = "ไม่พบข้อมูลสินค้า;Not have data article.".Split(";")(nLastLang)
                            C_CALxWriteLogErr(tMsg, oItem.tC_FullName)
                            oItem.nC_Status = 2
                            aC_Detail(nFile - 1).nC_Status = 2
                            nC_StaUpload = 2
                        End If

                        'Move File Backup
                        C_CALxBackupFile(oItem.tC_FullName)
                    Else
                        Continue For
                    End If
                End With
                nFile += 1
            Next
        Catch ex As Exception
            C_CALxWriteLogErr(ex.Message)
        Finally
            oPrepareFile = Nothing
            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(100)
        End Try
    End Sub


    Public Sub C_CALxWriteLogErr(ByVal ptMsg As String, Optional ByVal ptFileArticle As String = "", Optional ByVal ptFileLog As String = "")
        Try
            Dim tFileArticle As String = ""
            Dim tFileLog As String = IIf(ptFileLog <> "", ptFileLog, tC_FileLog)
            Dim oFileLog As New cImportLogHis(tFileLog)
            Dim oNewLog As New cImportLogHis.cLogHis
            With oNewLog
                .tErrMsg = ptMsg
            End With
            oFileLog.C_CALxAddNew(oNewLog)
            oFileLog.C_CALbSaveLogErr()
            Dim tDesc As String = "Insert(0),Update(0),Failed(1)"
            Dim oImp As New cImport
            tFileArticle = IIf(ptFileArticle <> "", ptFileArticle, tC_FullPath)
            oImp.C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFileArticle), tDesc, tFileLog))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub C_CALxWriteLogEr(ByVal patErr As ArrayList, ByVal ptFullPath As String, ByVal pnInsert As Integer, ByVal pnFailed As Integer)
        Dim tC_FileLog As String = AdaConfig.cConfig.tAppPath & "\Log"
        Dim dDateNow As Date = Date.Now
        Dim oHDfile As New StringBuilder
        Dim tDesc As String = ""
        Dim tFileName As String = ""
        Dim oSQL As New StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim tDateTimeNow As String = Format(Now, "yyyy-MM-dd HH:mm:ss.fff")
        Dim tErr As String = ""
        Try
            If Not IO.Directory.Exists(tC_FileLog) Then
                IO.Directory.CreateDirectory(tC_FileLog)
            End If

            tC_FileLog &= "\" & "Log_Master" & Format(Date.Now, "yyyyMMdd-HHmmss") & ".txt"
            If File.Exists(tC_FileLog) = False Then
                Using oSw As StreamWriter = File.CreateText(tC_FileLog)
                    oSw.Close()
                End Using

                oHDfile.Append("[Import Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Import Time] " + Format(Date.Now, "HH:mm:ss") + vbTab + "")
                oHDfile.Append("[Import By] " + Environment.MachineName + Environment.NewLine + Environment.NewLine + "")
                oHDfile.Append("[Code]" + vbTab + vbTab + vbTab + "[Description]")
                Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                    With oStreamWriter
                        .WriteLine(oHDfile)
                        .Flush()
                        .Close()
                    End With
                End Using
            End If

            For nRow = 0 To patErr.Count - 1
                tErr += patErr(nRow) + Environment.NewLine
            Next

            Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                With oStreamWriter
                    .WriteLine(tErr)
                    .Flush()
                    .Close()
                End With
            End Using

            tDesc = "Success(" + Convert.ToString(pnInsert) + "),Failed(" + Convert.ToString(pnFailed) + ")"
            tFileName = Path.GetFileName(ptFullPath)
            oSQL.Clear()
            oSQL.AppendLine("INSERT INTO TLNKLog(uid,FDDateTime,FNType,FTFileName,FTDescription,FTRefer) VALUES")
            oSQL.AppendLine("('" + Guid.NewGuid.ToString + "','" + tDateTimeNow + "',1,'" + tFileName + "','" + tDesc + "','" + tC_FileLog + "')")
            If oSQL.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub C_CALxBackupFile(ByVal ptFileCopyBk As String)
        Try
            Dim tFileName As String = ""
            If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import") Then
                IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import")
            End If
            Try
                If File.Exists(ptFileCopyBk) Then
                    tFileName = Path.GetFileName(ptFileCopyBk)
                    FileCopy(ptFileCopyBk, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import\" & tFileName)
                    File.Delete(ptFileCopyBk)
                End If
            Catch ex As Exception
            End Try
        Catch ex As Exception
        End Try
    End Sub



    Class cArticleList
        Property FDArtDocDate As String = ""
        Property FNArtSeqNo As String = ""
        ' Property FTPdtStkCode As String = ""
        Property FTArtListFile As String = ""
        Property FTArtFile As String = ""
        Property FTArtStaPrc As String = "NULL"
        Property FTDataCnt As String = ""
    End Class
End Class
