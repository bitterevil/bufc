﻿Imports System.IO

Public Class cJobTCNMPdtUnit
    Inherits cBaseImport

    Property tFullPath As String = ""
    Property oC_TCNMPdtUnit As New cTCNMPdtUnit
    Property tC_Table As String = "TCNMPdtUnit"
    Property tC_TableTemp As String = "TLNKPdtUnit"

    Public Overrides Sub C_CALxProcess()
        With oC_TCNMPdtUnit
            .tFileLog = tC_Log
            .tTableTemp = tC_TableTemp
            .tTable = tC_Table
            .tFullPath = tFullPath
            .C_CALbProcess()
        End With
    End Sub

    Public Overrides Sub C_GETxFile()

    End Sub

End Class
