﻿Imports System.IO

Public Class cJobTCNTPdtDetail
    Inherits cBaseImport

    Property tFullPath As String = ""
    Property oC_TCNTPdtDetail As New cTCNTPdtDetail
    Property tC_Table As String = "TCNTPdtDetail"
    Property tC_TableTemp As String = "TLNKPdtDetail"

    Public Overrides Sub C_CALxProcess()
        With oC_TCNTPdtDetail
            .tFileLog = tC_Log
            .tTableTemp = tC_TableTemp
            .tTable = tC_Table
            .tFullPath = tFullPath
            .C_CALbProcess()
        End With
    End Sub

    Public Overrides Sub C_GETxFile()

    End Sub

End Class
