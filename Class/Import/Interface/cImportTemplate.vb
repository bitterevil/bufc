﻿Imports System.IO
Imports System.Text

Public Class cImportTemplate

    'Sub Class Enum
    Class cTableCode 'Class Enum (ใช้แทน Enum) *Fix
        Public Const TCNMPdtGrp As String = "001"
        Public Const TCNMPdt As String = "002"
        Public Const TCNTPmt As String = "003"
        Public Const TCNTPdtAjp As String = "004"
        Public Const TCNTPdtTnf As String = "005"  ' Type 1
    End Class

    '*CH 19-12-2014
    Private Const tC_Acticle As String = "สินค้า;Article"
    Private Const tC_Promotion As String = "โปรโมชั่น;Promotion"
    Private Const tC_PdtAjp As String = "ข้อมูลใบปรับราคาขาย;Sale price adjustment"
    Private Const tC_PdtGrp As String = "กลุ่มสินค้า;Product Group"
    Private Const tC_PdtTnf As String = "ข้อมูลใบรับเข้าสินค้า;Product Received"
    Public Shared tC_Status As String = ""
    Public Shared tC_File As String = ""

    Shared Property oC_TCNTPmt As cJobTCNTPmtImp 'Promotion Price '*CH 27-11-2014
    Shared Property oC_TCNMPdt As cJobTCNMPdt
    Shared Property oC_TCNTPdtAjp As cJobTCNTPdtAjp
    Shared Property oC_TCNTPdtTnf As cJobTCNTPdtTnf
    Shared Property oC_TCNMPdtGrp As cJobTCNMPdtGrp
    Shared Property bC_Status As Boolean = False
    Shared Property oC_Progress As ProgressBar

    'List Import แสดงใน Grid
    Private Shared oC_Items As List(Of cItem)
    Public Shared Function C_GETaItemImport() As List(Of cItem) 'Fix Data
        If bC_Status = True Then Return Nothing
        If oC_Items Is Nothing Then
            oC_Items = New List(Of cItem)
            With oC_Items
                .Add(New cItem With {.tCode = cTableCode.TCNMPdt, .tItem = "Article", .tItemThai = "สินค้า"})
                .Add(New cItem With {.tCode = cTableCode.TCNTPdtAjp, .tItem = "Sale price adjustment", .tItemThai = "ข้อมูลใบปรับราคาขาย"})
                .Add(New cItem With {.tCode = cTableCode.TCNTPdtTnf, .tItem = "Prduct Received", .tItemThai = "ข้อมูลใบรับเข้าสินค้า"})
            End With
        End If

        'Call Load file   หาจำนวนไฟล์
        C_CALxInitail()
        Return oC_Items
    End Function

    'Load FileInfo to Class Import
    Private Shared Sub C_CALxInitail()
        bC_Status = False
        oC_TCNMPdt = New cJobTCNMPdt
        oC_TCNTPdtAjp = New cJobTCNTPdtAjp
        oC_TCNTPdtTnf = New cJobTCNTPdtTnf
        Try
            'Move File to Archive
            ' cCNSP.SP_GETxMoveXml2Archive()
            If AdaConfig.cConfig.oConfigXml.tFTPType = "FTP" Then
                cCNSP.SP_GETxDownloadToInbox()      ' Download ไฟล์จาก FTP
            Else
                cCNSP.SP_GETxDownloadToInboxsFTP()  ' Download ไฟล์จาก sFTP
            End If


            ' Get file เข้า Model เพื่อแสดงใน grid Detail
            oC_TCNMPdt.C_GETxFile()
            oC_TCNTPdtAjp.C_GETxFile()
            oC_TCNTPdtTnf.C_GETxFile()

            'Update จำนวนไฟล์
            Dim tSelCount As String = ""
            For Each oItem In oC_Items
                Select Case oItem.tCode
                    Case cTableCode.TCNMPdt
                        tSelCount = oC_TCNMPdt.tC_SelCount
                        oItem.nCount = oC_TCNMPdt.nC_Count
                    Case cTableCode.TCNTPdtAjp
                        tSelCount = oC_TCNTPdtAjp.tC_SelCount
                        oItem.nCount = oC_TCNTPdtAjp.nC_Count
                    Case cTableCode.TCNTPdtTnf
                        tSelCount = oC_TCNTPdtTnf.tC_SelCount
                        oItem.nCount = oC_TCNTPdtTnf.nC_Count
                End Select

                If oItem.nCount = 0 Then
                    oItem.nSelect = 0
                End If
                oItem.tSelectCount = tSelCount
            Next

        Catch ex As Exception
            cCNSP.SP_MSGnShowing(ex.Message, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
        End Try
    End Sub

    'Process แต่ละงาน
    ''' <summary>
    ''' task progress import
    ''' </summary>
    Public Shared Sub C_CALxProcessAll()
        bC_Status = True
        C_CHKxDbTemp()
        Dim tDateNow As String = Format(Date.Now, "yyyyMMddHHmmss")
        For Each oItem In oC_Items.Where(Function(c) c.nSelect = 1) 'Where nSelect = รายการที่เลือก 1:ทำ , 2:ไม่ทำ
            Select Case oItem.tCode
                Case cTableCode.TCNMPdt
                    If cCNVB.nVB_ImportPdt = 1 Then
                        tC_File = ""
                        tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_Acticle.Split(";")(0), tC_Acticle.Split(";")(1))
                        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                        oC_TCNMPdt.tC_Log = cLogTemplate.tC_FileMaster
                        oC_TCNMPdt.tC_DateNow = tDateNow
                        oC_TCNMPdt.C_CALxProcess()
                    End If


                Case cTableCode.TCNTPdtAjp
                    If cCNVB.nVB_ImportAjp = 1 Then
                        tC_File = ""
                        tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_PdtAjp.Split(";")(0), tC_PdtAjp.Split(";")(1))
                        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                        oC_TCNTPdtAjp.tC_Log = cLogTemplate.tC_FileTCNTPdtAJP
                        oC_TCNTPdtAjp.tC_DateNow = tDateNow
                        oC_TCNTPdtAjp.C_CALxProcess()
                    End If

                Case cTableCode.TCNTPdtTnf
                    If cCNVB.nVB_ImportTnf = 1 Then
                        tC_File = ""
                        tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_PdtTnf.Split(";")(0), tC_PdtTnf.Split(";")(1))
                        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                        oC_TCNTPdtTnf.tC_Log = cLogTemplate.tC_FileTCNTPdtTnf
                        oC_TCNTPdtTnf.tC_DateNow = tDateNow
                        oC_TCNTPdtTnf.C_CALxProcess()
                    End If

            End Select
        Next
        'Call C_CALxBackupFile("", tDateNow)
        'Purge File From FTP '*CH 25-09-2017
        ' cCNSP.SP_SETxPurgeBackup()
    End Sub

    ''' <summary>
    ''' ตรวจสอบ และสร้างตาราง Temp Product
    ''' </summary>
    Private Shared Sub C_CHKxDbTemp()
        Dim oSQL As New StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Try
            ' Check DB Pdt Temp
            oSQL.Clear()
            oSQL.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'TLNKPdt') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine(" CREATE TABLE [dbo].[TLNKPdt](")
            oSQL.AppendLine(" [FTPdtCode] [varchar](20) NULL,")
            oSQL.AppendLine(" [FTPdtName] [varchar](100) NULL,")
            oSQL.AppendLine(" [FTPdtNameOth] [nvarchar](100) NULL,")
            oSQL.AppendLine(" [FTPdtBarCode1] [varchar](25) NULL,")
            oSQL.AppendLine(" [FTPdtBarCode2] [varchar](25) NULL,")
            oSQL.AppendLine(" [FTPdtBarCode3] [varchar](25) NULL,")
            oSQL.AppendLine(" [FTPdtStkCode] [varchar](20) NULL,")
            oSQL.AppendLine(" [FCPdtStkFac] [float] NULL,")
            oSQL.AppendLine(" [FTPdtStkControl] [varchar](1) NULL,")
            oSQL.AppendLine(" [FCPdtQtyRet] [float] NULL,")
            oSQL.AppendLine(" [FCPdtQtyWhs] [float] NULL,")
            oSQL.AppendLine(" [FCPdtQtyDRet] [float] NULL,")
            oSQL.AppendLine(" [FCPdtQtyDWhs] [float] NULL,")
            oSQL.AppendLine(" [FCPdtQtyMRet] [float] NULL,")
            oSQL.AppendLine(" [FCPdtQtyMWhs] [float] NULL,")
            oSQL.AppendLine(" [FCPdtQtyOrdBuy] [float] NULL,")
            oSQL.AppendLine(" [FCPdtCostAvg] [float] NULL,")
            oSQL.AppendLine(" [FCPdtCostFiFo] [float] NULL,")
            oSQL.AppendLine(" [FCPdtCostLast] [float] NULL,")
            oSQL.AppendLine(" [FCPdtCostDef] [float] NULL,")
            oSQL.AppendLine(" [FCPdtCostOth] [float] NULL,")
            oSQL.AppendLine(" [FCPdtCostAmt] [float] NULL,")
            oSQL.AppendLine(" [FCPdtCostStd] [float] NULL,")
            oSQL.AppendLine(" [FTPgpChain] [varchar](30) NULL,")
            oSQL.AppendLine(" [FTSplCode] [varchar](20) NULL,")
            oSQL.AppendLine(" [FTUsrCode] [varchar](20) NULL,")
            oSQL.AppendLine(" [FTPdtVatType] [varchar](1) NULL,")
            oSQL.AppendLine(" [FCPdtMin] [float] NULL,")
            oSQL.AppendLine(" [FCPdtMax] [float] NULL,")
            oSQL.AppendLine(" [FTPdtSaleType] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtSUnit] [varchar](5) NULL,")
            oSQL.AppendLine(" [FCPdtSFactor] [float] NULL,")
            oSQL.AppendLine(" [FTPdtMUnit] [varchar](5) NULL,")
            oSQL.AppendLine(" [FCPdtMFactor] [float] NULL,")
            oSQL.AppendLine(" [FTPdtLUnit] [varchar](5) NULL,")
            oSQL.AppendLine(" [FCPdtLFactor] [float] NULL,")
            oSQL.AppendLine(" [FTPdtGrade] [varchar](50) NULL,")
            oSQL.AppendLine(" [FCPdtWeight] [float] NULL,")
            oSQL.AppendLine(" [FTPszCode] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTClrCode] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTPtyCode] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTPdtPoint] [varchar](1) NULL,")
            oSQL.AppendLine(" [FCPdtPointTime] [float] NULL,")
            oSQL.AppendLine(" [FTPmhCodeS] [varchar](20) NULL,")
            oSQL.AppendLine(" [FTPmhTypeS] [varchar](2) NULL,")
            oSQL.AppendLine(" [FDPdtPmtSDateS] [DateTime] NULL,")
            oSQL.AppendLine(" [FDPdtPmtEDateS] [DateTime] NULL,")
            oSQL.AppendLine(" [FTPdtPmtTypeS] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPmhCodeM] [varchar](20) NULL,")
            oSQL.AppendLine(" [FTPmhTypeM] [varchar](2) NULL,")
            oSQL.AppendLine(" [FDPdtPmtSDateM] [DateTime] NULL,")
            oSQL.AppendLine(" [FDPdtPmtEDateM] [DateTime] NULL,")
            oSQL.AppendLine(" [FTPdtPmtTypeM] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPmhCodeL] [varchar](20) NULL,")
            oSQL.AppendLine(" [FTPmhTypeL] [varchar](2) NULL,")
            oSQL.AppendLine(" [FDPdtPmtSDateL] [DateTime] NULL,")
            oSQL.AppendLine(" [FDPdtPmtEDateL] [DateTime] NULL,")
            oSQL.AppendLine(" [FTPdtPmtTypeL] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtConType] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtSrn] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtPlcCodeS] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTPdtPlcCodeM] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTPdtPlcCodeL] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTPdtAlwOrderS] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtAlwOrderM] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtAlwOrderL] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtAlwBuyS] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtAlwBuyM] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtAlwBuyL] [varchar](1) NULL,")
            oSQL.AppendLine(" [FCPdtRetPriS1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriM1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriL1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriS2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriM2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriL2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriS3] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriM3] [float] NULL,")
            oSQL.AppendLine(" [FCPdtRetPriL3] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriS1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriM1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriL1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriS2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriM2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriL2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriS3] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriM3] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriL3] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriS4] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriM4] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriL4] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriS5] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriM5] [float] NULL,")
            oSQL.AppendLine(" [FCPdtWhsPriL5] [float] NULL,")
            oSQL.AppendLine(" [FTPdtWhsDefUnit] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtOthPurSplN1] [varchar](100) NULL,")
            oSQL.AppendLine(" [FTPdtOthPurSplN2] [varchar](100) NULL,")
            oSQL.AppendLine(" [FTPdtOthPurSplN3] [varchar](100) NULL,")
            oSQL.AppendLine(" [FCPdtOthPurSplP1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtOthPurSplP2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtOthPurSplP3] [float] NULL,")
            oSQL.AppendLine(" [FTPdtOthPurSplCmt] [varchar](150) NULL,")
            oSQL.AppendLine(" [FTPdtOthSleSplN1] [varchar](100) NULL,")
            oSQL.AppendLine(" [FTPdtOthSleSplN2] [varchar](100) NULL,")
            oSQL.AppendLine(" [FTPdtOthSleSplN3] [varchar](100) NULL,")
            oSQL.AppendLine(" [FCPdtOthSleSplP1] [float] NULL,")
            oSQL.AppendLine(" [FCPdtOthSleSplP2] [float] NULL,")
            oSQL.AppendLine(" [FCPdtOthSleSplP3] [float] NULL,")
            oSQL.AppendLine(" [FTPdtOthSleSplCmt] [varchar](150) NULL,")
            oSQL.AppendLine(" [FTAccCode] [varchar](20) NULL,")
            oSQL.AppendLine(" [FTPdtPic] [varchar](200) NULL,")
            oSQL.AppendLine(" [FTPdtSound] [varchar](200) NULL,")
            oSQL.AppendLine(" [FTPdtStaDel] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtType] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtBarByGen] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtStaSet] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtStaSetPri] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtStaSetShwDT] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtStaActive] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtRmk] [varchar](200) NULL,")
            oSQL.AppendLine(" [FTPbnCode] [varchar](5) NULL,")
            oSQL.AppendLine(" [FCPdtLeftPO] [float] NULL,")
            oSQL.AppendLine(" [FTPmoCode] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTPdtTax] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTDcsCode] [varchar](30) NULL,")
            oSQL.AppendLine(" [FTDepCode] [varchar](10) NULL,")
            oSQL.AppendLine(" [FTClsCode] [varchar](10) NULL,")
            oSQL.AppendLine(" [FTSclCode] [varchar](10) NULL,")
            oSQL.AppendLine(" [FCPdtQtyNow] [float] NULL,")
            oSQL.AppendLine(" [FTPdtArticle] [varchar](50) NULL,")
            oSQL.AppendLine(" [FTPdtGrpControl] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtShwTch] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtNoDis] [varchar](1) NULL,")
            oSQL.AppendLine(" [FCPdtVatRate] [float] NULL,")
            oSQL.AppendLine(" [FTTcgCode] [varchar](3) NULL,")
            oSQL.AppendLine(" [FTPdtNameShort] [varchar](50) NULL,")
            oSQL.AppendLine(" [FTPdtNameShortEng] [varchar](50) NULL,")
            oSQL.AppendLine(" [FTPdtStaAlwBuy] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTFixGonCode] [varchar](3) NULL,")
            oSQL.AppendLine(" [FCPdtLifeCycle] [float] NULL,")
            oSQL.AppendLine(" [FTPdtOrdDay] [varchar](50) NULL,")
            oSQL.AppendLine(" [FTPdtOrdSun] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtOrdMon] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtOrdTue] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtOrdWed] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtOrdThu] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtOrdFri] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtOrdSat] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtDim] [varchar](50) NULL,")
            oSQL.AppendLine(" [FTPdtPkgDim] [varchar](50) NULL,")
            oSQL.AppendLine(" [FCPdtLeadTime] [float] NULL,")
            oSQL.AppendLine(" [FDPdtOrdStart] [DateTime] NULL,")
            oSQL.AppendLine(" [FDPdtOrdStop] [DateTime] NULL,")
            oSQL.AppendLine(" [FDPdtSaleStart] [DateTime] NULL,")
            oSQL.AppendLine(" [FDPdtSaleStop] [DateTime] NULL,")
            oSQL.AppendLine(" [FTPdtStaTouch] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtStaAlwRepack] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtGateWhe] [varchar](5) NULL,")
            oSQL.AppendLine(" [FNPdtPalletSize] [bigint] NULL,")
            oSQL.AppendLine(" [FNPdtPalletLev] [bigint] NULL,")
            oSQL.AppendLine(" [FNPdtAge] [bigint] NULL,")
            oSQL.AppendLine(" [FNPdtAgeB4Rcv] [bigint] NULL,")
            oSQL.AppendLine(" [FNPdtAgeB4Snd] [bigint] NULL,")
            oSQL.AppendLine(" [FTItyCode] [varchar](3) NULL,")
            oSQL.AppendLine(" [FTPdtDeliveryBy] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTMcrCode] [varchar](5) NULL,")
            oSQL.AppendLine(" [FTSlcItemCode] [varchar](30) NULL,")
            oSQL.AppendLine(" [FTPdtPic4Slip] [varchar](200) NULL,")
            oSQL.AppendLine(" [FDPdtMfg] [DateTime] NULL,")
            oSQL.AppendLine(" [FDPdtExp] [DateTime] NULL,")
            oSQL.AppendLine(" [FTPdtUnitSymbol] [varchar](10) NULL,")
            oSQL.AppendLine(" [FTPdtLabSizeS] [varchar](50) NULL,")
            oSQL.AppendLine(" [FNPdtLabQtyS] [float] NULL,")
            oSQL.AppendLine(" [FTPdtLabSizeM] [varchar](50) NULL,")
            oSQL.AppendLine(" [FNPdtLabQtyM] [float] NULL,")
            oSQL.AppendLine(" [FTPdtLabSizeL] [varchar](50) NULL,")
            oSQL.AppendLine(" [FNPdtLabQtyL] [float] NULL,")
            oSQL.AppendLine(" [FTPdtAlwPickS] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtAlwPickM] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtAlwPickL] [varchar](1) NULL,")
            oSQL.AppendLine(" [FTPdtStaFastF] [varchar](1) NULL,")
            oSQL.AppendLine(" [FNPdtOthSystem] [bigint] NULL,")
            oSQL.AppendLine(" [FDDateUpd] [DateTime] NULL,")
            oSQL.AppendLine(" [FTTimeUpd] [varchar](8) NULL,")
            oSQL.AppendLine(" [FTWhoUpd] [varchar](50) NULL,")
            oSQL.AppendLine(" [FDDateIns] [DateTime] NULL,")
            oSQL.AppendLine(" [FTTimeIns] [varchar](8) NULL,")
            oSQL.AppendLine(" [FTWhoIns] [varchar](50) NULL,")
            oSQL.AppendLine(" [FNLnkLine] [bigint] Not NULL,")
            oSQL.AppendLine(" [FNLnkStatus] [bigint] NULL,")
            oSQL.AppendLine(" [FTLnkLog] [varchar](255) NULL,")
            oSQL.AppendLine(" [FNLnkPrc] [bigint] NULL,")
            oSQL.AppendLine(" )")
            oSQL.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))

            ' Check Temp Ajp  HD
            oSQL.Clear()
            oSQL.AppendLine("If Not EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'TLNKPdtAjpHD') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TLNKPdtAjpHD](")
            oSQL.AppendLine("[FTIphDocNo] [varchar](20) Not NULL,")
            oSQL.AppendLine("[FTIphDocType] [varchar](1) NULL,")
            oSQL.AppendLine("[FDIphDocDate] [DateTime] NULL,")
            oSQL.AppendLine("[FTIphDocTime] [varchar](8) NULL,")
            oSQL.AppendLine("[FTBchCode] [varchar](3) NULL,")
            oSQL.AppendLine("[FTDptCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTUsrCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTIphApvCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FCIphTotal] [float] NULL,")
            oSQL.AppendLine("[FTIphStaDoc] [varchar](1) NULL,")
            oSQL.AppendLine("[FTIphStaPrcDoc] [varchar](1) NULL,")
            oSQL.AppendLine("[FNIphStaDocAct] [Int] NULL,")
            oSQL.AppendLine("[FTIphRmk] [varchar](200) NULL,")
            oSQL.AppendLine("[FTSplCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTCstCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTIphAdjType] [varchar](1) NULL,")
            oSQL.AppendLine("[FDIphAffect] [DateTime] NULL,")
            oSQL.AppendLine("[FTIphStaPrcPri] [varchar](1) NULL,")
            oSQL.AppendLine("[FTIphBchTo] [varchar](5) NULL,")
            oSQL.AppendLine("[FTIphZneTo] [varchar](30) NULL,")
            oSQL.AppendLine("[FTIphPriType] [varchar](1) NULL,")
            oSQL.AppendLine("[FDIphDStop] [DateTime] NULL,")
            oSQL.AppendLine("[FDDateUpd] [DateTime] NULL,")
            oSQL.AppendLine("[FTTimeUpd] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoUpd] [varchar](50) NULL,")
            oSQL.AppendLine("[FDDateIns] [DateTime] NULL,")
            oSQL.AppendLine("[FTTimeIns] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoIns] [varchar](50) NULL,")
            oSQL.AppendLine(")")
            oSQL.AppendLine("End")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))

            ' Check Temp Ajp DT
            oSQL.Clear()
            oSQL.AppendLine("If Not EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'TLNKPdtAjpDT') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TLNKPdtAjpDT](")
            oSQL.AppendLine("[FTIphDocNo] [varchar](20) Not NULL,")
            oSQL.AppendLine("[FNIpdSeqNo] [bigint] Not NULL,")
            oSQL.AppendLine("[FTPdtCode] [varchar](20) Not NULL,")
            oSQL.AppendLine("[FTPdtName] [varchar](100) NULL,")
            oSQL.AppendLine("[FTIphDocType] [varchar](1) NULL,")
            oSQL.AppendLine("[FDIphDocDate] [DateTime] NULL,")
            oSQL.AppendLine("[FTBchCode] [varchar](3) NULL,")
            oSQL.AppendLine("[FTIpdBarCode] [varchar](25) NULL,")
            oSQL.AppendLine("[FTIpdStkCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FCIpdStkFac] [float] NULL,")
            oSQL.AppendLine("[FTPgpChain] [varchar](30) NULL,")
            oSQL.AppendLine("[FTPunCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTIpdUnitName] [varchar](50) NULL,")
            oSQL.AppendLine("[FCIpdFactor] [float] NULL,")
            oSQL.AppendLine("[FNIpdAdjLev] [bigint] NULL,")
            oSQL.AppendLine("[FCIpdPriOld] [float] NULL,")
            oSQL.AppendLine("[FCIpdPriNew] [float] NULL,")
            oSQL.AppendLine("[FNIpdUnitType] [bigint] NULL,")
            oSQL.AppendLine("[FTIpdStaPrc] [varchar](1) NULL,")
            oSQL.AppendLine("[FCIpdCost] [float] NULL,")
            oSQL.AppendLine("[FTPdtArticle] [varchar](50) NULL,")
            oSQL.AppendLine("[FTDcsCode] [varchar](30) NULL,")
            oSQL.AppendLine("[FTPszCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTClrCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPszName] [varchar](50) NULL,")
            oSQL.AppendLine("[FTClrName] [varchar](50) NULL,")
            oSQL.AppendLine("[FTPdtNoDis] [varchar](1) NULL,")
            oSQL.AppendLine("[FCIpdDisAvg] [float] NULL,")
            oSQL.AppendLine("[FCIpdFootAvg] [float] NULL,")
            oSQL.AppendLine("[FCIpdRePackAvg] [float] NULL,")
            oSQL.AppendLine("[FTIpdBchTo] [varchar](5) NULL,")
            oSQL.AppendLine("[FTIpdStaPrcStkCrd] [varchar](1) NULL,")
            oSQL.AppendLine("[FNLnkLine] [bigint] Not NULL,")
            oSQL.AppendLine("[FDDateUpd] [DateTime] NULL,")
            oSQL.AppendLine("[FTTimeUpd] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoUpd] [varchar](50) NULL,")
            oSQL.AppendLine("[FDDateIns] [DateTime] NULL,")
            oSQL.AppendLine("[FTTimeIns] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoIns] [varchar](50) NULL,")
            oSQL.AppendLine(")")
            oSQL.AppendLine("End")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))

            ' Check Temp Tnf HD
            oSQL.Clear()
            oSQL.AppendLine("If Not EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'TLNKPdtTnfHD') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TLNKPdtTnfHD](")
            oSQL.AppendLine("[FTBchCode] [varchar](3) Not NULL,")
            oSQL.AppendLine("[FTPthDocNo] [varchar](20) Not NULL,")
            oSQL.AppendLine("[FTPthDocType] [varchar](2) NULL,")
            oSQL.AppendLine("[FDPthDocDate] [DateTime] NULL,")
            oSQL.AppendLine("[FTPthDocTime] [varchar](8) NULL,")
            oSQL.AppendLine("[FTPthVATInOrEx] [varchar](1) NULL,")
            oSQL.AppendLine("[FTDptCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTDepName] [varchar](100) NULL,")
            oSQL.AppendLine("[FTUsrCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTUsrName] [varchar](50) NULL,")
            oSQL.AppendLine("[FTSplCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTBchCodeTnf] [varchar](3) NULL,")
            oSQL.AppendLine("[FTPthWhFrm] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthWhTo] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthType] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthOther] [varchar](100) NULL,")
            oSQL.AppendLine("[FTCstCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTAreCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTSpnCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTPrdCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTWahCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthApvCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTShpCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FNCspCode] [bigint] NULL,")
            oSQL.AppendLine("[FNPthCrTerm] [bigint] NULL,")
            oSQL.AppendLine("[FDPthDueDate] [DateTime] NULL,")
            oSQL.AppendLine("[FTPthRefExt] [varchar](20) NULL,")
            oSQL.AppendLine("[FDPthRefExtDate] [DateTime] NULL,")
            oSQL.AppendLine("[FTPthRefInt] [varchar](20) NULL,")
            oSQL.AppendLine("[FDPthRefIntDate] [DateTime] NULL,")
            oSQL.AppendLine("[FTPthRefAE] [varchar](20) NULL,")
            oSQL.AppendLine("[FDPthTnfDate] [DateTime] NULL,")
            oSQL.AppendLine("[FDPthBillDue] [DateTime] NULL,")
            oSQL.AppendLine("[FTPthCtrName] [varchar](50) NULL,")
            oSQL.AppendLine("[FNPthDocPrint] [bigint] NULL,")
            oSQL.AppendLine("[FCPthVATRate] [float] NULL,")
            oSQL.AppendLine("[FCPthTotal] [float] NULL,")
            oSQL.AppendLine("[FCPthNonVat] [float] NULL,")
            oSQL.AppendLine("[FCPthB4DisChg] [float] NULL,")
            oSQL.AppendLine("[FTPthDisChgTxt] [varchar](50) NULL,")
            oSQL.AppendLine("[FCPthDis] [float] NULL,")
            oSQL.AppendLine("[FCPthChg] [float] NULL,")
            oSQL.AppendLine("[FCPthAftDisChg] [float] NULL,")
            oSQL.AppendLine("[FCPthVat] [float] NULL,")
            oSQL.AppendLine("[FCPthVatable] [float] NULL,")
            oSQL.AppendLine("[FCPthGrand] [float] NULL,")
            oSQL.AppendLine("[FCPthRnd] [float] NULL,")
            oSQL.AppendLine("[FCPthWpTax] [float] NULL,")
            oSQL.AppendLine("[FCPthReceive] [float] NULL,")
            oSQL.AppendLine("[FCPthChn] [float] NULL,")
            oSQL.AppendLine("[FTPthGndText] [varchar](200) NULL,")
            oSQL.AppendLine("[FCPthLeft] [float] NULL,")
            oSQL.AppendLine("[FCPthMnyCsh] [float] NULL,")
            oSQL.AppendLine("[FCPthMnyChq] [float] NULL,")
            oSQL.AppendLine("[FCPthMnyCrd] [float] NULL,")
            oSQL.AppendLine("[FCPthMnyCtf] [float] NULL,")
            oSQL.AppendLine("[FCPthMnyCpn] [float] NULL,")
            oSQL.AppendLine("[FCPthMnyCls] [float] NULL,")
            oSQL.AppendLine("[FCPthMnyCxx] [float] NULL,")
            oSQL.AppendLine("[FCPthGndCN] [float] NULL,")
            oSQL.AppendLine("[FCPthGndDN] [float] NULL,")
            oSQL.AppendLine("[FCPthGndAE] [float] NULL,")
            oSQL.AppendLine("[FCPthGndTH] [float] NULL,")
            oSQL.AppendLine("[FTPthStaPaid] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaRefund] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaType] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaDoc] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaPrcDoc] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaPrcSpn] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaPrcCst] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaPrcGL] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthStaPost] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPjcCode] [varchar](10) NULL,")
            oSQL.AppendLine("[FTAloCode] [varchar](10) NULL,")
            oSQL.AppendLine("[FTCcyCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FCPthCcyExg] [float] NULL,")
            oSQL.AppendLine("[FTPosCode] [varchar](3) NULL,")
            oSQL.AppendLine("[FTPthPosCN] [varchar](20) NULL,")
            oSQL.AppendLine("[FTLogCode] [varchar](15) NULL,")
            oSQL.AppendLine("[FTPthRmk] [varchar](200) NULL,")
            oSQL.AppendLine("[FNPthSign] [bigint] NULL,")
            oSQL.AppendLine("[FTPthCshOrCrd] [varchar](1) NULL,")
            oSQL.AppendLine("[FCPthPaid] [float] NULL,")
            oSQL.AppendLine("[FTPthDstPaid] [varchar](1) NULL,")
            oSQL.AppendLine("[FTXbhDocNo] [varchar](20) NULL,")
            oSQL.AppendLine("[FTXphDocNo] [varchar](20) NULL,")
            oSQL.AppendLine("[FNPthStaDocAct] [Int] NULL,")
            oSQL.AppendLine("[FNPthStaRef] [Int] NULL,")
            oSQL.AppendLine("[FTPthDptCRef] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthDptNRef] [varchar](100) NULL,")
            oSQL.AppendLine("[FTPthStaVatSend] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthBchFrm] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthBchTo] [varchar](5) NULL,")
            oSQL.AppendLine("[FTRteCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FCPthRteFac] [float] NULL,")
            oSQL.AppendLine("[FTTrnCode] [varchar](3) NULL,")
            oSQL.AppendLine("[FDDateUpd] [DateTime] NULL,")
            oSQL.AppendLine("[FTTimeUpd] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoUpd] [varchar](50) NULL,")
            oSQL.AppendLine("[FDDateIns] [DateTime] NULL,")
            oSQL.AppendLine("[FTTimeIns] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoIns] [varchar](50) NULL")
            oSQL.AppendLine(")")
            oSQL.AppendLine("End")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))

            ' Check Temp Tnf DT
            oSQL.Clear()
            oSQL.AppendLine("")
            oSQL.AppendLine("If Not EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'TLNKPdtTnfDT') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TLNKPdtTnfDT](")
            oSQL.AppendLine("[FTBchCode] [varchar](3) Not NULL,")
            oSQL.AppendLine("[FTPthDocNo] [varchar](20) Not NULL,")
            oSQL.AppendLine("[FNPtdSeqNo] [bigint] Not NULL,")
            oSQL.AppendLine("[FTPdtCode] [varchar](20) Not NULL,")
            oSQL.AppendLine("[FTPdtName] [varchar](100) NULL,")
            oSQL.AppendLine("[FTPthDocType] [varchar](2) NULL,")
            oSQL.AppendLine("[FDPthDocDate] [DateTime] NULL,")
            oSQL.AppendLine("[FTPthVATInOrEx] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPtdBarCode] [varchar](25) NULL,")
            oSQL.AppendLine("[FTPtdStkCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FCPtdStkFac] [float] NULL,")
            oSQL.AppendLine("[FTPtdVatType] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPtdSaleType] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPgpChain] [varchar](30) NULL,")
            oSQL.AppendLine("[FTSrnCode] [varchar](50) NULL,")
            oSQL.AppendLine("[FTSrnStaNow] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPmhCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FTPmhType] [varchar](2) NULL,")
            oSQL.AppendLine("[FTPunCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPtdUnitName] [varchar](50) NULL,")
            oSQL.AppendLine("[FCPtdFactor] [float] NULL,")
            oSQL.AppendLine("[FCPtdSalePrice] [float] NULL,")
            oSQL.AppendLine("[FCPtdQty] [float] NULL,")
            oSQL.AppendLine("[FCPtdSetPrice] [float] NULL,")
            oSQL.AppendLine("[FCPtdB4DisChg] [float] NULL,")
            oSQL.AppendLine("[FTPtdDisChgTxt] [varchar](50) NULL,")
            oSQL.AppendLine("[FCPtdDis] [float] NULL,")
            oSQL.AppendLine("[FCPtdChg] [float] NULL,")
            oSQL.AppendLine("[FCPtdNet] [float] NULL,")
            oSQL.AppendLine("[FCPtdVat] [float] NULL,")
            oSQL.AppendLine("[FCPtdVatable] [float] NULL,")
            oSQL.AppendLine("[FCPtdQtyAll] [float] NULL,")
            oSQL.AppendLine("[FCPtdCost] [float] NULL,")
            oSQL.AppendLine("[FCPtdCostIn] [float] NULL,")
            oSQL.AppendLine("[FCPtdCostEx] [float] NULL,")
            oSQL.AppendLine("[FCPtdPrice] [float] NULL,")
            oSQL.AppendLine("[FTPtdStaPdt] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPtdStaRfd] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPtdStaPrcStk] [varchar](1) NULL,")
            oSQL.AppendLine("[FNPthSign] [bigint] NULL,")
            oSQL.AppendLine("[FTAccCode] [varchar](20) NULL,")
            oSQL.AppendLine("[FNPtdPdtLevel] [bigint] NULL,")
            oSQL.AppendLine("[FTPtdPdtParent] [varchar](20) NULL,")
            oSQL.AppendLine("[FTPtdApOrAr] [varchar](20) NULL,")
            oSQL.AppendLine("[FTWahCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FNPtdStaRef] [Int] NULL,")
            oSQL.AppendLine("[FCPtdQtySet] [float] NULL,")
            oSQL.AppendLine("[FTPdtStaSet] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthWhFrm] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthWhTo] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthStaVatSend] [varchar](1) NULL,")
            oSQL.AppendLine("[FTPthBchFrm] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPthBchTo] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPdtArticle] [varchar](50) NULL,")
            oSQL.AppendLine("[FTDcsCode] [varchar](30) NULL,")
            oSQL.AppendLine("[FTPszCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTClrCode] [varchar](5) NULL,")
            oSQL.AppendLine("[FTPszName] [varchar](50) NULL,")
            oSQL.AppendLine("[FTClrName] [varchar](50) NULL,")
            oSQL.AppendLine("[FTPdtNoDis] [varchar](1) NULL,")
            oSQL.AppendLine("[FCPtdDisAvg] [float] NULL,")
            oSQL.AppendLine("[FCPtdFootAvg] [float] NULL,")
            oSQL.AppendLine("[FCPtdRePackAvg] [float] NULL,")
            oSQL.AppendLine("[FCPtdSetPriceAvg] [float] NULL,")
            oSQL.AppendLine("[FTPtdStaPrcStkCrd] [varchar](1) NULL,")
            oSQL.AppendLine("[FCPtdSetPriceRte] [float] NULL,")
            oSQL.AppendLine("[FTPtdDisChgTxtRte] [varchar](50) NULL,")
            oSQL.AppendLine("[FCPtdDisRte] [float] NULL, ")
            oSQL.AppendLine("[FDDateUpd] [DateTime] NULL, ")
            oSQL.AppendLine("[FTTimeUpd] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoUpd] [varchar](50) NULL,")
            oSQL.AppendLine("[FDDateIns] [DateTime] NULL,")
            oSQL.AppendLine("[FTTimeIns] [varchar](8) NULL,")
            oSQL.AppendLine("[FTWhoIns] [varchar](50) NULL,")
            oSQL.AppendLine(")")
            oSQL.AppendLine("End")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
        Catch ex As Exception
            cCNSP.SP_MSGnShowing(ex.Message, cCNEN.eEN_MSGStyle.nEN_MSGErr)
        Finally
            oSQL = Nothing
            oDatabase = Nothing
        End Try
    End Sub

    Private Shared Sub C_CALxBackupFile(ByVal ptFileCopyBk As String, ptDate As String)
        '-------------------------------------------------
        Dim strFileSize As String = ""
        Dim di As New IO.DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory() & "Inbox")
        Dim oAryFi As IO.FileInfo() = di.GetFiles("*.txt")
        Dim oFile As IO.FileInfo
        Dim tName() As String
        Dim tFullname As String

        Try
            For Each oFile In oAryFi
                tName = oFile.Name.Split("_")
                tFullname = oFile.FullName
                If tName(0) & tName(1) = "MMSCTL" Then
                    Dim tFileName As String = ""
                    'Create Dir '*CH 20-10-2015
                    If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & ptDate) Then
                        IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & ptDate)
                    End If

                    If File.Exists(tFullname) Then
                        'Dim nIndex As Integer = 0
                        tFileName = Path.GetFileName(tFullname)
                        FileCopy(tFullname, AdaConfig.cConfig.oConfigXml.tBackup & "\" & ptDate & "\" & tFileName)
                        File.Delete(tFullname)
                    End If
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub

    'Sub Class Import
    Class cItem
        Property nSelect As Integer = 1
        Property tCode As String = ""
        Property tItem As String = ""
        Property tItemThai As String = ""
        Property nCount As Integer = 0
        Property tSelectCount As String = ""
    End Class

    Public Shared Sub C_CALxClear()
        bC_Status = False
        oC_Items = Nothing
        oC_TCNMPdt = Nothing
    End Sub

End Class