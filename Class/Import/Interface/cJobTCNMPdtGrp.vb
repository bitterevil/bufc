﻿Imports System.Data.SqlClient
Imports System.IO
Public Class cJobTCNMPdtGrp
    Inherits cBaseImport

    Property tFullPath As String = ""
    Property oC_TCNMPdtGrp As New cTCNMPdtGrp
    Property tC_Table As String = "TCNMPdtGrp"
    Property tC_TableTemp As String = "TLNKPdtGrp"

    Private tSpr As String = AdaConfig.cConfig.tSeparator
    Private tC_FullPath As String = ""
    Private tC_Name As String = ""
    Public tC_FileLog As String = ""
    Public tC_PathList As String = ""
    Public Shared aC_GrpList As List(Of cTCNMPdtGrpList)

    Public Overrides Sub C_GETxFile()
        aC_Detail.Clear()
        aC_DetailSub.Clear()
        Dim tFullName As String
        Dim aFullName As String()
        Dim oDirInfoInbox As New DirectoryInfo(AdaConfig.cConfig.oConfigXml.tInbox)
        Dim oFiles As FileInfo() = oDirInfoInbox.GetFiles("*.xml")
        Dim oSortFiles = oFiles.OrderBy(Function(o) o.Name)
        For Each oItem In oSortFiles 'oFiles
            tFullName = Path.GetFileName(oItem.FullName)
            'aFullName = tFullName.Split("_")
            aFullName = tFullName.Split("-") 'Change Fomat file '*CH 02-12-2017

            If aFullName(0).ToUpper = "MCH" AndAlso aFullName.Count >= 3 Then
                'If InStr(Path.GetFileName(oItem.FullName).ToLower, "TCNMPdtBrand".ToLower) = 5 Then
                Dim oFileDetail As New cBaseImport.cImportDetail
                oFileDetail.tC_Name = oItem.Name.ToUpper
                oFileDetail.tC_FullName = oItem.FullName
                Dim nMaxLen As Integer = oFileDetail.tC_Name.Length
                If Not nMaxLen > 19 Then
                    Continue For
                End If

                Dim tDateTime As String = ""
                tDateTime = SP_CNSETtStrConvertDateTime(aFullName(1) & aFullName(2))
                If IsDate(tDateTime) = True Then
                    oFileDetail.tC_Date = tDateTime
                    oFileDetail.nC_Status = 0
                Else
                    Continue For
                End If

                oFileDetail.nC_Length = oItem.Length
                oFileDetail.tC_Length = oItem.Length.SP_CNSETtFormatFileSize
                aC_Detail.Add(oFileDetail)
                aC_DetailSub.Add(oFileDetail)
            End If
        Next

        oFiles = Nothing
        oDirInfoInbox = Nothing
    End Sub

    Public Overrides Sub C_CALxProcess()
        Dim tFileBak As String = ""
        Dim nFile As Integer = 1
        Dim nLastLang As Integer = 0 '1:thai, 2:Eng
        Dim tC_GrpList As String = ""
        Dim tC_GrpFile As String = ""
        Dim tFullPath As String = ""
        Dim tFullName As String = ""
        Dim oPrepareFile As New cPrepareFile
        Dim tMsgV As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLastLang = 1
        Dim oDatabase As New cDatabaseLocal
        Try
            'Dim tDocDate As String = ""
            'Cal Progress
            Dim cNextVal As Double = 0
            Dim cProgress As Double = 0
            cNextVal = 100 / aC_Detail.Count

            'For Each oGrpList In aC_DetailSub
            'tDocDate = Format(Now, "yyyy/MM/dd HH:mm:ss")

            'aC_GrpList = New List(Of cTCNMPdtGrpList)
            '    'เตรียมข้อมูล
            '    oPrepareFile.tFullPath = oGrpList.tC_FullName
            '    tFullPath = oPrepareFile.tFullPath
            '    oPrepareFile.C_CALxPreparePdtGrpTmp()
            '    oPrepareFile.tFullName = oGrpList.tC_Name
            '    tFullName = oPrepareFile.tFullName
            '    'Dim aItemColList As String() = tFullName.Split("_")
            '    Dim aItemColList As String() = tFullName.Split("-")
            '    tC_GrpList = aItemColList(2).Replace(".XML", "")

            '    oC_TCNMPdtGrp.C_CALbPrcGrpList(aC_GrpList)
            '    cCNVB.oDBGrp = C_GEToGrpMapping(cCNVB.oDBGrp)

            For Each oItem In aC_Detail.Where(Function(c) c.nC_Select = 1 And c.nC_Status = 0)

                aC_GrpList = New List(Of cTCNMPdtGrpList)
                'เตรียมข้อมูล
                oPrepareFile.tFullPath = oItem.tC_FullName
                tFullPath = oPrepareFile.tFullPath
                ' oPrepareFile.C_CALxPreparePdtGrpTmp()
                oPrepareFile.tFullName = oItem.tC_Name
                tFullName = oPrepareFile.tFullName
                'Dim aItemColList As String() = tFullName.Split("_")
                Dim aItemColList As String() = tFullName.Split("-")
                tC_GrpList = aItemColList(2).Replace(".XML", "")

                oC_TCNMPdtGrp.C_CALbPrcGrpList(aC_GrpList)
                cCNVB.oDBGrp = C_GEToGrpMapping(cCNVB.oDBGrp)


                '*CH 20-12-2014
                cImportTemplate.tC_File = "" '" [" & nFile & "]"
                If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                tC_FileLog = String.Format(tC_Log, Path.GetFileName(oItem.tC_FullName)).Replace(".xml", ".txt")
                'C_CALxWriteLogErr("TimePrepArtList :: " & DateDiff(DateInterval.Minute, dTPrepArt, Now) & " Minute", tC_FileLog)

                With oC_TCNMPdtGrp
                    .tFileLog = tC_FileLog
                    .tTableTemp = tC_TableTemp
                    .tTable = tC_Table
                    .tFullPath = oItem.tC_FullName
                    .tDateNow = tC_DateNow
                    tC_FullPath = oItem.tC_FullName
                    tC_Name = oItem.tC_Name
                    'Dim aItemColFile As String() = tC_Name.Split("_")
                    'Dim tName As String = aItemColFile(0) & "_" & aItemColFile(1) & "_" & aItemColFile(2) & ".xml"
                    Dim aItemColFile As String() = tC_Name.Split("-")
                    'Dim tName As String = aItemColFile(0) & "-" & aItemColFile(1) & "-" & aItemColFile(2)
                    'If aItemColFile.Count > 3 Then tName &= aItemColFile(3)
                    'tName &= ".xml"

                    tC_GrpFile = aItemColFile(2).Replace(".XML", "")

                    If tC_GrpList.ToLower = tC_GrpFile.ToLower Then
                        'Grid row
                        nC_GridRow = nFile

                        'เตรียมไฟล์
                        cImportTemplate.tC_File = " เตรียมข้อมูล;Prepare data".Split(";")(nLastLang) '" [" & nFile & "] " & "เตรียมข้อมูล;Prepare data".Split(";")(nLastLang)
                        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)

                        oPrepareFile.tFullPath = oItem.tC_FullName
                        '    oPrepareFile.C_CALxPreparePdtGrp()
                        'C_CLRxDeletePdtGrp(tName)'*** OLD PAN 20170418 'ไม่ Delete เปลี่ยนเป็น Update หรือ Insert 
                        C_CLRxUpdatePdtGrp("")

                        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(100)

                        'Check Article File 0 KB '*CH 2015-01-04
                        Dim oRdr = New StreamReader(oItem.tC_FullName, System.Text.Encoding.Default)
                        Dim bNotFile0Kb As Boolean = True
                        While True
                            If oRdr.ReadLine Is Nothing Then
                                bNotFile0Kb = False
                                Exit While
                            End If

                            Exit While
                        End While
                        oRdr.Dispose()
                        oRdr.Close()
                        oRdr = Nothing

                        If bNotFile0Kb Then
                            'Insert Product

                            .tFileLog = tC_FileLog
                            'dTPrepArt = Now
                            If .C_CALbProcess() Then 'Import สำเร็จ
                                oItem.nC_Status = 1
                                aC_Detail(nFile - 1).nC_Status = 1
                                nC_StaUpload = 1
                            Else 'Import ไม่สำเร็จ
                                oItem.nC_Status = 2
                                aC_Detail(nFile - 1).nC_Status = 2
                                nC_StaUpload = 2
                            End If
                            'C_CALxWriteLogErr("TimeInsert:: " & DateDiff(DateInterval.Minute, dTPrepArt, Now) & " Minute", tC_FileLog)
                        Else
                            Dim tMsg As String = "ไม่พบข้อมูลสินค้า;Not have data article.".Split(";")(nLastLang)
                            C_CALxWriteLogErr(tMsg, oItem.tC_FullName)
                            oItem.nC_Status = 2
                            aC_Detail(nFile - 1).nC_Status = 2
                            nC_StaUpload = 2
                        End If

                        C_CALxBackupFile(oItem.tC_FullName)
                    Else
                        Continue For
                    End If
                End With
                nFile += 1
            Next
            'C_CALxBackupFile(oGrpList.tC_FullName)
            '    C_CALxBackupFile(tFileBak)
            'Next

            'End If

        Catch ex As Exception
            C_CALxWriteLogErr(ex.Message, cCNVB.tVB_Log)
        Finally
            oPrepareFile = Nothing
            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(100)
        End Try
    End Sub

    Public Sub C_CALxWriteLogErr(ByVal ptMsg As String, ByVal ptLog As String, Optional ByVal ptFileArticle As String = "")
        Try
            Dim tFileArticle As String = ""
            Dim oFileLog As New cImportLogHis(ptLog)
            Dim oNewLog As New cImportLogHis.cLogHis
            With oNewLog
                .tErrMsg = ptMsg
            End With
            oFileLog.C_CALxAddNew(oNewLog)
            oFileLog.C_CALbSaveLogErr()
            Dim tDesc As String = "Insert(0),Update(0),Failed(1)"
            Dim oImp As New cImport
            tFileArticle = IIf(ptFileArticle <> "", ptFileArticle, tC_FullPath)
            oImp.C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFileArticle), tDesc, ptLog))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub C_CALxBackupFile(ByVal ptFileCopyBk As String)
        Try
            Dim tFileName As String = ""
            'Create Dir '*CH 20-10-2015
            If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import") Then
                IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import")
            End If
            Try
                If File.Exists(ptFileCopyBk) Then
                    'Dim nIndex As Integer = 0
                    tFileName = Path.GetFileName(ptFileCopyBk)
                    FileCopy(ptFileCopyBk, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import\" & tFileName)
                    File.Delete(ptFileCopyBk)
                End If
                'nIndex += 1
            Catch ex As Exception
            End Try
        Catch ex As Exception
        End Try
    End Sub

    Private Sub C_CLRxUpdatePdtGrp(ByVal ptFile As String)
        Dim oDatabase As New cDatabaseLocal
        Dim tDocDate As String = Format(Now, "yyyy/MM/dd")
        Dim oSql As System.Text.StringBuilder = Nothing

        Try
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("UPDATE Lnk SET")
            oSql.AppendLine("	Lnk.FNLnkStatus = 2")
            oSql.AppendLine("FROM TCNMPdtGrp Pgp INNER JOIN TLNKPdtGrp Lnk ON Pgp.FTPgpChain =  Lnk.FTPgpChain")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            'If oDBTbl.Rows.Count > 0 Then
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("UPDATE Pgp SET")
            oSql.AppendLine("   Pgp.FTPgpCode = Lnk.FTPgpCode,")
            oSql.AppendLine("   Pgp.FTPgpName = Lnk.FTPgpName,")
            oSql.AppendLine("   Pgp.FTPgpChainName = Lnk.FTPgpChainName,")
            oSql.AppendLine("   Pgp.FTPgpParent = Lnk.FTPgpParent,")
            oSql.AppendLine("   Pgp.FNPgpLevel = Lnk.FNPgpLevel,")
            oSql.AppendLine("   Pgp.FTPgpChain = Lnk.FTPgpChain,")
            oSql.AppendLine("   Pgp.FTPgpRmk = Lnk.FTPgpRmk,")
            oSql.AppendLine("   Pgp.FDDateUpd = Convert(Varchar(10), GetDate(), 121),")
            oSql.AppendLine("   Pgp.FTTimeUpd = Convert(Varchar(10), GetDate(), 108),")
            oSql.AppendLine("   Pgp.FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
            oSql.AppendLine("FROM TCNMPdtGrp Pgp INNER JOIN TLNKPdtGrp Lnk ON Pgp.FTPgpChain = Lnk.FTPgpChain")
            oSql.AppendLine("WHERE Lnk.FNLnkStatus = '2'")
            If oDatabase.C_CALnExecuteNonQuery(oSql.ToString) > 0 Then
                oSql = New System.Text.StringBuilder
                oSql.Append("UPDATE TLNKPdtGrp SET FNLnkPrc = 1,FTLnkLog='" & cCNVB.tVB_SuccessImp & "' WHERE FNLnkStatus = 2")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
            End If
            'oDBTbl.Clear()
            'End If

            'oSql = New System.Text.StringBuilder
            'oSql.AppendLine("INSERT INTO TCNMPdtGrp (")
            'oSql.AppendLine("FTPgpCode,FTPgpName,FTPgpChainName,")
            'oSql.AppendLine("FTPgpParent,FNPgpLevel,FTPgpChain,FTPgpRmk,")
            'oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd")
            'oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns")
            'oSql.AppendLine(")")
            'oSql.AppendLine("SELECT FTPgpCode,FTPgpName,FTPgpChainName,")
            'oSql.AppendLine("FTPgpParent,FNPgpLevel,FTPgpChain,FTPgpRmk,")
            'oSql.AppendLine("Convert(Varchar(10), GetDate(), 121),Convert(Varchar(10), GetDate(), 108),'" & cCNVB.tVB_UserName & "',")
            'oSql.AppendLine("Convert(Varchar(10), GetDate(), 121),Convert(Varchar(10), GetDate(), 108),'" & cCNVB.tVB_UserName & "'")
            'oSql.AppendLine("FROM TLNKPdtGrp")
            'oSql.AppendLine("WHERE FNLnkStatus = '1'")
            'oDatabase.C_CALoExecuteReader(oSql.ToString)
        Catch ex As Exception
            ' C_CALxWriteLogErr(ex.ToString, tC_FullName)
        Finally
            oSql = Nothing
            oDatabase = Nothing
        End Try
    End Sub

    Class cTCNMPdtGrpList
        Property FDArtDocDate As String = ""
        Property FNArtSeqNo As String = ""
        ' Property FTPdtStkCode As String = ""
        Property FTArtListFile As String = ""
        Property FTArtFile As String = ""
        Property FTArtStaPrc As String = "NULL"
        Property FTDataCnt As String = ""
    End Class

End Class
