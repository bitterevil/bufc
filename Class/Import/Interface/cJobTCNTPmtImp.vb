﻿Imports System.IO
Imports System.Data.SqlClient

Public Class cJobTCNTPmtImp
    Inherits cBaseImport

    Property oC_TCNTPmt As New cTCNTPmt
    'Private tC_TableTemp As String = "TTmpLnkAjp"
    Private tC_FileLog As String = ""
    Public tC_pathList As String = "" '*TON 59-03-11
    Public tC_ListFullName As String = "" 'PAN 59-06-06
    Public tC_PrcFullName As String = "" 'PAN 59-06-06
    Private Const tC_Table As String = "TCNTPmt"
    Private Const tC_TableTemp As String = "TLNKPmt"

    Public Overrides Sub C_GETxFile()
        aC_Detail.Clear()
        aC_DetailSub.Clear()
        Dim tFullName As String = ""
        Dim aFullName As String()
        Dim oDirInfoInbox As New DirectoryInfo(AdaConfig.cConfig.oConfigXml.tInbox)
        Dim oFiles As FileInfo() = oDirInfoInbox.GetFiles("*.xml")
        Dim oSortFiles = oFiles.OrderBy(Function(o) o.Name)

        For Each oItem In oSortFiles 'oFiles
            tFullName = Path.GetFileName(oItem.FullName)
            'aFullName = tFullName.Split("_")
            aFullName = tFullName.Split("-") 'Change Fomat file '*CH 02-12-2017

            If aFullName(0).ToUpper = "PROM" AndAlso aFullName.Count >= 3 Then
                Dim oFileDetail As New cBaseImport.cImportDetail
                oFileDetail.tC_Name = oItem.Name.ToUpper
                oFileDetail.tC_FullName = oItem.FullName
                Dim nMaxLen As Integer = oFileDetail.tC_Name.Length
                If Not nMaxLen > 20 Then
                    Continue For
                End If

                Dim tDateTime As String = ""
                tDateTime = SP_CNSETtStrConvertDateTime(aFullName(1) & aFullName(2))
                If IsDate(tDateTime) = True Then
                    oFileDetail.tC_Date = tDateTime
                    oFileDetail.nC_Status = 0
                Else
                    Continue For
                End If

                oFileDetail.nC_Length = oItem.Length
                oFileDetail.tC_Length = oItem.Length.SP_CNSETtFormatFileSize
                aC_Detail.Add(oFileDetail)
                aC_DetailSub.Add(oFileDetail)
            End If
        Next
        oFiles = Nothing
        oDirInfoInbox = Nothing
    End Sub

    Public Overrides Sub C_CALxProcess()
        'จำนวนไฟล์ '*CH 20-12-2014
        Dim nFile As Integer = 1
        Dim nLastLang As Integer = 0 '1:thai, 2:Eng
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLastLang = 1
        Dim cNextVal As Double = 0
        Dim cProgress As Double = 0
        Dim tMsgV As String = "" '*TON 59-03-11
        Dim bListStatus As Boolean = False
        Dim oPrepareFile As New cPrepareFile
        Dim tMsg As String = ""
        cNextVal = (100 / aC_Detail.Count) / 2

        'For Each oPmt In aC_DetailSub 'PAN 59-06-06 
        'oPrepareFile.tFullPath = oPmt.tC_FullName
        'cLog.C_CALxWriteLog("Import : " & oPrepareFile.tFullPath)

        'tC_pathList = oPrepareFile.tFullPath
        'oPrepareFile.tFullName = oPmt.tC_Name
        'tC_ListFullName = oPrepareFile.tFullName
        ''Dim aItemColFullPath As String() = tC_ListFullName.Split("_")
        'Dim aItemColFullPath As String() = tC_ListFullName.Split("-")
        'tC_ListFullName = aItemColFullPath(0)

        'If tC_pathList = "" Then
        '    MessageBox.Show("File Promotion not found." & vbCrLf & " Please Try Again", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Exit Sub '*TON 59-03-14
        'End If

        For Each oItem In aC_Detail.Where(Function(c) c.nC_Select = 1 And c.nC_Status = 0)
            oPrepareFile.tFullPath = oItem.tC_FullName
            cLog.C_CALxWriteLog("Import : " & oPrepareFile.tFullPath)

            tC_pathList = oPrepareFile.tFullPath
            oPrepareFile.tFullName = oItem.tC_Name
            tC_ListFullName = oPrepareFile.tFullName
            'Dim aItemColFullPath As String() = tC_ListFullName.Split("_")
            Dim aItemColFullPath As String() = tC_ListFullName.Split("-")
            tC_ListFullName = aItemColFullPath(0)

            If tC_pathList = "" Then
                MessageBox.Show("File Promotion not found." & vbCrLf & " Please Try Again", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub '*TON 59-03-14
            End If

            With oItem 'PAN 59-06-06
                tC_PrcFullName = .tC_Name
                'Dim aItemCol As String() = tC_PrcFullName.Split("_")
                Dim aItemCol As String() = tC_PrcFullName.Split("-")
                tC_PrcFullName = aItemCol(0)
            End With
            If tC_ListFullName.ToLower = tC_PrcFullName.ToLower Then

                'Grid row
                nC_GridRow = nFile

                cLog.C_CALxWriteLog("file :[" & nFile & " ] : " & oItem.tC_FullName)
                '*CH 20-12-2014
                cImportTemplate.tC_File = " [" & nFile & "] " & "นำเข้าข้อมูล;Import data".Split(";")(nLastLang)
                tC_FileLog = String.Format(tC_Log, Path.GetFileName(oItem.tC_FullName)).Replace(".xml", ".txt")
                cCNVB.tVB_PathLog = tC_FileLog

                With oC_TCNTPmt
                    cProgress += cNextVal
                    If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(cProgress)

                    'Check Article File 0 KB '*CH 2015-01-04
                    Dim oRdr = New StreamReader(oItem.tC_FullName, System.Text.Encoding.Default)
                    Dim bNotFile0Kb As Boolean = True
                    While True
                        If oRdr.ReadLine Is Nothing Then
                            bNotFile0Kb = False
                            Exit While
                        End If

                        Exit While
                    End While
                    oRdr.Dispose()
                    oRdr.Close()
                    oRdr = Nothing

                    If bNotFile0Kb Then
                        Dim oPrepare As New cPreparePmt
                        oPrepare.tFullPath = oItem.tC_FullName
                        oPrepare.tDateNow = tC_DateNow
                        If oPrepare.C_CALbProcess(tMsg) Then
                            'Update Promotion
                            C_CLRxUpdatePmt()

                            'Insert Product
                            .tFullPath = oItem.tC_FullName
                            .tFileLog = tC_FileLog
                            'dTPrepArt = Now
                            If .C_CALbProcess() Then 'Import สำเร็จ
                                oItem.nC_Status = 1
                                aC_Detail(nFile - 1).nC_Status = 1
                                nC_StaUpload = 1
                            Else 'Import ไม่สำเร็จ
                                oItem.nC_Status = 2
                                aC_Detail(nFile - 1).nC_Status = 2
                                nC_StaUpload = 2
                            End If
                            'C_CALxWriteLogErr("TimeInsert:: " & DateDiff(DateInterval.Minute, dTPrepArt, Now) & " Minute", tC_FileLog)

                            cProgress += cNextVal
                            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(cProgress)
                        Else
                            C_CALxWriteLogErr(tMsg, oItem.tC_FullName)
                            oItem.nC_Status = 2
                            aC_Detail(nFile - 1).nC_Status = 2
                            nC_StaUpload = 2
                        End If
                    Else
                        tMsg = "ไม่พบข้อมูลสินค้า;Not have data article.".Split(";")(nLastLang)
                        C_CALxWriteLogErr(tMsg, oItem.tC_FullName)
                        oItem.nC_Status = 2
                        aC_Detail(nFile - 1).nC_Status = 2
                        nC_StaUpload = 2
                    End If
                End With
            Else
                Continue For
            End If
            nFile += 1

            If tC_pathList <> "" Then
                C_CALxBackupFile(tC_pathList)
            End If
        Next
        'Next
        'If bListStatus = True Then C_CALxBackupFile(tc_pathList) '*TON ย้ายไฟล์ list
        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(100)
    End Sub

    Private Sub C_CALxWriteLogErr(ByVal ptMsg As String, ByVal ptFileNameErr As String)
        Try
            Dim oFileLog As New cImportLogHis(tC_FileLog)
            Dim oNewLog As New cImportLogHis.cLogHis
            With oNewLog
                .tErrMsg = ptMsg
            End With
            oFileLog.C_CALxAddNew(oNewLog)
            oFileLog.C_CALbSaveLogErr()
            Dim tDesc As String = "Insert(0),Update(0),Failed(1)"
            Dim oImp As New cImport
            oImp.C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(ptFileNameErr), tDesc, tC_FileLog))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub C_CALxBackupFile(ByVal ptFileCopyBk As String)
        Try
            Dim tFileName As String = ""
            'Create Dir '*CH 20-10-2015
            If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import") Then
                IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import")
            End If
            Try
                Dim nIndex As Integer = 0
                tFileName = Path.GetFileName(ptFileCopyBk)
                FileCopy(ptFileCopyBk, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import\" & tFileName)
                If File.Exists(ptFileCopyBk) Then
                    File.Delete(ptFileCopyBk)
                End If
                nIndex += 1
            Catch ex As Exception
            End Try
        Catch ex As Exception
        End Try
    End Sub

    Private Sub C_CLRxUpdatePmt()
        Dim oDatabase As New cDatabaseLocal
        Dim tDocDate As String = Format(Now, "yyyy/MM/dd")
        Dim oSql As System.Text.StringBuilder = Nothing
        Dim aSqlB4 As New List(Of String)
        Dim oDbTblHD As New DataTable
        Dim oDbTblDT As New DataTable
        Dim oDbTblCD As New DataTable
        Dim tMsg As String = ""

        Try
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("UPDATE Lnk SET")
            oSql.AppendLine("	Lnk.FNLnkStatus = 2")
            oSql.AppendLine("FROM TCNTPmtHD Pmt INNER JOIN TLNKPmtHD Lnk ON Pmt.FTPmhCode =  Lnk.FTPmhCode")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            'Update PdtCode,PdtName
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("UPDATE Temp SET")
            oSql.AppendLine("Temp.FTPdtCode = Pdt.FTPdtCode,")
            oSql.AppendLine("Temp.FTPdtName = Pdt.FTPdtName")
            oSql.AppendLine("FROM " & tC_TableTemp & "DT Temp INNER JOIN TCNMPdt Pdt ON Temp.FTPmdBarCode = Pdt.FTPdtBarCode1")
            'oSql.AppendLine("INNER JOIN TCNMPdtUnit Pun ON Pdt.FTPdtSUnit = Pun.FTPunCode")
            'oSql.AppendLine("WHERE Temp.FTPmdUnitName = Pun.FTPunName")
            aSqlB4.Add(oSql.ToString)
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("UPDATE Temp SET")
            oSql.AppendLine("Temp.FTPdtCode = Pdt.FTPdtCode,")
            oSql.AppendLine("Temp.FTPdtName = Pdt.FTPdtName")
            oSql.AppendLine("FROM " & tC_TableTemp & "DT Temp INNER JOIN TCNMPdt Pdt ON Temp.FTPmdBarCode = Pdt.FTPdtBarCode2")
            'oSql.AppendLine("INNER JOIN TCNMPdtUnit Pun ON Pdt.FTPdtMUnit = Pun.FTPunCode")
            'oSql.AppendLine("WHERE Temp.FTPmdUnitName = Pun.FTPunName")
            aSqlB4.Add(oSql.ToString)
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("UPDATE Temp SET")
            oSql.AppendLine("Temp.FTPdtCode = Pdt.FTPdtCode,")
            oSql.AppendLine("Temp.FTPdtName = Pdt.FTPdtName")
            oSql.AppendLine("FROM " & tC_TableTemp & "DT Temp INNER JOIN TCNMPdt Pdt ON Temp.FTPmdBarCode = Pdt.FTPdtBarCode3")
            'oSql.AppendLine("INNER JOIN TCNMPdtUnit Pun ON Pdt.FTPdtLUnit = Pun.FTPunCode")
            'oSql.AppendLine("WHERE Temp.FTPmdUnitName = Pun.FTPunName")
            aSqlB4.Add(oSql.ToString)
            oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)
            aSqlB4.Clear()

            oSql = New System.Text.StringBuilder
            oSql.AppendLine("UPDATE Pmh SET")
            oSql.AppendLine("FNLnkStatus=3,")
            oSql.AppendLine("FTLnkLog='FTPmdBarCode,ไม่มีอยู่จริงในฐานข้อมูล'")
            oSql.AppendLine("FROM " & tC_TableTemp & "HD Pmh INNER JOIN " & tC_TableTemp & "DT Pmd ON Pmh.FTPmhCode = Pmd.FTPmhCode")
            oSql.AppendLine("WHERE (ISNULL(Pmd.FTPdtCode,'') = '' AND ISNULL(Pmd.FTPdtName,'') = '')")
            oSql.AppendLine("AND FNLnkStatus <> 3")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            'Delete Old
            aSqlB4.Add("DELETE " & tC_Table & "HD WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 2)")
            aSqlB4.Add("DELETE " & tC_Table & "DT WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "DT WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 2))")
            aSqlB4.Add("DELETE " & tC_Table & "CD WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "CD WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 2))")
            oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)

            'Insert New
            'Header
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTBchCode,FTPmhCode,FTPmhName,")
            oSql.AppendLine("FTPmhNameSlip,FTSpmCode,FTSpmType,")
            oSql.AppendLine("FDPmhDStart,FDPmhDStop,FDPmhTStart,")
            oSql.AppendLine("FDPmhTStop,FTPmhClosed,FTPmhStatus,")
            oSql.AppendLine("FTPmhRetOrWhs,FTPmhRmk,FTPmhStaPrcDoc,")
            oSql.AppendLine("FNPmhStaAct,FTDptCode,FTUsrCode,")
            oSql.AppendLine("FTPmhApvCode,FTPmhBchTo,FTPmhZneTo,")
            oSql.AppendLine("FTPmhStaExceptPmt,FTSpmStaRcvFree,FTSpmStaAlwOffline,")
            oSql.AppendLine("FTSpmStaChkLimitGet,FNPmhLimitNum,FTPmhStaLimit,")
            oSql.AppendLine("FTPmhStaLimitCst,FTSpmStaChkCst,FNPmhCstNum,")
            oSql.AppendLine("FTSpmStaChkCstDOB,FNPmhCstDobNum,FNPmhCstDobPrev,")
            oSql.AppendLine("FNPmhCstDobNext,FTSpmStaUseRange,FTSplCode,")
            oSql.AppendLine("FDPntSplStart,FDPntSplExpired,FTCgpCode,")
            oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd,")
            oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns")
            oSql.AppendLine("FROM " & tC_TableTemp & "HD")
            oSql.AppendLine("WHERE FNLnkStatus = 2 ORDER BY FTPmhCode")
            oDbTblHD = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oDbTblHD.TableName = tC_Table & "HD"

            'Detail
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTBchCode,FTPmhCode,FNPmdSeq,")
            oSql.AppendLine("FTSpmCode,FTPmdGrpType,FTPmdGrpName,")
            oSql.AppendLine("FTPdtCode,FTPmdBarCode,FTPdtName,")
            oSql.AppendLine("FTPmdUnitName,FCPmdSetPriceOrg,")
            oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd,")
            oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns")
            oSql.AppendLine("FROM " & tC_TableTemp & "DT")
            oSql.AppendLine("WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 2)")
            oDbTblDT = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oDbTblDT.TableName = tC_Table & "DT"

            'Condition
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTBchCode,FTPmhCode,FNPmcSeq,")
            oSql.AppendLine("FTSpmCode,FTPmcGrpName,FTPmcStaGrpCond,")
            oSql.AppendLine("FCPmcPerAvgDis,FCPmcBuyAmt,FCPmcBuyQty,")
            oSql.AppendLine("FCPmcBuyMinQty,FCPmcBuyMaxQty,FDPmcBuyMinTime,")
            oSql.AppendLine("FDPmcBuyMaxTime,FCPmcGetCond,FCPmcGetValue,")
            oSql.AppendLine("FCPmcGetQty,FTSpmStaBuy,FTSpmStaRcv,FTSpmStaAllPdt,")
            oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd,")
            oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns")
            oSql.AppendLine("FROM " & tC_TableTemp & "CD")
            oSql.AppendLine("WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 2)")
            oDbTblCD = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oDbTblCD.TableName = tC_Table & "CD"
            If oDbTblHD.Rows.Count > 0 And oDbTblDT.Rows.Count > 0 And oDbTblCD.Rows.Count > 0 Then
                tMsg = C_COPtData(oDbTblHD)
                If tMsg = "" Then tMsg = C_COPtData(oDbTblDT)
                If tMsg = "" Then tMsg = C_COPtData(oDbTblCD)
                oDbTblHD.Clear()
                oDbTblDT.Clear()
                oDbTblCD.Clear()

                If tMsg = "" Then
                    oSql = New System.Text.StringBuilder
                    oSql.Append("UPDATE TLNKPmtHD SET FNLnkPrc = 1,FTLnkLog='" & cCNVB.tVB_SuccessImp & "' WHERE FNLnkStatus = 2")
                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
                Else
                    'UPDATE Status Temp
                    oSql = New System.Text.StringBuilder
                    oSql.AppendLine("UPDATE " & tC_TableTemp & "HD")
                    oSql.AppendLine("FNLnkStatus=3,")
                    oSql.AppendLine("FTLnkLog='" & Left(tMsg, 255) & "'")
                    oSql.AppendLine("WHERE FNLnkStatus = 2")
                    aSqlB4.Add(oSql.ToString)
                    oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)
                    aSqlB4.Clear()
                End If
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            aSqlB4 = Nothing
            oDbTblHD = Nothing
            oDbTblDT = Nothing
            oDbTblCD = Nothing
        End Try
    End Sub

End Class
