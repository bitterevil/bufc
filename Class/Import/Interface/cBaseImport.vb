﻿Public MustInherit Class cBaseImport

    Public Property tC_Log As String = ""
    Public Property tC_DateNow As String = ""

    'Property
    Property nC_Select As Integer = 1 'Default:1 Select,0:Deselect
    Property aC_Detail As New List(Of cImportDetail)
    Property aC_DetailSub As New List(Of cImportDetail)
    Public Shared nC_StaUpload As Integer = 0
    Public Shared nC_GridRow As Integer = 0
    Public ReadOnly Property nC_Count() As Integer 'จำนวนไฟล์
        Get
            Return aC_Detail.Count
        End Get
    End Property
    Public ReadOnly Property tC_SelCount() As String 'จำนวนไฟล์ที่เลือก
        Get
            Return aC_Detail.Count 'aW_Detail.Where(Function(c) c.nW_Select = 1).Count & "/" & aW_Detail.Count
        End Get
    End Property

    'Function
    Public MustOverride Sub C_GETxFile()
    Public MustOverride Sub C_CALxProcess()

    'Sub Class
    Class cImportDetail
        Private aStatus As New Dictionary(Of Integer, String)

        Sub New()
            'เพิ่มสถานะ
            aStatus.Add(0, "รอ;Wait")
            aStatus.Add(1, "สำเร็จ;Success")
            aStatus.Add(2, "ไม่สำเร็จ;Unsuccessful")
            aStatus.Add(-1, "ไฟล์ไม่ครบ;Missing")
        End Sub

        Property nC_Select As Integer = 1 'Default:1 Select,0:Deselect
        Property tC_Name As String = "" 'File Name
        Property tC_FullName As String = "" 'Full Path
        Property nC_Length As Long = 0
        Property tC_Length As String = nC_Length.SP_CNSETtFormatFileSize
        Property tC_Date As String = "" 'จาก Text File
        Property nC_Status As Integer = 0 '0:Wait,1:Import Complete,2:Import InComplete,-1:Error

        Public ReadOnly Property tC_Status() As String
            Get
                If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '2:thai
                    Return aStatus(nC_Status).Split(";")(1)
                Else
                    Return aStatus(nC_Status).Split(";")(0)
                End If
            End Get
        End Property

    End Class

End Class

