﻿Imports System.IO
Imports System.Text
Public Class cJobTCNTPdtTnf
    Inherits cBaseImport

    Property oC_TCNTPdtTnf As New cTCNTPdtTnf
    Private tC_FileLog As String = ""
    Public tC_pathList As String = ""
    Public tC_ListFullName As String = ""
    Public tC_PrcFullName As String = ""
    Private Const tC_Table As String = "TCNTPdtTnf"
    Private Const tC_TableTemp As String = "TLNKPdtTnf"
    Private tC_FullPath As String = ""

    Public Overrides Sub C_GETxFile()
        aC_Detail.Clear()
        aC_DetailSub.Clear()
        Dim tFullName As String = ""
        Dim aFullName As String()
        Dim oDirInfoInbox As New DirectoryInfo(AdaConfig.cConfig.oConfigXml.tInbox)
        Dim oFiles As FileInfo() = oDirInfoInbox.GetFiles("*.txt")
        Dim oSortFiles = oFiles.OrderBy(Function(o) o.Name)
        Try
            For Each oItem In oSortFiles 'oFiles
                tFullName = Path.GetFileName(oItem.FullName)
                aFullName = tFullName.Split("_")

                If aFullName(3).ToUpper = "STO.txt" AndAlso aFullName.Count >= 3 Then
                    Dim oFileDetail As New cBaseImport.cImportDetail
                    oFileDetail.tC_Name = oItem.Name.ToUpper
                    oFileDetail.tC_FullName = oItem.FullName
                    Dim nMaxLen As Integer = oFileDetail.tC_Name.Length
                    If Not nMaxLen > 20 Then
                        Continue For
                    End If

                    Dim tDateTime As String = ""
                    tDateTime = SP_CNSETtStrConvertDateTime(aFullName(1) & aFullName(2))
                    If IsDate(tDateTime) = True Then
                        oFileDetail.tC_Date = tDateTime
                        oFileDetail.nC_Status = 0
                    Else
                        Continue For
                    End If

                    oFileDetail.nC_Length = oItem.Length
                    oFileDetail.tC_Length = oItem.Length.SP_CNSETtFormatFileSize
                    aC_Detail.Add(oFileDetail)
                    aC_DetailSub.Add(oFileDetail)
                End If
            Next
            oFiles = Nothing
            oDirInfoInbox = Nothing
        Catch ex As Exception
        Finally
            tFullName = Nothing
            aFullName = Nothing
            oDirInfoInbox = Nothing
            oFiles = Nothing
            oSortFiles = Nothing
        End Try

    End Sub

    Public Overrides Sub C_CALxProcess()
        'จำนวนไฟล์ '*CH 20-12-2014
        Dim nFile As Integer = 1
        Dim nLastLang As Integer = 0 '1:thai, 2:Eng
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLastLang = 1
        Dim cNextVal As Double = 0
        Dim cProgress As Double = 0
        Dim tMsgV As String = "" '*TON 59-03-11
        Dim bListStatus As Boolean = False
        Dim oPrepareFile As New cPrepareFile
        Dim tMsg As String = ""
        Try

            cNextVal = (100 / aC_Detail.Count) / 2

            For Each oItem In aC_Detail.Where(Function(c) c.nC_Select = 1 And c.nC_Status = 0)
                oPrepareFile.tFullPath = oItem.tC_FullName
                cLog.C_CALxWriteLog("Import : " & oPrepareFile.tFullPath)
                tC_pathList = oPrepareFile.tFullPath
                oPrepareFile.tFullName = oItem.tC_Name
                tC_ListFullName = oPrepareFile.tFullName
                Dim aItemColFullPath As String() = tC_ListFullName.Split("_")
                tC_ListFullName = aItemColFullPath(0)
                If tC_pathList = "" Then
                    MessageBox.Show("File Tranfer Product not found." & vbCrLf & " Please Try Again", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                With oItem
                    tC_PrcFullName = .tC_Name
                    Dim aItemCol As String() = tC_PrcFullName.Split("_")
                    tC_PrcFullName = aItemCol(0)
                End With
                If tC_ListFullName.ToLower = tC_PrcFullName.ToLower Then

                    'Grid row
                    nC_GridRow = nFile
                    oPrepareFile.tFullPath = oItem.tC_FullName
                    oPrepareFile.tC_Log = tC_FileLog
                    cLog.C_CALxWriteLog("file :[" & nFile & " ] : " & oItem.tC_FullName)

                    'เตรียมไฟล์ เข้าตาราง Temp Tnf in
                    cImportTemplate.tC_File = " [" & nFile & "] " & " นำเข้าข้อมูล; Import data".Split(";")(nLastLang)
                    If Not oPrepareFile.C_CALbPrepareTnf() Then Exit For
                    If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(30)
                    tC_FileLog = String.Format(tC_Log, Path.GetFileName(oItem.tC_FullName)).Replace(".xml", ".txt")
                    cCNVB.tVB_PathLog = tC_FileLog

                    With oC_TCNTPdtTnf
                        cProgress += cNextVal
                        If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(cProgress)
                        Dim oRdr = New StreamReader(oItem.tC_FullName, System.Text.Encoding.Default)
                        Dim bNotFile0Kb As Boolean = True
                        While True
                            If oRdr.ReadLine Is Nothing Then
                                bNotFile0Kb = False
                                Exit While
                            End If
                            Exit While
                        End While
                        oRdr.Dispose()
                        oRdr.Close()
                        oRdr = Nothing

                        If bNotFile0Kb Then
                            cImportTemplate.tC_File = " [" & nFile & "] " & " นำเข้าข้อมูล; Import data".Split(";")(nLastLang)
                            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(35)
                            'Insert Tnf
                            .tFullPath = oItem.tC_FullName
                            .tFileLog = tC_FileLog
                            If .C_CALbProcess() Then 'Import สำเร็จ
                                oItem.nC_Status = 1
                                aC_Detail(nFile - 1).nC_Status = 1
                                nC_StaUpload = 1
                            Else 'Import ไม่สำเร็จ
                                oItem.nC_Status = 2
                                aC_Detail(nFile - 1).nC_Status = 2
                                nC_StaUpload = 2
                            End If
                            cProgress += cNextVal
                            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(90)
                        Else
                            C_CALxWriteLogErr(tMsg, oItem.tC_FullName)
                            oItem.nC_Status = 2
                            aC_Detail(nFile - 1).nC_Status = 2
                            nC_StaUpload = 2
                        End If
                        C_CALxBackupFile(tC_pathList)
                    End With
                Else
                    Continue For
                End If
                nFile += 1
            Next
        Catch ex As Exception
            C_CALxWriteLogErr(ex.Message)
        Finally
            oPrepareFile = Nothing
            If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(100)
        End Try

    End Sub

    Public Sub C_CALxWriteLogErr(ByVal ptMsg As String, Optional ByVal ptFileArticle As String = "", Optional ByVal ptFileLog As String = "")
        Dim tFileArticle As String = ""
        Dim tFileLog As String = IIf(ptFileLog <> "", ptFileLog, tC_FileLog)
        Dim oFileLog As New cImportLogHis(tFileLog)
        Dim oNewLog As New cImportLogHis.cLogHis
        Try
            With oNewLog
                .tErrMsg = ptMsg
            End With
            oFileLog.C_CALxAddNew(oNewLog)
            oFileLog.C_CALbSaveLogErr()
            Dim tDesc As String = "Insert(0),Update(0),Failed(1)"
            Dim oImp As New cImport
            tFileArticle = IIf(ptFileArticle <> "", ptFileArticle, tC_FullPath)
            oImp.C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFileArticle), tDesc, tFileLog))
        Catch ex As Exception
        Finally
            tFileArticle = Nothing
            tFileLog = Nothing
            oFileLog = Nothing
            oNewLog = Nothing
        End Try
    End Sub

    Public Sub C_CALxBackupFile(ByVal ptFileCopyBk As String)
        Dim tFileName As String = ""
        Try
            If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import") Then
                IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import")
            End If
            Try
                Dim nIndex As Integer = 0
                tFileName = Path.GetFileName(ptFileCopyBk)
                FileCopy(ptFileCopyBk, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tC_DateNow & "_Import\" & tFileName)
                If File.Exists(ptFileCopyBk) Then
                    File.Delete(ptFileCopyBk)
                End If
                nIndex += 1
            Catch ex As Exception
            End Try
        Catch ex As Exception
        Finally
            tFileName = Nothing
        End Try
    End Sub

    Public Sub C_CALxWriteLogEr(ByVal patErr As ArrayList, ByVal ptFullPath As String, ByVal pnInsert As Integer, ByVal pnFailed As Integer)
        Dim tC_FileLog As String = AdaConfig.cConfig.tAppPath & "\Log"
        Dim dDateNow As Date = Date.Now
        Dim oHDfile As New StringBuilder


        Dim tDesc As String = ""
        Dim tFileName As String = ""
        Dim oSQL As New StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim tDateTimeNow As String = Format(Now, "yyyy-MM-dd HH:mm:ss.fff")
        Dim tErr As String = ""

        Try
            If Not IO.Directory.Exists(tC_FileLog) Then
                IO.Directory.CreateDirectory(tC_FileLog)
            End If

            tC_FileLog &= "\" & "Log_ProductReceived" & Format(Date.Now, "yyyyMMdd-HHmmss") & ".txt"
            If File.Exists(tC_FileLog) = False Then
                Using oSw As StreamWriter = File.CreateText(tC_FileLog)
                    oSw.Close()
                End Using

                oHDfile.Append("[Import Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Import Time] " + Format(Date.Now, "HH:mm:ss") + vbTab + "")
                oHDfile.Append("[Import By] " + Environment.MachineName + Environment.NewLine + Environment.NewLine + "")
                oHDfile.Append("[Code]" + vbTab + vbTab + vbTab + "[Description]")
                Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                    With oStreamWriter
                        .WriteLine(oHDfile)
                        .Flush()
                        .Close()
                    End With
                End Using
            End If

            For nRow = 0 To patErr.Count - 1
                tErr += patErr(nRow) + Environment.NewLine
            Next

            Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                With oStreamWriter
                    .WriteLine(tErr)
                    .Flush()
                    .Close()
                End With
            End Using

            tDesc = "Success(" + Convert.ToString(pnInsert) + "),Failed(" + Convert.ToString(pnFailed) + ")"
            tFileName = Path.GetFileName(ptFullPath)
            oSQL.Clear()
            oSQL.AppendLine("INSERT INTO TLNKLog(uid,FDDateTime,FNType,FTFileName,FTDescription,FTRefer) VALUES")
            oSQL.AppendLine("('" + Guid.NewGuid.ToString + "','" + tDateTimeNow + "',1,'" + tFileName + "','" + tDesc + "','" + tC_FileLog + "')")
            If oSQL.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
            End If
        Catch ex As Exception
        Finally
            tC_FileLog = Nothing
            dDateNow = Nothing
            oHDfile = Nothing
        End Try

    End Sub
End Class
