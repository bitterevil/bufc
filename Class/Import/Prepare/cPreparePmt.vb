﻿Imports System.IO
Imports System.Text

Public Class cPreparePmt

    Property tFullPath As String = ""
    Property tDateNow As String = ""
    Private tC_FullPathTemp As String = ""

    Private tSpr As String = AdaConfig.cConfig.tSeparator
    'Private aPmtHD As New List(Of cPmtHD)
    'Private aPmtDT As New List(Of cPmtDT)
    'Private aPmtCD As New List(Of cPmtCD)
    Private tC_BchCode As String = ""
    'Private tC_UsrCode As String = ""
    Private tC_DptCode As String = ""
    Private tC_TempLnkPmt As String = "TLNKPmt"
    'Private nC_PmhRuning As Integer = 0
    Private aPmtCode As New List(Of cPmtCode)

    Property aDocFile As New List(Of cDocFile)
    Private aTempFile As New List(Of String)
    'Private oDatabase As New cDatabaseLocal

    Public Function C_CALbProcess(ByRef ptMsg As String) As Boolean
        Dim bPrepare As Boolean = False
        Try
            'Clear Temp
            C_CLRxClearTemp()

            'Get Branch HQ
            C_GETxBchHQ()

            ''Get User
            'C_GETxUser()

            'Prepare File Promotion
            bPrepare = C_CALbPmtFile(ptMsg)
        Catch ex As Exception

        End Try
        Return bPrepare
    End Function

    ''' <summary>
    ''' Clear Table Link Temp
    ''' </summary>
    Private Sub C_CLRxClearTemp()
        Dim oSql As System.Text.StringBuilder
        Dim aSqlB4 As New List(Of String)
        Dim oDatabase As New cDatabaseLocal

        oSql = New System.Text.StringBuilder
        oSql.AppendLine("TRUNCATE TABLE " & tC_TempLnkPmt & "HD")
        aSqlB4.Add(oSql.ToString)

        oSql = New System.Text.StringBuilder
        oSql.AppendLine("TRUNCATE TABLE " & tC_TempLnkPmt & "DT")
        aSqlB4.Add(oSql.ToString)

        oSql = New System.Text.StringBuilder
        oSql.AppendLine("TRUNCATE TABLE " & tC_TempLnkPmt & "CD")
        aSqlB4.Add(oSql.ToString)

        oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)

        aSqlB4.Clear()
    End Sub

    Private Sub C_GETxBchHQ()
        'Dim tSql As String = "SELECT TOP 1 FTBchCode FROM TCNMBranch WHERE FTBchHQ = '1'"
        Dim tSql As String = "SELECT FTBchCode FROM TCNMComp"
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TCNMBranch")
        If oDbTbl IsNot Nothing Then
            If oDbTbl.Rows.Count > 0 Then
                tC_BchCode = oDbTbl(0)("FTBchCode")
            End If
        End If
        oDbTbl = Nothing
        oDatabase = Nothing
    End Sub

    'Private Sub C_GETxUser()
    '    'UserCode
    '    Dim tSql As String = "SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END AS DEFUSER FROM TLNKMapping WHERE FTLNMCode='DEFUSER'"
    '    Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TLNKMapping")
    '    If oDbTbl IsNot Nothing Then
    '        If oDbTbl.Rows.Count > 0 Then
    '            tC_UsrCode = oDbTbl(0)("DEFUSER")
    '        End If
    '    End If
    '    tSql = "SELECT FTDptCode FROM TSysUser WHERE FTUsrCode = '" & tC_UsrCode & "'"
    '    oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TSysUser")
    '    If oDbTbl IsNot Nothing Then
    '        If oDbTbl.Rows.Count > 0 Then
    '            tC_DptCode = oDbTbl(0)("FTDptCode")
    '        End If
    '    End If
    '    oDbTbl = Nothing
    'End Sub

    Private Function C_GETnPmhCode(ByVal ptBchCode As String, ByVal ptRmk As String, ByRef ptOldPmhCode As String) As Integer
        Dim tSql As String = ""
        Dim oDbTbl As New DataTable
        Dim oDatabase As New cDatabaseLocal
        Dim nMaxCode As Integer = 0
        Try
            tSql = "SELECT TOP 1 RIGHT(FTPmhCode,6) As FTPmhCode FROM TCNTPmtHD WHERE LEFT(FTPmhCode,3) = '" & ptBchCode & "' AND FTPmhRmk = '" & ptRmk & "' "
            'tSql &= "CONVERT(DATE, FDPmhDStop) <= CONVERT(DATE, GETDATE())"
            oDbTbl = oDatabase.C_CALoExecuteReader(tSql)
            If oDbTbl.Rows.Count > 0 Then
                nMaxCode = -1
                ptOldPmhCode = oDbTbl(0)("FTPmhCode")
            Else
                oDbTbl = New DataTable
                tSql = "SELECT ISNULL(RIGHT(MAX(FTPmhCode),6),0) As FTPmhCode FROM TCNTPmtHD WHERE LEFT(FTPmhCode,3) = '" & ptBchCode & "'"
                oDbTbl = oDatabase.C_CALoExecuteReader(tSql)
                If oDbTbl IsNot Nothing Then
                    If oDbTbl.Rows.Count > 0 Then
                        nMaxCode = Convert.ToInt32(oDbTbl(0)("FTPmhCode"))
                    End If
                End If
            End If
        Catch ex As Exception
        Finally
            oDatabase = Nothing
        End Try
        Return nMaxCode
    End Function

    'Private Function C_CALbPmtFile(ByRef ptMsg As String) As Boolean
    '    'เตรียมข้อมูล เพื่อสร้างไฟล์ Temp
    '    Dim nCodeRuning As Integer = 1
    '    Dim nMaxCode As Integer = 0
    '    Dim tPmhCode As String = ""
    '    Dim tOldPmhCode As String = ""
    '    Dim tBchTo As String = ""
    '    Dim tMsg As String = ""
    '    Dim bPrepare As Boolean = False
    '    Dim nMinLoop As Integer = 2
    '    Dim nMaxLoop As Integer = 2
    '    Try
    '        Dim oDbTblHD As New DataTable
    '        Dim oDbTblDT As New DataTable
    '        Dim oDbTblCD As New DataTable
    '        Dim oXMLDoc As New XDocument
    '        oXMLDoc = XDocument.Load(tFullPath)
    '        Dim oElement = oXMLDoc.Root.Element("IDOC").Elements("E1WPBB01")

    '        oDbTblHD = C_CRTxDatatable(tC_TempLnkPmt & "HD")
    '        oDbTblDT = C_CRTxDatatable(tC_TempLnkPmt & "DT")
    '        oDbTblCD = C_CRTxDatatable(tC_TempLnkPmt & "CD")
    '        For nElement = 0 To oElement.Count - 1
    '            Dim tDStart As String = ""
    '            Dim tDEnd As String = ""
    '            Dim tSpmCode As String = "003"
    '            'Dim tBBYNR As String = "" 'BBY_NR
    '            Dim tPROMONR As String = "" 'PROMO_NR

    '            'PmtHD
    '            oDbTblHD.Rows.Add()
    '            With oDbTblHD.Rows(oDbTblHD.Rows.Count - 1)
    '                nMaxCode = 0
    '                tPmhCode = ""
    '                tOldPmhCode = ""
    '                'tBchTo = Right(oElement(nElement).Element("FILIALE").Value, 3)
    '                'Get Branch by Mapping
    '                tBchTo = SP_GETtBranchMap("", oElement(nElement).Element("FILIALE").Value)
    '                If tBchTo = "" Then tBchTo = SP_GETtPlantMap("", oElement(nElement).Element("FILIALE").Value)
    '                tDStart = oElement(nElement).Element("E1WPBB05").Element("START_DATE").Value
    '                tDEnd = oElement(nElement).Element("E1WPBB05").Element("END_DATE").Value
    '                'tBBYNR = oElement(nElement).Element("BBY_NR").Value
    '                tPROMONR = oElement(nElement).Element("E1WPBB05").Element("PROMO_NR").Value
    '                nMaxCode = C_GETnPmhCode(tC_BchCode, tPROMONR, tOldPmhCode)
    '                If nMaxCode >= 0 Then
    '                    tPmhCode = tC_BchCode & "-" & Convert.ToString(nCodeRuning + nMaxCode).PadLeft(6, "0")
    '                Else
    '                    If tOldPmhCode = "" Then tOldPmhCode = "1"
    '                    tPmhCode = tC_BchCode & "-" & tOldPmhCode.PadLeft(6, "0")
    '                End If
    '                .Item("FTBchCode") = tC_BchCode
    '                .Item("FTPmhBchTo") = tBchTo
    '                .Item("FTPmhCode") = tPmhCode
    '                .Item("FTPmhName") = Left(oElement(nElement).Element("E1WPBB07").Element("SHORT_TEXT").Value, 200)
    '                .Item("FTPmhNameSlip") = Left(oElement(nElement).Element("E1WPBB07").Element("SHORT_TEXT").Value, 25)
    '                If oElement(nElement).Element("E1WPBB02") Is Nothing Then
    '                    tSpmCode = "001"
    '                Else
    '                    'Check Barcode Is nothing
    '                    If oElement(nElement).Element("E1WPBB02").Element("MAT_EAN") Is Nothing Then
    '                    Else
    '                        'Check Have Barcode
    '                        If oElement(nElement).Element("E1WPBB02").Element("MAT_EAN").Value = "" Then
    '                            tSpmCode = "001"
    '                        Else
    '                            tSpmCode = "003"
    '                        End If
    '                    End If
    '                End If
    '                .Item("FTSpmCode") = tSpmCode
    '                .Item("FTSpmType") = "1"
    '                .Item("FDPmhDStart") = Left(tDStart, 4) & "-" & Mid(tDStart, 5, 2) & "-" & Right(tDStart, 2)
    '                .Item("FDPmhDStop") = Left(tDEnd, 4) & "-" & Mid(tDEnd, 5, 2) & "-" & Right(tDEnd, 2)
    '                .Item("FDPmhTStart") = .Item("FDPmhDStart") & " 00:00:00"
    '                .Item("FDPmhTStop") = .Item("FDPmhDStop") & " 23:59:59"
    '                .Item("FTPmhClosed") = "0"
    '                .Item("FTPmhStatus") = "2"
    '                .Item("FTPmhRetOrWhs") = "1"
    '                '.Item("FTPmhRmk") = tBBYNR & ":" & tPROMONR
    '                .Item("FTPmhRmk") = tPROMONR
    '                .Item("FTPmhStaPrcDoc") = "1"
    '                .Item("FNPmhStaAct") = 1
    '                .Item("FTUsrCode") = cCNVB.tVB_UserCode 'tC_UsrCode
    '                .Item("FTPmhApvCode") = cCNVB.tVB_UserCode 'tC_UsrCode
    '                .Item("FTPmhStaExceptPmt") = "1"
    '                .Item("FTSpmStaRcvFree") = "2"
    '                .Item("FTSpmStaAlwOffline") = "2"
    '                .Item("FTSpmStaChkLimitGet") = "2"
    '                .Item("FNPmhLimitNum") = 1
    '                .Item("FTPmhStaLimit") = "1"
    '                .Item("FTPmhStaLimitCst") = "1"
    '                .Item("FTSpmStaChkCst") = "2"
    '                .Item("FNPmhCstNum") = 1
    '                .Item("FTSpmStaChkCstDOB") = "2"
    '                .Item("FNPmhCstDobNum") = 1
    '                .Item("FNPmhCstDobPrev") = 0
    '                .Item("FNPmhCstDobNext") = 0
    '                .Item("FTSpmStaUseRange") = "3"
    '                .Item("FTSplCode") = ""
    '                .Item("FDPntSplStart") = Now.ToString("yyyy-MM-dd")
    '                .Item("FDPntSplExpired") = Now.ToString("yyyy-MM-dd")
    '                .Item("FTCgpCode") = ""
    '                .Item("FDDateUpd") = Now.ToString("yyyy-MM-dd")
    '                .Item("FTTimeUpd") = Now.ToString("HH:mm:ss")
    '                .Item("FTWhoUpd") = cCNVB.tVB_UserName
    '                .Item("FDDateIns") = Now.ToString("yyyy-MM-dd")
    '                .Item("FTTimeIns") = Now.ToString("HH:mm:ss")
    '                .Item("FTWhoIns") = cCNVB.tVB_UserName
    '                .Item("FNLNKStatus") = "1"
    '                .Item("FTLNKLog") = ""
    '                .Item("FNLNKLine") = nElement + 1
    '                .Item("FNLnkPrc") = "0"
    '            End With
    '            If tSpmCode = "001" Then nMinLoop = 1
    '            If tSpmCode = "001" Then nMaxLoop = 1

    '            'PmtDT
    '            For nItem = 1 To nMaxLoop
    '                Dim tBar As String = ""
    '                Dim tUnitName As String = ""
    '                Dim tElement As String = ""
    '                Dim bAddRow As Boolean = False
    '                Select Case nItem
    '                    Case 1 : If oElement(nElement).Element("E1WPBB03") IsNot Nothing Then bAddRow = True : tElement = "E1WPBB03"
    '                    Case 2 : If oElement(nElement).Element("E1WPBB02") IsNot Nothing Then bAddRow = True : tElement = "E1WPBB02"
    '                End Select
    '                If bAddRow Then
    '                    oDbTblDT.Rows.Add()
    '                    With oDbTblDT.Rows(oDbTblDT.Rows.Count - 1)
    '                        .Item("FTBchCode") = tC_BchCode
    '                        .Item("FTPmhCode") = tPmhCode
    '                        .Item("FNPmdSeq") = nItem
    '                        .Item("FTSpmCode") = tSpmCode
    '                        .Item("FTPmdGrpType") = "1"
    '                        .Item("FTPmdGrpName") = IIf(nItem = 1, "X", "Y")
    '                        '.Item("FTPmdBarCode") = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_EAN").Value
    '                        Select Case tElement
    '                            Case "E1WPBB03"
    '                                If oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_EAN") IsNot Nothing Then
    '                                    tBar = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_EAN").Value
    '                                End If
    '                            Case "E1WPBB02"
    '                                If oElement(nElement).Element("E1WPBB02").Element("MAT_EAN") IsNot Nothing Then
    '                                    tBar = oElement(nElement).Element("E1WPBB02").Element("MAT_EAN").Value
    '                                End If
    '                        End Select
    '                        .Item("FTPmdBarCode") = tBar
    '                        '.Item("FTPmdUnitName") = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_UNIT").Value
    '                        If oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_UNIT") IsNot Nothing Then
    '                            tUnitName = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_UNIT").Value
    '                        End If
    '                        .Item("FTPmdUnitName") = tUnitName
    '                        .Item("FCPmdSetPriceOrg") = 0
    '                        .Item("FDDateUpd") = Now.ToString("yyyy-MM-dd")
    '                        .Item("FTTimeUpd") = Now.ToString("HH:mm:ss")
    '                        .Item("FTWhoUpd") = cCNVB.tVB_UserName
    '                        .Item("FDDateIns") = Now.ToString("yyyy-MM-dd")
    '                        .Item("FTTimeIns") = Now.ToString("HH:mm:ss")
    '                        .Item("FTWhoIns") = cCNVB.tVB_UserName
    '                    End With
    '                End If
    '            Next

    '            'PmtCD
    '            For nItem = nMinLoop To 1 Step -1
    '                Dim bAddRow As Boolean = False
    '                Dim tCondType As String = "" 'BBY_TYPE
    '                Dim cVal As Double = 0
    '                Dim cValAmt As Double = 0 'KOND_VAL
    '                Dim cValPer As Double = 0 'KOND_PER

    '                Select Case nItem
    '                    Case 1 : If oElement(nElement).Element("E1WPBB03") IsNot Nothing Then bAddRow = True
    '                    Case 2 : If oElement(nElement).Element("E1WPBB02") IsNot Nothing Then bAddRow = True
    '                End Select
    '                If bAddRow Then
    '                    oDbTblCD.Rows.Add()
    '                    With oDbTblCD.Rows(oDbTblCD.Rows.Count - 1)
    '                        'FCPmcGetCond 1: Dis Amt., 2: Dis %, 3: Adjust Price
    '                        'tCondType = IIf(oElement(nElement).Element("E1WPBB05").Element("BBY_TYPE").Value = "%", "2", "1")
    '                        'BB01  Bonus buy: Price  >>> Fixed Price
    '                        'BB02  Bonus buy: abs.disc >> ส่วนลดเป็นมูลค่า 
    '                        'BB03  Bonus buy:  % disc. >> ส่วนลดเป็น %
    '                        'cVal = IIf(tCondType = "1", cValAmt, cValPer)
    '                        If oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_VAL") IsNot Nothing Then
    '                            cValAmt = Convert.ToDouble(oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_VAL").Value)
    '                        End If
    '                        If oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_PER") IsNot Nothing Then
    '                            cValPer = Convert.ToDouble(oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_PER").Value)
    '                        End If
    '                        Select Case oElement(nElement).Element("E1WPBB05").Element("COND_TYPE").Value.ToUpper
    '                            Case "BB01" : tCondType = "3" : cVal = cValAmt
    '                            Case "BB02" : tCondType = "1" : cVal = cValAmt
    '                            Case "BB03" : tCondType = "2" : cVal = cValPer
    '                        End Select

    '                        .Item("FTBchCode") = tC_BchCode
    '                        .Item("FTPmhCode") = tPmhCode
    '                        .Item("FNPmcSeq") = nItem
    '                        .Item("FTSpmCode") = tSpmCode
    '                        .Item("FTPmcGrpName") = IIf(nItem = 1, "X", "Y")
    '                        .Item("FCPmcGetCond") = tCondType
    '                        .Item("FCPmcGetValue") = cVal
    '                        .Item("FCPmcBuyQty") = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_QUAN").Value
    '                        .Item("FCPmcBuyMinQty") = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_QUAN").Value
    '                        If tSpmCode = "001" Then
    '                            .Item("FTPmcStaGrpCond") = "3"
    '                            .Item("FCPmcPerAvgDis") = "100"
    '                        Else
    '                            .Item("FTPmcStaGrpCond") = IIf(nItem = 1, "1", "2")
    '                            .Item("FCPmcPerAvgDis") = "50"
    '                            If nItem = 1 Then
    '                                .Item("FCPmcGetCond") = 1 'Fix
    '                                .Item("FCPmcGetValue") = 1 'Fix
    '                            Else
    '                                .Item("FCPmcBuyQty") = 0 'Fix
    '                                .Item("FCPmcBuyMinQty") = 0 'Fix
    '                            End If
    '                        End If
    '                        .Item("FCPmcBuyMaxQty") = 0
    '                        .Item("FCPmcBuyAmt") = 0
    '                        .Item("FDPmcBuyMinTime") = "1900-01-01"
    '                        .Item("FDPmcBuyMaxTime") = "1900-01-01"
    '                        .Item("FCPmcGetQty") = 1
    '                        .Item("FTSpmStaBuy") = "3"
    '                        .Item("FTSpmStaRcv") = "1"
    '                        .Item("FTSpmStaAllPdt") = "2"
    '                        .Item("FDDateUpd") = Now.ToString("yyyy-MM-dd")
    '                        .Item("FTTimeUpd") = Now.ToString("HH:mm:ss")
    '                        .Item("FTWhoUpd") = cCNVB.tVB_UserName
    '                        .Item("FDDateIns") = Now.ToString("yyyy-MM-dd")
    '                        .Item("FTTimeIns") = Now.ToString("HH:mm:ss")
    '                        .Item("FTWhoIns") = cCNVB.tVB_UserName
    '                    End With
    '                End If
    '            Next

    '            If nMaxCode > 0 Then nCodeRuning += 1
    '        Next

    '        If oDbTblHD.Rows.Count > 0 Then tMsg = C_COPtData(oDbTblHD)
    '        If oDbTblDT.Rows.Count > 0 And tMsg = "" Then tMsg = C_COPtData(oDbTblDT)
    '        If oDbTblCD.Rows.Count > 0 And tMsg = "" Then tMsg = C_COPtData(oDbTblCD)
    '        If tMsg = "" Then
    '            bPrepare = True
    '        End If
    '    Catch ex As Exception
    '        bPrepare = False
    '        ptMsg = ex.Message
    '    Finally
    '    End Try
    '    Return bPrepare
    'End Function
    Private Function C_CALbPmtFile(ByRef ptMsg As String) As Boolean
        'เตรียมข้อมูล เพื่อสร้างไฟล์ Temp
        Dim nCodeRuning As Integer = 1
        Dim nMaxCode As Integer = 0
        Dim tPmhCode As String = ""
        Dim tOldPmhCode As String = ""
        Dim tBchTo As String = ""
        Dim tMsg As String = ""
        Dim bPrepare As Boolean = False
        Dim nMinLoop As Integer = 2
        Dim nMaxLoop As Integer = 2
        Try
            Dim oDbTblHD As New DataTable
            Dim oDbTblDT As New DataTable
            Dim oDbTblCD As New DataTable
            Dim oXMLDoc As New XDocument
            oXMLDoc = XDocument.Load(tFullPath)
            Dim oElement = oXMLDoc.Root.Element("IDOC").Elements("E1WPBB01")

            oDbTblHD = C_CRTxDatatable(tC_TempLnkPmt & "HD")
            oDbTblDT = C_CRTxDatatable(tC_TempLnkPmt & "DT")
            oDbTblCD = C_CRTxDatatable(tC_TempLnkPmt & "CD")
            For nElement = 0 To oElement.Count - 1
                Dim tDStart As String = ""
                Dim tDEnd As String = ""
                Dim tSpmCode As String = "003"
                'Dim tBBYNR As String = "" 'BBY_NR
                Dim tPROMONR As String = "" 'PROMO_NR

                'PmtHD
                oDbTblHD.Rows.Add()
                With oDbTblHD.Rows(oDbTblHD.Rows.Count - 1)
                    nMaxCode = 0
                    tPmhCode = ""
                    tOldPmhCode = ""
                    'tBchTo = Right(oElement(nElement).Element("FILIALE").Value, 3)
                    'Get Branch by Mapping
                    tBchTo = SP_GETtBranchMap("", oElement(nElement).Element("FILIALE").Value)
                    If tBchTo = "" Then tBchTo = SP_GETtPlantMap("", oElement(nElement).Element("FILIALE").Value)
                    tDStart = oElement(nElement).Element("E1WPBB05").Element("START_DATE").Value
                    tDEnd = oElement(nElement).Element("E1WPBB05").Element("END_DATE").Value
                    'tBBYNR = oElement(nElement).Element("BBY_NR").Value
                    tPROMONR = oElement(nElement).Element("E1WPBB05").Element("PROMO_NR").Value
                    'nMaxCode = C_GETnPmhCode(tC_BchCode, tPROMONR, tOldPmhCode)
                    nMaxCode = C_GETnPmhCode(tBchTo, tPROMONR, tOldPmhCode)
                    If nMaxCode >= 0 Then
                        'tPmhCode = tC_BchCode & "-" & Convert.ToString(nCodeRuning + nMaxCode).PadLeft(6, "0")
                        tPmhCode = tBchTo & "-" & Convert.ToString(nCodeRuning + nMaxCode).PadLeft(6, "0")
                        nCodeRuning += 1
                    Else
                        If tOldPmhCode = "" Then tOldPmhCode = "1"
                        'tPmhCode = tC_BchCode & "-" & tOldPmhCode.PadLeft(6, "0")
                        tPmhCode = tBchTo & "-" & tOldPmhCode.PadLeft(6, "0")
                    End If
                    .Item("FTBchCode") = tC_BchCode
                    .Item("FTPmhBchTo") = tBchTo
                    .Item("FTPmhCode") = tPmhCode
                    .Item("FTPmhName") = Left(oElement(nElement).Element("E1WPBB07").Element("SHORT_TEXT").Value, 200)
                    .Item("FTPmhNameSlip") = Left(oElement(nElement).Element("E1WPBB07").Element("SHORT_TEXT").Value, 25)
                    If oElement(nElement).Element("E1WPBB02") Is Nothing Then
                        tSpmCode = "001"
                    Else
                        'Check Barcode Is nothing
                        If oElement(nElement).Element("E1WPBB02").Element("MAT_EAN") Is Nothing Then
                        Else
                            'Check Have Barcode
                            If oElement(nElement).Element("E1WPBB02").Element("MAT_EAN").Value = "" Then
                                tSpmCode = "001"
                            Else
                                tSpmCode = "003"
                            End If
                        End If
                    End If
                    .Item("FTSpmCode") = tSpmCode
                    .Item("FTSpmType") = "1"
                    .Item("FDPmhDStart") = Left(tDStart, 4) & "-" & Mid(tDStart, 5, 2) & "-" & Right(tDStart, 2)
                    .Item("FDPmhDStop") = Left(tDEnd, 4) & "-" & Mid(tDEnd, 5, 2) & "-" & Right(tDEnd, 2)
                    .Item("FDPmhTStart") = .Item("FDPmhDStart") & " 00:00:00"
                    .Item("FDPmhTStop") = .Item("FDPmhDStop") & " 23:59:59"
                    .Item("FTPmhClosed") = "0"
                    .Item("FTPmhStatus") = "2"
                    .Item("FTPmhRetOrWhs") = "1"
                    '.Item("FTPmhRmk") = tBBYNR & ":" & tPROMONR
                    .Item("FTPmhRmk") = tPROMONR
                    .Item("FTPmhStaPrcDoc") = "1"
                    .Item("FNPmhStaAct") = 1
                    .Item("FTUsrCode") = cCNVB.tVB_UserCode 'tC_UsrCode
                    .Item("FTPmhApvCode") = cCNVB.tVB_UserCode 'tC_UsrCode
                    .Item("FTPmhStaExceptPmt") = "1"
                    .Item("FTSpmStaRcvFree") = "2"
                    .Item("FTSpmStaAlwOffline") = "2"
                    .Item("FTSpmStaChkLimitGet") = "2"
                    .Item("FNPmhLimitNum") = 1
                    .Item("FTPmhStaLimit") = "1"
                    .Item("FTPmhStaLimitCst") = "1"
                    .Item("FTSpmStaChkCst") = "2"
                    .Item("FNPmhCstNum") = 1
                    .Item("FTSpmStaChkCstDOB") = "2"
                    .Item("FNPmhCstDobNum") = 1
                    .Item("FNPmhCstDobPrev") = 0
                    .Item("FNPmhCstDobNext") = 0
                    .Item("FTSpmStaUseRange") = "3"
                    .Item("FTSplCode") = ""
                    .Item("FDPntSplStart") = Now.ToString("yyyy-MM-dd")
                    .Item("FDPntSplExpired") = Now.ToString("yyyy-MM-dd")
                    .Item("FTCgpCode") = ""
                    .Item("FDDateUpd") = Now.ToString("yyyy-MM-dd")
                    .Item("FTTimeUpd") = Now.ToString("HH:mm:ss")
                    .Item("FTWhoUpd") = cCNVB.tVB_UserName
                    .Item("FDDateIns") = Now.ToString("yyyy-MM-dd")
                    .Item("FTTimeIns") = Now.ToString("HH:mm:ss")
                    .Item("FTWhoIns") = cCNVB.tVB_UserName
                    .Item("FNLNKStatus") = "1"
                    .Item("FTLNKLog") = ""
                    .Item("FNLNKLine") = nElement + 1
                    .Item("FNLnkPrc") = "0"
                End With
                If tSpmCode = "001" Then nMinLoop = 1
                If tSpmCode = "001" Then nMaxLoop = 1

                'PmtDT nItem=1:x, 2:y
                Dim nSeqDT As Integer = 1
                For nItem = 1 To nMaxLoop
                    Dim tElement As String = ""
                    Dim bAddRow As Boolean = False
                    Dim oEItem As IEnumerable(Of XElement) = oElement(nElement).Element("E1WPBB03").Elements("E1WPBB04")
                    Select Case nItem
                        Case 1 : If oElement(nElement).Element("E1WPBB03") IsNot Nothing Then bAddRow = True : tElement = "E1WPBB03" : 
                            oEItem = oElement(nElement).Element("E1WPBB03").Elements("E1WPBB04")
                        Case 2 : If oElement(nElement).Element("E1WPBB02") IsNot Nothing Then bAddRow = True : tElement = "E1WPBB02" : 
                            oEItem = oElement(nElement).Elements("E1WPBB02")
                    End Select

                    For nEItem = 0 To oEItem.Count - 1
                        Dim tBar As String = ""
                        Dim tUnitName As String = ""

                        If bAddRow Then oDbTblDT.Rows.Add()
                        With oDbTblDT.Rows(oDbTblDT.Rows.Count - 1)
                            .Item("FTBchCode") = tC_BchCode
                            .Item("FTPmhCode") = tPmhCode
                            .Item("FNPmdSeq") = nSeqDT
                            .Item("FTSpmCode") = tSpmCode
                            .Item("FTPmdGrpType") = "1"
                            .Item("FTPmdGrpName") = IIf(nItem = 1, "X", "Y")
                            '.Item("FTPmdBarCode") = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_EAN").Value
                            Select Case tElement
                                Case "E1WPBB03"
                                    If oEItem(nEItem).Element("MAT_EAN") IsNot Nothing Then
                                        tBar = oEItem(nEItem).Element("MAT_EAN").Value
                                    End If
                                    'If oEItem(nEItem).Element("MAT_UNIT") IsNot Nothing Then
                                    '    tUnitName = oEItem(nEItem).Element("MAT_UNIT").Value
                                    'End If
                                Case "E1WPBB02"
                                    If oEItem(nEItem).Element("MAT_EAN") IsNot Nothing Then
                                        tBar = oEItem(nEItem).Element("MAT_EAN").Value
                                    End If
                                    'If oElement(nElement).Element("E1WPBB03").Elements("E1WPBB04")(0).Element("MAT_UNIT") IsNot Nothing Then
                                    '    tUnitName = oElement(nElement).Element("E1WPBB03").Elements("E1WPBB04")(0).Element("MAT_UNIT").Value
                                    'End If
                            End Select
                            .Item("FTPmdBarCode") = tBar
                            .Item("FTPmdUnitName") = tUnitName
                            .Item("FCPmdSetPriceOrg") = 0
                            .Item("FDDateUpd") = Now.ToString("yyyy-MM-dd")
                            .Item("FTTimeUpd") = Now.ToString("HH:mm:ss")
                            .Item("FTWhoUpd") = cCNVB.tVB_UserName
                            .Item("FDDateIns") = Now.ToString("yyyy-MM-dd")
                            .Item("FTTimeIns") = Now.ToString("HH:mm:ss")
                            .Item("FTWhoIns") = cCNVB.tVB_UserName

                            If tBar = "" Then
                                bAddRow = False
                            Else
                                bAddRow = True
                                nSeqDT += 1
                            End If
                        End With
                    Next
                Next

                'PmtCD
                For nItem = nMinLoop To 1 Step -1
                    Dim bAddRow As Boolean = False
                    Dim tCondType As String = "" 'BBY_TYPE
                    Dim cVal As Double = 0
                    Dim cValAmt As Double = 0 'KOND_VAL
                    Dim cValPer As Double = 0 'KOND_PER

                    Select Case nItem
                        Case 1 : If oElement(nElement).Element("E1WPBB03") IsNot Nothing Then bAddRow = True
                        Case 2 : If oElement(nElement).Element("E1WPBB02") IsNot Nothing Then bAddRow = True
                    End Select
                    If bAddRow Then
                        oDbTblCD.Rows.Add()
                        With oDbTblCD.Rows(oDbTblCD.Rows.Count - 1)
                            'FCPmcGetCond 1: Dis Amt., 2: Dis %, 3: Adjust Price
                            'tCondType = IIf(oElement(nElement).Element("E1WPBB05").Element("BBY_TYPE").Value = "%", "2", "1")
                            'BB01  Bonus buy: Price  >>> Fixed Price
                            'BB02  Bonus buy: abs.disc >> ส่วนลดเป็นมูลค่า 
                            'BB03  Bonus buy:  % disc. >> ส่วนลดเป็น %
                            'cVal = IIf(tCondType = "1", cValAmt, cValPer)
                            If oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_VAL") IsNot Nothing Then
                                cValAmt = Convert.ToDouble(oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_VAL").Value)
                            End If
                            If oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_PER") IsNot Nothing Then
                                cValPer = Convert.ToDouble(oElement(nElement).Element("E1WPBB05").Element("E1WPBB06").Element("KOND_PER").Value)
                            End If
                            Select Case oElement(nElement).Element("E1WPBB05").Element("COND_TYPE").Value.ToUpper
                                Case "BB01" : tCondType = "3" : cVal = cValAmt
                                Case "BB02" : tCondType = "1" : cVal = cValAmt
                                Case "BB03" : tCondType = "2" : cVal = cValPer
                            End Select

                            .Item("FTBchCode") = tC_BchCode
                            .Item("FTPmhCode") = tPmhCode
                            .Item("FNPmcSeq") = nItem
                            .Item("FTSpmCode") = tSpmCode
                            .Item("FTPmcGrpName") = IIf(nItem = 1, "X", "Y")
                            .Item("FCPmcGetCond") = tCondType
                            .Item("FCPmcGetValue") = cVal
                            .Item("FCPmcBuyQty") = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_QUAN").Value
                            .Item("FCPmcBuyMinQty") = oElement(nElement).Element("E1WPBB03").Element("E1WPBB04").Element("MAT_QUAN").Value
                            If tSpmCode = "001" Then
                                .Item("FTPmcStaGrpCond") = "3"
                                .Item("FCPmcPerAvgDis") = "100"
                            Else
                                .Item("FTPmcStaGrpCond") = IIf(nItem = 1, "1", "2")
                                .Item("FCPmcPerAvgDis") = "0"
                                If .Item("FTPmcGrpName") = "Y" Then .Item("FCPmcPerAvgDis") = "100"
                                If nItem = 1 Then
                                    .Item("FCPmcGetCond") = 1 'Fix
                                    .Item("FCPmcGetValue") = 1 'Fix
                                Else
                                    .Item("FCPmcBuyQty") = 0 'Fix
                                    .Item("FCPmcBuyMinQty") = 0 'Fix
                                End If
                            End If
                            .Item("FCPmcBuyMaxQty") = 0
                            .Item("FCPmcBuyAmt") = 0
                            .Item("FDPmcBuyMinTime") = "1900-01-01"
                            .Item("FDPmcBuyMaxTime") = "1900-01-01"
                            .Item("FCPmcGetQty") = 1
                            .Item("FTSpmStaBuy") = "3"
                            .Item("FTSpmStaRcv") = "1"
                            .Item("FTSpmStaAllPdt") = "2"
                            .Item("FDDateUpd") = Now.ToString("yyyy-MM-dd")
                            .Item("FTTimeUpd") = Now.ToString("HH:mm:ss")
                            .Item("FTWhoUpd") = cCNVB.tVB_UserName
                            .Item("FDDateIns") = Now.ToString("yyyy-MM-dd")
                            .Item("FTTimeIns") = Now.ToString("HH:mm:ss")
                            .Item("FTWhoIns") = cCNVB.tVB_UserName
                        End With
                    End If
                Next
            Next

            If oDbTblHD.Rows.Count > 0 Then tMsg = C_COPtData(oDbTblHD)
            If oDbTblDT.Rows.Count > 0 And tMsg = "" Then tMsg = C_COPtData(oDbTblDT)
            If oDbTblCD.Rows.Count > 0 And tMsg = "" Then tMsg = C_COPtData(oDbTblCD)
            If tMsg = "" Then
                bPrepare = True
            End If
        Catch ex As Exception
            bPrepare = False
            ptMsg = ex.Message
        Finally
        End Try
        Return bPrepare
    End Function


    Public Sub C_CALxClearTempFile()
        Try
            For Each tFileTemp In aTempFile
                If File.Exists(tFileTemp) Then
                    File.Delete(tFileTemp)
                End If
            Next
        Catch ex As Exception
        End Try
        aTempFile.Clear()

        Dim tFileName As String = ""
        Dim aFile As String() = {tFullPath}
        'Create Dir '*CH 20-10-2015
        If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tDateNow) Then
            IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tDateNow)
        End If
        Try
            Dim nIndex As Integer = 0
            For Each tFile In aFile
                tFileName = Path.GetFileName(tFile)
                FileCopy(tFile, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tDateNow & "\" & tFileName)
                If File.Exists(tFile) Then
                    File.Delete(tFile)
                End If
                nIndex += 1
            Next
        Catch ex As Exception
        End Try
    End Sub

    ''Promotion HD
    'Private Class cPmtHD
    '    Property FTBchCode As String = ""
    '    Property FTPmhCode As String = ""
    '    Property FTPmhName As String = "Promotion Price Change"
    '    Property FTPmhNameSlip As String = "Promotion Price Change"
    '    Property FTSpmCode As String = ""
    '    Property FTSpmType As String = "1"
    '    Property FDPmhDStart As String = ""
    '    Property FDPmhDStop As String = ""
    '    Property FDPmhTStart As String = Format(Now, "HH:mm:ss")
    '    Property FDPmhTStop As String = Format(Now, "HH:mm:ss")
    '    Property FTPmhClosed As String = "0"
    '    Property FTPmhStatus As String = ""
    '    Property FTPmhRetOrWhs As String = "1"
    '    Property FTPmhRmk As String = ""
    '    Property FTPmhStaPrcDoc As String = "1"
    '    Property FNPmhStaAct As String = "1"
    '    Property FTDptCode As String = ""
    '    Property FTUsrCode As String = ""
    '    Property FTPmhApvCode As String = ""
    '    Property FTPmhBchTo As String = ""
    '    Property FTPmhZneTo As String = ""
    '    Property FTPmhStaExceptPmt As String = "1"
    '    Property FTSpmStaRcvFree As String = ""
    '    Property FTSpmStaAlwOffline As String = "2"
    '    Property FTSpmStaChkLimitGet As String = "2"
    '    Property FNPmhLimitNum As String = "1"
    '    Property FTPmhStaLimit As String = "1"
    '    Property FTPmhStaLimitCst As String = "1"
    '    Property FTSpmStaChkCst As String = "2"
    '    Property FNPmhCstNum As String = "1"
    '    Property FTSpmStaChkCstDOB As String = "2"
    '    Property FNPmhCstDobNum As String = "1"
    '    Property FNPmhCstDobPrev As String = "0"
    '    Property FNPmhCstDobNext As String = "0"
    '    Property FTSpmStaUseRange As String = "3"
    '    Property FTSplCode As String = ""
    '    Property FDPntSplStart As String = Format(Now, "yyyy/MM/dd")
    '    Property FDPntSplExpired As String = Format(Now, "yyyy/MM/dd")
    '    Property FTCgpCode As String = ""
    'End Class

    ''Promotion DT
    'Private Class cPmtDT
    '    Property FTBchCode As String = ""
    '    Property FTPmhCode As String = ""
    '    Property FNPmdSeq As String = ""
    '    Property FTSpmCode As String = ""
    '    Property FTPmdGrpType As String = ""
    '    Property FTPmdGrpName As String = "Promotion Price Change"
    '    Property FTPdtCode As String = ""
    '    Property FTPmdBarCode As String = ""
    '    Property FTPdtName As String = ""
    '    Property FTPmdUnitName As String = ""
    '    Property FCPmdSetPriceOrg As String = "0"
    'End Class

    ''Promotion CD
    'Private Class cPmtCD
    '    Property FTBchCode As String = ""
    '    Property FTPmhCode As String = ""
    '    Property FNPmcSeq As String = ""
    '    Property FTSpmCode As String = ""
    '    Property FTPmcGrpName As String = "Promotion Price Change"
    '    Property FTPmcStaGrpCond As String = ""
    '    Property FCPmcPerAvgDis As String = ""
    '    Property FCPmcBuyAmt As String = ""
    '    Property FCPmcBuyQty As String = ""
    '    Property FCPmcBuyMinQty As String = ""
    '    Property FCPmcBuyMaxQty As String = ""
    '    Property FDPmcBuyMinTime As String = Format(Now, "HH:mm:ss")
    '    Property FDPmcBuyMaxTime As String = Format(Now, "HH:mm:ss")
    '    Property FCPmcGetCond As String = ""
    '    Property FCPmcGetValue As String = ""
    '    Property FCPmcGetQty As String = ""
    '    Property FTSpmStaBuy As String = "3"
    '    Property FTSpmStaRcv As String = "1"
    '    Property FTSpmStaAllPdt As String = "2"
    'End Class

    'Running Promotion Code
    Private Class cPmtCode
        Property FTBchCode As String = ""
        Property FNPmhCode As Integer = 0
        Property FNSeqNo As Integer = 0
    End Class

    Private Function C_CRTxDatatable(ptTable As String) As DataTable
        Dim oDT As New DataTable(ptTable)
        Try
            Select Case ptTable 'PAN 20170321 'สร้าง Datatable ตามที่ส่งมา
                Case "TLNKPmtHD"
                    oDT.Columns.Add("FTBchCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhName", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhNameSlip", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmType", Type.GetType("System.String"))
                    oDT.Columns.Add("FDPmhDStart", Type.GetType("System.String"))
                    oDT.Columns.Add("FDPmhDStop", Type.GetType("System.String"))
                    oDT.Columns.Add("FDPmhTStart", Type.GetType("System.String"))
                    oDT.Columns.Add("FDPmhTStop", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhClosed", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhStatus", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhRetOrWhs", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhRmk", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhStaPrcDoc", Type.GetType("System.String"))
                    oDT.Columns.Add("FNPmhStaAct", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FTDptCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTUsrCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhApvCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhBchTo", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhZneTo", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhStaExceptPmt", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmStaRcvFree", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmStaAlwOffline", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmStaChkLimitGet", Type.GetType("System.String"))
                    oDT.Columns.Add("FNPmhLimitNum", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FTPmhStaLimit", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhStaLimitCst", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmStaChkCst", Type.GetType("System.String"))
                    oDT.Columns.Add("FNPmhCstNum", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FTSpmStaChkCstDOB", Type.GetType("System.String"))
                    oDT.Columns.Add("FNPmhCstDobNum", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FNPmhCstDobPrev", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FNPmhCstDobNext", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FTSpmStaUseRange", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSplCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FDPntSplStart", Type.GetType("System.String"))
                    oDT.Columns.Add("FDPntSplExpired", Type.GetType("System.String"))
                    oDT.Columns.Add("FTCgpCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FDDateUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FTTimeUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FTWhoUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FDDateIns", Type.GetType("System.String"))
                    oDT.Columns.Add("FTTimeIns", Type.GetType("System.String"))
                    oDT.Columns.Add("FTWhoIns", Type.GetType("System.String"))
                    oDT.Columns.Add("FNLnkLine", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FNLnkStatus", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FTLnkLog", Type.GetType("System.String"))
                    oDT.Columns.Add("FNLnkPrc", Type.GetType("System.Int32"))
                Case "TLNKPmtDT"
                    oDT.Columns.Add("FTBchCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FNPmdSeq", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FTSpmCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmdGrpType", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmdGrpName", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPdtCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmdBarCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPdtName", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmdUnitName", Type.GetType("System.String"))
                    oDT.Columns.Add("FCPmdSetPriceOrg", Type.GetType("System.Double"))
                    oDT.Columns.Add("FDDateUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FTTimeUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FTWhoUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FDDateIns", Type.GetType("System.String"))
                    oDT.Columns.Add("FTTimeIns", Type.GetType("System.String"))
                    oDT.Columns.Add("FTWhoIns", Type.GetType("System.String"))
                Case "TLNKPmtCD"
                    oDT.Columns.Add("FTBchCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmhCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FNPmcSeq", Type.GetType("System.Int32"))
                    oDT.Columns.Add("FTSpmCode", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmcGrpName", Type.GetType("System.String"))
                    oDT.Columns.Add("FTPmcStaGrpCond", Type.GetType("System.String"))
                    oDT.Columns.Add("FCPmcPerAvgDis", Type.GetType("System.Double"))
                    oDT.Columns.Add("FCPmcBuyAmt", Type.GetType("System.Double"))
                    oDT.Columns.Add("FCPmcBuyQty", Type.GetType("System.Double"))
                    oDT.Columns.Add("FCPmcBuyMinQty", Type.GetType("System.Double"))
                    oDT.Columns.Add("FCPmcBuyMaxQty", Type.GetType("System.Double"))
                    oDT.Columns.Add("FDPmcBuyMinTime", Type.GetType("System.String"))
                    oDT.Columns.Add("FDPmcBuyMaxTime", Type.GetType("System.String"))
                    oDT.Columns.Add("FCPmcGetCond", Type.GetType("System.Double"))
                    oDT.Columns.Add("FCPmcGetValue", Type.GetType("System.Double"))
                    oDT.Columns.Add("FCPmcGetQty", Type.GetType("System.Double"))
                    oDT.Columns.Add("FTSpmStaBuy", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmStaRcv", Type.GetType("System.String"))
                    oDT.Columns.Add("FTSpmStaAllPdt", Type.GetType("System.String"))
                    oDT.Columns.Add("FDDateUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FTTimeUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FTWhoUpd", Type.GetType("System.String"))
                    oDT.Columns.Add("FDDateIns", Type.GetType("System.String"))
                    oDT.Columns.Add("FTTimeIns", Type.GetType("System.String"))
                    oDT.Columns.Add("FTWhoIns", Type.GetType("System.String"))
            End Select
        Catch ex As Exception
        End Try
        Return oDT
    End Function
End Class
