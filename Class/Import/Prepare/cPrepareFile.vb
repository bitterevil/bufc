﻿Imports System.IO
Imports System.Threading
Imports System.Reflection
Imports System.Data.SqlClient
Imports AdaLinkSAPBUFC.Class.Config
Imports System.Text

Public Class cPrepareFile
    Public Property tC_Log As String = ""
    Private aTemp As ArrayList
    Private nMxLevel As Integer = 1
    Private nLevel As Integer = 1
    Property tFullPath As String = ""
    Property tFullName As String = ""
    Property PronoCode1 As String = ""
    Property PronoCode2 As String = ""
    Property PronoCode3 As String = ""
    Property tDateNow As String = ""
    Private tFullPathTemp As String = ""

    Private tSpr As String = AdaConfig.cConfig.tSeparator

    Private aItemModel As New List(Of String)
    Private aItemType As New List(Of String)
    Private tINSGrp As String = ""
    Private aTempFile As New List(Of String)
    Private oC_Database As New cDatabaseLocal

    Private tC_Prepare As String = "เตรียมข้อมูล;Prepare"
    Private tC_TempLnkPdt As String = "TLNKPdt"
    Private tC_TempLnkPdtUnit As String = "TLNKPdtUnit"
    Private tC_TempLnkPdtGrp As String = "TLNKPdtGrp"

    'Private oDatabase As New cDatabaseLocal
    Private oTCNMPdt As cJobTCNMPdt
    Private oTCNTPdtAjp As cJobTCNTPdtAjp
    Private oTCNTPdtTnf As cJobTCNTPdtTnf
    Property bCVTChain As Boolean = False


    Public Function C_CALbPreparePdt() As Boolean
        Dim bStaPre As Boolean = False
        Try
            bStaPre = C_CALbPdtFile()
        Catch ex As Exception
        End Try
        Return bStaPre
    End Function

    Public Function C_CALbPrepareAjp() As Boolean
        Dim bStaPre As Boolean = False
        Try
            bStaPre = C_CALbAjpFile()
        Catch ex As Exception
        End Try
        Return bStaPre
    End Function

    Public Function C_CALbPrepareTnf() As Boolean
        Dim bStaPre As Boolean = False
        Try
            bStaPre = C_CALbTnfFile()
        Catch ex As Exception
        End Try
        Return bStaPre
    End Function

    ''' <summary>
    ''' เตรียมข้อมูลสินค้าลง Table Temp
    ''' </summary>
    ''' <returns>Boolean</returns>
    Private Function C_CALbPdtFile() As Boolean
        'เตรียมข้อมูล เพื่อสร้างไฟล์ Temp
        Dim oDbTbl As New DataTable
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim tMsg As String = ""
        Dim bStaPrc As Boolean = True
        Dim oLstPdtMaster As New List(Of cPdtMaster)
        Dim atPdtRow As New ArrayList
        Dim tPdtDataRow As String = String.Empty
        Dim tPdtPreFix As String = String.Empty
        Dim tPathLog As String = AdaConfig.cConfig.oConfigXml.tLog  ' Pong Path Log
        Dim nSuccess As Integer = 0
        Dim nFailed As Integer = 0
        Dim tDateTimeNow As String = Format(Now, "yyyy-MM-dd HH:mm:ss.fff")
        Dim tDescription As String = String.Empty
        Dim atFTData() As String
        Dim tFTRow As String = ""
        Dim atDesc As New ArrayList
        Dim oPdtCodeDulicate As New List(Of cPdtCodeDulicate)
        Try
            'Get Vat company
            oSql.AppendLine("SELECT ISNULL(FCVatRate,0)FCVatRate FROM TCNMVatRate WHERE FTVatCode IN (SELECT TOP 1 FTVatCode FROM TCNMComp)")
            Dim cVatRate As Double = 0
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                cVatRate = Convert.ToDouble(oDbTbl.Rows(0)("FCVatRate"))
            End If

            'Read Text
            Dim tTxt As String = File.ReadAllText(tFullPath)
            tTxt = tTxt.Replace("&", "&amp;")
            tTxt = tTxt.Replace("'", """")
            Dim oaPdt As New List(Of cProduct)

            '------------------ Pong ---------------------------------
            ' Add ข้อมูลเข้า Array List  แยกแถว
            For Each tPdtRow As String In tTxt.Split(vbCr & vbLf)
                'Check เงื่อนไข รับ Record ที่เป็นบรรทัดเปล่า
                If tPdtRow = vbLf Then
                    Exit For
                Else
                    If Not String.IsNullOrEmpty(tPdtRow) Then
                        atPdtRow.Add(tPdtRow)
                    End If
                End If

            Next

            ' ค้นหาจำนวนแถวจาก FT เพื่อตรวจสอบว่า จำนวนแถว DT เท่ากับค่าที่ระบุแถวใน FT หรือไม่ ถ้าไม่เท่าจะไม่ทำงาน
            atFTData = atPdtRow(atPdtRow.Count - 1).Split("|")
            If atFTData(1) = Convert.ToString(atPdtRow.Count - 2) Then
                ' แยกข้อมูลแต่ละแถวเข้า Model โดยมีเงื่อนไขแถวแรก 01 
                For nRow As Integer = 0 To atPdtRow.Count - 1
                    tPdtDataRow = atPdtRow(nRow).Replace(vbLf, "")
                    tPdtPreFix = tPdtDataRow.Substring(0, 2)
                    If tPdtPreFix = "01" Then
                        Dim atPdtDataRow() As String = tPdtDataRow.Split("|")
                        ' Check ว่าเป็น Case Reject 
                        If atPdtDataRow(1) = "C" Then
                            ' เขียน log Reject
                            atDesc.Add("รหัสสินค้า " + atPdtDataRow(2) + vbTab + "เป็นสินค้าสถานะ Change (Failed)")
                            nFailed += 1
                            cCNVB.nVB_StaImportFailed = 1
                        Else
                            oSql.Clear()
                            oSql.AppendLine("SELECT FTPdtCode FROM TCNMPdt WHERE FTPdtCode='" + atPdtDataRow(2) + "'")
                            If oDatabase.C_CHKnDATHASRow(oSql.ToString()) = 1 Then
                                'เขียน log PdtCode Existing
                                atDesc.Add("รหัสสินค้า " + atPdtDataRow(2) + vbTab + "มีอยู่แล้วในระบบ (Failed)")
                                nFailed += 1
                                cCNVB.nVB_StaImportFailed = 1
                            Else

                                Dim nRowBarPdt = (From oBar In oLstPdtMaster Where oBar.FTPdtCode = atPdtDataRow(2)).Count

                                If nRowBarPdt = 0 Then
                                    Dim oPdtMaster As New cPdtMaster
                                    oPdtMaster.FTPdtCode = atPdtDataRow(2)
                                    oPdtMaster.FTPdtName = atPdtDataRow(4)
                                    oPdtMaster.FTPdtNameOth = atPdtDataRow(4)
                                    oPdtMaster.FTPdtBarCode1 = atPdtDataRow(2)
                                    oPdtMaster.FTPdtStkCode = atPdtDataRow(2)
                                    oPdtMaster.FCPdtStkFac = 1
                                    oPdtMaster.FTPdtStkControl = 1
                                    oPdtMaster.FCPdtQtyRet = 0
                                    oPdtMaster.FCPdtQtyWhs = 0
                                    oPdtMaster.FTPdtVatType = 1
                                    oPdtMaster.FTPdtSaleType = 1
                                    oPdtMaster.FTPdtSUnit = atPdtDataRow(3)
                                    oPdtMaster.FCPdtSFactor = 1
                                    oPdtMaster.FTPtyCode = atPdtDataRow(5)
                                    oPdtMaster.FTPdtPoint = 1
                                    oPdtMaster.FCPdtPointTime = 0
                                    oPdtMaster.FTPdtConType = 1
                                    oPdtMaster.FTPdtSrn = 2
                                    oPdtMaster.FTPdtAlwOrderS = 1
                                    oPdtMaster.FTPdtAlwOrderM = 1
                                    oPdtMaster.FTPdtAlwOrderL = 1
                                    oPdtMaster.FTPdtAlwBuyS = 1
                                    oPdtMaster.FTPdtAlwBuyM = 1
                                    oPdtMaster.FTPdtAlwBuyL = 1
                                    oPdtMaster.FCPdtRetPriS1 = 0
                                    oPdtMaster.FTPdtWhsDefUnit = 1
                                    oPdtMaster.FTPdtType = 1
                                    oPdtMaster.FTPdtStaSet = 1
                                    oPdtMaster.FTPdtStaSetPri = 1
                                    oPdtMaster.FTPdtStaActive = 1
                                    oPdtMaster.FTPbnCode = atPdtDataRow(6)
                                    oPdtMaster.FTPmoCode = atPdtDataRow(7)
                                    oPdtMaster.FTPdtTax = 1
                                    oPdtMaster.FTPdtGrpControl = 2
                                    oPdtMaster.FTPdtNoDis = 2
                                    oPdtMaster.FCPdtVatRate = cVatRate
                                    oPdtMaster.FTPdtStaAlwBuy = 1
                                    oPdtMaster.FDPdtOrdStart = Format(Now, "yyyy-MM-dd")
                                    oPdtMaster.FDPdtOrdStop = "9999-12-31"
                                    oPdtMaster.FDPdtSaleStart = Format(Now, "yyyy-MM-dd")
                                    oPdtMaster.FDPdtSaleStop = "9999-12-31"
                                    oPdtMaster.FTPdtStaTouch = 2
                                    oPdtMaster.FTPdtStaAlwRepack = 1
                                    oPdtMaster.FDDateUpd = Format(Now, "yyyy-MM-dd")
                                    oPdtMaster.FTTimeUpd = Format(Now, "HH:mm:ss")
                                    oPdtMaster.FTWhoUpd = "AdaLink"
                                    oPdtMaster.FDDateIns = Format(Now, "yyyy-MM-dd")
                                    oPdtMaster.FTTimeIns = Format(Now, "HH:mm:ss")
                                    oPdtMaster.FTWhoIns = "AdaLink"
                                    oLstPdtMaster.Add(oPdtMaster)
                                Else
                                    Dim oPdtDulicate As New cPdtCodeDulicate
                                    oPdtDulicate.FTPdtCode = atPdtDataRow(2)
                                    oPdtCodeDulicate.Add(oPdtDulicate)
                                    atDesc.Add("ระบุรหัสสินค้า " + atPdtDataRow(2) + vbTab + "มากกว่า 1 แถว  (Failed)")
                                    nFailed += 1
                                    cCNVB.nVB_StaImportFailed = 1
                                End If

                            End If
                        End If
                    End If
                Next

                ' ลบข้อมูล table ก่อน TLNKPdt
                oSql.Clear()
                oSql.AppendLine("DELETE FROM TLNKPdt")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))

                Dim oPdtDulicateGrp = From oBar In oPdtCodeDulicate Group oBar By oBar.FTPdtCode Into oGrpCode = Group


                If oPdtDulicateGrp.Count > 0 Then
                    For nRow As Integer = 0 To oLstPdtMaster.Count - 1

                        For nDulicate = 0 To oPdtDulicateGrp.Count - 1
                            If oPdtDulicateGrp(nDulicate).FTPdtCode = oLstPdtMaster(nRow).FTPdtCode Then
                                Dim oPdtDulicate As New cPdtCodeDulicate
                                oPdtDulicate.FTPdtCode = oLstPdtMaster(nRow).FTPdtCode
                                oPdtCodeDulicate.Add(oPdtDulicate)
                                atDesc.Add("ระบุรหัสสินค้า " + oLstPdtMaster(nRow).FTPdtCode + vbTab + "มากกว่า 1 แถว  (Failed)")
                                nFailed += 1
                                cCNVB.nVB_StaImportFailed = 1
                            Else
                                oSql.Clear()
                                oSql.AppendLine("INSERT INTO TLNKPdt(FTPdtCode,FTPdtName,FTPdtNameOth,FTPdtBarCode1")
                                oSql.AppendLine(",FTPdtStkCode,FCPdtStkFac,FTPdtStkControl,FCPdtQtyRet")
                                oSql.AppendLine(",FCPdtQtyWhs, FTPdtVatType, FTPdtSaleType,FTPdtSUnit")
                                oSql.AppendLine(",FCPdtSFactor,FTPtyCode, FTPdtPoint,FCPdtPointTime")
                                oSql.AppendLine(",FTPdtConType,FTPdtSrn,FTPdtAlwOrderS,FTPdtAlwOrderM")
                                oSql.AppendLine(",FTPdtAlwOrderL,FTPdtAlwBuyS,FTPdtAlwBuyM,FTPdtAlwBuyL")
                                oSql.AppendLine(",FCPdtRetPriS1,FTPdtWhsDefUnit,FTPdtType,FTPdtStaSet")
                                oSql.AppendLine(",FTPdtStaSetPri,FTPdtStaActive,FTPbnCode,FTPmoCode")
                                oSql.AppendLine(",FTPdtTax,FTPdtGrpControl,FTPdtNoDis,FCPdtVatRate")
                                oSql.AppendLine(",FTPdtStaAlwBuy,FDPdtOrdStart,FDPdtOrdStop,FDPdtSaleStart")
                                oSql.AppendLine(",FDPdtSaleStop,FTPdtStaTouch,FTPdtStaAlwRepack,FDDateUpd")
                                oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns,FNLnkLine) VALUES")
                                oSql.AppendLine("('" + oLstPdtMaster(nRow).FTPdtCode + "','" + oLstPdtMaster(nRow).FTPdtName + "','" + oLstPdtMaster(nRow).FTPdtNameOth + "','" + oLstPdtMaster(nRow).FTPdtBarCode1 + "'")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtStkCode + "'," + oLstPdtMaster(nRow).FCPdtStkFac + ",'" + oLstPdtMaster(nRow).FTPdtStkControl + "'," + oLstPdtMaster(nRow).FCPdtQtyRet + "")
                                oSql.AppendLine("," + oLstPdtMaster(nRow).FCPdtQtyWhs + ",'" + oLstPdtMaster(nRow).FTPdtVatType + "','" + oLstPdtMaster(nRow).FTPdtSaleType + "','" + oLstPdtMaster(nRow).FTPdtSUnit + "'")
                                oSql.AppendLine("," + oLstPdtMaster(nRow).FCPdtSFactor + ",'" + oLstPdtMaster(nRow).FTPtyCode + "','" + oLstPdtMaster(nRow).FTPdtPoint + "'," + oLstPdtMaster(nRow).FCPdtPointTime + "")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtConType + "','" + oLstPdtMaster(nRow).FTPdtSrn + "','" + oLstPdtMaster(nRow).FTPdtAlwOrderS + "','" + oLstPdtMaster(nRow).FTPdtAlwOrderM + "'")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtAlwOrderL + "','" + oLstPdtMaster(nRow).FTPdtAlwBuyS + "','" + oLstPdtMaster(nRow).FTPdtAlwBuyM + "','" + oLstPdtMaster(nRow).FTPdtAlwBuyL + "'")
                                oSql.AppendLine("," + oLstPdtMaster(nRow).FCPdtRetPriS1 + ",'" + oLstPdtMaster(nRow).FTPdtWhsDefUnit + "','" + oLstPdtMaster(nRow).FTPdtType + "','" + oLstPdtMaster(nRow).FTPdtStaSet + "'")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtStaSetPri + "','" + oLstPdtMaster(nRow).FTPdtStaActive + "','" + oLstPdtMaster(nRow).FTPbnCode + "','" + oLstPdtMaster(nRow).FTPmoCode + "'")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtTax + "','" + oLstPdtMaster(nRow).FTPdtGrpControl + "','" + oLstPdtMaster(nRow).FTPdtNoDis + "'," + oLstPdtMaster(nRow).FCPdtVatRate + "")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtStaAlwBuy + "','" + oLstPdtMaster(nRow).FDPdtOrdStart + "','" + oLstPdtMaster(nRow).FDPdtOrdStop + "','" + oLstPdtMaster(nRow).FDPdtSaleStart + "'")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FDPdtSaleStop + "','" + oLstPdtMaster(nRow).FTPdtStaTouch + "','" + oLstPdtMaster(nRow).FTPdtStaAlwRepack + "','" + oLstPdtMaster(nRow).FDDateUpd + "'")
                                oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTTimeUpd + "','" + oLstPdtMaster(nRow).FTWhoUpd + "','" + oLstPdtMaster(nRow).FDDateIns + "','" + oLstPdtMaster(nRow).FTTimeIns + "','" + oLstPdtMaster(nRow).FTWhoIns + "',1)")
                                If oSql.ToString <> "" Then
                                    nSuccess += 1
                                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                                End If
                                atDesc.Add("นำเข้ารหัสสินค้า " + oLstPdtMaster(nRow).FTPdtCode + vbTab + " สำเร็จ (Success)")
                            End If
                        Next
                    Next

                Else
                    For nRow As Integer = 0 To oLstPdtMaster.Count - 1
                        oSql.Clear()
                        oSql.AppendLine("INSERT INTO TLNKPdt(FTPdtCode,FTPdtName,FTPdtNameOth,FTPdtBarCode1")
                        oSql.AppendLine(",FTPdtStkCode,FCPdtStkFac,FTPdtStkControl,FCPdtQtyRet")
                        oSql.AppendLine(",FCPdtQtyWhs, FTPdtVatType, FTPdtSaleType,FTPdtSUnit")
                        oSql.AppendLine(",FCPdtSFactor,FTPtyCode, FTPdtPoint,FCPdtPointTime")
                        oSql.AppendLine(",FTPdtConType,FTPdtSrn,FTPdtAlwOrderS,FTPdtAlwOrderM")
                        oSql.AppendLine(",FTPdtAlwOrderL,FTPdtAlwBuyS,FTPdtAlwBuyM,FTPdtAlwBuyL")
                        oSql.AppendLine(",FCPdtRetPriS1,FTPdtWhsDefUnit,FTPdtType,FTPdtStaSet")
                        oSql.AppendLine(",FTPdtStaSetPri,FTPdtStaActive,FTPbnCode,FTPmoCode")
                        oSql.AppendLine(",FTPdtTax,FTPdtGrpControl,FTPdtNoDis,FCPdtVatRate")
                        oSql.AppendLine(",FTPdtStaAlwBuy,FDPdtOrdStart,FDPdtOrdStop,FDPdtSaleStart")
                        oSql.AppendLine(",FDPdtSaleStop,FTPdtStaTouch,FTPdtStaAlwRepack,FDDateUpd")
                        oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns,FNLnkLine) VALUES")
                        oSql.AppendLine("('" + oLstPdtMaster(nRow).FTPdtCode + "','" + oLstPdtMaster(nRow).FTPdtName + "','" + oLstPdtMaster(nRow).FTPdtNameOth + "','" + oLstPdtMaster(nRow).FTPdtBarCode1 + "'")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtStkCode + "'," + oLstPdtMaster(nRow).FCPdtStkFac + ",'" + oLstPdtMaster(nRow).FTPdtStkControl + "'," + oLstPdtMaster(nRow).FCPdtQtyRet + "")
                        oSql.AppendLine("," + oLstPdtMaster(nRow).FCPdtQtyWhs + ",'" + oLstPdtMaster(nRow).FTPdtVatType + "','" + oLstPdtMaster(nRow).FTPdtSaleType + "','" + oLstPdtMaster(nRow).FTPdtSUnit + "'")
                        oSql.AppendLine("," + oLstPdtMaster(nRow).FCPdtSFactor + ",'" + oLstPdtMaster(nRow).FTPtyCode + "','" + oLstPdtMaster(nRow).FTPdtPoint + "'," + oLstPdtMaster(nRow).FCPdtPointTime + "")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtConType + "','" + oLstPdtMaster(nRow).FTPdtSrn + "','" + oLstPdtMaster(nRow).FTPdtAlwOrderS + "','" + oLstPdtMaster(nRow).FTPdtAlwOrderM + "'")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtAlwOrderL + "','" + oLstPdtMaster(nRow).FTPdtAlwBuyS + "','" + oLstPdtMaster(nRow).FTPdtAlwBuyM + "','" + oLstPdtMaster(nRow).FTPdtAlwBuyL + "'")
                        oSql.AppendLine("," + oLstPdtMaster(nRow).FCPdtRetPriS1 + ",'" + oLstPdtMaster(nRow).FTPdtWhsDefUnit + "','" + oLstPdtMaster(nRow).FTPdtType + "','" + oLstPdtMaster(nRow).FTPdtStaSet + "'")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtStaSetPri + "','" + oLstPdtMaster(nRow).FTPdtStaActive + "','" + oLstPdtMaster(nRow).FTPbnCode + "','" + oLstPdtMaster(nRow).FTPmoCode + "'")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtTax + "','" + oLstPdtMaster(nRow).FTPdtGrpControl + "','" + oLstPdtMaster(nRow).FTPdtNoDis + "'," + oLstPdtMaster(nRow).FCPdtVatRate + "")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTPdtStaAlwBuy + "','" + oLstPdtMaster(nRow).FDPdtOrdStart + "','" + oLstPdtMaster(nRow).FDPdtOrdStop + "','" + oLstPdtMaster(nRow).FDPdtSaleStart + "'")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FDPdtSaleStop + "','" + oLstPdtMaster(nRow).FTPdtStaTouch + "','" + oLstPdtMaster(nRow).FTPdtStaAlwRepack + "','" + oLstPdtMaster(nRow).FDDateUpd + "'")
                        oSql.AppendLine(",'" + oLstPdtMaster(nRow).FTTimeUpd + "','" + oLstPdtMaster(nRow).FTWhoUpd + "','" + oLstPdtMaster(nRow).FDDateIns + "','" + oLstPdtMaster(nRow).FTTimeIns + "','" + oLstPdtMaster(nRow).FTWhoIns + "',1)")
                        If oSql.ToString <> "" Then
                            nSuccess += 1
                            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                        End If
                        atDesc.Add("นำเข้ารหัสสินค้า " + oLstPdtMaster(nRow).FTPdtCode + vbTab + " สำเร็จ (Success)")
                    Next
                End If

                bStaPrc = True
            Else
                'เขียน log จำนวนแถว DT ไม่เท่ากับค่าที่ระบุใน FT
                atDesc.Add("จำนวนแถว DT ไม่เท่ากับค่าที่ระบุใน FT")
                cCNVB.nVB_StaImportFailed = 1
                nFailed += 1
            End If

            'Save TLNKLog
            oTCNMPdt = New cJobTCNMPdt
            oTCNMPdt.C_CALxWriteLogEr(atDesc, tFullPath, nSuccess, nFailed)
            Return bStaPrc
        Catch ex As Exception
            Return False
            atDesc.Add(ex.Message)
            oTCNMPdt = New cJobTCNMPdt
            oTCNMPdt.C_CALxWriteLogEr(atDesc, tFullPath, nSuccess, nFailed)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
            tMsg = Nothing
            bStaPrc = Nothing
            oLstPdtMaster = Nothing
            atPdtRow = Nothing
            tPdtDataRow = Nothing
            tPdtPreFix = Nothing
            tPathLog = Nothing
            nSuccess = Nothing
            nFailed = Nothing
            tDateTimeNow = Nothing
            tDescription = Nothing
        End Try

    End Function


    ''' <summary>
    ''' เตรียมข้อมูล Tnf เข้า Temp
    ''' </summary>
    ''' <returns></returns>
    Private Function C_CALbTnfFile() As Boolean
        'เตรียมข้อมูล เพื่อสร้างไฟล์ Temp
        Dim oDbTbl As New DataTable
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim tMsg As String = ""
        Dim bStaPrc As Boolean = True
        Dim oLstTCNTPdtTnfDT As New List(Of cTCNTPdtTnfDT)
        Dim oLstTCNTpdtTnfHD As New List(Of cTCNTPdtTnfHD)
        Dim atTnfRow As New ArrayList
        Dim tTnfDataRow As String = String.Empty
        Dim tTnfPreFix As String = String.Empty
        Dim tPathLog As String = AdaConfig.cConfig.oConfigXml.tLog  ' Pong Path Log
        Dim nSuccess As Integer = 0
        Dim nFailed As Integer = 0
        Dim tDateTimeNow As String = Format(Now, "yyyy-MM-dd HH:mm:ss.fff")
        Dim tDesc As String = String.Empty
        Dim tFileName As String = ""
        Dim tDocNo As String = ""
        Dim nHasRowDoc As Integer
        Dim atFTData() As String
        Dim atDesc As New ArrayList
        Dim oTCNTPdtTnfHD As New cTCNTPdtTnfHD
        Dim tSuplier As String = AdaConfig.cConfig.oUrsDefault.tSupplier
        Dim tReason As String = AdaConfig.cConfig.oUrsDefault.tReason
        Dim oDbTblPdt As New DataTable()

        Try
            cCNVB.tVB_FTPthDocNo = "" 'Reset Doc No Tnf In

            'Get Vat company
            oSql.AppendLine("SELECT ISNULL(FCVatRate,0)FCVatRate FROM TCNMVatRate WHERE FTVatCode IN (SELECT TOP 1 FTVatCode FROM TCNMComp)")
            Dim cVatRate As Double = 0
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                cVatRate = Convert.ToDouble(oDbTbl.Rows(0)("FCVatRate"))
            End If

            Dim tTxt As String = File.ReadAllText(tFullPath)
            tTxt = tTxt.Replace("&", "&amp;")
            tTxt = tTxt.Replace("'", """")

            ' Add ข้อมูลเข้า Array List  แยกแถว
            For Each tTnfRow As String In tTxt.Split(vbCr & vbLf)
                'Check เงื่อนไข รับ Record ที่เป็นบรรทัดเปล่า
                If tTnfRow = vbLf Then
                    Exit For
                Else
                    If Not String.IsNullOrEmpty(tTnfRow) Then
                        atTnfRow.Add(tTnfRow)
                    End If
                End If

            Next

            ' ค้นหาจำนวนแถวจาก FT เพื่อตรวจสอบว่า จำนวนแถว DT เท่ากับค่าที่ระบุแถวใน FT หรือไม่ ถ้าไม่เท่าจะไม่ทำงาน
            atFTData = atTnfRow(atTnfRow.Count - 1).Split("|")
            If atFTData(1) = Convert.ToString(atTnfRow.Count - 2) Then

                For nRow As Integer = 0 To atTnfRow.Count - 1

                    tTnfDataRow = atTnfRow(nRow).Replace(vbLf, "")
                    tTnfPreFix = tTnfDataRow.Substring(0, 2)
                    Dim atTnfDataRow() As String = tTnfDataRow.Split("|")

                    If tTnfPreFix = "01" Then
                        ' เตรียมข้อมูลเพื่อ Import HD แค่ Row เดียว
                        ' If nRow = 1 Then
                        oTCNTPdtTnfHD = New cTCNTPdtTnfHD
                        oTCNTPdtTnfHD.FTBchCode = "000"
                        oTCNTPdtTnfHD.FTPthDocNo = atTnfDataRow(2)
                        cCNVB.tVB_FTPthDocNo = atTnfDataRow(2)
                        oTCNTPdtTnfHD.FTPthDocType = "1"
                        oTCNTPdtTnfHD.FDPthDocDate = atTnfDataRow(5).Substring(0, 4) + "-" + atTnfDataRow(5).Substring(4, 2) + "-" + atTnfDataRow(5).Substring(6, 2)
                        oTCNTPdtTnfHD.FTPthDocTime = Format(Now, "HH:mm:ss")
                        oTCNTPdtTnfHD.FTPthVATInOrEx = "1"
                        oTCNTPdtTnfHD.FTDptCode = "001"
                        oTCNTPdtTnfHD.FTDepName = "Administrator"
                        oTCNTPdtTnfHD.FTUsrCode = "009"
                        oTCNTPdtTnfHD.FTUsrName = "Manager"
                        ' oTCNTPdtTnfHD.FTSplCode = "0"
                        oTCNTPdtTnfHD.FTSplCode = tSuplier
                        oTCNTPdtTnfHD.FTTrnCode = tReason
                        oTCNTPdtTnfHD.FTPthWhTo = "001"
                        oTCNTPdtTnfHD.FTPthType = "1"
                        oTCNTPdtTnfHD.FTWahCode = "001"
                        oTCNTPdtTnfHD.FTPthApvCode = "009"
                        oTCNTPdtTnfHD.FDPthTnfDate = atTnfDataRow(11).Substring(0, 4) + "-" + atTnfDataRow(11).Substring(4, 2) + "-" + atTnfDataRow(11).Substring(6, 2)
                        oTCNTPdtTnfHD.FNPthDocPrint = 0
                        oTCNTPdtTnfHD.FCPthVATRate = cVatRate
                        oTCNTPdtTnfHD.FCPthNonVat = 0
                        oTCNTPdtTnfHD.FCPthDis = 0
                        oTCNTPdtTnfHD.FCPthChg = 0
                        oTCNTPdtTnfHD.FTPthStaPaid = "1"
                        oTCNTPdtTnfHD.FTPthStaRefund = "1"
                        oTCNTPdtTnfHD.FTPthStaType = "1"
                        oTCNTPdtTnfHD.FTPthStaDoc = "1"
                        oTCNTPdtTnfHD.FTPthStaPrcDoc = "1"
                        oTCNTPdtTnfHD.FNPthSign = 0
                        oTCNTPdtTnfHD.FTPthCshOrCrd = "1"
                        oTCNTPdtTnfHD.FCPthPaid = 0
                        oTCNTPdtTnfHD.FTPthDstPaid = "1"
                        oTCNTPdtTnfHD.FNPthStaDocAct = "1"
                        oTCNTPdtTnfHD.FNPthStaRef = 0
                        oTCNTPdtTnfHD.FTPthStaVatSend = "1"
                        oTCNTPdtTnfHD.FDDateUpd = Format(Now, "yyyy-MM-dd")
                        oTCNTPdtTnfHD.FTTimeUpd = Format(Now, "HH:mm:ss")
                        oTCNTPdtTnfHD.FTWhoUpd = "AdaLink"
                        oTCNTPdtTnfHD.FDDateIns = Format(Now, "yyyy-MM-dd")
                        oTCNTPdtTnfHD.FTTimeIns = Format(Now, "HH:mm:ss")
                        oTCNTPdtTnfHD.FTWhoIns = "AdaLink"
                        oLstTCNTpdtTnfHD.Add(oTCNTPdtTnfHD)
                        '     End If

                        ' เป็น case CREATE ถึงนำเข้า
                        If atTnfDataRow(1) = "N" Then

                            ' ตรวจสอบสินค้ามีในระบบหรือไม่
                            oSql.Clear()
                            oSql.AppendLine("SELECT FTPdtCode FROM TCNMPdt WHERE FTPdtCode='" + atTnfDataRow(8) + "'")
                            oDbTblPdt = oDatabase.C_GEToTblSQL(oSql.ToString())
                            'If oDatabase.C_CHKnDATHASRow(oSql.ToString()) = 0 Then

                            '    ' Case ไม่มีข้อมูลสินค้าในระบบ
                            '    atDesc.Add("ไม่มีข้อมูลสินค้า " + atTnfDataRow(8) + vbTab + "อยู่ในระบบ  (Failed)")
                            '    nFailed += 1
                            '    cCNVB.nVB_StaImportFailed = 1
                            If oDbTblPdt.Rows.Count = 0 Then

                                ' Case ไม่มีข้อมูลสินค้าในระบบ
                                atDesc.Add("ไม่มีข้อมูลสินค้า " + atTnfDataRow(8) + vbTab + "อยู่ในระบบ  (Failed)")
                                nFailed += 1
                                cCNVB.nVB_StaImportFailed = 1

                            Else
                                oSql.Clear()
                                oSql.AppendLine("SELECT FTPdtname FROM TCNMPdt WHERE FTPdtCode='" + atTnfDataRow(8) + "'")
                                Dim atPdtName() As String = oDatabase.C_GETtSQLExecuteScalar(oSql.ToString(), "t").Split("|")

                                If atPdtName(0) = "1" Then

                                    Dim oTCNTPdtTnfDT As New cTCNTPdtTnfDT
                                    oTCNTPdtTnfDT.FTBchCode = "000"
                                    oTCNTPdtTnfDT.FTPthDocNo = atTnfDataRow(2)
                                    oTCNTPdtTnfDT.FNPtdSeqNo = Convert.ToInt32(atTnfDataRow(7))
                                    oTCNTPdtTnfDT.FTPdtCode = atTnfDataRow(8)
                                    oTCNTPdtTnfDT.FTPdtName = atPdtName(1)
                                    oTCNTPdtTnfDT.FTPthDocType = "1"
                                    oTCNTPdtTnfDT.FDPthDocDate = atTnfDataRow(5).Substring(0, 4) + "-" + atTnfDataRow(5).Substring(4, 2) + "-" + atTnfDataRow(5).Substring(6, 2)
                                    oTCNTPdtTnfDT.FTPthVATInOrEx = "1"
                                    oTCNTPdtTnfDT.FTPtdBarCode = atTnfDataRow(8)
                                    oTCNTPdtTnfDT.FTPtdStkCode = atTnfDataRow(8)
                                    oTCNTPdtTnfDT.FCPtdStkFac = 1
                                    oTCNTPdtTnfDT.FTPtdVatType = "1"
                                    oTCNTPdtTnfDT.FTPtdSaleType = "1"
                                    oTCNTPdtTnfDT.FTPunCode = atTnfDataRow(10)
                                    oTCNTPdtTnfDT.FTPtdUnitName = C_GETtUnit(atTnfDataRow(10))
                                    oTCNTPdtTnfDT.FCPtdQty = atTnfDataRow(9)  ''''
                                    ' oTCNTPdtTnfDT.FCPtdFactor = 1
                                    oTCNTPdtTnfDT.FCPtdFactor = C_GETtFactor(atTnfDataRow(8), atTnfDataRow(10))
                                    oTCNTPdtTnfDT.FNPtdPdtLevel = 0
                                    oTCNTPdtTnfDT.FCPtdQtyAll = atTnfDataRow(9)
                                    oTCNTPdtTnfDT.FTPtdStaPdt = "1"
                                    oTCNTPdtTnfDT.FTPtdStaRfd = "1"
                                    oTCNTPdtTnfDT.FTPtdStaPrcStk = "1"
                                    oTCNTPdtTnfDT.FTPtdStaPrcStkCrd = "1"
                                    oTCNTPdtTnfDT.FNPthSign = 1
                                    oTCNTPdtTnfDT.FTWahCode = "001"
                                    oTCNTPdtTnfDT.FNPtdStaRef = 0
                                    oTCNTPdtTnfDT.FTPthWhTo = "001"
                                    oTCNTPdtTnfDT.FTPthStaVatSend = "1"
                                    oTCNTPdtTnfDT.FDDateUpd = Format(Now, "yyyy-MM-dd")
                                    oTCNTPdtTnfDT.FTTimeUpd = Format(Now, "HH:mm:ss")
                                    oTCNTPdtTnfDT.FTWhoUpd = "AdaLink"
                                    oTCNTPdtTnfDT.FDDateIns = Format(Now, "yyyy-MM-dd")
                                    oTCNTPdtTnfDT.FTTimeIns = Format(Now, "HH:mm:ss")
                                    oTCNTPdtTnfDT.FTWhoIns = "AdaLink"
                                    oLstTCNTPdtTnfDT.Add(oTCNTPdtTnfDT)
                                End If
                            End If

                        Else
                            atDesc.Add("ข้อมูลสินค้า " + atTnfDataRow(8) + " ไม่อนุญาติให้นำเข้าระบบ (Failed)")
                            nFailed += 1
                            cCNVB.nVB_StaImportFailed = 1
                        End If

                    End If
                Next


                ' ลบข้อมูล table Temp HD

                oSql.Clear()
                oSql.AppendLine("DELETE FROM TLNKPdtTnfHD")
                If oSql.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                End If

                ' ลบข้อมูล table Temp DT
                oSql.Clear()
                oSql.AppendLine("DELETE FROM TLNKPdtTnfDT")
                If oSql.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                End If


                Dim oDocNO = From oTnf In oLstTCNTpdtTnfHD Group oTnf By oTnf.FTPthDocNo Into oTnf = Group
                Dim atDocHDGrp As New ArrayList
                Dim atDocHDimport As New ArrayList
                For nRowDoc = 0 To oDocNO.Count - 1

                    ' เช็ค Doc No มีอยู่ในระบบแล้วหรือยัง ถ้ามีแล้วไม่นำเข้า
                    Dim tHDocNo As String = oDocNO(nRowDoc).FTPthDocNo
                    atDocHDGrp.Add(tHDocNo)  ' เก็บเลขที่เอกสารที่ Group แล้ว

                    oSql.Clear()
                    oSql.AppendLine("SELECT FTPthDocNo FROM TCNTPdtTnfHD WHERE FTPthDocNo='" + tHDocNo + "'")
                    nHasRowDoc = oDatabase.C_CHKnDATHASRow(oSql.ToString())
                    If nHasRowDoc = 1 Then
                        'เขียน Log เลขที่เอกสารมีอยู่แล้วในระบบ
                        atDesc.Add("เลขที่เอกสารนี้ " + tHDocNo + vbTab + "มีอยู่แล้วในระบบ (Failed)")
                        nFailed += 1
                        cCNVB.nVB_StaImportFailed = 1

                    Else
                        'Save HD ลง Temp
                        atDocHDimport.Add(tHDocNo)
                        Dim oHD = (From HD In oLstTCNTpdtTnfHD Where HD.FTPthDocNo = tHDocNo Select HD).FirstOrDefault
                        oSql.Clear()
                        oSql.AppendLine("  INSERT INTO TLNKPdtTnfHD(FTBchCode, FTPthDocNo, FTPthDocType, FDPthDocDate")
                        oSql.AppendLine(", FTPthDocTime, FTPthVATInOrEx, FTDptCode, FTDepName")
                        oSql.AppendLine(", FTUsrCode, FTUsrName, FTTrnCode,FTSplCode, FTPthWhTo")
                        oSql.AppendLine(", FTPthType, FTWahCode, FTPthApvCode, FDPthTnfDate")
                        oSql.AppendLine(", FNPthDocPrint, FCPthVATRate, FCPthNonVat, FCPthDis")
                        oSql.AppendLine(", FCPthChg, FTPthStaPaid, FTPthStaRefund")
                        oSql.AppendLine(", FTPthStaType, FTPthStaDoc, FTPthStaPrcDoc")
                        oSql.AppendLine(", FNPthSign, FTPthCshOrCrd, FCPthPaid, FTPthDstPaid")
                        oSql.AppendLine(", FNPthStaDocAct, FNPthStaRef, FTPthStaVatSend, FDDateUpd")
                        oSql.AppendLine(", FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns) VALUES")
                        oSql.AppendLine("('" + oHD.FTBchCode + "'")
                        oSql.AppendLine(",'" + oHD.FTPthDocNo + "'")
                        oSql.AppendLine(",'" + oHD.FTPthDocType + "'")
                        oSql.AppendLine(",'" + oHD.FDPthDocDate + "'")
                        oSql.AppendLine(",'" + oHD.FTPthDocTime + "'")
                        oSql.AppendLine(",'" + oHD.FTPthVATInOrEx + "'")
                        oSql.AppendLine(",'" + oHD.FTDptCode + "'")
                        oSql.AppendLine(",'" + oHD.FTDepName + "'")
                        oSql.AppendLine(",'" + oHD.FTUsrCode + "'")
                        oSql.AppendLine(",'" + oHD.FTUsrName + "'")
                        oSql.AppendLine(",'" + oHD.FTTrnCode + "'")
                        oSql.AppendLine(",'" + oHD.FTSplCode + "'")
                        oSql.AppendLine(",'" + oHD.FTPthWhTo + "'")
                        oSql.AppendLine(",'" + oHD.FTPthType + "'")
                        oSql.AppendLine(",'" + oHD.FTWahCode + "'")
                        oSql.AppendLine(",'" + oHD.FTPthApvCode + "'")
                        oSql.AppendLine(",'" + oHD.FDPthTnfDate + "'")
                        oSql.AppendLine("," + Convert.ToString(oHD.FNPthDocPrint) + "")
                        oSql.AppendLine(",'" + Convert.ToString(oHD.FCPthVATRate) + "'")
                        oSql.AppendLine(",'" + Convert.ToString(oHD.FCPthNonVat) + "'")
                        oSql.AppendLine(",'" + Convert.ToString(oHD.FCPthDis) + "'")
                        oSql.AppendLine(",'" + Convert.ToString(oHD.FCPthChg) + "'")
                        oSql.AppendLine(",'" + oHD.FTPthStaPaid + "'")
                        oSql.AppendLine(",'" + oHD.FTPthStaRefund + "'")
                        oSql.AppendLine(",'" + oHD.FTPthStaType + "'")
                        oSql.AppendLine(",'" + oHD.FTPthStaDoc + "'")
                        oSql.AppendLine(",'" + oHD.FTPthStaPrcDoc + "'")
                        oSql.AppendLine("," + Convert.ToString(oHD.FNPthSign) + "")
                        oSql.AppendLine(",'" + oHD.FTPthCshOrCrd + "'")
                        oSql.AppendLine(",'" + Convert.ToString(oHD.FCPthPaid) + "'")
                        oSql.AppendLine(",'" + oHD.FTPthDstPaid + "'")
                        oSql.AppendLine(",'" + Convert.ToString(oHD.FNPthStaDocAct) + "'")
                        oSql.AppendLine("," + Convert.ToString(oHD.FNPthStaRef) + "")
                        oSql.AppendLine(",'" + oHD.FTPthStaVatSend + "'")
                        oSql.AppendLine(",'" + oHD.FDDateUpd + "'")
                        oSql.AppendLine(",'" + oHD.FTTimeUpd + "'")
                        oSql.AppendLine(",'" + oHD.FTWhoUpd + "'")
                        oSql.AppendLine(",'" + oHD.FDDateIns + "'")
                        oSql.AppendLine(",'" + oHD.FTTimeIns + "'")
                        oSql.AppendLine(",'" + oHD.FTWhoIns + "')")
                        If oSql.ToString <> "" Then
                            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                        End If

                    End If
                Next

                If atDocHDimport.Count > 0 Then
                    For nRowHDGrp As Integer = 0 To atDocHDimport.Count - 1
                        Dim oDocDT = From DT In oLstTCNTPdtTnfDT Where DT.FTPthDocNo = atDocHDimport(nRowHDGrp) Select DT
                        ' 'Save DT ลง Temp

                        For nRowTnfDT As Integer = 0 To oDocDT.Count - 1
                            oSql.Clear()
                            oSql.AppendLine(" INSERT INTO TLNKPdtTnfDT(FTBchCode,FTPthDocNo ,FNPtdSeqNo")
                            oSql.AppendLine(" ,FTPdtCode,FTPdtName,FTPthDocType")
                            oSql.AppendLine(" ,FDPthDocDate ,FTPthVATInOrEx,FTPtdBarCode")
                            oSql.AppendLine(" ,FTPtdStkCode,FCPtdStkFac,FTPtdVatType")
                            oSql.AppendLine(" ,FTPtdSaleType,FTPunCode,FCPtdQtyAll")
                            oSql.AppendLine(" ,FTPtdStaPdt,FTPtdStaRfd ,FTPtdStaPrcStk,FTPtdStaPrcStkCrd")
                            oSql.AppendLine(" ,FNPthSign,FTWahCode,FNPtdStaRef")
                            oSql.AppendLine(" ,FTPthWhTo,FTPthStaVatSend,FDDateUpd")
                            oSql.AppendLine(" ,FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FCPtdQty,FNPtdPdtLevel,FTWhoIns,FCPtdFactor,FTPtdUnitName) VALUES")
                            oSql.AppendLine("('" + oDocDT(nRowTnfDT).FTBchCode + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPthDocNo + "'")
                            oSql.AppendLine("," + Convert.ToString(oDocDT(nRowTnfDT).FNPtdSeqNo) + "")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPdtCode + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPdtName + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPthDocType + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FDPthDocDate + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPthVATInOrEx + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdBarCode + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdStkCode + "'")
                            oSql.AppendLine(",'" + Convert.ToString(oDocDT(nRowTnfDT).FCPtdStkFac) + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdVatType + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdSaleType + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPunCode + "'")
                            oSql.AppendLine(",'" + Convert.ToString(oDocDT(nRowTnfDT).FCPtdQtyAll) + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdStaPdt + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdStaRfd + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdStaPrcStk + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPtdStaPrcStkCrd + "'")
                            oSql.AppendLine("," + Convert.ToString(oDocDT(nRowTnfDT).FNPthSign) + "")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTWahCode + "'")
                            oSql.AppendLine("," + Convert.ToString(oDocDT(nRowTnfDT).FNPtdStaRef) + "")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPthWhTo + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTPthStaVatSend + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FDDateUpd + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTTimeUpd + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTWhoUpd + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FDDateIns + "'")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTTimeIns + "'")
                            oSql.AppendLine("," + Convert.ToString(oDocDT(nRowTnfDT).FCPtdQty) + "")
                            'oSql.AppendLine("," + Convert.ToString(oLstTCNTPdtTnfDT(nRowTnfDT).FCPtdFactor) + "")
                            oSql.AppendLine("," + Convert.ToString(oDocDT(nRowTnfDT).FNPtdPdtLevel) + "")
                            oSql.AppendLine(",'" + oDocDT(nRowTnfDT).FTWhoIns + "'")
                            oSql.AppendLine(",'" + Convert.ToString(oDocDT(nRowTnfDT).FCPtdFactor) + "'")
                            oSql.AppendLine(" , '" + oDocDT(nRowTnfDT).FTPtdUnitName + "')")

                            If oSql.ToString <> "" Then
                                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                            End If
                        Next
                        atDesc.Add("นำเข้าเลขที่เอกสาร " + atDocHDGrp(nRowHDGrp) + vbTab + "สำเร็จ (Success)")
                        nSuccess += 1


                    Next

                End If

                bStaPrc = True
            Else
                'เขียน log จำนวนแถว DT ไม่เท่ากับค่าที่ระบุใน FT
                cCNVB.nVB_StaImportFailed = 1
                atDesc.Add("จำนวนแถว DT ไม่เท่ากับค่าที่ระบุใน FT (Failed)")
                nFailed = 1
            End If

            oTCNTPdtTnf = New cJobTCNTPdtTnf
            oTCNTPdtTnf.C_CALxWriteLogEr(atDesc, tFullPath, nSuccess, nFailed)
            Return bStaPrc
        Catch ex As Exception
            Return False
            atDesc.Add(ex.Message)
            oTCNTPdtTnf = New cJobTCNTPdtTnf
            oTCNTPdtTnf.C_CALxWriteLogEr(atDesc, tFullPath, nSuccess, nFailed)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
            tMsg = Nothing
            bStaPrc = Nothing
            oLstTCNTPdtTnfDT = Nothing
            atTnfRow = Nothing
            tTnfDataRow = Nothing
            tTnfPreFix = Nothing
            tPathLog = Nothing
            nSuccess = Nothing
            nFailed = Nothing
            tDateTimeNow = Nothing
            tDesc = Nothing
            tFileName = Nothing
            tDocNo = Nothing
            oTCNTPdtTnfHD = Nothing
            nHasRowDoc = Nothing
        End Try

    End Function

    ''' <summary>
    ''' เตรียมข้อมูล Ajp เข้า Temp
    ''' </summary>
    ''' <returns></returns>
    Private Function C_CALbAjpFile() As Boolean
        'เตรียมข้อมูล เพื่อสร้างไฟล์ Temp
        Dim oDbTbl As New DataTable
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim tMsg As String = ""
        Dim bStaPrc As Boolean = True
        Dim oLstTCNTPdtAjpHD As New List(Of cTCNTPdtAjpHD)
        Dim oLstTCNTPdtAjpDT As New List(Of cTCNTPdtAjpDT)
        Dim atAjpRow As New ArrayList
        Dim tAjpDataRow As String = String.Empty
        Dim tAjpPreFix As String = String.Empty
        Dim tPathLog As String = AdaConfig.cConfig.oConfigXml.tLog  ' Pong Path Log
        Dim nSuccess As Integer = 0
        Dim nFailed As Integer = 0
        Dim tDateTimeNow As String = Format(Now, "yyyy-MM-dd HH:mm:ss.fff")
        Dim tDesc As String = String.Empty
        Dim tFileName As String = ""
        Dim tDocNo As String = ""
        Dim atFTData() As String
        Dim atDesc As New ArrayList
        Dim atBarDulicate As New ArrayList
        Dim oPdtCodeDulicate As New List(Of cPdtCodeDulicate)
        Dim nChkRowDT As Integer = 0
        Dim oTCNTPdtAjpHD As cTCNTPdtAjpHD
        Dim oTCNTPdtAjpDT As cTCNTPdtAjpDT
        Try
            'Get Vat company
            oSql.AppendLine("SELECT ISNULL(FCVatRate,0)FCVatRate FROM TCNMVatRate WHERE FTVatCode IN (SELECT TOP 1 FTVatCode FROM TCNMComp)")
            Dim cVatRate As Double = 0
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                cVatRate = Convert.ToDouble(oDbTbl.Rows(0)("FCVatRate"))
            End If

            ' ลบ LockDoc
            oSql.Clear()
            oSql.AppendLine("DELETE FROM TCNTLockDoc")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If

            'Read Text
            Dim tTxt As String = File.ReadAllText(tFullPath)
            tTxt = tTxt.Replace("&", "&amp;")
            tTxt = tTxt.Replace("'", """")
            'Dim oaPdt As New List(Of cProduct)

            ' Add ข้อมูลเข้า Array List  แยกแถว
            For Each tAjpRow As String In tTxt.Split(vbCr & vbLf)
                If tAjpRow = vbLf Then
                    Exit For
                Else
                    If Not String.IsNullOrEmpty(tAjpRow) Then
                        atAjpRow.Add(tAjpRow)
                    End If
                End If

            Next

            ' ค้นหาจำนวนแถวจาก FT เพื่อตรวจสอบว่า จำนวนแถว DT เท่ากับค่าที่ระบุแถวใน FT หรือไม่ ถ้าไม่เท่าจะไม่ทำงาน
            atFTData = atAjpRow(atAjpRow.Count - 1).Split("|")
            If atFTData(1) = Convert.ToString(atAjpRow.Count - 2) Then
                ' แยกข้อมูลแต่ละแถวเข้า Model โดยมีเงื่อนไขขึ้นต้น 01 
                For nRow As Integer = 0 To atAjpRow.Count - 1
                    tAjpDataRow = atAjpRow(nRow).Replace(vbLf, "")
                    tAjpPreFix = tAjpDataRow.Substring(0, 2)
                    If tAjpPreFix = "01" Then
                        Dim atAjpDataRow() As String = tAjpDataRow.Split("|")
                        oTCNTPdtAjpHD = New cTCNTPdtAjpHD
                        oTCNTPdtAjpHD.FTIphDocNo = ""
                        oTCNTPdtAjpHD.FTIphDocType = "1"
                        oTCNTPdtAjpHD.FDIphDocDate = Format(Now, "yyyy-MM-dd")
                        oTCNTPdtAjpHD.FTIphDocTime = Format(Now, "HH:mm:ss")
                        oTCNTPdtAjpHD.FTBchCode = "000"
                        oTCNTPdtAjpHD.FTDptCode = "001"
                        oTCNTPdtAjpHD.FTUsrCode = "009"
                        oTCNTPdtAjpHD.FTIphApvCode = "009"
                        oTCNTPdtAjpHD.FCIphTotal = 0.00
                        oTCNTPdtAjpHD.FTIphStaDoc = "1"
                        oTCNTPdtAjpHD.FTIphStaPrcDoc = "1"
                        oTCNTPdtAjpHD.FNIphStaDocAct = "1"
                        oTCNTPdtAjpHD.FTIphAdjType = "1"
                        '   oTCNTPdtAjpHD.FDIphAffect = atAjpDataRow(4).Substring(0, 4) + "-" + atAjpDataRow(4).Substring(4, 2) + "-" + atAjpDataRow(4).Substring(6, 2)
                        oTCNTPdtAjpHD.FDIphAffect = Format(Now, "yyyy-MM-dd")
                        oTCNTPdtAjpHD.FDIphDStop = "9999-12-31"
                        oTCNTPdtAjpHD.FTIphBchTo = "000"
                        oTCNTPdtAjpHD.FTIphPriType = "1"
                        oTCNTPdtAjpHD.FDDateUpd = Format(Now, "yyyy-MM-dd")
                        oTCNTPdtAjpHD.FTTimeUpd = Format(Now, "HH:mm:ss")
                        oTCNTPdtAjpHD.FTWhoUpd = "AdaLink"
                        oTCNTPdtAjpHD.FDDateIns = Format(Now, "yyyy-MM-dd")
                        oTCNTPdtAjpHD.FTTimeIns = Format(Now, "HH:mm:ss")
                        oTCNTPdtAjpHD.FTWhoIns = "AdaLink"
                        oLstTCNTPdtAjpHD.Add(oTCNTPdtAjpHD)

                        Dim nRowBarPdt = (From oBar In oLstTCNTPdtAjpDT Where oBar.FTPdtCode = atAjpDataRow(1)).Count
                        If nRowBarPdt = 0 Then
                            oTCNTPdtAjpDT = New cTCNTPdtAjpDT
                            oTCNTPdtAjpDT.FTIphDocNo = ""
                            oTCNTPdtAjpDT.FNIpdSeqNo = 0
                            oTCNTPdtAjpDT.FTPdtCode = atAjpDataRow(1)
                            oTCNTPdtAjpDT.FTPdtName = ""
                            oTCNTPdtAjpDT.FTIphDocType = "1"
                            oTCNTPdtAjpDT.FDIphDocDate = Format(Now, "yyyy-MM-dd")
                            oTCNTPdtAjpDT.FTBchCode = "000"
                            oTCNTPdtAjpDT.FTIpdBarCode = ""
                            oTCNTPdtAjpDT.FTIpdStkCode = ""
                            oTCNTPdtAjpDT.FCIpdStkFac = "1"
                            oTCNTPdtAjpDT.FTPunCode = atAjpDataRow(3)
                            oTCNTPdtAjpDT.FCIpdFactor = 1
                            oTCNTPdtAjpDT.FNIpdAdjLev = 1
                            oTCNTPdtAjpDT.FCIpdPriOld = 0
                            oTCNTPdtAjpDT.FCIpdPriNew = atAjpDataRow(2)
                            oTCNTPdtAjpDT.FNIpdUnitType = 1
                            oTCNTPdtAjpDT.FTIpdBchTo = "000"
                            oTCNTPdtAjpDT.FDDateUpd = Format(Now, "yyyy-MM-dd")
                            oTCNTPdtAjpDT.FTTimeUpd = Format(Now, "HH:mm:ss")
                            oTCNTPdtAjpDT.FTWhoUpd = "AdaLink"
                            oTCNTPdtAjpDT.FDDateIns = Format(Now, "yyyy-MM-dd")
                            oTCNTPdtAjpDT.FTTimeIns = Format(Now, "HH:mm:ss")
                            oTCNTPdtAjpDT.FTWhoIns = "AdaLink"
                            '        oTCNTPdtAjpDT.FDIphAffect = atAjpDataRow(4).Substring(0, 4) + "-" + atAjpDataRow(4).Substring(4, 2) + "-" + atAjpDataRow(4).Substring(6, 2)
                            oTCNTPdtAjpDT.FDIphAffect = Format(Now, "yyyy-MM-dd")
                            oLstTCNTPdtAjpDT.Add(oTCNTPdtAjpDT)
                        Else
                            Dim oPdtDulicate As New cPdtCodeDulicate
                            oPdtDulicate.FTPdtCode = atAjpDataRow(1)
                            oPdtCodeDulicate.Add(oPdtDulicate)
                            atDesc.Add("ระบุรหัสสินค้า " + atAjpDataRow(1) + vbTab + "มากกว่า 1 แถว  (Failed)")
                            nFailed += 1
                            cCNVB.nVB_StaImportFailed = 1
                        End If

                    End If
                Next




                'Group by Date Affect แยกเอกสารตามวันที่ที่มีผล
                Dim oAjpGrpDateAff = From oAjp In oLstTCNTPdtAjpHD Group oAjp By oAjp.FDIphAffect Into oGrpAff = Group

                ' ลบข้อมูล table Temp DT,HD
                oSql.Clear()
                oSql.AppendLine("DELETE FROM TLNKPdtAjpDT")
                If oSql.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                End If

                oSql.Clear()
                oSql.AppendLine("DELETE FROM TLNKPdtAjpHD")
                If oSql.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                End If

                For nRowAff As Integer = 0 To oAjpGrpDateAff.Count - 1
                    Dim oLstAjpDTToTmp As New List(Of cTCNTPdtAjpDT)
                    Dim nFNIpdSeqNo = 0

                    ' Get รหัสเอกสาร
                    If tDocNo = "" Then
                        tDocNo = C_GENtDocNoAjp()
                    Else
                        tDocNo = tDocNo.Substring(0, 7) + "-" + (Convert.ToInt32(tDocNo.Substring(8, 6)) + 1).ToString("000000")
                    End If

                    ' Get DT Affect
                    Dim oAjpDT = oLstTCNTPdtAjpDT.Where(Function(o) o.FDIphAffect = oAjpGrpDateAff(nRowAff).FDIphAffect).ToList

                    'เตรียมข้อมูล DT
                    For nAjpDT As Integer = 0 To oAjpDT.Count - 1
                        ' ตรวตสอบว่าสินค้ามีในระบบหรือไม่ และมีค่ามากกว่า 0
                        oSql.Clear()
                        oSql.AppendLine("SELECT FTPdtCode FROM TCNMPdt WHERE FTPdtCode='" + oAjpDT(nAjpDT).FTPdtCode + "'")
                        If oDatabase.C_CHKnDATHASRow(oSql.ToString()) = 0 Then
                            'เขียน log PdtCode ไม่มีในระบบ
                            atDesc.Add("รหัสสินค้า " + oAjpDT(nAjpDT).FTPdtCode + vbTab + "ไม่มีอยู่ในระบบ  (Failed)")
                            nFailed += 1
                            cCNVB.nVB_StaImportFailed = 1
                        Else
                            oSql.Clear()
                            oSql.AppendLine("SELECT FCPdtRetPriS1 FROM TCNMPdt WHERE FTPdtCode='" + oAjpDT(nAjpDT).FTPdtCode + "'")
                            Dim atResultpris1() As String = oDatabase.C_GETtSQLExecuteScalar(oSql.ToString(), "c").Split("|")
                            If atResultpris1(0) = "1" Then
                                ' Update ราคาสินค้าที่ Master เป็น 0 เท่านั้น
                                If atResultpris1(1) = 0 Or atResultpris1(1) = 0.00 Then

                                    'Set FNIpdSeqNo
                                    nFNIpdSeqNo += 1
                                    oAjpDT(nAjpDT).FNIpdSeqNo = nFNIpdSeqNo

                                    'Set ราคาขายเดิม
                                    oAjpDT(nAjpDT).FCIpdPriOld = atResultpris1(1)

                                    ' Set DocNo
                                    oAjpDT(nAjpDT).FTIphDocNo = tDocNo

                                    ' Set ราคาใหม่บวก Vat
                                    oAjpDT(nAjpDT).FCIpdPriNew = oAjpDT(nAjpDT).FCIpdPriNew + (oAjpDT(nAjpDT).FCIpdPriNew * 0.07)

                                    ' Set Product name
                                    oSql.Clear()
                                    oSql.AppendLine("SELECT FTPdtName FROM TCNMPdt WHERE FTPdtCode='" + oAjpDT(nAjpDT).FTPdtCode + "'")
                                    Dim atResultPdtName() As String = oDatabase.C_GETtSQLExecuteScalar(oSql.ToString(), "t").Split("|")
                                    If atResultPdtName(0) = "1" Then
                                        oAjpDT(nAjpDT).FTPdtName = atResultPdtName(1)
                                    End If

                                    ' Set barcode
                                    oSql.Clear()
                                    oSql.AppendLine("SELECT FTPdtBarCode1 FROM TCNMPdt WHERE FTPdtCode='" + oAjpDT(nAjpDT).FTPdtCode + "'")
                                    Dim atResultPdtBar() As String = oDatabase.C_GETtSQLExecuteScalar(oSql.ToString(), "t").Split("|")
                                    If atResultPdtBar(0) = "1" Then
                                        oAjpDT(nAjpDT).FTIpdBarCode = atResultPdtBar(1)
                                    End If

                                    ' Set สต็อก ควบคุม
                                    oSql.Clear()
                                    oSql.AppendLine("SELECT FTPdtStkCode FROM TCNMPdt WHERE FTPdtCode='" + oAjpDT(nAjpDT).FTPdtCode + "'")
                                    Dim atResultStkCode() As String = oDatabase.C_GETtSQLExecuteScalar(oSql.ToString(), "t").Split("|")
                                    If atResultStkCode(0) = "1" Then
                                        oAjpDT(nAjpDT).FTIpdStkCode = atResultStkCode(1)
                                    End If

                                    ' Add เข้า Model To Temp
                                    oLstAjpDTToTmp.Add(oAjpDT(nAjpDT))
                                Else
                                    'เขียน log PdtCode มีราคามากกว่า 0 
                                    atDesc.Add("รหัสสินค้า " + oAjpDT(nAjpDT).FTPdtCode + vbTab + "มีราคาไม่เท่ากับ 0  (Failed)")
                                    nFailed += 1
                                    cCNVB.nVB_StaImportFailed = 1
                                End If
                            End If
                        End If
                    Next

                    'เตรียมข้อมูล HD
                    Dim oAjpHD = oLstTCNTPdtAjpHD.Where(Function(o) o.FDIphAffect = oAjpGrpDateAff(nRowAff).FDIphAffect).FirstOrDefault
                    oAjpHD.FTIphDocNo = tDocNo
                    oAjpHD.FCIphTotal = (From p In oLstAjpDTToTmp).Sum(Function(o) o.FCIpdPriNew)

                    'Save ลง Temp
                    If oLstAjpDTToTmp.Count > 0 Then

                        ' Save DocNo ลง TCNTLockDoc
                        oSql.Clear()
                        oSql.AppendLine("INSERT INTO TCNTLockDoc(FTLokDocNo,FTLokDocType,FTLokModule,FTLokHwName,FTUsrCode,FDLokDate,FTLokAppName) VALUES")
                        oSql.AppendLine("('" + tDocNo + "'")
                        oSql.AppendLine(",1")
                        oSql.AppendLine(",'TCNTPdtAjpHD'")
                        oSql.AppendLine(",'" + Environment.MachineName + "'")
                        oSql.AppendLine(",'009'")
                        oSql.AppendLine(",'" + Format(Now, "yyyy-MM-dd HH:mm:ss.fff") + "'")
                        oSql.AppendLine(",'AdaLinkBUFC')")
                        If oSql.ToString <> "" Then
                            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                        End If

                        'Dim oPdtDulicateGrp = From oBar In oPdtCodeDulicate
                        '                      Group oBar By Key = oBar.FTPdtCode Into Group
                        '                      Where Group.Count() > 1
                        '                      Select FTPdtCode = Key, numbersCount = Group.Count()
                        Dim oPdtDulicateGrp = From oBar In oPdtCodeDulicate Group oBar By oBar.FTPdtCode Into oGrpCode = Group

                        nChkRowDT = 0
                        ' Save  DT
                        For nRowDTTmp As Integer = 0 To oLstAjpDTToTmp.Count - 1

                            If oPdtDulicateGrp.Count > 0 Then
                                For nRowDulicatePdt = 0 To oPdtDulicateGrp.Count - 1
                                    If oPdtDulicateGrp(nRowDulicatePdt).FTPdtCode = oLstAjpDTToTmp(nRowDTTmp).FTPdtCode Then
                                        atDesc.Add("ระบุรหัสสินค้า " + oPdtDulicateGrp(nRowDulicatePdt).FTPdtCode + vbTab + "มากกว่า 1 แถว  (Failed)")
                                        nFailed += 1
                                        cCNVB.nVB_StaImportFailed = 1
                                    Else
                                        nChkRowDT += 1
                                        oSql.Clear()
                                        oSql.AppendLine("INSERT INTO TLNKPdtAjpDT(FTIphDocNo,FNIpdSeqNo,FTPdtCode")
                                        oSql.AppendLine(",FTPdtName,FTIphDocType,FDIphDocDate,FTBchCode")
                                        oSql.AppendLine(",FTIpdBarCode,FTIpdStkCode,FCIpdStkFac,FTPunCode")
                                        oSql.AppendLine(",FCIpdFactor,FNIpdAdjLev,FCIpdPriOld,FCIpdPriNew")
                                        oSql.AppendLine(",FNIpdUnitType,FTIpdBchTo,FDDateUpd,FTTimeUpd")
                                        oSql.AppendLine(",FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns,FNLnkLine) VALUES")
                                        oSql.AppendLine("('" + oLstAjpDTToTmp(nRowDTTmp).FTIphDocNo + "'")
                                        oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FNIpdSeqNo) + "")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTPdtCode + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTPdtName + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIphDocType + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FDIphDocDate + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTBchCode + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIpdBarCode + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIpdStkCode + "'")
                                        oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdStkFac) + "")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTPunCode + "'")
                                        oSql.AppendLine(",'" + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdFactor) + "'")
                                        oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FNIpdAdjLev) + "")
                                        oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdPriOld) + "")
                                        oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdPriNew) + "")
                                        oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FNIpdUnitType) + "")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIpdBchTo + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FDDateUpd + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTTimeUpd + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTWhoUpd + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FDDateIns + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTTimeIns + "'")
                                        oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTWhoIns + "',1)")
                                        If oSql.ToString <> "" Then
                                            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                                        End If
                                    End If
                                Next
                            Else
                                nChkRowDT += 1
                                oSql.Clear()
                                oSql.AppendLine("INSERT INTO TLNKPdtAjpDT(FTIphDocNo,FNIpdSeqNo,FTPdtCode")
                                oSql.AppendLine(",FTPdtName,FTIphDocType,FDIphDocDate,FTBchCode")
                                oSql.AppendLine(",FTIpdBarCode,FTIpdStkCode,FCIpdStkFac,FTPunCode")
                                oSql.AppendLine(",FCIpdFactor,FNIpdAdjLev,FCIpdPriOld,FCIpdPriNew")
                                oSql.AppendLine(",FNIpdUnitType,FTIpdBchTo,FDDateUpd,FTTimeUpd")
                                oSql.AppendLine(",FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns,FNLnkLine) VALUES")
                                oSql.AppendLine("('" + oLstAjpDTToTmp(nRowDTTmp).FTIphDocNo + "'")
                                oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FNIpdSeqNo) + "")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTPdtCode + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTPdtName + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIphDocType + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FDIphDocDate + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTBchCode + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIpdBarCode + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIpdStkCode + "'")
                                oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdStkFac) + "")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTPunCode + "'")
                                oSql.AppendLine(",'" + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdFactor) + "'")
                                oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FNIpdAdjLev) + "")
                                oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdPriOld) + "")
                                oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FCIpdPriNew) + "")
                                oSql.AppendLine("," + Convert.ToString(oLstAjpDTToTmp(nRowDTTmp).FNIpdUnitType) + "")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTIpdBchTo + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FDDateUpd + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTTimeUpd + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTWhoUpd + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FDDateIns + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTTimeIns + "'")
                                oSql.AppendLine(",'" + oLstAjpDTToTmp(nRowDTTmp).FTWhoIns + "',1)")
                                If oSql.ToString <> "" Then
                                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                                End If
                            End If


                        Next

                        If nChkRowDT > 0 Then
                            'Save HD
                            oSql.Clear()
                            oSql.AppendLine("INSERT INTO TLNKPdtAjpHD(FTIphDocNo,FTIphDocType,FDIphDocDate")
                            oSql.AppendLine(",FTIphDocTime,FTBchCode,FTDptCode,FTUsrCode")
                            oSql.AppendLine(",FTIphApvCode,FCIphTotal,FTIphStaDoc,FTIphStaPrcDoc")
                            oSql.AppendLine(",FNIphStaDocAct,FTIphAdjType,FDIphAffect,FTIphBchTo")
                            oSql.AppendLine(",FTIphPriType,FDIphDStop,FDDateUpd,FTTimeUpd,FTWhoUpd")
                            oSql.AppendLine(",FDDateIns,FTTimeIns,FTWhoIns) VALUES")
                            oSql.AppendLine("('" + oAjpHD.FTIphDocNo + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTIphDocType + "'")
                            oSql.AppendLine(",'" + oAjpHD.FDIphDocDate + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTIphDocTime + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTBchCode + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTDptCode + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTUsrCode + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTIphApvCode + "'")
                            oSql.AppendLine("," + Convert.ToString(oAjpHD.FCIphTotal) + "")
                            oSql.AppendLine(",'" + oAjpHD.FTIphStaDoc + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTIphStaPrcDoc + "'")
                            oSql.AppendLine("," + Convert.ToString(oAjpHD.FNIphStaDocAct) + "")
                            oSql.AppendLine(",'" + oAjpHD.FTIphAdjType + "'")
                            oSql.AppendLine(",'" + oAjpHD.FDIphAffect + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTIphBchTo + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTIphPriType + "'")
                            oSql.AppendLine(",'" + oAjpHD.FDIphDStop + "'")
                            oSql.AppendLine(",'" + oAjpHD.FDDateUpd + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTTimeUpd + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTWhoUpd + "'")
                            oSql.AppendLine(",'" + oAjpHD.FDDateIns + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTTimeIns + "'")
                            oSql.AppendLine(",'" + oAjpHD.FTWhoIns + "')")
                            If oSql.ToString <> "" Then
                                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                            End If
                            nSuccess += 1
                            atDesc.Add("นำเข้าเลขที่เอกสาร " + oAjpHD.FTIphDocNo + vbTab + "สำเร็จ (Success)")
                        End If

                    End If
                Next
                bStaPrc = True

            Else
                'เขียน log จำนวนแถว DT ไม่เท่ากับค่าที่ระบุใน FT
                atDesc.Add("จำนวนแถว DT ไม่เท่ากับค่าที่ระบุใน FT  (Failed)")
                nFailed += 1
                cCNVB.nVB_StaImportFailed = 1
            End If

            oTCNTPdtAjp = New cJobTCNTPdtAjp
            oTCNTPdtAjp.C_CALxWriteLogEr(atDesc, tFullPath, nSuccess, nFailed)
            Return bStaPrc
        Catch ex As Exception
            Return False
            atDesc.Add(ex.Message)
            oTCNTPdtAjp = New cJobTCNTPdtAjp
            oTCNTPdtAjp.C_CALxWriteLogEr(atDesc, tFullPath, nSuccess, nFailed)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
            tMsg = Nothing
            bStaPrc = Nothing
            oLstTCNTPdtAjpHD = Nothing
            oLstTCNTPdtAjpDT = Nothing
            atAjpRow = Nothing
            tAjpDataRow = Nothing
            tAjpPreFix = Nothing
            tPathLog = Nothing
            nSuccess = Nothing
            nFailed = Nothing
            tDateTimeNow = Nothing
            tDesc = Nothing
            tFileName = Nothing
            tDocNo = Nothing
            oTCNTPdtAjpHD = Nothing
        End Try

    End Function

    Private Function C_GENtDocNoAjp() As String
        Dim oCNSP As New cCNSP
        Dim oModel As New cGetFormatAutoTemplate
        Dim tReturn As String = ""
        Try
            oModel = oCNSP.SP_GETmAutoFmtCode("TCNTPdtAjpHD", "FTIphDocNo", 1, "001", Nothing, Nothing)
            tReturn = oCNSP.SP_GETtLastCode(oModel, Nothing, Nothing)
            Return tReturn
        Catch ex As Exception
            Return ""
        Finally
            oCNSP = Nothing
            oModel = Nothing
        End Try
    End Function

    Private Function C_GETtUnit(ByVal ptPunCode As String) As String
        Try
            Dim oSql As New StringBuilder()
            Dim oDatabase As New cDatabaseLocal()
            Dim oDbtbl As New DataTable()
            Dim tPuncode As String = ""
            oSql.Clear()
            oSql.AppendLine(" SELECT FTPunCode,FTPunName ")
            oSql.AppendLine(" FROM TCNMPdtUnit ")
            oSql.AppendLine(" WHERE FTPunCode = '" + ptPunCode + "'")
            oDbtbl = oDatabase.C_GEToTblSQL(oSql.ToString())
            If oDbtbl.Rows.Count > 0 Then
                tPuncode = oDbtbl.Rows(0).Item("FTPunName").ToString()
            End If
            Return tPuncode
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function C_GETtFactor(ByVal ptPdtCode As String, ByVal ptPuncode As String) As Double
        Try
            Dim oSql As New StringBuilder()
            Dim oDatabase As New cDatabaseLocal()
            Dim oDbtbl As New DataTable()
            Dim cFactor As Double
            oSql.Clear()
            oSql.AppendLine(" SELECT FCPdtSFactor ")
            oSql.AppendLine(" FROM TCNMPdt ")
            oSql.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "'")
            oSql.AppendLine(" AND FTPdtSUnit = '" + ptPuncode + "' ")
            oDbtbl = oDatabase.C_GEToTblSQL(oSql.ToString())
            If oDbtbl.Rows.Count > 0 Then
                CFactor = oDbtbl.Rows(0).Item("FCPdtSFactor")
            End If
            Return cFactor
        Catch ex As Exception
        End Try
    End Function


    Class cProduct
        Property FTPdtCode As String = ""
        Property FTPdtName As String = ""
        Property FTPdtNameOth As String = ""
        Property FTPdtBarCode1 As String = ""
        Property FTPdtBarCode2 As String = "NULL"
        Property FTPdtBarCode3 As String = "NULL"
        Property FTPdtStkCode As String = ""
        Property FTPgpChain As String = ""
        Property FTSplCode As String = ""
        Property FTPdtVatType As String = "1"
        Property FTPdtSaleType As String = "1"
        Property FTPdtSUnit As String = ""
        Property FCPdtSFactor As String = "0.0"
        Property FTPdtMUnit As String = ""
        Property FCPdtMFactor As String = "0.0"
        Property FTPdtLUnit As String = ""
        Property FCPdtLFactor As String = "0.0"
        Property FCPdtRetPriS1 As String = "0.0"
        Property FCPdtRetPriM1 As String = "0.0"
        Property FCPdtRetPriL1 As String = "0.0"
        Property FTPbnCode As String = ""
        Property FTPmoCode As String = ""
        Property FTPdtNoDis As String = "2"
        Property FTPdtGrade As String = ""
        Property FTPdtPoint As String = ""
        Property FCPdtPointTime As String = "0.0"
        Property FTPdtType As String = "1"
        Property FTPdtStaActive As String = "1"
        Property FTPdtRmk As String = ""
        Property FTPdtDim As String = ""
        Property FTPdtPkgDim As String = ""
        Property FTPdtGateWhe As String = ""
        Property FTPdtDeliveryBy As String = ""
        Property FDPdtMfg As String = "NULL" 'Fix *CH 12-01-2015
        Property FNPdtAge As String = "0" 'Age of product *CH 20-01-2015
        Property FDPdtExp As String = ""
        Property FTPdtUnitSymbol As String = ""
        Property FTPdtLabSizeS As String = "1"
        Property FNPdtLabQtyS As String = "1"
        Property FTPdtLabSizeM As String = "1"
        Property FNPdtLabQtyM As String = "1"
        Property FTPdtLabSizeL As String = "1"
        Property FNPdtLabQtyL As String = "1"
        Property FDPdtOrdStart As String = ""
        Property FDPdtOrdStop As String = ""
        Property FTPtyCode As String = ""       '*Em 58-02-05
        Property FCPdtStkFac As String = "1"
        Property FTPdtStkControl As String = "1"
        Property FCPdtQtyRet As String = "0"
        Property FCPdtQtyWhs As String = "0"
        Property FCPdtQtyDRet As String = "0"
        Property FCPdtQtyDWhs As String = "0"
        Property FCPdtQtyMRet As String = "0"
        Property FCPdtQtyMWhs As String = "0"
        Property FCPdtQtyOrdBuy As String = "0"
        Property FCPdtCostAvg As String = "0"
        Property FCPdtCostFiFo As String = "0"
        Property FCPdtCostLast As String = "0"
        Property FCPdtCostDef As String = "0"
        Property FCPdtCostOth As String = "0"
        Property FCPdtCostAmt As String = "0"
        Property FCPdtCostStd As String = "0"
        Property FTUsrCode As String = ""
        Property FCPdtMin As String = "0"
        Property FCPdtMax As String = "0"
        Property FCPdtWeight As String = "0"
        Property FTPszCode As String = ""
        Property FTClrCode As String = ""
        Property FTPmhCodeS As String = ""
        Property FTPmhTypeS As String = ""
        Property FDPdtPmtSDateS As String = "NULL"
        Property FDPdtPmtEDateS As String = "NULL"
        Property FTPdtPmtTypeS As String = ""
        Property FTPmhCodeM As String = ""
        Property FTPmhTypeM As String = ""
        Property FDPdtPmtSDateM As String = "NULL"
        Property FDPdtPmtEDateM As String = "NULL"
        Property FTPdtPmtTypeM As String = ""
        Property FTPmhCodeL As String = ""
        Property FTPmhTypeL As String = ""
        Property FDPdtPmtSDateL As String = "NULL"
        Property FDPdtPmtEDateL As String = "NULL"
        Property FTPdtPmtTypeL As String = ""
        Property FTPdtConType As String = "1"
        Property FTPdtSrn As String = "2"
        Property FTPdtPlcCodeS As String = ""
        Property FTPdtPlcCodeM As String = ""
        Property FTPdtPlcCodeL As String = ""
        Property FTPdtAlwOrderS As String = ""
        Property FTPdtAlwOrderM As String = ""
        Property FTPdtAlwOrderL As String = ""
        Property FTPdtAlwBuyS As String = ""
        Property FTPdtAlwBuyM As String = ""
        Property FTPdtAlwBuyL As String = ""
        Property FCPdtRetPriS2 As String = "0"
        Property FCPdtRetPriM2 As String = "0"
        Property FCPdtRetPriL2 As String = "0"
        Property FCPdtRetPriS3 As String = "0"
        Property FCPdtRetPriM3 As String = "0"
        Property FCPdtRetPriL3 As String = "0"
        Property FCPdtWhsPriS1 As String = "0"
        Property FCPdtWhsPriM1 As String = "0"
        Property FCPdtWhsPriL1 As String = "0"
        Property FCPdtWhsPriS2 As String = "0"
        Property FCPdtWhsPriM2 As String = "0"
        Property FCPdtWhsPriL2 As String = "0"
        Property FCPdtWhsPriS3 As String = "0"
        Property FCPdtWhsPriM3 As String = "0"
        Property FCPdtWhsPriL3 As String = "0"
        Property FCPdtWhsPriS4 As String = "0"
        Property FCPdtWhsPriM4 As String = "0"
        Property FCPdtWhsPriL4 As String = "0"
        Property FCPdtWhsPriS5 As String = "0"
        Property FCPdtWhsPriM5 As String = "0"
        Property FCPdtWhsPriL5 As String = "0"
        Property FTPdtWhsDefUnit As String = "1"
        Property FTPdtOthPurSplN1 As String = "NULL"
        Property FTPdtOthPurSplN2 As String = "NULL"
        Property FTPdtOthPurSplN3 As String = "NULL"
        Property FCPdtOthPurSplP1 As String = "0"
        Property FCPdtOthPurSplP2 As String = "0"
        Property FCPdtOthPurSplP3 As String = "0"
        Property FTPdtOthPurSplCmt As String = "0"
        Property FTPdtOthSleSplN1 As String = "NULL"
        Property FTPdtOthSleSplN2 As String = "NULL"
        Property FTPdtOthSleSplN3 As String = "NULL"
        Property FCPdtOthSleSplP1 As String = "NULL"
        Property FCPdtOthSleSplP2 As String = "NULL"
        Property FCPdtOthSleSplP3 As String = "NULL"
        Property FTPdtOthSleSplCmt As String = ""
        Property FTAccCode As String = ""
        Property FTPdtPic As String = ""
        Property FTPdtSound As String = ""
        Property FTPdtStaDel As String = ""
        Property FTPdtBarByGen As String = ""
        Property FTPdtStaSet As String = "1"
        Property FTPdtStaSetPri As String = "1"
        Property FTPdtStaSetShwDT As String = ""
        Property FCPdtLeftPO As String = "0"
        Property FTPdtTax As String = "1"
        Property FTDcsCode As String = ""
        Property FTDepCode As String = ""
        Property FTClsCode As String = ""
        Property FTSclCode As String = ""
        Property FCPdtQtyNow As String = "0"
        Property FTPdtArticle As String = ""
        Property FTPdtGrpControl As String = "2"
        Property FTPdtShwTch As String = ""
        Property FCPdtVatRate As String = "NULL"
        Property FTTcgCode As String = ""
        Property FTPdtNameShort As String = ""
        Property FTPdtNameShortEng As String = ""
        Property FTPdtStaAlwBuy As String = "1"
        Property FTFixGonCode As String = ""
        Property FCPdtLifeCycle As String = "0"
        Property FTPdtOrdDay As String = ""
        Property FTPdtOrdSun As String = "1"
        Property FTPdtOrdMon As String = "1"
        Property FTPdtOrdTue As String = "1"
        Property FTPdtOrdWed As String = "1"
        Property FTPdtOrdThu As String = "1"
        Property FTPdtOrdFri As String = "1"
        Property FTPdtOrdSat As String = "1"
        Property FCPdtLeadTime As String = "0"
        Property FDPdtSaleStart As String = Format(Now, "yyyy/MM/dd")
        Property FDPdtSaleStop As String = "9999-12-31"
        Property FTPdtStaTouch As String = "2"
        Property FTPdtStaAlwRepack As String = "1"
        Property FNPdtPalletSize As String = "0"
        Property FNPdtPalletLev As String = "0"
        Property FNPdtAgeB4Rcv As String = "0"
        Property FNPdtAgeB4Snd As String = "0"
        Property FTItyCode As String = ""
        Property FTMcrCode As String = ""
        Property FTSlcItemCode As String = ""
        Property FTPdtPic4Slip As String = ""
        Property FTPdtAlwPickS As String = ""
        Property FTPdtAlwPickM As String = ""
        Property FTPdtAlwPickL As String = ""
        Property FTPdtStaFastF As String = "2"
        Property FDDateUpd As String = ""
        Property FTTimeUpd As String = ""
        Property FTWhoUpd As String = ""
        Property FDDateIns As String = ""
        Property FTTimeIns As String = ""
        Property FTWhoIns As String = ""
        Property FNLNKLine As String = ""
        Property FNLNKStatus As String = ""
        Property FTLNKLog As String = ""
        Property FNLnkPrc As String = "0"
    End Class

    Class cPdtUnit
        Property FTPunName As String
    End Class

    Class cPdtGrp
        Property FTPgpCode As String
        Property FNPgpLevel As String
        Property FTPgpChain As String
        Property FTPgpRmk As String
    End Class

    Class cPdtCodeDulicate
        Property FTPdtCode As String
    End Class

End Class
