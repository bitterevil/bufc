﻿Public Class cImportAuto

    Public Sub C_CALxProcess()
        Dim aImportTemp = cImportTemplate.C_GETaItemImport()
        For Each oImportTemp In aImportTemp
            If oImportTemp.nCount > 0 Then
                oImportTemp.nSelect = 1
                'Console.WriteLine(oImportTemp.tItem)
                cLog.C_CALxWriteLogAuto(oImportTemp.tItem)
            End If
        Next
        cImportTemplate.oC_Progress = New ProgressBar
        cImportTemplate.C_CALxProcessAll()
    End Sub

End Class
