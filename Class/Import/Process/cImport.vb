﻿Imports System.Text
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.IO

Public Class cImport

    Private tSpr As String = AdaConfig.cConfig.tSeparator
    Private oC_BulkCopy As SqlBulkCopy
    Private oC_DataSet As DataSet
    Private oC_DataSetTemp As DataSet
    Private aC_TableName As String()

    Public Sub New(ParamArray paTableName As String())
        Try
            aC_SQLCmdExtB4.Clear()
            aC_SQLCmdExtAF.Clear()
            oC_DataSetTemp = New DataSet
            Dim aSql As New StringBuilder("")
            Dim nIndex As Integer = 0
            ReDim aC_TableName(paTableName.Length - 1)
            For Each tTableName In paTableName
                aC_TableName(nIndex) = tTableName
                Dim oDatatable = New DataTable

                aSql.Clear()
                aSql.Append("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & tTableName & "]') AND type in (N'U'))")
                aSql.Append(Environment.NewLine)
                aSql.Append("TRUNCATE TABLE [dbo].[" & tTableName & "]")
                If C_CALbExecSQL(False, aSql.ToString) = False Then
                    Exit Sub
                End If
                aSql.Clear()

                Using oSQLDA As New SqlDataAdapter("SELECT * FROM [" & tTableName & "]", AdaConfig.cConfig.tSQLConnSourceString)
                    oSQLDA.FillSchema(oDatatable, SchemaType.Source)
                End Using

                If oDatatable.Columns("FNLnkLine") Is Nothing Then
                    aSql.Clear()
                    oDatatable.Columns.Add("FNLnkLine", GetType(Integer))
                    aSql.Append("ALTER TABLE " & tTableName)
                    aSql.Append(Environment.NewLine)
                    aSql.Append("ADD FNLnkLine INT")
                    If C_CALbExecSQL(True, aSql.ToString) = False Then
                        Exit Sub
                    End If
                End If

                If oDatatable.Columns("FNLnkStatus") Is Nothing Then
                    oDatatable.Columns.Add("FNLnkStatus", GetType(Integer)) '1:Insert ,2:Update ,3:Error
                    aSql.Clear()
                    aSql.Append("ALTER TABLE " & tTableName)
                    aSql.Append(Environment.NewLine)
                    aSql.Append("ADD FNLnkStatus INT")
                    If C_CALbExecSQL(True, aSql.ToString) = False Then
                        Exit Sub
                    End If
                End If

                If oDatatable.Columns("FTLnkLog") Is Nothing Then
                    aSql.Clear()
                    oDatatable.Columns.Add("FTLnkLog", GetType(String))
                    aSql.Append("ALTER TABLE " & tTableName)
                    aSql.Append(Environment.NewLine)
                    aSql.Append("ADD FTLnkLog VARCHAR(250)")
                    If C_CALbExecSQL(True, aSql.ToString) = False Then
                        Exit Sub
                    End If
                End If

                If oDatatable.Columns("FNLnkPrc") Is Nothing Then
                    oDatatable.Columns.Add("FNLnkPrc", GetType(Integer)) '0:ยังไม่ทำ ,1:ทำแล้ว
                    aSql.Clear()
                    aSql.Append("ALTER TABLE " & tTableName)
                    aSql.Append(Environment.NewLine)
                    aSql.Append("ADD FNLnkPrc INT")
                    If C_CALbExecSQL(True, aSql.ToString) = False Then
                        Exit Sub
                    End If
                End If

                oDatatable.Columns("FNLnkLine").DefaultValue = -1
                oDatatable.Columns("FNLnkStatus").DefaultValue = 1
                oDatatable.Columns("FTLnkLog").DefaultValue = ""
                oDatatable.Columns("FNLnkPrc").DefaultValue = 0

                oC_DataSetTemp.Tables.Add(oDatatable)

                nIndex += 1
            Next
            aSql = Nothing
            bC_Status = True
        Catch ex As Exception
            bC_Status = False
        End Try
    End Sub

    Private aC_Fields As New List(Of DataColumns)
    Public Property aFields() As List(Of DataColumns)
        Get
            Return aC_Fields
        End Get
        Set(ByVal value As List(Of DataColumns))
            aC_Fields = value
        End Set
    End Property

    Private aC_AutoNumber As New List(Of DataColumns)
    Public Property aAutoNumber() As List(Of DataColumns)
        Get
            Return aC_AutoNumber
        End Get
        Set(ByVal value As List(Of DataColumns))
            aC_AutoNumber = value
        End Set
    End Property

    Private aC_Defaults As New List(Of DataColumns)
    Public Property aDefaults As List(Of DataColumns)
        Get
            Return aC_Defaults
        End Get
        Set(ByVal value As List(Of DataColumns))
            aC_Defaults = value
        End Set
    End Property

    Private aC_FieldsMap As New List(Of cMapColumn)
    Public Property aFieldsMap() As List(Of cMapColumn)
        Get
            Return aC_FieldsMap
        End Get
        Set(ByVal value As List(Of cMapColumn))
            aC_FieldsMap = value
        End Set
    End Property

    Private aC_SQLCmdExtB4 As New List(Of String)
    Public Property aSQLCmdExtB4() As List(Of String)
        Get
            Return aC_SQLCmdExtB4
        End Get
        Set(ByVal value As List(Of String))
            aC_SQLCmdExtB4 = value
        End Set
    End Property

    Private aC_SQLCmdExtB4Import As New List(Of String)
    Public Property aSQLCmdExtB4Import() As List(Of String)
        Get
            Return aC_SQLCmdExtB4Import
        End Get
        Set(ByVal value As List(Of String))
            aC_SQLCmdExtB4Import = value
        End Set
    End Property

    Private aC_SQLCmdExtAF As New List(Of String)
    Public Property aSQLCmdExtAF() As List(Of String)
        Get
            Return aC_SQLCmdExtAF
        End Get
        Set(ByVal value As List(Of String))
            aC_SQLCmdExtAF = value
        End Set
    End Property

    Private bC_Status As Boolean = False
    Public Property bStatus() As Boolean
        Get
            Return bC_Status
        End Get
        Set(ByVal value As Boolean)
            bC_Status = value
        End Set
    End Property

    'Datatable To Table Temp (TTHSxxx)
    Private Sub C_CALxSQLBulkCopy(poData As DataTable)
        Using oBulkCopy = New SqlBulkCopy(AdaConfig.cConfig.tSQLConnSourceString)
            Try
                oBulkCopy.DestinationTableName = poData.TableName
                oBulkCopy.WriteToServer(poData)
            Catch ex As Exception
                Me.C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, "", ex.Message.Replace("'", ""), ""))
            End Try
        End Using
        poData.Clear()
    End Sub

    Public Function C_CALbExecSQL(pbUseSqlTransaction As Boolean, ParamArray aSql As String()) As Boolean
        Dim nLoop As Integer = 0
        Dim tErrCmd As String = ""

        C_CALbExecSQL = False
        Using oConnSQL As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oConnSQL.Open()
            Select Case pbUseSqlTransaction
                Case True
                    Using oSQLCmd As New SqlCommand
                        Try
                            oSQLCmd.Connection = oConnSQL
                            oSQLCmd.Transaction = oConnSQL.BeginTransaction(IsolationLevel.ReadCommitted)
                            For Each tSql In aSql
                                tErrCmd = tSql
                                oSQLCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                                oSQLCmd.CommandText = tSql
                                oSQLCmd.ExecuteNonQuery()
                                nLoop += 1
                            Next
                            oSQLCmd.Transaction.Commit()
                            C_CALbExecSQL = True
                        Catch ex As Exception
                            'MsgBox(tErrCmd)
                            oSQLCmd.Transaction.Rollback()
                            Me.C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, "", ex.Message.Replace("'", ""), tErrCmd.Replace("'", "")))
                        End Try
                    End Using

                Case Else
                    Try
                        Using oSQLCmd As New SqlCommand
                            Try
                                oSQLCmd.Connection = oConnSQL
                                For Each tSql In aSql
                                    tErrCmd = tSql
                                    oSQLCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                                    oSQLCmd.CommandText = tSql
                                    oSQLCmd.ExecuteNonQuery()
                                    nLoop += 1
                                Next
                                C_CALbExecSQL = True
                            Catch ex As Exception
                                'MsgBox(tErrCmd)
                                Me.C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, "", ex.Message.Replace("'", ""), tErrCmd.Replace("'", "")))
                            End Try
                        End Using
                        C_CALbExecSQL = True
                    Catch ex As Exception

                    End Try

            End Select
        End Using
    End Function

    'Text File To Table Temp (TTHSxxx)
    Public Function C_CALbImportFileToTemp(poEncoding As Encoding, ParamArray paPathFile As String()) As Boolean
        C_CALbImportFileToTemp = False
        Try
            Dim nIndexTable As Integer = 0
            For Each tFile In paPathFile
                If File.Exists(tFile) = False Then
                    'หาไฟล์ไม่พบ
                    Exit For
                End If

                If nIndexTable > (oC_DataSetTemp.Tables.Count - 1) Then
                    Exit For
                End If

                Dim oTempTable = oC_DataSetTemp.Tables(aC_TableName(nIndexTable))
                If oTempTable.Columns.Count = 0 Then
                    Continue For
                End If

                'Set Default DBNull
                For Each oCol As DataColumn In oTempTable.Columns
                    If oCol.AllowDBNull = False Then
                        Select Case oCol.DataType
                            Case GetType(String)
                                oCol.DefaultValue = ""
                            Case GetType(DateTime)
                                oCol.DefaultValue = Date.Now 'Nothing 
                            Case Else
                                oCol.DefaultValue = 0
                        End Select
                    End If
                Next

                'Set Default Fix
                Dim dDate As DateTime = Date.Now
                Dim tDate As String = Format(dDate, "yyyy-MM-dd")
                Dim tDateTime As String = Format(dDate, "yyyy-MM-dd HH:mm:ss")
                Dim tTime As String = Format(dDate, "HH:mm:ss")
                For Each oDataCol As DataColumns In aC_Defaults
                    If oTempTable.TableName = oDataCol.TableName Then
                        If Not oTempTable.Columns(oDataCol.ColumnName) Is Nothing Then
                            Select Case oTempTable.Columns(oDataCol.ColumnName).DataType
                                Case GetType(DateTime)
                                    Select Case oDataCol.DefaultValue
                                        Case cSQL.DATE
                                            oTempTable.Columns(oDataCol.ColumnName).DefaultValue = tDate
                                        Case cSQL.DATETIME
                                            oTempTable.Columns(oDataCol.ColumnName).DefaultValue = tDateTime
                                    End Select
                                Case Else
                                    Select Case oDataCol.DefaultValue
                                        Case cSQL.TIME
                                            oTempTable.Columns(oDataCol.ColumnName).DefaultValue = tTime
                                        Case Else
                                            oTempTable.Columns(oDataCol.ColumnName).DefaultValue = oDataCol.DefaultValue
                                    End Select
                            End Select

                        End If
                    End If
                Next

                Dim tFileExt As String = System.IO.Path.GetExtension(tFile)
                'Read File To DataTable
                Dim oReader = New System.IO.StreamReader(tFile, poEncoding)
                Dim tLine As String = ""
                Dim nCount As Integer = 1
                Dim nLine As Integer = 0
                Dim oQryColumnMap = aFieldsMap.Where(Function(c) c.tTable = oTempTable.TableName).FirstOrDefault

                While True
                    nLine += 1
                    tLine = oReader.ReadLine
                    If tLine Is Nothing Then
                        If oTempTable.Rows.Count > 0 Then
                            C_CALxSQLBulkCopy(oTempTable)
                        End If
                        Exit While
                    End If

                    Select Case tFileExt.ToLower
                        Case ".TMP".ToLower

                        Case Else
                            tLine = tLine.Replace(tSpr, vbTab)
                    End Select

                    If InStr(tLine, vbTab) > 0 Then
                        Dim oDataRow As DataRow = oTempTable.NewRow
                        oDataRow("FNLnkLine") = nLine
                        Dim aData As String() = tLine.Split(vbTab)

                        'If aData(0).StartsWith("""") = False OrElse aData(aData.Count - 1).EndsWith("""") = False Then
                        '    'ไม่ตรง format 
                        '    oDataRow("FNLnkStatus") = 3
                        '    oDataRow("FTLnkLog") = "Format ข้อมูลไม่ถูกต้อง"
                        '    oTempTable.Rows.Add(oDataRow)
                        '    nCount += 1
                        '    Continue While
                        'End If

                        Dim nErr As Integer = 0
                        Dim nIndex As Integer = 0
                        Dim tValue As String = ""
                        If oQryColumnMap.oDataColumns.Count = 0 Then
                            'ถ้าไม่ได้ Map
                            Dim nCol As Integer = 0
                            For Each tItem In aData
                                nErr = 0
                                tValue = aData(nIndex) '.Replace("""", "").Replace(Chr(34), "")
                                tValue = tValue.Remove(0, 1)
                                tValue = tValue.Remove(tValue.Length - 1, 1)

                                Select Case oTempTable.Columns(nCol).DataType
                                    Case GetType(String)
                                        If tValue.Length <= oTempTable.Columns(nCol).MaxLength Then
                                            tValue = tValue.Replace("'", "").Replace("""", "").Replace(";", "").Replace(Chr(13), "")
                                        Else
                                            'เก็บ Log ***
                                            nErr = 1
                                        End If
                                    Case GetType(DateTime)
                                        If IsDate(tValue) Then

                                        Else
                                            If tValue = "NULL" Then
                                                nIndex += 1
                                                Continue For
                                            End If

                                            'เก็บ Log ***
                                            If tValue.Length = 8 Then
                                                tValue = tValue.Insert(6, "-")
                                                tValue = tValue.Insert(4, "-")
                                                If IsDate(tValue) Then

                                                Else
                                                    nErr = 2
                                                End If
                                            Else
                                                nErr = 2
                                            End If
                                        End If

                                    Case Else
                                        'ใส่ Auto Number ป้องกัน Pk ซ้ำกัน
                                        If aAutoNumber.Count > 0 Then
                                            For Each oFieldAuto In aAutoNumber
                                                If oTempTable.TableName = oFieldAuto.TableName Then
                                                    tValue = nLine
                                                End If
                                            Next
                                        End If

                                        If IsNumeric(tValue) Then

                                        Else
                                            'เก็บ Log ***
                                            nErr = 3
                                        End If

                                End Select

                                'ว่าง/
                                If nErr = 0 OrElse nErr > 1 Then
                                    For Each tField As String In oQryColumnMap.aNeedField
                                        If oTempTable.Columns(nCol).ColumnName = tField Then
                                            If tValue.Length = 0 Then
                                                Select Case nErr
                                                    Case 0
                                                        nErr = 4
                                                    Case 2
                                                        nErr = 0
                                                    Case 3
                                                        nErr = 0
                                                        tValue = "0"
                                                End Select
                                            End If
                                            Exit For
                                        End If
                                    Next
                                End If

                                Select Case nErr
                                    Case 1
                                        oDataRow("FTLnkLog") = oTempTable.Columns(nCol).ColumnName & ",ข้อมูลมีความยาวเกินขนาด " & oTempTable.Columns(nCol).MaxLength
                                    Case 2
                                        oDataRow("FTLnkLog") = oTempTable.Columns(nCol).ColumnName & ",ข้อมูลไม่ใช่รูปแบบวันที่"
                                    Case 3
                                        oDataRow("FTLnkLog") = oTempTable.Columns(nCol).ColumnName & ",ข้อมูลไม่ใช่ชนิดตัวเลข"
                                    Case 4
                                        oDataRow("FTLnkLog") = oTempTable.Columns(nCol).ColumnName & ",ข้อมูลเป็นค่าว่าง"
                                    Case Else

                                End Select

                                If nErr > 0 Then
                                    oDataRow("FNLnkStatus") = 3
                                End If

                                oDataRow(nIndex) = tValue
                                nIndex += 1
                                nCol += 1

                            Next

                        Else 'Map Column 

                            If oQryColumnMap.oDataColumns.Count <> aData.Count Then
                                'เก็บ Log ***
                                Try
                                    For Each oColumn In oTempTable.PrimaryKey
                                        If oColumn.MaxLength > 5 Then
                                            Select Case oColumn.DataType
                                                Case GetType(String)
                                                    oDataRow(oColumn.ColumnName) = nLine.ToString.PadLeft(oColumn.MaxLength - 1, "0")
                                                    'Case GetType(DateTime)
                                                    '    ' oDataRow(oColumn.ColumnName) = Date.Now
                                                Case Else
                                                    oDataRow(oColumn.ColumnName) = nLine * -1
                                            End Select
                                        End If
                                    Next

                                    oDataRow("FNLnkStatus") = 3
                                    oDataRow("FTLnkLog") = "Format ข้อมูลไม่ถูกต้อง"
                                    oTempTable.Rows.Add(oDataRow)
                                Catch ex As Exception
                                    'MsgBox(ex.Message)
                                End Try

                                If nCount = 1000 Then
                                    C_CALxSQLBulkCopy(oTempTable)
                                    nCount = 0
                                End If

                                nCount += 1
                                Continue While
                            End If

                            For Each oCol In oQryColumnMap.oDataColumns
                                nErr = 0
                                tValue = aData(nIndex) '.Replace("""", "").Replace(Chr(34), "")
                                'tValue = tValue.Remove(0, 1)
                                'tValue = tValue.Remove(tValue.Length - 1, 1)

                                If Not oDataRow(oCol.DataSetColumn) Is Nothing Then

                                    Select Case oTempTable.Columns(oCol.DataSetColumn).DataType
                                        Case GetType(String)
                                            If tValue.Length <= oTempTable.Columns(oCol.DataSetColumn).MaxLength Then
                                                tValue = tValue.Replace("'", "").Replace("""", "").Replace(";", "").Replace(Chr(13), "")
                                            Else
                                                nErr = 1
                                            End If

                                        Case GetType(DateTime)
                                            If IsDate(tValue) Then

                                            Else
                                                If tValue = "NULL" Then
                                                    nIndex += 1
                                                    Continue For
                                                End If

                                                'เก็บ Log ***
                                                If tValue.Length = 8 Then
                                                    tValue = tValue.Insert(6, "-")
                                                    tValue = tValue.Insert(4, "-")
                                                    If IsDate(tValue) Then

                                                    Else
                                                        nErr = 2
                                                    End If
                                                Else
                                                    nErr = 2
                                                End If
                                            End If

                                        Case Else
                                            'ใส่ Auto Number ป้องกัน Pk ซ้ำกัน
                                            If aAutoNumber.Count > 0 Then
                                                For Each oFieldAuto In aAutoNumber
                                                    If oTempTable.TableName = oFieldAuto.TableName Then
                                                        If oCol.DataSetColumn = oFieldAuto.ColumnName Then
                                                            tValue = nLine
                                                        End If
                                                    End If
                                                Next
                                            End If

                                            If IsNumeric(tValue) Then

                                            Else
                                                nErr = 3
                                            End If
                                    End Select

                                    'ว่าง/
                                    If nErr = 0 OrElse nErr > 1 Then
                                        For Each tField As String In oQryColumnMap.aNeedField
                                            If oCol.DataSetColumn = tField Then
                                                If tValue.Length = 0 Then
                                                    Select Case nErr
                                                        Case 0
                                                            nErr = 4
                                                        Case 2
                                                            nErr = 0
                                                        Case 3
                                                            nErr = 0
                                                            tValue = "0"
                                                    End Select
                                                End If
                                                Exit For
                                            End If
                                        Next
                                    End If

                                    Select Case nErr
                                        Case 1
                                            oDataRow("FTLnkLog") = oCol.DataSetColumn & ",ความยาวเกิน " & oTempTable.Columns(oCol.DataSetColumn).MaxLength & " ตัวอักษร"
                                        Case 2
                                            oDataRow("FTLnkLog") = oCol.DataSetColumn & ",ไม่ใช่รูปแบบวันที่"
                                        Case 3
                                            oDataRow("FTLnkLog") = oCol.DataSetColumn & ",ไม่ใช่ชนิดตัวเลข"
                                        Case 4
                                            oDataRow("FTLnkLog") = oCol.DataSetColumn & ",เป็นค่าว่าง"
                                        Case Else

                                    End Select

                                    If nErr > 0 Then
                                        oDataRow("FNLnkStatus") = 3
                                    Else
                                        oDataRow(oCol.DataSetColumn) = tValue
                                    End If

                                    nIndex += 1
                                End If

                            Next

                        End If

                        Try
                            oTempTable.Rows.Add(oDataRow)
                        Catch ex As Exception
                            Dim tCol As String = ""
                            'aW_TableName มากว่า 1 คือมีมากกว่า 1 ตาราง ถ้า aW_TableName มี 1 ตาราง คือตาราง Master
                            If aC_TableName.Count > 1 Then
                                'Duplicate Data
                                For Each oColumn In oTempTable.PrimaryKey
                                    Select Case oColumn.DataType
                                        Case GetType(String)
                                            'oDataRow(oColumn.ColumnName) = nCount
                                        Case GetType(DateTime)
                                            ' oDataRow(oColumn.ColumnName) = Date.Now
                                        Case Else
                                            oDataRow(oColumn.ColumnName) = nLine * -1
                                            tCol = oColumn.ColumnName
                                    End Select
                                Next

                                oDataRow("FNLnkStatus") = 3
                                oDataRow("FTLnkLog") = tCol & "," & ex.Message
                                oTempTable.Rows.Add(oDataRow)
                            Else
                                For Each oColumn In oTempTable.PrimaryKey
                                    Select Case oColumn.DataType
                                        Case GetType(String)
                                            oDataRow(oColumn.ColumnName) = "@" & nLine.ToString.PadLeft(oColumn.MaxLength - 1, "0")
                                            'Case GetType(DateTime)
                                            '    ' oDataRow(oColumn.ColumnName) = Date.Now
                                            'Case Else
                                            '    oDataRow(oColumn.ColumnName) = nCount * -1
                                    End Select
                                    tCol = oColumn.ColumnName
                                    Exit For
                                Next

                                oDataRow("FNLnkStatus") = 3
                                oDataRow("FTLnkLog") = tCol & "," & ex.Message
                                oTempTable.Rows.Add(oDataRow)
                            End If

                        End Try
                    Else
                        'ไม่ตรง format 
                        Dim oDataRow As DataRow = oTempTable.NewRow
                        oDataRow("FNLnkLine") = nLine
                        oDataRow("FNLnkStatus") = 3
                        oDataRow("FTLnkLog") = "Format ข้อมูลไม่ถูกต้อง"
                        oTempTable.Rows.Add(oDataRow)
                    End If

                    If nCount = 1000 Then
                        C_CALxSQLBulkCopy(oTempTable)
                        nCount = 0
                    End If

                    nCount += 1
                End While
                oReader.Close()
                nIndexTable += 1
            Next
            C_CALbImportFileToTemp = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    'Table Temp (TTHSxxx) To ตารางจริง
    Public Sub C_CALxTransferData(ptOption As eTransferOtion, ParamArray paTableName As String())
        Dim aSqlTransfer As New List(Of String)

        oC_DataSet = New DataSet
        Dim nIndexTable As Integer = 0
        For Each tTableName In paTableName

            Dim oDatatable = New DataTable
            Using oSQLDA As New SqlDataAdapter("SELECT * FROM [" & tTableName & "]", AdaConfig.cConfig.tSQLConnSourceString)
                oSQLDA.FillSchema(oDatatable, SchemaType.Source)
            End Using
            oC_DataSet.Tables.Add(oDatatable)
            '--------------------------------------------------------------------------

            Dim oTable = oC_DataSet.Tables(tTableName)
            Dim oTableTemp = oC_DataSetTemp.Tables(aC_TableName(nIndexTable))
            Dim aColumnsPK() As DataColumn
            aColumnsPK = oTable.PrimaryKey

            If tTableName.Length = 0 Then Exit Sub
            If oTable.Columns.Count = 0 Then Exit Sub

            Dim tSql As String = ""
            Dim tSqlIns As String = ""
            Dim tCol As String = ""
            Dim tWhere As String = ""
            'Dim tWhereIns As String = ""
            For Each oDataCol As DataColumn In oTable.Columns

                If tCol.Length > 0 And tCol.ToString.EndsWith(",") = False Then
                    tCol &= ","
                End If

                If IsDBNull(oDataCol.DefaultValue) = True Then
                    tCol &= "[" & oDataCol.ColumnName & "]"
                Else
                    Dim tValue As String = ""
                    Select Case oDataCol.DefaultValue
                        Case cSQL.DATE
                            tValue = "CONVERT(VARCHAR(10),GETDATE(),120)"
                            tCol &= " " & tValue & " AS [" & oDataCol.ColumnName & "]"
                        Case cSQL.DATETIME
                            tValue = "CONVERT(VARCHAR(20),GETDATE(),120)"
                            tCol &= " " & tValue & " AS [" & oDataCol.ColumnName & "]"
                        Case Else

                            'Check Data Type
                            Select Case oDataCol.DataType
                                Case GetType(String)
                                    tCol &= " '" & oDataCol.DefaultValue & "' AS [" & oDataCol.ColumnName & "]"
                                    'Case GetType(Integer), GetType(Int32), GetType(Int64), GetType(Double), GetType(Decimal)
                                Case Else
                                    tCol &= " " & oDataCol.DefaultValue & " AS [" & oDataCol.ColumnName & "]"
                            End Select

                    End Select

                End If

                'tSqlIns
                If tSqlIns.Length > 0 And tSqlIns.ToString.EndsWith(",") = False Then
                    tSqlIns &= ","
                End If
                tSqlIns &= oDataCol.ColumnName

            Next

            'Insert Into
            If tSqlIns.Length > 0 Then
                tSqlIns = "INSERT INTO " & oTable.TableName & " (" & tSqlIns & ")"
            End If

            For Each oDataCol As DataColumns In aFields
                If oTable.TableName = oDataCol.TableName Then
                    Dim tValue As String = ""
                    Select Case oDataCol.DefaultValue
                        Case cSQL.DATE
                            tValue = "CONVERT(VARCHAR(10),GETDATE(),120)"
                            tCol = tCol.Replace("[" & oDataCol.ColumnName & "]", " " & tValue & " AS [" & oDataCol.ColumnName & "]")
                        Case cSQL.DATETIME
                            tValue = "CONVERT(VARCHAR(20),GETDATE(),120)"
                            tCol = tCol.Replace("[" & oDataCol.ColumnName & "]", " " & tValue & " AS [" & oDataCol.ColumnName & "]")
                        Case cSQL.TIME
                            tValue = "CONVERT(VARCHAR(20),GETDATE(),108)"
                            tCol = tCol.Replace("[" & oDataCol.ColumnName & "]", " " & tValue & " AS [" & oDataCol.ColumnName & "]")
                        Case Else
                            Select Case oDataCol.DataType
                                Case GetType(String)
                                    tValue = "'" & oDataCol.DefaultValue & "'"
                                Case Else
                                    If IsNumeric(oDataCol.DefaultValue) = True Then
                                        tValue = oDataCol.DefaultValue.ToString
                                    Else
                                        tValue = "0"
                                    End If
                            End Select
                            tCol = tCol.Replace("[" & oDataCol.ColumnName & "]", tValue & " AS [" & oDataCol.ColumnName & "]")
                    End Select
                End If
            Next

            '------------------------------------------------------------------
            'Update
            Dim tColUpd As String = ""
            tWhere = ""
            For Each oDataCol In aC_FieldsMap.Where(Function(c) c.tTable = oTableTemp.TableName).FirstOrDefault.oDataColumns

                If Not aFields.Where(Function(c) c.ColumnName = oDataCol.DataSetColumn).FirstOrDefault Is Nothing Then
                    Continue For
                End If

                If tColUpd.Length > 0 And tColUpd.EndsWith(",") = False Then
                    tColUpd &= ","
                End If

                Dim nExit As Integer = 0
                For Each oColumn In aColumnsPK
                    If oDataCol.DataSetColumn = oColumn.ColumnName Then
                        nExit = 1
                        Exit For
                    End If
                Next

                If nExit = 0 Then
                    tColUpd &= oDataCol.DataSetColumn & "=" & oTableTemp.TableName & "." & oDataCol.DataSetColumn
                End If
            Next
            If oTableTemp.TableName = "TLNKPdtType" Then tColUpd = "" '*Em 58-02-05   ถ้าเป็น PdtType ไม่ต้อง Update Name
            '------------------------------------------------------------------
            'PK
            'Where
            For Each oColumn In aColumnsPK
                If tWhere.Length > 0 And tWhere.EndsWith("AND ") = False Then
                    tWhere &= " AND "
                End If

                tWhere &= tTableName & "." & oColumn.ColumnName & "=" & oTableTemp.TableName & "." & oColumn.ColumnName

            Next

            If tWhere.Length > 0 And tWhere.EndsWith("WHERE ") = False Then
                tWhere = "WHERE " & tWhere
            End If

            Dim tSqlUpdTemp As String = "UPDATE {0} SET FNLnkStatus=2 FROM {1} {2} AND FNLnkStatus<>3"
            tSqlUpdTemp = String.Format(tSqlUpdTemp, oTableTemp.TableName, tTableName, tWhere)
            '"UPDATE TTHSBranch SET FNLnkStatus=2 FROM TCNMBranch WHERE TTHSBranch.FTBchCode=TCNMBranch.FTBchCode"
            C_CALbExecSQL(True, tSqlUpdTemp)
            '*****************************

            'If aW_SQLCmdExtB4Import.Count > 0 Then
            '    aSqlTransfer.AddRange(aW_SQLCmdExtB4Import.ToArray)
            'End If

            'Run SQL Cmd
            C_CALbExecSQL(True, aC_SQLCmdExtB4Import.ToArray)
            aSqlTransfer.Clear()

            'Try
            '------------------------------------------------------------------
            'Insert
            If eTransferOtion.INSERT = ptOption OrElse eTransferOtion.BOTH = ptOption Then

                tSql = "SELECT ISNULL(MAX(FNPage),-1) FROM(SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/5000) AS FNPage FROM " & oTableTemp.TableName & " WHERE FNLnkStatus=1)TEMP"
                Using oSQLConn As New SqlClient.SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                    oSQLConn.Open()
                    Dim oSQLCmd As New SqlClient.SqlCommand(tSql, oSQLConn)
                    Dim oSQLRead As SqlClient.SqlDataReader = oSQLCmd.ExecuteReader()
                    While oSQLRead.Read()
                        Dim nMax As Integer = oSQLRead(0)
                        If nMax = -1 Then
                            Exit While
                        End If

                        Dim nPage As Integer = 0
                        While nPage <= nMax
                            aSqlTransfer.Clear()
                            'tSql = "{0} SELECT {1} FROM (SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/1000) AS FNPage,* FROM [dbo].[{2}] WHERE FNLnkStatus=1 ) TEMP WHERE TEMP.FNPage=" & nPage & "" & vbCrLf
                            'tSql = "{0} SELECT {1} FROM (SELECT TOP 1000 * FROM [dbo].[{2}] WHERE FNLnkStatus=1 AND FNLnkPrc=0 ) TEMP"
                            tSql = "{0} SELECT{1} FROM [dbo].[{2}] WHERE FNLnkLine IN (SELECT TOP 5000 FNLnkLine FROM " & oTableTemp.TableName & " WHERE FNLnkStatus=1  AND FNLnkPrc=0 ORDER BY FNLnkLine)"

                            'Fix
                            Dim tColSel As String = ""
                            Select Case oTableTemp.TableName
                                Case "TLNKPdt"
                                    tColSel = tCol
                                    tColSel = tColSel.Replace("[FTPbnCode]", "LEFT(FTPbnCode,5)")
                                    tColSel = tColSel.Replace("[FTPmoCode]", "LEFT(FTPmoCode,5)")
                                Case Else
                                    tColSel = tCol
                            End Select

                            tSql = String.Format(tSql, tSqlIns, tColSel, oTableTemp.TableName) 'tWhereIns
                            ' aSqlTransfer.Add(tSql)

                            If C_CALbExecSQL(True, tSql) Then
                                'tSql = "UPDATE " & oTableTemp.TableName & " SET FNLnkPrc=1 WHERE FNLnkLine IN (SELECT FNLnkLine FROM (SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/1000) AS FNPage,FNLnkLine FROM " & oTableTemp.TableName & " WHERE FNLnkStatus=1 ) TEMP WHERE TEMP.FNPage=" & nPage & ")"
                                tSql = "UPDATE " & oTableTemp.TableName & " SET FNLnkPrc=1 WHERE FNLnkLine IN (SELECT FNLnkLine FROM (SELECT TOP 5000 FNLnkLine FROM " & oTableTemp.TableName & " WHERE FNLnkStatus=1 AND FNLnkPrc=0 ORDER BY FNLnkLine) TEMP)"
                                'aSqlTransfer.Add(tSql)
                                'W_CALbExecSQL(True, aSqlTransfer.ToArray)
                                C_CALbExecSQL(True, tSql)
                            End If
                            nPage += 1
                        End While
                        Exit While
                    End While
                End Using
                aSqlTransfer.Clear()

            End If

            'Update
            If eTransferOtion.UPDATE = ptOption OrElse eTransferOtion.BOTH = ptOption Then
                'Fix กรณีมี Update
                'tColUpd &= ",FDDateUpd=CONVERT(VARCHAR(10),GETDATE(),120),FTTimeUpd=CONVERT(VARCHAR(20),GETDATE(),108),FTWhoUpd='AdaLinkTCC'"

                '*Em 58-02-05
                If tColUpd = "" Then
                    tColUpd &= "FDDateUpd=CONVERT(VARCHAR(10),GETDATE(),120),FTTimeUpd=CONVERT(VARCHAR(20),GETDATE(),108),FTWhoUpd='AdaLinkPTG'"
                Else
                    tColUpd &= ",FDDateUpd=CONVERT(VARCHAR(10),GETDATE(),120),FTTimeUpd=CONVERT(VARCHAR(20),GETDATE(),108),FTWhoUpd='AdaLinkPTG'"
                End If
                '++++++++++++++

                tSql = "SELECT ISNULL(MAX(FNPage),-1) FROM(SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/5000) AS FNPage FROM " & oTableTemp.TableName & " WHERE FNLnkStatus=2)TEMP"
                Using oSQLConn As New SqlClient.SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                    oSQLConn.Open()
                    Dim oSQLCmd As New SqlClient.SqlCommand(tSql, oSQLConn)
                    Dim oSQLRead As SqlClient.SqlDataReader = oSQLCmd.ExecuteReader()
                    While oSQLRead.Read()
                        Dim nMax As Integer = oSQLRead(0)
                        If nMax = -1 Then
                            Exit While
                        End If

                        Dim nPage As Integer = 0
                        While nPage <= nMax
                            aSqlTransfer.Clear()
                            'tSql = "UPDATE {0} SET {1} FROM {2} {3} AND FNLnkStatus=2 AND FNLnkLine IN (SELECT FNLnkLine FROM (SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/1000) AS FNPage,FNLnkLine FROM {2} WHERE FNLnkStatus=2 ) TEMP WHERE TEMP.FNPage=" & nPage & ")" & vbCrLf
                            tSql = "UPDATE {0} SET {1} FROM {2} {3} AND FNLnkStatus=2 AND FNLnkLine IN (SELECT TOP 5000 FNLnkLine FROM {2} WHERE FNLnkStatus=2 AND FNLnkPrc=0)" & vbCrLf
                            tSql = String.Format(tSql, tTableName, tColUpd, oTableTemp.TableName, tWhere) 'tWhereUpd
                            ' aSqlTransfer.Add(tSql)
                            If C_CALbExecSQL(True, tSql) Then
                                'tSql = "UPDATE " & oTableTemp.TableName & " SET FNLnkPrc=1 WHERE FNLnkLine IN (SELECT FNLnkLine FROM (SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/1000) AS FNPage,FNLnkLine FROM " & oTableTemp.TableName & " WHERE FNLnkStatus=2 ) TEMP WHERE TEMP.FNPage=" & nPage & ")"
                                tSql = "UPDATE " & oTableTemp.TableName & " SET FNLnkPrc=1 WHERE FNLnkLine IN (SELECT FNLnkLine FROM (SELECT TOP 5000 FNLnkLine FROM " & oTableTemp.TableName & " WHERE FNLnkStatus=2 AND FNLnkPrc=0 ORDER BY FNLnkLine) TEMP)"
                                'aSqlTransfer.Add(tSql)
                                'W_CALbExecSQL(True, aSqlTransfer.ToArray)
                                C_CALbExecSQL(True, tSql)
                            End If
                            nPage += 1
                        End While
                        Exit While
                    End While
                End Using
                aSqlTransfer.Clear()

            End If

            '-----------------------------------------------------------------------

            nIndexTable += 1
        Next

        Dim aSqlAll As New List(Of String)
        If aC_SQLCmdExtB4.Count > 0 Then
            aSqlAll.AddRange(aC_SQLCmdExtB4.ToArray)
        End If

        If aSqlTransfer.Count > 0 Then
            aSqlAll.AddRange(aSqlTransfer.ToArray)
        End If

        If aC_SQLCmdExtAF.Count > 0 Then
            aSqlAll.AddRange(aC_SQLCmdExtAF.ToArray)
        End If

        If aSqlAll.Count > 0 Then
            C_CALbExecSQL(True, aSqlAll.ToArray)
        End If

        aSqlAll.Clear()
        aC_SQLCmdExtB4Import.Clear()
    End Sub

    Protected Overrides Sub Finalize()
        If Not oC_DataSet Is Nothing Then
            oC_DataSet.Clear()
        End If
        oC_DataSetTemp.Clear()
        MyBase.Finalize()
    End Sub

    'ไม่ได้ใช้
    Private Sub oC_BulkCopy_SqlRowsCopied(sender As Object, e As System.Data.SqlClient.SqlRowsCopiedEventArgs)

    End Sub

    Enum eTransferOtion
        [INSERT]
        [UPDATE]
        [BOTH]
    End Enum

    'Sub Class Format แทน ENUM
    Class cSQL
        Public Const [DATE] = "[yyyy-MM-dd]"
        Public Const [DATETIME] = "[yyyy-MM-dd HH:mm:ss]"
        Public Const [TIME] = "[HH:mm:ss]"
    End Class

    'Sub Class
    Class cMapColumn
        Property tTable As String = ""
        Property oDataColumns As New List(Of DataColumnMapping)
        Property aNeedField As New List(Of String)

        'Check Need Column
        Public Sub W_SETxCheckCol(ParamArray ptCol As String())
            For Each tItem In ptCol
                aNeedField.Add(tItem)
            Next
        End Sub

    End Class

End Class
