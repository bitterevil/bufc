﻿Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports AdaLinkSAPBUFC.cJobTCNMPdtGrp
Public Class cTCNMPdtGrp
    Property tFullPath As String = ""
    Property tTableTemp As String = ""
    Property tTable As String = ""
    Property cNextVal As Double = 0
    Property cProgress As Double = 0
    Property tDateNow As String = ""
    Property tFileLog As String = ""
    Private tSpr As String = AdaConfig.cConfig.tSeparator
    'Private tC_TempLnkPdt As String = "TLNKTmpPdt"
    Private nC_PdtItem As Integer = 0
    Private nC_ValPropress As Double = 0
    Private nC_NextVal As Double = 0

    Public Function C_CALbProcess() As Boolean
        C_CALbProcess = True
        Try

            C_CALbProcess = True
            Dim tChkStk As String = ""
            Dim nLang As Integer = 0
            Dim oDatabase As New cDatabaseLocal
            Dim oDbTbl As New DataTable
            Dim oSql As System.Text.StringBuilder
            Dim tSql As String = ""
            Dim tMsg As String = ""
            cNextVal = cNextVal \ 3
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLang = 1

            If Not cCNVB.bVB_Auto Then '1 Progress
                cProgress += cNextVal
                cProgress = IIf(nC_ValPropress > 100, 100, cProgress)
                wImports.oW_BackgroudWord.ReportProgress(cProgress)
            End If

            'Checked Duplicate In PrepareFile '*CH 19-10-2017
            'Check Duplicate IN Temp
            'tSql = "UPDATE " & tTableTemp & " SET FNLnkStatus=3,FTLnkLog='FTPgpRmk,ซ้ำกันในตาราง' "
            'tSql &= "WHERE FTPgpRmk IN (SELECT FTPgpRmk FROM " & tTableTemp & " WHERE FNLnkStatus = 1 GROUP BY FTPgpRmk HAVING COUNT(FTPgpRmk)>1)"
            'oDatabase.C_CALnExecuteNonQuery(tSql)

            'COUNT Data
            tSql = "SELECT COUNT(FTPgpCode) FROM " & tTableTemp & " WHERE FNLnkStatus = 1"
            Dim nCount As Integer = 0
            Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSQLConn.Open()
                Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                While oSQLRdWhe.Read()
                    nCount = oSQLRdWhe(0)
                End While
            End Using
            Dim aSqlB4 As New List(Of String)
            If nCount > AdaConfig.cConfig.oConfigXml.nCountIndex Then
                'DISABLE Index
                tSql = "SELECT 'ALTER INDEX ['+name+'] ON " & tTable & " DISABLE' FROM sys.indexes"
                tSql &= " WHERE object_id = (select object_id from sys.objects where name = '" & tTable & "')"
                tSql &= " AND type=2 AND is_disabled=0"
                Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                    oSQLConn.Open()
                    Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                    Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                    While oSQLRdWhe.Read()
                        aSqlB4.Add(oSQLRdWhe(0))
                    End While
                End Using
                oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)
                aSqlB4.Clear()
            End If

            Dim nRow As Integer = 0
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTPgpCode FROM " & tTable)
            Dim oPdtTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)

            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT ")
            oSql.AppendLine("FTPgpCode")
            oSql.AppendLine(",FTPgpName")
            oSql.AppendLine(",FTPgpChainName")
            oSql.AppendLine(",FTPgpPic")
            oSql.AppendLine(",FTPgpParent")
            oSql.AppendLine(",FNPgpLevel")
            oSql.AppendLine(",FTPgpChain")
            oSql.AppendLine(",FTPgpRmk")
            oSql.AppendLine(",FDDateUpd")
            oSql.AppendLine(",FTTimeUpd")
            oSql.AppendLine(",FTWhoUpd")
            oSql.AppendLine(",FDDateIns")
            oSql.AppendLine(",FTTimeIns")
            oSql.AppendLine(",FTWhoIns")
            oSql.AppendLine(" FROM " & tTableTemp & " WHERE FNLnkStatus = 1 ORDER BY FTPgpCode")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oDbTbl.TableName = tTable
            If oDbTbl.Rows.Count > 0 Then
                tMsg = C_COPtData(oDbTbl)
                oDbTbl.Clear()

                If tMsg = "" Then
                    oSql = New System.Text.StringBuilder
                    oSql.AppendLine("UPDATE " & tTableTemp & " SET FNLnkPrc=1,FTLnkLog='" & cCNVB.tVB_SuccessImp & "' WHERE FNLnkStatus = 1")
                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
                Else
                    aSqlB4.Add("DELETE TCNMPdtGrp WHERE FTPgpCode IN (SELECT FTPgpCode FROM " & tTableTemp & " WHERE FNLnkStatus = 1)")

                    'UPDATE Status Temp
                    oSql = New System.Text.StringBuilder
                    oSql.AppendLine("UPDATE " & tTableTemp)
                    oSql.AppendLine("FNLnkStatus=3,")
                    oSql.AppendLine("FTLnkLog='" & Left(tMsg, 255) & "'")
                    oSql.AppendLine("WHERE FNLnkStatus = 1")
                    aSqlB4.Add(oSql.ToString)
                    oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)
                    aSqlB4.Clear()
                End If
            End If

            If Not cCNVB.bVB_Auto Then '3 Progress
                cProgress += cNextVal
                cProgress = IIf(nC_ValPropress > 100, 100, cProgress)
                wImports.oW_BackgroudWord.ReportProgress(cProgress)
            End If

            Me.C_CALxBakAndLog()

            'บันทึก TLNKLog
            Dim tDesc As String = ""
            aSqlB4.Add("SELECT 'Insert('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=1 AND FNLnkPrc=1")
            aSqlB4.Add("SELECT 'Update('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=2 AND FNLnkPrc=1")
            aSqlB4.Add("SELECT 'Failed('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=3 AND FNLnkPrc=0")
            For Each tSql In aSqlB4
                If tDesc.Length > 0 Then
                    tDesc &= ","
                End If

                Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                    oSQLConn.Open()
                    Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                    Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                    While oSQLRdWhe.Read()
                        tDesc &= oSQLRdWhe(0)
                    End While
                End Using

            Next

            If File.Exists(tFileLog) = False Then tFileLog = ""
            oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFullPath), tDesc, tFileLog))
            aSqlB4.Clear()

            Dim nFail As Integer = 0
            tSql = "SELECT ISNULL(COUNT(FNLnkStatus),0) FROM " & tTableTemp & " WHERE FNLnkStatus=3 AND FNLnkPrc=0"
            Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSQLConn.Open()
                Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                While oSQLRdWhe.Read()
                    nFail = Convert.ToInt32(oSQLRdWhe(0))
                End While
            End Using
            If nFail > 0 Then
                C_CALbProcess = False
            Else
                C_CALbProcess = True
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub C_CALxBakAndLog()
        Dim oFileLog As New cImportLog(tFileLog)
        Dim tSql As String = ""
        tSql = "SELECT FNLnkLine,'" & Path.GetFileName(tFullPath) & "' AS FileName,FTPgpRmk,FTPgpChainName,FTLnkLog "
        tSql &= "FROM " & tTableTemp
        Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oSQLConn.Open()
            Dim oSQLCmd As New SqlCommand(tSql, oSQLConn)
            Dim oSQLRead As SqlDataReader = oSQLCmd.ExecuteReader()
            Dim tErr As String = ""
            While oSQLRead.Read()
                tErr = oSQLRead(4)
                If tErr.Length > 0 Then
                    Dim oNewLog As New cImportLog.cLog
                    With oNewLog
                        .nLineNo = oSQLRead(0)
                        .tType = oSQLRead(1)
                        If tErr.StartsWith("Format") Then
                            .tDocNo = ""
                            .tSeqNo = ""
                            .tCode = ""
                            .tName = ""
                        Else
                            .tDocNo = oSQLRead(2)
                            .tName = oSQLRead(3)
                        End If

                        If tErr.Split(",").Count = 2 Then
                            .tErrField = tErr.Split(",")(0)
                            .tErrDesc = tErr.Split(",")(1)
                        Else
                            .tErrDesc = tErr
                        End If
                    End With
                    oFileLog.C_CALxAddNew(oNewLog)
                End If
            End While
            oSQLConn.Close()
        End Using
        oFileLog.C_CALbSave("2")
    End Sub
    Public Function C_CALbPrcGrpList(ByVal poListUnUse As List(Of cTCNMPdtGrpList)) As Boolean
        Dim oDatabase As New cDatabaseLocal
        Dim oSql As System.Text.StringBuilder = Nothing
        Dim nRec As Integer
        Dim nSeq As Integer = 0
        Dim tTableArtLst As String = "TCNTFileNameList"
        Dim tDocDate As String = ""

        Try
            cCNVB.oDBInst = C_CRTxDBArtLst(tTableArtLst)
            For nRec = 0 To poListUnUse.Count - 1
                cCNVB.oDBInst.Rows.Add()
                With cCNVB.oDBInst.Rows(cCNVB.oDBInst.Rows.Count - 1)
                    .Item("FDArtDocDate") = poListUnUse(nSeq).FDArtDocDate
                    '.Item("FDArtDocDate") = poListUnUse(0).FDArtDocDate
                    .Item("FNArtSeqNo") = poListUnUse(nRec).FNArtSeqNo
                    .Item("FTDataCnt") = poListUnUse(nRec).FTDataCnt
                    .Item("FTArtListFile") = poListUnUse(nRec).FTArtListFile
                    .Item("FTArtFile") = poListUnUse(nRec).FTArtFile
                    .Item("FTArtStaPrc") = IIf(poListUnUse(nRec).FTArtStaPrc = "NULL", "", poListUnUse(nRec).FTArtStaPrc)
                    .Item("FDDateIns") = Format(Now, "yyyy/MM/dd")
                    .Item("FTTimeIns") = Format(Now, "HH:mm:ss")
                    .Item("FTWhoIns") = "AdaLinkPTG"
                    .Item("FDDateUpd") = Format(Now, "yyyy/MM/dd")
                    .Item("FTTimeUpd") = Format(Now, "HH:mm:ss")
                    .Item("FTWhoUpd") = "AdaLinkPTG"

                End With
                nSeq += 1
            Next
            If cCNVB.oDBInst.Rows.Count > 0 Then
                C_COPtData(cCNVB.oDBInst)
            End If
            Return True
        Catch ex As Exception
            Return False
        Finally
            oDatabase = Nothing
            oSql = Nothing
        End Try
        Return True
    End Function
    Private Function C_CRTxDBArtLst(ptTable) As DataTable
        'EYE 59-02-23 สร้าง Datatable เพื่อเก็บข้อมูลก่อนการ Insert to TCNTArticleList
        Dim oDT As New DataTable(ptTable)
        oDT.Columns.Add("FDArtDocDate", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FNArtSeqNo", Type.GetType("System.Int32"))
        oDT.Columns.Add("FTDataCnt", Type.GetType("System.String"))
        oDT.Columns.Add("FTArtListFile", Type.GetType("System.String"))
        oDT.Columns.Add("FTArtFile", Type.GetType("System.String"))
        oDT.Columns.Add("FTArtStaPrc", Type.GetType("System.String"))
        oDT.Columns.Add("FDDateIns", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTTimeIns", Type.GetType("System.String"))
        oDT.Columns.Add("FTWhoIns", Type.GetType("System.String"))
        oDT.Columns.Add("FDDateUpd", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTTimeUpd", Type.GetType("System.String"))
        oDT.Columns.Add("FTWhoUpd", Type.GetType("System.String"))

        Return oDT
    End Function

End Class
