﻿Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO

Public Class cTCNMPdtUnit

    Property tFullPath As String = ""
    Property tTableTemp As String = ""
    Property tTable As String = ""

    Property tFileLog As String = ""

    Public Function C_CALbProcess() As Boolean
        C_CALbProcess = True
        Dim nPage As Integer = 0
        Try
            Dim oDbTbl As New DataTable
            Dim oSql As New System.Text.StringBuilder
            Dim oDatabase As New cDatabaseLocal

            'Update
            oSql.AppendLine("UPDATE Lnk SET")
            oSql.AppendLine("	Lnk.FNLnkStatus = 2")
            oSql.AppendLine("FROM " & tTable & " Pun INNER JOIN " & tTableTemp & " Lnk ON Pun.FTPunCode =  Lnk.FTPunCode")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            oDbTbl = New DataTable
            oSql = New System.Text.StringBuilder
            nPage = 0
            oSql.AppendLine("SELECT ISNULL(MAX(FNPage),-1) FNPage FROM(SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/5000) AS FNPage FROM " & tTableTemp & " WHERE FNLnkStatus=2)TEMP")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                nPage = Convert.ToInt32(oDbTbl.Rows(0)("FNPage"))
            End If
            For nRound = 0 To nPage
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("UPDATE " & tTable & " SET")
                oSql.AppendLine("FTPunName=" & tTableTemp & ".FTPunName,")
                oSql.AppendLine("FDDateUpd=CONVERT(VARCHAR(10),GETDATE(),120),")
                oSql.AppendLine("FTTimeUpd=CONVERT(VARCHAR(20),GETDATE(),108),")
                oSql.AppendLine("FTWhoUpd='" & cCNVB.tVB_UserName & "'")
                oSql.AppendLine("FROM " & tTableTemp & "")
                oSql.AppendLine("WHERE " & tTable & ".FTPunCode=" & tTableTemp & ".FTPunCode")
                oSql.AppendLine("AND FNLnkStatus=2")
                oSql.AppendLine("AND FNLnkLine IN (SELECT TOP 5000 FNLnkLine FROM " & tTableTemp & " WHERE FNLnkStatus=2 AND FNLnkPrc=0)")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

                oSql = New System.Text.StringBuilder
                oSql.AppendLine("UPDATE " & tTableTemp & " SET FNLnkPrc=1,FTLnkLog='" & cCNVB.tVB_SuccessImp & "'")
                oSql.AppendLine("WHERE FNLnkLine IN (SELECT FNLnkLine FROM (SELECT TOP 5000 FNLnkLine FROM " & tTableTemp & " WHERE FNLnkStatus=2 And FNLnkPrc=0 ORDER BY FNLnkLine) TEMP)")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
            Next

            'Insert
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT ISNULL(MAX(FNPage),-1) FNPage FROM(SELECT (ROW_NUMBER() OVER(ORDER BY FNLnkLine )/5000) AS FNPage")
            oSql.AppendLine("FROM " & tTableTemp & " WHERE FNLnkStatus=1)TEMP")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                nPage = Convert.ToInt32(oDbTbl.Rows(0)("FNPage"))
            End If
            For nRound = 0 To nPage
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("INSERT INTO " & tTable & "(")
                oSql.AppendLine("FTPunCode,FTPunName,")
                oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd,")
                oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns)")
                oSql.AppendLine("SELECT FTPunCode,FTPunName,")
                oSql.AppendLine("CONVERT(VARCHAR(10),GETDATE(),120),CONVERT(VARCHAR(20),GETDATE(),108),'" & cCNVB.tVB_UserName & "',")
                oSql.AppendLine("CONVERT(VARCHAR(10),GETDATE(),120),CONVERT(VARCHAR(20),GETDATE(),108),'" & cCNVB.tVB_UserName & "'")
                oSql.AppendLine("FROM " & tTableTemp)
                oSql.AppendLine("WHERE FNLnkStatus=1")
                oSql.AppendLine("AND FNLnkLine IN (SELECT TOP 5000 FNLnkLine FROM " & tTableTemp & " WHERE FNLnkStatus=1 AND FNLnkPrc=0)")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

                oSql = New System.Text.StringBuilder
                oSql.AppendLine("UPDATE " & tTableTemp & " SET FNLnkPrc=1,FTLnkLog='" & cCNVB.tVB_SuccessImp & "'")
                oSql.AppendLine("WHERE FNLnkLine IN (SELECT FNLnkLine FROM (SELECT TOP 5000 FNLnkLine FROM " & tTableTemp & " WHERE FNLnkStatus=1 AND FNLnkPrc=0 ORDER BY FNLnkLine) TEMP)")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
            Next

            Me.C_CALxBakAndLog()

            ''บันทึก TLNKLog
            'Dim aSqlB4 As New List(Of String)
            'Dim tDesc As String = ""
            'aSqlB4.Add("SELECT 'Insert('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=1 AND FNLnkPrc=1")
            'aSqlB4.Add("SELECT 'Update('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=2 AND FNLnkPrc=1")
            'aSqlB4.Add("SELECT 'Failed('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=3 AND FNLnkPrc=0")
            'For Each tSql In aSqlB4
            '    If tDesc.Length > 0 Then
            '        tDesc &= ","
            '    End If

            '    Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            '        oSQLConn.Open()
            '        Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
            '        Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
            '        While oSQLRdWhe.Read()
            '            tDesc &= oSQLRdWhe(0)
            '        End While
            '    End Using

            'Next

            ''tFileLog = tTable
            'If File.Exists(tFileLog) = False Then tFileLog = ""
            'oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFullPath), tDesc, tFileLog))
            'aSqlB4.Clear()
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub C_CALxBakAndLog()
        Dim oFileLog As New cImportLog(tFileLog)
        Dim tSql As String = ""
        tSql = "SELECT FNLnkLine,'" & Path.GetFileName(tFullPath) & "' AS FileName,FTPunCode,FTPunName,FTLnkLog "
        tSql &= "FROM " & tTableTemp
        Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oSQLConn.Open()
            Dim oSQLCmd As New SqlCommand(tSql, oSQLConn)
            Dim oSQLRead As SqlDataReader = oSQLCmd.ExecuteReader()
            Dim tErr As String = ""
            While oSQLRead.Read()
                tErr = oSQLRead(4)
                If tErr.Length > 0 Then
                    Dim oNewLog As New cImportLog.cLog
                    With oNewLog
                        .nLineNo = oSQLRead(0)
                        .tType = oSQLRead(1)
                        If tErr.StartsWith("Format") Then
                            .tDocNo = ""
                            .tSeqNo = ""
                            .tCode = ""
                            .tName = ""
                        Else
                            .tDocNo = oSQLRead(2)
                            .tName = oSQLRead(3)
                        End If

                        If tErr.Split(",").Count = 2 Then
                            .tErrField = tErr.Split(",")(0)
                            .tErrDesc = tErr.Split(",")(1)
                        Else
                            .tErrDesc = tErr
                        End If
                    End With
                    oFileLog.C_CALxAddNew(oNewLog)
                End If
            End While
            oSQLConn.Close()
        End Using
        oFileLog.C_CALbSave()
    End Sub

End Class
