﻿Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO

Public Class cTCNTPdtDetail

    Property tFullPath As String = ""
    Property tTableTemp As String = ""
    Property tTable As String = ""

    Property tFileLog As String = ""

    Public Function C_CALbProcess() As Boolean
        C_CALbProcess = True
        Try
            Dim oNewTable = New cImport(tTableTemp)
            With oNewTable
                If .bStatus = True Then
                    Dim oTempMap As New cImport.cMapColumn
                    oTempMap.tTable = tTableTemp
                    oTempMap.oDataColumns.Add(New DataColumnMapping(0, "FTPdtCode"))
                    oTempMap.oDataColumns.Add(New DataColumnMapping(1, "FNPdeSeq"))
                    oTempMap.oDataColumns.Add(New DataColumnMapping(2, "FTPdeDesc"))
                    oTempMap.oDataColumns.Add(New DataColumnMapping(3, "FTPdeDescOth"))

                    oTempMap.W_SETxCheckCol("FTPdtCode", "FNPdeSeq")
                    .aFieldsMap.Add(oTempMap)

                    Dim tFullPathTemp As String = tFullPath.ToUpper.Replace(".txt".ToUpper, "_detail.tmp".ToUpper)
                    .C_CALbImportFileToTemp(System.Text.Encoding.Default, tFullPathTemp)

                    .aFields.Add(New DataColumns With {.TableName = tTable, .ColumnName = "FDDateUpd", .DefaultValue = cImport.cSQL.DATE})
                    .aFields.Add(New DataColumns With {.TableName = tTable, .ColumnName = "FTTimeUpd", .DefaultValue = cImport.cSQL.TIME})
                    .aFields.Add(New DataColumns With {.TableName = tTable, .ColumnName = "FTWhoUpd", .DefaultValue = "AdaLinkPTG"})
                    .aFields.Add(New DataColumns With {.TableName = tTable, .ColumnName = "FDDateIns", .DefaultValue = cImport.cSQL.DATE})
                    .aFields.Add(New DataColumns With {.TableName = tTable, .ColumnName = "FTTimeIns", .DefaultValue = cImport.cSQL.TIME})
                    .aFields.Add(New DataColumns With {.TableName = tTable, .ColumnName = "FTWhoIns", .DefaultValue = "AdaLinkPTG"})
                    .C_CALxTransferData(cImport.eTransferOtion.BOTH, tTable)

                    Me.C_CALxBakAndLog()

                    'บันทึก TLNKLog
                    Dim aSqlB4 As New List(Of String)
                    Dim tDesc As String = ""
                    aSqlB4.Add("SELECT 'Insert('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=1 AND FNLnkPrc=1")
                    aSqlB4.Add("SELECT 'Update('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=2 AND FNLnkPrc=1")
                    aSqlB4.Add("SELECT 'Failed('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tTableTemp & " WHERE FNLnkStatus=3 AND FNLnkPrc=0")
                    For Each tSql In aSqlB4
                        If tDesc.Length > 0 Then
                            tDesc &= ","
                        End If

                        Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                            oSQLConn.Open()
                            Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                            Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                            While oSQLRdWhe.Read()
                                tDesc &= oSQLRdWhe(0)
                            End While
                        End Using
                    Next

                    tFileLog = tTable
                    'If File.Exists(tFileLog) = False Then tFileLog = tTable
                    .C_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFullPath), tDesc, tFileLog))
                    aSqlB4.Clear()

                End If
            End With

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub C_CALxBakAndLog()
        'Backup File
        'Dim tFileName As String = ""
        'Try
        '    tFileName = Path.GetFileName(tFullPath)
        '    FileCopy(tFullPath, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tFileName)
        '    If File.Exists(tFullPath) Then
        '        File.Delete(tFullPath)
        '    End If
        'Catch ex As Exception
        'End Try

        ' tFileLog = App.oConfigSetting.tLog & "\Log_" & tFileName
        Dim oFileLog As New cImportLog(tFileLog)
        Dim tSql As String = "SELECT FNLnkLine,'" & Path.GetFileName(tFullPath.ToUpper.Replace(".txt".ToUpper, "_detail.tmp".ToUpper)) & "' AS FileName,'' AS DocNo,FNPdeSeq AS Seq,FTPdtCode,ISNULL(FTPdeDesc,'')AS FTPdeDesc,FTLnkLog FROM " & tTableTemp & "  WHERE FNLnkStatus=3"
        Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oSQLConn.Open()
            Dim oSQLCmd As New SqlCommand(tSql, oSQLConn)
            Dim oSQLRead As SqlDataReader = oSQLCmd.ExecuteReader()
            Dim tErr As String = ""
            While oSQLRead.Read()
                tErr = oSQLRead(6)
                If tErr.Length > 0 Then
                    Dim oNewLog As New cImportLog.cLog
                    With oNewLog
                        .nLineNo = oSQLRead(0)
                        .tType = oSQLRead(1)
                        If tErr.StartsWith("Format") Then
                            .tDocNo = ""
                            .tSeqNo = ""
                            .tCode = ""
                            .tName = ""
                        Else
                            .tDocNo = oSQLRead(2)
                            .tSeqNo = oSQLRead(3)
                            .tCode = oSQLRead(4)
                            .tName = oSQLRead(5)
                        End If

                        If tErr.Split(",").Count = 2 Then
                            .tErrField = tErr.Split(",")(0)
                            .tErrDesc = tErr.Split(",")(1)
                        Else
                            .tErrDesc = oSQLRead(6)
                        End If
                    End With
                    oFileLog.C_CALxAddNew(oNewLog)
                End If
            End While
            oSQLConn.Close()
        End Using
        oFileLog.C_CALbSave()
    End Sub

End Class
