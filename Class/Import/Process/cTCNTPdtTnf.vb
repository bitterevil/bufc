﻿Imports System.Data.SqlClient

Public Class cTCNTPdtTnf
    Property tDateFrom As String = ""
    Property tDateTo As String = ""
    Property nMode As Integer = 0 '0:All,1:Interval,2:Select
    Property tCodeFrom As String = ""
    Property tCodeTo As String = ""
    Property tCodeSelect As String = ""
    Property tFullPath As String = ""
    Property tOutPath As String = ""
    Property tHDTable As String = ""
    Property tFileLog As String = ""
    Property cNextVal As Double = 0
    Property cProgress As Double = 0
    Private nC_ValPropress As Double = 0

    ''' <summary>
    ''' Save pdt Tnf In เข้าตารางจริง
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function C_CALbProcess() As Boolean
        Dim nLang As Integer = 0
        Dim oDatabase As New cDatabaseLocal
        Dim oSql As New System.Text.StringBuilder
        Dim tMsg As String = ""
        Dim oDbTblHDDoc As New DataTable
        Try
            '------------------------ Procees 1 SAVE HD------------------------
            oSql.Clear()
            oSql.AppendLine(" INSERT INTO TCNTPdtTnfHD(FTBchCode,FTPthDocNo,FTPthDocType,FDPthDocDate")
            oSql.AppendLine(",FTPthDocTime,FTPthVATInOrEx,FTDptCode,FTDepName")
            oSql.AppendLine(",FTUsrCode,FTUsrName,FTTrnCode,FTSplCode ,FTPthWhTo")
            oSql.AppendLine(",FTPthType,FTWahCode,FTPthApvCode,FDPthTnfDate")
            oSql.AppendLine(",FNPthDocPrint,FCPthVATRate,FCPthNonVat,FCPthDis")
            oSql.AppendLine(",FCPthChg,FTPthStaPaid,FTPthStaRefund")
            oSql.AppendLine(",FTPthStaType,FTPthStaDoc,FTPthStaPrcDoc")
            oSql.AppendLine(",FNPthSign,FTPthCshOrCrd,FCPthPaid,FTPthDstPaid")
            oSql.AppendLine(",FNPthStaDocAct,FNPthStaRef,FTPthStaVatSend,FDDateUpd")
            oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns) ")
            oSql.AppendLine("SELECT DISTINCT  FTBchCode,FTPthDocNo,FTPthDocType,FDPthDocDate")
            oSql.AppendLine(",FTPthDocTime,FTPthVATInOrEx,FTDptCode,FTDepName")
            oSql.AppendLine(",FTUsrCode,FTUsrName,FTTrnCode,FTSplCode ,FTPthWhTo")
            oSql.AppendLine(",FTPthType,FTWahCode,FTPthApvCode,FDPthTnfDate")
            oSql.AppendLine(",FNPthDocPrint,FCPthVATRate,FCPthNonVat,FCPthDis")
            oSql.AppendLine(",FCPthChg,FTPthStaPaid,FTPthStaRefund")
            oSql.AppendLine(",FTPthStaType,FTPthStaDoc,FTPthStaPrcDoc")
            oSql.AppendLine(",FNPthSign,FTPthCshOrCrd,FCPthPaid,FTPthDstPaid")
            oSql.AppendLine(",FNPthStaDocAct,FNPthStaRef,FTPthStaVatSend,FDDateUpd")
            oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns ")
            oSql.AppendLine("FROM TLNKPdtTnfHD")

            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If
            '------------------------ Procees 1 SAVE HD------------------------

            '------------------------ Procees 2 SAVE DT------------------------
            oSql.Clear()
            oSql.AppendLine("INSERT INTO TCNTPdtTnfDT(FTBchCode,FTPthDocNo,FNPtdSeqNo")
            oSql.AppendLine(",FTPdtCode,FTPdtName,FTPthDocType")
            oSql.AppendLine(",FDPthDocDate ,FTPthVATInOrEx,FTPtdBarCode")
            oSql.AppendLine(",FTPtdStkCode,FCPtdStkFac,FTPtdVatType")
            oSql.AppendLine(",FTPtdSaleType,FTPunCode,FCPtdQtyAll")
            oSql.AppendLine(",FTPtdStaPdt,FTPtdStaRfd ,FTPtdStaPrcStk,FTPtdStaPrcStkCrd")
            oSql.AppendLine(",FNPthSign,FTWahCode,FNPtdStaRef")
            oSql.AppendLine(",FTPthWhTo,FTPthStaVatSend,FDDateUpd")
            oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FCPtdQty,FNPtdPdtLevel,FTWhoIns,FCPtdFactor,FTPtdUnitName) ")
            oSql.AppendLine("SELECT FTBchCode,FTPthDocNo,FNPtdSeqNo")
            oSql.AppendLine(",FTPdtCode,FTPdtName,FTPthDocType")
            oSql.AppendLine(",FDPthDocDate ,FTPthVATInOrEx,FTPtdBarCode")
            oSql.AppendLine(",FTPtdStkCode,FCPtdStkFac,FTPtdVatType")
            oSql.AppendLine(",FTPtdSaleType,FTPunCode,FCPtdQtyAll")
            oSql.AppendLine(",FTPtdStaPdt,FTPtdStaRfd ,FTPtdStaPrcStk,FTPtdStaPrcStkCrd")
            oSql.AppendLine(",FNPthSign,FTWahCode,FNPtdStaRef")
            oSql.AppendLine(",FTPthWhTo,FTPthStaVatSend,FDDateUpd")
            oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FCPtdQty,FNPtdPdtLevel,FTWhoIns,FCPtdFactor,FTPtdUnitName ")
            oSql.AppendLine("FROM TLNKPdtTnfDT")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If
            '------------------------ Procees 2 SAVE DT-----------------------------------------

            '------------------------ Procees 3 ปรับสต๊อก----------------------------------------
            oSql.Clear()
            oSql.Append("SELECT FTPthDocNo FROM TLNKPdtTnfHD")
            oDbTblHDDoc = oDatabase.C_GEToTblSQL(oSql.ToString())

            If oDbTblHDDoc.Rows.Count > 0 Then

                ' For Each oRow As DataRow In oDbTblHDDoc.Rows
                cCNVB.nVB_CNPdtSet = 0
                W_aDOCxApproved(oDbTblHDDoc)
                '  Next

            End If

            '------------------------ Procees 3 ปรับสต๊อก----------------------------------------

            '------------------------ Procees 4 ข้อมูลตาราง Temp----------------------------------'
            ' ลบข้อมูล table Temp HD
            oSql.Clear()
            oSql.AppendLine("DELETE FROM TLNKPdtTnfHD")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If

            ' ลบข้อมูล table Temp DT
            oSql.Clear()
            oSql.AppendLine("DELETE FROM TLNKPdtTnfDT")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If
            '------------------------ Procees 4 ข้อมูลตาราง Temp----------------------------------'
            Return True
        Catch ex As Exception
            Return False
        Finally
            nLang = Nothing
            oDatabase = Nothing
            oSql = Nothing
            tMsg = Nothing
        End Try

    End Function


    Private tW_TblDT As String = "TCNTPdtTnfDT"
    Private tW_TblHD As String = "TCNTPdtTnfHD"
    Private tW_StdDT As String = "Ptd"
    Private tW_StdHD As String = "Pth"

    ' Public Sub W_aDOCxApproved(ByVal ptDocNo As String)
    Public Sub W_aDOCxApproved(ByVal oDbTbl As DataTable)
        '----------------------------------------------------------
        '   Call:
        '   Cmt:    approved document
        '----------------------------------------------------------
        Dim tSql$, tCode$, tDocNo$, tWahC$
        Dim oSTP As New cProcessSTP
        Dim oDatabase As New cDatabaseLocal
        Dim oConnect As New SqlConnection
        Dim oException As New Exception
        Try
            oConnect.ConnectionString = AdaConfig.cConfig.tSQLConnSourceString
            If oConnect.State = ConnectionState.Closed Then
                oConnect.Open()
            End If

            '.STP_AlwCost = True
            '.STP_AlwInWha = True
            '.STP_AlwStkCrd = True
            '.STP_AlwReMonthEnd = True
            '.STP_AlwReMonthEnd = True
            '.STP_DocNo = Trim(ptDocNo)
            ''.STP_CNPdtSet = 0
            ''.STP_DecAmtForSav = 2
            ''.STP_UserName = cCNVB.tVB_UserName
            ''.STP_BchCode = "1090"
            ''

            For Each oRow As DataRow In oDbTbl.Rows
                With oSTP

                    .STP_AlwInWha = True
                    .STP_AlwStkCrd = True
                    .STP_AlwReMonthEnd = False 'เอกสารย้อนหลัง/ไม่ย้อน   false 
                    .STP_ReMonthEnd = False 'อนุญาต ReMonthEnd
                    .STP_BchCode = "000"
                    .STP_BchHQCode = "000"
                    .STP_DecAmtForSav = 2   ' 2
                    .STP_UserName = cCNVB.tVB_UserName
                    .STP_CNPdtSet = 0 'FTSysUsrValue
                    .STP_AdjStkpcVatRate = 0  '0
                    .STP_DocNo = oRow("FTPthDocNo")
                    .STP_AlwUpdQtyNow = True  'true
                    If .STP_PRCb1DocApprove(oConnect, cProcessSTP.eDB_DocType.nSTP_DOCxTCNTPdtTnfDT1) Then
                        tSql = "UPDATE " & tW_TblDT
                        tSql = tSql & " SET FT" & tW_StdDT & "StaPrcStk='1'"
                        tSql = tSql & "," & oDatabase.C_GETtSQLUpd(cCNVB.tVB_UserName)
                        tSql = tSql & " WHERE (" & tW_TblDT & ".FT" & tW_StdDT & "StaPrcStk='' OR " & tW_TblDT & ".FT" & tW_StdDT & "StaPrcStk IS NULL"
                        tSql = tSql & " AND FT" & tW_StdHD & "DocNo = '" & Trim(oRow("FTPthDocNo")) & "')"
                        If tSql <> "" Then
                            oDatabase.C_CALnExecuteNonQuery(tSql)
                        End If
                    Else
                        Exit Sub
                    End If
                End With

                tSql = "UPDATE " & tW_TblHD
                tSql = tSql & " SET FT" & tW_StdHD & "StaPrcDoc='1'"
                tSql = tSql & ",FT" & tW_StdHD & "ApvCode='" & cCNVB.tVB_UserCode & "'"
                ' tSql = tSql & "," & cUT.UT_SQLtLastUpd(tVB_CNUserName)
                tSql = tSql & "," & oDatabase.C_GETtSQLUpd(cCNVB.tVB_UserName)
                tSql = tSql & " WHERE FT" & tW_StdHD & "DocNo='" & Trim(oRow("FTPthDocNo")) & "'"

                If oDatabase.C_CALnExecuteNonQuery(tSql) <> 1 Then Exit Sub

                '=== ตัด Fn คงเหลือขายได้ 9/5/2018 ==='
                'If SP_PRCxPdtQtyNow(tW_TblDT, "FT" & tW_StdHD & "DocNo", "FT" & tW_StdDT & "StkCode", "FN" & tW_StdDT & "PdtLevel" _
                '        , "FC" & tW_StdDT & "QtyAll", oRow("FTPthDocNo"), "+", oException) = False Then

                '    Throw New Exception(oException.Message)
                'End If
                ' cLog.C_CALxWriteLog("QTYNow")
            Next

            ' With oSTP
            '    .STP_AlwInWha = True
            '    .STP_AlwStkCrd = True
            '    .STP_AlwReMonthEnd = False 'เอกสารย้อนหลัง/ไม่ย้อน   false 
            '    .STP_ReMonthEnd = False 'อนุญาต ReMonthEnd
            '    .STP_BchCode = "000"
            '    .STP_BchHQCode = "000"
            '    .STP_DecAmtForSav = 2   ' 2
            '    .STP_UserName = cCNVB.tVB_UserName
            '    .STP_CNPdtSet = 0 'FTSysUsrValue
            '    .STP_AdjStkpcVatRate = 0  '0
            '    .STP_DocNo = ptDocNo
            '    .STP_AlwUpdQtyNow = True  'true
            '    If .STP_PRCb1DocApprove(oConnect, cProcessSTP.eDB_DocType.nSTP_DOCxTCNTPdtTnfDT1) Then
            '        tSql = "UPDATE " & tW_TblDT
            '        tSql = tSql & " SET FT" & tW_StdDT & "StaPrcStk='1'"
            '        tSql = tSql & "," & oDatabase.C_GETtSQLUpd(cCNVB.tVB_UserName)
            '        tSql = tSql & " WHERE (" & tW_TblDT & ".FT" & tW_StdDT & "StaPrcStk='' OR " & tW_TblDT & ".FT" & tW_StdDT & "StaPrcStk IS NULL"
            '        tSql = tSql & " AND FT" & tW_StdHD & "DocNo = '" & Trim(ptDocNo) & "')"
            '        If tSql <> "" Then
            '            oDatabase.C_CALnExecuteNonQuery(tSql)
            '        End If
            '    Else
            '        Exit Sub
            '    End If
            'End With

            'tSql = "UPDATE " & tW_TblHD
            'tSql = tSql & " SET FT" & tW_StdHD & "StaPrcDoc='1'"
            'tSql = tSql & ",FT" & tW_StdHD & "ApvCode='" & cCNVB.tVB_UserCode & "'"
            '' tSql = tSql & "," & cUT.UT_SQLtLastUpd(tVB_CNUserName)
            'tSql = tSql & "," & oDatabase.C_GETtSQLUpd(cCNVB.tVB_UserName)
            'tSql = tSql & " WHERE FT" & tW_StdHD & "DocNo='" & Trim(ptDocNo) & "'"

            'If oDatabase.C_CALnExecuteNonQuery(tSql) <> 1 Then Exit Sub
            'If SP_PRCxPdtQtyNow(tW_TblDT, "FT" & tW_StdHD & "DocNo", "FT" & tW_StdDT & "StkCode", "FN" & tW_StdDT & "PdtLevel" _
            '        , "FC" & tW_StdDT & "QtyAll", ptDocNo, "-", oException) = False Then
            '    Throw New Exception(oException.Message)
            'End If






            ''''If Not bVB_CNPrcAll Then
            ''''    Call SP_MSGnShowing(tMS_CN040, tCS_CNMsgInfo)
            ''''    Call otbForm_LostFocus(0)
            ''''    '*WD 48-03-04
            ''''Else
            ''''    tVB_CNEvnCode = "PRC"
            ''''    tVB_CNEvnDesc = "Message Information"
            ''''    tVB_CNEvnOld = Me.Caption & " : process complete"
            ''''    tVB_CNEvnNew = cUT.UT_STRtToken(tMS_CN040, ";", nVB_CNCutTag)
            ''''    tVB_CNEvnNew = Replace(tVB_CNEvnNew, "'", "#", 1, , vbTextCompare)          '*WD 49-02-03
            ''''    Call SP_INSTSysLog(tVB_CNEvnCode, tVB_CNEvnDesc, tVB_CNEvnOld, tVB_CNEvnNew)
            ''''    '***
            ''''End If
        Catch ex As Exception

        End Try

    End Sub

    Class cFileInfo
        Property tPathFile As String = ""
        Property nItem As Integer = 0
        Property tDocDate As String = ""
        Property tBchCode As String = ""
    End Class
End Class
