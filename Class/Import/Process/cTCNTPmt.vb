﻿Imports System.IO
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Threading

Public Class cTCNTPmt

    'Public Delegate Sub ProgressDelegate(pnValue As Integer, pnCount As Integer)
    'Public Event EventProgress As ProgressDelegate

    'For Sql
    Property tDateFrom As String = ""
    Property tDateTo As String = ""
    Property nMode As Integer = 0 '0:All,1:Interval,2:Select
    Property tCodeFrom As String = ""
    Property tCodeTo As String = ""
    Property tCodeSelect As String = ""
    ''
    Property tFullPath As String = ""
    Property tOutPath As String = ""
    Property tHDTable As String = ""
    Property tFileLog As String = ""
    'Private oDatabase As New cDatabaseLocal
    'Private aFileExport As New List(Of cFileInfo)
    'Private Const tC_ExpTbl As String = "TLNKExPmtHis"

    Property cNextVal As Double = 0
    Property cProgress As Double = 0
    Private nC_ValPropress As Double = 0
    Private Const tC_Table As String = "TCNTPmt"
    Private Const tC_TableTemp As String = "TLNKPmt"

    'Process
    Public Function C_CALbProcess() As Boolean
        C_CALbProcess = True
        Dim nLang As Integer = 0
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTblHD As New DataTable
        Dim oDbTblDT As New DataTable
        Dim oDbTblCD As New DataTable
        Dim oSql As System.Text.StringBuilder
        Dim aSqlB4 As New List(Of String)
        Dim tMsg As String = ""
        Try
            cNextVal = cNextVal \ 3
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLang = 1

            If Not cCNVB.bVB_Auto Then '1 Progress
                cProgress += cNextVal
                cProgress = IIf(nC_ValPropress > 100, 100, cProgress)
                wImports.oW_BackgroudWord.ReportProgress(cProgress)
            End If

            ''Update PdtCode,PdtName
            'oSql = New System.Text.StringBuilder
            'oSql.AppendLine("UPDATE Temp SET")
            'oSql.AppendLine("Temp.FTPdtCode = Pdt.FTPdtCode,")
            'oSql.AppendLine("Temp.FTPdtName = Pdt.FTPdtName")
            'oSql.AppendLine("FROM " & tC_TableTemp & "DT Temp INNER JOIN TCNMPdt Pdt ON Temp.FTPmdBarCode = Pdt.FTPdtBarCode1")
            'oSql.AppendLine("INNER JOIN TCNMPdtUnit Pun ON Pdt.FTPdtSUnit = Pun.FTPunCode")
            'oSql.AppendLine("WHERE Temp.FTPmdUnitName = Pun.FTPunName")
            'aSqlB4.Add(oSql.ToString)
            'oSql = New System.Text.StringBuilder
            'oSql.AppendLine("UPDATE Temp SET")
            'oSql.AppendLine("Temp.FTPdtCode = Pdt.FTPdtCode,")
            'oSql.AppendLine("Temp.FTPdtName = Pdt.FTPdtName")
            'oSql.AppendLine("FROM " & tC_TableTemp & "DT Temp INNER JOIN TCNMPdt Pdt ON Temp.FTPmdBarCode = Pdt.FTPdtBarCode2")
            'oSql.AppendLine("INNER JOIN TCNMPdtUnit Pun ON Pdt.FTPdtMUnit = Pun.FTPunCode")
            'oSql.AppendLine("WHERE Temp.FTPmdUnitName = Pun.FTPunName")
            'aSqlB4.Add(oSql.ToString)
            'oSql = New System.Text.StringBuilder
            'oSql.AppendLine("UPDATE Temp SET")
            'oSql.AppendLine("Temp.FTPdtCode = Pdt.FTPdtCode,")
            'oSql.AppendLine("Temp.FTPdtName = Pdt.FTPdtName")
            'oSql.AppendLine("FROM " & tC_TableTemp & "DT Temp INNER JOIN TCNMPdt Pdt ON Temp.FTPmdBarCode = Pdt.FTPdtBarCode3")
            'oSql.AppendLine("INNER JOIN TCNMPdtUnit Pun ON Pdt.FTPdtLUnit = Pun.FTPunCode")
            'oSql.AppendLine("WHERE Temp.FTPmdUnitName = Pun.FTPunName")
            'aSqlB4.Add(oSql.ToString)
            'oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)
            'aSqlB4.Clear()

            'oSql = New System.Text.StringBuilder
            'oSql.AppendLine("UPDATE Pmh SET")
            'oSql.AppendLine("FNLnkStatus=3,")
            'oSql.AppendLine("FTLnkLog='FTPmdBarCode,ไม่มีอยู่จริงในฐานข้อมูล'")
            'oSql.AppendLine("FROM " & tC_TableTemp & "HD Pmh INNER JOIN " & tC_TableTemp & "DT Pmd ON Pmh.FTPmhCode = Pmd.FTPmhCode")
            'oSql.AppendLine("WHERE (ISNULL(Pmd.FTPdtCode,'') = '' AND ISNULL(Pmd.FTPdtName,'') = '')")
            'oSql.AppendLine("AND FNLnkStatus <> 3")
            'oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            If Not cCNVB.bVB_Auto Then '2 Progress
                cProgress += cNextVal
                cProgress = IIf(nC_ValPropress > 100, 100, cProgress)
                wImports.oW_BackgroudWord.ReportProgress(cProgress)
            End If

            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTBchCode,FTPmhCode,FTPmhName,")
            oSql.AppendLine("FTPmhNameSlip,FTSpmCode,FTSpmType,")
            oSql.AppendLine("FDPmhDStart,FDPmhDStop,FDPmhTStart,")
            oSql.AppendLine("FDPmhTStop,FTPmhClosed,FTPmhStatus,")
            oSql.AppendLine("FTPmhRetOrWhs,FTPmhRmk,FTPmhStaPrcDoc,")
            oSql.AppendLine("FNPmhStaAct,FTDptCode,FTUsrCode,")
            oSql.AppendLine("FTPmhApvCode,FTPmhBchTo,FTPmhZneTo,")
            oSql.AppendLine("FTPmhStaExceptPmt,FTSpmStaRcvFree,FTSpmStaAlwOffline,")
            oSql.AppendLine("FTSpmStaChkLimitGet,FNPmhLimitNum,FTPmhStaLimit,")
            oSql.AppendLine("FTPmhStaLimitCst,FTSpmStaChkCst,FNPmhCstNum,")
            oSql.AppendLine("FTSpmStaChkCstDOB,FNPmhCstDobNum,FNPmhCstDobPrev,")
            oSql.AppendLine("FNPmhCstDobNext,FTSpmStaUseRange,FTSplCode,")
            oSql.AppendLine("FDPntSplStart,FDPntSplExpired,FTCgpCode,")
            oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd,")
            oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns")
            oSql.AppendLine("FROM " & tC_TableTemp & "HD")
            oSql.AppendLine("WHERE FNLnkStatus = 1 ORDER BY FTPmhCode")
            oDbTblHD = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oDbTblHD.TableName = tC_Table & "HD"

            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTBchCode,FTPmhCode,FNPmdSeq,")
            oSql.AppendLine("FTSpmCode,FTPmdGrpType,FTPmdGrpName,")
            oSql.AppendLine("FTPdtCode,FTPmdBarCode,FTPdtName,")
            oSql.AppendLine("FTPmdUnitName,FCPmdSetPriceOrg,")
            oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd,")
            oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns")
            oSql.AppendLine("FROM " & tC_TableTemp & "DT")
            oSql.AppendLine("WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 1)")
            oDbTblDT = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oDbTblDT.TableName = tC_Table & "DT"

            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTBchCode,FTPmhCode,FNPmcSeq,")
            oSql.AppendLine("FTSpmCode,FTPmcGrpName,FTPmcStaGrpCond,")
            oSql.AppendLine("FCPmcPerAvgDis,FCPmcBuyAmt,FCPmcBuyQty,")
            oSql.AppendLine("FCPmcBuyMinQty,FCPmcBuyMaxQty,FDPmcBuyMinTime,")
            oSql.AppendLine("FDPmcBuyMaxTime,FCPmcGetCond,FCPmcGetValue,")
            oSql.AppendLine("FCPmcGetQty,FTSpmStaBuy,FTSpmStaRcv,FTSpmStaAllPdt,")
            oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd,")
            oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns")
            oSql.AppendLine("FROM " & tC_TableTemp & "CD")
            oSql.AppendLine("WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 1)")
            oDbTblCD = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oDbTblCD.TableName = tC_Table & "CD"

            If oDbTblHD.Rows.Count > 0 And oDbTblDT.Rows.Count > 0 And oDbTblCD.Rows.Count > 0 Then
                tMsg = C_COPtData(oDbTblHD)
                If tMsg = "" Then tMsg = C_COPtData(oDbTblDT)
                If tMsg = "" Then tMsg = C_COPtData(oDbTblCD)
                oDbTblHD.Clear()
                oDbTblDT.Clear()
                oDbTblCD.Clear()

                If tMsg = "" Then
                    oSql.AppendLine("UPDATE " & tC_TableTemp & "HD SET FNLnkPrc=1,FTLnkLog='" & cCNVB.tVB_SuccessImp & "' WHERE FNLnkStatus = 1")
                    oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
                Else
                    'DELETE
                    aSqlB4.Add("DELETE " & tC_Table & "HD WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 1)")
                    aSqlB4.Add("DELETE " & tC_Table & "DT WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "DT WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 1))")
                    aSqlB4.Add("DELETE " & tC_Table & "CD WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "CD WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FNLnkStatus = 1))")

                    'UPDATE Status Temp
                    oSql = New System.Text.StringBuilder
                    oSql.AppendLine("UPDATE " & tC_TableTemp & "HD")
                    oSql.AppendLine("FNLnkStatus=3,")
                    oSql.AppendLine("FTLnkLog='" & Left(tMsg, 255) & "'")
                    oSql.AppendLine("WHERE FNLnkStatus = 1")
                    aSqlB4.Add(oSql.ToString)
                    oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)
                    aSqlB4.Clear()
                End If
            End If

            If Not cCNVB.bVB_Auto Then '3 Progress
                cProgress += cNextVal
                cProgress = IIf(nC_ValPropress > 100, 100, cProgress)
                wImports.oW_BackgroudWord.ReportProgress(cProgress)
            End If

            Me.C_CALxBakAndLog()

            'บันทึก TLNKLog
            Dim tDesc As String = ""
            aSqlB4.Add("SELECT 'Insert('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tC_TableTemp & "HD WHERE FNLnkStatus=1 AND FNLnkPrc=1")
            aSqlB4.Add("SELECT 'Update('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tC_TableTemp & "HD WHERE FNLnkStatus=2 AND FNLnkPrc=1")
            aSqlB4.Add("SELECT 'Failed('+CAST(ISNULL(COUNT(FNLnkStatus),0) AS VARCHAR(10))+')' FROM " & tC_TableTemp & "HD WHERE FNLnkStatus=3 AND FNLnkPrc=0")
            For Each tSql In aSqlB4
                If tDesc.Length > 0 Then
                    tDesc &= ","
                End If

                Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                    oSQLConn.Open()
                    Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                    Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                    While oSQLRdWhe.Read()
                        tDesc &= oSQLRdWhe(0)
                    End While
                End Using
            Next

            If File.Exists(tFileLog) = False Then tFileLog = ""
            oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFullPath), tDesc, tFileLog))
            aSqlB4.Clear()

            Dim nFail As Integer = 0
            oSql = New System.Text.StringBuilder
            oSql.Append("SELECT ISNULL(COUNT(FNLnkStatus),0) FROM " & tC_TableTemp & "HD WHERE FNLnkStatus=3 AND FNLnkPrc=0")
            Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSQLConn.Open()
                Dim oSQLCmdWhe As New SqlCommand(oSql.ToString, oSQLConn)
                Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                While oSQLRdWhe.Read()
                    nFail = Convert.ToInt32(oSQLRdWhe(0))
                End While
            End Using
            If nFail > 0 Then
                C_CALbProcess = False
            Else
                C_CALbProcess = True
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function

    Class cFileInfo
        Property tPathFile As String = ""
        Property nItem As Integer = 0
        Property tDocDate As String = ""
        Property tBchCode As String = ""
    End Class

    Private Sub C_CALxBakAndLog()
        Dim oFileLog As New cImportLog(tFileLog)
        Dim tSql As String = ""
        tSql = "SELECT FNLnkLine,'" & Path.GetFileName(tFullPath) & "' AS FileName,FTPmhCode,ISNULL(FTPmhName,'') AS FTPmhName,FTLnkLog "
        tSql &= "FROM " & tC_TableTemp & "HD "
        'tSql &= "WHERE FNLnkStatus=3 ORDER BY FNLnkLine"
        Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oSQLConn.Open()
            Dim oSQLCmd As New SqlCommand(tSql, oSQLConn)
            Dim oSQLRead As SqlDataReader = oSQLCmd.ExecuteReader()
            Dim tErr As String = ""
            While oSQLRead.Read()
                tErr = oSQLRead(4)
                If tErr.Length > 0 Then
                    Dim oNewLog As New cImportLog.cLog
                    With oNewLog
                        .nLineNo = oSQLRead(0)
                        .tType = oSQLRead(1)
                        If tErr.StartsWith("Format") Then
                            .tDocNo = ""
                            .tSeqNo = ""
                            .tCode = ""
                            .tName = ""
                        Else
                            .tDocNo = oSQLRead(2)
                            .tName = oSQLRead(3)
                        End If

                        If tErr.Split(",").Count = 2 Then
                            .tErrField = tErr.Split(",")(0)
                            .tErrDesc = tErr.Split(",")(1)
                        Else
                            .tErrDesc = oSQLRead(4)
                        End If
                    End With
                    oFileLog.C_CALxAddNew(oNewLog)
                End If
            End While
            oSQLConn.Close()
        End Using
        oFileLog.C_CALbSave("2")
    End Sub
End Class
