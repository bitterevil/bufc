﻿Public Class DataColumns
    Inherits DataColumn

    Public Property TableName() As String
        Get
            Return Me.Namespace
        End Get
        Set(ByVal value As String)
            Me.Namespace = value
        End Set
    End Property

End Class
