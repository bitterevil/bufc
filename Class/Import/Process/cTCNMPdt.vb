﻿Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports System.Threading
Imports AdaLinkSAPBUFC.cJobTCNMPdt

Public Class cTCNMPdt
    Property tFullPath As String = ""
    Property tTableTemp As String = ""
    Property tTable As String = ""
    Property cNextVal As Double = 0
    Property cProgress As Double = 0
    Property tDateNow As String = ""
    Property tFileLog As String = ""
    Private tSpr As String = AdaConfig.cConfig.tSeparator
    'Private tC_TempLnkPdt As String = "TLNKTmpPdt"
    Private nC_PdtItem As Integer = 0
    Private nC_ValPropress As Double = 0
    Private nC_NextVal As Double = 0

    Class cProduct
        Property FTPdtCode As String = ""
        Property FTPdtName As String = ""
        Property FTPdtNameOth As String = ""
        Property FTPdtBarCode1 As String = ""
        Property FTPdtBarCode2 As String = "NULL"
        Property FTPdtBarCode3 As String = "NULL"
        Property FTPdtStkCode As String = ""
        Property FTPgpChain As String = ""
        Property FTSplCode As String = ""
        Property FTPdtVatType As String = ""
        Property FTPdtSaleType As String = ""
        Property FTPdtSUnit As String = ""
        Property FCPdtSFactor As String = "0.0"
        Property FTPdtMUnit As String = ""
        Property FCPdtMFactor As String = "0.0"
        Property FTPdtLUnit As String = ""
        Property FCPdtLFactor As String = "0.0"
        Property FCPdtRetPriS1 As String = "0.0"
        Property FCPdtRetPriM1 As String = "0.0"
        Property FCPdtRetPriL1 As String = "0.0"
        Property FTPbnCode As String = ""
        Property FTPmoCode As String = ""
        Property FTPdtNoDis As String = ""
        Property FTPdtGrade As String = ""
        Property FTPdtPoint As String = ""
        Property FCPdtPointTime As String = "0.0"
        Property FTPdtType As String = ""
        Property FTPdtStaActive As String = ""
        Property FTPdtRmk As String = ""
        Property FTPdtDim As String = ""
        Property FTPdtPkgDim As String = ""
        Property FTPdtGateWhe As String = ""
        Property FTPdtDeliveryBy As String = ""
        Property FDPdtMfg As String = "NULL" 'Fix *CH 12-01-2015
        Property FNPdtAge As String = "" 'Age of product *CH 20-01-2015
        Property FDPdtExp As String = ""
        Property FTPdtUnitSymbol As String = ""
        Property FTPdtLabSizeS As String = "1"
        Property FNPdtLabQtyS As String = "1"
        Property FTPdtLabSizeM As String = "1"
        Property FNPdtLabQtyM As String = "1"
        Property FTPdtLabSizeL As String = "1"
        Property FNPdtLabQtyL As String = "1"
        Property FDPdtOrdStart As String = ""
        Property FDPdtOrdStop As String = ""
        Property FTPtyCode As String = ""       '*Em 58-02-05
        Property FCPdtStkFac As String = "1"
        Property FTPdtStkControl As String = "1"
        Property FCPdtQtyRet As String = "0"
        Property FCPdtQtyWhs As String = "0"
        Property FCPdtQtyDRet As String = "0"
        Property FCPdtQtyDWhs As String = "0"
        Property FCPdtQtyMRet As String = "0"
        Property FCPdtQtyMWhs As String = "0"
        Property FCPdtQtyOrdBuy As String = "0"
        Property FCPdtCostAvg As String = "0"
        Property FCPdtCostFiFo As String = "0"
        Property FCPdtCostLast As String = "0"
        Property FCPdtCostDef As String = "0"
        Property FCPdtCostOth As String = "0"
        Property FCPdtCostAmt As String = "0"
        Property FCPdtCostStd As String = "0"
        Property FTUsrCode As String = ""
        Property FCPdtMin As String = "0"
        Property FCPdtMax As String = "0"
        Property FCPdtWeight As String = "0"
        Property FTPszCode As String = ""
        Property FTClrCode As String = ""
        Property FTPmhCodeS As String = ""
        Property FTPmhTypeS As String = ""
        Property FDPdtPmtSDateS As String = "NULL"
        Property FDPdtPmtEDateS As String = "NULL"
        Property FTPdtPmtTypeS As String = ""
        Property FTPmhCodeM As String = ""
        Property FTPmhTypeM As String = ""
        Property FDPdtPmtSDateM As String = "NULL"
        Property FDPdtPmtEDateM As String = "NULL"
        Property FTPdtPmtTypeM As String = ""
        Property FTPmhCodeL As String = ""
        Property FTPmhTypeL As String = ""
        Property FDPdtPmtSDateL As String = "NULL"
        Property FDPdtPmtEDateL As String = "NULL"
        Property FTPdtPmtTypeL As String = ""
        Property FTPdtConType As String = "1"
        Property FTPdtSrn As String = "2"
        Property FTPdtPlcCodeS As String = ""
        Property FTPdtPlcCodeM As String = ""
        Property FTPdtPlcCodeL As String = ""
        Property FTPdtAlwOrderS As String = "1"
        Property FTPdtAlwOrderM As String = "1"
        Property FTPdtAlwOrderL As String = "1"
        Property FTPdtAlwBuyS As String = "1"
        Property FTPdtAlwBuyM As String = "1"
        Property FTPdtAlwBuyL As String = "1"
        Property FCPdtRetPriS2 As String = "0"
        Property FCPdtRetPriM2 As String = "0"
        Property FCPdtRetPriL2 As String = "0"
        Property FCPdtRetPriS3 As String = "0"
        Property FCPdtRetPriM3 As String = "0"
        Property FCPdtRetPriL3 As String = "0"
        Property FCPdtWhsPriS1 As String = "0"
        Property FCPdtWhsPriM1 As String = "0"
        Property FCPdtWhsPriL1 As String = "0"
        Property FCPdtWhsPriS2 As String = "0"
        Property FCPdtWhsPriM2 As String = "0"
        Property FCPdtWhsPriL2 As String = "0"
        Property FCPdtWhsPriS3 As String = "0"
        Property FCPdtWhsPriM3 As String = "0"
        Property FCPdtWhsPriL3 As String = "0"
        Property FCPdtWhsPriS4 As String = "0"
        Property FCPdtWhsPriM4 As String = "0"
        Property FCPdtWhsPriL4 As String = "0"
        Property FCPdtWhsPriS5 As String = "0"
        Property FCPdtWhsPriM5 As String = "0"
        Property FCPdtWhsPriL5 As String = "0"
        Property FTPdtWhsDefUnit As String = "1"
        Property FTPdtOthPurSplN1 As String = ""
        Property FTPdtOthPurSplN2 As String = ""
        Property FTPdtOthPurSplN3 As String = ""
        Property FCPdtOthPurSplP1 As String = "0"
        Property FCPdtOthPurSplP2 As String = "0"
        Property FCPdtOthPurSplP3 As String = "0"
        Property FTPdtOthPurSplCmt As String = "0"
        Property FTPdtOthSleSplN1 As String = ""
        Property FTPdtOthSleSplN2 As String = ""
        Property FTPdtOthSleSplN3 As String = ""
        Property FCPdtOthSleSplP1 As String = "NULL"
        Property FCPdtOthSleSplP2 As String = "NULL"
        Property FCPdtOthSleSplP3 As String = "NULL"
        Property FTPdtOthSleSplCmt As String = ""
        Property FTAccCode As String = ""
        Property FTPdtPic As String = ""
        Property FTPdtSound As String = ""
        Property FTPdtStaDel As String = ""
        Property FTPdtBarByGen As String = ""
        Property FTPdtStaSet As String = "1"
        Property FTPdtStaSetPri As String = "1"
        Property FTPdtStaSetShwDT As String = ""
        Property FCPdtLeftPO As String = "0"
        Property FTPdtTax As String = "1"
        Property FTDcsCode As String = ""
        Property FTDepCode As String = ""
        Property FTClsCode As String = ""
        Property FTSclCode As String = ""
        Property FCPdtQtyNow As String = "0"
        Property FTPdtArticle As String = ""
        Property FTPdtGrpControl As String = "2"
        Property FTPdtShwTch As String = ""
        Property FCPdtVatRate As String = "NULL"
        Property FTTcgCode As String = ""
        Property FTPdtNameShort As String = ""
        Property FTPdtNameShortEng As String = ""
        Property FTPdtStaAlwBuy As String = "1"
        Property FTFixGonCode As String = ""
        Property FCPdtLifeCycle As String = "0"
        Property FTPdtOrdDay As String = ""
        Property FTPdtOrdSun As String = "1"
        Property FTPdtOrdMon As String = "1"
        Property FTPdtOrdTue As String = "1"
        Property FTPdtOrdWed As String = "1"
        Property FTPdtOrdThu As String = "1"
        Property FTPdtOrdFri As String = "1"
        Property FTPdtOrdSat As String = "1"
        Property FCPdtLeadTime As String = "0"
        Property FDPdtSaleStart As String = Format(Now, "yyyy/MM/dd")
        Property FDPdtSaleStop As String = "9999-12-31"
        Property FTPdtStaTouch As String = "2"
        Property FTPdtStaAlwRepack As String = "3"
        Property FNPdtPalletSize As String = "0"
        Property FNPdtPalletLev As String = "0"
        Property FNPdtAgeB4Rcv As String = "0"
        Property FNPdtAgeB4Snd As String = "0"
        Property FTItyCode As String = ""
        Property FTMcrCode As String = ""
        Property FTSlcItemCode As String = ""
        Property FTPdtPic4Slip As String = ""
        Property FDDateUpd As String = ""
        Property FTTimeUpd As String = ""
        Property FTWhoUpd As String = ""
        Property FDDateIns As String = ""
        Property FTTimeIns As String = ""
        Property FTWhoIns As String = ""
        Property FTCompName As String = My.Computer.Name
        Property FNLNKLine As String = ""
        Property FTLNKStatus As String = ""
        Property FTLNKLog As String = ""
        'Property FTPbnName As String = ""
        'Property FTPmoName As String = ""
    End Class

    ''' <summary>
    ''' INSERT ตาราง Temp pdt เข้า ตาราง จริง
    ''' </summary>
    ''' <returns></returns>
    Public Function C_CALbProcess() As Boolean
        C_CALbProcess = True
        Dim tChkStk As String = ""
        Dim nLang As Integer = 0
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim oSql As New System.Text.StringBuilder
        Dim aSqlB4 As New List(Of String)
        Dim tMsg As String = ""
        Try
            cNextVal = cNextVal \ 3
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLang = 1

            If Not cCNVB.bVB_Auto Then '1 Progress
                cProgress += cNextVal
                cProgress = IIf(nC_ValPropress > 100, 100, cProgress)
                wImports.oW_BackgroudWord.ReportProgress(cProgress)
            End If

            oSql.Clear()
            oSql.AppendLine("INSERT INTO TCNMPdt(FTPdtCode,FTPdtName,FTPdtNameOth,FTPdtBarCode1")
            oSql.AppendLine(",FTPdtStkCode,FCPdtStkFac,FTPdtStkControl,FCPdtQtyRet")
            oSql.AppendLine(",FCPdtQtyWhs, FTPdtVatType, FTPdtSaleType,FTPdtSUnit")
            oSql.AppendLine(",FCPdtSFactor,FTPtyCode, FTPdtPoint,FCPdtPointTime")
            oSql.AppendLine(",FTPdtConType,FTPdtSrn,FTPdtAlwOrderS,FTPdtAlwOrderM")
            oSql.AppendLine(",FTPdtAlwOrderL,FTPdtAlwBuyS,FTPdtAlwBuyM,FTPdtAlwBuyL")
            oSql.AppendLine(",FCPdtRetPriS1,FTPdtWhsDefUnit,FTPdtType,FTPdtStaSet")
            oSql.AppendLine(",FTPdtStaSetPri,FTPdtStaActive,FTPbnCode,FTPmoCode")
            oSql.AppendLine(",FTPdtTax,FTPdtGrpControl,FTPdtNoDis,FCPdtVatRate")
            oSql.AppendLine(",FTPdtStaAlwBuy,FDPdtOrdStart,FDPdtOrdStop,FDPdtSaleStart")
            oSql.AppendLine(",FDPdtSaleStop,FTPdtStaTouch,FTPdtStaAlwRepack,FDDateUpd")
            oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns) ")
            oSql.AppendLine("SELECT FTPdtCode,FTPdtName,FTPdtNameOth,FTPdtBarCode1")
            oSql.AppendLine(",FTPdtStkCode,FCPdtStkFac,FTPdtStkControl,FCPdtQtyRet")
            oSql.AppendLine(",FCPdtQtyWhs, FTPdtVatType, FTPdtSaleType,FTPdtSUnit")
            oSql.AppendLine(",FCPdtSFactor,FTPtyCode, FTPdtPoint,FCPdtPointTime")
            oSql.AppendLine(",FTPdtConType,FTPdtSrn,FTPdtAlwOrderS,FTPdtAlwOrderM")
            oSql.AppendLine(",FTPdtAlwOrderL,FTPdtAlwBuyS,FTPdtAlwBuyM,FTPdtAlwBuyL")
            oSql.AppendLine(",FCPdtRetPriS1,FTPdtWhsDefUnit,FTPdtType,FTPdtStaSet")
            oSql.AppendLine(",FTPdtStaSetPri,FTPdtStaActive,FTPbnCode,FTPmoCode")
            oSql.AppendLine(",FTPdtTax,FTPdtGrpControl,FTPdtNoDis,FCPdtVatRate")
            oSql.AppendLine(",FTPdtStaAlwBuy,FDPdtOrdStart,FDPdtOrdStop,FDPdtSaleStart")
            oSql.AppendLine(",FDPdtSaleStop,FTPdtStaTouch,FTPdtStaAlwRepack,FDDateUpd")
            oSql.AppendLine(",FTTimeUpd,FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns FROM TLNKPdt")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))

            If Not cCNVB.bVB_Auto Then '2 Progress
                cProgress += cNextVal
                cProgress = IIf(nC_ValPropress > 100, 100, cProgress)
                wImports.oW_BackgroudWord.ReportProgress(cProgress)
            End If

            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Function

    Private Function C_CRTxDBTCNMPdt(ptTable As String) As DataTable
        'EYE 59-02-25 สร้าง Datatable เพื่อเก็บข้อมูลก่อนการ Insert to TCNMPdt
        Dim oDT As New DataTable(ptTable)
        oDT.Columns.Add("FTPdtCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtName", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtNameOth", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtBarCode1", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtBarCode2", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtBarCode3", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStkCode", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtStkFac", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtStkControl", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtQtyRet", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtQtyWhs", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtQtyDRet", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtQtyDWhs", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtQtyMRet", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtQtyMWhs", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtQtyOrdBuy", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtCostAvg", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtCostFiFo", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtCostLast", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtCostDef", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtCostOth", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtCostAmt", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtCostStd", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPgpChain", Type.GetType("System.String"))
        oDT.Columns.Add("FTSplCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTUsrCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtVatType", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtMin", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtMax", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtSaleType", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtSUnit", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtSFactor", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtMUnit", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtMFactor", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtLUnit", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtLFactor", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtGrade", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtWeight", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPszCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTClrCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPtyCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtPoint", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtPointTime", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPmhCodeS", Type.GetType("System.String"))
        oDT.Columns.Add("FTPmhTypeS", Type.GetType("System.String"))
        oDT.Columns.Add("FDPdtPmtSDateS", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FDPdtPmtEDateS", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTPdtPmtTypeS", Type.GetType("System.String"))
        oDT.Columns.Add("FTPmhCodeM", Type.GetType("System.String"))
        oDT.Columns.Add("FTPmhTypeM", Type.GetType("System.String"))
        oDT.Columns.Add("FDPdtPmtSDateM", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FDPdtPmtEDateM", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTPdtPmtTypeM", Type.GetType("System.String"))
        oDT.Columns.Add("FTPmhCodeL", Type.GetType("System.String"))
        oDT.Columns.Add("FTPmhTypeL", Type.GetType("System.String"))
        oDT.Columns.Add("FDPdtPmtSDateL", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FDPdtPmtEDateL", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTPdtPmtTypeL", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtConType", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtSrn", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtPlcCodeS", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtPlcCodeM", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtPlcCodeL", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtAlwOrderS", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtAlwOrderM", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtAlwOrderL", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtAlwBuyS", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtAlwBuyM", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtAlwBuyL", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtRetPriS1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriM1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriL1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriS2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriM2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriL2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriS3", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriM3", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtRetPriL3", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriS1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriM1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriL1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriS2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriM2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriL2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriS3", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriM3", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriL3", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriS4", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriM4", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriL4", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriS5", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriM5", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtWhsPriL5", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtWhsDefUnit", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOthPurSplN1", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOthPurSplN2", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOthPurSplN3", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtOthPurSplP1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtOthPurSplP2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtOthPurSplP3", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtOthPurSplCmt", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOthSleSplN1", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOthSleSplN2", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOthSleSplN3", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtOthSleSplP1", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtOthSleSplP2", Type.GetType("System.Double"))
        oDT.Columns.Add("FCPdtOthSleSplP3", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtOthSleSplCmt", Type.GetType("System.String"))
        oDT.Columns.Add("FTAccCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtPic", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtSound", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStaDel", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtType", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtBarByGen", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStaSet", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStaSetPri", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStaSetShwDT", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStaActive", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtRmk", Type.GetType("System.String"))
        oDT.Columns.Add("FTPbnCode", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtLeftPO", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtTax", Type.GetType("System.String"))
        oDT.Columns.Add("FTPmoCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTDcsCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTDepCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTClsCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTSclCode", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtQtyNow", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtArticle", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtGrpControl", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtShwTch", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtNoDis", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtVatRate", Type.GetType("System.Double"))
        oDT.Columns.Add("FTTcgCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtNameShort", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtNameShortEng", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStaAlwBuy", Type.GetType("System.String"))
        oDT.Columns.Add("FTFixGonCode", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtLifeCycle", Type.GetType("System.Double"))
        oDT.Columns.Add("FTPdtOrdDay", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOrdSun", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOrdMon", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOrdTue", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOrdWed", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOrdThu", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOrdFri", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtOrdSat", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtDim", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtPkgDim", Type.GetType("System.String"))
        oDT.Columns.Add("FCPdtLeadTime", Type.GetType("System.Double"))
        oDT.Columns.Add("FDPdtOrdStart", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FDPdtOrdStop", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FDPdtSaleStart", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FDPdtSaleStop", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTPdtStaTouch", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtStaAlwRepack", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtGateWhe", Type.GetType("System.String"))
        oDT.Columns.Add("FNPdtPalletSize", Type.GetType("System.Int32"))
        oDT.Columns.Add("FNPdtPalletLev", Type.GetType("System.Int32"))
        oDT.Columns.Add("FNPdtAge", Type.GetType("System.Int32"))
        oDT.Columns.Add("FNPdtAgeB4Rcv", Type.GetType("System.Int32"))
        oDT.Columns.Add("FNPdtAgeB4Snd", Type.GetType("System.Int32"))
        oDT.Columns.Add("FTItyCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtDeliveryBy", Type.GetType("System.String"))
        oDT.Columns.Add("FTMcrCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTSlcItemCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtPic4Slip", Type.GetType("System.String"))
        oDT.Columns.Add("FDPdtMfg", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FDPdtExp", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTPdtUnitSymbol", Type.GetType("System.String"))
        oDT.Columns.Add("FTPdtLabSizeS", Type.GetType("System.String"))
        oDT.Columns.Add("FNPdtLabQtyS", Type.GetType("System.Int32"))
        oDT.Columns.Add("FTPdtLabSizeM", Type.GetType("System.String"))
        oDT.Columns.Add("FNPdtLabQtyM", Type.GetType("System.Int32"))
        oDT.Columns.Add("FTPdtLabSizeL", Type.GetType("System.String"))
        oDT.Columns.Add("FNPdtLabQtyL", Type.GetType("System.String"))
        oDT.Columns.Add("FDDateUpd", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTTimeUpd", Type.GetType("System.String"))
        oDT.Columns.Add("FTWhoUpd", Type.GetType("System.String"))
        oDT.Columns.Add("FDDateIns", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTTimeIns", Type.GetType("System.String"))
        oDT.Columns.Add("FTWhoIns", Type.GetType("System.String"))


        Return oDT
    End Function

    Private Function C_SEToModelPdt(ByVal poPdt As cProduct, ByVal poDbRow As DataRow, ByVal ptPdtCode As String, ByVal pnBar As Integer) As cProduct
        Dim oPdt As New cProduct
        Try
            oPdt = poPdt
            With oPdt
                Select Case pnBar
                    Case 1
                        .FTPdtCode = poDbRow("FTPdtStkCode") & IIf(Convert.ToInt32(ptPdtCode) > 1, "-" & Convert.ToInt32(ptPdtCode) - 1, "")
                        .FTPdtName = poDbRow("FTPdtName")
                        .FTPdtNameOth = poDbRow("FTPdtNameOth")
                        .FTPdtBarCode1 = poDbRow("FTPdtBarCode1")
                        .FTPdtStkCode = poDbRow("FTPdtStkCode")
                        .FTPgpChain = poDbRow("FTPgpChain")
                        .FTSplCode = poDbRow("FTSplCode")
                        .FTPdtVatType = poDbRow("FTPdtVatType")
                        .FTPdtSaleType = poDbRow("FTPdtSaleType")
                        .FTPdtSUnit = poDbRow("FTPdtSUnit")
                        .FCPdtSFactor = poDbRow("FCPdtSFactor")
                        .FCPdtRetPriS1 = poDbRow("FCPdtRetPriS1")
                        .FTPbnCode = poDbRow("FTPbnCode")
                        .FTPmoCode = poDbRow("FTPmoCode")
                        .FTPdtNoDis = poDbRow("FTPdtNoDis")
                        .FTPdtGrade = poDbRow("FTPdtGrade")
                        .FTPdtPoint = poDbRow("FTPdtPoint")
                        .FCPdtPointTime = poDbRow("FCPdtPointTime")
                        .FTPdtType = poDbRow("FTPdtType")
                        .FTPdtStaActive = poDbRow("FTPdtStaActive")
                        .FTPdtRmk = poDbRow("FTPdtRmk")
                        .FTPdtDim = poDbRow("FTPdtDim")
                        .FTPdtPkgDim = poDbRow("FTPdtPkgDim")
                        .FTPdtGateWhe = poDbRow("FTPdtGateWhe")
                        .FTPdtDeliveryBy = poDbRow("FTPdtDeliveryBy")
                        .FNPdtAge = poDbRow("FNPdtAge") 'Age of product *CH 20-01-2015
                        .FDPdtExp = poDbRow("FDPdtExp")
                        .FTPdtUnitSymbol = poDbRow("FTPdtUnitSymbol")
                        .FTPdtLabSizeS = poDbRow("FTPdtLabSizeS")
                        .FNPdtLabQtyS = poDbRow("FNPdtLabQtyS")
                        .FDPdtOrdStart = poDbRow("FDPdtOrdStart")
                        .FDPdtOrdStop = poDbRow("FDPdtOrdStop")
                        .FTPtyCode = poDbRow("FTPtyCode")       '*Em 58-02-05
                        .FCPdtStkFac = poDbRow("FCPdtStkFac")
                        .FTPdtStkControl = poDbRow("FTPdtStkControl")
                        .FCPdtQtyRet = poDbRow("FCPdtQtyRet")
                        .FCPdtQtyWhs = poDbRow("FCPdtQtyWhs")
                        .FCPdtQtyDRet = poDbRow("FCPdtQtyDRet")
                        .FCPdtQtyDWhs = poDbRow("FCPdtQtyDWhs")
                        .FCPdtQtyMRet = poDbRow("FCPdtQtyMRet")
                        .FCPdtQtyMWhs = poDbRow("FCPdtQtyMWhs")
                        .FCPdtQtyOrdBuy = poDbRow("FCPdtQtyOrdBuy")
                        .FCPdtCostAvg = poDbRow("FCPdtCostAvg")
                        .FCPdtCostFiFo = poDbRow("FCPdtCostFiFo")
                        .FCPdtCostLast = poDbRow("FCPdtCostLast")
                        .FCPdtCostDef = poDbRow("FCPdtCostDef")
                        .FCPdtCostOth = poDbRow("FCPdtCostOth")
                        .FCPdtCostAmt = poDbRow("FCPdtCostAmt")
                        .FCPdtCostStd = poDbRow("FCPdtCostStd")
                        .FTUsrCode = poDbRow("FTUsrCode")
                        .FCPdtMin = poDbRow("FCPdtMin")
                        .FCPdtMax = poDbRow("FCPdtMax")
                        .FCPdtWeight = poDbRow("FCPdtWeight")
                        .FTPszCode = poDbRow("FTPszCode")
                        .FTClrCode = poDbRow("FTClrCode")
                        .FTPmhCodeS = poDbRow("FTPmhCodeS")
                        .FTPmhTypeS = poDbRow("FTPmhTypeS")
                        .FTPdtPmtTypeS = poDbRow("FTPdtPmtTypeS")
                        .FTPmhCodeM = poDbRow("FTPmhCodeM")
                        .FTPmhTypeM = poDbRow("FTPmhTypeM")
                        .FTPdtPmtTypeM = poDbRow("FTPdtPmtTypeM")
                        .FTPmhCodeL = poDbRow("FTPmhCodeL")
                        .FTPmhTypeL = poDbRow("FTPmhTypeL")
                        .FTPdtPmtTypeL = poDbRow("FTPdtPmtTypeL")
                        .FTPdtConType = poDbRow("FTPdtConType")
                        .FTPdtSrn = poDbRow("FTPdtSrn")
                        .FTPdtPlcCodeS = poDbRow("FTPdtPlcCodeS")
                        .FTPdtPlcCodeM = poDbRow("FTPdtPlcCodeM")
                        .FTPdtPlcCodeL = poDbRow("FTPdtPlcCodeL")
                        .FTPdtAlwOrderS = poDbRow("FTPdtAlwOrderS")
                        .FTPdtAlwOrderM = poDbRow("FTPdtAlwOrderM")
                        .FTPdtAlwOrderL = poDbRow("FTPdtAlwOrderL")
                        .FTPdtAlwBuyS = poDbRow("FTPdtAlwBuyS")
                        .FTPdtAlwBuyM = poDbRow("FTPdtAlwBuyM")
                        .FTPdtAlwBuyL = poDbRow("FTPdtAlwBuyL")
                        .FCPdtRetPriS2 = poDbRow("FCPdtRetPriS2")
                        .FCPdtRetPriM2 = poDbRow("FCPdtRetPriM2")
                        .FCPdtRetPriL2 = poDbRow("FCPdtRetPriL2")
                        .FCPdtRetPriS3 = poDbRow("FCPdtRetPriS3")
                        .FCPdtRetPriM3 = poDbRow("FCPdtRetPriM3")
                        .FCPdtRetPriL3 = poDbRow("FCPdtRetPriL3")
                        .FCPdtWhsPriS1 = poDbRow("FCPdtWhsPriS1")
                        .FCPdtWhsPriM1 = poDbRow("FCPdtWhsPriM1")
                        .FCPdtWhsPriL1 = poDbRow("FCPdtWhsPriL1")
                        .FCPdtWhsPriS2 = poDbRow("FCPdtWhsPriS2")
                        .FCPdtWhsPriM2 = poDbRow("FCPdtWhsPriM2")
                        .FCPdtWhsPriL2 = poDbRow("FCPdtWhsPriL2")
                        .FCPdtWhsPriS3 = poDbRow("FCPdtWhsPriS3")
                        .FCPdtWhsPriM3 = poDbRow("FCPdtWhsPriM3")
                        .FCPdtWhsPriL3 = poDbRow("FCPdtWhsPriL3")
                        .FCPdtWhsPriS4 = poDbRow("FCPdtWhsPriS4")
                        .FCPdtWhsPriM4 = poDbRow("FCPdtWhsPriM4")
                        .FCPdtWhsPriL4 = poDbRow("FCPdtWhsPriL4")
                        .FCPdtWhsPriS5 = poDbRow("FCPdtWhsPriS5")
                        .FCPdtWhsPriM5 = poDbRow("FCPdtWhsPriM5")
                        .FCPdtWhsPriL5 = poDbRow("FCPdtWhsPriL5")
                        .FTPdtWhsDefUnit = poDbRow("FTPdtWhsDefUnit")
                        .FTPdtOthPurSplN1 = poDbRow("FTPdtOthPurSplN1")
                        .FTPdtOthPurSplN2 = poDbRow("FTPdtOthPurSplN2")
                        .FTPdtOthPurSplN3 = poDbRow("FTPdtOthPurSplN3")
                        .FCPdtOthPurSplP1 = poDbRow("FCPdtOthPurSplP1")
                        .FCPdtOthPurSplP2 = poDbRow("FCPdtOthPurSplP2")
                        .FCPdtOthPurSplP3 = poDbRow("FCPdtOthPurSplP3")
                        .FTPdtOthPurSplCmt = poDbRow("FTPdtOthPurSplCmt")
                        .FTPdtOthSleSplN1 = poDbRow("FTPdtOthSleSplN1")
                        .FTPdtOthSleSplN2 = poDbRow("FTPdtOthSleSplN2")
                        .FTPdtOthSleSplN3 = poDbRow("FTPdtOthSleSplN3")
                        .FTPdtOthSleSplCmt = poDbRow("FTPdtOthSleSplCmt")
                        .FTAccCode = poDbRow("FTAccCode")
                        .FTPdtPic = poDbRow("FTPdtPic")
                        .FTPdtSound = poDbRow("FTPdtSound")
                        .FTPdtStaDel = poDbRow("FTPdtStaDel")
                        .FTPdtBarByGen = poDbRow("FTPdtBarByGen")
                        .FTPdtStaSet = poDbRow("FTPdtStaSet")
                        .FTPdtStaSetPri = poDbRow("FTPdtStaSetPri")
                        .FTPdtStaSetShwDT = poDbRow("FTPdtStaSetShwDT")
                        .FCPdtLeftPO = poDbRow("FCPdtLeftPO")
                        .FTPdtTax = poDbRow("FTPdtTax")
                        .FTDcsCode = poDbRow("FTDcsCode")
                        .FTDepCode = poDbRow("FTDepCode")
                        .FTClsCode = poDbRow("FTClsCode")
                        .FTSclCode = poDbRow("FTSclCode")
                        .FCPdtQtyNow = poDbRow("FCPdtQtyNow")
                        .FTPdtArticle = poDbRow("FTPdtArticle")
                        .FTPdtGrpControl = poDbRow("FTPdtGrpControl")
                        .FTPdtShwTch = poDbRow("FTPdtShwTch")
                        .FCPdtVatRate = poDbRow("FCPdtVatRate")
                        .FTTcgCode = poDbRow("FTTcgCode")
                        .FTPdtNameShort = poDbRow("FTPdtNameShort")
                        .FTPdtNameShortEng = poDbRow("FTPdtNameShortEng")
                        .FTPdtStaAlwBuy = poDbRow("FTPdtStaAlwBuy")
                        .FTFixGonCode = poDbRow("FTFixGonCode")
                        .FCPdtLifeCycle = poDbRow("FCPdtLifeCycle")
                        .FTPdtOrdDay = poDbRow("FTPdtOrdDay")
                        .FTPdtOrdSun = poDbRow("FTPdtOrdSun")
                        .FTPdtOrdMon = poDbRow("FTPdtOrdMon")
                        .FTPdtOrdTue = poDbRow("FTPdtOrdTue")
                        .FTPdtOrdWed = poDbRow("FTPdtOrdWed")
                        .FTPdtOrdThu = poDbRow("FTPdtOrdThu")
                        .FTPdtOrdFri = poDbRow("FTPdtOrdFri")
                        .FTPdtOrdSat = poDbRow("FTPdtOrdSat")
                        .FCPdtLeadTime = poDbRow("FCPdtLeadTime")
                        .FDPdtSaleStart = Format(Now, "yyyy/MM/dd")
                        .FDPdtSaleStop = "9999-12-31"
                        .FTPdtStaTouch = "2"
                        .FTPdtStaAlwRepack = "3"
                        .FNPdtPalletSize = poDbRow("FNPdtPalletSize")
                        .FNPdtPalletLev = poDbRow("FNPdtPalletLev")
                        .FNPdtAgeB4Rcv = poDbRow("FNPdtAgeB4Rcv")
                        .FNPdtAgeB4Snd = poDbRow("FNPdtAgeB4Snd")
                        .FTItyCode = poDbRow("FTItyCode")
                        .FTMcrCode = poDbRow("FTMcrCode")
                        .FTSlcItemCode = poDbRow("FTSlcItemCode")
                        .FTPdtPic4Slip = poDbRow("FTPdtPic4Slip")
                    Case 2
                        .FTPdtBarCode2 = poDbRow("FTPdtBarCode1")
                        .FTPdtMUnit = poDbRow("FTPdtSUnit")
                        .FCPdtMFactor = poDbRow("FCPdtSFactor")
                        .FCPdtRetPriM1 = poDbRow("FCPdtRetPriS1")
                        .FTPdtLabSizeM = poDbRow("FTPdtLabSizeS")
                        .FNPdtLabQtyM = poDbRow("FNPdtLabQtyS")
                    Case 3
                        .FTPdtBarCode3 = poDbRow("FTPdtBarCode1")
                        .FTPdtLUnit = poDbRow("FTPdtSUnit")
                        .FCPdtLFactor = poDbRow("FCPdtSFactor")
                        .FCPdtRetPriL1 = poDbRow("FCPdtRetPriS1")
                        .FTPdtLabSizeL = poDbRow("FTPdtLabSizeS")
                        .FNPdtLabQtyL = poDbRow("FNPdtLabQtyS")
                End Select
            End With
        Catch ex As Exception

        End Try
        Return oPdt
    End Function

    Private Function C_SEToDRPdt(ByVal poDr As DataRow, ByVal poDbRow As DataRow, ByVal ptPdtCode As String, ByVal pnBar As Integer) As DataRow
        '*EYE 59-03-08

        Dim oDr As DataRow = poDr
        Dim tDateUpd As String = Format(Now, "yyyy/MM/dd")
        Dim tTimeUpd As String = Format(Now, "HH:mm:ss")
        Dim tWhoUpd As String = "AdaLinkPTG"
        Try
            With oDr
                Select Case pnBar
                    Case 1
                        ' .Item("FTPdtCode") = poDbRow("FTPdtStkCode") & IIf(Convert.ToInt32(ptPdtCode) > 1, "-" & Convert.ToInt32(ptPdtCode) - 1, "") ** ใช้กับ Case ลบ
                        .Item("FTPdtCode") = poDbRow("FTPdtCode") '& IIf(Convert.ToInt32(ptPdtCode) > 1, "-" & Convert.ToInt32(ptPdtCode) - 1, "")
                        .Item("FTPdtName") = poDbRow("FTPdtName")
                        .Item("FTPdtNameOth") = poDbRow("FTPdtNameOth")
                        .Item("FTPdtBarCode1") = poDbRow("FTPdtBarCode1")
                        .Item("FTPdtStkCode") = poDbRow("FTPdtStkCode")
                        .Item("FTPgpChain") = poDbRow("FTPgpChain")
                        .Item("FTSplCode") = poDbRow("FTSplCode")
                        .Item("FTPdtVatType") = poDbRow("FTPdtVatType")
                        .Item("FTPdtSaleType") = poDbRow("FTPdtSaleType")
                        .Item("FTPdtSUnit") = poDbRow("FTPdtSUnit")
                        .Item("FCPdtSFactor") = poDbRow("FCPdtSFactor")
                        .Item("FCPdtRetPriS1") = poDbRow("FCPdtRetPriS1")
                        .Item("FTPbnCode") = poDbRow("FTPbnCode")
                        .Item("FTPmoCode") = poDbRow("FTPmoCode")
                        .Item("FTPdtNoDis") = poDbRow("FTPdtNoDis")
                        .Item("FTPdtGrade") = poDbRow("FTPdtGrade")
                        .Item("FTPdtPoint") = poDbRow("FTPdtPoint")
                        .Item("FCPdtPointTime") = poDbRow("FCPdtPointTime")
                        .Item("FTPdtType") = poDbRow("FTPdtType")
                        .Item("FTPdtStaActive") = poDbRow("FTPdtStaActive")
                        .Item("FTPdtRmk") = poDbRow("FTPdtRmk")
                        .Item("FTPdtDim") = poDbRow("FTPdtDim")
                        .Item("FTPdtPkgDim") = poDbRow("FTPdtPkgDim")
                        .Item("FTPdtGateWhe") = poDbRow("FTPdtGateWhe")
                        .Item("FTPdtDeliveryBy") = poDbRow("FTPdtDeliveryBy")
                        .Item("FNPdtAge") = poDbRow("FNPdtAge") 'Age of product *CH 20-01-2015
                        .Item("FDPdtExp") = poDbRow("FDPdtExp")
                        .Item("FTPdtUnitSymbol") = poDbRow("FTPdtUnitSymbol")
                        .Item("FTPdtLabSizeS") = poDbRow("FTPdtLabSizeS")
                        .Item("FNPdtLabQtyS") = poDbRow("FNPdtLabQtyS")
                        .Item("FDPdtOrdStart") = poDbRow("FDPdtOrdStart")
                        .Item("FDPdtOrdStop") = poDbRow("FDPdtOrdStop")
                        .Item("FTPtyCode") = poDbRow("FTPtyCode")       '*Em 58-02-05
                        .Item("FCPdtStkFac") = poDbRow("FCPdtStkFac")
                        .Item("FTPdtStkControl") = poDbRow("FTPdtStkControl")
                        .Item("FCPdtQtyRet") = poDbRow("FCPdtQtyRet")
                        .Item("FCPdtQtyWhs") = poDbRow("FCPdtQtyWhs")
                        .Item("FCPdtQtyDRet") = poDbRow("FCPdtQtyDRet")
                        .Item("FCPdtQtyDWhs") = poDbRow("FCPdtQtyDWhs")
                        .Item("FCPdtQtyMRet") = poDbRow("FCPdtQtyMRet")
                        .Item("FCPdtQtyMWhs") = poDbRow("FCPdtQtyMWhs")
                        .Item("FCPdtQtyOrdBuy") = poDbRow("FCPdtQtyOrdBuy")
                        .Item("FCPdtCostAvg") = poDbRow("FCPdtCostAvg")
                        .Item("FCPdtCostFiFo") = poDbRow("FCPdtCostFiFo")
                        .Item("FCPdtCostLast") = poDbRow("FCPdtCostLast")
                        .Item("FCPdtCostDef") = poDbRow("FCPdtCostDef")
                        .Item("FCPdtCostOth") = poDbRow("FCPdtCostOth")
                        .Item("FCPdtCostAmt") = poDbRow("FCPdtCostAmt")
                        .Item("FCPdtCostStd") = poDbRow("FCPdtCostStd")
                        .Item("FTUsrCode") = poDbRow("FTUsrCode")
                        .Item("FCPdtMin") = poDbRow("FCPdtMin")
                        .Item("FCPdtMax") = poDbRow("FCPdtMax")
                        .Item("FCPdtWeight") = poDbRow("FCPdtWeight")
                        .Item("FTPszCode") = poDbRow("FTPszCode")
                        .Item("FTClrCode") = poDbRow("FTClrCode")
                        .Item("FTPmhCodeS") = poDbRow("FTPmhCodeS")
                        .Item("FTPmhTypeS") = poDbRow("FTPmhTypeS")
                        .Item("FTPdtPmtTypeS") = poDbRow("FTPdtPmtTypeS")
                        .Item("FTPmhCodeM") = poDbRow("FTPmhCodeM")
                        .Item("FTPmhTypeM") = poDbRow("FTPmhTypeM")
                        .Item("FTPdtPmtTypeM") = poDbRow("FTPdtPmtTypeM")
                        .Item("FTPmhCodeL") = poDbRow("FTPmhCodeL")
                        .Item("FTPmhTypeL") = poDbRow("FTPmhTypeL")
                        .Item("FTPdtPmtTypeL") = poDbRow("FTPdtPmtTypeL")
                        .Item("FTPdtConType") = poDbRow("FTPdtConType")
                        .Item("FTPdtSrn") = poDbRow("FTPdtSrn")
                        .Item("FTPdtPlcCodeS") = poDbRow("FTPdtPlcCodeS")
                        .Item("FTPdtPlcCodeM") = poDbRow("FTPdtPlcCodeM")
                        .Item("FTPdtPlcCodeL") = poDbRow("FTPdtPlcCodeL")
                        .Item("FTPdtAlwOrderS") = poDbRow("FTPdtAlwOrderS")
                        .Item("FTPdtAlwOrderM") = poDbRow("FTPdtAlwOrderM")
                        .Item("FTPdtAlwOrderL") = poDbRow("FTPdtAlwOrderL")
                        .Item("FTPdtAlwBuyS") = poDbRow("FTPdtAlwBuyS")
                        .Item("FTPdtAlwBuyM") = poDbRow("FTPdtAlwBuyM")
                        .Item("FTPdtAlwBuyL") = poDbRow("FTPdtAlwBuyL")
                        .Item("FCPdtRetPriS2") = poDbRow("FCPdtRetPriS2")
                        .Item("FCPdtRetPriM2") = poDbRow("FCPdtRetPriM2")
                        .Item("FCPdtRetPriL2") = poDbRow("FCPdtRetPriL2")
                        .Item("FCPdtRetPriS3") = poDbRow("FCPdtRetPriS3")
                        .Item("FCPdtRetPriM3") = poDbRow("FCPdtRetPriM3")
                        .Item("FCPdtRetPriL3") = poDbRow("FCPdtRetPriL3")
                        .Item("FCPdtWhsPriS1") = poDbRow("FCPdtWhsPriS1")
                        .Item("FCPdtWhsPriM1") = poDbRow("FCPdtWhsPriM1")
                        .Item("FCPdtWhsPriL1") = poDbRow("FCPdtWhsPriL1")
                        .Item("FCPdtWhsPriS2") = poDbRow("FCPdtWhsPriS2")
                        .Item("FCPdtWhsPriM2") = poDbRow("FCPdtWhsPriM2")
                        .Item("FCPdtWhsPriL2") = poDbRow("FCPdtWhsPriL2")
                        .Item("FCPdtWhsPriS3") = poDbRow("FCPdtWhsPriS3")
                        .Item("FCPdtWhsPriM3") = poDbRow("FCPdtWhsPriM3")
                        .Item("FCPdtWhsPriL3") = poDbRow("FCPdtWhsPriL3")
                        .Item("FCPdtWhsPriS4") = poDbRow("FCPdtWhsPriS4")
                        .Item("FCPdtWhsPriM4") = poDbRow("FCPdtWhsPriM4")
                        .Item("FCPdtWhsPriL4") = poDbRow("FCPdtWhsPriL4")
                        .Item("FCPdtWhsPriS5") = poDbRow("FCPdtWhsPriS5")
                        .Item("FCPdtWhsPriM5") = poDbRow("FCPdtWhsPriM5")
                        .Item("FCPdtWhsPriL5") = poDbRow("FCPdtWhsPriL5")
                        .Item("FTPdtWhsDefUnit") = poDbRow("FTPdtWhsDefUnit")
                        .Item("FTPdtOthPurSplN1") = poDbRow("FTPdtOthPurSplN1")
                        .Item("FTPdtOthPurSplN2") = poDbRow("FTPdtOthPurSplN2")
                        .Item("FTPdtOthPurSplN3") = poDbRow("FTPdtOthPurSplN3")
                        .Item("FCPdtOthPurSplP1") = poDbRow("FCPdtOthPurSplP1")
                        .Item("FCPdtOthPurSplP2") = poDbRow("FCPdtOthPurSplP2")
                        .Item("FCPdtOthPurSplP3") = poDbRow("FCPdtOthPurSplP3")
                        .Item("FTPdtOthPurSplCmt") = poDbRow("FTPdtOthPurSplCmt")
                        .Item("FTPdtOthSleSplN1") = poDbRow("FTPdtOthSleSplN1")
                        .Item("FTPdtOthSleSplN2") = poDbRow("FTPdtOthSleSplN2")
                        .Item("FTPdtOthSleSplN3") = poDbRow("FTPdtOthSleSplN3")
                        .Item("FTPdtOthSleSplCmt") = poDbRow("FTPdtOthSleSplCmt")
                        .Item("FTAccCode") = poDbRow("FTAccCode")
                        .Item("FTPdtPic") = poDbRow("FTPdtPic")
                        .Item("FTPdtSound") = poDbRow("FTPdtSound")
                        .Item("FTPdtStaDel") = poDbRow("FTPdtStaDel")
                        .Item("FTPdtBarByGen") = poDbRow("FTPdtBarByGen")
                        .Item("FTPdtStaSet") = poDbRow("FTPdtStaSet")
                        .Item("FTPdtStaSetPri") = poDbRow("FTPdtStaSetPri")
                        .Item("FTPdtStaSetShwDT") = poDbRow("FTPdtStaSetShwDT")
                        .Item("FCPdtLeftPO") = poDbRow("FCPdtLeftPO")
                        .Item("FTPdtTax") = poDbRow("FTPdtTax")
                        .Item("FTDcsCode") = poDbRow("FTDcsCode")
                        .Item("FTDepCode") = poDbRow("FTDepCode")
                        .Item("FTClsCode") = poDbRow("FTClsCode")
                        .Item("FTSclCode") = poDbRow("FTSclCode")
                        .Item("FCPdtQtyNow") = poDbRow("FCPdtQtyNow")
                        .Item("FTPdtArticle") = poDbRow("FTPdtArticle")
                        .Item("FTPdtGrpControl") = poDbRow("FTPdtGrpControl")
                        .Item("FTPdtShwTch") = poDbRow("FTPdtShwTch")
                        .Item("FCPdtVatRate") = poDbRow("FCPdtVatRate")
                        .Item("FTTcgCode") = poDbRow("FTTcgCode")
                        .Item("FTPdtNameShort") = poDbRow("FTPdtNameShort")
                        .Item("FTPdtNameShortEng") = poDbRow("FTPdtNameShortEng")
                        .Item("FTPdtStaAlwBuy") = poDbRow("FTPdtStaAlwBuy")
                        .Item("FTFixGonCode") = poDbRow("FTFixGonCode")
                        .Item("FCPdtLifeCycle") = poDbRow("FCPdtLifeCycle")
                        .Item("FTPdtOrdDay") = poDbRow("FTPdtOrdDay")
                        .Item("FTPdtOrdSun") = poDbRow("FTPdtOrdSun")
                        .Item("FTPdtOrdMon") = poDbRow("FTPdtOrdMon")
                        .Item("FTPdtOrdTue") = poDbRow("FTPdtOrdTue")
                        .Item("FTPdtOrdWed") = poDbRow("FTPdtOrdWed")
                        .Item("FTPdtOrdThu") = poDbRow("FTPdtOrdThu")
                        .Item("FTPdtOrdFri") = poDbRow("FTPdtOrdFri")
                        .Item("FTPdtOrdSat") = poDbRow("FTPdtOrdSat")
                        .Item("FCPdtLeadTime") = poDbRow("FCPdtLeadTime")
                        .Item("FDPdtSaleStart") = Format(Now, "yyyy/MM/dd")
                        .Item("FDPdtSaleStop") = "9999-12-31"
                        .Item("FTPdtStaTouch") = "2"
                        .Item("FTPdtStaAlwRepack") = "3"
                        .Item("FNPdtPalletSize") = poDbRow("FNPdtPalletSize")
                        .Item("FNPdtPalletLev") = poDbRow("FNPdtPalletLev")
                        .Item("FNPdtAgeB4Rcv") = poDbRow("FNPdtAgeB4Rcv")
                        .Item("FNPdtAgeB4Snd") = poDbRow("FNPdtAgeB4Snd")
                        .Item("FTItyCode") = poDbRow("FTItyCode")
                        .Item("FTMcrCode") = poDbRow("FTMcrCode")
                        .Item("FTSlcItemCode") = poDbRow("FTSlcItemCode")
                        .Item("FTPdtPic4Slip") = poDbRow("FTPdtPic4Slip")
                        .Item("FDPdtMfg") = poDbRow("FDPdtMfg")
                        .Item("FDPdtPmtSDateS") = poDbRow("FDPdtPmtSDateS")
                        .Item("FDPdtPmtEDateS") = poDbRow("FDPdtPmtEDateS")
                        .Item("FDPdtPmtSDateM") = poDbRow("FDPdtPmtSDateM")
                        .Item("FDPdtPmtEDateM") = poDbRow("FDPdtPmtEDateM")
                        .Item("FDPdtPmtSDateL") = poDbRow("FDPdtPmtSDateL")
                        .Item("FDPdtPmtEDateL") = poDbRow("FDPdtPmtEDateL")
                        .Item("FCPdtOthSleSplP1") = poDbRow("FCPdtOthSleSplP1")
                        .Item("FCPdtOthSleSplP2") = poDbRow("FCPdtOthSleSplP2")
                        .Item("FCPdtOthSleSplP3") = poDbRow("FCPdtOthSleSplP3")
                        .Item("FDDateUpd") = tDateUpd   'poDbRow("FDDateUpd")
                        .Item("FTTimeUpd") = tTimeUpd   'poDbRow("FTTimeUpd")
                        .Item("FTWhoUpd") = tWhoUpd     'poDbRow("FTWhoUpd")
                        .Item("FDDateIns") = tDateUpd   'poDbRow("FDDateIns")
                        .Item("FTTimeIns") = tTimeUpd   'poDbRow("FTTimeIns")
                        .Item("FTWhoIns") = tWhoUpd     'poDbRow("FTWhoIns")
                        '.Item("FTCompName") = poDbRow("FTCompName")
                        .Item("FNLNKLine") = poDbRow("FNLNKLine")
                        .Item("FTLNKLog") = poDbRow("FTLNKLog")
                        .Item("FTMcrCode") = poDbRow("FTMcrCode")
                        '.Item("FTPbnName") = poDbRow("FTPbnName")
                        '.Item("FTPmoName") = poDbRow("FTPmoName")
                    Case 2
                        .Item("FTPdtBarCode2") = poDbRow("FTPdtBarCode1")
                        .Item("FTPdtMUnit") = poDbRow("FTPdtSUnit")
                        .Item("FCPdtMFactor") = poDbRow("FCPdtSFactor")
                        .Item("FCPdtRetPriM1") = poDbRow("FCPdtRetPriS1")
                        .Item("FTPdtLabSizeM") = poDbRow("FTPdtLabSizeS")
                        .Item("FNPdtLabQtyM") = poDbRow("FNPdtLabQtyS")
                    Case 3
                        .Item("FTPdtBarCode3") = poDbRow("FTPdtBarCode1")
                        .Item("FTPdtLUnit") = poDbRow("FTPdtSUnit")
                        .Item("FCPdtLFactor") = poDbRow("FCPdtSFactor")
                        .Item("FCPdtRetPriL1") = poDbRow("FCPdtRetPriS1")
                        .Item("FTPdtLabSizeL") = poDbRow("FTPdtLabSizeS")
                        .Item("FNPdtLabQtyL") = poDbRow("FNPdtLabQtyS")
                End Select
            End With
        Catch ex As Exception

        End Try
        Return oDr
    End Function

    Private Function C_GETtFieldPdt() As String
        Dim oSql As New System.Text.StringBuilder
        Dim tSql As String = ""
        Try
            oSql.AppendLine("(FTPdtCode, FTPdtName, FTPdtNameOth, FTPdtBarCode1,")
            oSql.AppendLine("FTPdtBarCode2, FTPdtBarCode3, FTPdtStkCode,")
            oSql.AppendLine("FCPdtStkFac, FTPdtStkControl, FCPdtQtyRet,")
            oSql.AppendLine("FCPdtQtyWhs, FCPdtQtyDRet, FCPdtQtyDWhs,")
            oSql.AppendLine("FCPdtQtyMRet, FCPdtQtyMWhs, FCPdtQtyOrdBuy,")
            oSql.AppendLine("FCPdtCostAvg, FCPdtCostFiFo, FCPdtCostLast,")
            oSql.AppendLine("FCPdtCostDef, FCPdtCostOth, FCPdtCostAmt,")
            oSql.AppendLine("FCPdtCostStd, FTPgpChain, FTSplCode,")
            oSql.AppendLine("FTUsrCode, FTPdtVatType, FCPdtMin,")
            oSql.AppendLine("FCPdtMax, FTPdtSaleType, FTPdtSUnit,")
            oSql.AppendLine("FCPdtSFactor, FTPdtMUnit, FCPdtMFactor,")
            oSql.AppendLine("FTPdtLUnit, FCPdtLFactor, FTPdtGrade,")
            oSql.AppendLine("FCPdtWeight, FTPszCode, FTClrCode,")
            oSql.AppendLine("FTPtyCode, FTPdtPoint, FCPdtPointTime,")
            oSql.AppendLine("FTPmhCodeS, FTPmhTypeS, FDPdtPmtSDateS,")
            oSql.AppendLine("FDPdtPmtEDateS, FTPdtPmtTypeS, FTPmhCodeM,")
            oSql.AppendLine("FTPmhTypeM, FDPdtPmtSDateM, FDPdtPmtEDateM,")
            oSql.AppendLine("FTPdtPmtTypeM, FTPmhCodeL, FTPmhTypeL,")
            oSql.AppendLine("FDPdtPmtSDateL, FDPdtPmtEDateL, FTPdtPmtTypeL,")
            oSql.AppendLine("FTPdtConType, FTPdtSrn, FTPdtPlcCodeS,")
            oSql.AppendLine("FTPdtPlcCodeM, FTPdtPlcCodeL, FTPdtAlwOrderS,")
            oSql.AppendLine("FTPdtAlwOrderM, FTPdtAlwOrderL, FTPdtAlwBuyS,")
            oSql.AppendLine("FTPdtAlwBuyM, FTPdtAlwBuyL, FCPdtRetPriS1,")
            oSql.AppendLine("FCPdtRetPriM1, FCPdtRetPriL1, FCPdtRetPriS2,")
            oSql.AppendLine("FCPdtRetPriM2, FCPdtRetPriL2, FCPdtRetPriS3,")
            oSql.AppendLine("FCPdtRetPriM3, FCPdtRetPriL3, FCPdtWhsPriS1,")
            oSql.AppendLine("FCPdtWhsPriM1, FCPdtWhsPriL1, FCPdtWhsPriS2,")
            oSql.AppendLine("FCPdtWhsPriM2, FCPdtWhsPriL2, FCPdtWhsPriS3,")
            oSql.AppendLine("FCPdtWhsPriM3, FCPdtWhsPriL3, FCPdtWhsPriS4,")
            oSql.AppendLine("FCPdtWhsPriM4, FCPdtWhsPriL4, FCPdtWhsPriS5,")
            oSql.AppendLine("FCPdtWhsPriM5, FCPdtWhsPriL5, FTPdtWhsDefUnit,")
            oSql.AppendLine("FTPdtOthPurSplN1, FTPdtOthPurSplN2, FTPdtOthPurSplN3,")
            oSql.AppendLine("FCPdtOthPurSplP1, FCPdtOthPurSplP2, FCPdtOthPurSplP3,")
            oSql.AppendLine("FTPdtOthPurSplCmt, FTPdtOthSleSplN1, FTPdtOthSleSplN2,")
            oSql.AppendLine("FTPdtOthSleSplN3, FCPdtOthSleSplP1, FCPdtOthSleSplP2,")
            oSql.AppendLine("FCPdtOthSleSplP3, FTPdtOthSleSplCmt, FTAccCode,")
            oSql.AppendLine("FTPdtPic, FTPdtSound, FTPdtStaDel,")
            oSql.AppendLine("FTPdtType, FTPdtBarByGen, FTPdtStaSet,")
            oSql.AppendLine("FTPdtStaSetPri, FTPdtStaSetShwDT, FTPdtStaActive,")
            oSql.AppendLine("FTPdtRmk, FTPbnCode, FCPdtLeftPO,")
            oSql.AppendLine("FTPdtTax, FTPmoCode, FTDcsCode,")
            oSql.AppendLine("FTDepCode, FTClsCode, FTSclCode,")
            oSql.AppendLine("FCPdtQtyNow, FTPdtArticle, FTPdtGrpControl,")
            oSql.AppendLine("FTPdtShwTch, FTPdtNoDis, FCPdtVatRate,")
            oSql.AppendLine("FTTcgCode, FTPdtNameShort, FTPdtNameShortEng,")
            oSql.AppendLine("FTPdtStaAlwBuy, FTFixGonCode, FCPdtLifeCycle,")
            oSql.AppendLine("FTPdtOrdDay, FTPdtOrdSun, FTPdtOrdMon,")
            oSql.AppendLine("FTPdtOrdTue, FTPdtOrdWed, FTPdtOrdThu,")
            oSql.AppendLine("FTPdtOrdFri, FTPdtOrdSat, FTPdtDim,")
            oSql.AppendLine("FTPdtPkgDim, FCPdtLeadTime, FDPdtOrdStart,")
            oSql.AppendLine("FDPdtOrdStop, FDPdtSaleStart, FDPdtSaleStop,")
            oSql.AppendLine("FTPdtStaTouch, FTPdtStaAlwRepack, FTPdtGateWhe,")
            oSql.AppendLine("FNPdtPalletSize, FNPdtPalletLev, FNPdtAge,")
            oSql.AppendLine("FNPdtAgeB4Rcv, FNPdtAgeB4Snd, FTItyCode,")
            oSql.AppendLine("FTPdtDeliveryBy, FTMcrCode, FTSlcItemCode,")
            oSql.AppendLine("FTPdtPic4Slip, FDPdtMfg, FDPdtExp,")
            oSql.AppendLine("FTPdtUnitSymbol, FTPdtLabSizeS, FNPdtLabQtyS,")
            oSql.AppendLine("FTPdtLabSizeM, FNPdtLabQtyM, FTPdtLabSizeL,")
            oSql.AppendLine("FNPdtLabQtyL, FDDateUpd, FTTimeUpd,")
            oSql.AppendLine("FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns)")
            tSql = oSql.ToString
        Catch ex As Exception
        Finally
            oSql = Nothing
        End Try
        Return tSql
    End Function

    Private Sub C_CALxBakAndLog()
        Dim oFileLog As New cImportLog(tFileLog)
        Dim tSql As String = ""
        tSql = "SELECT FNLnkLine,'" & Path.GetFileName(tFullPath) & "' AS FileName,FTPdtStkCode,FTPdtName,FTLnkLog "
        tSql &= "FROM " & tTableTemp
        Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oSQLConn.Open()
            Dim oSQLCmd As New SqlCommand(tSql, oSQLConn)
            Dim oSQLRead As SqlDataReader = oSQLCmd.ExecuteReader()
            Dim tErr As String = ""
            While oSQLRead.Read()
                tErr = oSQLRead(4)
                If tErr.Length > 0 Then
                    Dim oNewLog As New cImportLog.cLog
                    With oNewLog
                        .nLineNo = oSQLRead(0)
                        .tType = oSQLRead(1)
                        If tErr.StartsWith("Format") Then
                            .tDocNo = ""
                            .tSeqNo = ""
                            .tCode = ""
                            .tName = ""
                        Else
                            .tDocNo = oSQLRead(2)
                            .tName = oSQLRead(3)
                        End If

                        If tErr.Split(",").Count = 2 Then
                            .tErrField = tErr.Split(",")(0)
                            .tErrDesc = tErr.Split(",")(1)
                        Else
                            .tErrDesc = tErr
                        End If
                    End With
                    oFileLog.C_CALxAddNew(oNewLog)
                End If
            End While
            oSQLConn.Close()
        End Using
        oFileLog.C_CALbSave("2")
    End Sub

    'Sub Class
    Class cDetail
        Property FTPdtCode As String = ""
        Property FNPdeSeq As String = "0"
        Property FTPdeDesc As String = ""
        Property FTPdeDescOth As String = ""
    End Class

    Public Function C_CALbPrcPdtReuse(ByVal ptDocDate As String) As Boolean
        Dim oDatabase As New cDatabaseLocal
        Dim oSql As System.Text.StringBuilder = Nothing
        Try
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("INSERT INTO TCNTPdtReuse")
            oSql.AppendLine("(FDPruDocDate,FTPruStaPrc,FNPruSeqNo,FTPdtCode,FTPdtStkCode,FTPruBarcodeOld,FTPruBarcodeNew)")
            oSql.AppendLine("SELECT '" & ptDocDate & "','N',ROW_NUMBER() OVER(ORDER BY Pdt.FTPdtCode DESC),Pdt.FTPdtCode,Pdt.FTPdtStkCode,Pdt.FTPdtBarCode,")
            oSql.AppendLine("LEFT(Pdt.FTPdtBarCode,19) + (SELECT CASE WHEN FTLNMUsrValue = '' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode = 'SUFFIXREUSE')")
            oSql.AppendLine("FROM TCNTArticleList Tmp INNER JOIN SI_VCNMPdtByBar Pdt ON Tmp.FTPdtStkCode = Pdt.FTPdtStkCode")
            oSql.AppendLine("WHERE FDArtDocDate = '" & ptDocDate & "'")

            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
            Return True
        Catch ex As Exception
            Return False
        Finally
            oDatabase = Nothing
            oSql = Nothing
        End Try
        Return True
    End Function

    Public Function C_CALbPrcArticleList(ByVal poListUnUse As List(Of cArticleList)) As Boolean
        Dim oDatabase As New cDatabaseLocal
        Dim oSql As System.Text.StringBuilder = Nothing
        Dim nRec As Integer
        Dim nSeq As Integer = 0
        Dim tTableArtLst As String = "TCNTFileNameList"
        Dim tDocDate As String = ""
        Dim tMsg As String = ""

        Try
            cCNVB.oDBInst = C_CRTxDBArtLst(tTableArtLst)
            For nRec = 0 To poListUnUse.Count - 1

                'TCNTArticleList
                'Log Product List
                'oSql = New System.Text.StringBuilder
                'oSql.AppendLine("INSERT INTO TCNTArticleList")
                'oSql.AppendLine("(FDArtDocDate,FNArtSeqNo,FTPdtStkCode,")
                'oSql.AppendLine("FTArtListFile,FTArtFile,FTArtStaPrc,")
                'oSql.AppendLine("FDDateIns,FTTimeIns,FTWhoIns,")
                'oSql.AppendLine("FDDateUpd,FTTimeUpd,FTWhoUpd)")
                'oSql.AppendLine("SELECT")
                'oSql.AppendLine("'" & poListUnUse(0).FDArtDocDate & "'," & poListUnUse(nRec).FNArtSeqNo & ",'" & poListUnUse(nRec).FTPdtStkCode & "',")
                'oSql.AppendLine("'" & poListUnUse(nRec).FTArtListFile & "','','" & poListUnUse(nRec).FTArtStaPrc & "',")
                'oSql.AppendLine("'" & Format(Now, "yyyy/MM/dd") & "','" & Format(Now, "HH:mm:ss") & "','AdaLinkPTG',")
                'oSql.AppendLine("'" & Format(Now, "yyyy/MM/dd") & "','" & Format(Now, "HH:mm:ss") & "','AdaLinkPTG'")
                'oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
                '*EYE 59-02-25
                cCNVB.oDBInst.Rows.Add()

                With cCNVB.oDBInst.Rows(cCNVB.oDBInst.Rows.Count - 1)
                    .Item("FDArtDocDate") = poListUnUse(nSeq).FDArtDocDate
                    '.Item("FDArtDocDate") = poListUnUse(0).FDArtDocDate
                    .Item("FNArtSeqNo") = poListUnUse(nRec).FNArtSeqNo
                    .Item("FTDataCnt") = poListUnUse(nRec).FTDataCnt
                    ' .Item("FTPdtStkCode") = poListUnUse(nRec).FTPdtStkCode
                    .Item("FTArtListFile") = poListUnUse(nRec).FTArtListFile
                    .Item("FTArtFile") = poListUnUse(nRec).FTArtFile
                    .Item("FTArtStaPrc") = IIf(poListUnUse(nRec).FTArtStaPrc = "NULL", "", poListUnUse(nRec).FTArtStaPrc)
                    .Item("FDDateIns") = Format(Now, "yyyy/MM/dd")
                    .Item("FTTimeIns") = Format(Now, "HH:mm:ss")
                    .Item("FTWhoIns") = "AdaLinkPTG"
                    .Item("FDDateUpd") = Format(Now, "yyyy/MM/dd")
                    .Item("FTTimeUpd") = Format(Now, "HH:mm:ss")
                    .Item("FTWhoUpd") = "AdaLinkPTG"

                End With
                nSeq += 1
            Next
            If cCNVB.oDBInst.Rows.Count > 0 Then
                C_COPtData(cCNVB.oDBInst)
            End If

            Return True
        Catch ex As Exception
            Return False
        Finally
            oDatabase = Nothing
            oSql = Nothing
        End Try
        Return True
    End Function

    Private Function C_CRTxDBArtLst(ptTable) As DataTable
        'EYE 59-02-23 สร้าง Datatable เพื่อเก็บข้อมูลก่อนการ Insert to TCNTArticleList
        Dim oDT As New DataTable(ptTable)
        oDT.Columns.Add("FDArtDocDate", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FNArtSeqNo", Type.GetType("System.Int32"))
        oDT.Columns.Add("FTDataCnt", Type.GetType("System.String"))
        'oDT.Columns.Add("FTPdtStkCode", Type.GetType("System.String"))
        oDT.Columns.Add("FTArtListFile", Type.GetType("System.String"))
        oDT.Columns.Add("FTArtFile", Type.GetType("System.String"))
        oDT.Columns.Add("FTArtStaPrc", Type.GetType("System.String"))
        oDT.Columns.Add("FDDateIns", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTTimeIns", Type.GetType("System.String"))
        oDT.Columns.Add("FTWhoIns", Type.GetType("System.String"))
        oDT.Columns.Add("FDDateUpd", Type.GetType("System.DateTime"))
        oDT.Columns.Add("FTTimeUpd", Type.GetType("System.String"))
        oDT.Columns.Add("FTWhoUpd", Type.GetType("System.String"))

        Return oDT
    End Function

End Class