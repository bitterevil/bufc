﻿Imports System.Data.SqlClient
Imports System.IO

Public Class cTCNTPdtAjp
    Property tDateFrom As String = ""
    Property tDateTo As String = ""
    Property nMode As Integer = 0 '0:All,1:Interval,2:Select
    Property tCodeFrom As String = ""
    Property tCodeTo As String = ""
    Property tCodeSelect As String = ""
    Property tFullPath As String = ""
    Property tOutPath As String = ""
    Property tHDTable As String = ""
    Property tFileLog As String = ""
    Property cNextVal As Double = 0
    Property cProgress As Double = 0
    Private nC_ValPropress As Double = 0

    ''' <summary>
    ''' SAVE Ajp เข้าตารางข้อมูลจริง
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function C_CALbProcess() As Boolean
        C_CALbProcess = True
        Dim nLang As Integer = 0
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTblHD As New DataTable
        Dim oDbTblDT As New DataTable
        Dim oDbTblCD As New DataTable
        Dim oSql As New System.Text.StringBuilder
        Dim aSqlB4 As New List(Of String)
        Dim tMsg As String = ""
        Try

            '------------------------ Procees 1 SAVE DT------------------------
            oSql.Clear()
            oSql.AppendLine("INSERT INTO TCNTPdtAjpDT(FTIphDocNo,FNIpdSeqNo,FTPdtCode")
            oSql.AppendLine(",FTPdtName,FTIphDocType,FDIphDocDate,FTBchCode")
            oSql.AppendLine(",FTIpdBarCode,FTIpdStkCode,FCIpdStkFac,FTPunCode")
            oSql.AppendLine(",FCIpdFactor,FNIpdAdjLev,FCIpdPriOld,FCIpdPriNew")
            oSql.AppendLine(",FNIpdUnitType,FTIpdBchTo,FDDateUpd,FTTimeUpd")
            oSql.AppendLine(",FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns) ")
            oSql.AppendLine("SELECT FTIphDocNo,FNIpdSeqNo,FTPdtCode")
            oSql.AppendLine(",FTPdtName,FTIphDocType,FDIphDocDate,FTBchCode")
            oSql.AppendLine(",FTIpdBarCode,FTIpdStkCode,FCIpdStkFac,FTPunCode")
            oSql.AppendLine(",FCIpdFactor,FNIpdAdjLev,FCIpdPriOld,FCIpdPriNew")
            oSql.AppendLine(",FNIpdUnitType,FTIpdBchTo,FDDateUpd,FTTimeUpd")
            oSql.AppendLine(",FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns ")
            oSql.AppendLine(" FROM TLNKPdtAjpDT")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If
            '------------------------ Procees 1 SAVE DT------------------------

            '------------------------ Procees 2 SAVE HD------------------------
            oSql.Clear()
            oSql.AppendLine("INSERT INTO TCNTPdtAjpHD(FTIphDocNo,FTIphDocType,FDIphDocDate")
            oSql.AppendLine(",FTIphDocTime,FTBchCode,FTDptCode,FTUsrCode")
            oSql.AppendLine(",FTIphApvCode,FCIphTotal,FTIphStaDoc,FTIphStaPrcDoc")
            oSql.AppendLine(",FNIphStaDocAct,FTIphAdjType,FDIphAffect,FTIphBchTo")
            oSql.AppendLine(",FTIphPriType,FDIphDStop,FDDateUpd,FTTimeUpd,FTWhoUpd")
            oSql.AppendLine(",FDDateIns,FTTimeIns,FTWhoIns,FTIphStaPrcPri) ")
            oSql.AppendLine("SELECT FTIphDocNo,FTIphDocType,FDIphDocDate")
            oSql.AppendLine(",FTIphDocTime,FTBchCode,FTDptCode,FTUsrCode")
            oSql.AppendLine(",FTIphApvCode,FCIphTotal,FTIphStaDoc,FTIphStaPrcDoc")
            oSql.AppendLine(",FNIphStaDocAct,FTIphAdjType,FDIphAffect,FTIphBchTo")
            oSql.AppendLine(",FTIphPriType,FDIphDStop,FDDateUpd,FTTimeUpd,FTWhoUpd")
            oSql.AppendLine(",FDDateIns,FTTimeIns,FTWhoIns,'1' AS FTIphStaPrcPri")
            oSql.AppendLine(" FROM TLNKPdtAjpHD")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If

            '-----Update Price To Product ------'
            oSql.Clear()
            oSql.AppendLine(" UPDATE TCNMPdt  ")
            oSql.AppendLine(" SET TCNMPdt.FCPdtRetPriS1 = Ajp.FCIpdPriNew")
            oSql.AppendLine(" FROM TCNMPdt Pdt")
            oSql.AppendLine(" INNER JOIN ")
            oSql.AppendLine(" TLNKPdtAjpDT Ajp")
            oSql.AppendLine(" ON Pdt.FTPdtCode = Ajp.FTPdtCode")
            oSql.AppendLine(" WHERE (Pdt.FTPdtCode = Ajp.FTPdtCode)")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If

            oSql.Clear()
            oSql.AppendLine(" ")



            '------------------------ Procees 2 SAVE HD------------------------

            '------------------------ Procees 3 ลบ TCNTLockDoc Gen DocNo------------------------
            oSql.Clear()
            oSql.AppendLine("DELETE FROM TCNTLockDoc")
            If oSql.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
            End If
            '------------------------ Procees 3 ลบ TCNTLockDoc Gen DocNo------------------------
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Class cFileInfo
        Property tPathFile As String = ""
        Property nItem As Integer = 0
        Property tDocDate As String = ""
        Property tBchCode As String = ""
    End Class
End Class
