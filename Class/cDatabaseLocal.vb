﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Text

Public Class cDatabaseLocal

    Public Function C_CALnExecuteNonQuery(ParamArray paSql As String()) As Integer
        C_CALnExecuteNonQuery = -1
        Using oConnSQL As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oConnSQL.Open()
            Using oSQLCmd As New SqlCommand
                Try
                    oSQLCmd.Connection = oConnSQL
                    oSQLCmd.Transaction = oConnSQL.BeginTransaction(IsolationLevel.ReadCommitted)
                    For Each tSql In paSql
                        Application.DoEvents()
                        oSQLCmd.CommandTimeout = 1000 'AdaConfig.cConfig.oConnSource.nTimeOut
                        oSQLCmd.CommandText = tSql
                        oSQLCmd.ExecuteNonQuery()
                    Next
                    oSQLCmd.Transaction.Commit()
                    C_CALnExecuteNonQuery = True
                Catch ex As Exception
                    oSQLCmd.Transaction.Rollback()
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
    End Function

    Public Function C_CALnExecuteNonQuery(ptSql As String) As Integer
        C_CALnExecuteNonQuery = -1
        Try
            Using oSqlConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSqlConn.Open()
                Using oSqlCmd As New SqlCommand(ptSql, oSqlConn)
                    oSqlCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                    Return oSqlCmd.ExecuteNonQuery()
                End Using
            End Using
            Return 1
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function C_CALoExecuteReader(ptSql As String, Optional ptTableName As String = "TableTemp", Optional pbMemDB As Boolean = False) As DataTable
        C_CALoExecuteReader = New DataTable
        C_CALoExecuteReader.TableName = ptTableName
        Try
            Using oSqlConn As New SqlConnection(IIf(Not pbMemDB, AdaConfig.cConfig.tSQLConnSourceString, AdaConfig.cConfig.tSQLMemConnSourceString))
                oSqlConn.Open()
                Using oSqlCmd As New SqlCommand(ptSql, oSqlConn)
                    oSqlCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                    Dim oSqlDataReader As SqlDataReader = oSqlCmd.ExecuteReader(CommandBehavior.CloseConnection)
                    C_CALoExecuteReader.Load(oSqlDataReader)
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function C_CHKnDATHASRow(ByVal ptSQL As String) As Integer
        Dim nResult As Integer = 0
        Dim oDbRed As SqlDataReader
        Try
            Using oSqlConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSqlConn.Open()
                Using oSqlCmd As New SqlCommand(ptSQL, oSqlConn)
                    oSqlCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                    oDbRed = oSqlCmd.ExecuteReader
                    If oDbRed.HasRows Then
                        nResult = 1
                    Else
                        nResult = 0
                    End If
                End Using
            End Using
        Catch oEx As Exception
            Throw New Exception(oEx.Message)
        Finally
            oDbRed = Nothing
        End Try
        Return nResult
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="ptSQL">คำสั่ง SQL</param>
    ''' <param name="ptType">Type Return  t:strinf,n:int,c:double</param>
    ''' <returns>สภานะ|value  (1:OK,0:Error)</returns>
    Public Function C_GETtSQLExecuteScalar(ByVal ptSQL As String, ByVal ptType As String) As String
        Dim tResult As String = ""
        Dim tValue As String = ""
        Try
            Using oSqlConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSqlConn.Open()
                Using oSqlCmd As New SqlCommand(ptSQL, oSqlConn)
                    oSqlCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                    Select Case ptType
                        Case "t"
                            tValue = Convert.ToString(oSqlCmd.ExecuteScalar())
                            tResult = "1|" + tValue
                        Case "n"
                            tValue = Convert.ToInt32(oSqlCmd.ExecuteScalar())
                            tResult = "1|" + tValue
                        Case "c"
                            tValue = Convert.ToDouble(oSqlCmd.ExecuteScalar())
                            tResult = "1|" + tValue
                    End Select
                End Using
            End Using
        Catch oEx As Exception
            tResult = "0|" + oEx.Message
        Finally
        End Try
        Return tResult
    End Function

    ''' <summary>
    ''' สร้าง String Update
    ''' </summary>
    ''' <param name="ptWhoUpd">ชื่อผู้ Update</param>
    ''' <returns>string update 3 Field</returns>
    Public Function C_GETtSQLUpd(ByVal ptWhoUpd As String) As String
        Dim tResult As String = ""
        Dim oSql As New StringBuilder
        Dim tDateNow As String = Format(Now, "yyyy-MM-dd")
        Dim tTimeNow As String = Format(Now, "HH:mm:ss")
        Try
            oSql.Clear()
            oSql.Append("FDDateUpd='" + tDateNow + "'")
            oSql.Append(",FTTimeUpd='" + tTimeNow + "'")
            oSql.Append(",FTWhoUpd='" + ptWhoUpd + "'")
            tResult = oSql.ToString
        Catch ex As Exception
        End Try
        Return tResult
    End Function

    Public Function C_GEToTblSQL(ByVal ptSQL As String) As DataTable
        Dim oDbTbl As New DataTable
        Dim oDbApt As SqlDataAdapter
        Try
            Using oSqlConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSqlConn.Open()
                Using oSqlCmd As New SqlCommand(ptSQL, oSqlConn)
                    oSqlCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                    oDbApt = New SqlDataAdapter(oSqlCmd)
                    oDbApt.Fill(oDbTbl)
                End Using
            End Using
            Return oDbTbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function W_DAToExcuteds(ByVal ptSQL As String) As DataSet

        Try
            Dim oDbDts As New DataSet
            Dim oDbAdt As New SqlDataAdapter

            Using oSqlConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSqlConn.Open()
                Using oSqlCmd As New SqlCommand(ptSQL, oSqlConn)
                    oSqlCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                    oDbAdt = New SqlDataAdapter(oSqlCmd)
                    oDbAdt.Fill(oDbDts)
                End Using
            End Using

            Return oDbDts

        Catch oExn As SqlException
            Return Nothing
        End Try
    End Function

End Class
