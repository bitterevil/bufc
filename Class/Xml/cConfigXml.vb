﻿Public Class cConfigXml

    Property tInbox As String = ""
    Property tOutbox As String = ""
    Property tLog As String = ""
    Property tBackup As String = ""
    Property tTemp As String = ""
    Property nSplit As Integer = 0
    Property nCountIndex As Integer = 100000

    'sFTP '*CH 22-06-2017
    Property tInFolder As String = ""
    Property tOutFolder As String = ""
    Property tReconcile As String = ""
    Property tHostName As String = ""
    Property tUserName As String = ""
    Property tPassword As String = ""
    Property tPortNumber As String = ""
    Property tCashTransfer As String = ""
    Property tExpense As String = ""
    Property tFTPType As String = ""

End Class
