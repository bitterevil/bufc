﻿Imports System.IO

Public Class cLog

    Private Shared dDateNow As Date = Date.Now

    Public Shared Sub C_CALxWriteLog(ptMsg As String)

        Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\AdaLog" 'เพิ่ม Folder AdaLog *CH 02-12-2014 

        If Not IO.Directory.Exists(tPathLog) Then
            IO.Directory.CreateDirectory(tPathLog)
        End If
        tPathLog &= "\" & Format(dDateNow, "yyyyMMdd") & "_" & My.Application.Info.ProductName.ToArray & ".txt"

        If File.Exists(tPathLog) = False Then
            Using oSw As StreamWriter = File.CreateText(tPathLog)
                oSw.Close()
            End Using
        End If

        Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
            With oStreamWriter
                .WriteLine(AdaConfig.cConfig.oApplication.tUser & " - " & Format(Date.Now, "HH:mm:ss.fff") & ": " & ptMsg)
                .Flush()
                .Close()
            End With
        End Using
    End Sub

    Public Shared Sub C_CALxWriteLogAuto(ptMsg As String)

        Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\AdaLog" 'เพิ่ม Folder AdaLog *CH 02-12-2014

        If Not IO.Directory.Exists(tPathLog) Then
            IO.Directory.CreateDirectory(tPathLog)
        End If
        tPathLog &= "\" & Format(dDateNow, "yyyyMMdd") & "_" & My.Application.Info.ProductName.ToArray & "_Auto.txt"

        If File.Exists(tPathLog) = False Then
            Using oSw As StreamWriter = File.CreateText(tPathLog)
                oSw.Close()
            End Using
        End If

        Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
            With oStreamWriter
                .WriteLine("Auto - " & Format(Date.Now, "HH:mm:ss.fff") & ": " & ptMsg)
                .Flush()
                .Close()
            End With
        End Using
    End Sub

    'Public Shared Sub C_CALxDelete()

    '    Dim tPath As String = AdaConfig.cConfig.tAppPath
    'If AdaConfig.cConfig.oAdaSync.tActive = "1" Then
    '    If Directory.Exists(AdaConfig.cConfig.oAdaSync.tPathLog) Then
    '        tPath = AdaConfig.cConfig.oAdaSync.tPathLog
    '    End If
    'End If

    '    Dim oDirInfoInbox As New DirectoryInfo(tPath)
    '    Dim oFiles As FileInfo() = oDirInfoInbox.GetFiles("*.txt")
    '    For Each oItem In oFiles.Where(Function(c) c.Name.Length > 8).Where(Function(c) c.Name.Substring(0, 8) < Format(Date.Now.AddDays(-7), "yyyyMMdd"))
    '        Try
    '            If File.Exists(oItem.FullName) Then
    '                File.Delete(oItem.FullName)
    '            End If
    '        Catch ex As Exception
    '        End Try
    '    Next
    '    oFiles = Nothing
    '    oDirInfoInbox = Nothing
    'End Sub

End Class
