﻿Imports System.IO
Imports System.Data.SqlClient

Public Class cPromotion
    Private Shared tC_TableTemp As String = "TTmpPmt"
    Private Shared tC_TablePmt As String = "TCNTPmt"
    Private Shared tC_TableHis As String = "TLNKUpPmtHis"
    Public Shared tC_File As String = ""
    Private Shared tC_PathFile As String = ""
    Private Shared tC_BchCode As String = ""
    Private Shared oC_LIistVal As List(Of List(Of String))
    Private Shared oDatabase As New cDatabaseLocal
    Private Shared tC_BchHQ As String = ""
    Private Shared tC_UsrCode As String = ""
    Private Shared tC_DptCode As String = ""
    Private Shared nC_RuningCode As Integer = 0
    Private Shared tC_DateIns As String = ""
    Private Shared tC_TimeIns As String = ""
    Private Shared tC_UserName As String = "AdaLinkPTG"
    Private Shared nC_ValPropress As Double
    Private Shared nC_NextVal As Double
    Private Shared tC_AppPath As String = Application.StartupPath
    Private Shared tC_FolderLog As String = "\LogUpload"
    Private Shared tC_FileLog As String = ""
    Private Shared bC_WriteLog As Boolean = False
    Public Shared nC_GridRow As Integer = 0
    Public Shared nC_StaUpload As Integer = 0 '0:รอ, 1:สำเร็จ, 2:ไม่สำเร็จ, -1:ไฟล์ไม่ครบ

#Region "Class Property"
    'Promotion HD
    Private Class cPmtHD
        Property FTBchCode As String = ""
        Property FTPmhCode As String = ""
        Property FTPmhName As String = "Promotion Price Change"
        Property FTPmhNameSlip As String = "Promotion Price Change"
        Property FTSpmCode As String = ""
        Property FTSpmType As String = "1"
        Property FDPmhDStart As String = ""
        Property FDPmhDStop As String = ""
        Property FDPmhTStart As String = Format(Now, "HH:mm:ss")
        Property FDPmhTStop As String = Format(Now, "HH:mm:ss")
        Property FTPmhClosed As String = "0"
        Property FTPmhStatus As String = ""
        Property FTPmhRetOrWhs As String = "1"
        Property FTPmhRmk As String = ""
        Property FTPmhStaPrcDoc As String = "1"
        Property FNPmhStaAct As String = "1"
        Property FTDptCode As String = ""
        Property FTUsrCode As String = ""
        Property FTPmhApvCode As String = ""
        Property FTPmhBchTo As String = ""
        Property FTPmhZneTo As String = ""
        Property FTPmhStaExceptPmt As String = "1"
        Property FTSpmStaRcvFree As String = ""
        Property FTSpmStaAlwOffline As String = "2"
        Property FTSpmStaChkLimitGet As String = "2"
        Property FNPmhLimitNum As String = "1"
        Property FTPmhStaLimit As String = "1"
        Property FTPmhStaLimitCst As String = "1"
        Property FTSpmStaChkCst As String = "2"
        Property FNPmhCstNum As String = "1"
        Property FTSpmStaChkCstDOB As String = "2"
        Property FNPmhCstDobNum As String = "1"
        Property FNPmhCstDobPrev As String = "0"
        Property FNPmhCstDobNext As String = "0"
        Property FTSpmStaUseRange As String = "3"
        Property FTSplCode As String = ""
        Property FDPntSplStart As String = Format(Now, "yyyy/MM/dd")
        Property FDPntSplExpired As String = Format(Now, "yyyy/MM/dd")
        Property FTCgpCode As String = ""
        Property FDDateIns As String = ""
        Property FTTimeIns As String = ""
        Property FTWhoIns As String = ""
        Property FDDateUpd As String = ""
        Property FTTimeUpd As String = ""
        Property FTWhoUpd As String = ""
        Property FTCompName As String = My.Computer.Name
        Property FNLnkLine As String = ""
        Property FTLnkStatus As String = ""
        Property FTLnkLog As String = ""
    End Class

    'Promotion DT
    Private Class cPmtDT
        Property FTBchCode As String = ""
        Property FTPmhCode As String = ""
        Property FNPmdSeq As String = ""
        Property FTSpmCode As String = ""
        Property FTPmdGrpType As String = ""
        Property FTPmdGrpName As String = "Promotion Price Change"
        Property FTPdtCode As String = ""
        Property FTPmdBarCode As String = ""
        Property FTPdtName As String = ""
        Property FTPmdUnitName As String = ""
        Property FCPmdSetPriceOrg As String = "0"
        Property FDDateIns As String = ""
        Property FTTimeIns As String = ""
        Property FTWhoIns As String = ""
        Property FDDateUpd As String = ""
        Property FTTimeUpd As String = ""
        Property FTWhoUpd As String = ""
    End Class

    'Promotion CD
    Private Class cPmtCD
        Property FTBchCode As String = ""
        Property FTPmhCode As String = ""
        Property FNPmcSeq As String = ""
        Property FTSpmCode As String = ""
        Property FTPmcGrpName As String = "Promotion Price Change"
        Property FTPmcStaGrpCond As String = ""
        Property FCPmcPerAvgDis As String = ""
        Property FCPmcBuyAmt As String = ""
        Property FCPmcBuyQty As String = ""
        Property FCPmcBuyMinQty As String = ""
        Property FCPmcBuyMaxQty As String = ""
        Property FDPmcBuyMinTime As String = Format(Now, "HH:mm:ss")
        Property FDPmcBuyMaxTime As String = Format(Now, "HH:mm:ss")
        Property FCPmcGetCond As String = ""
        Property FCPmcGetValue As String = ""
        Property FCPmcGetQty As String = ""
        Property FTSpmStaBuy As String = "3"
        Property FTSpmStaRcv As String = "1"
        Property FTSpmStaAllPdt As String = "2"
        Property FDDateIns As String = ""
        Property FTTimeIns As String = ""
        Property FTWhoIns As String = ""
        Property FDDateUpd As String = ""
        Property FTTimeUpd As String = ""
        Property FTWhoUpd As String = ""
    End Class
#End Region

    Shared Sub C_CALxProcess()
        Dim oSql As New System.Text.StringBuilder
        Dim bHasNotComplete As Boolean = False
        Dim bFirst As Boolean = True

        'คำนวน Propress Bar '*CH 20-12-2014
        Dim nItem = wUploadPmt.aC_Detail.Count
        nItem = IIf(nItem <= 0, 1, nItem)
        nC_ValPropress = (100 / nItem) / 2
        nC_NextVal = nC_ValPropress
        wUploadPmt.oW_BackgroudWord.ReportProgress(0)
        Try
            'Get variable
            If nItem > 0 Then
                'Get Branch HQ
                C_GETxBchHQ()

                'Get User
                C_GETxUser()
            End If

            For Each oItem In wUploadPmt.aC_Detail
                bC_WriteLog = False
                tC_PathFile = oItem.tC_FullName
                tC_File = oItem.tC_Name
                tC_BchCode = oItem.tC_BchCode
                tC_DateIns = oItem.tC_DateIns
                tC_TimeIns = oItem.tC_TimeIns
                nC_GridRow = oItem.nC_GridRow

                'Create Temp Promotion
                C_SETxCreateTemp()

                'Get Running Code
                If bFirst Then C_GETxRunningPmhCode(tC_BchCode) : bFirst = False

                'Import upload promotion
                C_PRCxUploadPmt()

                'Log File
                tC_FileLog = "\LogUploadPmt_" & tC_DateIns.Replace("/", "") & tC_TimeIns.Replace(":", "") & ".txt"

                'Write Log file
                C_CALbSaveLog(bHasNotComplete)

                'บันทึก TLNKLog
                Dim aSqlB4 As New List(Of String)
                Dim tDesc As String = ""
                aSqlB4.Add("SELECT 'Insert('+CAST(ISNULL(COUNT(FTLnkStatus),0) AS VARCHAR(10))+')' FROM " & tC_TableTemp & "HD WHERE FTLnkStatus=1 AND FTCompName='" & My.Computer.Name & "'")
                aSqlB4.Add("SELECT 'Update('+CAST(ISNULL(COUNT(FTLnkStatus),0) AS VARCHAR(10))+')' FROM " & tC_TableTemp & "HD WHERE FTLnkStatus=2 AND FTCompName='" & My.Computer.Name & "'")
                aSqlB4.Add("SELECT 'Failed('+CAST(ISNULL(COUNT(FTLnkStatus),0) AS VARCHAR(10))+')' FROM " & tC_TableTemp & "HD WHERE FTLnkStatus=3 AND FTCompName='" & My.Computer.Name & "'")
                For Each tSql In aSqlB4
                    If tDesc.Length > 0 Then
                        tDesc &= ","
                    End If

                    Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                        oSQLConn.Open()
                        Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                        Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                        While oSQLRdWhe.Read()
                            tDesc &= oSQLRdWhe(0)
                        End While
                    End Using
                Next

                Dim tFileLog As String = ""
                If bHasNotComplete Then tFileLog = tC_AppPath & tC_FolderLog & tC_FileLog
                oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 1, tC_File, tDesc, tFileLog))

                'Runing Progress  *CH 20-12-2014
                wUploadPmt.oW_BackgroudWord.ReportProgress(nC_ValPropress)
                nC_ValPropress += nC_NextVal

                'Backup File Upload
                C_CALxClearTempFile()
            Next

            'Insert History
            oSql.AppendLine("INSERT INTO " & tC_TableHis)
            oSql.AppendLine("(FDDateIns,FTTimeIns,FTLnkStaPrc)")
            oSql.AppendLine("VALUES")
            oSql.AppendLine("('" & tC_DateIns & "','" & tC_TimeIns & "','0')")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

        Catch ex As Exception
        Finally
            oSql = Nothing
        End Try
    End Sub

    Private Shared Sub C_SETxCreateTemp()
        Dim oSql As System.Text.StringBuilder
        Dim aSqlB4 As New List(Of String)
        oSql = New System.Text.StringBuilder
        'History Upload Promotion
        oSql.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & tC_TableHis & "]') AND type in (N'U')) ")
        oSql.AppendLine("BEGIN ")
        oSql.AppendLine("CREATE TABLE [dbo].[" & tC_TableHis & "](")
        oSql.AppendLine("[FDDateIns] [datetime] NULL,")
        oSql.AppendLine("[FTTimeIns] [varchar](8) NULL,")
        oSql.AppendLine("[FTLnkStaPrc] [varchar](1) NULL,")
        oSql.AppendLine(") ON [PRIMARY]")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        'PmtHD
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & tC_TableTemp & "HD]') AND type in (N'U')) ")
        oSql.AppendLine("BEGIN ")
        oSql.AppendLine("TRUNCATE TABLE " & tC_TableTemp & "HD")
        oSql.AppendLine("END")
        oSql.AppendLine("Else BEGIN")
        oSql.AppendLine("SELECT * INTO " & tC_TableTemp & "HD FROM " & tC_TablePmt & "HD WHERE 2=1 ")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        'PmtDT
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & tC_TableTemp & "DT]') AND type in (N'U')) ")
        oSql.AppendLine("BEGIN ")
        oSql.AppendLine("TRUNCATE TABLE " & tC_TableTemp & "DT")
        oSql.AppendLine("END")
        oSql.AppendLine("Else BEGIN")
        oSql.AppendLine("SELECT * INTO " & tC_TableTemp & "DT FROM " & tC_TablePmt & "DT WHERE 2=1 ")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        'PmtCD
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & tC_TableTemp & "CD]') AND type in (N'U')) ")
        oSql.AppendLine("BEGIN ")
        oSql.AppendLine("TRUNCATE TABLE " & tC_TableTemp & "CD")
        oSql.AppendLine("END")
        oSql.AppendLine("Else BEGIN")
        oSql.AppendLine("SELECT * INTO " & tC_TableTemp & "CD FROM " & tC_TablePmt & "CD WHERE 2=1 ")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        'เพิ่ม Field CompName
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("IF NOT EXISTS ( SELECT c.Name FROM SysObjects o, SysColumns c WHERE o.id = c.id AND o.name = '" & tC_TableTemp & "HD' AND c.name = 'FTCompName' )")
        oSql.AppendLine("BEGIN")
        oSql.AppendLine("ALTER TABLE " & tC_TableTemp & "HD ADD FTCompName VARCHAR(255) NULL")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("IF NOT EXISTS ( SELECT c.Name FROM SysObjects o, SysColumns c WHERE o.id = c.id AND o.name = '" & tC_TableTemp & "HD' AND c.name = 'FTLnkStatus' )")
        oSql.AppendLine("BEGIN")
        oSql.AppendLine("ALTER TABLE " & tC_TableTemp & "HD ADD FTLnkStatus VARCHAR(1) NULL")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("IF NOT EXISTS ( SELECT c.Name FROM SysObjects o, SysColumns c WHERE o.id = c.id AND o.name = '" & tC_TableTemp & "HD' AND c.name = 'FTLnkLog' )")
        oSql.AppendLine("BEGIN")
        oSql.AppendLine("ALTER TABLE " & tC_TableTemp & "HD ADD FTLnkLog VARCHAR(255) NULL")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("IF NOT EXISTS ( SELECT c.Name FROM SysObjects o, SysColumns c WHERE o.id = c.id AND o.name = '" & tC_TableTemp & "HD' AND c.name = 'FNLnkLine' )")
        oSql.AppendLine("BEGIN")
        oSql.AppendLine("ALTER TABLE " & tC_TableTemp & "HD ADD FNLnkLine INTEGER NULL")
        oSql.AppendLine("END")
        aSqlB4.Add(oSql.ToString)
        oDatabase.C_CALnExecuteNonQuery(aSqlB4.ToArray)
        aSqlB4.Clear()
    End Sub

    Private Shared Sub C_GETxBchHQ()
        Dim tSql As String = "SELECT TOP 1 FTBchCode FROM TCNMBranch WHERE FTBchHQ = '1'"
        Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TCNMBranch")
        If oDbTbl IsNot Nothing Then
            If oDbTbl.Rows.Count > 0 Then
                tC_BchHQ = oDbTbl(0)("FTBchCode")
            End If
        End If
        oDbTbl = Nothing
    End Sub

    Private Shared Sub C_GETxUser()
        'UserCode
        Dim tSql As String = "SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END AS DEFUSER FROM TLNKMapping WHERE FTLNMCode='DEFUSER'"
        Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TLNKMapping")
        If oDbTbl IsNot Nothing Then
            If oDbTbl.Rows.Count > 0 Then
                tC_UsrCode = oDbTbl(0)("DEFUSER")
            End If
        End If
        tSql = "SELECT FTDptCode FROM TSysUser WHERE FTUsrCode = '" & tC_UsrCode & "'"
        oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TSysUser")
        If oDbTbl IsNot Nothing Then
            If oDbTbl.Rows.Count > 0 Then
                tC_DptCode = oDbTbl(0)("FTDptCode")
            End If
        End If
        oDbTbl = Nothing
    End Sub

    Private Shared Sub C_GETxRunningPmhCode(ByVal ptBchCode As String)
        Dim tSql As String = "SELECT MAX(FTPmhCode) AS FTPmhCode FROM TCNTPmtHD WHERE FTPmhCode LIKE '" & ptBchCode & "-%'"
        Dim oDbTbl = oDatabase.C_CALoExecuteReader(tSql, "TLNKMapping")
        Dim tPmhCode As String = ""
        If oDbTbl IsNot Nothing Then
            If oDbTbl.Rows.Count > 0 Then
                tPmhCode = IIf(IsDBNull(oDbTbl(0).Item("FTPmhCode")), "-0", oDbTbl(0)("FTPmhCode"))
                Dim tPmhRuning = tPmhCode.Split("-")(1)
                nC_RuningCode = Convert.ToInt32(tPmhRuning) + 1
            End If
        End If
    End Sub

    Private Shared Sub C_PRCxUploadPmt()
        oC_LIistVal = New List(Of List(Of String))
        Try
            oC_LIistVal = C_GEToReadExcel()
            If oC_LIistVal.Count > 0 Then
                C_RECxSavePromotion()
                nC_StaUpload = 1
            Else
                'Write log Not Read tC_File
                'Runing Progress  *CH 20-12-2014
                nC_StaUpload = 2
                wUploadPmt.oW_BackgroudWord.ReportProgress(nC_ValPropress)
                nC_ValPropress += nC_NextVal
            End If
        Catch ex As Exception
            'Write log Exception Error tC_File
            'Runing Progress  *CH 20-12-2014
            nC_StaUpload = 2
            wUploadPmt.oW_BackgroudWord.ReportProgress(nC_ValPropress)
            nC_ValPropress += nC_NextVal
        End Try
    End Sub

    Private Shared Function C_GEToReadExcel() As List(Of List(Of String))
        Dim oList As New List(Of List(Of String))
        Try
            'Read Excel file
            Dim oC1XLBook As New C1.C1Excel.C1XLBook
            oC1XLBook.Load(tC_PathFile)
            With oC1XLBook.Sheets(0)
                Dim iColCount As Integer = 10 - 1
                Dim iRowCount As Integer = .Rows.Count - 1
                Dim iCrrCol As Integer = 0
                Dim oTemp As List(Of String)
                For iCrrRow As Integer = 1 To iRowCount
                    oTemp = New List(Of String)
                    For iCrrCol = 0 To iColCount
                        Try
                            oTemp.Add(.GetCell(iCrrRow, iCrrCol).Text)
                        Catch ex As Exception
                            oTemp.Add("")
                        End Try
                    Next
                    oList.Add(oTemp)
                Next
            End With
        Catch ex As Exception
            Throw ex
        End Try
        Return oList
    End Function

    Private Shared Sub C_RECxSavePromotion()
        Dim oSql As System.Text.StringBuilder
        Dim oPmtHD As cPmtHD = Nothing
        Dim oPmtDT As cPmtDT = Nothing
        Dim oPmtCD As cPmtCD = Nothing
        Dim oLPmtHD As New List(Of cPmtHD)
        Dim oLPmtDT As New List(Of cPmtDT)
        Dim oLPmtCD As New List(Of cPmtCD)
        Dim nItem As Integer = 0
        Dim bStaChkBar As Boolean = False
        Dim tDStart As String = ""
        Dim tDStop As String = ""
        Try
            For Each oItem In oC_LIistVal
                oPmtHD = New cPmtHD
                With oPmtHD
                    tDStart = IIf(oItem(8).Length = 10, oItem(8), "NULL")
                    tDStop = IIf(oItem(9).Length = 10, oItem(9), "NULL")
                    If tDStart <> "NULL" Then tDStart = tDStart.Split(".")(2) & "/" & tDStart.Split(".")(1) & "/" & tDStart.Split(".")(0)
                    If tDStop <> "NULL" Then tDStop = IIf(CInt(tDStop.Split(".")(2)) > 2099, "2099", tDStop.Split(".")(2)) & "/" & tDStop.Split(".")(1) & "/" & tDStop.Split(".")(0)
                    .FTBchCode = tC_BchHQ
                    .FTPmhBchTo = tC_BchCode
                    .FTPmhCode = tC_BchCode & "-" & Convert.ToString(nC_RuningCode).PadLeft(6, "0")
                    .FTPmhName &= ":" & oItem(3)
                    .FTSpmCode = "001" 'Fix
                    .FDPmhDStart = tDStart
                    .FDPmhDStop = tDStop
                    .FDPmhTStart = IIf(tDStart <> "NULL", tDStart & " 00:00:00", "NULL")
                    .FDPmhTStop = IIf(tDStop <> "NULL", tDStop & " 23:59:59", "NULL")
                    .FTPmhStatus = "2" 'Fix
                    .FTPmhRmk = ""
                    .FTSpmStaRcvFree = "2" 'Fix
                    .FTDptCode = tC_DptCode
                    .FTUsrCode = tC_UsrCode
                    .FTPmhApvCode = tC_UsrCode
                    .FNLnkLine = nItem + 2
                    .FTLnkStatus = "1"
                    .FDDateIns = tC_DateIns
                    .FTTimeIns = tC_TimeIns
                    .FTWhoIns = tC_UserName
                    .FDDateUpd = tC_DateIns
                    .FTTimeUpd = tC_TimeIns
                    .FTWhoUpd = tC_UserName
                    nC_RuningCode += 1
                End With
                oLPmtHD.Add(oPmtHD)

                'Get BarCode
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("SELECT FTPdtCode,FTPdtName,FTPdtBarCode FROM [dbo].[SI_VCNMPdtByBar]")
                oSql.AppendLine("WHERE FTPdtStkCode = '" & oItem(3) & "' AND FTPdtUnit = '" & oItem(4) & "'")
                Dim oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
                If oDbTbl.Rows.Count > 0 Then
                    oDbTbl = New DataTable
                    oSql = New System.Text.StringBuilder
                    oSql.AppendLine("SELECT FTPdtCode,FTPdtName,FTPdtBarCode FROM [dbo].[SI_VCNMPdtByBar]")
                    oSql.AppendLine("WHERE FTPdtStkCode = '" & oItem(3) & "' AND FTPdtUnit = '" & oItem(4) & "' AND FTPdtSaleType <> '3'")
                    oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
                    If oDbTbl.Rows.Count > 0 Then
                        For nRowDT = 0 To oDbTbl.Rows.Count - 1
                            oPmtDT = New cPmtDT
                            With oPmtDT
                                .FTBchCode = tC_BchCode
                                .FTPmhCode = oLPmtHD(nItem).FTPmhCode
                                .FTSpmCode = oLPmtHD(nItem).FTSpmCode
                                .FNPmdSeq = nRowDT + 1
                                .FTPmdGrpType = "1" 'Fix
                                .FTPmdBarCode = oDbTbl(nRowDT)("FTPdtBarCode")
                                .FTPdtCode = oDbTbl(nRowDT)("FTPdtCode")
                                .FTPdtName = oDbTbl(nRowDT)("FTPdtName")
                                .FTPmdUnitName = oItem(4)
                                .FDDateIns = oLPmtHD(nItem).FDDateIns
                                .FTTimeIns = oLPmtHD(nItem).FTTimeIns
                                .FTWhoIns = tC_UserName
                                .FDDateUpd = oLPmtHD(nItem).FDDateUpd
                                .FTTimeUpd = oLPmtHD(nItem).FTTimeUpd
                                .FTWhoUpd = tC_UserName
                            End With
                            oLPmtDT.Add(oPmtDT)
                        Next
                        bStaChkBar = True
                    Else
                        nC_RuningCode -= 1
                        oLPmtHD(nItem).FTLnkStatus = "3"
                        oLPmtHD(nItem).FTLnkLog = oItem(3) & ";" & oItem(4) & ";เป็นสินค้าเครื่องชั่ง"
                        bStaChkBar = False
                    End If
                Else
                    nC_RuningCode -= 1
                    oLPmtHD(nItem).FTLnkStatus = "3"
                    oLPmtHD(nItem).FTLnkLog = oItem(3) & ";" & oItem(4) & ";ไม่มีอยู่ในฐานข้อมูล"
                    bStaChkBar = False
                End If

                oPmtCD = New cPmtCD
                If bStaChkBar Then
                    With oPmtCD
                        .FTBchCode = oLPmtHD(nItem).FTBchCode
                        .FTPmhCode = oLPmtHD(nItem).FTPmhCode
                        .FTSpmCode = oLPmtHD(nItem).FTSpmCode
                        .FNPmcSeq = "1"
                        .FTPmcStaGrpCond = "3"
                        .FCPmcPerAvgDis = "100"
                        .FCPmcBuyAmt = "0"
                        .FCPmcBuyQty = "1"
                        .FCPmcBuyMinQty = "1"
                        .FCPmcBuyMaxQty = "0"
                        .FCPmcGetCond = "3"
                        .FCPmcGetValue = oItem(5)
                        .FCPmcGetQty = "1"
                        .FDDateIns = oLPmtHD(nItem).FDDateIns
                        .FTTimeIns = oLPmtHD(nItem).FTTimeIns
                        .FTWhoIns = tC_UserName
                        .FDDateUpd = oLPmtHD(nItem).FDDateUpd
                        .FTTimeUpd = oLPmtHD(nItem).FTTimeUpd
                        .FTWhoUpd = tC_UserName
                    End With
                    oLPmtCD.Add(oPmtCD)
                End If

                nItem += 1
            Next

            'Insert promotion
            C_INSxPromotion(oLPmtHD, oLPmtDT, oLPmtCD)
        Catch ex As Exception
        Finally
            oSql = Nothing
            oPmtHD = Nothing
            oPmtDT = Nothing
            oPmtCD = Nothing
        End Try
    End Sub

    Private Shared Sub C_INSxPromotion(ByVal poPmtHD As List(Of cPmtHD), ByVal poPmtDT As List(Of cPmtDT), ByVal poPmtCD As List(Of cPmtCD))
        Try
            'Insert to Temp
            C_INSxPmt(poPmtHD) 'PmtHD
            C_INSxPmt(poPmtDT) 'PmtDT
            C_INSxPmt(poPmtCD) 'PmtCD

            'Runing Progress  *CH 20-12-2014
            wUploadPmt.oW_BackgroudWord.ReportProgress(nC_ValPropress)
            nC_ValPropress += nC_NextVal

            'Insert to Real Table promotion
            C_INSxPromotion("HD")
            C_INSxPromotion("DT")
            C_INSxPromotion("CD")

        Catch ex As Exception
        End Try
    End Sub

    Private Overloads Shared Sub C_INSxPmt(ByVal poPmtHD As List(Of cPmtHD))
        Dim oSql As New System.Text.StringBuilder
        Dim nRow As Integer = 0
        Try
            oSql.AppendLine("INSERT INTO " & tC_TableTemp & "HD(")
            oSql.AppendLine("FTBchCode, FTPmhCode, FTPmhName,")
            oSql.AppendLine("FTPmhNameSlip, FTSpmCode, FTSpmType,")
            oSql.AppendLine("FDPmhDStart, FDPmhDStop, FDPmhTStart,")
            oSql.AppendLine("FDPmhTStop, FTPmhClosed, FTPmhStatus,")
            oSql.AppendLine("FTPmhRetOrWhs, FTPmhRmk, FTPmhStaPrcDoc,")
            oSql.AppendLine("FNPmhStaAct, FTDptCode, FTUsrCode,")
            oSql.AppendLine("FTPmhApvCode, FTPmhBchTo, FTPmhZneTo,")
            oSql.AppendLine("FTPmhStaExceptPmt, FTSpmStaRcvFree, FTSpmStaAlwOffline,")
            oSql.AppendLine("FTSpmStaChkLimitGet, FNPmhLimitNum, FTPmhStaLimit,")
            oSql.AppendLine("FTPmhStaLimitCst, FTSpmStaChkCst, FNPmhCstNum,")
            oSql.AppendLine("FTSpmStaChkCstDOB, FNPmhCstDobNum, FNPmhCstDobPrev,")
            oSql.AppendLine("FNPmhCstDobNext, FTSpmStaUseRange, FTSplCode,")
            oSql.AppendLine("FDPntSplStart, FDPntSplExpired, FTCgpCode,")
            oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns,")
            oSql.AppendLine("FNLnkLine, FTLnkLog, FTLnkStatus, FTCompName)")
            For Each oPmt In poPmtHD
                With oPmt
                    If nRow = 0 Then
                        oSql.AppendLine("SELECT")
                    Else
                        oSql.AppendLine("UNION ALL SELECT")
                    End If
                    nRow += 1
                    oSql.AppendLine("'" & .FTBchCode & "','" & .FTPmhCode & "','" & .FTPmhName & "',")
                    oSql.AppendLine("'" & .FTPmhNameSlip & "','" & .FTSpmCode & "','" & .FTSpmType & "',")
                    oSql.AppendLine("'" & .FDPmhDStart & "','" & .FDPmhDStop & "','" & .FDPmhTStart & "',")
                    oSql.AppendLine("'" & .FDPmhTStop & "','" & .FTPmhClosed & "','" & .FTPmhStatus & "',")
                    oSql.AppendLine("'" & .FTPmhRetOrWhs & "','" & .FTPmhRmk & "','" & .FTPmhStaPrcDoc & "',")
                    oSql.AppendLine("" & .FNPmhStaAct & ",'" & .FTDptCode & "','" & .FTUsrCode & "',")
                    oSql.AppendLine("'" & .FTPmhApvCode & "','" & .FTPmhBchTo & "','" & .FTPmhZneTo & "',")
                    oSql.AppendLine("'" & .FTPmhStaExceptPmt & "','" & .FTSpmStaRcvFree & "','" & .FTSpmStaAlwOffline & "',")
                    oSql.AppendLine("'" & .FTSpmStaChkLimitGet & "','" & .FNPmhLimitNum & "','" & .FTPmhStaLimit & "',")
                    oSql.AppendLine("'" & .FTPmhStaLimitCst & "','" & .FTSpmStaChkCst & "'," & .FNPmhCstNum & ",")
                    oSql.AppendLine("'" & .FTSpmStaChkCstDOB & "'," & .FNPmhCstDobNum & "," & .FNPmhCstDobPrev & ",")
                    oSql.AppendLine("" & .FNPmhCstDobNext & ",'" & .FTSpmStaUseRange & "','" & .FTSplCode & "',")
                    oSql.AppendLine("'" & .FDPntSplStart & "','" & .FDPntSplExpired & "','" & .FTCgpCode & "',")
                    oSql.AppendLine("'" & .FDDateIns & "','" & .FTTimeIns & "','" & .FTWhoIns & "',")
                    oSql.AppendLine("'" & .FDDateUpd & "','" & .FTTimeUpd & "','" & .FTWhoUpd & "',")
                    oSql.AppendLine(.FNLnkLine & ",'" & .FTLnkLog & "','" & .FTLnkStatus & "','" & .FTCompName & "'")
                End With
            Next
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))

        Catch ex As Exception
        Finally
            oSql = Nothing
        End Try
    End Sub

    Private Overloads Shared Sub C_INSxPmt(ByVal poPmtDT As List(Of cPmtDT))
        Dim oSql As New System.Text.StringBuilder
        Dim nRow As Integer = 0
        Try
            oSql.AppendLine("INSERT INTO " & tC_TableTemp & "DT(")
            oSql.AppendLine("FTBchCode, FTPmhCode, FNPmdSeq,")
            oSql.AppendLine("FTSpmCode, FTPmdGrpType, FTPmdGrpName,")
            oSql.AppendLine("FTPdtCode, FTPmdBarCode, FTPdtName,")
            oSql.AppendLine("FTPmdUnitName, FCPmdSetPriceOrg,")
            oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd,")
            oSql.AppendLine("FDDateIns, FTTimeIns, FTWhoIns)")
            For Each oPmt In poPmtDT
                With oPmt
                    If nRow = 0 Then
                        oSql.AppendLine("SELECT")
                    Else
                        oSql.AppendLine("UNION ALL SELECT")
                    End If
                    nRow += 1
                    oSql.AppendLine("'" & .FTBchCode & "','" & .FTPmhCode & "','" & .FNPmdSeq & "',")
                    oSql.AppendLine("'" & .FTSpmCode & "','" & .FTPmdGrpType & "','" & .FTPmdGrpName & "',")
                    oSql.AppendLine("'" & .FTPdtCode & "','" & .FTPmdBarCode & "','" & .FTPdtName & "',")
                    oSql.AppendLine("'" & .FTPmdUnitName & "','" & .FCPmdSetPriceOrg & "',")
                    oSql.AppendLine("'" & .FDDateUpd & "','" & .FTTimeUpd & "','" & .FTWhoUpd & "',")
                    oSql.AppendLine("'" & .FDDateIns & "','" & .FTTimeIns & "','" & .FTWhoIns & "'")
                End With
            Next
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
        Catch ex As Exception
        Finally
            oSql = Nothing
        End Try
    End Sub

    Private Overloads Shared Sub C_INSxPmt(ByVal poPmtCD As List(Of cPmtCD))
        Dim oSql As New System.Text.StringBuilder
        Dim nRow As Integer = 0
        Try
            oSql.AppendLine("INSERT INTO " & tC_TableTemp & "CD(")
            oSql.AppendLine("FTBchCode, FTPmhCode, FNPmcSeq,")
            oSql.AppendLine("FTSpmCode, FTPmcGrpName, FTPmcStaGrpCond,")
            oSql.AppendLine("FCPmcPerAvgDis, FCPmcBuyAmt, FCPmcBuyQty,")
            oSql.AppendLine("FCPmcBuyMinQty, FCPmcBuyMaxQty, FDPmcBuyMinTime,")
            oSql.AppendLine("FDPmcBuyMaxTime, FCPmcGetCond, FCPmcGetValue,")
            oSql.AppendLine("FCPmcGetQty, FTSpmStaBuy, FTSpmStaRcv, FTSpmStaAllPdt,")
            oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns)")
            For Each oPmt In poPmtCD
                With oPmt
                    If nRow = 0 Then
                        oSql.AppendLine("SELECT")
                    Else
                        oSql.AppendLine("UNION ALL SELECT")
                    End If
                    nRow += 1
                    oSql.AppendLine("'" & .FTBchCode & "','" & .FTPmhCode & "'," & .FNPmcSeq & ",")
                    oSql.AppendLine("'" & .FTSpmCode & "','" & .FTPmcGrpName & "','" & .FTPmcStaGrpCond & "',")
                    oSql.AppendLine("" & .FCPmcPerAvgDis & "," & .FCPmcBuyAmt & "," & .FCPmcBuyQty & ",")
                    oSql.AppendLine("" & .FCPmcBuyMinQty & "," & .FCPmcBuyMaxQty & ",'" & .FDPmcBuyMinTime & "',")
                    oSql.AppendLine("'" & .FDPmcBuyMaxTime & "','" & .FCPmcGetCond & "','" & .FCPmcGetValue & "',")
                    oSql.AppendLine("'" & .FCPmcGetQty & "','" & .FTSpmStaBuy & "','" & .FTSpmStaRcv & "','" & .FTSpmStaAllPdt & "',")
                    oSql.AppendLine("'" & .FDDateUpd & "','" & .FTTimeUpd & "','" & .FTWhoUpd & "',")
                    oSql.AppendLine("'" & .FDDateIns & "','" & .FTTimeIns & "','" & .FTWhoIns & "'")
                End With
            Next
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString.Replace("'NULL'", "NULL"))
        Catch ex As Exception
        Finally
            oSql = Nothing
        End Try
    End Sub

    'Insert TCNTPmtHD,DT,CD
    Private Shared Sub C_INSxPromotion(ByVal ptCase As String)
        Dim oSql As New System.Text.StringBuilder
        Try
            Select Case ptCase.ToUpper
                Case "HD"
                    oSql.AppendLine("INSERT INTO " & tC_TablePmt & "HD(")
                    oSql.AppendLine("FTBchCode, FTPmhCode, FTPmhName,")
                    oSql.AppendLine("FTPmhNameSlip, FTSpmCode, FTSpmType,")
                    oSql.AppendLine("FDPmhDStart, FDPmhDStop, FDPmhTStart,")
                    oSql.AppendLine("FDPmhTStop, FTPmhClosed, FTPmhStatus,")
                    oSql.AppendLine("FTPmhRetOrWhs, FTPmhRmk, FTPmhStaPrcDoc,")
                    oSql.AppendLine("FNPmhStaAct, FTDptCode, FTUsrCode,")
                    oSql.AppendLine("FTPmhApvCode, FTPmhBchTo, FTPmhZneTo,")
                    oSql.AppendLine("FTPmhStaExceptPmt, FTSpmStaRcvFree, FTSpmStaAlwOffline,")
                    oSql.AppendLine("FTSpmStaChkLimitGet, FNPmhLimitNum, FTPmhStaLimit,")
                    oSql.AppendLine("FTPmhStaLimitCst, FTSpmStaChkCst, FNPmhCstNum,")
                    oSql.AppendLine("FTSpmStaChkCstDOB, FNPmhCstDobNum, FNPmhCstDobPrev,")
                    oSql.AppendLine("FNPmhCstDobNext, FTSpmStaUseRange, FTSplCode,")
                    oSql.AppendLine("FDPntSplStart, FDPntSplExpired, FTCgpCode,")
                    oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns)")
                    oSql.AppendLine("SELECT")
                    oSql.AppendLine("FTBchCode, FTPmhCode, FTPmhName,")
                    oSql.AppendLine("FTPmhNameSlip, FTSpmCode, FTSpmType,")
                    oSql.AppendLine("FDPmhDStart, FDPmhDStop, FDPmhTStart,")
                    oSql.AppendLine("FDPmhTStop, FTPmhClosed, FTPmhStatus,")
                    oSql.AppendLine("FTPmhRetOrWhs, FTPmhRmk, FTPmhStaPrcDoc,")
                    oSql.AppendLine("FNPmhStaAct, FTDptCode, FTUsrCode,")
                    oSql.AppendLine("FTPmhApvCode, FTPmhBchTo, FTPmhZneTo,")
                    oSql.AppendLine("FTPmhStaExceptPmt, FTSpmStaRcvFree, FTSpmStaAlwOffline,")
                    oSql.AppendLine("FTSpmStaChkLimitGet, FNPmhLimitNum, FTPmhStaLimit,")
                    oSql.AppendLine("FTPmhStaLimitCst, FTSpmStaChkCst, FNPmhCstNum,")
                    oSql.AppendLine("FTSpmStaChkCstDOB, FNPmhCstDobNum, FNPmhCstDobPrev,")
                    oSql.AppendLine("FNPmhCstDobNext, FTSpmStaUseRange, FTSplCode,")
                    oSql.AppendLine("FDPntSplStart, FDPntSplExpired, FTCgpCode,")
                    oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns")
                    oSql.AppendLine("FROM " & tC_TableTemp & "HD")
                    oSql.AppendLine("WHERE FTLnkStatus = '1' AND FTCompName = '" & My.Computer.Name & "'")
                Case "DT"
                    oSql.AppendLine("INSERT INTO " & tC_TablePmt & "DT(")
                    oSql.AppendLine("FTBchCode, FTPmhCode, FNPmdSeq,")
                    oSql.AppendLine("FTSpmCode, FTPmdGrpType, FTPmdGrpName,")
                    oSql.AppendLine("FTPdtCode, FTPmdBarCode, FTPdtName,")
                    oSql.AppendLine("FTPmdUnitName, FCPmdSetPriceOrg,")
                    oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd,")
                    oSql.AppendLine("FDDateIns, FTTimeIns, FTWhoIns)")
                    oSql.AppendLine("SELECT")
                    oSql.AppendLine("FTBchCode, FTPmhCode, FNPmdSeq,")
                    oSql.AppendLine("FTSpmCode, FTPmdGrpType, FTPmdGrpName,")
                    oSql.AppendLine("FTPdtCode, FTPmdBarCode, FTPdtName,")
                    oSql.AppendLine("FTPmdUnitName, FCPmdSetPriceOrg,")
                    oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd,")
                    oSql.AppendLine("FDDateIns, FTTimeIns, FTWhoIns")
                    oSql.AppendLine("FROM " & tC_TableTemp & "DT")
                    oSql.AppendLine("WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FTLnkStatus = '1' AND FTCompName = '" & My.Computer.Name & "')")
                Case "CD"
                    oSql.AppendLine("INSERT INTO " & tC_TablePmt & "CD(")
                    oSql.AppendLine("FTBchCode, FTPmhCode, FNPmcSeq,")
                    oSql.AppendLine("FTSpmCode, FTPmcGrpName, FTPmcStaGrpCond,")
                    oSql.AppendLine("FCPmcPerAvgDis, FCPmcBuyAmt, FCPmcBuyQty,")
                    oSql.AppendLine("FCPmcBuyMinQty, FCPmcBuyMaxQty, FDPmcBuyMinTime,")
                    oSql.AppendLine("FDPmcBuyMaxTime, FCPmcGetCond, FCPmcGetValue,")
                    oSql.AppendLine("FCPmcGetQty, FTSpmStaBuy, FTSpmStaRcv, FTSpmStaAllPdt,")
                    oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns)")
                    oSql.AppendLine("SELECT")
                    oSql.AppendLine("FTBchCode, FTPmhCode, FNPmcSeq,")
                    oSql.AppendLine("FTSpmCode, FTPmcGrpName, FTPmcStaGrpCond,")
                    oSql.AppendLine("FCPmcPerAvgDis, FCPmcBuyAmt, FCPmcBuyQty,")
                    oSql.AppendLine("FCPmcBuyMinQty, FCPmcBuyMaxQty, FDPmcBuyMinTime,")
                    oSql.AppendLine("FDPmcBuyMaxTime, FCPmcGetCond, FCPmcGetValue,")
                    oSql.AppendLine("FCPmcGetQty, FTSpmStaBuy, FTSpmStaRcv, FTSpmStaAllPdt,")
                    oSql.AppendLine("FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns")
                    oSql.AppendLine("FROM " & tC_TableTemp & "CD")
                    oSql.AppendLine("WHERE FTPmhCode IN (SELECT FTPmhCode FROM " & tC_TableTemp & "HD WHERE FTLnkStatus = '1' AND FTCompName = '" & My.Computer.Name & "')")
            End Select
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
        Catch ex As Exception
        Finally
            oSql = Nothing
        End Try
    End Sub

    Private Shared Sub C_CALbSaveLog(ByRef pbHavNotComplete As Boolean)
        Dim tFmtLineHead As String = "[Upload File] {0}" & vbTab & "[Upload Date] {1}" & vbTab & "[Upload Time] {2}" & vbTab & "[Upload By] {3}"
        Dim tFmtLineColumn As String = "[Seq]" & vbTab & "[Article]" & vbTab & "[Unit]" & vbTab & "[Log]"
        Dim tFmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}"
        Try
            Dim oDbTbl As New DataTable
            oDbTbl = oDatabase.C_CALoExecuteReader("SELECT FNLnkLine,FTLnkLog FROM " & tC_TableTemp & "HD WHERE FTLnkStatus = '3' AND FTCompName = '" & My.Computer.Name & "'")
            'Count จำนวน Error มากว่า 0 ถึงจะสร้าง File Log
            If oDbTbl Is Nothing Then
                pbHavNotComplete = False
                Exit Sub
            Else
                pbHavNotComplete = True
            End If

            'ถ้าไม่มี Directory ให้สร้างขึ้นใหม่ '*CH 17-10-2014
            Dim tPath As String = tC_AppPath & tC_FolderLog
            If Not IO.Directory.Exists(tPath) Then
                IO.Directory.CreateDirectory(tPath)
            End If

            'ถ้าไม่มีไฟล์ให้สร้างขึ้นใหม่ '*CH 17-10-2014
            Dim tPathFileLog As String = tPath & tC_FileLog
            If Not File.Exists(tPathFileLog) Then
                Using oStreamWriter As New StreamWriter(tPathFileLog, False, System.Text.ASCIIEncoding.Default)
                    oStreamWriter.Close()
                End Using
            End If
            cCNVB.bVB_DocNotComplete = True
            cCNVB.tVB_MsgExportSale = String.Format(cCNMS.tMS_CN112, tPathFileLog)

            If oDbTbl.Rows.Count > 0 Then
                Using oStreamWriter As StreamWriter = File.AppendText(tPathFileLog)
                    With oStreamWriter
                        If Not bC_WriteLog Then
                            .WriteLine(String.Format(tFmtLineHead, tC_File, tC_DateIns, tC_TimeIns, tC_UserName))
                            .WriteLine(tFmtLineColumn)
                            .Flush()
                        End If

                        For Each oLog In oDbTbl.Rows '.OrderBy(Function(c) c.nLineNo)
                            oStreamWriter.WriteLine(String.Format(tFmtLineData, oLog("FNLnkLine"), oLog("FTLnkLog").ToString.Split(";")(0), _
                                                                  oLog("FTLnkLog").ToString.Split(";")(1), oLog("FTLnkLog").ToString.Split(";")(2)))
                            .Flush()
                        Next
                        .Close()
                    End With
                End Using
            End If

            bC_WriteLog = True
        Catch ex As Exception
        End Try
    End Sub

    Private Shared Sub C_CALxClearTempFile()
        Dim tFileName As String = ""
        Dim aFile As String() = {tC_PathFile}
        Try
            Dim nIndex As Integer = 0
            For Each tFile In aFile
                tFileName = Path.GetFileName(tFile)
                FileCopy(tFile, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tFileName)
                If File.Exists(tFile) Then
                    File.Delete(tFile)
                End If
                nIndex += 1
            Next
        Catch ex As Exception
        End Try
    End Sub
End Class