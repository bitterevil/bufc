﻿Imports System.IO

Public Class cPurgeAuto
    Public Sub C_CALxProcess()
        Dim oSql As System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As DataTable
        Dim tPrgDay As String = cCNVB.nVB_PurgeDay
        Try
            'Get Config Purge
            oSql = New System.Text.StringBuilder
            oDbTbl = New DataTable
            oSql.Append("SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FTPurge FROM TLNKMapping WHERE FTLNMCode='PURGE'")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                tPrgDay = oDbTbl.Rows(0)("FTPurge")
                If Convert.ToInt32(tPrgDay) < 1 Then tPrgDay = "1"
                Console.WriteLine("Purge data (" & tPrgDay & " days)...")
            Else
                Exit Sub
            End If

            'Delete data
            'Console.WriteLine("DB: TLGTZGLINT001")
            cLog.C_CALxWriteLogAuto("DB: TLGTZGLINT001")
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("DELETE FROM TLGTZGLINT001")
            oSql.AppendLine("WHERE CONVERT(VARCHAR(10), GETDATE(), 121) > CONVERT(VARCHAR(10), FDDateUpd + (" & tPrgDay & "), 121) ")
            oSql.AppendLine("AND (FTExpFileName <> '' AND FTExpStaSend = '1')")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            cLog.C_CALxWriteLogAuto("DB: TLGTZGLINT002")
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("DELETE FROM TLGTZGLINT002")
            oSql.AppendLine("WHERE CONVERT(VARCHAR(10), GETDATE(), 121) > CONVERT(VARCHAR(10), FDDateUpd + (" & tPrgDay & "), 121) ")
            oSql.AppendLine("AND (FTExpFileName <> '' AND FTExpStaSend = '1')")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            cLog.C_CALxWriteLogAuto("DB: TLGTZSDINT005")
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("DELETE FROM TLGTZSDINT005")
            oSql.AppendLine("WHERE CONVERT(VARCHAR(10), GETDATE(), 121) > CONVERT(VARCHAR(10), FDDateUpd + (" & tPrgDay & "), 121) ")
            oSql.AppendLine("AND (FTExpFileName <> '' AND FTExpStaSend = '1')")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            Dim nDatePrg As Integer = Convert.ToInt32(Format(Now, "yyyyMMdd"))
            'Delete Backup
            Dim tPathBak As String = AdaConfig.cConfig.oConfigXml.tBackup
            'Console.WriteLine("Folder: " & tPathBak)
            cLog.C_CALxWriteLogAuto("Folder: " & tPathBak)
            Dim atDir = IO.Directory.GetDirectories(tPathBak)
            For nDir = 0 To atDir.Length - 1
                'Import
                Dim tFolderBak As String = atDir(nDir)
                Dim tDateBak As String = Left(tFolderBak.Split("\")(tFolderBak.Split("\").Length - 1), 8)
                Dim nDateBak As Integer = 0
                If IsNumeric(tDateBak) Then
                    nDateBak = Convert.ToInt32(tDateBak)
                    nDateBak = nDateBak + Convert.ToInt32(tPrgDay)
                    If nDateBak < nDatePrg Then IO.Directory.Delete(tFolderBak, True) 'Delete
                End If

                'Export
                tDateBak = Right(tFolderBak.Split("\")(tFolderBak.Split("\").Length - 1), 8)
                If Not IsNumeric(tDateBak) Then Continue For
                nDateBak = Convert.ToInt32(tDateBak)
                nDateBak = nDateBak + Convert.ToInt32(tPrgDay)
                If nDateBak < nDatePrg Then IO.Directory.Delete(tFolderBak, True) 'Delete
            Next

            'Delete AdaLog
            Dim tPathAdaLog As String = AdaConfig.cConfig.tAppPath & "\AdaLog"
            'Console.WriteLine("Folder: " & tPathAdaLog)
            cLog.C_CALxWriteLogAuto("Folder: " & tPathAdaLog)
            If IO.Directory.Exists(tPathAdaLog) Then
                Dim odiAda As New System.IO.DirectoryInfo(tPathAdaLog)
                Dim atFile = odiAda.GetFiles("*.txt")
                For nFle = 0 To atFile.Length - 1
                    'Dim tDateAdaLog As String = Left(atFile(nFle).Name, 8)
                    Dim tDateAdaLog As String = Format(atFile(nFle).LastWriteTime, "yyyyMMdd")
                    If Not IsNumeric(tDateAdaLog) Then Continue For
                    Dim nDateAdaLog As Integer = Convert.ToInt32(tDateAdaLog)
                    nDateAdaLog = nDateAdaLog + Convert.ToInt32(tPrgDay)
                    If nDateAdaLog < nDatePrg Then IO.File.Delete(atFile(nFle).FullName) 'Delete
                Next
            End If

            'Delete LogExport
            Dim tPathLogExp As String = Path.GetDirectoryName(AdaConfig.cConfig.oConfigXml.tOutbox) & "\LogExport" 'AdaConfig.cConfig.tAppPath & "\LogExport"
            'Console.WriteLine("Folder: " & tPathLogExp)
            cLog.C_CALxWriteLogAuto("Folder: " & tPathLogExp)
            If IO.Directory.Exists(tPathLogExp) Then
                Dim odiExp As New System.IO.DirectoryInfo(tPathLogExp)
                Dim atFile = odiExp.GetFiles("*.txt")
                For nFle = 0 To atFile.Length - 1
                    'Dim tDateLogExp As String = Right(atFile(nFle).Name.Split(".")(0), 8)
                    Dim tDateLogExp As String = Format(atFile(nFle).LastWriteTime, "yyyyMMdd")
                    If Not IsNumeric(tDateLogExp) Then Continue For
                    Dim nDateLogExp As Integer = Convert.ToInt32(tDateLogExp)
                    nDateLogExp = nDateLogExp + Convert.ToInt32(tPrgDay)
                    If nDateLogExp < nDatePrg Then IO.File.Delete(atFile(nFle).FullName) 'Delete
                Next
            End If

            ''Delete LogUpload
            'Dim tPathLogUpl As String = AdaConfig.cConfig.tAppPath & "\LogUpload"
            'Console.WriteLine("Folder: " & tPathLogUpl)
            'cLog.C_CALxWriteLogAuto("Folder: " & tPathLogUpl)
            'If IO.Directory.Exists(tPathLogUpl) Then
            '    Dim odiUpl As New System.IO.DirectoryInfo(tPathLogUpl)
            '    Dim atFile = odiUpl.GetFiles("*.txt")
            '    For nFle = 0 To atFile.Length - 1
            '        'Dim tDateLogUpl As String = Left(Right(atFile(nFle).Name.Split(".")(0), 14), 8)
            '        Dim tDateLogUpl As String = Format(atFile(nFle).LastWriteTime, "yyyyMMdd")
            '        If Not IsNumeric(tDateLogUpl) Then Continue For
            '        Dim nDateLogUpl As Integer = Convert.ToInt32(tDateLogUpl)
            '        nDateLogUpl = nDateLogUpl + Convert.ToInt32(tPrgDay)
            '        If nDateLogUpl < nDatePrg Then IO.File.Delete(atFile(nFle).FullName) 'Delete
            '    Next
            'End If

            'Delete Log
            Dim tPathLog As String = AdaConfig.cConfig.oConfigXml.tLog 'AdaConfig.cConfig.tAppPath & "\Log"
            'Console.WriteLine("Folder: " & tPathLog)
            cLog.C_CALxWriteLogAuto("Folder: " & tPathLog)
            If IO.Directory.Exists(tPathLog) Then
                Dim odiLog As New System.IO.DirectoryInfo(tPathLog)
                Dim atFile = odiLog.GetFiles("*.txt")
                For nFle = 0 To atFile.Length - 1
                    'Dim tDateLog As String = Left(Right(atFile(nFle).Name.Split(".")(0), 14), 8)
                    Dim tDateLog As String = Format(atFile(nFle).LastWriteTime, "yyyyMMdd")
                    If Not IsNumeric(tDateLog) Then Continue For
                    Dim nDateLog As Integer = Convert.ToInt32(tDateLog)
                    nDateLog = nDateLog + Convert.ToInt32(tPrgDay)
                    If nDateLog < nDatePrg Then IO.File.Delete(atFile(nFle).FullName) 'Delete
                Next
            End If

        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
        End Try
    End Sub
End Class
