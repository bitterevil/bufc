﻿Imports System.Text
Public Class cExportAuto

    Public Sub C_CALxProcess()
        ''Get parameter Previous date export
        'Dim nPrevious As Integer = 0
        'Dim oDbTbl As New DataTable
        'Dim oDatabase As New cDatabaseLocal
        'Dim dDateNow As Date = Date.Now
        'Dim oItemExport = cExportTemplate.C_GETaItemExport("", Now.ToString("yyyy-MM-dd"))
        ''oDbTbl = oDatabase.C_CALoExecuteReader("SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END AS ExpAuto FROM TLNKMapping WHERE FTLNMCode='EXPORTAUTO001'")
        ''If oDbTbl IsNot Nothing AndAlso oDbTbl.Rows.Count > 0 Then
        ''    nPrevious = Convert.ToInt32(oDbTbl.Rows(0)("ExpAuto"))
        ''End If
        ''dDateNow = dDateNow.AddDays(-nPrevious)
        'For Each oItem In oItemExport
        '    With oItem
        '        .nSelect = 1
        '        'Add Default Condition
        '        .nMode = 0 'Branch 0:All,1:Interval,2:Select
        '        .tDateFrom = Format(dDateNow, "yyyy-MM-dd")
        '        .tDateTo = Format(dDateNow, "yyyy-MM-dd")
        '        .tConditon = cExportTemplate.C_GETtFmtCondition(Format(dDateNow, "dd/MM/yyyy"), Format(dDateNow, "dd/MM/yyyy"))

        '        'Console.WriteLine(.tItem)
        '        cLog.C_CALxWriteLogAuto(.tItem)
        '    End With
        'Next
        'cExportTemplate.oC_Progress = New ProgressBar
        'cExportTemplate.C_CALxProcessAll()
        Try
            C_CALxFindTable()
            Dim oExp As New wExports2
            oExp.W_SETxGridExport()
            oExp.W_SETxListExport()
            oExp.W_SETxProcessExp()


        Catch ex As Exception

        End Try
    End Sub

    Public Sub C_CALxFindTable()
        Try
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal

            oSQL.Clear()
            oSQL.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine("WHERE object_id = OBJECT_ID(N'TCNTPdtRunning') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TCNTPdtRunning]")
            oSQL.AppendLine("([FNNum] [int] IDENTITY(1,1) NOT NULL,")
            oSQL.AppendLine("[FTRunningNumber] [varchar](50) NULL")
            oSQL.AppendLine(")")
            oSQL.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)


        Catch ex As Exception

        End Try
    End Sub

End Class
