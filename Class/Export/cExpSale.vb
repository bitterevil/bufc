﻿Imports System.IO
Imports System.Text

Public Class cExpSale

    Private oC_TblSaleDT As New DataTable ' สำหรับสร้าง Tbl Temp  SaleDT
    Private tC_PmtRmk As String = ""
    Public Shared tC_SaleDate As String
    ''' <summary>
    ''' เตรียมข้อมูล Sale Dt เข้า Datatable Tmpe
    ''' </summary>
    ''' <param name="ptSaleDate">เงื่อนไขวัน</param>
    ''' <returns></returns>
    Public Function C_SETbExpSale(ByVal ptSaleDate As String) As Boolean
        Dim oTblDocNo As New DataTable
        Dim oSql As New StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim bResult As Boolean = True

        ' ตัวแปรตาราง Tbl ขาย
        Dim oTblSalePdt As New DataTable  ' สำหรับ Get ข้อมูล Type ขาย FTShdDocType=1
        Dim oTblSalenoPmtPdt As New DataTable  ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้าไม่ติด promotion
        Dim oTblSalenoPmtPunCode As New DataTable  ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้าไม่ติด promotion แยกตามประเภทหน่วยสินค้า
        Dim oTblSalenoPmtVatable As New DataTable ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้าไม่ติด promotion vatable
        Dim oTblSalenoPmtVat As New DataTable ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้าไม่ติด promotion Vat
        Dim oTblSalePmtPdt As New DataTable  ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้ามี promotion
        Dim oTblSalePmtPdtPmt As New DataTable  ' สำหรับ  Check ข้อมูล Type ขาย แบบสินค้ามี promotion
        Dim oTblSalePmtPunCode As New DataTable  ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้ามี promotion แยกตามประเภทหน่วยสินค้า
        Dim oTblSalePmtVatable As New DataTable ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้ามี promotion vatable
        Dim oTblSalePmtVat As New DataTable ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้ามี promotion Vat

        ' ตัวแปรตาราง Tbl คืน
        Dim oTblRtnPdt As New DataTable  ' สำหรับ Get ข้อมูล Type คืน FTShdDocType=9
        Dim oTblRtnnoPmtPdt As New DataTable  ' สำหรับ Get ข้อมูล Type คืน แบบสินค้าไม่ติด promotion
        Dim oTblRtnnoPmtPunCode As New DataTable  ' สำหรับ Get ข้อมูล Type คืน แบบสินค้าไม่ติด promotion แยกตามประเภทหน่วยสินค้า
        Dim oTblRtnnoPmtVatable As New DataTable ' สำหรับ Get ข้อมูล Type คืน แบบสินค้าไม่ติด promotion vatable
        Dim oTblRtnnoPmtVat As New DataTable ' สำหรับ Get ข้อมูล Type คืน แบบสินค้าไม่ติด promotion Vat
        Dim oTblRtnPmtPdt As New DataTable  ' สำหรับ Get ข้อมูล Type คืน แบบสินค้ามี promotion
        Dim oTblRtnPmtPunCode As New DataTable  ' สำหรับ Get ข้อมูล Type คืน แบบสินค้ามี promotion แยกตามประเภทหน่วยสินค้า
        Dim oTblRtnPmtPunCodePmt As New DataTable
        Dim oTblRtnPmtVatable As New DataTable ' สำหรับ Get ข้อมูล Type คืน แบบสินค้ามี promotion vatable
        Dim oTblRtnPmtVat As New DataTable ' สำหรับ Get ข้อมูล Type คืน แบบสินค้ามี promotion Vat
        Dim nCountSalePdt As Integer = 0
        'ค่าที่ User setting มา
        Dim tCompanyCode As String = AdaConfig.cConfig.oUrsDefault.tCompCode

        Try
            'Get Temp Tbl DT เพื่อรับข้อมูลมาเขียน Text File
            oC_TblSaleDT = C_SEToColTblSaleDT()

            '//----------------------------------Start Get loop Pdt DT loop ขาย-----------------------------//
            oSql.Clear()
            oSql.AppendLine("SELECT FTPdtCode FROM TPSTSalDT ")
            oSql.AppendLine("WHERE FTShdDocType='1' AND ")
            oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
            oSql.AppendLine("GROUP BY FTPdtCode")
            oTblSalePdt = oDatabase.C_CALoExecuteReader(oSql.ToString)

            '//---------------------------End Get Pdt Code DT ขาย แบบไม่ติด Pmt-----------------//
            If oTblSalePdt.Rows.Count > 0 Then
                For nRowSalePdt As Integer = 0 To oTblSalePdt.Rows.Count - 1

                    oSql.Clear()
                    oSql.AppendLine("SELECT FTPdtCode FROM TPSTSalDT ")
                    oSql.AppendLine("WHERE FTShdDocType='1' AND ")
                    oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                    oSql.AppendLine("ISNULL(FTPmhCode,'')='' ")
                    oSql.AppendLine("AND FTPdtCode='" + oTblSalePdt.Rows(nRowSalePdt)(0).ToString() + "' ")
                    oSql.AppendLine("GROUP BY FTPdtCode")
                    oTblSalenoPmtPdt = oDatabase.C_CALoExecuteReader(oSql.ToString)

                    If oTblSalenoPmtPdt.Rows.Count > 0 Then
                        nCountSalePdt = 1
                        ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้าไม่ติด promotion แยกตามประเภทหน่วยสินค้า
                        For nSalenoPmtPdt As Integer = 0 To oTblSalenoPmtPdt.Rows.Count - 1
                            oSql.Clear()
                            oSql.AppendLine("SELECT FTPunCode FROM TPSTSalDT ")
                            oSql.AppendLine("WHERE FTShdDocType='1' AND ")
                            oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                            oSql.AppendLine("ISNULL(FTPmhCode,'')='' AND FTPdtCode='" + oTblSalenoPmtPdt.Rows(nSalenoPmtPdt)(0).ToString() + "' ")
                            oSql.AppendLine("GROUP BY FTPunCode")
                            oTblSalenoPmtPunCode = oDatabase.C_CALoExecuteReader(oSql.ToString)
                            If oTblSalenoPmtPunCode.Rows.Count > 0 Then
                                For nRowSalenoPmtPunCode As Integer = 0 To oTblSalenoPmtPunCode.Rows.Count - 1
                                    'ทำ Vatable ราคาสินค้ายังไม่รวม Vat 
                                    oSql.Clear()
                                    oSql.AppendLine("DECLARE @tFTPdtCode AS VARCHAR(20) ")
                                    oSql.AppendLine("DECLARE @tFTPunCode AS VARCHAR(5) ")
                                    oSql.AppendLine("SET @tFTPdtCode ='" + oTblSalenoPmtPdt.Rows(nSalenoPmtPdt)(0).ToString() + "'")
                                    oSql.AppendLine("SET @tFTPunCode ='" + oTblSalenoPmtPunCode.Rows(nRowSalenoPmtPunCode)(0).ToString() + "'")
                                    'คำนวณ หัก Vat ตาม Table TCNTMComp
                                    oSql.AppendLine(" SELECT ((Discount - (Discount * TVAT) / (100 + TVAT))) AS 'Discount amount',")
                                    'รวม Sale Amout กับ Discount ที่แยก Vat แล้ว 
                                    oSql.AppendLine(" SaleAm + ((Discount - (Discount * TVAT) / (100 + TVAT)))  AS 'Sales amount' , ")
                                    oSql.AppendLine("   CONVERT(decimal(18,2), SaleAm - ((SaleAm*TVAT) / (100 + TVAT))) AS 'SaleAmt ExVat' ,")
                                    oSql.AppendLine(" * FROM ")
                                    oSql.AppendLine(" (SELECT '01' AS 'Record_Id',CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date")
                                    oSql.AppendLine(" ,'" & tCompanyCode & "' AS 'Company Code',")
                                    oSql.AppendLine(" 'PROD' AS 'Transaction Type',")
                                    oSql.AppendLine(" '' AS 'Proposition Code'")
                                    oSql.AppendLine(" ,FTPdtCode AS 'Sale Code',")
                                    oSql.AppendLine(" SUM(ISNULL(FCSdtQtyAll,0)) AS 'Sales quantity'")
                                    oSql.AppendLine(" ,FTPunCode AS 'Sales quantity UOM',")
                                    oSql.AppendLine(" SUM(CONVERT(decimal(18,2),ISNULL(FCSdtB4DisChg,0))) AS SaleAm  ")
                                    oSql.AppendLine(" ,SUM(ISNULL(FCSdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)) AS Discount")
                                    oSql.AppendLine(" ,'' AS 'Text1',")
                                    oSql.AppendLine("  '' AS 'Text2',")
                                    oSql.AppendLine("  '' AS 'Text3',")
                                    oSql.AppendLine("  '' AS 'Amount01',")
                                    oSql.AppendLine("  '' AS 'Amount02',")
                                    oSql.AppendLine("  '' AS 'Amount03',")
                                    oSql.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT")
                                    oSql.AppendLine(" FROM TPSTSalDT ")
                                    oSql.AppendLine("WHERE 1=1 ")
                                    oSql.AppendLine("AND FTShdDocType='1' ")
                                    oSql.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                                    oSql.AppendLine("AND ISNULL(FTPmhCode,'')='' ")
                                    oSql.AppendLine("AND FTPdtCode=@tFTPdtCode ")
                                    oSql.AppendLine("AND FTPunCode=@tFTPunCode ")
                                    oSql.AppendLine("GROUP BY FTPdtCode,FDShdDocDate,FTPunCode)  AS TPS ")

                                    oTblSalenoPmtVatable = oDatabase.C_CALoExecuteReader(oSql.ToString)

                                    ' Insert Row Vatable เข้า Datatable Tmpe  DT
                                    If oTblSalenoPmtVatable.Rows.Count > 0 Then
                                        For nRowSalenoPmtVatable As Integer = 0 To oTblSalenoPmtVatable.Rows.Count - 1
                                            Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()
                                            oRowDT("Record_Id") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Record_Id").ToString()
                                            oRowDT("Date") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Date").ToString()
                                            oRowDT("Company Code") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Company Code").ToString()
                                            oRowDT("Transaction Type") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Transaction Type").ToString()
                                            oRowDT("Proposition Code") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Proposition Code").ToString()
                                            oRowDT("Sale Code") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Sale Code").ToString()
                                            oRowDT("Sales quantity") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Sales quantity").ToString()
                                            oRowDT("Sales quantity UOM") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Sales quantity UOM").ToString()
                                            oRowDT("Sales amount") = C_SETtNumDigit(oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("SaleAmt ExVat"))
                                            ' oRowDT("Sales amount") = C_SETtNumDigit(oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("SaleAm").ToString())
                                            '=== ของเดิม ใช้แบบคำนวณ Vat
                                            'oRowDT("Sales amount") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Sales amount").ToString()
                                            oRowDT("Discount amount") = C_SETtNumDigit(oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Discount amount").ToString())
                                            oRowDT("Text1") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Text1").ToString()
                                            oRowDT("Text2") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Text2").ToString()
                                            oRowDT("Text3") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Text3").ToString()
                                            oRowDT("Amount01") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Amount01").ToString()
                                            oRowDT("Amount02") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Amount02").ToString()
                                            oRowDT("Amount03") = oTblSalenoPmtVatable.Rows(nRowSalenoPmtVatable)("Amount03").ToString()
                                            oC_TblSaleDT.Rows.Add(oRowDT)
                                        Next
                                    End If

                                Next
                            End If
                        Next

                    End If

                Next
            End If

            '//---------------------------End Get Pdt Code DT ขาย แบบไม่ติด Pmt-----------------//


            '//---------------------------Start Get Pdt Code DT ขาย แบบมี Pmt------------------- //




            If oTblSalePdt.Rows.Count > 0 Then

                For nRowSalePdt As Integer = 0 To oTblSalePdt.Rows.Count - 1

                    ''แยก รายการสินค้าเพื่อ Check Promotions
                    'oSql.Clear()
                    'oSql.AppendLine(" SELECT FTPdtCode,FTShdDocNo,FNSdtSeqNo FROM TPSTSalDT  ")
                    'oSql.AppendLine(" WHERE FTShdDocType='1' AND  ")
                    'oSql.AppendLine(" CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND ")
                    'oSql.AppendLine(" ISNULL(FTPmhCode,'') != '' ")
                    'oSql.AppendLine("AND FTPdtCode='" + oTblSalePdt.Rows(nRowSalePdt)(0).ToString() + "' ")
                    'oTblSalePmtPdtCheck = oDatabase.C_CALoExecuteReader(oSql.ToString)

                    'Group รายการสินค้าที่มี Promotion ตามรหัส สินค้า
                    oSql.Clear()
                    oSql.AppendLine("SELECT FTPdtCode FROM TPSTSalDT ")
                    oSql.AppendLine("WHERE FTShdDocType='1' AND ")
                    oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                    oSql.AppendLine("ISNULL(FTPmhCode,'') != '' ")
                    oSql.AppendLine("AND FTPdtCode='" + oTblSalePdt.Rows(nRowSalePdt)(0).ToString() + "' ")
                    oSql.AppendLine("GROUP BY FTPdtCode")
                    oTblSalePmtPdt = oDatabase.C_CALoExecuteReader(oSql.ToString)

                    If oTblSalePmtPdt.Rows.Count > 0 Then
                        ' สำหรับ Get ข้อมูล Type ขาย แบบสินค้ามี promotion แยกตามประเภทหน่วยสินค้า
                        For nSalePmtPdt As Integer = 0 To oTblSalePmtPdt.Rows.Count - 1
                            'Get รหัส Promotions 
                            oSql.Clear()
                            oSql.AppendLine("SELECT FTPunCode FROM TPSTSalDT ")
                            oSql.AppendLine("WHERE FTShdDocType='1' AND ")
                            oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                            oSql.AppendLine("ISNULL(FTPmhCode,'')!='' AND FTPdtCode='" + oTblSalePmtPdt.Rows(nSalePmtPdt)(0).ToString() + "' ")
                            oSql.AppendLine("GROUP BY FTPunCode ")
                            oTblSalePmtPunCode = oDatabase.C_CALoExecuteReader(oSql.ToString)

                            If oTblSalePmtPunCode.Rows.Count > 0 Then

                                For nRowSalePmtPunCode As Integer = 0 To oTblSalePmtPunCode.Rows.Count - 1
                                    oSql.Clear()
                                    oSql.AppendLine(" DECLARE @tFTPdtCode AS VARCHAR(20) ")
                                    oSql.AppendLine(" DECLARE @tFTPunCode AS VARCHAR(5) ")
                                    oSql.AppendLine(" SET @tFTPdtCode ='" + oTblSalePmtPdt.Rows(nSalePmtPdt)(0).ToString() + "'")
                                    oSql.AppendLine(" SET @tFTPunCode ='" + oTblSalePmtPunCode.Rows(nRowSalePmtPunCode)(0).ToString() + "'")
                                    oSql.AppendLine(" SELECT ((Discount - (Discount * TVAT) / (100 + TVAT))) AS 'Discount amount',")
                                    oSql.AppendLine(" SaleAm + ((Discount - (Discount * TVAT) / (100 + TVAT)))  AS 'Sales amount', ")
                                    oSql.AppendLine(" CONVERT(decimal(18,2), SaleAm - ((SaleAm*TVAT) / (100 + TVAT))) AS 'SaleAmt ExVat' ,")
                                    oSql.AppendLine(" * FROM (SELECT  '01' AS 'Record_Id'")
                                    oSql.AppendLine(" ,CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date")
                                    oSql.AppendLine(" ,'" & tCompanyCode & "' AS 'Company Code'")
                                    oSql.AppendLine(" ,'PROD' AS 'Transaction Type'")
                                    oSql.AppendLine(" ,Pmt.FTPmhRmk AS 'Proposition Code'")
                                    oSql.AppendLine(" ,FTPdtCode AS 'Sale Code'")
                                    oSql.AppendLine(" ,SUM(FCSdtQtyAll) AS 'Sales quantity'")
                                    oSql.AppendLine(" ,FTPunCode AS 'Sales quantity UOM'")
                                    oSql.AppendLine(" ,  CONVERT(decimal(18,2),SUM(ISNULL(FCSdtB4DisChg,0))) AS SaleAm  ")
                                    oSql.AppendLine(" ,SUM(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg) AS  Discount")
                                    oSql.AppendLine(" ,'' AS 'Text1' ")
                                    oSql.AppendLine(" ,'' AS 'Text2'")
                                    oSql.AppendLine(" ,'' AS 'Text3'")
                                    oSql.AppendLine(" ,'' AS 'Amount01'")
                                    oSql.AppendLine(" ,'' AS 'Amount02'")
                                    oSql.AppendLine(" ,'' AS 'Amount03',")
                                    oSql.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT")
                                    oSql.AppendLine("  FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode) ")
                                    oSql.AppendLine(" WHERE 1=1 ")
                                    oSql.AppendLine(" AND FTShdDocType='1' ")
                                    oSql.AppendLine(" AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                                    oSql.AppendLine(" AND ISNULL(DT.FTPmhCode,'') != '' ")
                                    oSql.AppendLine(" AND FTPdtCode=@tFTPdtCode ")
                                    oSql.AppendLine(" AND FTPunCode=@tFTPunCode")
                                    oSql.AppendLine(" GROUP BY FTPdtCode,FDShdDocDate,FTPunCode,FTPmhRmk) AS TPS")

                                    'ทำ Vatable ราคาสินค้ายังไม่รวม Vat 
                                    'oSql.Clear()
                                    'oSql.AppendLine("DECLARE @tFTPdtCode AS VARCHAR(20) ")
                                    'oSql.AppendLine("DECLARE @tFTPunCode AS VARCHAR(5) ")
                                    'oSql.AppendLine("SET @tFTPdtCode ='" + oTblSalePmtPdt.Rows(nSalePmtPdt)(0).ToString() + "'")
                                    'oSql.AppendLine("SET @tFTPunCode ='" + oTblSalePmtPunCode.Rows(nRowSalePmtPunCode)(0).ToString() + "'")
                                    'oSql.AppendLine("SELECT ")
                                    'oSql.AppendLine("'01' AS 'Record_Id'")
                                    'oSql.AppendLine(",CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date")
                                    'oSql.AppendLine(",'1090' AS 'Company Code'")
                                    'oSql.AppendLine(",'PROD' AS 'Transaction Type'")
                                    'oSql.AppendLine(",Pmt.FTPmhRmk AS 'Proposition Code'")
                                    'oSql.AppendLine(",FTPdtCode AS 'Sale Code'")
                                    'oSql.AppendLine(",SUM(FCSdtQtyAll) AS 'Sales quantity'")
                                    'oSql.AppendLine(",FTPunCode AS 'Sales quantity UOM'")
                                    'oSql.AppendLine(",SUM(FCSdtVatable) AS 'Sales amount'")
                                    'oSql.AppendLine(",SUM(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg) AS 'Discount amount'")
                                    'oSql.AppendLine(",'' AS 'Text1'")
                                    'oSql.AppendLine(",'' AS 'Text2'")
                                    'oSql.AppendLine(",'' AS 'Text3'")
                                    'oSql.AppendLine(",'' AS 'Amount01'")
                                    'oSql.AppendLine(",'' AS 'Amount02'")
                                    'oSql.AppendLine(",'' AS 'Amount03'")
                                    'oSql.AppendLine(" FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode) ")
                                    'oSql.AppendLine("WHERE 1=1 ")
                                    'oSql.AppendLine("AND FTShdDocType='1' ")
                                    'oSql.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                                    'oSql.AppendLine("AND ISNULL(DT.FTPmhCode,'') != '' ")
                                    'oSql.AppendLine("AND FTPdtCode=@tFTPdtCode ")
                                    'oSql.AppendLine("AND FTPunCode=@tFTPunCode ")
                                    'oSql.AppendLine("GROUP BY FTPdtCode,FDShdDocDate,FTPunCode,FTPmhRmk")
                                    oTblSalePmtVatable = oDatabase.C_CALoExecuteReader(oSql.ToString)

                                    '   Dim tDocPmt As String = oTblSalePmtPdt.Rows(nRowSalePmtPunCode)("FTShdDocNo")
                                    '  Dim tSqcPmt As String = oTblSalePmtPdt.Rows(nRowSalePmtPunCode)("FNSdtSeqNo")

                                    'Dim tDocPmt As String = oTblSalePmtPdtCheck.Rows(nRowSalePmtPunCode)("FTShdDocNo")
                                    'Dim tSqcPmt As String = oTblSalePmtPdtCheck.Rows(nRowSalePmtPunCode)("FNSdtSeqNo")

                                    ' Insert Row Vatable เข้า Datatable Tmpe  DT
                                    If oTblSalePmtVatable.Rows.Count > 0 Then
                                        For nRowSalePmtVatable As Integer = 0 To oTblSalePmtVatable.Rows.Count - 1
                                            Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()

                                            oRowDT("Record_Id") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Record_Id").ToString()
                                            oRowDT("Date") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Date").ToString()
                                            oRowDT("Company Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Company Code").ToString()
                                            oRowDT("Transaction Type") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Transaction Type").ToString()
                                            oRowDT("Sale Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sale Code").ToString()
                                            oRowDT("Sales quantity") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales quantity").ToString()
                                            oRowDT("Sales quantity UOM") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales quantity UOM").ToString()
                                            ' oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales amount"))
                                            '  oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("SaleAm"))
                                            oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("SaleAmt Exvat"))
                                            oRowDT("Discount amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Discount amount"))
                                            oRowDT("Text1") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text1").ToString()
                                            oRowDT("Text2") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text2").ToString()
                                            oRowDT("Text3") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text3").ToString()
                                            oRowDT("Amount01") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount01").ToString()
                                            oRowDT("Amount02") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount02").ToString()
                                            oRowDT("Amount03") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount03").ToString()

                                            'If C_GETxPromotion(tDocPmt, tSqcPmt) Then
                                            '    oRowDT("Proposition Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Proposition Code").ToString()
                                            'Else
                                            oRowDT("Proposition Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Proposition Code").ToString()
                                            'End If

                                            oC_TblSaleDT.Rows.Add(oRowDT)


                                            'oRowDT("Record_Id") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Record_Id").ToString()
                                            'oRowDT("Date") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Date").ToString()
                                            'oRowDT("Company Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Company Code").ToString()
                                            'oRowDT("Transaction Type") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Transaction Type").ToString()
                                            'If C_GETxPromotion(tDocPmt, tSqcPmt) Then
                                            '    oRowDT("Proposition Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Proposition Code").ToString()
                                            'Else
                                            '    oRowDT("Proposition Code") = ""
                                            'End If
                                            'oRowDT("Sale Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sale Code").ToString()
                                            '    oRowDT("Sales quantity") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales quantity").ToString()
                                            '    oRowDT("Sales quantity UOM") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales quantity UOM").ToString()
                                            '    ' oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales amount"))
                                            '    oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("SaleAm"))
                                            '    oRowDT("Discount amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Discount amount"))
                                            '    oRowDT("Text1") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text1").ToString()
                                            '    oRowDT("Text2") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text2").ToString()
                                            '    oRowDT("Text3") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text3").ToString()
                                            '    oRowDT("Amount01") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount01").ToString()
                                            '    oRowDT("Amount02") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount02").ToString()
                                            '    oRowDT("Amount03") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount03").ToString()
                                            '    oC_TblSaleDT.Rows.Add(oRowDT)
                                        Next

                                        'แยก รายการสินค้าเพื่อ Check Promotions
                                        ' oSql.Clear()
                                        'oSql.AppendLine(" SELECT FTPdtCode,FTShdDocNo,FNSdtSeqNo FROM TPSTSalDT  ")
                                        'oSql.AppendLine(" WHERE FTShdDocType='1' AND  ")
                                        'oSql.AppendLine(" CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND ")
                                        'oSql.AppendLine(" ISNULL(FTPmhCode,'') != '' ")
                                        'oSql.AppendLine("AND FTPdtCode='" + oTblSalePdt.Rows(nRowSalePdt)(0).ToString() + "' ")
                                        'oTblSalePmtPdtPmt = oDatabase.C_CALoExecuteReader(oSql.ToString)

                                        'If oTblSalePmtPdtPmt.Rows.Count > 0 Then

                                        '    For nRow As Integer = 0 To oTblSalePmtPdtPmt.Rows.Count - 1
                                        '        If C_GETxPromotion(oTblSalePmtPdtPmt.Rows(nRow)("FTShdDocNo"), oTblSalePmtPdtPmt.Rows(nRow)("FNSdtSeqNo")) Then
                                        '            For nRowSalePmtVatable As Integer = 0 To oTblSalePmtVatable.Rows.Count - 1
                                        '                Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()

                                        '                oRowDT("Record_Id") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Record_Id").ToString()
                                        '                oRowDT("Date") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Date").ToString()
                                        '                oRowDT("Company Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Company Code").ToString()
                                        '                oRowDT("Transaction Type") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Transaction Type").ToString()
                                        '                oRowDT("Sale Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sale Code").ToString()
                                        '                oRowDT("Sales quantity") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales quantity").ToString()
                                        '                oRowDT("Sales quantity UOM") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales quantity UOM").ToString()
                                        '                ' oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Sales amount"))
                                        '                oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("SaleAm"))
                                        '                oRowDT("Discount amount") = C_SETtNumDigit(oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Discount amount"))
                                        '                oRowDT("Text1") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text1").ToString()
                                        '                oRowDT("Text2") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text2").ToString()
                                        '                oRowDT("Text3") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Text3").ToString()
                                        '                oRowDT("Amount01") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount01").ToString()
                                        '                oRowDT("Amount02") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount02").ToString()
                                        '                oRowDT("Amount03") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Amount03").ToString()
                                        '                oRowDT("Proposition Code") = oTblSalePmtVatable.Rows(nRowSalePmtVatable)("Proposition Code").ToString()
                                        '                oC_TblSaleDT.Rows.Add(oRowDT)
                                        '            Next

                                        '        End If

                                        '    Next

                                        'End If





                                    End If
                                Next
                            End If
                        Next
                    End If
                Next

                If oTblSalePdt.Rows.Count > 0 Then
                    'ทำแถว vat 
                    'oSql.Clear()
                    'oSql.AppendLine("SELECT ")
                    'oSql.AppendLine("'01' AS 'Record_Id'")
                    'oSql.AppendLine(",CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date")
                    'oSql.AppendLine(",'1090' AS 'Company Code'")
                    'oSql.AppendLine(",'VPRO' AS 'Transaction Type'")
                    'oSql.AppendLine(",'' AS 'Proposition Code'")
                    'oSql.AppendLine(",'' AS 'Sale Code'")
                    'oSql.AppendLine(",'' AS 'Sales quantity'")
                    'oSql.AppendLine(",'' AS 'Sales quantity UOM'")
                    'oSql.AppendLine(",SUM(ISNULL(FCSdtVat,0))AS 'Sales amount'")
                    'oSql.AppendLine(",0.00 AS 'Discount amount'")
                    'oSql.AppendLine(",'' AS 'Text1'")
                    'oSql.AppendLine(",'' AS 'Text2'")
                    'oSql.AppendLine(",'' AS 'Text3'")
                    'oSql.AppendLine(",'' AS 'Amount01'")
                    'oSql.AppendLine(",'' AS 'Amount02'")
                    'oSql.AppendLine(",'' AS 'Amount03'")
                    'oSql.AppendLine(" FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode)")
                    'oSql.AppendLine("WHERE 1=1 ")
                    'oSql.AppendLine("AND FTShdDocType='1' ")
                    'oSql.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                    'oSql.AppendLine("GROUP BY FDShdDocDate")
                    oSql.Clear()
                    oSql.AppendLine(" SELECT ((SAmout-(sAmout* TVAT) / (100 + TVAT))) AS 'Sales amount' ,* FROM")
                    oSql.AppendLine(" (SELECT  ")
                    oSql.AppendLine(" '01' AS 'Record_Id' ")
                    oSql.AppendLine(" ,CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date ")
                    oSql.AppendLine(" ,'" & tCompanyCode & "' AS 'Company Code' ")
                    oSql.AppendLine(" ,'VPRD' AS 'Transaction Type' ")
                    oSql.AppendLine(" ,'' AS 'Proposition Code' ")
                    oSql.AppendLine(" ,'' AS 'Sale Code' ")
                    oSql.AppendLine(" ,'' AS 'Sales quantity' ")
                    oSql.AppendLine(" ,'' AS 'Sales quantity UOM' ")
                    oSql.AppendLine(" ,SUM(ISNULL(FCSdtVat,0)) AS SAmout ")
                    oSql.AppendLine(" ,0.00 AS 'Discount amount' ")
                    oSql.AppendLine(" ,'' AS 'Text1' ")
                    oSql.AppendLine(" ,'' AS 'Text2' ")
                    oSql.AppendLine(" ,'' AS 'Text3' ")
                    oSql.AppendLine(" ,'' AS 'Amount01' ")
                    oSql.AppendLine(" ,'' AS 'Amount02' ")
                    oSql.AppendLine(" ,'' AS 'Amount03' ")
                    oSql.AppendLine(" ,(SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT ")
                    oSql.AppendLine("  FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode) ")
                    oSql.AppendLine(" WHERE 1=1  AND FTShdDocType='1'  ")
                    oSql.AppendLine(" AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "'  ")
                    oSql.AppendLine(" GROUP BY FDShdDocDate) AS TBV ")
                    oTblSalePmtVat = oDatabase.C_CALoExecuteReader(oSql.ToString)

                    If oTblSalePmtVat.Rows.Count > 0 Then
                        For nRowSalePmtVat As Integer = 0 To oTblSalePmtVat.Rows.Count - 1
                            Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()
                            oRowDT("Record_Id") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Record_Id").ToString()
                            oRowDT("Date") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Date").ToString()
                            oRowDT("Company Code") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Company Code").ToString()
                            oRowDT("Transaction Type") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Transaction Type").ToString()
                            oRowDT("Proposition Code") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Proposition Code").ToString()
                            oRowDT("Sale Code") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Sale Code").ToString()
                            oRowDT("Sales quantity") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Sales quantity").ToString()
                            oRowDT("Sales quantity UOM") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Sales quantity UOM").ToString()
                            ' oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVat.Rows(nRowSalePmtVat)("Sales amount").ToString())
                            oRowDT("Sales amount") = C_SETtNumDigit(oTblSalePmtVat.Rows(nRowSalePmtVat)("SAmout"))
                            oRowDT("Discount amount") = C_SETtNumDigit(oTblSalePmtVat.Rows(nRowSalePmtVat)("Discount amount").ToString())
                            oRowDT("Text1") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Text1").ToString()
                            oRowDT("Text2") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Text2").ToString()
                            oRowDT("Text3") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Text3").ToString()
                            oRowDT("Amount01") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Amount01").ToString()
                            oRowDT("Amount02") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Amount02").ToString()
                            oRowDT("Amount03") = oTblSalePmtVat.Rows(nRowSalePmtVat)("Amount03").ToString()
                            oC_TblSaleDT.Rows.Add(oRowDT)
                        Next
                    End If
                End If
            End If
            '//---------------------------End Get Pdt Code DT ขาย แบบมี Pmt------------------- //



            '//----------------------------------Start Get loop Pdt DT loop คืน-----------------------------//
            '////////////////////////////////////////////////////////////////////////////////////////////////
            '//--------------------------Start Get Pdt Code DT คืน แบบไม่ติด Pmt-----------------//
            oSql.Clear()
            oSql.AppendLine("SELECT FTPdtCode FROM TPSTSalDT ")
            oSql.AppendLine("WHERE FTShdDocType='9' AND ")
            oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
            oSql.AppendLine("GROUP BY FTPdtCode")
            oTblRtnPdt = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oTblRtnPdt.Rows.Count > 0 Then
                For nRowRtnPdt As Integer = 0 To oTblRtnPdt.Rows.Count - 1
                    oSql.Clear()
                    oSql.AppendLine("SELECT FTPdtCode FROM TPSTSalDT ")
                    oSql.AppendLine("WHERE FTShdDocType='9' AND ")
                    oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                    oSql.AppendLine("ISNULL(FTPmhCode,'') ='' ")
                    oSql.AppendLine("AND FTPdtCode='" + oTblRtnPdt.Rows(nRowRtnPdt)(0).ToString() + "' ")
                    oSql.AppendLine("GROUP BY FTPdtCode")
                    oTblRtnnoPmtPdt = oDatabase.C_CALoExecuteReader(oSql.ToString)

                    If oTblRtnnoPmtPdt.Rows.Count > 0 Then
                        ' สำหรับ Get ข้อมูล Type คืน แบบสินค้าไม่ติด promotion แยกตามประเภทหน่วยสินค้า
                        For nRtnnoPmtPdt As Integer = 0 To oTblRtnnoPmtPdt.Rows.Count - 1
                            oSql.Clear()
                            oSql.AppendLine("SELECT FTPunCode FROM TPSTSalDT ")
                            oSql.AppendLine("WHERE FTShdDocType='9' AND ")
                            oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                            oSql.AppendLine("ISNULL(FTPmhCode,'') ='' AND FTPdtCode='" + oTblRtnnoPmtPdt.Rows(nRtnnoPmtPdt)(0).ToString() + "' ")
                            oSql.AppendLine("GROUP BY FTPunCode")
                            oTblRtnnoPmtPunCode = oDatabase.C_CALoExecuteReader(oSql.ToString)
                            If oTblRtnnoPmtPunCode.Rows.Count > 0 Then
                                For nRowRtnnoPmtPunCode As Integer = 0 To oTblRtnnoPmtPunCode.Rows.Count - 1
                                    'ทำ Vatable ราคาสินค้ายังไม่รวม Vat 
                                    'oSql.Clear()
                                    'oSql.AppendLine("DECLARE @tFTPdtCode AS VARCHAR(20) ")
                                    'oSql.AppendLine("DECLARE @tFTPunCode AS VARCHAR(5) ")
                                    'oSql.AppendLine("SET @tFTPdtCode ='" + oTblRtnnoPmtPdt.Rows(nRtnnoPmtPdt)(0).ToString() + "'")
                                    'oSql.AppendLine("SET @tFTPunCode ='" + oTblRtnnoPmtPunCode.Rows(nRowRtnnoPmtPunCode)(0).ToString() + "'")
                                    'oSql.AppendLine("SELECT ")
                                    'oSql.AppendLine("'01' AS 'Record_Id'")
                                    'oSql.AppendLine(",CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date")
                                    'oSql.AppendLine(",'" & tCompanyCode & "'  AS 'Company Code'")
                                    'oSql.AppendLine(",'CNPR' AS 'Transaction Type'")
                                    'oSql.AppendLine(",'' AS 'Proposition Code'")
                                    'oSql.AppendLine(",FTPdtCode AS 'Sale Code'")
                                    'oSql.AppendLine(",SUM(ISNULL(FCSdtQtyAll,0)) AS 'Sales quantity'")
                                    'oSql.AppendLine(",FTPunCode AS 'Sales quantity UOM'")
                                    'oSql.AppendLine(",SUM(ISNULL(FCSdtVatable,0)) AS 'Sales amount'")
                                    'oSql.AppendLine(",SUM(ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)) AS 'Discount amount'")
                                    'oSql.AppendLine(",'' AS 'Text1'")
                                    'oSql.AppendLine(",'' AS 'Text2'")
                                    'oSql.AppendLine(",'' AS 'Text3'")
                                    'oSql.AppendLine(",'' AS 'Amount01'")
                                    'oSql.AppendLine(",'' AS 'Amount02'")
                                    'oSql.AppendLine(",'' AS 'Amount03'")
                                    'oSql.AppendLine(" FROM TPSTSalDT ")
                                    'oSql.AppendLine("WHERE 1=1 ")
                                    'oSql.AppendLine("AND FTShdDocType='9' ")
                                    'oSql.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                                    'oSql.AppendLine("AND ISNULL(FTPmhCode,'')='' ")
                                    'oSql.AppendLine("AND FTPdtCode=@tFTPdtCode ")
                                    'oSql.AppendLine("AND FTPunCode=@tFTPunCode ")
                                    'oSql.AppendLine("GROUP BY FTPdtCode,FDShdDocDate,FTPunCode")

                                    'แก้ไขให้มีการ ExVat
                                    oSql.Clear()
                                    oSql.AppendLine("DECLARE @tFTPdtCode AS VARCHAR(20) ")
                                    oSql.AppendLine("DECLARE @tFTPunCode AS VARCHAR(5) ")
                                    oSql.AppendLine("SET @tFTPdtCode ='" + oTblRtnnoPmtPdt.Rows(nRtnnoPmtPdt)(0).ToString() + "'")
                                    oSql.AppendLine("SET @tFTPunCode ='" + oTblRtnnoPmtPunCode.Rows(nRowRtnnoPmtPunCode)(0).ToString() + "'")
                                    oSql.AppendLine(" SELECT ((Discount - (Discount * TVAT) / (100 + TVAT))) AS 'Discount amount', ")
                                    oSql.AppendLine("  SaleAm + ((Discount - (Discount * TVAT) / (100 + TVAT)))  AS 'Sales amount' , ")
                                    oSql.AppendLine("  CONVERT(decimal(18,2), SaleAm - ((SaleAm*TVAT) / (100 + TVAT))) AS 'SaleAmt ExVat', ")
                                    oSql.AppendLine("  * FROM  ")
                                    oSql.AppendLine(" (SELECT  ")
                                    oSql.AppendLine(" '01' AS 'Record_Id' ")
                                    oSql.AppendLine(" ,CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date ")
                                    oSql.AppendLine(" ,'" & tCompanyCode & "'  AS 'Company Code' ")
                                    oSql.AppendLine(" ,'CNPR' AS 'Transaction Type' ")
                                    oSql.AppendLine(" ,'' AS 'Proposition Code' ")
                                    oSql.AppendLine(" ,FTPdtCode AS 'Sale Code' ")
                                    oSql.AppendLine(" ,SUM(ISNULL(FCSdtQtyAll,0)) AS 'Sales quantity' ")
                                    oSql.AppendLine(" ,FTPunCode AS 'Sales quantity UOM' ")
                                    oSql.AppendLine(" , CONVERT(decimal(18,2),SUM(ISNULL(FCSdtB4DisChg,0))) AS SaleAm   ")
                                    oSql.AppendLine(" ,SUM(ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)) AS Discount, ")
                                    oSql.AppendLine("  (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT ")
                                    oSql.AppendLine(" ,'' AS 'Text1'")
                                    oSql.AppendLine(" ,'' AS 'Text2' ")
                                    oSql.AppendLine(" ,'' AS 'Text3' ")
                                    oSql.AppendLine(" ,'' AS 'Amount01' ")
                                    oSql.AppendLine(" ,'' AS 'Amount02' ")
                                    oSql.AppendLine(" ,'' AS 'Amount03'")
                                    oSql.AppendLine("  FROM TPSTSalDT  ")
                                    oSql.AppendLine("  WHERE 1=1 ")
                                    oSql.AppendLine("  AND FTShdDocType='9' ")
                                    oSql.AppendLine("  AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                                    oSql.AppendLine("  AND ISNULL(FTPmhCode,'')='' ")
                                    oSql.AppendLine("  AND FTPdtCode=@tFTPdtCode ")
                                    oSql.AppendLine("  AND FTPunCode=@tFTPunCode ")
                                    oSql.AppendLine("  GROUP BY FTPdtCode,FDShdDocDate,FTPunCode) AS TPS ")
                                    oTblRtnnoPmtVatable = oDatabase.C_CALoExecuteReader(oSql.ToString)

                                    ' Insert Row Vatable เข้า Datatable Tmpe  DT
                                    If oTblRtnnoPmtVatable.Rows.Count > 0 Then
                                        For nRowRtnnoPmtVatable As Integer = 0 To oTblRtnnoPmtVatable.Rows.Count - 1
                                            Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()
                                            oRowDT("Record_Id") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Record_Id").ToString()
                                            oRowDT("Date") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Date").ToString()
                                            oRowDT("Company Code") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Company Code").ToString()
                                            oRowDT("Transaction Type") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Transaction Type").ToString()
                                            oRowDT("Proposition Code") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Proposition Code").ToString()
                                            oRowDT("Sale Code") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Sale Code").ToString()
                                            oRowDT("Sales quantity") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Sales quantity").ToString()
                                            oRowDT("Sales quantity UOM") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Sales quantity UOM").ToString()
                                            '  oRowDT("Sales amount") = C_SETtNumDigit(oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("SaleAm").ToString())
                                            oRowDT("Sales amount") = C_SETtNumDigit(oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("SaleAmt Exvat"))
                                            oRowDT("Discount amount") = C_SETtNumDigit(oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Discount amount").ToString())
                                            oRowDT("Text1") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Text1").ToString()
                                            oRowDT("Text2") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Text2").ToString()
                                            oRowDT("Text3") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Text3").ToString()
                                            oRowDT("Amount01") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Amount01").ToString()
                                            oRowDT("Amount02") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Amount02").ToString()
                                            oRowDT("Amount03") = oTblRtnnoPmtVatable.Rows(nRowRtnnoPmtVatable)("Amount03").ToString()
                                            oC_TblSaleDT.Rows.Add(oRowDT)
                                        Next
                                    End If

                                Next
                            End If
                        Next
                    End If
                Next

            End If
            '//---------------------------End Get Pdt Code DT คืน แบบไม่ติด Pmt-----------------//

            '//---------------------------Start Get Pdt Code คืน แบบมี Pmt------------------- //
            If oTblRtnPdt.Rows.Count > 0 Then
                For nRowRtnPdt As Integer = 0 To oTblRtnPdt.Rows.Count - 1
                    oSql.Clear()
                    oSql.AppendLine("SELECT FTPdtCode FROM TPSTSalDT ")
                    oSql.AppendLine("WHERE FTShdDocType='9' AND ")
                    oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                    oSql.AppendLine("ISNULL(FTPmhCode,'') != '' ")
                    oSql.AppendLine("AND FTPdtCode='" + oTblRtnPdt.Rows(nRowRtnPdt)(0).ToString() + "' ")
                    oSql.AppendLine("GROUP BY FTPdtCode")
                    oTblRtnPmtPdt = oDatabase.C_CALoExecuteReader(oSql.ToString)

                    If oTblRtnPmtPdt.Rows.Count > 0 Then

                        For nRtnPmtPdt As Integer = 0 To oTblRtnPmtPdt.Rows.Count - 1
                            oSql.Clear()
                            oSql.AppendLine("SELECT FTPunCode FROM TPSTSalDT ")
                            oSql.AppendLine("WHERE FTShdDocType='9' AND ")
                            oSql.AppendLine("CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                            oSql.AppendLine("ISNULL(FTPmhCode,'')!='' AND FTPdtCode='" + oTblRtnPmtPdt.Rows(nRtnPmtPdt)(0).ToString() + "' ")
                            oSql.AppendLine("GROUP BY FTPunCode")
                            'oSql.Clear()
                            'oSql.AppendLine(" SELECT FTPdtCode,FTShdDocNo,FNSdtSeqNo FROM TPSTSalDT  ")
                            'oSql.AppendLine(" WHERE FTShdDocType='9' AND ")
                            'oSql.AppendLine(" CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                            'oSql.AppendLine(" ISNULL(FTPmhCode,'')!='' AND FTPdtCode='" + oTblRtnPmtPdt.Rows(nRtnPmtPdt)(0).ToString() + "' ")
                            ' oTblRtnPmtPunCodePmt = oDatabase.C_CALoExecuteReader(oSql.ToString)

                            oTblRtnPmtPunCode = oDatabase.C_CALoExecuteReader(oSql.ToString)
                            If oTblRtnPmtPunCode.Rows.Count > 0 Then
                                For nRowRtnPmtPunCode As Integer = 0 To oTblRtnPmtPunCode.Rows.Count - 1
                                    'ทำ Vatable ราคาสินค้ายังไม่รวม Vat 
                                    'oSql.Clear()
                                    'oSql.AppendLine("DECLARE @tFTPdtCode AS VARCHAR(20) ")
                                    'oSql.AppendLine("DECLARE @tFTPunCode AS VARCHAR(5) ")
                                    'oSql.AppendLine("SET @tFTPdtCode ='" + oTblRtnPmtPdt.Rows(nRtnPmtPdt)(0).ToString() + "'")
                                    'oSql.AppendLine("SET @tFTPunCode ='" + oTblRtnPmtPunCode.Rows(nRowRtnPmtPunCode)(0).ToString() + "'")
                                    'oSql.AppendLine("SELECT ")
                                    'oSql.AppendLine("'01' AS 'Record_Id'")
                                    'oSql.AppendLine(",CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date")
                                    'oSql.AppendLine(",'" & tCompanyCode & "' AS 'Company Code'")
                                    'oSql.AppendLine(",'CNPR' AS 'Transaction Type'")
                                    'oSql.AppendLine(",Pmt.FTPmhRmk AS 'Proposition Code'")
                                    'oSql.AppendLine(",FTPdtCode AS 'Sale Code'")
                                    'oSql.AppendLine(",SUM(ISNULL(FCSdtQtyAll,0)) AS 'Sales quantity'")
                                    'oSql.AppendLine(",FTPunCode AS 'Sales quantity UOM'")
                                    'oSql.AppendLine(",SUM(ISNULL(FCSdtVatable,0)) AS 'Sales amount'")
                                    'oSql.AppendLine(",SUM(ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)) AS 'Discount amount'")
                                    'oSql.AppendLine(",'' AS 'Text1'")
                                    'oSql.AppendLine(",'' AS 'Text2'")
                                    'oSql.AppendLine(",'' AS 'Text3'")
                                    'oSql.AppendLine(",'' AS 'Amount01'")
                                    'oSql.AppendLine(",'' AS 'Amount02'")
                                    'oSql.AppendLine(",'' AS 'Amount03'")
                                    'oSql.AppendLine(" FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode) ")
                                    'oSql.AppendLine("WHERE 1=1 ")
                                    'oSql.AppendLine("AND FTShdDocType='1' ")
                                    'oSql.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                                    'oSql.AppendLine("AND ISNULL(DT.FTPmhCode,'') != '' ")
                                    'oSql.AppendLine("AND FTPdtCode=@tFTPdtCode ")
                                    'oSql.AppendLine("AND FTPunCode=@tFTPunCode ")
                                    'oSql.AppendLine("GROUP BY FTPdtCode,FDShdDocDate,FTPunCode,FTPmhRmk")


                                    'แก้ไขให้ Exvat
                                    oSql.Clear()
                                    oSql.AppendLine("DECLARE @tFTPdtCode AS VARCHAR(20) ")
                                    oSql.AppendLine("DECLARE @tFTPunCode AS VARCHAR(5) ")
                                    oSql.AppendLine("SET @tFTPdtCode ='" + oTblRtnPmtPdt.Rows(nRtnPmtPdt)(0).ToString() + "'")
                                    oSql.AppendLine("SET @tFTPunCode ='" + oTblRtnPmtPunCode.Rows(nRowRtnPmtPunCode)(0).ToString() + "'")
                                    oSql.AppendLine(" SELECT   ((Discount - (Discount * TVAT) / (100 + TVAT))) AS 'Discount amount', ")
                                    oSql.AppendLine(" SaleAm + ((Discount - (Discount * TVAT) / (100 + TVAT)))  AS 'Sales amount' , ")
                                    oSql.AppendLine(" CONVERT(decimal(18,2), SaleAm - ((SaleAm*TVAT) / (100 + TVAT))) AS 'SaleAmt ExVat' ,")
                                    oSql.AppendLine(" * FROM ")
                                    oSql.AppendLine(" (SELECT  ")
                                    oSql.AppendLine(" '01' AS 'Record_Id' ")
                                    oSql.AppendLine(" ,CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date ")
                                    oSql.AppendLine(" ,'" & tCompanyCode & "' AS 'Company Code' ")
                                    oSql.AppendLine(" ,'CNPR' AS 'Transaction Type' ")
                                    oSql.AppendLine(" ,Pmt.FTPmhRmk AS 'Proposition Code' ")
                                    oSql.AppendLine(" ,FTPdtCode AS 'Sale Code' ")
                                    oSql.AppendLine(" ,SUM(ISNULL(FCSdtQtyAll,0)) AS 'Sales quantity' ")
                                    oSql.AppendLine(" ,FTPunCode AS 'Sales quantity UOM' ")
                                    oSql.AppendLine(" ,CONVERT(decimal(18,2),SUM(ISNULL(FCSdtB4DisChg,0))) AS SaleAm   ")
                                    oSql.AppendLine(" ,SUM(ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)) AS Discount, ")
                                    oSql.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT ")
                                    oSql.AppendLine(" ,'' AS 'Text1' ")
                                    oSql.AppendLine(" ,'' AS 'Text2'")
                                    oSql.AppendLine(" ,'' AS 'Text3' ")
                                    oSql.AppendLine(" ,'' AS 'Amount01' ")
                                    oSql.AppendLine(" ,'' AS 'Amount02' ")
                                    oSql.AppendLine(" ,'' AS 'Amount03' ")
                                    oSql.AppendLine("  FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode)  ")
                                    oSql.AppendLine(" WHERE 1=1  AND FTShdDocType='9'  ")
                                    oSql.AppendLine(" AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                                    oSql.AppendLine(" AND ISNULL(DT.FTPmhCode,'') != '' ")
                                    oSql.AppendLine(" AND FTPdtCode=@tFTPdtCode ")
                                    oSql.AppendLine(" AND FTPunCode=@tFTPunCode ")
                                    oSql.AppendLine(" GROUP BY FTPdtCode,FDShdDocDate,FTPunCode,FTPmhRmk) AS TPS ")

                                    oTblRtnPmtVatable = oDatabase.C_CALoExecuteReader(oSql.ToString)

                                    'Dim tDocPmt As String = oTblRtnPmtPunCode.Rows(nRowRtnPmtPunCode)("FTShdDocNo")
                                    'Dim tSeqPmt As String = oTblRtnPmtPunCode.Rows(nRowRtnPmtPunCode)("FNSdtSeqNo")
                                    ' Insert Row Vatable เข้า Datatable Tmpe  DT
                                    If oTblRtnPmtVatable.Rows.Count > 0 Then
                                        For nRowRtnPmtVatable As Integer = 0 To oTblRtnPmtVatable.Rows.Count - 1
                                            Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()
                                            oRowDT("Record_Id") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Record_Id").ToString()
                                            oRowDT("Date") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Date").ToString()
                                            oRowDT("Company Code") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Company Code").ToString()
                                            oRowDT("Transaction Type") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Transaction Type").ToString()

                                            'If C_GETxPromotion(tDocPmt, tSeqPmt) Then
                                            oRowDT("Proposition Code") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Proposition Code").ToString()
                                            'Else
                                            ' oRowDT("Proposition Code") = ""
                                            'End If

                                            oRowDT("Sale Code") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Sale Code").ToString()
                                            oRowDT("Sales quantity") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Sales quantity").ToString()
                                            oRowDT("Sales quantity UOM") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Sales quantity UOM").ToString()
                                            '    oRowDT("Sales amount") = C_SETtNumDigit(oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("SaleAm").ToString())
                                            oRowDT("Sales amount") = C_SETtNumDigit(oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("SaleAmt Exvat"))
                                            oRowDT("Discount amount") = C_SETtNumDigit(oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Discount amount").ToString())
                                            oRowDT("Text1") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Text1").ToString()
                                            oRowDT("Text2") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Text2").ToString()
                                            oRowDT("Text3") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Text3").ToString()
                                            oRowDT("Amount01") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Amount01").ToString()
                                            oRowDT("Amount02") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Amount02").ToString()
                                            oRowDT("Amount03") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Amount03").ToString()
                                            oC_TblSaleDT.Rows.Add(oRowDT)
                                        Next
                                    End If

                                    'Check สินค้า Promotion ทีละรายการ

                                    'oSql.Clear()
                                    'oSql.AppendLine(" SELECT FTPdtCode,FTShdDocNo,FNSdtSeqNo FROM TPSTSalDT  ")
                                    'oSql.AppendLine(" WHERE FTShdDocType='9' AND ")
                                    'oSql.AppendLine(" CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' AND")
                                    'oSql.AppendLine(" ISNULL(FTPmhCode,'')!='' AND FTPdtCode='" + oTblRtnPmtPdt.Rows(nRtnPmtPdt)(0).ToString() + "' ")
                                    'oTblRtnPmtPunCodePmt = oDatabase.C_CALoExecuteReader(oSql.ToString)
                                    'If oTblRtnPmtPunCodePmt.Rows.Count > 0 Then
                                    '    For nRow As Integer = 0 To oTblRtnPmtPunCodePmt.Rows.Count - 1
                                    '        If C_GETxPromotion(oTblRtnPmtPunCodePmt.Rows(nRow)("FTShdDocNo"), oTblRtnPmtPunCodePmt.Rows(nRow)("FNSdtSeqNo")) Then

                                    '            For nRowRtnPmtVatable As Integer = 0 To oTblRtnPmtVatable.Rows.Count - 1
                                    '                Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()
                                    '                oRowDT("Record_Id") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Record_Id").ToString()
                                    '                oRowDT("Date") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Date").ToString()
                                    '                oRowDT("Company Code") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Company Code").ToString()
                                    '                oRowDT("Transaction Type") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Transaction Type").ToString()
                                    '                oRowDT("Proposition Code") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Proposition Code").ToString()
                                    '                oRowDT("Sale Code") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Sale Code").ToString()
                                    '                oRowDT("Sales quantity") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Sales quantity").ToString()
                                    '                oRowDT("Sales quantity UOM") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Sales quantity UOM").ToString()
                                    '                oRowDT("Sales amount") = C_SETtNumDigit(oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Sales amount").ToString())
                                    '                oRowDT("Discount amount") = C_SETtNumDigit(oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Discount amount").ToString())
                                    '                oRowDT("Text1") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Text1").ToString()
                                    '                oRowDT("Text2") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Text2").ToString()
                                    '                oRowDT("Text3") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Text3").ToString()
                                    '                oRowDT("Amount01") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Amount01").ToString()
                                    '                oRowDT("Amount02") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Amount02").ToString()
                                    '                oRowDT("Amount03") = oTblRtnPmtVatable.Rows(nRowRtnPmtVatable)("Amount03").ToString()
                                    '                oC_TblSaleDT.Rows.Add(oRowDT)

                                    '            Next
                                    '        End If

                                    '    Next

                                    'End If

                                Next
                            End If
                        Next
                    End If
                Next
            End If

            'Vat
            If oTblRtnPdt.Rows.Count > 0 Then
                'ทำแถว vat 
                'oSql.Clear()
                'oSql.AppendLine("DECLARE @tFTPdtCode AS VARCHAR(20) ")
                'oSql.AppendLine("SELECT ")
                'oSql.AppendLine("'01' AS 'Record_Id'")
                'oSql.AppendLine(",CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date")
                'oSql.AppendLine(",'1090' AS 'Company Code'")
                'oSql.AppendLine(",'VCPR' AS 'Transaction Type'")
                'oSql.AppendLine(",'' AS 'Proposition Code'")
                'oSql.AppendLine(",'' AS 'Sale Code'")
                'oSql.AppendLine(",'' AS 'Sales quantity'")
                'oSql.AppendLine(",'' AS 'Sales quantity UOM'")
                'oSql.AppendLine(",SUM(ISNULL(FCSdtVat,0)) AS 'Sales amount'")
                'oSql.AppendLine(",0.00 AS 'Discount amount'")
                'oSql.AppendLine(",'' AS 'Text1'")
                'oSql.AppendLine(",'' AS 'Text2'")
                'oSql.AppendLine(",'' AS 'Text3'")
                'oSql.AppendLine(",'' AS 'Amount01'")
                'oSql.AppendLine(",'' AS 'Amount02'")
                'oSql.AppendLine(",'' AS 'Amount03'")
                'oSql.AppendLine(" FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode)")
                'oSql.AppendLine("WHERE 1=1 ")
                'oSql.AppendLine("AND FTShdDocType='9' ")
                'oSql.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                'oSql.AppendLine("GROUP BY FDShdDocDate")
                oSql.Clear()
                oSql.AppendLine(" DECLARE @tFTPdtCode AS VARCHAR(20)  ")
                oSql.AppendLine(" SELECT ( SalAmt - (SalAmt * TVAT) / (TVAT + 100)) AS 'Sales amount'  , * FROM ")
                oSql.AppendLine(" (SELECT ")
                oSql.AppendLine(" '01' AS 'Record_Id' ")
                oSql.AppendLine(" ,CONVERT(VARCHAR(10),FDShdDocDate, 112) AS Date ")
                oSql.AppendLine(" ,'" & tCompanyCode & "' AS 'Company Code' ")
                oSql.AppendLine(" ,'VCPR' AS 'Transaction Type' ")
                oSql.AppendLine(" ,'' AS 'Proposition Code' ")
                oSql.AppendLine(" ,'' AS 'Sale Code' ")
                oSql.AppendLine(" ,'' AS 'Sales quantity' ")
                oSql.AppendLine(" ,'' AS 'Sales quantity UOM' ")
                oSql.AppendLine(" ,SUM(ISNULL(FCSdtVat,0)) AS SalAmt ")
                oSql.AppendLine(" ,0.00 AS 'Discount amount' ")
                oSql.AppendLine(" ,'' AS 'Text1' ")
                oSql.AppendLine(" ,'' AS 'Text2' ")
                oSql.AppendLine(" ,'' AS 'Text3' ")
                oSql.AppendLine(" ,'' AS 'Amount01' ")
                oSql.AppendLine(" ,'' AS 'Amount02' ")
                oSql.AppendLine(" ,'' AS 'Amount03', ")
                oSql.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT ")
                oSql.AppendLine("  FROM TPSTSalDT DT LEFT JOIN TCNTPmtHD Pmt ON (DT.FTPmhCode=Pmt.FTPmhCode) ")
                oSql.AppendLine(" WHERE 1=1 AND FTShdDocType='9'  ")
                oSql.AppendLine(" AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                oSql.AppendLine(" GROUP BY FDShdDocDate) AS VAT ")

                oTblRtnPmtVat = oDatabase.C_CALoExecuteReader(oSql.ToString)

                If oTblRtnPmtVat.Rows.Count > 0 Then
                    For nRowRtnPmtVat As Integer = 0 To oTblRtnPmtVat.Rows.Count - 1
                        Dim oRowDT As DataRow = oC_TblSaleDT.NewRow()
                        oRowDT("Record_Id") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Record_Id").ToString()
                        oRowDT("Date") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Date").ToString()
                        oRowDT("Company Code") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Company Code").ToString()
                        oRowDT("Transaction Type") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Transaction Type").ToString()
                        oRowDT("Proposition Code") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Proposition Code").ToString()
                        oRowDT("Sale Code") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Sale Code").ToString()
                        oRowDT("Sales quantity") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Sales quantity").ToString()
                        oRowDT("Sales quantity UOM") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Sales quantity UOM").ToString()
                        '      oRowDT("Sales amount") = C_SETtNumDigit(oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Sales amount").ToString())
                        oRowDT("Sales amount") = C_SETtNumDigit(oTblRtnPmtVat.Rows(nRowRtnPmtVat)("SalAmt").ToString())
                        oRowDT("Discount amount") = C_SETtNumDigit(oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Discount amount").ToString())
                        oRowDT("Text1") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Text1").ToString()
                        oRowDT("Text2") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Text2").ToString()
                        oRowDT("Text3") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Text3").ToString()
                        oRowDT("Amount01") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Amount01").ToString()
                        oRowDT("Amount02") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Amount02").ToString()
                        oRowDT("Amount03") = oTblRtnPmtVat.Rows(nRowRtnPmtVat)("Amount03").ToString()
                        oC_TblSaleDT.Rows.Add(oRowDT)
                    Next
                End If
            End If
            '//----------------------------------End Get loop Pdt DT loop คืน-------------------------------//

        Catch oEx As Exception
            oC_TblSaleDT.Rows.Clear()
            bResult = False
        Finally
        End Try
        Return bResult
    End Function

    Public Function C_CALtSaleAmt(ByVal pnVat As Integer, ByVal ptSaleAmt As Double) As String
        Try
            Dim tDigit As Double = Math.Round(ptSaleAmt, 2)
            Dim tExvat As Double = Math.Round(tDigit - ((tDigit * pnVat) / (100 + pnVat)), 2)

            Return tExvat

        Catch ex As Exception

        End Try
    End Function

    Public Function C_GETxPromotion(ByVal ptDocNo As String, ByVal ptSqeNo As String) As Boolean
        Try
            Dim oDbtblCheckProm As New DataTable
            Dim oDbtblPromotions As New DataTable
            Dim odatabase As New cDatabaseLocal
            Dim oSql As New StringBuilder

            '=== นำรหัส Promotion ไป หาค่า Unloading Point
            oSql.Clear()
            oSql.AppendLine(" SELECT (FCSdtSetPrice - FCSpdDis) AS AmtPromotion FROM TPSTSalPD ")
            oSql.AppendLine("   WHERE (FTShdDocNo = '" & ptDocNo & "' )  ")
            oSql.AppendLine(" AND FNSdtSeqNo = '" & ptSqeNo & "' ")
            oDbtblCheckProm = odatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbtblCheckProm.Rows(0)("AmtPromotion") = "0" Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            Return False
        End Try
    End Function


    ''' <summary>
    ''' สร้าง Column Datatable Temp SaleDT 
    ''' </summary>
    ''' <returns>Datatable</returns>
    Function C_SEToColTblSaleDT() As DataTable
        Dim oTbl As New DataTable
        Try
            oTbl.Columns.Add("Record_Id", GetType(String))
            oTbl.Columns.Add("Date", GetType(String))
            oTbl.Columns.Add("Company Code", GetType(String))
            oTbl.Columns.Add("Transaction Type", GetType(String))
            oTbl.Columns.Add("Proposition Code", GetType(String))
            oTbl.Columns.Add("Sale Code", GetType(String))
            oTbl.Columns.Add("Sales quantity", GetType(String))    'Double
            oTbl.Columns.Add("Sales quantity UOM", GetType(String))
            oTbl.Columns.Add("Sales amount", GetType(String))  'Double
            oTbl.Columns.Add("Discount amount", GetType(String))  'Double
            oTbl.Columns.Add("Text1", GetType(String))
            oTbl.Columns.Add("Text2", GetType(String))
            oTbl.Columns.Add("Text3", GetType(String))
            oTbl.Columns.Add("Amount01", GetType(String))
            oTbl.Columns.Add("Amount02", GetType(String))
            oTbl.Columns.Add("Amount03", GetType(String))
            Return oTbl
        Catch ex As Exception
            Return Nothing
        Finally
            oTbl = Nothing
        End Try

    End Function

    Function C_SETtNumDigit(ByVal tNumber As Double) As String
        Try
            Dim tDigit As String = Math.Round(tNumber, 2)
            Return tDigit
        Catch ex As Exception
            Return ""
        End Try
    End Function

    ''' <summary>
    ''' Export Temp sale to text
    ''' </summary>
    ''' <param name="ptSaleDate">เงือนไขตามวันที่</param>
    Public Sub C_GETbExpSalToFile(ByVal ptSaleDate As String)
        Dim tSaleExp As String = ""
        Dim oSQL As New StringBuilder
        Dim tQtyAll As String = ""
        Dim atQtyAll() As String
        Dim tSleAmt As String = ""
        Dim atSleAmt() As String
        Dim tDisAmt As String = ""
        Dim atDisAmt() As String
        Dim oDatabase As New cDatabaseLocal
        Dim tDir As String = ""
        Dim nSaleAmout As Decimal
        Dim nDiscount As Decimal
        Dim tShopCode As String = AdaConfig.cConfig.oUrsDefault.tShopCode
        Dim nQtyAll As Integer = 0
        Try
            ' Set HD
            ' tSaleExp += "00|SAP|TSM|BFC001|" + Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US")) + vbCr + vbLf

            'แปลงวันที่ 
            Dim tConFormat As String = DateTime.ParseExact(ptSaleDate, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim tDate As String = Convert.ToDateTime(tConFormat).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))


            ' tSaleExp += "00|SAP|TSM|" & tShopCode & "|" + Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US")) + vbCr + vbLf
            tSaleExp += "00|TSM|SAP|" & tShopCode & "|" + tDate + vbCr + vbLf

            If oC_TblSaleDT.Rows.Count > 0 Then
                'Get Sum QtyAll
                'oSQL.Clear()
                'oSQL.AppendLine("SELECT ")
                'oSQL.AppendLine("SUM(CASE FTShdDocType")
                'oSQL.AppendLine(" WHEN '1' THEN ISNULL(FCSdtQtyAll,0)")
                'oSQL.AppendLine(" WHEN '9' THEN ISNULL(FCSdtQtyAll,0)*-1")
                'oSQL.AppendLine(" END) AS 'SumQtyAll' ")
                'oSQL.AppendLine("FROM TPSTSalDT ")
                'oSQL.AppendLine("WHERE 1=1 ")
                'oSQL.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "'")
                'oSQL.AppendLine("GROUP BY FDShdDocDate ")
                'If oSQL.ToString <> "" Then
                '    atQtyAll = Convert.ToString(oDatabase.C_GETtSQLExecuteScalar(oSQL.ToString(), "c")).Split("|")
                '    If atQtyAll(0) = "1" Then
                '        tQtyAll = atQtyAll(1)
                '    End If
                'End If

                For nSum As Integer = 0 To oC_TblSaleDT.Rows.Count - 1
                    nQtyAll += IIf(oC_TblSaleDT.Rows(nSum)("Sales quantity") = "", 0, oC_TblSaleDT.Rows(nSum)("Sales quantity"))
                Next

                'GET Sale Amount
                'oSQL.Clear()
                'oSQL.AppendLine("SELECT ")
                'oSQL.AppendLine("SUM(CASE FTShdDocType")
                'oSQL.AppendLine(" WHEN '1' THEN ISNULL(FCSdtVatable,0)")
                'oSQL.AppendLine(" WHEN '9' THEN ISNULL(FCSdtVatable,0)*-1")
                'oSQL.AppendLine(" END) AS 'Sales Amount' ")
                'oSQL.AppendLine("FROM TPSTSalDT ")
                'oSQL.AppendLine("WHERE 1=1 ")
                'oSQL.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "'")
                'oSQL.AppendLine("GROUP BY FDShdDocDate ")
                '== แก้ไข SumAmout เพิ่ม Sum ค่า FCSdtvat +  FCSdtVatable 10/5/61 CAT
                'oSQL.Clear()
                'oSQL.AppendLine(" SELECT (SaleAmout + Vat) AS SaleAmt  FROM ")
                'oSQL.AppendLine(" (SELECT ")
                'oSQL.AppendLine(" SUM(CASE FTShdDocType")
                'oSQL.AppendLine(" WHEN '1' THEN ISNULL(FCSdtVatable,0)")
                'oSQL.AppendLine(" WHEN '9' THEN ISNULL(FCSdtVatable,0)*-1")
                'oSQL.AppendLine(" END) AS SaleAmout ,")
                'oSQL.AppendLine("  SUM(CASE FTShdDocType  WHEN '1' THEN ISNULL(FCSdtvat,0)")
                'oSQL.AppendLine("  WHEN '9' THEN ISNULL(FCSdtvat,0)*-1  END) AS Vat")
                'oSQL.AppendLine(" FROM TPSTSalDT  WHERE 1=1 ")
                'oSQL.AppendLine(" AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "' ")
                'oSQL.AppendLine(" GROUP BY FDShdDocDate) AS TPS")

                'oSQL.Clear()
                'oSQL.AppendLine(" SELECT SaleAmt + ((Discount - (Discount * TVAT) / (100 + TVAT)))  AS 'Sales Amount',* FROM")
                'oSQL.AppendLine(" (SELECT  ")
                'oSQL.AppendLine(" SUM(CASE FTShdDocType")
                'oSQL.AppendLine(" WHEN '1' THEN ISNULL(FCSdtVatable,0)")
                'oSQL.AppendLine(" WHEN '9' THEN ISNULL(FCSdtVatable,0)*-1")
                'oSQL.AppendLine(" END) AS SaleAmt ,")
                'oSQL.AppendLine(" SUM (ISNULL(FCSdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)) AS Discount ,")
                'oSQL.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT")
                'oSQL.AppendLine(" FROM TPSTSalDT ")
                'oSQL.AppendLine(" WHERE 1 = 1 ")
                'oSQL.AppendLine(" And CONVERT(varchar, FDShdDocDate, 103)='" + ptSaleDate + "' ")
                'oSQL.AppendLine(" GROUP BY FDShdDocDate) AS TPS")


                'If oSQL.ToString <> "" Then
                '    atSleAmt = Convert.ToString(oDatabase.C_GETtSQLExecuteScalar(oSQL.ToString(), "c")).Split("|")
                '    If atSleAmt(0) = "1" Then
                '        tSleAmt = C_SETtNumDigit(atSleAmt(1))
                '    End If
                'End If


                If oC_TblSaleDT.Rows.Count > 0 Then

                    For nSum As Integer = 0 To oC_TblSaleDT.Rows.Count - 1
                        nSaleAmout += IIf(oC_TblSaleDT.Rows(nSum)("Sales amount") = 0, 0, oC_TblSaleDT.Rows(nSum)("Sales amount"))

                        'Discount ตาม แถวที่ ดึงข้อมูลออกมา
                        nDiscount += IIf(oC_TblSaleDT.Rows(nSum)("Discount amount") = 0, 0, oC_TblSaleDT.Rows(nSum)("Discount amount"))
                    Next




                End If

                ' GET Discount Amount
                'oSQL.Clear()
                'oSQL.AppendLine("SELECT ")
                'oSQL.AppendLine("SUM(CASE FTShdDocType")
                'oSQL.AppendLine(" WHEN '1' THEN ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)")
                'oSQL.AppendLine(" WHEN '9' THEN ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)*-1")
                'oSQL.AppendLine(" END) AS 'Discount Amount' ")
                'oSQL.AppendLine("FROM TPSTSalDT ")
                'oSQL.AppendLine("WHERE 1=1 ")
                'oSQL.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "'")
                'oSQL.AppendLine("GROUP BY FDShdDocDate ")
                'Discount Amout แยก Vat
                'oSQL.Clear()
                'oSQL.AppendLine(" SELECT (DiscountAmt - ((DiscountAmt * TVAT) /(100 + TVAT)))  AS 'Discount Amount' , * FROM ")
                'oSQL.AppendLine(" (SELECT ")
                'oSQL.AppendLine(" SUM(CASE FTShdDocType")
                'oSQL.AppendLine("  WHEN '1' THEN ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)")
                'oSQL.AppendLine("  WHEN '9' THEN ISNULL(FCsdtDis + FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg + FCSdtChg,0)*-1")
                'oSQL.AppendLine("  END) AS DiscountAmt,")
                'oSQL.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT ")
                'oSQL.AppendLine("FROM TPSTSalDT ")
                'oSQL.AppendLine("WHERE 1=1 ")
                'oSQL.AppendLine("AND CONVERT(varchar,FDShdDocDate, 103)='" + ptSaleDate + "'")
                'oSQL.AppendLine("GROUP BY FDShdDocDate) AS TPS ")

                'If oSQL.ToString <> "" Then
                '    atDisAmt = Convert.ToString(oDatabase.C_GETtSQLExecuteScalar(oSQL.ToString(), "c")).Split("|")
                '    If atDisAmt(0) = "1" Then
                '        tDisAmt = C_SETtNumDigit(atDisAmt(1))
                '    End If
                'End If




                'Set DT
                For Each oRow As DataRow In oC_TblSaleDT.Rows
                    For Each oCol As DataColumn In oC_TblSaleDT.Columns
                        'Add the Data rows.
                        tSaleExp += oRow(oCol.ColumnName).ToString() & "|"
                    Next
                    'Add new line.
                    tSaleExp += vbCr & vbLf
                Next
            End If
            'Set FT
            '  tSaleExp += "99|" + Convert.ToString(oC_TblSaleDT.Rows.Count) + "|" + tQtyAll + "|" + tSleAmt + "|" + tDisAmt
            tSaleExp += "99|" + Convert.ToString(oC_TblSaleDT.Rows.Count) + "|" + CType(nQtyAll, String) + "|" + C_SETtNumDigit(nSaleAmout) + "|" + C_SETtNumDigit(nDiscount)
            '   tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + Now.ToString("yyyyMMdd")

            If cExpTransaction.tC_ShareTranDate = "" Then
                If tC_SaleDate < cExpPayment.tC_Paydate And cExpPayment.tC_Paydate <> "" Then
                    tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tC_SaleDate
                ElseIf cExpPayment.tC_Paydate < tC_SaleDate And cExpPayment.tC_Paydate <> "" Then
                    tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + cExpPayment.tC_Paydate
                Else
                    tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tC_SaleDate
                End If
            Else
                tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + cExpTransaction.tC_ShareTranDate
            End If

            C_SETxWriteExpSale(tSaleExp, tDir, oC_TblSaleDT.Rows.Count, 0, tDate)
        Catch ex As Exception

        Finally
            tSaleExp = Nothing
            oSQL = Nothing
            tQtyAll = Nothing
            atQtyAll = Nothing
            tSleAmt = Nothing
            atSleAmt = Nothing
            tDisAmt = Nothing
            atDisAmt = Nothing
            oDatabase = Nothing
        End Try

    End Sub

    Private Sub C_SETxWriteExpSale(ByVal ptMsg As String, ByVal ptDir As String, ByVal pnINS As Integer, ByVal pnFailed As Integer, ByVal ptDate As String)
        Dim tPathFile As String = ""
        Dim oWrite As StreamWriter
        Dim tShopCode As String = AdaConfig.cConfig.oUrsDefault.tShopCode
        Try
            '    tPathFile = ptDir + "/" + "TSM_" + Now.ToString("yyyyMMdd") + "_BFC001_SALE.txt"
            '   tPathFile = ptDir + "/" + "TSM_" + Now.ToString("yyyyMMdd") + "_" + tShopCode + "_SALE.txt"
            tPathFile = ptDir + "/" + "TSM_" + ptDate + "_" + tShopCode + "_SALE.txt"

            ' สร้าง Folder ถ้ายังไม่ได้สร้าง
            If (Not System.IO.Directory.Exists(ptDir)) Then
                System.IO.Directory.CreateDirectory(ptDir)
            End If

            ' สร้าง File ถ้ายังไม่ได้สร้าง
            If (Not File.Exists(tPathFile)) Then
                File.Create(tPathFile).Dispose()
                oWrite = New StreamWriter(tPathFile, True)
                oWrite.Write(ptMsg)
                oWrite.Close()
                C_CALxWriteLogEr(tPathFile, "", pnINS, 0)
            Else
                cExpTransaction.tC_WrieLog += 1
                C_CALxWriteLogEr(tPathFile, "Export ข้อมูลเกิน 1 ครั้ง", 0, 1)
            End If
            ' Log-----------------------------
        Catch ex As Exception
            C_CALxWriteLogEr(tPathFile, ex.Message, 0, 1)
        Finally
            tPathFile = Nothing
            oWrite = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="ptFileName">ชื่อไฟล์ที่ Export</param>
    ''' <param name="ptMsg">ข้อความแสดงข้อผิดพลาด</param>
    ''' <param name="pnSuccess">จำนวน Row DT ที่ทำการ Export</param>
    Public Sub C_CALxWriteLogEr(ByVal ptFileName As String, ByVal ptMsg As String, ByVal pnSuccess As Integer, ByVal pnFailed As Integer)
        Dim tC_FileLog As String = AdaConfig.cConfig.tAppPath & "\Log"
        Dim dDateNow As Date = Date.Now
        Dim oHDfile As New StringBuilder
        Dim tDesc As String = ""
        Dim oSQL As New StringBuilder
        Dim tDateTimeNow As String = ""
        Dim oDatabase As New cDatabaseLocal
        Dim tFileName As String = ""

        Try
            If Not IO.Directory.Exists(tC_FileLog) Then
                IO.Directory.CreateDirectory(tC_FileLog)
            End If

            tC_FileLog &= "\" & "Log_Sale" & Format(Date.Now, "yyyyMMdd-HHmmss") & ".txt"
            If File.Exists(tC_FileLog) = False Then
                Using oSw As StreamWriter = File.CreateText(tC_FileLog)
                    oSw.Close()
                End Using

                oHDfile.Append("[Export Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Export Time] " + Format(Date.Now, "HH:mm:ss") + vbTab + "")
                oHDfile.Append("[Export By] " + Environment.MachineName + Environment.NewLine + Environment.NewLine + "")
                oHDfile.Append("[Success] " + Convert.ToString(pnSuccess) + vbTab + "")
                oHDfile.Append("[Description]")
                Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                    With oStreamWriter
                        .WriteLine(oHDfile)
                        .Flush()
                        .Close()
                    End With
                End Using
            End If

            Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                With oStreamWriter
                    .WriteLine(ptMsg)
                    .Flush()
                    .Close()
                End With
            End Using
            C_SAVxLogToDB(ptFileName, pnSuccess, pnFailed, tC_FileLog)
        Catch ex As Exception
            MessageBox.Show("C_CALxWriteLogEr " + ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Insert log to DB
    ''' </summary>
    ''' <param name="ptFullPath">พาธไฟล์ที่ Export</param>
    ''' <param name="pnSuccess">จำนวน Row DT ที่ Export สำเร็จ</param>
    ''' <param name="pnFailed">จำนวนข้อผิด</param>
    ''' <param name="ptPathLog">พาธไฟล์ log</param>
    Private Sub C_SAVxLogToDB(ByVal ptFullPath As String, ByVal pnSuccess As Integer, ByVal pnFailed As Integer, ByVal ptPathLog As String)
        Dim tDesc As String = ""
        Dim tDateTimeNow As String = ""
        Dim tFileName As String = ""
        Dim oSQL As New StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Try
            'Save TLNKLog
            tDesc = "Success(" & Convert.ToString(pnSuccess) & "),Failed(" & Convert.ToString(pnFailed) & ")"
            tDateTimeNow = Format(Date.Now, "yyyy-MM-dd HH:mm:ss.fff")
            tFileName = Path.GetFileName(ptFullPath)
            oSQL.Clear()
            oSQL.AppendLine("INSERT INTO TLNKLog(uid,FDDateTime,FNType,FTFileName,FTDescription,FTRefer) VALUES")
            oSQL.AppendLine("('" + Guid.NewGuid.ToString + "','" + tDateTimeNow + "',2,'" + tFileName + "','" + tDesc + "','" + ptPathLog + "')")
            If oSQL.ToString <> "" Then
                oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
            End If
        Catch ex As Exception
            MessageBox.Show("C_SAVxLogToDB " + ex.Message)
        Finally
            tDesc = Nothing
            tDateTimeNow = Nothing
            tFileName = Nothing
            oSQL = Nothing
            oDatabase = Nothing
        End Try
    End Sub
End Class
