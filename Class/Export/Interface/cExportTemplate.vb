﻿Public Class cExportTemplate

    'Format string condition
    Private Const tC_FmtCondition As String = "Date between {0} to {1}"
    Private Const tC_FmtConditionThai As String = "วันที่ {0} ถึง {1}"
    Private Const tC_FmtConditionAll As String = "All"
    Private Const tC_FmtConditionAllThai As String = "ทั้งหมด"
    'Private Const tC_FmtConditionBch As String = "and branch {0} to {1}"
    'Private Const tC_FmtConditionBchThai As String = " และ สาขา {0} ถึง สาขา {1}"
    'Private Const tC_FmtConditionBchSel As String = " and branch {0}"
    'Private Const tC_FmtConditionBchSelThai As String = " และ สาขา {0}"
    '*CH 19-12-2014
    'Private Const tC_Customer As String = "ข้อมูลลูกค้า;Customer"
    Private Const tC_Sale As String = "ข้อมูลขาย;Sale and Return"
    'Private Const tC_Promotion As String = "ข้อมูลเอกสารโปรโมชั่น;Promotion"
    'Private Const tC_EOD As String = "EOD"
    'Private Const tC_PriceOff As String = "โปรโมชั่น (price off);Promotion (price off)"
    'Private Const tC_Deposit As String = "Deposit;Deposit" '*TON 59-03-07
    Private Const tC_Expense As String = "ค่าใช้จ่าย;Expense"
    Private Const tC_CashTnf As String = "นำเงินเข้าธนาคาร;Money into Bank"
    Public Shared tC_Status As String = ""


    'Sub Class Enum
    Class cTableCode 'Class Enum (ใช้แทน Enum) *Fix
        'Public Const TPSTCst As String = "001"
        'Public Const TPSTSal As String = "002"
        'Public Const TCNTPmt As String = "003"
        'Public Const EOD As String = "004" '*CH 22-10-2014
        'Public Const TCNTPdtAjp As String = "005" 'Price off '*CH 21-01-2015
        'Public Const Deposit As String = "006" '*TON 59-03-07
        Public Const ZSDINT005 As String = "001"
        Public Const ZSDINT006 As String = "002"
        Public Const ZGLINT001 As String = "003"
        Public Const ZGLINT002 As String = "004"
    End Class

    'List Export *Fix สำหรับ Process
    'Shared Property oC_TCNMCst As New cJobTCNMCst
    Shared Property oC_TPSTSal As New cJobTPSTSal
    'Shared Property oC_TCNTPmt As New cJobTCNTPmt
    'Shared Property oC_EOD As New cJobEOD
    'Shared Property oC_Deposit As New cJobDeposit
    'Shared Property oC_PriceOff As New cJobPriceOff
    Shared Property oC_Expense As New cJobExpense
    Shared Property oC_CashTnf As New cJobCashTnf
    Shared Property oC_Progress As ProgressBar

    'List Export แสดงใน Grid
    Private Shared oC_Items As List(Of cItem)

    Private Const tC_StaSendEN As String = "Sent"
    Private Const tC_StaSendTH As String = "ส่งแล้ว"

    Public Sub New()

    End Sub

    Public Shared Function C_GETaItemExport(ByVal ptBchCode As String, ByVal ptSaleDate As String) As List(Of cItem) 'Fix Data
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tWhe As String = ""
        Dim tCode As String = ""
        Dim tPosCode As String = ""
        Dim tStaSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaSendTH, tC_StaSendEN)
        Try
            Select Case cCNVB.tVB_Export
                Case cCNVB.tVB_ZSDINT005 'Sale
                    'PosCode
                    cCNVB.oVB_PosCode = New List(Of String)
                    oSql.Append("SELECT FTPosCode FROM TPSMPos WHERE FTPosStatus = 'A'")
                    oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tWhe))
                    If oDbTbl.Rows.Count > 0 Then
                        For Each oRow As DataRow In oDbTbl.Rows
                            tPosCode = oRow("FTPosCode")
                            If C_CHKbTableTempInDatabase(tPosCode) Then cCNVB.oVB_PosCode.Add(tPosCode)
                        Next
                    End If

                    'TPSTSalHD
                    oSql = New System.Text.StringBuilder
                    oDbTbl = New DataTable
                    oSql.AppendLine("SELECT * FROM (")
                    oSql.AppendLine("SELECT DISTINCT Shd.FTShdDocNo,Shd.FDShdDocDate AS tDocDate,Shd.FTShdDocTime AS tDocTime,")
                    oSql.AppendLine("ISNULL(Sal.FTExpFileName,'') AS tExpFileName,ISNULL(Sal.FTExpStaSend,'') AS tExpStaSend,Shd.FTBchCode,")
                    oSql.AppendLine("CASE WHEN ISNULL(Sal.FTExpFileName,'') = '' THEN 1 ELSE 0 END nStaCrtFile,")
                    oSql.AppendLine("CASE WHEN ISNULL(Sal.FTExpStaSend,'') = '' THEN 1 ELSE 0 END nStaSend")
                    oSql.AppendLine("FROM TPSTSalHD Shd LEFT JOIN TLGTZSDINT005 Sal ON Shd.FTShdDocNo = Sal.FTShdDocNo AND Shd.FTBchCode = Sal.FTBchCode")
                    If ptBchCode <> "" Then
                        oSql.AppendLine("WHERE Shd.FTBchCode = '" & ptBchCode & "'")
                    Else
                        oSql.AppendLine("WHERE 1 = 1")
                    End If
                    If ptSaleDate <> "" Then oSql.AppendLine("AND Shd.FDShdDocDate = '" & ptSaleDate & "'")
                    oSql.AppendLine("{0}")

                    'Temp Sale
                    For Each tPosCode In cCNVB.oVB_PosCode
                        oSql.AppendLine("UNION ALL")
                        oSql.AppendLine("SELECT DISTINCT Shd.FTShdDocNo,Shd.FDShdDocDate AS tDocDate,Shd.FTShdDocTime AS tDocTime,")
                        oSql.AppendLine("ISNULL(Sal.FTExpFileName,'') AS tExpFileName,ISNULL(Sal.FTExpStaSend,'') AS tExpStaSend,Shd.FTBchCode,")
                        oSql.AppendLine("CASE WHEN ISNULL(Sal.FTExpFileName,'') = '' THEN 1 ELSE 0 END nStaCrtFile,")
                        oSql.AppendLine("CASE WHEN ISNULL(Sal.FTExpStaSend,'') = '' THEN 1 ELSE 0 END nStaSend")
                        oSql.AppendLine("FROM TSHD" & tPosCode & " Shd LEFT JOIN TLGTZSDINT005 Sal ON Shd.FTShdDocNo = Sal.FTShdDocNo AND Shd.FTBchCode = Sal.FTBchCode")
                        If ptBchCode <> "" Then
                            oSql.AppendLine("WHERE Shd.FTBchCode = '" & ptBchCode & "'")
                        Else
                            oSql.AppendLine("WHERE 1 = 1")
                        End If
                        If ptSaleDate <> "" Then oSql.AppendLine("AND Shd.FDShdDocDate = '" & ptSaleDate & "'")
                        oSql.AppendLine("AND Shd.FTShdStaDoc = '1'")
                        oSql.AppendLine("{0}")
                    Next
                    oSql.AppendLine(") Shd")
                    oSql.AppendLine("ORDER BY Shd.tDocDate DESC, Shd.tDocTime DESC")
                Case cCNVB.tVB_ZGLINT001 'Expense
                    oSql.AppendLine("SELECT Ltx.FTLogCode AS FTShdDocNo,Ltx.FDDateUpd AS tDocDate, Ltx.FTTimeUpd AS tDocTime,")
                    oSql.AppendLine("ISNULL(Temp.FTExpFileName,'') AS tExpFileName,ISNULL(Temp.FTExpStaSend,'') AS tExpStaSend,LEFT(Ltx.FTlogCode,3) AS FTBchCode,")
                    oSql.AppendLine("CASE WHEN ISNULL(Temp.FTExpFileName,'') = '' THEN 1 ELSE 0 END nStaCrtFile,")
                    oSql.AppendLine("CASE WHEN ISNULL(Temp.FTExpStaSend,'') = '' THEN 1 ELSE 0 END nStaSend,Ltx.FNLtxSeq AS nSeqNo")
                    oSql.AppendLine("FROM TPSTLoginTxn Ltx LEFT JOIN TLGTZGLINT001 Temp ON Ltx.FTLogCode = Temp.FTLogCode AND Ltx.FNLtxType = Temp.FNLtxType AND Ltx.FNLtxSeq = Temp.FNLtxSeq")
                    'oSql.AppendLine("	INNER JOIN TPSTLogIn LogPos ON Ltx.FTLogCode = LogPos.FTLogCode")
                    oSql.AppendLine("	LEFT JOIN (SELECT DISTINCT FTLogCode,FDShdSaleDate FROM TLGTZSDINT005) Sal ON Ltx.FTLogCode = Sal.FTLogCode")
                    If ptBchCode <> "" Then
                        oSql.AppendLine("WHERE LEFT(Ltx.FTlogCode,3) = '" & ptBchCode & "'")
                    Else
                        oSql.AppendLine("WHERE 1 = 1")
                    End If
                    'If ptSaleDate <> "" Then oSql.AppendLine("AND LogPos.FDLogSaleDate = '" & ptSaleDate & "'")
                    If ptSaleDate <> "" Then oSql.AppendLine("AND (Sal.FDShdSaleDate = '" & ptSaleDate & "' OR Ltx.FDDateIns = '" & ptSaleDate & "')")
                    oSql.AppendLine("AND Ltx.FNLtxType = 2")
                    oSql.AppendLine("{0}")
                    oSql.AppendLine("ORDER BY Ltx.FDDateUpd DESC, Ltx.FTTimeUpd DESC")
                Case cCNVB.tVB_ZGLINT002 'Cash Transfer
                    oSql.AppendLine("SELECT Mib.FTMibDocNo FTShdDocNo,ISNULL(Temp.FTExpFileName,'') AS tExpFileName,ISNULL(Temp.FTExpStaSend,'') AS tExpStaSend,Mib.FTBchCode,")
                    oSql.AppendLine("	CASE WHEN ISNULL(Temp.FTExpFileName,'') = '' THEN 1 ELSE 0 END nStaCrtFile,")
                    oSql.AppendLine("	CASE WHEN ISNULL(Temp.FTExpStaSend,'') = '' THEN 1 ELSE 0 END nStaSend,")
                    oSql.AppendLine("	Mib.FDMibDocDate AS tDocDate, Mib.FTMibDocTime AS tDocTime")
                    oSql.AppendLine("FROM TACTMnyInBnk Mib LEFT JOIN TLGTZGLINT002 Temp ON Mib.FTMibDocNo = Temp.FTMibDocNo")
                    If ptBchCode <> "" Then
                        oSql.AppendLine("WHERE Mib.FTBchCode = '" & ptBchCode & "'")
                    Else
                        oSql.AppendLine("WHERE 1 = 1")
                    End If
                    If ptSaleDate <> "" Then oSql.AppendLine("AND Mib.FDMibDocDate = '" & ptSaleDate & "'")
                    oSql.AppendLine("AND Mib.FTMibStaSndBnk = '2'")
                    oSql.AppendLine("{0}")
                    oSql.AppendLine("ORDER BY Mib.FDMibDocDate DESC, Mib.FTMibDocTime DESC")
            End Select

            'oSql.AppendLine("ORDER BY Shd.FDShdDocDate,Shd.FTShdDocTime")
            If oC_Items Is Nothing Then
                If cCNVB.bVB_Auto = True Then  'PAN 2016-06-16 Check Auto 
                    '### Auto ###
                    If cCNVB.tVB_Export <> "" Then 'PAN 2016-06-09 
                        oC_Items = New List(Of cItem)
                        Select Case cCNVB.tVB_Export
                            Case cCNVB.tVB_ZSDINT005 'Sale
                                tCode = cTableCode.ZSDINT005
                                tWhe = "AND (ISNULL(Sal.FTExpFileName,'') = '' OR ISNULL(Sal.FTExpStaSend,'') = '')"
                                oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tWhe))
                            Case cCNVB.tVB_ZSDINT006 'Reconcile
                                oC_Items.Add(New cItem With {.tCode = cTableCode.ZSDINT006, .tItem = "Sale and Return (Reconcile)", .tItemThai = "ข้อมูลขาย (Reconcile)", .tTable = "TPSTSalHD"})
                            Case cCNVB.tVB_ZGLINT001 'Expense
                                tCode = cTableCode.ZGLINT001
                                tWhe = "AND (ISNULL(Temp.FTExpFileName,'') = '' OR ISNULL(Temp.FTExpStaSend,'') = '')"
                                oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tWhe))
                            Case cCNVB.tVB_ZGLINT002 'Cash Transfer
                                tCode = cTableCode.ZGLINT002
                                tWhe = "AND (ISNULL(Temp.FTExpFileName,'') = '' OR ISNULL(Temp.FTExpStaSend,'') = '')"
                                oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tWhe))
                        End Select
                    Else
                        oC_Items = New List(Of cItem)
                    End If
                Else
                    '### Manual ###
                    oC_Items = New List(Of cItem)
                    Select Case cCNVB.tVB_Export
                        Case cCNVB.tVB_ZSDINT005
                            tCode = cTableCode.ZSDINT005
                            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tWhe))
                        Case cCNVB.tVB_ZSDINT006
                            tCode = cTableCode.ZSDINT006
                            oC_Items.Add(New cItem With {.tCode = cTableCode.ZSDINT006, .tItem = "Sale and Return (Reconcile)", .tItemThai = "ข้อมูลขาย (Reconcile)", .tTable = "TPSTSalHD"})
                        Case cCNVB.tVB_ZGLINT001
                            tCode = cTableCode.ZGLINT001
                            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tWhe))
                        Case cCNVB.tVB_ZGLINT002
                            tCode = cTableCode.ZGLINT002
                            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tWhe))
                    End Select
                End If
            End If
            For nItem = 0 To oDbTbl.Rows.Count - 1
                With oC_Items
                    Select Case cCNVB.tVB_Export
                        Case cCNVB.tVB_ZGLINT001
                            .Add(New cItem With {.tCode = tCode,
                                             .tBchSelect = oDbTbl.Rows(nItem)("FTBchCode"),
                                             .tItem = oDbTbl.Rows(nItem)("FTShdDocNo"), .tItemThai = oDbTbl.Rows(nItem)("FTShdDocNo"),
                                             .tExpFileName = oDbTbl.Rows(nItem)("tExpFileName"),
                                             .tExpStaSend = IIf(oDbTbl.Rows(nItem)("tExpStaSend") <> "", tStaSend, ""),
                                             .nStaCrtFile = Convert.ToInt32(oDbTbl.Rows(nItem)("nStaCrtFile")),
                                             .nStaSend = Convert.ToInt32(oDbTbl.Rows(nItem)("nStaSend")),
                                             .tTable = "TPSTSalHD",
                                             .tDocDate = oDbTbl.Rows(nItem)("tDocDate"),
                                             .tDocTime = oDbTbl.Rows(nItem)("tDocTime"),
                                             .nSeqNo = oDbTbl.Rows(nItem)("nSeqNo")})
                        Case Else
                            .Add(New cItem With {.tCode = tCode,
                                             .tBchSelect = oDbTbl.Rows(nItem)("FTBchCode"),
                                             .tItem = oDbTbl.Rows(nItem)("FTShdDocNo"), .tItemThai = oDbTbl.Rows(nItem)("FTShdDocNo"),
                                             .tExpFileName = oDbTbl.Rows(nItem)("tExpFileName"),
                                             .tExpStaSend = IIf(oDbTbl.Rows(nItem)("tExpStaSend") <> "", tStaSend, ""),
                                             .nStaCrtFile = Convert.ToInt32(oDbTbl.Rows(nItem)("nStaCrtFile")),
                                             .nStaSend = Convert.ToInt32(oDbTbl.Rows(nItem)("nStaSend")),
                                             .tTable = "TPSTSalHD",
                                             .tDocDate = oDbTbl.Rows(nItem)("tDocDate"),
                                             .tDocTime = oDbTbl.Rows(nItem)("tDocTime")})
                    End Select
                End With
            Next
        Catch ex As Exception
        End Try
        Return oC_Items
    End Function

    'Get Format Condition
    Public Shared Function C_GETtFmtCondition(Optional ptDateFrom As String = "", Optional ptDateTo As String = "", Optional ptBchFrom As String = "", Optional ptBchTo As String = "", Optional ptBchSelect As String = "") As String
        Dim tStrCondition As String = ""
        Select Case AdaConfig.cConfig.oApplication.nLanguage
            Case 2 '2:Eng
                If ptDateFrom.Length = 0 And ptDateTo.Length = 0 Then
                    tStrCondition = tC_FmtConditionAll
                Else
                    tStrCondition = String.Format(tC_FmtCondition, ptDateFrom, ptDateTo)
                End If

                If ptBchFrom.Length > 0 And ptBchTo.Length > 0 Then
                    'tStrCondition &= String.Format(tC_FmtConditionBch, ptBchFrom, ptBchTo)
                    tStrCondition &= String.Format(cCNVB.tVB_FmtCondition, ptBchFrom, ptBchTo) '*CH 05-12-2014
                Else
                    If ptBchSelect.Length > 0 Then
                        'tStrCondition &= String.Format(tC_FmtConditionBchSel, ptBchSelect)
                        tStrCondition &= String.Format(cCNVB.tVB_FmtConditionSel, ptBchSelect) '*CH 05-12-2014
                    End If
                End If

            Case Else '1:thai
                If ptDateFrom.Length = 0 And ptDateTo.Length = 0 Then
                    tStrCondition = tC_FmtConditionAllThai
                Else
                    tStrCondition = String.Format(tC_FmtConditionThai, ptDateFrom, ptDateTo)
                End If

                If ptBchFrom.Length > 0 And ptBchTo.Length > 0 Then
                    'tStrCondition &= String.Format(tC_FmtConditionBchThai, ptBchFrom, ptBchTo)
                    tStrCondition &= String.Format(cCNVB.tVB_FmtConditionThai, ptBchFrom, ptBchTo) '*CH 05-12-2014
                Else
                    If ptBchSelect.Length > 0 Then
                        'tStrCondition &= String.Format(tC_FmtConditionBchSelThai, ptBchSelect)
                        tStrCondition &= String.Format(cCNVB.tVB_FmtConditionSelThai, ptBchSelect) '*CH 05-12-2014
                    End If
                End If

        End Select

        Return tStrCondition
    End Function

    'Process
    Public Shared Sub C_CALxProcessAll()
        cCNVB.bVB_StaCheckExpContinue = False
        cCNVB.bVB_ExportContinue = True
        If Not cCNVB.bVB_Auto Then wExports.olaValStaExp.Text = "" '*CH 19-12-2014

        cCNVB.oDBBchMap = C_GEToBchMapping(cCNVB.oDBBchMap)

        Dim oItem As New List(Of cItem)
        oItem = oC_Items.Where(Function(c) c.nSelect = 1).ToList
        'For Each oItem In oC_Items.Where(Function(c) c.nSelect = 1) 'Where nSelect = รายการที่เลือก 1:ทำ , 2:ไม่ทำ
        If oItem.Count > 0 Then
            Select Case oItem(0).tCode
                Case cTableCode.ZSDINT005
                    '1:thai, 2:Eng '*CH 19-12-2014
                    tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_Sale.Split(";")(0), tC_Sale.Split(";")(1))
                    'wExports.C_SETxStatusPrc(0)
                    If Not cCNVB.bVB_Auto Then wExports.oW_BackgroudWord.ReportProgress(0)
                    With oC_TPSTSal
                        cLog.C_CALxWriteLog("ZSDINT005")
                        .tC_Code = cTableCode.ZSDINT005
                        .oC_Item = New List(Of cItem)
                        .oC_Item = oItem
                        .C_CALxProcess()
                    End With
                Case cTableCode.ZSDINT006
                    tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_Sale.Split(";")(0), tC_Sale.Split(";")(1))
                    If Not cCNVB.bVB_Auto Then wExpReconcile.oW_BackgroudWord.ReportProgress(0)
                    With oC_TPSTSal
                        .tC_Code = cTableCode.ZSDINT006
                        .tC_DateFrom = oItem(0).tDateFrom
                        .tC_DateTo = oItem(0).tDateTo
                        'Get Date Unsend to FTP
                        .oC_Item = C_GEToDataUnsendFTP(oItem(0).tDateFrom, oItem(0).tDateTo)
                        .C_CALxProcess()
                    End With
                Case cTableCode.ZGLINT001
                    tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_Expense.Split(";")(0), tC_Expense.Split(";")(1))
                    If Not cCNVB.bVB_Auto Then wExpExpense.oW_BackgroudWord.ReportProgress(0)
                    With oC_Expense
                        .oC_Item = oItem
                        .C_CALxProcess()
                    End With
                Case cTableCode.ZGLINT002
                    tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_CashTnf.Split(";")(0), tC_CashTnf.Split(";")(1))
                    If Not cCNVB.bVB_Auto Then wExpCashTnf.oW_BackgroudWord.ReportProgress(0)
                    With oC_CashTnf
                        .oC_Item = oItem
                        .C_CALxProcess()
                    End With
            End Select
        End If
        'Next
    End Sub

    ''' <summary>
    ''' Check Table Temp Sale in base
    ''' </summary>
    ''' <param name="ptPosCode"></param>
    ''' <returns>Boolean True:have, False:not have</returns>
    Private Shared Function C_CHKbTableTempInDatabase(ByVal ptPosCode As String) As Boolean
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Try
            oSql.AppendLine("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES")
            oSql.AppendLine("WHERE TABLE_NAME='TSHD" & ptPosCode & "'")
            oSql.AppendLine("AND TABLE_TYPE='BASE TABLE'")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl IsNot Nothing AndAlso oDbTbl.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Function

    'Clear ตัวแปรกลาง
    Public Shared Sub C_CALxClear()
        oC_Items = Nothing
        oC_TPSTSal = Nothing
        oC_TPSTSal = New cJobTPSTSal
    End Sub

    'Sub Class Import
    Class cItem
        Property nSelect As Integer = 1
        Property tItem As String = ""
        Property tItemThai As String = ""
        Property tConditon As String = ""
        Property tCode As String = ""
        Property tDateFrom As String = ""
        Property tDateTo As String = ""
        Property nMode As Integer = 0 '0:All,1:Interval,2:Select
        Property tBchFrom As String = ""
        Property tBchTo As String = ""
        Property tBchSelect As String = ""
        Property tTable As String = ""
        Property tExpFileName As String = ""
        Property tExpStaSend As String = ""
        Property nStaCrtFile As Integer = 0
        Property nStaSend As Integer = 0
        Property tDocDate As String = ""
        Property tDocTime As String = ""
        Property nSeqNo As Integer = 0
    End Class

    ''' <summary>
    ''' Get Data unsend to FTP
    ''' </summary>
    ''' <returns>List(Of cItem)</returns>
    Private Shared Function C_GEToDataUnsendFTP(ByVal ptDateFrm As String, ByVal ptDateTo As String) As List(Of cItem)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim oItem As New List(Of cItem)
        Try
            'oSql.AppendLine("SELECT FTBchCode,FTShdDocNo,FTExpFileName,FTExpStaSend ")
            'oSql.AppendLine("FROM TLGTZSDINT005")
            'oSql.AppendLine("WHERE 1=1")
            'If ptDateFrm <> "" And ptDateTo <> "" Then
            '    oSql.AppendLine("AND FDShdSaleDate BETWEEN '" & ptDateFrm & "' AND '" & ptDateTo & "'")
            'End If
            'oSql.AppendLine("	AND FTExpFileName <> ''")
            'oSql.AppendLine("	AND FTExpStaSend = ''")
            'oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            'If oDbTbl.Rows.Count > 0 Then
            '    oItem = (From oRow As DataRow In oDbTbl.Rows
            '             Select New cItem With {
            '                 .tCode = cTableCode.ZSDINT005,
            '                 .tBchSelect = oRow("FTBchCode"),
            '                 .tItem = oRow("FTShdDocNo"),
            '                 .tExpFileName = oRow("FTExpFileName"),
            '                 .nStaSend = 1,
            '                 .tTable = "TPSTSalHD"
            '                 }).ToList
            'End If
            '*CH 10-10-2017
            oSql.AppendLine("SELECT DISTINCT Shd.FTShdDocNo,Shd.FDShdDocDate AS tDocDate,Shd.FTShdDocTime AS tDocTime,")
            oSql.AppendLine("ISNULL(Sal.FTExpFileName,'') AS tExpFileName,ISNULL(Sal.FTExpStaSend,'') AS tExpStaSend,Shd.FTBchCode,")
            oSql.AppendLine("1 AS nStaCrtFile,") 'สร้างไฟล์ใหม่เสมอ
            oSql.AppendLine("CASE WHEN ISNULL(Sal.FTExpStaSend,'') = '' THEN 1 ELSE 0 END nStaSend")
            oSql.AppendLine("FROM TPSTSalHD Shd LEFT JOIN TLGTZSDINT005 Sal ON Shd.FTShdDocNo = Sal.FTShdDocNo AND Shd.FTBchCode = Sal.FTBchCode")
            oSql.AppendLine("WHERE 1=1")
            If ptDateFrm <> "" And ptDateTo <> "" Then
                oSql.AppendLine("AND Shd.FDShdDocDate BETWEEN '" & ptDateFrm & "' AND '" & ptDateTo & "'")
            End If
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                oItem = (From oRow As DataRow In oDbTbl.Rows
                         Select New cItem With {
                             .tCode = cTableCode.ZSDINT005,
                             .tBchSelect = oRow("FTBchCode"),
                             .tItem = oRow("FTShdDocNo"),
                             .tExpFileName = oRow("tExpFileName"),
                             .nStaCrtFile = Convert.ToInt32(oRow("nStaCrtFile")),
                             .nStaSend = Convert.ToInt32(oRow("nStaSend")),
                             .tTable = "TPSTSalHD"
                             }).ToList
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return oItem
    End Function
End Class
