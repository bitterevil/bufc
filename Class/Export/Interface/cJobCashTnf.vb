﻿Public Class cJobCashTnf
    Inherits cBaseExport

    Property oC_Item As List(Of cExportTemplate.cItem)
    Property oC_CashTnf As New cCashTnf
    Private Const tC_FmtFile As String = "{0}{1}" 'Path,FileName

    Public Overrides Sub C_CALxProcess()
        Dim otBchCode As New List(Of String)
        Dim oCrtFile As New List(Of cExportTemplate.cItem)
        Dim oSendFile As New List(Of cExportTemplate.cItem)
        Dim otPathFileBk As New List(Of String)
        Dim dNow As DateTime = Now
        Dim bStaPrc As Boolean = True
        Dim tBchMap As String = ""
        Dim tPlant As String = ""
        Dim tPath As String = ""
        Dim tPathFile As String = ""
        Dim nRunning As Integer = 1
        Try
            tPath = AdaConfig.cConfig.oConfigXml.tOutbox
            otBchCode = (From oBch In oC_Item Select oBch.tBchSelect).Distinct.ToList
            If otBchCode.Count > 0 Then
                With oC_CashTnf
                    For Each tBch In otBchCode
                        nRunning = C_GETnFileRunning(tBch) 'Running File '*CH 12-10-2017
                        tBchMap = SP_GETtBranchMap(tBch)
                        tPlant = SP_GETtPlantMap(tBch)
                        'Prepare File
                        'tPathFile = String.Format(tC_FmtFile, tPath, "\Cash\CASH_{0}_{1}_{2}") & ".xml"
                        'tPathFile = String.Format(tPathFile, tPlant, Now.ToString("yyyyMMdd"), Now.ToString("HHmmss"))
                        'Not use time change to Running '*CH 12-10-2017
                        tPathFile = String.Format(tC_FmtFile, tPath, "\Cash\CASH_{0}_{1}_")
                        tPathFile = String.Format(tPathFile, tPlant, Now.ToString("yyyyMMdd")) & "{0}.xml"
                        .tC_FullPath = tPathFile

                        tPathFile = String.Format(tC_FmtFile, IO.Path.GetDirectoryName(tPath), "\LogExport\Log_ZGLINT002_{0}") & ".txt"
                        tPathFile = String.Format(tPathFile, Now.ToString("yyyyMMdd"))
                        .tC_PathFileLog = tPathFile

                        oCrtFile = oC_Item.Where(Function(c) c.nStaCrtFile = 1 And c.tBchSelect = tBch).ToList
                        If oCrtFile.Count > 0 Then
                            bStaPrc = .C_CRTbCrateFileExpense(oCrtFile, tBchMap, tPlant, nRunning)
                        End If

                        'Send File
                        oSendFile = oC_Item.Where(Function(c) c.nStaSend = 1 And c.tBchSelect = tBch).ToList
                        If oSendFile.Count > 0 Then
                            otPathFileBk = New List(Of String)
                            bStaPrc = .C_PRCbSendFile2FTP(oSendFile, otPathFileBk)
                            If bStaPrc Then
                                If Not cCNVB.bVB_Auto Then wExpCashTnf.oW_BackgroudWord.ReportProgress(80)

                                'Back up File
                                If otPathFileBk IsNot Nothing Then
                                    For nItem = 0 To otPathFileBk.Count - 1
                                        C_CALxBackupFileSale(otPathFileBk(nItem))
                                    Next
                                End If
                                If Not cCNVB.bVB_Auto Then wExpCashTnf.oW_BackgroudWord.ReportProgress(100)
                            End If
                        End If
                    Next

                    'Log
                    .C_SAVxLog(oC_Item, dNow)
                End With
                If Not cCNVB.bVB_Auto Then wExpCashTnf.bW_StaProcess = bStaPrc
            End If
        Catch ex As Exception
        Finally
            otBchCode = Nothing
            oCrtFile = Nothing
            oSendFile = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Copy file to backup folder
    ''' </summary>
    ''' <param name="ptFileCopyBk">Path file move to backup</param>
    Public Sub C_CALxBackupFileSale(ByVal ptFileCopyBk As String)
        Dim tFileName As String = ""
        Dim tPathMove As String = ""
        Dim tDateNow As String = ""
        Try
            tDateNow = Now.ToString("yyyyMMdd")
            tPathMove = AdaConfig.cConfig.oConfigXml.tBackup & "\CASH_" & tDateNow
            Try
                If IO.File.Exists(ptFileCopyBk) Then
                    'Create Dir '*CH 20-10-2015
                    If Not IO.Directory.Exists(tPathMove) Then
                        IO.Directory.CreateDirectory(tPathMove)
                    End If

                    tFileName = IO.Path.GetFileName(ptFileCopyBk)
                    FileCopy(ptFileCopyBk, tPathMove & "\" & tFileName)
                    IO.File.Delete(ptFileCopyBk)
                End If
                'nIndex += 1
            Catch ex As Exception
            End Try
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Get Running number File
    ''' </summary>
    Private Function C_GETnFileRunning(ByVal ptBchCode As String) As Integer
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim nRunning As Integer = 1
        Try
            'Mapping
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("Select ISNULL(MAX(FTExpRunning),0)FTExpRunning")
            oSql.AppendLine("FROM TLGTZGLINT002")
            oSql.AppendLine("WHERE FDDateUpd = '" & Now.ToString("yyyy-MM-dd") & "'")
            oSql.AppendLine("AND FTBchCode = '" & ptBchCode & "'")
            oDbTbl = New DataTable
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                nRunning = Convert.ToInt32(oDbTbl.Rows(0)("FTExpRunning").ToString)
                nRunning += 1
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return nRunning
    End Function
End Class
