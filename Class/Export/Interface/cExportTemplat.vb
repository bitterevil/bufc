﻿Imports System.IO
Public Class cExportTemplat
    Class cTableCode 'Class Enum (ใช้แทน Enum) *Fix
        Public Const TCNMPdtGrp As String = "001"
        Public Const TCNMPdt As String = "002"
        Public Const TCNTPmt As String = "003"
        Public Const TCNTPdtAjp As String = "004"
        Public Const TCNTPdtTnf As String = "005"  ' Type 1

    End Class

    Shared Property oC_Progress As ProgressBar
    Private Shared oC_Items As List(Of cItem)



    Private Const tC_Acticle As String = "สินค้า;Article"
    Private Const tC_Promotion As String = "โปรโมชั่น;Promotion"
    Private Const tC_PdtAjp As String = "ข้อมูลใบปรับราคาขาย;Sale price adjustment"
    Private Const tC_PdtGrp As String = "กลุ่มสินค้า;Product Group"
    Private Const tC_PdtTnf As String = "ข้อมูลใบรับเข้าสินค้า;Product Received"
    Public Shared tC_Status As String = ""
    Public Shared tC_File As String = ""

    Shared Property oC_TCNTPmt As cJobTCNTPmtImp 'Promotion Price '*CH 27-11-2014
        Shared Property oC_TCNMPdt As cJobTCNMPdt
        Shared Property oC_TCNTPdtAjp As cJobTCNTPdtAjp
        Shared Property oC_TCNTPdtTnf As cJobTCNTPdtTnf
        Shared Property oC_TCNMPdtGrp As cJobTCNMPdtGrp
        Shared Property bC_Status As Boolean = False



        Public Shared Function C_GETaItemImport() As List(Of cItem) 'Fix Data
        If oC_Items Is Nothing Then
            oC_Items = New List(Of cItem)
            With oC_Items
                .Add(New cItem With {.tCode = cTableCode.TCNMPdtGrp, .tItem = "Article", .tItemThai = "สินค้า"})
                .Add(New cItem With {.tCode = cTableCode.TCNMPdt, .tItem = "Sale price adjustment", .tItemThai = "ข้อมูลใบปรับราคาขาย"})
                .Add(New cItem With {.tCode = cTableCode.TCNTPmt, .tItem = "Prduct Received", .tItemThai = "ข้อมูลใบรับเข้าสินค้า"})
            End With
        End If
        Return oC_Items
    End Function


    Public Shared Sub C_CALxProcessAll()
        bC_Status = True

        Dim tDateNow As String = Format(Date.Now, "yyyyMMddHHmmss")
        For Each oItem In oC_Items.Where(Function(c) c.nSelect = 1) 'Where nSelect = รายการที่เลือก 1:ทำ , 2:ไม่ทำ
            Select Case oItem.tCode
                Case cTableCode.TCNMPdt
                    tC_File = ""
                    tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_Acticle.Split(";")(0), tC_Acticle.Split(";")(1))
                    If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                    oC_TCNMPdt.tC_Log = cLogTemplate.tC_FileMaster
                    oC_TCNMPdt.tC_DateNow = tDateNow
                    oC_TCNMPdt.C_CALxProcess()
                Case cTableCode.TCNTPdtAjp
                    tC_File = ""
                    tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_PdtAjp.Split(";")(0), tC_PdtAjp.Split(";")(1))
                    If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                    oC_TCNTPdtAjp.tC_Log = cLogTemplate.tC_FileTCNTPdtAJP
                    oC_TCNTPdtAjp.tC_DateNow = tDateNow
                    oC_TCNTPdtAjp.C_CALxProcess()
                Case cTableCode.TCNTPdtTnf
                    tC_File = ""
                    tC_Status = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_PdtTnf.Split(";")(0), tC_PdtTnf.Split(";")(1))
                    If Not cCNVB.bVB_Auto Then wImports.oW_BackgroudWord.ReportProgress(0)
                    oC_TCNTPdtTnf.tC_Log = cLogTemplate.tC_FileTCNTPdtTnf
                    oC_TCNTPdtTnf.tC_DateNow = tDateNow
                    oC_TCNTPdtTnf.C_CALxProcess()
            End Select
        Next
 
        cCNSP.SP_SETxPurgeBackup()
    End Sub

    Class cItem
        Property nSelect As Integer = 1
        Property tCode As String = ""
        Property tItem As String = ""
        Property tItemThai As String = ""
        Property nCount As Integer = 0
        Property tSelectCount As String = ""
    End Class

End Class
