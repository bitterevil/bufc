﻿Imports System.Data.SqlClient
Imports System.IO

Public Class cJobTPSTSal
    Inherits cBaseExport

    Property oC_Progress As ProgressBar

    Property oC_TPSTSal As New cTPSTSal
    Property tC_Code As String = "" 'cExportTemplate.cTableCode.TPSTSal
    Property tC_Table As String = "TPSTSal"
    Private Const tC_FmtFile As String = "{0}{1}" 'Path,FileName
    Property bC_StaCrtFile As Boolean = False
    Property bC_StaSend As Boolean = False
    Property oC_Item As List(Of cExportTemplate.cItem)
    Property tC_DateFrom As String = ""
    Property tC_DateTo As String = ""

    'Process
    Public Overrides Sub C_CALxProcess()
        cCNVB.bVB_DocNotComplete = False
        cCNVB.tVB_MsgExportSale = ""
        Dim oCrtFile As New List(Of cExportTemplate.cItem)
        Dim oSendFile As New List(Of cExportTemplate.cItem)
        Dim bStaPrc As Boolean = True
        Dim tPathFile As String = ""
        'Dim tPathFileBk As String = ""
        Dim otPathFileBk As New List(Of String)
        Dim dNow As DateTime = Now
        Dim tPlant As String = ""
        Dim nRunning As Integer = 1
        Try
            'Mapping
            C_GETxVariable()
            'cCNVB.tVB_StoreNo = SP_GETtBranchMap(cCNVB.tVB_BchCode)
            tPlant = SP_GETtPlantMap(cCNVB.tVB_BchCode)

            nRunning = C_GETnFileRunning() 'Running File '*CH 14-11-2017

            Dim tPath = AdaConfig.cConfig.oConfigXml.tOutbox

            With oC_TPSTSal
                'AddHandler .EventProgress, AddressOf C_CALxProgress
                '.tPathControl = String.Format(tC_FmtFile, tPath, "\Sale\{0}_" & "CTL_{1}")
                .tOutPath = AdaConfig.cConfig.oConfigXml.tOutbox & "\Sale"
                .tHDTable = tC_Table & "HD"
                .tDTTable = tC_Table & "DT"
                .tRCTable = tC_Table & "RC"
                .tPDTable = tC_Table & "PD"
                .tDCTable = tC_Table & "DC"

                Select Case tC_Code
                    Case cExportTemplate.cTableCode.ZSDINT005
                        'tPathFile = String.Format(tC_FmtFile, tPath, "\Sale\SALE_{0}_{1}_{2}") & ".tmp"
                        'tPathFile = String.Format(tPathFile, tPlant, Now.ToString("yyyyMMdd"), Now.ToString("HHmmss"))
                        tPathFile = String.Format(tC_FmtFile, tPath, "\Sale\SALE_{0}_{1}_")
                        tPathFile = String.Format(tPathFile, tPlant, Now.ToString("yyyyMMdd")) & "{0}.tmp"
                        .tFullPath = tPathFile
                        .tPathFileLog = Path.GetDirectoryName(AdaConfig.cConfig.oConfigXml.tOutbox) & "\LogExport\Log_ZSDINT005_" & Format(Now, "yyyyMMdd") & ".txt"

                        oCrtFile = oC_Item.Where(Function(c) c.nStaCrtFile = 1).ToList
                        If oCrtFile.Count > 0 Then
                            bStaPrc = .C_CRTbCrateFileSale(oCrtFile, tPlant, nRunning)
                        End If

                        oSendFile = oC_Item.Where(Function(c) c.nStaSend = 1).ToList
                        If oSendFile.Count > 0 Then
                            bStaPrc = .C_PRCbSendFile2FTP(oSendFile, otPathFileBk)
                            If bStaPrc Then
                                If Not cCNVB.bVB_Auto Then wExports.oW_BackgroudWord.ReportProgress(80)

                                'Back up File
                                If otPathFileBk IsNot Nothing Then
                                    For nItem = 0 To otPathFileBk.Count - 1
                                        C_CALxBackupFileSale(otPathFileBk(nItem))
                                    Next
                                End If
                                If Not cCNVB.bVB_Auto Then wExports.oW_BackgroudWord.ReportProgress(100)
                            End If
                        End If

                        'Log
                        If oCrtFile.Count > 0 OrElse oSendFile.Count > 0 Then
                            .C_SAVxLog(oC_Item, dNow)
                        End If

                        If Not cCNVB.bVB_Auto Then wExports.bW_StaProcess = bStaPrc
                    Case cExportTemplate.cTableCode.ZSDINT006
                        Dim oZSDINT006 As New List(Of cZSDINT006)

                        'tPathFile = String.Format(tC_FmtFile, tPath, "\Sale\SALE_{0}_{1}_{2}") & ".tmp"
                        'tPathFile = String.Format(tPathFile, tPlant, Now.ToString("yyyyMMdd"), Now.ToString("HHmmss"))
                        tPathFile = String.Format(tC_FmtFile, tPath, "\Sale\SALE_{0}_{1}_")
                        tPathFile = String.Format(tPathFile, tPlant, Now.ToString("yyyyMMdd")) & "{0}.tmp"
                        .tFullPath = tPathFile
                        .tPathFileLog = Path.GetDirectoryName(AdaConfig.cConfig.oConfigXml.tOutbox) & "\LogExport\Log_ZSDINT006_" & Format(Now, "yyyyMMdd") & ".txt"

                        If Not cCNVB.bVB_Auto Then wExpReconcile.oW_BackgroudWord.ReportProgress(0)

                        'Send Export file UnSend
                        oSendFile = oC_Item.Where(Function(c) c.nStaSend = 1).ToList
                        If oSendFile.Count > 0 Then
                            bStaPrc = .C_CRTbCrateFileSale(oSendFile, tPlant, nRunning, cExportTemplate.cTableCode.ZSDINT006) 'Create New XML
                            cCNVB.tVB_Export = cCNVB.tVB_ZSDINT005
                            bStaPrc = .C_PRCbSendFile2FTP(oSendFile, otPathFileBk)
                            If bStaPrc Then
                                If Not cCNVB.bVB_Auto Then wExpReconcile.oW_BackgroudWord.ReportProgress(20)

                                'Back up File
                                If otPathFileBk IsNot Nothing Then
                                    For nItem = 0 To otPathFileBk.Count - 1
                                        C_CALxBackupFileSale(otPathFileBk(nItem))
                                    Next
                                End If
                                If Not cCNVB.bVB_Auto Then wExpReconcile.oW_BackgroudWord.ReportProgress(40)
                            End If
                        End If

                        'Create And Send file Reconcile
                        cCNVB.tVB_Export = cCNVB.tVB_ZSDINT006
                        tPathFile = String.Format(tC_FmtFile, tPath, "\Sale\EOD_{0}_{1}") & ".txt"
                        tPathFile = String.Format(tPathFile, Now.ToString("yyyyMMdd"), Now.ToString("HHmmss"))
                        .tFullPath = tPathFile
                        .tDateFrom = tC_DateFrom
                        .tDateTo = tC_DateTo
                        If bStaPrc Then If Not .C_CRTbCrateFileReconcile(oZSDINT006, tPlant) Then bStaPrc = False
                        If oZSDINT006.Count > 0 Then
                            If bStaPrc Then If Not .C_PRCbSendFileReconcile2FTP() Then bStaPrc = False
                            If bStaPrc Then C_CALxBackupFileSale(tPathFile)
                        End If

                        'Save Log
                        .C_SAVxLogReconcile(oC_Item, oZSDINT006, dNow, bStaPrc)

                        If Not cCNVB.bVB_Auto Then wExpReconcile.oW_BackgroudWord.ReportProgress(100)

                        If Not cCNVB.bVB_Auto Then wExpReconcile.bW_StaProcess = bStaPrc
                End Select
            End With
        Catch ex As Exception
        Finally
            oCrtFile = Nothing
            oSendFile = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Get Mapping
    ''' </summary>
    Private Sub C_GETxVariable()
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tCode As String = ""
        Try
            'Mapping
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("SELECT FTLNMCode, CASE WHEN ISNULL(FTLNMUsrValue,'') = '' THEN ISNULL(FTLNMDefValue,'') ELSE ISNULL(FTLNMUsrValue,'') END Value")
            oSql.AppendLine("FROM TLNKMapping")
            oSql.AppendLine("WHERE FTLNMCode IN ('SALETYPE001','SALETYPE002','SALEITEMTYPE001','SALEITEMTYPE002','SALEITEMTYPE003','SALEITEMTYPE004','SALEITEMTYPE005','SALEITEMTYPE006')")
            oDbTbl = New DataTable
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                For Each oRow As DataRow In oDbTbl.Rows
                    tCode = oRow("FTLNMCode").ToString
                    Select Case tCode.ToUpper
                        Case "SALETYPE001" : cCNVB.tVB_SaleCode = oRow("Value").ToString
                        Case "SALETYPE002" : cCNVB.tVB_ReturnCode = oRow("Value").ToString
                        Case "SALEITEMTYPE001" : cCNVB.tVB_SaleItemCode = oRow("Value").ToString
                        Case "SALEITEMTYPE002" : cCNVB.tVB_SaleFreeItemCode = oRow("Value").ToString
                        Case "SALEITEMTYPE003" : cCNVB.tVB_SaleServiceItemCode = oRow("Value").ToString
                        Case "SALEITEMTYPE004" : cCNVB.tVB_ReturnItemCode = oRow("Value").ToString
                        Case "SALEITEMTYPE005" : cCNVB.tVB_ReturnFreeItemCode = oRow("Value").ToString
                        Case "SALEITEMTYPE006" : cCNVB.tVB_ReturnServiceItemCode = oRow("Value").ToString
                    End Select
                Next
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Sub

    Sub C_CALxProgress(pnValue As Integer, pnCount As Integer)
        oC_Progress.Maximum = pnCount
        oC_Progress.Value = pnValue
    End Sub

    ''' <summary>
    ''' Copy file to backup folder
    ''' </summary>
    ''' <param name="ptFileCopyBk">Path file move to backup</param>
    Public Sub C_CALxBackupFileSale(ByVal ptFileCopyBk As String)
        Dim tFileName As String = ""
        Dim tPathMove As String = ""
        Dim tDateNow As String = ""
        Try
            tDateNow = Now.ToString("yyyyMMdd")
            tPathMove = AdaConfig.cConfig.oConfigXml.tBackup & "\SALE_" & tDateNow
            Try
                If File.Exists(ptFileCopyBk) Then
                    'Create Dir '*CH 20-10-2015
                    If Not IO.Directory.Exists(tPathMove) Then
                        IO.Directory.CreateDirectory(tPathMove)
                    End If

                    tFileName = Path.GetFileName(ptFileCopyBk)
                    FileCopy(ptFileCopyBk, tPathMove & "\" & tFileName)
                    File.Delete(ptFileCopyBk)
                End If
                'nIndex += 1
            Catch ex As Exception
            End Try
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Get Running number File
    ''' </summary>
    Private Function C_GETnFileRunning() As Integer
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim nRunning As Integer = 1
        Try
            'Mapping
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("Select ISNULL(MAX(FTExpRunning),0)FTExpRunning")
            oSql.AppendLine("FROM TLGTZSDINT005")
            oSql.AppendLine("WHERE FDDateUpd = '" & Now.ToString("yyyy-MM-dd") & "'")
            oDbTbl = New DataTable
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                nRunning = Convert.ToInt32(oDbTbl.Rows(0)("FTExpRunning").ToString)
                nRunning += 1
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return nRunning
    End Function
End Class
