﻿Imports System.IO
Imports System.Text

Public Class cExpPayment

    'Create Temp Table.
    Private oC_DbTblTemp As New DataTable
    Public Shared tC_Paydate As String


    ''' <summary>
    ''' Select Data Export And Insert To TempTable
    ''' </summary>
    ''' <param name="ptPayDate">Date Export</param>
    ''' <returns></returns>
    Public Function C_SETbExpPayment(ByVal ptPayDate As String) As Boolean
        Try
            Dim oDbTblRcCash As New DataTable  'Get ข้อมูล Table Rc ประเภท Cash
            Dim oDbTblRcCredit As New DataTable  'Get ข้อมูล Table Rc ประเภท Credit
            Dim oDbTblRcCheuqe As New DataTable  'Get ข้อมูล Table Rc ประเภท Cheuqe
            Dim oDbTblRcOther As New DataTable  'Get ข้อมูล Table Rc ประเภท Other
            Dim oSql As New StringBuilder
            Dim oDatabase As New cDatabaseLocal
            Dim bResult As Boolean = True
            Dim tComCode As String = AdaConfig.cConfig.oUrsDefault.tCompCode

            'แปลงวันที่ให้เป็นค่าที่ส่งมา 
            Dim tConFormat As String = DateTime.ParseExact(ptPayDate, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim tDate As String = Convert.ToDateTime(tConFormat).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))

            oC_DbTblTemp = C_SEToColumnPayment()
            oSql.Clear()
            oSql.AppendLine(" SELECT")
            oSql.AppendLine(" '01' AS FTRecordNo , '" & tComCode & "' AS FTCompanyCode, 'CSH' AS FTRcvCode,FDShdDocDate,")
            oSql.AppendLine("  CASE  WHEN TSR1.FTSumSale IS NULL THEN '0.00'  ELSE TSR1.FTSumSale END FTSumSale,")
            oSql.AppendLine("  CASE  WHEN TSR2.FTSumReturn IS NULL THEN '0.00'  ELSE TSR2.FTSumReturn END FTSumReturn")
            oSql.AppendLine(" FROM (")

            'TSR1 = Groupby Table TPSTSalRC ประเถทขาย
            oSql.AppendLine("  SELECT FTRcvCode,CONVERT(varchar, FDShdDocDate, 112) AS FDShdDocDate,")
            oSql.AppendLine("  SUM (FCSrcNet) AS FTSumSale")
            oSql.AppendLine("  FROM     dbo.TPSTSalRC")
            oSql.AppendLine("  WHERE  (1 = 1) AND (CONVERT(varchar, FDShdDocDate, 103) = '" & ptPayDate & "') AND (FTShdDocType = 1) AND (FTRcvCode = '001')")
            oSql.AppendLine("  GROUP BY FDShdDocDate, FTRcvCode) as TSR1")

            'TSR2 = Groupby Table TPSTSalRC ประเภทคืน
            oSql.AppendLine("  FULL OUTER JOIN (")
            oSql.AppendLine("  SELECT FTRcvCode,")
            oSql.AppendLine("  SUM (FCSrcNet) AS FTSumReturn")
            oSql.AppendLine("  FROM   dbo.TPSTSalRC")
            oSql.AppendLine("  WHERE  (1 = 1) AND (CONVERT(varchar, FDShdDocDate, 103) = '" & ptPayDate & "') AND (FTShdDocType = 9) AND")
            oSql.AppendLine("  (FTRcvCode = '001') ")
            oSql.AppendLine("  GROUP BY FDShdDocDate, FTRcvCode) AS TSR2")
            oSql.AppendLine("  ON TSR1.FTRcvCode = TSR2.FTRcvCode")

            oDbTblRcCash = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTblRcCash.Rows.Count > 0 Then
                For nRowCount As Integer = 0 To oDbTblRcCash.Rows.Count - 1
                    Dim oRowDt As DataRow = oC_DbTblTemp.NewRow()
                    oRowDt("FTRecordNo") = oDbTblRcCash.Rows(nRowCount)("FTRecordNo").ToString()
                    oRowDt("FDShdDocDate") = tDate
                    oRowDt("FTCompanyCode") = oDbTblRcCash.Rows(nRowCount)("FTCompanyCode").ToString()
                    oRowDt("FTTypeCredit") = ""
                    oRowDt("FTRcvCode") = oDbTblRcCash.Rows(nRowCount)("FTRcvCode").ToString()
                    oRowDt("FTSumSale") = IIf(oDbTblRcCash.Rows(nRowCount)("FTSumSale") = 0, "", oDbTblRcCash.Rows(nRowCount)("FTSumSale"))
                    oRowDt("FTSumReturn") = IIf(oDbTblRcCash.Rows(nRowCount)("FTSumReturn") = 0, "", oDbTblRcCash.Rows(nRowCount)("FTSumReturn"))
                    'Dim tExvatSale As String = C_SUMtExVatForCash(ptPayDate, oDbTblRcCash.Rows(nRowCount)("FTSumSale"))
                    'oRowDt("FTSumSale") = C_SETtTsys(tExvatSale)
                    '  If oDbTblRcCash.Rows(nRowCount)("FTSumReturn") = 0 Then
                    ' oRowDt("FTSumReturn") = ""
                    ' Else
                    'Dim tExvatReturn As String = C_SUMtExVatForCash(ptPayDate, oDbTblRcCash.Rows(nRowCount)("FTSumReturn"))
                    'oRowDt("FTSumReturn") = C_SETtTsys(tExvatReturn)

                    'End If
                    oC_DbTblTemp.Rows.Add(oRowDt)
                Next

            End If

            '==== END ประเภท เงินสด  ==='


            '==== ประเภท บัตรเครดิต ==='
            oSql.Clear()
            oSql.AppendLine(" SELECT")
            oSql.AppendLine("  '01' AS FTRecordNo ,'" & tComCode & "' AS FTCompanyCode,'CRC' AS FTRcvCode,")
            oSql.AppendLine("  TSR1.FTLNMUsrValue,")
            oSql.AppendLine("  CASE WHEN  TSR1.FTSumSale IS NULL THEN '0.00' ELSE TSR1.FTSumSale END FTSumSale,")
            oSql.AppendLine("  CASE WHEN TSR2.FTSumReturn IS NULL THEN '0.00' ELSE TSR2.FTSumReturn END FTSumReturn")
            oSql.AppendLine("  FROM ")
            'TSR1 = Groupby Table TPSTSalRC ประเถทขาย
            oSql.AppendLine("       (SELECT SUM(TPS.FCSrcNet) AS FTSumSale, TLM.FTLNMUsrValue")
            oSql.AppendLine("       FROM     dbo.TPSTSalRC TPS INNER JOIN")
            oSql.AppendLine("       dbo.TLNKMapping TLM ON TPS.FTBnkCode = TLM.FTLNMName")
            oSql.AppendLine("       WHERE  (1 = 1) AND (CONVERT(varchar, TPS.FDShdDocDate, 103) = '" & ptPayDate & "')")
            oSql.AppendLine("       AND (TPS.FTRcvCode = '002') AND (TPS.FTShdDocType = 1) ")
            oSql.AppendLine("       GROUP BY TLM.FTLNMUsrValue) AS TSR1")
            oSql.AppendLine("  FULL OUTER JOIN ")
            'TSR2 = Groupby Table TPSTSalRC ประเภทคืน
            oSql.AppendLine("       (SELECT SUM(TPS.FCSrcNet) AS FTSumReturn, TLM.FTLNMUsrValue")
            oSql.AppendLine("       FROM dbo.TPSTSalRC TPS INNER JOIN")
            oSql.AppendLine("       dbo.TLNKMapping TLM ON TPS.FTBnkCode = TLM.FTLNMName")
            oSql.AppendLine("       WHERE  (1 = 1) AND (CONVERT(varchar, TPS.FDShdDocDate, 103) = '" & ptPayDate & "') ")
            oSql.AppendLine("       AND (TPS.FTRcvCode = '002') AND (TPS.FTShdDocType = 9)")
            oSql.AppendLine("       GROUP BY TLM.FTLNMUsrValue) AS TSR2")
            oSql.AppendLine("       ON TSR1.FTLNMUsrValue = TSR2.FTLNMUsrValue")

            oDbTblRcCredit = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTblRcCredit.Rows.Count > 0 Then
                For Each nRowCD As DataRow In oDbTblRcCredit.Rows
                    Dim oRowDt As DataRow = oC_DbTblTemp.NewRow()
                    oRowDt("FTRecordNo") = nRowCD("FTRecordNo")
                    oRowDt("FDShdDocDate") = tDate
                    oRowDt("FTCompanyCode") = nRowCD("FTCompanyCode")

                    If nRowCD("FTLNMUsrValue") IsNot DBNull.Value Then
                        oRowDt("FTTypeCredit") = nRowCD("FTLNMUsrValue")
                    Else
                        oRowDt("FTTypeCredit") = C_GETtCredit(nRowCD("FTSumSale"), nRowCD("FTSumReturn"), ptPayDate)
                    End If

                    oRowDt("FTRcvCode") = nRowCD("FTRcvCode")
                    oRowDt("FTSumSale") = IIf(nRowCD("FTSumSale") = 0, "", nRowCD("FTSumSale"))
                    oRowDt("FTSumReturn") = IIf(nRowCD("FTSumReturn") = 0, "", nRowCD("FTSumReturn"))

                    '   Dim tExvatCd As String = C_SUMtExVat(ptPayDate, nRowCD("FTSumSale"))
                    ' oRowDt("FTSumSale") = C_SETtTsys(tExvatCd)


                    'If nRowCD("FTSumReturn") = 0 Then
                    '    oRowDt("FTSumReturn") = ""
                    'Else
                    '    Dim tExvatCdReturn As String = C_SUMtExVat(ptPayDate, nRowCD("FTSumReturn"))
                    '    oRowDt("FTSumReturn") = C_SETtTsys(tExvatCdReturn)
                    'End If

                    oC_DbTblTemp.Rows.Add(oRowDt)
                Next
            End If
            '==== END ประเภท บัตรเครดิต ==='

            '==== ประเภท Cheque ==='
            oSql.Clear()
            oSql.AppendLine(" SELECT")
            oSql.AppendLine(" '01' AS FTRecordNo , '" & tComCode & "' AS FTCompanyCode, 'CHQ' AS	FTRcvCode,")
            oSql.AppendLine(" CASE  WHEN TSR1.FTSumSale IS NULL THEN '0.00'  ELSE TSR1.FTSumSale END FTSumSale,")
            oSql.AppendLine("  CASE  WHEN TSR2.FTSumReturn IS NULL THEN '0.00'  ELSE TSR2.FTSumReturn END FTSumReturn")
            oSql.AppendLine(" FROM (")

            'TSR1 = Groupby Table TPSTSalRC ประเถทขาย
            oSql.AppendLine("  SELECT")
            oSql.AppendLine("  SUM (FCSrcNet) AS FTSumSale,FTRcvCode")
            oSql.AppendLine("  FROM     dbo.TPSTSalRC")
            oSql.AppendLine("  WHERE  (1 = 1) AND (CONVERT(varchar, FDShdDocDate, 103) = '" & ptPayDate & "')")
            oSql.AppendLine("  AND (FTShdDocType = 1) AND (FTRcvCode = '003')")
            oSql.AppendLine("  GROUP BY FDShdDocDate, FTRcvCode) as TSR1")
            oSql.AppendLine(" FULL OUTER JOIN ( ")

            'TSR2 = Groupby Table TPSTSalRC ประเภทคืน
            oSql.AppendLine(" SELECT ")
            oSql.AppendLine(" SUM (FCSrcNet) AS FTSumReturn,FTRcvCode ")
            oSql.AppendLine(" FROM   dbo.TPSTSalRC")
            oSql.AppendLine("  WHERE  (1 = 1) AND (CONVERT(varchar, FDShdDocDate, 103) = '" & ptPayDate & "') AND (FTShdDocType = 9) AND")
            oSql.AppendLine(" (FTRcvCode = '003')")
            oSql.AppendLine("  GROUP BY FDShdDocDate, FTRcvCode) AS TSR2")

            oSql.AppendLine("  ON TSR1.FTRcvCode = TSR2.FTRcvCode")
            oDbTblRcCheuqe = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTblRcCheuqe.Rows.Count > 0 Then
                For nRowCheuqe As Integer = 0 To oDbTblRcCheuqe.Rows.Count - 1
                    Dim oRowDt As DataRow = oC_DbTblTemp.NewRow()
                    oRowDt("FTRecordNo") = oDbTblRcCheuqe.Rows(nRowCheuqe)("FTRecordNo").ToString()
                    oRowDt("FDShdDocDate") = tDate
                    oRowDt("FTCompanyCode") = oDbTblRcCheuqe.Rows(nRowCheuqe)("FTCompanyCode").ToString()
                    oRowDt("FTTypeCredit") = ""
                    oRowDt("FTRcvCode") = oDbTblRcCheuqe.Rows(nRowCheuqe)("FTRcvCode").ToString()

                    oRowDt("FTSumSale") = IIf(oDbTblRcCheuqe.Rows(nRowCheuqe)("FTSumSale") = 0, "", oDbTblRcCheuqe.Rows(nRowCheuqe)("FTSumSale"))
                    oRowDt("FTSumReturn") = IIf(oDbTblRcCheuqe.Rows(nRowCheuqe)("FTSumReturn") = 0, "", oDbTblRcCheuqe.Rows(nRowCheuqe)("FTSumReturn"))

                    'Dim tExvatChe As String = C_SUMtExVat(ptPayDate, oDbTblRcCheuqe.Rows(nRowCheuqe)("FTSumSale"))
                    'oRowDt("FTSumSale") = C_SETtTsys(tExvatChe)
                    'If oDbTblRcCheuqe.Rows(nRowCheuqe)("FTSumReturn") = "0" Then
                    '    oRowDt("FTSumReturn") = ""
                    'Else
                    '    Dim tExvatCheReturn As String = C_SUMtExVat(ptPayDate, oDbTblRcCheuqe.Rows(nRowCheuqe)("FTSumReturn").ToString())
                    '    oRowDt("FTSumReturn") = C_SETtTsys(tExvatCheReturn)
                    'End If

                    oC_DbTblTemp.Rows.Add(oRowDt)
                Next
            End If
            '==== END ประเภท Cheque ==='


            '==== ประเภท อื่น ๆ==='
            oSql.Clear()
            oSql.AppendLine(" SELECT")
            oSql.AppendLine(" '01' AS FTRecordNo , '" & tComCode & "' AS FTCompanyCode, 'OTH' AS FTRcvCode,")
            oSql.AppendLine("  CASE  WHEN TSR1.FTSumSale IS NULL THEN '0.00'  ELSE TSR1.FTSumSale END FTSumSale,")
            oSql.AppendLine(" CASE  WHEN TSR2.FTSumReturn IS NULL THEN '0.00'  ELSE TSR2.FTSumReturn END FTSumReturn")
            oSql.AppendLine(" FROM (")
            'TSR1 = Groupby Table TPSTSalRC ประเถทขาย
            oSql.AppendLine(" SELECT ")
            oSql.AppendLine(" SUM (FCSrcNet) AS FTSumSale ,FTRcvCode")
            oSql.AppendLine(" FROM     dbo.TPSTSalRC")
            oSql.AppendLine(" WHERE  (1 = 1) AND (CONVERT(varchar, FDShdDocDate, 103) = '" & ptPayDate & "') AND (FTShdDocType = 1)")
            oSql.AppendLine(" AND (FTRcvCode NOT IN('001','002','003')) ")
            oSql.AppendLine(" GROUP BY FDShdDocDate, FTRcvCode) AS TSR1")
            oSql.AppendLine(" FULL OUTER JOIN (")
            'TSR2 = Groupby Table TPSTSalRC ประเภทคืน
            oSql.AppendLine(" SELECT ")
            oSql.AppendLine(" SUM (FCSrcNet) AS FTSumReturn ,FTRcvCode")
            oSql.AppendLine(" FROM     dbo.TPSTSalRC")
            oSql.AppendLine(" 		WHERE  (1 = 1)  AND (CONVERT(varchar, FDShdDocDate, 103) = '" & ptPayDate & "') AND (FTShdDocType = 9)")
            oSql.AppendLine(" AND (FTRcvCode NOT IN('001','002','003'))")
            oSql.AppendLine(" GROUP BY FDShdDocDate, FTRcvCode) AS TSR2")
            oSql.AppendLine("  ON TSR1.FTRcvCode = TSR2.FTRcvCode")
            oDbTblRcOther = oDatabase.C_CALoExecuteReader(oSql.ToString)

            If oDbTblRcOther.Rows.Count > 0 Then
                For nRowOth As Integer = 0 To oDbTblRcOther.Rows.Count - 1
                    Dim oRowDt As DataRow = oC_DbTblTemp.NewRow()
                    oRowDt("FTRecordNo") = oDbTblRcOther.Rows(nRowOth)("FTRecordNo").ToString()
                    oRowDt("FDShdDocDate") = tDate
                    oRowDt("FTCompanyCode") = oDbTblRcOther.Rows(nRowOth)("FTCompanyCode").ToString()
                    oRowDt("FTTypeCredit") = ""
                    oRowDt("FTRcvCode") = oDbTblRcOther.Rows(nRowOth)("FTRcvCode").ToString()

                    oRowDt("FTSumSale") = IIf(oDbTblRcOther.Rows(nRowOth)("FTSumSale") = 0, "", oDbTblRcOther.Rows(nRowOth)("FTSumSale"))
                    oRowDt("FTSumReturn") = IIf(oDbTblRcOther.Rows(nRowOth)("FTSumReturn") = 0, "", oDbTblRcOther.Rows(nRowOth)("FTSumReturn"))
                    'Dim tExVatOth As String = C_SUMtExVat(ptPayDate, oDbTblRcOther.Rows(nRowOth)("FTSumSale"))
                    'oRowDt("FTSumSale") = C_SETtTsys(tExVatOth)

                    'If oDbTblRcOther.Rows(nRowOth)("FTSumReturn") = "0" Then
                    '    oRowDt("FTSumReturn") = ""
                    'Else
                    '    Dim tExVatOthReturn As String = C_SUMtExVat(ptPayDate, oDbTblRcOther.Rows(nRowOth)("FTSumReturn").ToString())
                    '    oRowDt("FTSumReturn") = C_SETtTsys(tExVatOthReturn)
                    'End If

                    oC_DbTblTemp.Rows.Add(oRowDt)
                Next
            End If
            '==== END ประเภท อื่น ๆ==='

            Return bResult

        Catch ex As Exception
            C_CALxWriteLogEr(ex.Message, "", oC_DbTblTemp.Rows.Count, "Expression")
            Return False
        End Try
    End Function

    Function C_SUMtExVat(ByVal tDate As String, ByVal tSum As String) As String
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oDbtblRo As New DataTable
            Dim oSql As New StringBuilder
            oSql.Clear()
            oSql.AppendLine(" DECLARE @tSum as VARCHAR(10)")
            oSql.AppendLine(" SET @tSum = '" & tSum & "' ")
            oSql.AppendLine(" SELECT  (@tSum - ((@tSum * TVAT)/(100 + TVAT))) AS SumExVat FROM ")
            oSql.AppendLine(" (SELECT  SUM(FCSHDRnd) AS SumRounding , (SELECT TOP(1) FCCmpVatAmt FROM TCNMcomp) AS TVAT ")
            oSql.AppendLine(" FROM TPSTSalHD ")
            oSql.AppendLine(" WHERE (CONVERT(varchar, FDShdDocDate, 103) = '" & tDate & "' )")
            oSql.AppendLine(" GROUP BY (CONVERT(varchar, FDShdDocDate, 103))) as TRO ")

            oDbtblRo = oDatabase.C_CALoExecuteReader(oSql.ToString)
            '  Dim tSumEx As String = Format("{0:D2}", oDbtblRo.Rows(0).Item("SumExVat"))
            Dim tx As String = Math.Round(oDbtblRo.Rows(0).Item("SumExVat"), 2)
            If oDbtblRo.Rows.Count > 0 Then
                Return tx
            End If

        Catch ex As Exception
            Return ""
        End Try
    End Function


    Function C_GETtCredit(ByVal tSale As String, ByVal tReturn As String, ByVal tDate As String) As String
        Try
            Dim oDbtblRo As New DataTable
            Dim oSql As New StringBuilder
            Dim tWhereType As String = ""
            Dim tCredit As String = ""

            If tSale <> "0" Then
                tWhereType = "1"
            ElseIf tReturn <> "0" Then
                tWhereType = "9"
            End If

            oSql.Clear()
            oSql.AppendLine(" SELECT  TLM.FTLNMUsrValue ")
            oSql.AppendLine(" FROM dbo.TPSTSalRC TPS INNER JOIN ")
            oSql.AppendLine(" dbo.TLNKMapping TLM ON TPS.FTBnkCode = TLM.FTLNMName")
            oSql.AppendLine(" WHERE  (1 = 1) AND (CONVERT(varchar, TPS.FDShdDocDate, 103) = '" & tDate & "') ")
            oSql.AppendLine(" AND (TPS.FTRcvCode = '002') AND (TPS.FTShdDocType = '" & tWhereType & "') ")
            oSql.AppendLine(" GROUP BY TLM.FTLNMUsrValue")
            Dim oDatabase As New cDatabaseLocal
            oDbtblRo = oDatabase.C_CALoExecuteReader(oSql.ToString)
            tCredit = oDbtblRo.Rows(0).Item("FTLNMUsrValue")


            If oDbtblRo.Rows.Count > 0 Then
                Return tCredit
            End If

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Function ForCash(ByVal tDate As String, ByVal tSum As String) As String
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oDbtblRo As New DataTable
            Dim oSql As New StringBuilder
            oSql.Clear()
            oSql.AppendLine(" DECLARE @tSum as VARCHAR(10)")
            oSql.AppendLine(" SET @tSum = '" & tSum & "' ")
            oSql.AppendLine(" SELECT  ((@tSum - ((@tSum * TVAT)/(100 + TVAT)))- SumRounding) AS SumExVat FROM ")
            oSql.AppendLine(" (SELECT  SUM(FCSHDRnd) AS SumRounding , (SELECT TOP(1) FCCmpVatAmt FROM TCNMcomp) AS TVAT")
            oSql.AppendLine(" FROM TPSTSalHD")
            oSql.AppendLine(" WHERE (CONVERT(varchar, FDShdDocDate, 103) = '" & tDate & "' )")
            oSql.AppendLine(" GROUP BY (CONVERT(varchar, FDShdDocDate, 103))) as TRO ")

            oDbtblRo = oDatabase.C_CALoExecuteReader(oSql.ToString)
            '  Dim tSumEx As String = Format("{0:D2}", oDbtblRo.Rows(0).Item("SumExVat"))
            Dim tx As String = Math.Round(oDbtblRo.Rows(0).Item("SumExVat"), 2)
            If oDbtblRo.Rows.Count > 0 Then
                Return tx
            End If

        Catch ex As Exception
            Return ""
        End Try
    End Function

    ''' <summary>
    ''' Create Column on TempData
    ''' </summary>
    ''' <returns></returns>
    Function C_SEToColumnPayment() As DataTable
        Dim oDbTbl As New DataTable

        Try
            oDbTbl.Columns.Add("FTRecordNo", GetType(String))
            oDbTbl.Columns.Add("FDShdDocDate", GetType(String))
            oDbTbl.Columns.Add("FTCompanyCode", GetType(String))
            oDbTbl.Columns.Add("FTRcvCode", GetType(String))
            oDbTbl.Columns.Add("FTTypeCredit", GetType(String))
            oDbTbl.Columns.Add("FTSumSale", GetType(String))
            oDbTbl.Columns.Add("FTSumReturn", GetType(String))
            Return oDbTbl

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' ต่อ String ที่รับมาจาก TempData
    ''' </summary>
    ''' <param name="ptPaymentDate"> Date Export </param>
    Public Sub C_GETbExpPaymentToFile(ByVal ptPaymentDate As String)

        Dim oSQL As New StringBuilder
        Dim tCountTemp As String = ""
        Dim tPaymentText As String = ""
        Dim tDir As String = ""
        Dim tSumNetTypeSale As String = ""
        Dim tSumNetTypeReturn As String = ""
        Dim nSumSale As Decimal
        Dim nSumReturn As Decimal
        Dim tShopcode As String = AdaConfig.cConfig.oUrsDefault.tShopCode

        Try

            'แปลงวันที่ 
            Dim tConFormat As String = DateTime.ParseExact(ptPaymentDate, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim tDate As String = Convert.ToDateTime(tConFormat).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))
            'วันที่สำหรับสร้าง Folder 
            tC_Paydate = Convert.ToDateTime(tConFormat).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))

            tCountTemp = oC_DbTblTemp.Rows.Count
            If oC_DbTblTemp.Rows.Count > 0 Then
                For nRowSum As Integer = 0 To oC_DbTblTemp.Rows.Count - 1

                    nSumSale += IIf(oC_DbTblTemp.Rows(nRowSum)("FTSumSale") = "", 0, oC_DbTblTemp.Rows(nRowSum)("FTSumSale"))
                    '     tSumSaleSys = C_SETtTsys(nSumSale)
                Next

                For nRowReturn As Integer = 0 To oC_DbTblTemp.Rows.Count - 1
                    nSumReturn += IIf(oC_DbTblTemp.Rows(nRowReturn)("FTSumReturn") = "", 0, oC_DbTblTemp.Rows(nRowReturn)("FTSumReturn"))
                    '   tSumReturnSys = C_SETtTsys(nSumReturn)
                Next
            End If




            'Header
            '   tPaymentText += "00|TSM|SAP|BFC001|" & Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US")) + vbCr + vbLf
            ' tPaymentText += "00|TSM|SAP|" & tShopcode & "|" & Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US")) + vbCr + vbLf
            '  tPaymentText += "00|TSM|SAP|" & tShopcode & "|" & tDate + vbCr + vbLf
            tPaymentText += "00|TSM|SAP|" & tShopcode & "|" & tDate + vbLf

            'For Detail TempTable 
            For Each oRow As DataRow In oC_DbTblTemp.Rows
                'Loop Column text +
                For Each oCol As DataColumn In oC_DbTblTemp.Columns
                    'Add One Rows.
                    tPaymentText += oRow(oCol.ColumnName).ToString & ("|")
                Next
                '  tPaymentText += "0.00" + vbCr + vbLf
                tPaymentText += "0.00" + vbLf
            Next

            'Footer

            tPaymentText += "99|" + CType(tCountTemp + "|", String) + C_SETtNumDigit(nSumSale) + "|" + C_SETtNumDigit(nSumReturn) + "|" & "0.00"



            ' tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + Now.ToString("yyyyMMdd")

            If cExpTransaction.tC_ShareTranDate = "" Then
                If cExpSale.tC_SaleDate < tC_Paydate And cExpSale.tC_SaleDate <> "" Then
                    tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + cExpSale.tC_SaleDate
                ElseIf tC_Paydate < cExpSale.tC_SaleDate And cExpSale.tC_SaleDate <> "" Then
                    tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tC_Paydate
                Else
                    tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tC_Paydate
                End If
            Else
                tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + cExpTransaction.tC_ShareTranDate
            End If



            'Write Textfile.
            ' Dim tPathFile As String = tDir + "/" + "TSM_" + Now.ToString("yyyyMMdd") + "_" + tShopcode + "_PAYMENT.txt"
            Dim tPathFile As String = tDir + "/" + "TSM_" + tC_Paydate + "_" + tShopcode + "_PAYMENT.txt"

            If (Not File.Exists(tPathFile)) Then
                C_SETxWriteExpPayment(tPaymentText, tDir, tDate)

            Else
                cExpTransaction.tC_WrieLog += 1
                C_CALxWriteLogEr("Export ข้อมูลเกิน 1 ครั้ง", tPathFile, "0", "Expression")
            End If


        Catch ex As Exception
            C_CALxWriteLogEr(ex.Message, "", oC_DbTblTemp.Rows.Count, "Expression")
        End Try
    End Sub

    Private Function C_SETtTsys(ByVal tSumSys As Decimal) As String
        Try
            Dim nResult As Decimal
            Dim oDatabase As New cDatabaseLocal
            Dim oDbtblDe As New DataTable
            Dim oSQL As New StringBuilder

            oSQL.Clear()
            oSQL.AppendLine(" SELECT *")
            oSQL.AppendLine(" FROM [dbo].[TSysConfig]")
            oSQL.AppendLine(" WHERE FTSysNameTha Like '%ปัดเศษ%'")
            oDbtblDe = oDatabase.C_GEToTblSQL(oSQL.ToString())
            If oDbtblDe.Rows.Count > 0 Then
                If oDbtblDe.Rows(1).Item("FTSysUsrValue") = "1" Then

                    Select Case oDbtblDe.Rows(0).Item("FTSysUsrValue")
                        Case "1"
                            Math.Round(tSumSys, 2)
                        Case "2"
                            nResult = tSumSys + 0.1
                        Case "3"
                            nResult = tSumSys - 0.1
                    End Select

                    Return nResult

                Else

                    Return tSumSys

                End If
            End If

        Catch ex As Exception

        End Try
    End Function

    ''' <summary>
    ''' Write Textfile
    ''' </summary>
    ''' <param name="ptMsg">Data Write To Textfile</param>
    ''' <param name="ptDir">Path File</param>
    Private Sub C_SETxWriteExpPayment(ByVal ptMsg As String, ByVal ptDir As String, ByVal ptDate As String)
        Dim tPathFile As String = ""
        Dim oWrite As StreamWriter
        Dim tShopCode As String = AdaConfig.cConfig.oUrsDefault.tShopCode
        Try
            '   tPathFile = ptDir + "/" + "TSM_" + Now.ToString("yyyyMMdd") + "_BFC001_PAYMENT.txt"
            '     tPathFile = ptDir + "/" + "TSM_" + Now.ToString("yyyyMMdd") + "_" + tShopCode + "_PAYMENT.txt"
            tPathFile = ptDir + "/" + "TSM_" + ptDate + "_" + tShopCode + "_PAYMENT.txt"

            ' สร้าง Folder ถ้ายังไม่ได้สร้าง
            If (Not System.IO.Directory.Exists(ptDir)) Then
                System.IO.Directory.CreateDirectory(ptDir)
            End If

            ' สร้าง File ถ้ายังไม่ได้สร้าง
            If (Not File.Exists(tPathFile)) Then
                File.Create(tPathFile).Dispose()
            End If

            oWrite = New StreamWriter(tPathFile, True)
            oWrite.Write(ptMsg)
            oWrite.Close()

            C_CALxWriteLogEr("", tPathFile, oC_DbTblTemp.Rows.Count, "Success") 'ExportFile สำเร็จ
        Catch ex As Exception
            C_CALxWriteLogEr(ex.Message, tPathFile, oC_DbTblTemp.Rows.Count, "Expression")
        Finally
            tPathFile = Nothing
            oWrite = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Sava Log Error.
    ''' </summary>
    ''' <param name="ptMsg">Msg Error</param>
    ''' <param name="ptFullPath">Path Save File</param>
    ''' <param name="ptCountRowIns">Row Insert Error</param>
    Public Sub C_CALxWriteLogEr(ByVal ptMsg As String, ByVal ptFullPath As String,
            ByVal ptCountRowIns As String, ByVal ptType As String)

        Dim tC_FileLog As String = AdaConfig.cConfig.tAppPath & "\Log"
        Dim dDateNow As Date = Date.Now
        Dim oHDfile As New StringBuilder
        Dim tDesc As String = ""
        Dim oSQL As New StringBuilder
        Dim tDateTimeNow As String = ""
        Dim oDatabase As New cDatabaseLocal
        Dim tFileName As String = ""

        Try
            If ptType = "Expression" Then

                If Not IO.Directory.Exists(tC_FileLog) Then
                    IO.Directory.CreateDirectory(tC_FileLog)
                End If
                tC_FileLog &= "\" & "Log_Payment" & Format(Date.Now, "yyyyMMdd-HHmmss") & ".txt"

                If File.Exists(tC_FileLog) = False Then
                    Using oSw As StreamWriter = File.CreateText(tC_FileLog)
                        oSw.Close()
                    End Using

                    oHDfile.Append("[Export Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Export Time] " + Format(Date.Now, "HH: mm:ss") + vbTab + "")
                    oHDfile.Append("[Export By] " + Environment.MachineName + Environment.NewLine + Environment.NewLine + "")
                    oHDfile.Append("[Success]" + vbTab + "0" + Environment.NewLine + "")
                    oHDfile.Append("[Description]" + vbTab + ptMsg)

                    Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                        With oStreamWriter
                            .WriteLine(oHDfile)
                            .Flush()
                            .Close()
                        End With
                    End Using

                End If

                tDesc = "Success(" & ptCountRowIns & "),Failed(1)"
                tDateTimeNow = Format(Date.Now, "yyyy-MM-dd HH:mm:ss.fff")
                tFileName = Path.GetFileName(ptFullPath)
                oSQL.Clear()
                oSQL.AppendLine("INSERT INTO TLNKLog(uid,FDDateTime,FNType,FTFileName,FTDescription,FTRefer) VALUES")
                oSQL.AppendLine("('" + Guid.NewGuid.ToString + "','" + tDateTimeNow + "',2,'" + tFileName + "','" + tDesc + "','" + tC_FileLog + "')")
                If oSQL.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
                End If

            ElseIf ptType = "Success" Then

                If Not IO.Directory.Exists(tC_FileLog) Then
                    IO.Directory.CreateDirectory(tC_FileLog)
                End If
                tC_FileLog &= "\" & "Log_Payment" & Format(Date.Now, "yyyyMMdd-HHmmss") & ".txt"

                If File.Exists(tC_FileLog) = False Then
                    Using oSw As StreamWriter = File.CreateText(tC_FileLog)
                        oSw.Close()
                    End Using

                    oHDfile.Append("[Export Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Import Time] " + Format(Date.Now, "HH: mm:ss") + vbTab + "")
                    oHDfile.Append("[Export By] " + Environment.MachineName + Environment.NewLine + Environment.NewLine + "")
                    oHDfile.Append("[Success]" + vbTab + ptCountRowIns + Environment.NewLine + "")
                    oHDfile.Append("[Description]" + vbTab + ptMsg)

                    Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                        With oStreamWriter
                            .WriteLine(oHDfile)
                            .Flush()
                            .Close()
                        End With
                    End Using
                End If

                tDesc = "Success(" & ptCountRowIns & "),Failed(0)"
                tDateTimeNow = Format(Date.Now, "yyyy-MM-dd HH:mm:ss.fff")
                tFileName = Path.GetFileName(ptFullPath)
                oSQL.Clear()
                oSQL.AppendLine("INSERT INTO TLNKLog(uid,FDDateTime,FNType,FTFileName,FTDescription,FTRefer) VALUES")
                oSQL.AppendLine("('" + Guid.NewGuid.ToString + "','" + tDateTimeNow + "',2,'" + tFileName + "','" + tDesc + "','" + tC_FileLog + "')")
                If oSQL.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
                End If


            End If


        Catch ex As Exception
            MessageBox.Show("C_CALxWriteLogEr" + ex.Message)
        End Try
    End Sub


    Function C_SETtNumDigit(ByVal tNumber As Double) As String
        Try
            Dim tDigit As String = Math.Round(tNumber, 2)

            Return tDigit
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Class
