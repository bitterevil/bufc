﻿Imports System.IO

Public Class cExpense

    Property tC_FullPath As String = ""
    Property tC_PathFileLog As String = ""
    Private Const tC_StaSendEN As String = "Sent"
    Private Const tC_StaSendTH As String = "ส่งแล้ว"
    Private Const tC_StaUnSendEN As String = "Unsend"
    Private Const tC_StaUnSendTH As String = "ยังไม่ได้ส่ง"

    ''' <summary>
    ''' Create File Expense for Send to FTP
    ''' </summary>
    ''' <param name="poItem"></param>
    ''' <returns>Boolean Status create file</returns>
    Public Function C_CRTbCrateFileExpense(ByRef poItem As List(Of cExportTemplate.cItem), ByVal ptPlant As String, ByVal pnRunning As Integer) As Boolean
        Dim bPrcCrt As Boolean = True
        Dim oZGLINT001 As New List(Of cZGLINT001)
        Try
            If Not cCNVB.bVB_Auto Then wExpExpense.oW_BackgroudWord.ReportProgress(0)

            'Get Data
            oZGLINT001 = C_GEToDataZGLINT001(poItem, ptPlant)
            If Not cCNVB.bVB_Auto Then wExpExpense.oW_BackgroudWord.ReportProgress(20)

            'Create File
            bPrcCrt = C_SETbCreateFileXml(oZGLINT001, pnRunning)
            If Not cCNVB.bVB_Auto Then wExpExpense.oW_BackgroudWord.ReportProgress(40)

            'Upsert To Log
            'If bPrcCrt Then bPrcCrt = C_SAVbTempCreateFile(poItem) 'ย้ายไปฟังก์ชั่นสร้างไฟล์ เมื่อสร้าง 1 ไฟล์ให้ลง Log '*CH 12-10-2017
            If Not cCNVB.bVB_Auto Then wExpExpense.oW_BackgroudWord.ReportProgress(60)
        Catch ex As Exception
            bPrcCrt = False
        End Try
        Return bPrcCrt
    End Function

    ''' <summary>
    ''' Get Data Expense to Model
    ''' </summary>
    ''' <param name="poItem"></param>
    ''' <param name="ptPlant"></param>
    ''' <returns>List(Of cZGLINT001)</returns>
    Private Function C_GEToDataZGLINT001(ByRef poItem As List(Of cExportTemplate.cItem), ByVal ptPlant As String) As List(Of cZGLINT001)
        Dim oZGLINT001 As New List(Of cZGLINT001)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tLogCode As String = ""
        Dim tSeqNo As String = ""
        Try
            oSql.AppendLine("SELECT")
            oSql.AppendLine("	(REPLACE(STR(DAY(Ltx.FDDateIns), 2), SPACE(1), '0') + REPLACE(STR(MONTH(Ltx.FDDateIns), 2), SPACE(1), '0') + CONVERT(VARCHAR(4), YEAR(Ltx.FDDateIns))) AS BLDAT,")
            oSql.AppendLine("	(REPLACE(STR(DAY(Ltx.FDDateIns), 2), SPACE(1), '0') + REPLACE(STR(MONTH(Ltx.FDDateIns), 2), SPACE(1), '0') + CONVERT(VARCHAR(4), YEAR(Ltx.FDDateIns))) As BUDAT,")
            oSql.AppendLine("	Ltx.FTLtxRemark As XBLNR, ")
            oSql.AppendLine("	Rsn.FTRsnOthName As HKONT, Ltx.FCLtxAmt As WRBTR, Rsn.FTRsnRemark As MWSKZ, ")
            'oSql.AppendLine("	'" & ptBchMap & "' AS BSEG_WERKS, Rsn.FTRsnName + CONVERT(VARCHAR(18), Ltx.FCLtxAmt) + Rsn.FTRsnRemark AS BSEG_SGTXT")
            oSql.AppendLine("	'" & ptPlant & "' AS WERKS,Rsn.FTRsnName,Ltx.FCLtxAmt,")
            oSql.AppendLine("	CASE WHEN Rsn.FTRsnRemark = 'VX' THEN ' ไม่มีภาษี'")
            oSql.AppendLine("	WHEN Rsn.FTRsnRemark IN ('V7','D7') THEN ' รวมภาษี' ")
            oSql.AppendLine("	ELSE '' END AS FTRsnRemark,")
            oSql.AppendLine("	CASE WHEN Rsn.FTRsnRemark = 'VX' THEN 'NON'")
            oSql.AppendLine("	WHEN Rsn.FTRsnRemark IN ('V7','D7') THEN 'VAT' ")
            oSql.AppendLine("	ELSE '' END AS BKTXT,")
            oSql.AppendLine("	Ltx.FTLogCode,Ltx.FNLtxType,Ltx.FNLtxSeq,Ltx.FCLtxAmt,Ltx.FTRsnCode,Ltx.FTLtxRemark")
            oSql.AppendLine("FROM TPSTLoginTxn Ltx LEFT JOIN TLGTZGLINT001 Temp ON Ltx.FTLogCode = Temp.FTLogCode AND Ltx.FNLtxType = Temp.FNLtxType AND Ltx.FNLtxSeq = Temp.FNLtxSeq")
            oSql.AppendLine("	INNER JOIN TCNMReason Rsn ON Ltx.FTRsnCode = Rsn.FTRsnCode")
            oSql.AppendLine("WHERE Ltx.FTLogCode IN ({0})")
            oSql.AppendLine("	AND Ltx.FNLtxSeq IN ({1})")
            oSql.AppendLine("	AND Ltx.FNLtxType = 2")
            For Each oItem In poItem
                tLogCode &= "'" & oItem.tItem & "',"
                tSeqNo &= oItem.nSeqNo & ","
            Next
            tLogCode = Left(tLogCode, tLogCode.Length - 1)
            tSeqNo = Left(tSeqNo, tSeqNo.Length - 1)
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tLogCode, tSeqNo))
            Dim oExpense = From oRow As DataRow In oDbTbl.Rows
                           Select New cZGLINT001 With {
                              .BLDAT = oRow("BLDAT"),
                              .BUDAT = oRow("BUDAT"),
                              .XBLNR = oRow("XBLNR"),
                              .HKONT = oRow("HKONT"),
                              .WRBTR = oRow("WRBTR"),
                              .MWSKZ = oRow("MWSKZ"),
                              .WERKS = oRow("WERKS"),
                              .SGTXT = oRow("FTRsnName") & " " & Convert.ToInt32(oRow("FCLtxAmt")).ToString("#,##0") & " บาท", '& oRow("FTRsnRemark")
                              .BKTXT = oRow("BKTXT"),
                              .FTLogCode = oRow("FTLogCode"),
                              .FNLtxType = oRow("FNLtxType"),
                              .FNLtxSeq = oRow("FNLtxSeq"),
                              .FCLtxAmt = oRow("FCLtxAmt"),
                              .FTRsnCode = oRow("FTRsnCode"),
                              .FTLtxRemark = oRow("FTLtxRemark")
                           }
            If oExpense IsNot Nothing AndAlso oExpense.Count > 0 Then
                oZGLINT001 = oExpense.ToList
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return oZGLINT001
    End Function

    '''' <summary>
    '''' Create XML File (Set Model to XML File)
    '''' </summary>
    '''' <param name="poZGLINT001"></param>
    '''' <returns>Boolean Status Create File</returns>
    'Private Function C_SETbCreateFileXml(ByVal poZGLINT001 As List(Of cZGLINT001)) As Boolean
    '    Dim oXml As Xml.Serialization.XmlSerializer
    '    Dim oStreamWriter As StreamWriter
    '    Dim tPath As String = ""
    '    Dim bStaCrt As Boolean = True
    '    Try
    '        If poZGLINT001.Count > 0 Then
    '            tPath = Path.GetDirectoryName(tC_FullPath)
    '            If Not Directory.Exists(tPath) Then
    '                Directory.CreateDirectory(tPath)
    '            End If

    '            'Create Log File
    '            If IO.File.Exists(tC_FullPath) = False Then
    '                Using oStream As StreamWriter = File.CreateText(tC_FullPath)
    '                    oStream.Close()
    '                End Using
    '            End If

    '            oStreamWriter = File.AppendText(tC_FullPath)
    '            oXml = New Xml.Serialization.XmlSerializer(poZGLINT001.GetType)
    '            oXml.Serialize(oStreamWriter, poZGLINT001)
    '            oStreamWriter.WriteLine()
    '            oStreamWriter.Close()
    '        Else
    '            bStaCrt = False
    '        End If
    '    Catch ex As Exception
    '        bStaCrt = False
    '    Finally
    '        oXml = Nothing
    '        oStreamWriter = Nothing
    '    End Try
    '    Return bStaCrt
    'End Function
    '''' <summary>
    '''' Create XML File (Set Model to XML File)
    '''' </summary>
    '''' <param name="poZGLINT001"></param>
    '''' <returns>Boolean Status Create File</returns>
    'Private Function C_SETbCreateFileXml(ByVal poZGLINT001 As List(Of cZGLINT001), ByVal pnRunning As Integer) As Boolean
    '    Dim tPath As String = ""
    '    Dim tData As String = ""
    '    Dim bStaCrt As Boolean = True
    '    Try
    '        If poZGLINT001.Count > 0 Then
    '            tPath = Path.GetDirectoryName(tC_FullPath)
    '            If Not Directory.Exists(tPath) Then
    '                Directory.CreateDirectory(tPath)
    '            End If

    '            'Create Log File
    '            If IO.File.Exists(tC_FullPath) = False Then
    '                Using oStream As StreamWriter = File.CreateText(tC_FullPath)
    '                    oStream.Close()
    '                End Using
    '            End If

    '            'Set Data
    '            tData = "<FIDCCP01>" & vbCrLf
    '            tData &= "<IDOC>" & vbCrLf
    '            For Each oZGLINT001 In poZGLINT001
    '                tData &= vbTab & "<E1FIKPF>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BUKRS>" & oZGLINT001.BUKRS & "</BUKRS>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BLART>" & oZGLINT001.BLART & "</BLART>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BLDAT>" & oZGLINT001.BLDAT & "</BLDAT>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BUDAT>" & oZGLINT001.BUDAT & "</BUDAT>" & vbCrLf
    '                tData &= vbTab & vbTab & "<XBLNR>" & oZGLINT001.XBLNR & "</XBLNR>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BKTXT>" & oZGLINT001.BKTXT & "</BKTXT>" & vbCrLf
    '                tData &= vbTab & vbTab & "<WAERS>" & oZGLINT001.WAERS & "</WAERS>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BRNCH>" & oZGLINT001.BRNCH & "</BRNCH>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1FISEG>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<BUZEI>" & oZGLINT001.BUZEI & "</BUZEI>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<BSCHL>" & oZGLINT001.BSCHL & "</BSCHL>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<MWSKZ>" & oZGLINT001.MWSKZ & "</MWSKZ>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<WRBTR>" & oZGLINT001.WRBTR & "</WRBTR>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<SGTXT>" & oZGLINT001.SGTXT & "</SGTXT>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<HKONT>" & oZGLINT001.HKONT & "</HKONT>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<WERKS>" & oZGLINT001.WERKS & "</WERKS>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1FISEG>" & vbCrLf
    '                tData &= vbTab & "</E1FIKPF>" & vbCrLf
    '            Next
    '            tData &= "</IDOC>" & vbCrLf
    '            tData &= "</FIDCCP01>"

    '            'Write Data
    '            Using oStreamWriter As New StreamWriter(tC_FullPath, True, New System.Text.UTF8Encoding(False))
    '                With oStreamWriter
    '                    .WriteLine(tData)
    '                    .Flush()
    '                    .Close()
    '                End With
    '            End Using
    '        Else
    '            bStaCrt = False
    '        End If

    '        C_SAVbTempCreateFile(poItem)
    '    Catch ex As Exception
    '        bStaCrt = False
    '    Finally
    '    End Try
    '    Return bStaCrt
    'End Function
    ''' <summary>
    ''' Create XML File (Set Model to XML File)
    ''' </summary>
    ''' <param name="poZGLINT001"></param>
    ''' <param name="pnRunning">Running File</param>
    ''' <returns>Boolean Status Create File</returns>
    Private Function C_SETbCreateFileXml(ByVal poZGLINT001 As List(Of cZGLINT001), ByVal pnRunning As Integer) As Boolean
        Dim tPath As String = ""
        Dim tData As String = ""
        Dim tFileName As String = ""
        Dim nRunning As Integer = 0
        Dim oItem As New cZGLINT001
        Dim bStaCrt As Boolean = True
        Try
            If poZGLINT001.Count > 0 Then
                'Set Data
                nRunning = pnRunning
                For Each oZGLINT001 In poZGLINT001
                    tFileName = String.Format(tC_FullPath, nRunning.ToString.PadLeft(4, "0"))
                    tPath = Path.GetDirectoryName(tFileName)
                    If Not Directory.Exists(tPath) Then
                        Directory.CreateDirectory(tPath)
                    End If

                    'Create Log File
                    If IO.File.Exists(tFileName) = False Then
                        Using oStream As StreamWriter = File.CreateText(tFileName)
                            oStream.Close()
                        End Using
                    End If

                    tData = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf
                    tData &= "<ns0:MT_Expense xmlns:ns0=""urn:bigcamera.co.th:ecc:Expense"">" & vbCrLf
                    'tData = "<FIDCCP01>" & vbCrLf
                    'tData &= "<IDOC>" & vbCrLf
                    tData &= vbTab & "<E1FIKPF>" & vbCrLf
                    tData &= vbTab & vbTab & "<BUKRS>" & oZGLINT001.BUKRS & "</BUKRS>" & vbCrLf
                    tData &= vbTab & vbTab & "<BLART>" & oZGLINT001.BLART & "</BLART>" & vbCrLf
                    tData &= vbTab & vbTab & "<BLDAT>" & oZGLINT001.BLDAT & "</BLDAT>" & vbCrLf
                    tData &= vbTab & vbTab & "<BUDAT>" & oZGLINT001.BUDAT & "</BUDAT>" & vbCrLf
                    tData &= vbTab & vbTab & "<XBLNR>" & oZGLINT001.XBLNR & "</XBLNR>" & vbCrLf
                    tData &= vbTab & vbTab & "<BKTXT>" & oZGLINT001.BKTXT & "</BKTXT>" & vbCrLf
                    tData &= vbTab & vbTab & "<WAERS>" & oZGLINT001.WAERS & "</WAERS>" & vbCrLf
                    tData &= vbTab & vbTab & "<BRNCH>" & oZGLINT001.BRNCH & "</BRNCH>" & vbCrLf
                    tData &= vbTab & vbTab & "<E1FISEG>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<BUZEI>" & oZGLINT001.BUZEI & "</BUZEI>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<BSCHL>" & oZGLINT001.BSCHL & "</BSCHL>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<MWSKZ>" & oZGLINT001.MWSKZ & "</MWSKZ>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<WRBTR>" & oZGLINT001.WRBTR & "</WRBTR>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<SGTXT>" & oZGLINT001.SGTXT & "</SGTXT>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<HKONT>" & oZGLINT001.HKONT & "</HKONT>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<WERKS>" & oZGLINT001.WERKS & "</WERKS>" & vbCrLf
                    tData &= vbTab & vbTab & "</E1FISEG>" & vbCrLf
                    tData &= vbTab & "</E1FIKPF>" & vbCrLf
                    'tData &= "</IDOC>" & vbCrLf
                    'tData &= "</FIDCCP01>"
                    tData &= "</ns0:MT_Expense>"

                    'Write Data
                    Using oStreamWriter As New StreamWriter(tFileName, True, New System.Text.UTF8Encoding(False))
                        With oStreamWriter
                            .WriteLine(tData)
                            .Flush()
                            .Close()
                        End With
                    End Using

                    oItem = oZGLINT001
                    oItem.FTExpFileName = Path.GetFileName(tFileName)
                    oItem.FTExpRunning = nRunning.ToString.PadLeft(4, "0")
                    C_SAVbTempCreateFile(oItem)
                    nRunning += 1
                Next
            Else
                bStaCrt = False
            End If

        Catch ex As Exception
            bStaCrt = False
        Finally
            oItem = Nothing
        End Try
        Return bStaCrt
    End Function

    '''' <summary>
    '''' Upsert to TLGTZSDINT005
    '''' </summary>
    'Private Function C_SAVbTempCreateFile(ByRef poItem As List(Of cExportTemplate.cItem)) As Boolean
    '    Dim oSql As New System.Text.StringBuilder
    '    Dim oDatabase As New cDatabaseLocal
    '    Dim tFileName As String = ""
    '    Dim tLogCode As String = ""
    '    Dim bStaSav As Boolean = True
    '    Try
    '        'FileName
    '        tFileName = Path.GetFileName(tC_FullPath)

    '        'Conition
    '        For Each oItem In poItem
    '            tLogCode &= "'" & oItem.tItem & "',"
    '        Next
    '        tLogCode = Left(tLogCode, tLogCode.Length - 1)

    '        'Update
    '        oSql.AppendLine("UPDATE ZGLINT001 SET")
    '        oSql.AppendLine("	ZGLINT001.FTExpFileName = '" & tFileName & "',")
    '        oSql.AppendLine("	ZGLINT001.FTExpStaSend = '',")
    '        oSql.AppendLine("	ZGLINT001.FDDateUpd = CONVERT(DATE, GETDATE()),")
    '        oSql.AppendLine("	ZGLINT001.FTTimeUpd = CONVERT(VARCHAR(10), GETDATE(), 108),")
    '        oSql.AppendLine("	ZGLINT001.FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
    '        oSql.AppendLine("FROM TLGTZGLINT001 ZGLINT001 INNER JOIN TPSTLoginTxn Ltx ON ZGLINT001.FTLogCode = Ltx.FTLogCode AND ZGLINT001.FNLtxType = Ltx.FNLtxType AND ZGLINT001.FNLtxSeq = Ltx.FNLtxSeq")
    '        oSql.AppendLine("WHERE Ltx.FTLogCode IN ({0})")
    '        oSql.AppendLine("	AND Ltx.FNLtxType = 2")
    '        oDatabase.C_CALnExecuteNonQuery(String.Format(oSql.ToString, tLogCode))

    '        'Insert
    '        oSql = New System.Text.StringBuilder
    '        oSql.AppendLine("INSERT INTO TLGTZGLINT001 (")
    '        oSql.AppendLine("	FTLogCode,FNLtxType,FNLtxSeq,")
    '        oSql.AppendLine("	FCLtxAmt,FTRsnCode,FTLtxRemark,")
    '        oSql.AppendLine("	FTExpFileName,FTExpStaSend,")
    '        oSql.AppendLine("	FDDateUpd,FTTimeUpd,FTWhoUpd,")
    '        oSql.AppendLine("	FDDateIns,FTTimeIns,FTWhoIns)")
    '        oSql.AppendLine("SELECT Ltx.FTLogCode,Ltx.FNLtxType,Ltx.FNLtxSeq,")
    '        oSql.AppendLine("	Ltx.FCLtxAmt,Ltx.FTRsnCode,Ltx.FTLtxRemark,")
    '        oSql.AppendLine("	'" & tFileName & "','',")
    '        oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "',")
    '        oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "'")
    '        oSql.AppendLine("FROM TPSTLoginTxn Ltx LEFT JOIN TLGTZGLINT001 ZGLINT001 ON Ltx.FTLogCode = ZGLINT001.FTLogCode AND Ltx.FNLtxType = ZGLINT001.FNLtxType AND Ltx.FNLtxSeq = ZGLINT001.FNLtxSeq")
    '        oSql.AppendLine("WHERE Ltx.FTLogCode IN ({0})")
    '        oSql.AppendLine("	AND ISNULL(ZGLINT001.FTLogCode,'') = ''")
    '        oSql.AppendLine("	AND Ltx.FNLtxType = 2")
    '        oDatabase.C_CALnExecuteNonQuery(String.Format(oSql.ToString, tLogCode))

    '    Catch ex As Exception
    '        bStaSav = False
    '        Throw New Exception(ex.Message)
    '    Finally
    '        oSql = Nothing
    '        oDatabase = Nothing
    '    End Try
    '    Return bStaSav
    'End Function
    ''' <summary>
    ''' Save Log
    ''' </summary>
    ''' <param name="poItem">cZGLINT001</param>
    ''' <returns></returns>
    Private Function C_SAVbTempCreateFile(ByVal poItem As cZGLINT001) As Boolean
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim nAffect As Integer = 0
        Dim bStaSav As Boolean = True
        Try
            'Update
            oSql.AppendLine("UPDATE ZGLINT001 SET")
            oSql.AppendLine("	ZGLINT001.FTExpFileName = '" & poItem.FTExpFileName & "',")
            oSql.AppendLine("	ZGLINT001.FTExpStaSend = '',")
            oSql.AppendLine("	ZGLINT001.FTExpRunning = '" & poItem.FTExpRunning & "',")
            oSql.AppendLine("	ZGLINT001.FDDateUpd = CONVERT(DATE, GETDATE()),")
            oSql.AppendLine("	ZGLINT001.FTTimeUpd = CONVERT(VARCHAR(10), GETDATE(), 108),")
            oSql.AppendLine("	ZGLINT001.FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
            oSql.AppendLine("FROM TLGTZGLINT001 ZGLINT001 INNER JOIN TPSTLoginTxn Ltx ON ZGLINT001.FTLogCode = Ltx.FTLogCode AND ZGLINT001.FNLtxType = Ltx.FNLtxType AND ZGLINT001.FNLtxSeq = Ltx.FNLtxSeq")
            oSql.AppendLine("WHERE Ltx.FTLogCode = '" & poItem.FTLogCode & "'")
            oSql.AppendLine("	AND Ltx.FNLtxType = 2")
            oSql.AppendLine("	AND Ltx.FNLtxSeq = " & poItem.FNLtxSeq)
            nAffect = oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            'Insert
            If nAffect < 1 Then
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("INSERT INTO TLGTZGLINT001 (")
                oSql.AppendLine("	FTLogCode,FNLtxType,FNLtxSeq,")
                oSql.AppendLine("	FCLtxAmt,FTRsnCode,FTLtxRemark,")
                oSql.AppendLine("	FTExpFileName,FTExpStaSend,FTExpRunning,")
                oSql.AppendLine("	FDDateUpd,FTTimeUpd,FTWhoUpd,")
                oSql.AppendLine("	FDDateIns,FTTimeIns,FTWhoIns)")
                oSql.AppendLine("SELECT Ltx.FTLogCode,Ltx.FNLtxType,Ltx.FNLtxSeq,")
                oSql.AppendLine("	Ltx.FCLtxAmt,Ltx.FTRsnCode,Ltx.FTLtxRemark,")
                oSql.AppendLine("	'" & poItem.FTExpFileName & "','','" & poItem.FTExpRunning & "',")
                oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "',")
                oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "'")
                oSql.AppendLine("FROM TPSTLoginTxn Ltx LEFT JOIN TLGTZGLINT001 ZGLINT001 ON Ltx.FTLogCode = ZGLINT001.FTLogCode AND Ltx.FNLtxType = ZGLINT001.FNLtxType AND Ltx.FNLtxSeq = ZGLINT001.FNLtxSeq")
                oSql.AppendLine("WHERE Ltx.FTLogCode = '" & poItem.FTLogCode & "'")
                oSql.AppendLine("	AND ISNULL(ZGLINT001.FTLogCode,'') = ''")
                oSql.AppendLine("	AND Ltx.FNLtxType = 2")
                oSql.AppendLine("	AND Ltx.FNLtxSeq = " & poItem.FNLtxSeq)
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
            End If

        Catch ex As Exception
            bStaSav = False
            Throw New Exception(ex.Message)
        Finally
            oSql = Nothing
            oDatabase = Nothing
        End Try
        Return bStaSav
    End Function

    ''' <summary>
    ''' Send file to FTP
    ''' </summary>
    Public Function C_PRCbSendFile2FTP(ByRef poItem As List(Of cExportTemplate.cItem), ByRef Optional poPathFile As List(Of String) = Nothing)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tLogCode As String = ""
        Dim tSeqNo As String = ""
        Dim bStaSend As Boolean = True
        Try
            'Conition
            For Each oItem In poItem
                tLogCode &= "'" & oItem.tItem & "',"
                tSeqNo &= "'" & oItem.nSeqNo & "',"
            Next
            tLogCode = Left(tLogCode, tLogCode.Length - 1)
            tSeqNo = Left(tSeqNo, tSeqNo.Length - 1)

            'Get File Name
            oSql.AppendLine("SELECT DISTINCT FTExpFileName FROM TLGTZGLINT001")
            oSql.AppendLine("WHERE FTLogCode IN ({0}) AND FNLtxSeq IN ({1})")
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tLogCode, tSeqNo))

            If oDbTbl.Rows.Count > 0 Then
                For nRow = 0 To oDbTbl.Rows.Count - 1
                    If oDbTbl.Rows(nRow)("FTExpFileName").ToString <> "" Then
                        bStaSend = cCNSP.SP_PRCbSendFile2FTP(AdaConfig.cConfig.oConfigXml.tOutbox & "\Expense\" & oDbTbl.Rows(nRow)("FTExpFileName").ToString)
                        If bStaSend Then
                            'Update StaSend
                            oSql = New System.Text.StringBuilder
                            oSql.AppendLine("UPDATE TLGTZGLINT001 SET")
                            oSql.AppendLine("	FTExpStaSend = '1',")
                            oSql.AppendLine("	FDDateUpd = CONVERT(DATE, GETDATE()),")
                            oSql.AppendLine("	FTTimeUpd = CONVERT(VARCHAR(8), GETDATE(), 108),")
                            oSql.AppendLine("	FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
                            oSql.AppendLine("WHERE FTExpFileName = '" & oDbTbl.Rows(nRow)("FTExpFileName").ToString & "'")
                            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

                            'Return Path File
                            poPathFile.Add(AdaConfig.cConfig.oConfigXml.tOutbox & "\Expense\" & oDbTbl.Rows(nRow)("FTExpFileName").ToString)
                        Else
                            'Error Send to FTP
                            Exit For
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            bStaSend = False
            Throw New Exception(ex.Message)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return bStaSend
    End Function


    ''' <summary>
    ''' Save Log Create File And Send File
    ''' </summary>
    Public Sub C_SAVxLog(ByVal poItem As List(Of cExportTemplate.cItem), ByVal pdNow As DateTime)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tCmdWhe As String = ""
        Dim tSeqNo As String = ""
        Dim tPath As String = ""
        'Dim tLogName As String = ""
        Dim tStaSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaSendTH, tC_StaSendEN)
        Dim tStaUnSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaUnSendTH, tC_StaUnSendEN)
        Dim tFmtLineHead As String = "[Export Date] {0}" & vbTab & "[Export Time] {1}" & vbTab & "[Export By] {2}"
        Dim tFmtLineColumn As String = "[Branch Code]" & vbTab & "[Log Code]" & vbTab & "[Export File Name]" & vbTab & "[Status Send]"
        Dim tFmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}"
        Dim tFmtLine As String = "====================================================================================="
        Try
            oSql.AppendLine("SELECT LEFT(FTlogCode,3) FTBchCode,FTLogCode,FTExpFileName,")
            oSql.AppendLine("   CASE WHEN FTExpStaSend = '1' THEN '" & tStaSend & "' ELSE '" & tStaUnSend & "' END FTExpStaSend")
            oSql.AppendLine("FROM TLGTZGLINT001")
            oSql.AppendLine("WHERE FTLogCode IN ({0}) AND FNLtxSeq IN ({1})")
            For Each oItem In poItem
                tCmdWhe &= "'" & oItem.tItem & "',"
                tSeqNo &= "'" & oItem.nSeqNo & "',"
            Next
            tCmdWhe = Left(tCmdWhe, tCmdWhe.Length - 1)
            tSeqNo = Left(tSeqNo, tSeqNo.Length - 1)
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe, tSeqNo))

            If oDbTbl.Rows.Count > 0 Then
                tPath = Path.GetDirectoryName(tC_PathFileLog)
                If Not IO.Directory.Exists(tPath) Then
                    IO.Directory.CreateDirectory(tPath)
                End If

                'Create File Log
                'tLogName = String.Format(cLogTemplate.tC_FileMaster, tC_LogName)
                If File.Exists(tC_PathFileLog) = False Then
                    Using oStreamWriter As StreamWriter = File.CreateText(tC_PathFileLog)
                        oStreamWriter.Close()
                    End Using
                End If

                Using oStreamWriter As StreamWriter = File.AppendText(tC_PathFileLog)
                    With oStreamWriter
                        .WriteLine(tFmtLine)
                        .WriteLine(String.Format(tFmtLineHead, pdNow.ToString("dd/MM/yyyy"), pdNow.ToString("HH:mm:ss"), My.Computer.Name))
                        .WriteLine(tFmtLine)
                        .WriteLine(tFmtLineColumn)
                        .Flush()
                        For nRow = 0 To oDbTbl.Rows.Count - 1
                            .WriteLine(String.Format(tFmtLineData, oDbTbl.Rows(nRow)("FTBchCode"), oDbTbl.Rows(nRow)("FTLogCode"),
                                                     oDbTbl.Rows(nRow)("FTExpFileName"), oDbTbl.Rows(nRow)("FTExpStaSend")))
                            .Flush()
                        Next
                        .WriteLine("")
                        .Close()
                    End With
                End Using

                'Insert Log
                oDbTbl = New DataTable
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("SELECT DISTINCT FTExpFileName,CASE WHEN FTExpStaSend = '1' THEN '" & tStaSend & "' ELSE '" & tStaUnSend & "' END FTExpStaSend")
                oSql.AppendLine("FROM TLGTZGLINT001")
                oSql.AppendLine("WHERE FTLogCode IN ({0})")
                oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe))
                If oDbTbl.Rows.Count > 0 Then
                    For nRow = 0 To oDbTbl.Rows.Count - 1
                        oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 2, oDbTbl(nRow)("FTExpFileName"), oDbTbl.Rows(nRow)("FTExpStaSend"), tC_PathFileLog))
                    Next
                End If
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Sub
End Class
