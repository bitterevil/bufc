﻿Imports System.IO

Public Class cCashTnf

    Property tC_FullPath As String = ""
    Property tC_PathFileLog As String = ""
    Private Const tC_StaSendEN As String = "Sent"
    Private Const tC_StaSendTH As String = "ส่งแล้ว"
    Private Const tC_StaUnSendEN As String = "Unsend"
    Private Const tC_StaUnSendTH As String = "ยังไม่ได้ส่ง"

    ''' <summary>
    ''' Create File Expense for Send to FTP
    ''' </summary>
    ''' <param name="poItem"></param>
    ''' <returns>Boolean Status create file</returns>
    Public Function C_CRTbCrateFileExpense(ByRef poItem As List(Of cExportTemplate.cItem), ByVal ptBchMap As String, ByVal ptPlant As String, ByVal pnRunning As Integer) As Boolean
        Dim bPrcCrt As Boolean = True
        Dim oZGLINT002 As New List(Of cZGLINT002)
        Try
            If Not cCNVB.bVB_Auto Then wExpCashTnf.oW_BackgroudWord.ReportProgress(0)

            'Get Data
            oZGLINT002 = C_GEToDataZGLINT002(poItem, ptBchMap, ptPlant)
            If Not cCNVB.bVB_Auto Then wExpCashTnf.oW_BackgroudWord.ReportProgress(20)

            'Create File
            bPrcCrt = C_SETbCreateFileXml(oZGLINT002, pnRunning)
            If Not cCNVB.bVB_Auto Then wExpCashTnf.oW_BackgroudWord.ReportProgress(40)

            'Upsert To Log
            'If bPrcCrt Then bPrcCrt = C_SAVbTempCreateFile(poItem) 'ย้ายไปฟังก์ชั่นสร้างไฟล์ เมื่อสร้าง 1 ไฟล์ให้ลง Log '*CH 12-10-2017
            If Not cCNVB.bVB_Auto Then wExpCashTnf.oW_BackgroudWord.ReportProgress(60)
        Catch ex As Exception
            bPrcCrt = False
        End Try
        Return bPrcCrt
    End Function

    ''' <summary>
    ''' Get Data Expense to Model
    ''' </summary>
    ''' <param name="poItem"></param>
    ''' <param name="ptBchMap"></param>
    ''' <returns>List(Of cZGLINT001)</returns>
    Private Function C_GEToDataZGLINT002(ByRef poItem As List(Of cExportTemplate.cItem), ByVal ptBchMap As String, ByVal ptPlant As String) As List(Of cZGLINT002)
        Dim oZGLINT002 As New List(Of cZGLINT002)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tDocNo As String = ""
        Try
            oSql.AppendLine("SELECT (REPLACE(STR(DAY(Mib.FDMibDocDate), 2), SPACE(1), '0') + REPLACE(STR(MONTH(Mib.FDMibDocDate), 2), SPACE(1), '0') + CONVERT(VARCHAR(4), YEAR(Mib.FDMibDocDate))) As BLDAT,")
            oSql.AppendLine("	(REPLACE(STR(DAY(Mib.FDMibDocDate), 2), SPACE(1), '0') + REPLACE(STR(MONTH(Mib.FDMibDocDate), 2), SPACE(1), '0') + CONVERT(VARCHAR(4), YEAR(Mib.FDMibDocDate))) As BUDAT,")
            oSql.AppendLine("	(REPLACE(STR(DAY(Mib.FDDateSalStart), 2), SPACE(1), '0') + REPLACE(STR(MONTH(Mib.FDDateSalStart), 2), SPACE(1), '0') + CONVERT(VARCHAR(4), YEAR(Mib.FDDateSalStart))) As XBLNR,")
            oSql.AppendLine("	'" & ptBchMap & "' AS BRNCH,Abk.FTAbkRefCode AS HKONT,Mib.FCMibQty AS WRBTR,")
            'oSql.AppendLine("	'" & ptPlant & "' AS BSEG_WERKS, (Mib.FTBnkCode + CONVERT(VARCHAR, Mib.FCMibQty) + CONVERT(VARCHAR(10), Mib.FDMibDocDate, 103) + CONVERT(VARCHAR(10), Mib.FDDateSalStart, 103)) AS BSEG_SGTXT")
            oSql.AppendLine("	'" & ptPlant & "' AS WERKS,")
            oSql.AppendLine("	Mib.FTBnkCode,Mib.FCMibQty,Mib.FDMibDocDate,Mib.FDDateSalStart,Mib.FTMibDocNo")
            oSql.AppendLine("FROM TACTMnyInBnk Mib LEFT JOIN TLGTZGLINT002 Temp ON Mib.FTMibDocNo = Temp.FTMibDocNo")
            oSql.AppendLine("	INNER JOIN TCNMAcBook Abk ON Mib.FTAbkCode = Abk.FTAbkCode")
            oSql.AppendLine("WHERE Mib.FTMibDocNo IN ({0})")
            oSql.AppendLine("	AND Mib.FTMibStaSndBnk = '2'")
            For Each oItem In poItem
                tDocNo &= "'" & oItem.tItem & "',"
            Next
            tDocNo = Left(tDocNo, tDocNo.Length - 1)
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tDocNo))
            Dim oCash = From oRow As DataRow In oDbTbl.Rows
                        Select New cZGLINT002 With {
                            .BLDAT = oRow("BLDAT"),
                            .BUDAT = oRow("BUDAT"),
                            .XBLNR = oRow("XBLNR"),
                            .BRNCH = oRow("BRNCH"),
                            .HKONT = oRow("HKONT"),
                            .WRBTR = oRow("WRBTR"),
                            .WERKS = oRow("WERKS"),
                            .SGTXT = oRow("FTBnkCode") & " วันที่ขาย " & oRow("FDDateSalStart") & " = " & Convert.ToInt32(oRow("FCMibQty")).ToString("#,##0"),
                            .FTMibDocNo = oRow("FTMibDocNo")
                           }
            If oCash IsNot Nothing AndAlso oCash.Count > 0 Then
                oZGLINT002 = oCash.ToList
            End If

            '.SGTXT = "โอนเงินเข้าธนาคาร " & oRow("FTBnkCode") & " = " & Convert.ToInt32(oRow("FCMibQty")).ToString("#,##0") &
            '" วันที่ " & oRow("FDMibDocDate") & " จากยอดขายของวันที่ " & oRow("FDDateSalStart")

        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return oZGLINT002
    End Function

    '''' <summary>
    '''' Create XML File (Set Model to XML File)
    '''' </summary>
    '''' <param name="poZGLINT002"></param>
    '''' <returns>Boolean Status Create File</returns>
    'Private Function C_SETbCreateFileXml(ByVal poZGLINT002 As List(Of cZGLINT002)) As Boolean
    '    Dim oXml As Xml.Serialization.XmlSerializer
    '    Dim oStreamWriter As StreamWriter
    '    Dim tPath As String = ""
    '    Dim bStaCrt As Boolean = True
    '    Try
    '        If poZGLINT002.Count > 0 Then
    '            tPath = Path.GetDirectoryName(tC_FullPath)
    '            If Not Directory.Exists(tPath) Then
    '                Directory.CreateDirectory(tPath)
    '            End If

    '            'Create Log File
    '            If IO.File.Exists(tC_FullPath) = False Then
    '                Using oStream As StreamWriter = File.CreateText(tC_FullPath)
    '                    oStream.Close()
    '                End Using
    '            End If

    '            oStreamWriter = File.AppendText(tC_FullPath)
    '            oXml = New Xml.Serialization.XmlSerializer(poZGLINT002.GetType)
    '            oXml.Serialize(oStreamWriter, poZGLINT002)
    '            oStreamWriter.WriteLine()
    '            oStreamWriter.Close()
    '        Else
    '            bStaCrt = False
    '        End If
    '    Catch ex As Exception
    '        bStaCrt = False
    '    Finally
    '        oXml = Nothing
    '        oStreamWriter = Nothing
    '    End Try
    '    Return bStaCrt
    'End Function
    '''' <summary>
    '''' Create XML File (Set Model to XML File)
    '''' </summary>
    '''' <param name="poZGLINT002"></param>
    '''' <returns>Boolean Status Create File</returns>
    'Private Function C_SETbCreateFileXml(ByVal poZGLINT002 As List(Of cZGLINT002)) As Boolean
    '    Dim tPath As String = ""
    '    Dim tData As String = ""
    '    Dim bStaCrt As Boolean = True
    '    Try
    '        If poZGLINT002.Count > 0 Then
    '            tPath = Path.GetDirectoryName(tC_FullPath)
    '            If Not Directory.Exists(tPath) Then
    '                Directory.CreateDirectory(tPath)
    '            End If

    '            'Create Log File
    '            If IO.File.Exists(tC_FullPath) = False Then
    '                Using oStream As StreamWriter = File.CreateText(tC_FullPath)
    '                    oStream.Close()
    '                End Using
    '            End If

    '            'Set Data
    '            tData = "<FIDCCP01>" & vbCrLf
    '            tData &= "<IDOC>" & vbCrLf
    '            For Each oZGLINT002 In poZGLINT002
    '                tData &= vbTab & "<E1FIKPF>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BUKRS>" & oZGLINT002.BUKRS & "</BUKRS>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BLART>" & oZGLINT002.BLART & "</BLART>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BLDAT>" & oZGLINT002.BLDAT & "</BLDAT>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BUDAT>" & oZGLINT002.BUDAT & "</BUDAT>" & vbCrLf
    '                tData &= vbTab & vbTab & "<XBLNR>" & oZGLINT002.XBLNR & "</XBLNR>" & vbCrLf
    '                tData &= vbTab & vbTab & "<WAERS>" & oZGLINT002.WAERS & "</WAERS>" & vbCrLf
    '                tData &= vbTab & vbTab & "<BRNCH>" & oZGLINT002.BRNCH & "</BRNCH>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1FISEG>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<BUZEI>" & oZGLINT002.BUZEI & "</BUZEI>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<BSCHL>" & oZGLINT002.BSCHL & "</BSCHL>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<WRBTR>" & oZGLINT002.WRBTR & "</WRBTR>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<SGTXT>" & oZGLINT002.SGTXT & "</SGTXT>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<HKONT>" & oZGLINT002.HKONT & "</HKONT>" & vbCrLf
    '                tData &= vbTab & vbTab & vbTab & "<WERKS>" & oZGLINT002.WERKS & "</WERKS>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1FISEG>" & vbCrLf
    '                tData &= vbTab & "</E1FIKPF>" & vbCrLf
    '            Next
    '            tData &= "</IDOC>" & vbCrLf
    '            tData &= "</FIDCCP01>"

    '            'Write Data
    '            Using oStreamWriter As New StreamWriter(tC_FullPath, True, New System.Text.UTF8Encoding(False))
    '                With oStreamWriter
    '                    .WriteLine(tData)
    '                    .Flush()
    '                    .Close()
    '                End With
    '            End Using
    '        Else
    '            bStaCrt = False
    '        End If
    '    Catch ex As Exception
    '        bStaCrt = False
    '    Finally
    '    End Try
    '    Return bStaCrt
    'End Function
    ''' <summary>
    ''' Create XML File (Set Model to XML File)
    ''' </summary>
    ''' <param name="poZGLINT002"></param>
    ''' <returns>Boolean Status Create File</returns>
    Private Function C_SETbCreateFileXml(ByVal poZGLINT002 As List(Of cZGLINT002), ByVal pnRunning As Integer) As Boolean
        Dim tPath As String = ""
        Dim tData As String = ""
        Dim tFileName As String = ""
        Dim nRunning As Integer = 0
        Dim bStaCrt As Boolean = True
        Try
            If poZGLINT002.Count > 0 Then
                'Set Data
                nRunning = pnRunning
                For Each oZGLINT002 In poZGLINT002
                    tFileName = String.Format(tC_FullPath, nRunning.ToString.PadLeft(4, "0"))
                    tPath = Path.GetDirectoryName(tFileName)
                    If Not Directory.Exists(tPath) Then
                        Directory.CreateDirectory(tPath)
                    End If

                    'Create Log File
                    If IO.File.Exists(tFileName) = False Then
                        Using oStream As StreamWriter = File.CreateText(tFileName)
                            oStream.Close()
                        End Using
                    End If

                    tData = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf
                    tData &= "<ns0:MT_CashTransfer xmlns:ns0=""urn:bigcamera.co.th:ecc:CashTransfer"">" & vbCrLf
                    'tData = "<FIDCCP01>" & vbCrLf
                    'tData &= "<IDOC>" & vbCrLf
                    tData &= vbTab & "<E1FIKPF>" & vbCrLf
                    tData &= vbTab & vbTab & "<BUKRS>" & oZGLINT002.BUKRS & "</BUKRS>" & vbCrLf
                    tData &= vbTab & vbTab & "<BLART>" & oZGLINT002.BLART & "</BLART>" & vbCrLf
                    tData &= vbTab & vbTab & "<BLDAT>" & oZGLINT002.BLDAT & "</BLDAT>" & vbCrLf
                    tData &= vbTab & vbTab & "<BUDAT>" & oZGLINT002.BUDAT & "</BUDAT>" & vbCrLf
                    tData &= vbTab & vbTab & "<XBLNR>" & oZGLINT002.XBLNR & "</XBLNR>" & vbCrLf
                    tData &= vbTab & vbTab & "<WAERS>" & oZGLINT002.WAERS & "</WAERS>" & vbCrLf
                    tData &= vbTab & vbTab & "<BRNCH>" & oZGLINT002.BRNCH & "</BRNCH>" & vbCrLf
                    tData &= vbTab & vbTab & "<E1FISEG>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<BUZEI>" & oZGLINT002.BUZEI & "</BUZEI>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<BSCHL>" & oZGLINT002.BSCHL & "</BSCHL>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<WRBTR>" & oZGLINT002.WRBTR & "</WRBTR>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<SGTXT>" & oZGLINT002.SGTXT & "</SGTXT>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<HKONT>" & oZGLINT002.HKONT & "</HKONT>" & vbCrLf
                    tData &= vbTab & vbTab & vbTab & "<WERKS>" & oZGLINT002.WERKS & "</WERKS>" & vbCrLf
                    tData &= vbTab & vbTab & "</E1FISEG>" & vbCrLf
                    tData &= vbTab & "</E1FIKPF>" & vbCrLf
                    'tData &= "</IDOC>" & vbCrLf
                    'tData &= "</FIDCCP01>"
                    tData &= "</ns0:MT_CashTransfer>"

                    'Write Data
                    Using oStreamWriter As New StreamWriter(tFileName, True, New System.Text.UTF8Encoding(False))
                        With oStreamWriter
                            .WriteLine(tData)
                            .Flush()
                            .Close()
                        End With
                    End Using

                    oZGLINT002.FTExpFileName = Path.GetFileName(tFileName)
                    oZGLINT002.FTExpRunning = nRunning.ToString.PadLeft(4, "0")
                    C_SAVbTempCreateFile(oZGLINT002)
                    nRunning += 1
                Next
            Else
                bStaCrt = False
            End If
        Catch ex As Exception
            bStaCrt = False
        Finally
        End Try
        Return bStaCrt
    End Function

    '''' <summary>
    '''' Upsert to TLGTZSDINT005
    '''' </summary>
    'Private Function C_SAVbTempCreateFile(ByRef poItem As List(Of cExportTemplate.cItem)) As Boolean
    '    Dim oSql As New System.Text.StringBuilder
    '    Dim oDatabase As New cDatabaseLocal
    '    Dim tFileName As String = ""
    '    Dim tDocNo As String = ""
    '    Dim bStaSav As Boolean = True
    '    Try
    '        'FileName
    '        tFileName = Path.GetFileName(tC_FullPath)

    '        'Conition
    '        For Each oItem In poItem
    '            tDocNo &= "'" & oItem.tItem & "',"
    '        Next
    '        tDocNo = Left(tDocNo, tDocNo.Length - 1)

    '        'Update
    '        oSql.AppendLine("UPDATE ZGLINT002 SET")
    '        oSql.AppendLine("	ZGLINT002.FTExpFileName = '" & tFileName & "',")
    '        oSql.AppendLine("	ZGLINT002.FTExpStaSend = '',")
    '        oSql.AppendLine("	ZGLINT002.FDDateUpd = CONVERT(DATE, GETDATE()),")
    '        oSql.AppendLine("	ZGLINT002.FTTimeUpd = CONVERT(VARCHAR(10), GETDATE(), 108),")
    '        oSql.AppendLine("	ZGLINT002.FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
    '        oSql.AppendLine("FROM TLGTZGLINT002 ZGLINT002 INNER JOIN TACTMnyInBnk Mib ON ZGLINT002.FTMibDocNo = Mib.FTMibDocNo")
    '        oSql.AppendLine("WHERE Mib.FTMibDocNo IN ({0})")
    '        oSql.AppendLine("	AND Mib.FTMibStaSndBnk = '2'")
    '        oDatabase.C_CALnExecuteNonQuery(String.Format(oSql.ToString, tDocNo))

    '        'Insert
    '        oSql = New System.Text.StringBuilder
    '        oSql.AppendLine("INSERT INTO TLGTZGLINT002 (")
    '        oSql.AppendLine("	FTMibDocNo,FDMibDocDate,FDDateSalStart,")
    '        oSql.AppendLine("	FTBchCode,FTAbkRefCode,FCMibQty,")
    '        oSql.AppendLine("	FTBnkCode,FTExpFileName,FTExpStaSend,")
    '        oSql.AppendLine("	FDDateUpd,FTTimeUpd,FTWhoUpd,")
    '        oSql.AppendLine("	FDDateIns,FTTimeIns,FTWhoIns)")
    '        oSql.AppendLine("SELECT Mib.FTMibDocNo,Mib.FDMibDocDate,Mib.FDDateSalStart,")
    '        oSql.AppendLine("	Mib.FTBchCode,Abk.FTAbkRefCode,Mib.FCMibQty,")
    '        oSql.AppendLine("	Mib.FTBnkCode,'" & tFileName & "','',")
    '        oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "',")
    '        oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "'")
    '        oSql.AppendLine("FROM TACTMnyInBnk Mib LEFT JOIN TLGTZGLINT002 Temp ON Mib.FTMibDocNo = Temp.FTMibDocNo")
    '        oSql.AppendLine("	INNER JOIN TCNMAcBook Abk ON Mib.FTAbkCode = Abk.FTAbkCode")
    '        oSql.AppendLine("WHERE Mib.FTMibDocNo IN ({0})")
    '        oSql.AppendLine("	AND Mib.FTMibStaSndBnk = '2'")
    '        oSql.AppendLine("	AND ISNULL(Temp.FTMibDocNo,'') = ''")
    '        oDatabase.C_CALnExecuteNonQuery(String.Format(oSql.ToString, tDocNo))

    '    Catch ex As Exception
    '        bStaSav = False
    '        Throw New Exception(ex.Message)
    '    Finally
    '        oSql = Nothing
    '        oDatabase = Nothing
    '    End Try
    '    Return bStaSav
    'End Function
    ''' <summary>
    ''' Upsert to TLGTZSDINT005
    ''' </summary>
    Private Function C_SAVbTempCreateFile(ByRef poItem As cZGLINT002) As Boolean
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim nAffect As Integer = 0
        Dim bStaSav As Boolean = True
        Try
            'Update
            oSql.AppendLine("UPDATE ZGLINT002 SET")
            oSql.AppendLine("	ZGLINT002.FTExpFileName = '" & poItem.FTExpFileName & "',")
            oSql.AppendLine("	ZGLINT002.FTExpStaSend = '',")
            oSql.AppendLine("	ZGLINT002.FTExpRunning = '" & poItem.FTExpRunning & "',")
            oSql.AppendLine("	ZGLINT002.FDDateUpd = CONVERT(DATE, GETDATE()),")
            oSql.AppendLine("	ZGLINT002.FTTimeUpd = CONVERT(VARCHAR(10), GETDATE(), 108),")
            oSql.AppendLine("	ZGLINT002.FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
            oSql.AppendLine("FROM TLGTZGLINT002 ZGLINT002 INNER JOIN TACTMnyInBnk Mib ON ZGLINT002.FTMibDocNo = Mib.FTMibDocNo")
            oSql.AppendLine("WHERE Mib.FTMibDocNo = '" & poItem.FTMibDocNo & "'")
            oSql.AppendLine("	AND Mib.FTMibStaSndBnk = '2'")
            nAffect = oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

            'Insert
            If nAffect < 1 Then
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("INSERT INTO TLGTZGLINT002 (")
                oSql.AppendLine("	FTMibDocNo,FDMibDocDate,FDDateSalStart,")
                oSql.AppendLine("	FTBchCode,FTAbkRefCode,FCMibQty,")
                oSql.AppendLine("	FTBnkCode,FTExpFileName,FTExpStaSend,FTExpRunning,")
                oSql.AppendLine("	FDDateUpd,FTTimeUpd,FTWhoUpd,")
                oSql.AppendLine("	FDDateIns,FTTimeIns,FTWhoIns)")
                oSql.AppendLine("SELECT Mib.FTMibDocNo,Mib.FDMibDocDate,Mib.FDDateSalStart,")
                oSql.AppendLine("	Mib.FTBchCode,Abk.FTAbkRefCode,Mib.FCMibQty,")
                oSql.AppendLine("	Mib.FTBnkCode,'" & poItem.FTExpFileName & "','','" & poItem.FTExpRunning & "',")
                oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "',")
                oSql.AppendLine("	CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(10), GETDATE(), 108),'" & cCNVB.tVB_UserName & "'")
                oSql.AppendLine("FROM TACTMnyInBnk Mib LEFT JOIN TLGTZGLINT002 Temp ON Mib.FTMibDocNo = Temp.FTMibDocNo")
                oSql.AppendLine("	INNER JOIN TCNMAcBook Abk ON Mib.FTAbkCode = Abk.FTAbkCode")
                oSql.AppendLine("WHERE Mib.FTMibDocNo = '" & poItem.FTMibDocNo & "'")
                oSql.AppendLine("	AND Mib.FTMibStaSndBnk = '2'")
                oSql.AppendLine("	AND ISNULL(Temp.FTMibDocNo,'') = ''")
                nAffect = oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
            End If

        Catch ex As Exception
            bStaSav = False
            Throw New Exception(ex.Message)
        Finally
            oSql = Nothing
            oDatabase = Nothing
        End Try
        Return bStaSav
    End Function

    ''' <summary>
    ''' Send file to FTP
    ''' </summary>
    Public Function C_PRCbSendFile2FTP(ByRef poItem As List(Of cExportTemplate.cItem), ByRef Optional poPathFile As List(Of String) = Nothing)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tDocNo As String = ""
        Dim bStaSend As Boolean = True
        Try
            'Conition
            For Each oItem In poItem
                tDocNo &= "'" & oItem.tItem & "',"
            Next
            tDocNo = Left(tDocNo, tDocNo.Length - 1)

            'Get File Name
            oSql.AppendLine("SELECT DISTINCT FTExpFileName FROM TLGTZGLINT002")
            oSql.AppendLine("WHERE FTMibDocNo IN ({0})")
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tDocNo))

            If oDbTbl.Rows.Count > 0 Then
                For nRow = 0 To oDbTbl.Rows.Count - 1
                    If oDbTbl.Rows(nRow)("FTExpFileName").ToString <> "" Then
                        bStaSend = cCNSP.SP_PRCbSendFile2FTP(AdaConfig.cConfig.oConfigXml.tOutbox & "\Cash\" & oDbTbl.Rows(nRow)("FTExpFileName").ToString)
                        If bStaSend Then
                            'Update StaSend
                            oSql = New System.Text.StringBuilder
                            oSql.AppendLine("UPDATE TLGTZGLINT002 SET")
                            oSql.AppendLine("	FTExpStaSend = '1',")
                            oSql.AppendLine("	FDDateUpd = CONVERT(DATE, GETDATE()),")
                            oSql.AppendLine("	FTTimeUpd = CONVERT(VARCHAR(8), GETDATE(), 108),")
                            oSql.AppendLine("	FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
                            oSql.AppendLine("WHERE FTExpFileName = '" & oDbTbl.Rows(nRow)("FTExpFileName").ToString & "'")
                            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

                            'Return Path File
                            poPathFile.Add(AdaConfig.cConfig.oConfigXml.tOutbox & "\Cash\" & oDbTbl.Rows(nRow)("FTExpFileName").ToString)
                        Else
                            'Error Send to FTP
                            Exit For
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            bStaSend = False
            Throw New Exception(ex.Message)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return bStaSend
    End Function


    ''' <summary>
    ''' Save Log Create File And Send File
    ''' </summary>
    Public Sub C_SAVxLog(ByVal poItem As List(Of cExportTemplate.cItem), ByVal pdNow As DateTime)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tCmdWhe As String = ""
        Dim tPath As String = ""
        'Dim tLogName As String = ""
        Dim tStaSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaSendTH, tC_StaSendEN)
        Dim tStaUnSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaUnSendTH, tC_StaUnSendEN)
        Dim tFmtLineHead As String = "[Export Date] {0}" & vbTab & "[Export Time] {1}" & vbTab & "[Export By] {2}"
        Dim tFmtLineColumn As String = "[Branch Code]" & vbTab & "[DocNo.]" & vbTab & "[Export File Name]" & vbTab & "[Status Send]"
        Dim tFmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}"
        Dim tFmtLine As String = "====================================================================================="
        Try
            oSql.AppendLine("SELECT FTBchCode,FTMibDocNo,FTExpFileName,")
            oSql.AppendLine("   CASE WHEN FTExpStaSend = '1' THEN '" & tStaSend & "' ELSE '" & tStaUnSend & "' END FTExpStaSend")
            oSql.AppendLine("FROM TLGTZGLINT002")
            oSql.AppendLine("WHERE FTMibDocNo IN ({0})")
            For Each oItem In poItem
                tCmdWhe &= "'" & oItem.tItem & "',"
            Next
            tCmdWhe = Left(tCmdWhe, tCmdWhe.Length - 1)
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe))

            If oDbTbl.Rows.Count > 0 Then
                tPath = Path.GetDirectoryName(tC_PathFileLog)
                If Not IO.Directory.Exists(tPath) Then
                    IO.Directory.CreateDirectory(tPath)
                End If

                'Create File Log
                'tLogName = String.Format(cLogTemplate.tC_FileMaster, tC_LogName)
                If File.Exists(tC_PathFileLog) = False Then
                    Using oStreamWriter As StreamWriter = File.CreateText(tC_PathFileLog)
                        oStreamWriter.Close()
                    End Using
                End If

                Using oStreamWriter As StreamWriter = File.AppendText(tC_PathFileLog)
                    With oStreamWriter
                        .WriteLine(tFmtLine)
                        .WriteLine(String.Format(tFmtLineHead, pdNow.ToString("dd/MM/yyyy"), pdNow.ToString("HH:mm:ss"), My.Computer.Name))
                        .WriteLine(tFmtLine)
                        .WriteLine(tFmtLineColumn)
                        .Flush()
                        For nRow = 0 To oDbTbl.Rows.Count - 1
                            .WriteLine(String.Format(tFmtLineData, oDbTbl.Rows(nRow)("FTBchCode"), oDbTbl.Rows(nRow)("FTMibDocNo"),
                                                     oDbTbl.Rows(nRow)("FTExpFileName"), oDbTbl.Rows(nRow)("FTExpStaSend")))
                            .Flush()
                        Next
                        .WriteLine("")
                        .Close()
                    End With
                End Using

                'Insert Log
                oDbTbl = New DataTable
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("SELECT DISTINCT FTExpFileName,CASE WHEN FTExpStaSend = '1' THEN '" & tStaSend & "' ELSE '" & tStaUnSend & "' END FTExpStaSend")
                oSql.AppendLine("FROM TLGTZGLINT002")
                oSql.AppendLine("WHERE FTMibDocNo IN ({0})")
                oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe))
                If oDbTbl.Rows.Count > 0 Then
                    For nRow = 0 To oDbTbl.Rows.Count - 1
                        oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 2, oDbTbl(nRow)("FTExpFileName"), oDbTbl.Rows(nRow)("FTExpStaSend"), tC_PathFileLog))
                    Next
                End If
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Sub
End Class
