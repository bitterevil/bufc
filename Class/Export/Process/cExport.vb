﻿Imports System.Text
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.IO

Public Class cExport

    Private aC_TableName As String()

    Public Sub New(ParamArray paTableName As String())
        Try
            Dim aSql As New StringBuilder("")
            Dim nIndex As Integer = 0
            ReDim aC_TableName(paTableName.Length - 1)
            For Each tTableName In paTableName
                aC_TableName(nIndex) = tTableName

                'Clear ข้อมูลในตามราง Temp
                aSql.Clear()
                aSql.Append("IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & tTableName & "]') AND type in (N'U'))")
                aSql.Append("TRUNCATE TABLE [dbo].[" & tTableName & "]")
                If C_CALbEXECSQL(False, aSql.ToString) = False Then
                    Exit Sub
                End If
                nIndex += 1

            Next
            aSql = Nothing
            bC_Status = True
        Catch ex As Exception
            bC_Status = False
        End Try
    End Sub

    Private bC_Status As Boolean = False
    Public Property bStatus() As Boolean
        Get
            Return bC_Status
        End Get
        Set(ByVal value As Boolean)
            bC_Status = value
        End Set
    End Property

    'Query ข้อมูล
    Public Function C_CALbEXECSQL(pbUseSqlTransaction As Boolean, ParamArray aSql As String()) As Boolean
        Dim nLoop As Integer = 0
        Dim tErrCmd As String = ""
        C_CALbEXECSQL = False
        Using oConnSQL As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oConnSQL.Open()
            Select Case pbUseSqlTransaction
                Case True
                    Using oSQLCmd As New SqlCommand
                        Try
                            oSQLCmd.Connection = oConnSQL
                            oSQLCmd.Transaction = oConnSQL.BeginTransaction(IsolationLevel.ReadCommitted)
                            For Each tSql In aSql
                                tErrCmd = tSql
                                oSQLCmd.CommandText = tSql
                                oSQLCmd.ExecuteNonQuery()
                                nLoop += 1
                            Next
                            oSQLCmd.Transaction.Commit()
                            C_CALbEXECSQL = True
                        Catch ex As Exception
                            oSQLCmd.Transaction.Rollback()
                            Me.C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, "", ex.Message.Replace("'", ""), tErrCmd.Replace("'", "")))
                        End Try
                    End Using

                Case Else
                    Try
                        Using oSQLCmd As New SqlCommand
                            Try
                                oSQLCmd.Connection = oConnSQL
                                For Each tSql In aSql
                                    tErrCmd = tSql
                                    oSQLCmd.CommandText = tSql
                                    oSQLCmd.ExecuteNonQuery()
                                    nLoop += 1
                                Next
                                C_CALbEXECSQL = True
                            Catch ex As Exception
                            End Try
                        End Using
                        C_CALbEXECSQL = True
                    Catch ex As Exception
                        Me.C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, "", ex.Message.Replace("'", ""), tErrCmd.Replace("'", "")))
                    End Try

            End Select
        End Using
    End Function

    Public Function C_CALnEXECSQL(ParamArray aSql As String()) As Integer
        Dim tErrCmd As String = ""
        Dim nExeSql As Integer = 0
        C_CALnEXECSQL = nExeSql
        Try
            Using oConnSQL As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oConnSQL.Open()
                Using oSQLCmd As New SqlCommand
                    Try
                        oSQLCmd.Connection = oConnSQL
                        For Each tSql In aSql
                            tErrCmd = tSql
                            oSQLCmd.CommandText = tSql
                            nExeSql += oSQLCmd.ExecuteNonQuery()
                        Next
                        C_CALnEXECSQL = nExeSql
                    Catch ex As Exception
                    End Try
                End Using
            End Using
        Catch ex As Exception
            Me.C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, "", ex.Message.Replace("'", ""), tErrCmd.Replace("'", "")))
        End Try
    End Function

    'Write File
    Public Function C_CALxTempToFileText(ptFileName As String, poTableTemp As DataTable)

        'If File.Exists(ptFileName) Then
        '    File.Delete(ptFileName)
        'End If

        'ถ้าไม่มี Directory ให้สร้างขึ้นใหม่ '*CH 17-10-2014
        Dim tPath As String = ""
        Dim atPath As String() = ptFileName.Replace("/", "\").Split("\")
        Dim nLine As Integer = 0
        For nPath = 0 To atPath.Length - 2
            tPath &= atPath(nPath) & "\"
        Next
        tPath = Left(tPath, tPath.Length - 1)
        If Not IO.Directory.Exists(tPath) Then
            IO.Directory.CreateDirectory(tPath)
        End If

        'ถ้าไม่มีไฟล์ให้สร้างขึ้นใหม่ '*CH 17-10-2014
        If Not File.Exists(ptFileName) Then
            Using oStreamWriter As New StreamWriter(ptFileName, False, New System.Text.UTF8Encoding(False))
                oStreamWriter.Close()
            End Using
        End If

        Using oStreamWriter As New StreamWriter(ptFileName, True, New System.Text.UTF8Encoding(False))

            With oStreamWriter

                Dim tLine As String = ""
                For Each oDataRow As DataRow In poTableTemp.Rows
                    tLine = ""
                    For Each oCol As DataColumn In poTableTemp.Columns
                        If tLine.Length > 0 Then
                            tLine &= """|"""
                        End If

                        If oDataRow.Item(oCol).ToString.Length = 0 Then
                            Select Case oCol.DataType
                                Case GetType(String)
                                    tLine &= ""
                                Case GetType(DateTime)
                                    tLine &= ""
                                Case Else
                                    tLine &= "0"
                            End Select
                        Else
                            Select Case oCol.DataType
                                'Case GetType(String)
                                '    tLine &= ""
                                Case GetType(DateTime)
                                    tLine &= Format(oDataRow.Item(oCol), "yyyy-MM-dd")
                                Case Else
                                    tLine &= oDataRow.Item(oCol).ToString
                                    nLine += 1
                            End Select
                        End If
                    Next
                    'tLine = """" & tLine & """"
                    tLine = tLine.Replace(vbCrLf, " ").Replace(vbCr, " ").Replace(vbLf, " ") 'Replace Enter Remark '*CH 13-12-2014 [1.5702.1.23]
                    .WriteLine(tLine)
                    .Flush()
                Next





                .Close()
            End With

        End Using
        Return nLine
    End Function
    Public Sub C_WriterControl(ptFileTxt As String)
        Dim oDatabase As New cDatabaseLocal
        Dim tLine As String = ""
        Dim tFilename As String = ""
        Dim aFileName As New List(Of String)
        Dim aRecord As New List(Of String)
        Dim aSize As New List(Of String)
        Dim tSql As String = "SELECT * FROM TempList"
        Dim oDTCT = oDatabase.C_CALoExecuteReader(tSql)
        If oDTCT IsNot Nothing Then
            If oDTCT.Rows.Count > 0 Then
                For Each oItem In oDTCT.Rows
                    aFileName.Add(oItem("FTFileName"))
                    aRecord.Add(oItem("FTFileRecord"))
                    aSize.Add(oItem("FTFilesize"))
                Next
            End If
        End If
        If Not File.Exists(ptFileTxt) Then
            Using oStreamWriter As New StreamWriter(ptFileTxt, False, New System.Text.UTF8Encoding(False))
                oStreamWriter.Close()
            End Using
        End If

        Using oStreamWriter As New StreamWriter(ptFileTxt, True, New System.Text.UTF8Encoding(False))
            Try
                With oStreamWriter
                    For i = 0 To oDTCT.Rows.Count - 1
                        tLine &= aFileName(i) & "|" & aSize(i) & "|" & aRecord(i) & " "
                    Next
                    tLine = tLine.Replace(" ", vbNewLine) 'Replace Enter Remark '*CH 13-12-2014 [1.5702.1.23]
                    tLine = tLine & "*EOF*"
                    .WriteLine(tLine)
                    .Flush()
                    .Close()
                End With
            Catch ex As Exception
            End Try
        End Using

    End Sub
    Public Sub C_SETxDataControl(ptFileName As String, ptName As String)
        Dim tSize As String = ""
        Dim tFileName As String = ""
        Dim oRdr As New StreamReader(ptFileName, System.Text.Encoding.Default)
        Dim tLine As String = ""
        Dim tSql As String = ""
        Dim oReader As System.IO.FileInfo
        Dim nCnt As Integer = 0
        ' Dim atPath As String()


        ' atPath = ptFileName.Replace("/", "\").Split("\")
        'tFileName = atPath(7).Replace(".txt", "")
        tFileName = ptName.Replace(".txt", "")
        oReader = My.Computer.FileSystem.GetFileInfo(ptFileName)
        tSize = oReader.Length


        While True
            tLine = oRdr.ReadLine
            If tLine Is Nothing Then
                Exit While
            End If
            If tLine <> "*EOF*" Then
                tLine = tLine.Replace("|", " ")
                Dim aItemCol As String() = tLine.Split(" ")
                nCnt += 1
            End If
        End While
        Try
            tSql = ""
            tSql &= "INSERT INTO TempList "
            tSql &= "VALUES('" & tFileName & "','" & nCnt & "','" & tSize & "')"
            C_CALbEXECSQL(False, tSql.ToString)
        Catch ex As Exception
        End Try

    End Sub





    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    ''' <summary>
    ''' Insert History Export file (Cst,Sale,Pmt,EOD)
    ''' </summary>
    ''' <param name="ptTable">Table Name ex. TLNKExSaleHis</param>
    ''' <param name="ptField">Field Code ex. FTShdDocNo</param>
    ''' <param name="ptBchCode">Value BranchCode</param>
    ''' <param name="ptCode">Value Code ex. S1410001001-0000001</param>
    ''' <param name="ptWhoExp">Who Export ex. AdaLinkTCCAuto</param>
    ''' <remarks>*CH 05-12-2014</remarks>
    Public Sub C_INSxLogExport(ByVal ptTable As String, ByVal ptField As String, ByVal ptBchCode As String, ByVal ptCode As String, ByVal ptWhoExp As String)
        Dim oSql As New StringBuilder
        Dim tDateExp As String = Format(Now, "yyyy/MM/dd HH:mm:ss")
        Dim tDateUpd As String = ""
        Dim tDatetime As String = ""
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTblTime = oDatabase.C_CALoExecuteReader("select  convert(varchar(19),(FTTimeUpd),120) AS FTTimeUpd from TCNMCst where FTCSTCODE ='" & ptCode & "'")
        If oDbTblTime IsNot Nothing AndAlso oDbTblTime.Rows.Count > 0 Then tDatetime = oDbTblTime.Rows(0)("FTTimeUpd")
        tDateUpd = (Now.ToString("yyyy/MM/dd") + " " + tDatetime)

        Try
            Select Case ptTable.ToUpper
                Case "TLNKExCstHis".ToUpper
                    'Update
                    oSql.AppendLine("UPDATE " & ptTable & " SET")
                    oSql.AppendLine("FDDateIns='" & tDateUpd & "',")
                    oSql.AppendLine("FTWhoIns='" & ptWhoExp & "'")
                    oSql.AppendLine("WHERE FTBchCode='" & ptBchCode & "'")
                    oSql.AppendLine("AND " & ptField & "='" & ptCode & "'")
                    Dim nExeSql = C_CALnEXECSQL(oSql.ToString)

                    'Insert
                    If nExeSql < 1 Then
                        oSql = New StringBuilder
                        oSql.AppendLine("INSERT INTO " & ptTable)
                        ' oSql.AppendLine("(FTBchCode," & ptField & ",FDDateIns,FTWhoIns,FDDateUpd,FTWhoUpd)")
                        oSql.AppendLine("(FTBchCode," & ptField & ",FDDateIns,FTWhoIns)") '**PAN 2016-05-12 
                        oSql.AppendLine("SELECT '" & ptBchCode & "','" & ptCode & "',")
                        'oSql.AppendLine("'" & tDateExp & "','" & ptWhoExp & "','" & tDateUpd & "','" & ptWhoExp & "'")
                        oSql.AppendLine("'" & tDateExp & "','" & ptWhoExp & "'")
                    End If
                Case Else
                    oSql.AppendLine("INSERT INTO " & ptTable)
                    oSql.AppendLine("(FTBchCode," & ptField & ",FDDateIns,FTWhoIns)")
                    oSql.AppendLine("SELECT '" & ptBchCode & "','" & ptCode & "',")
                    oSql.AppendLine("'" & tDateExp & "','" & ptWhoExp & "'")
            End Select
            C_CALbEXECSQL(False, oSql.ToString)
        Catch ex As Exception

        End Try
    End Sub
    Public Sub C_INSxExport(ByVal ptTable As String, ByVal ptField As String, ByVal ptBchCode As String, ByVal ptCode As String, ByVal pdDate As Date, ByRef ptTime As String, ByVal ptWhoExp As String)
        Dim oSql As New StringBuilder
        Dim tDateExp As String = Format(Now, "yyyy/MM/dd HH:mm:ss")
        Dim tDateUpd As String = Format(Now, "yyyy/MM/dd HH:mm:ss")
        Dim tDateIns As String = Format(pdDate, "yyyy/MM/dd ") & ptTime
        Try
            Select Case ptTable.ToUpper
                Case "TLNKExCstHis".ToUpper
                    'Update
                    oSql.AppendLine("UPDATE " & ptTable & " SET")
                    oSql.AppendLine("FDDateIns='" & tDateIns & "',")
                    oSql.AppendLine("FTWhoIns='" & ptWhoExp & "'")
                    oSql.AppendLine("WHERE FTBchCode='" & ptBchCode & "'")
                    oSql.AppendLine("AND " & ptField & "='" & ptCode & "'")
                    Dim nExeSql = C_CALnEXECSQL(oSql.ToString)

                    'Insert
                    If nExeSql < 1 Then
                        oSql = New StringBuilder
                        oSql.AppendLine("INSERT INTO " & ptTable)
                        oSql.AppendLine("(FTBchCode," & ptField & ",FDDateIns,FTWhoIns)")
                        oSql.AppendLine("SELECT '" & ptBchCode & "','" & ptCode & "',")
                        oSql.AppendLine("Convert(varchar(19), '" & tDateIns & "', 120),'" & ptWhoExp & "'")

                    End If
                Case Else
                    oSql.AppendLine("INSERT INTO " & ptTable)
                    oSql.AppendLine("(FTBchCode," & ptField & ",FDDateIns,FTWhoIns)")
                    oSql.AppendLine("SELECT '" & ptBchCode & "','" & ptCode & "',")
                    oSql.AppendLine("Convert(varchar(19), '" & tDateIns & "', 120),'" & ptWhoExp & "'")
            End Select
            C_CALbEXECSQL(False, oSql.ToString)
        Catch ex As Exception

        End Try
    End Sub
End Class

