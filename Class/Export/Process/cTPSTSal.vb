﻿Imports System.IO
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Threading

Public Class cTPSTSal

    'Public Delegate Sub ProgressDelegate(pnValue As Integer, pnCount As Integer)
    'Public Event EventProgress As ProgressDelegate

    'For Sql
    Property tDateFrom As String = ""
    Property tDateTo As String = ""
    Property nMode As Integer = 0 '0:All,1:Interval,2:Select
    'Property tBchFrom As String = ""
    'Property tBchTo As String = ""
    Property tBchSelect As String = ""
    Property tDocFrom As String = ""
    Property tDocTo As String = ""
    Property tDocSelect As String = ""
    '

    Property tFullPath As String = ""
    Property tOutPath As String = ""
    Property tHDTable As String = ""
    Property tDTTable As String = ""
    Property tRCTable As String = ""
    Property tPDTable As String = ""
    Property tDCTable As String = ""
    Property tPathFileLog As String = ""
    Property tPathControl As String = ""

    Property tFileLog As String = ""

    'อาเรย์เก็บข้อมูลสาขา
    Private aBchCode As New List(Of String)
    'อาเรย์เก็บข้อมูลวันที่ขาย
    Private aSaleDate As New List(Of String)
    Private oDatabase As New cDatabaseLocal
    Private tSpr As String = AdaConfig.cConfig.tSeparator
    Private aFileExportHD As New List(Of cFileInfo)
    Private aFileExportDT As New List(Of cFileInfo)

    Private aFileContrlHD As New List(Of cListcontrol)
    Private aFileContrlDT As New List(Of cListcontrol)


    Private Const tC_ExpTbl As String = "TLNKExSaleHis"
    Private bWriteLogExp As Boolean = False
    Private nSeqLogExp As Integer = 0
    'Private tC_LogName As String = "ZSDINT005_" & Now.ToString("yyyyMMdd") & ".txt"
    Private Const tC_StaSendEN As String = "Sent"
    Private Const tC_StaSendTH As String = "ส่งแล้ว"
    Private Const tC_StaUnSendEN As String = "Unsend"
    Private Const tC_StaUnSendTH As String = "ยังไม่ได้ส่ง"

#Region "Element"
    Private Const tC_Tran As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<BEGINDATETIMESTAMP>{5}</BEGINDATETIMESTAMP>" & vbCrLf &
        vbTab & vbTab & "<ENDDATETIMESTAMP>{6}</ENDDATETIMESTAMP>" & vbCrLf &
        vbTab & vbTab & "<OPERATORID>{7}</OPERATORID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONCURRENCY>{8}</TRANSACTIONCURRENCY>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONCURRENCY_ISO>{9}</TRANSACTIONCURRENCY_ISO>" & vbCrLf &
        vbTab & vbTab & "<PARTNERID>{10}</PARTNERID>" & vbCrLf
    'vbTab & vbTab & "<ZREFERENCE0>{10}</ZREFERENCE0>" & vbCrLf &
    'vbTab & vbTab & "<ZREFERENCE1>{11}</ZREFERENCE1>" & vbCrLf &
    Private Const tC_Cust As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<CUSTOMERINFOTYPE>{5}</CUSTOMERINFOTYPE>" & vbCrLf &
        vbTab & vbTab & "<DATAELEMENTID>{6}</DATAELEMENTID>" & vbCrLf &
        vbTab & vbTab & "<DATAELEMENTVALUE>{7}</DATAELEMENTVALUE>" & vbCrLf
    Private Const tC_Retail As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<RETAILSEQUENCENUMBER>{5}</RETAILSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<RETAILTYPECODE>{6}</RETAILTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<ITEMIDQUALIFIER>{7}</ITEMIDQUALIFIER>" & vbCrLf &
        vbTab & vbTab & "<ITEMID>{8}</ITEMID>" & vbCrLf &
        vbTab & vbTab & "<RETAILQUANTITY>{9}</RETAILQUANTITY>" & vbCrLf &
        vbTab & vbTab & "<SALESUNITOFMEASURE>{10}</SALESUNITOFMEASURE>" & vbCrLf &
        vbTab & vbTab & "<SALESAMOUNT>{11}</SALESAMOUNT>" & vbCrLf &
        vbTab & vbTab & "<NORMALSALESAMOUNT>{12}</NORMALSALESAMOUNT>" & vbCrLf &
        vbTab & vbTab & "<SERIALNUMBER>{13}</SERIALNUMBER>" & vbCrLf &
        vbTab & vbTab & "<PROMOTIONID>{14}</PROMOTIONID>" & vbCrLf &
        vbTab & vbTab & "<ACTUALUNITPRICE>{15}</ACTUALUNITPRICE>" & vbCrLf &
        vbTab & vbTab & "<UNITS>{16}</UNITS>" & vbCrLf
    Private Const tC_Discount As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<RETAILSEQUENCENUMBER>{5}</RETAILSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<DISCOUNTSEQUENCENUMBER>{6}</DISCOUNTSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<DISCOUNTTYPECODE>{7}</DISCOUNTTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<REDUCTIONAMOUNT>{8}</REDUCTIONAMOUNT>" & vbCrLf
    Private Const tC_Commis As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<COMMISIONSEQUENCENUMBER>{5}</COMMISIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<COMMEMPLOYEEQUAL>{6}</COMMEMPLOYEEQUAL>" & vbCrLf &
        vbTab & vbTab & "<COMMISSIONEMPLOYEEID>{7}</COMMISSIONEMPLOYEEID>" & vbCrLf &
        vbTab & vbTab & "<COMMISSIONAMOUNT>{8}</COMMISSIONAMOUNT>" & vbCrLf
    Private Const tC_TranExt As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<FIELDGROUP>{5}</FIELDGROUP>" & vbCrLf &
        vbTab & vbTab & "<FIELDNAME>{6}</FIELDNAME>" & vbCrLf &
        vbTab & vbTab & "<FIELDVALUE>{7}</FIELDVALUE>" & vbCrLf
    Private Const tC_ItemExt As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<RETAILSEQUENCENUMBER>{5}</RETAILSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<FIELDGROUP>{6}</FIELDGROUP>" & vbCrLf &
        vbTab & vbTab & "<FIELDNAME>{7}</FIELDNAME>" & vbCrLf &
        vbTab & vbTab & "<FIELDVALUE>{8}</FIELDVALUE>" & vbCrLf
    Private Const tC_Tender As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<TENDERSEQUENCENUMBER>{5}</TENDERSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<TENDERTYPECODE>{6}</TENDERTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<TENDERAMOUNT>{7}</TENDERAMOUNT>" & vbCrLf &
        vbTab & vbTab & "<TENDERCURRENCY>{8}</TENDERCURRENCY>" & vbCrLf &
        vbTab & vbTab & "<TENDERID>{9}</TENDERID>" & vbCrLf &
        vbTab & vbTab & "<ACCOUNTNUMBER>{10}</ACCOUNTNUMBER>" & vbCrLf &
        vbTab & vbTab & "<REFERENCEID>{11}</REFERENCEID>" & vbCrLf
    Private Const tC_Credit As String =
        vbTab & vbTab & "<RETAILSTOREID>{0}</RETAILSTOREID>" & vbCrLf &
        vbTab & vbTab & "<BUSINESSDAYDATE>{1}</BUSINESSDAYDATE>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONTYPECODE>{2}</TRANSACTIONTYPECODE>" & vbCrLf &
        vbTab & vbTab & "<WORKSTATIONID>{3}</WORKSTATIONID>" & vbCrLf &
        vbTab & vbTab & "<TRANSACTIONSEQUENCENUMBER>{4}</TRANSACTIONSEQUENCENUMBER>" & vbCrLf &
        vbTab & vbTab & "<PAYMENTCARD>{5}</PAYMENTCARD>" & vbCrLf &
        vbTab & vbTab & "<CARDNUMBER>{6}</CARDNUMBER>" & vbCrLf &
        vbTab & vbTab & "<CARDNUMBERSUFFIX>{7}</CARDNUMBERSUFFIX>" & vbCrLf &
        vbTab & vbTab & "<CARDEXPIRATIONDATE>{8}</CARDEXPIRATIONDATE>" & vbCrLf &
        vbTab & vbTab & "<CARDHOLDERNAME>{9}</CARDHOLDERNAME>" & vbCrLf &
        vbTab & vbTab & "<ADJUDICATIONCODE>{10}</ADJUDICATIONCODE>" & vbCrLf &
        vbTab & vbTab & "<AUTHORIZINGTERMID>{11}</AUTHORIZINGTERMID>" & vbCrLf &
        vbTab & vbTab & "<AUTHORIZATIONDATETIME>{12}</AUTHORIZATIONDATETIME>" & vbCrLf &
        vbTab & vbTab & "<MEDIAISSUERID>{13}</MEDIAISSUERID>" & vbCrLf &
        vbTab & vbTab & "<AUTHORIZATIONCODE>{14}</AUTHORIZATIONCODE>" & vbCrLf &
        vbTab & vbTab & "<TENDERSEQUENCENUMBER>{15}</TENDERSEQUENCENUMBER>" & vbCrLf
#End Region

    'Process
    Public Function C_CALbProcess() As Boolean
        C_CALbProcess = True
        bWriteLogExp = False
        nSeqLogExp = 1
        Try
            'Thread.Sleep(200)
            'RaiseEvent EventProgress(20, 100)
            Dim oExportTable = New cExport()
            'Thread.Sleep(200)
            'RaiseEvent EventProgress(40, 100)
            aFileExportHD = New List(Of cFileInfo) 'New List *CH 31-10-2014
            aFileExportDT = New List(Of cFileInfo) 'New List *CH 31-10-2014
            aBchCode = New List(Of String)
            aSaleDate = New List(Of String)
            With oExportTable
                If .bStatus = True Then

                    Dim aSql As New List(Of String)
                    Dim nIndex As Integer = 0
                    Dim tFileNameHD As String = ""
                    Dim tFileNameDT As String = ""
                    Dim dDateFrom As Date
                    Dim dDateTo As Date
                    'Dim tWhere As String = " WHERE CONVERT(VARCHAR(10),FDDateIns,120)='{0}' AND "
                    Dim tBchWhere As String = " WHERE 1=1 "
                    Dim tDocWhere As String = ""
                    Dim tCompNo As String = ""
                    Dim tTxtPathHD As String = ""
                    Dim tTxtPathDT As String = ""
                    Dim tStrNameHD As String = ""
                    Dim tStrNameDT As String = ""

                    If IsDate(tDateFrom) And IsDate(tDateTo) Then
                        dDateFrom = CDate(tDateFrom)
                        dDateTo = CDate(tDateTo)
                    End If

                    'tBchWhere
                    If IsDate(tDateFrom) And IsDate(tDateTo) Then
                        tBchWhere &= "AND CONVERT(VARCHAR(10),FDShdDocDate,120) BETWEEN '" & Format(dDateFrom, "yyyy-MM-dd") & "' AND '" & Format(dDateTo, "yyyy-MM-dd") & "' "
                    End If

                    Select Case nMode
                        Case 1
                            'tBchWhere &= "AND FTBchCode BETWEEN '" & tBchFrom & "' AND '" & tBchTo & "'"
                            '*CH 05-12-2014
                            tBchWhere &= "AND FTShdDocNo BETWEEN '" & tDocFrom & "' AND '" & tDocTo & "'"
                            tDocWhere = "AND FTShdDocNo BETWEEN '" & tDocFrom & "' AND '" & tDocTo & "' "
                        Case 2
                            If tBchSelect <> "" Then tBchWhere &= "AND FTBchCode='" & tBchSelect & "'"
                            '*CH 05-12-2014
                            tBchWhere &= "AND FTShdDocNo='" & tDocSelect & "'"
                            tDocWhere = "AND FTShdDocNo='" & tDocSelect & "' "
                    End Select

                    'Thread.Sleep(200)
                    'RaiseEvent EventProgress(50, 100)

                    'เก็บรหัสเครื่อง '*CH 16-10-2014
                    cLog.C_CALxWriteLog("เก็บรหัสเครื่อง(CompNo)")
                    Dim tSqlComp As String = "SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END AS CompNo from TLNKMapping WHERE FTLNMCode='COMPNO'"
                    Dim oDTComp = oDatabase.C_CALoExecuteReader(tSqlComp)
                    If oDTComp IsNot Nothing Then
                        If oDTComp.Rows.Count > 0 Then
                            tCompNo = oDTComp.Rows(0).Item("CompNo")
                        End If
                    End If

                    cLog.C_CALxWriteLog("เก็บรหัสสาขา(BchCode)")
                    'เก็บรหัสสาขา
                    Dim tSqlBch As String = "SELECT DISTINCT FTBchCode FROM " & tHDTable & tBchWhere
                    Dim oDTBch = oDatabase.C_CALoExecuteReader(tSqlBch)
                    If oDTBch IsNot Nothing Then
                        If oDTBch.Rows.Count > 0 Then
                            For Each oItem In oDTBch.Rows
                                aBchCode.Add(oItem("FTBchCode"))
                            Next
                        End If
                    End If

                    cLog.C_CALxWriteLog("เก็บวันที่ขายจริง(DocDate)")
                    'เก็บวันที่ขาย จริง 
                    Dim nRow As Integer = 0
                    Dim tSqlSaleDate As String = "SELECT DISTINCT CONVERT(VARCHAR(10),FDShdDocDate,120) AS DocDate FROM " & tHDTable & tBchWhere
                    Dim oDTSalDocDate = oDatabase.C_CALoExecuteReader(tSqlSaleDate)
                    If oDTSalDocDate IsNot Nothing Then
                        If oDTSalDocDate.Rows.Count > 0 Then
                            nRow = oDTSalDocDate.Rows.Count
                            'Dim tSqlData As String = "SELECT Count (FTShdDocNo) FROM TPSTSalHD WHERE  FDShdDocDate = " & oDTSalDocDate.Select

                            For Each oItem In oDTSalDocDate.Rows
                                aSaleDate.Add(oItem("DocDate"))
                            Next
                        End If
                    End If

                    'Thread.Sleep(200)
                    'RaiseEvent EventProgress(60, 100)

                    'Check สาขา 
                    If aBchCode.Count < 1 Then
                            Return False
                        End If

                        'ไม่พบวันที่ขาย
                        If aSaleDate.Count = 0 Then
                            Return False
                        End If

                        'Thread.Sleep(200)
                        'RaiseEvent EventProgress(100, 100)

                        'คำนวน Propress Bar '*CH 19-12-2014
                        Dim nValPropress As Double = 100 / nRow
                        Dim nNextVal As Double = nValPropress
                        Dim nFixSubVal As Double = 0

                        Dim dDateNow As DateTime
                        Dim oDataTemp As DataTable = Nothing
                        For Each tSaleDate In aSaleDate
                            'Thread.Sleep(200)
                            'RaiseEvent EventProgress(10, 100)

                            nIndex = 0
                            'Thread.Sleep(600)
                            dDateNow = Date.Now

                            'Thread.Sleep(200)
                            'RaiseEvent EventProgress(30, 100)
                            Dim aDocNo As New List(Of String)
                            For Each tBchCode In aBchCode

                            cLog.C_CALxWriteLog("เก็บรหัสMapping(BchMapping)")
                            Dim tBchMapping As String = ""
                            Dim oRow() As DataRow = cCNVB.oDBBchMap.Select("FTLNMUsrValue='" & tBchCode & "'")
                            If oRow IsNot Nothing AndAlso oRow.Count > 0 Then
                                tBchMapping = oRow(0)("FTLNMDefValue")
                            End If

                            'For Export Auto '*CH 05-12-2014
                            Dim tNotSalDoc As String = ""
                            tNotSalDoc = "AND FTShdDocNo NOT IN (SELECT FTShdDocNo FROM " & tC_ExpTbl & " WHERE FTBchCode = '" & tBchMapping & "') "


                            'Thread.Sleep(200)
                            'RaiseEvent EventProgress(50, 100)

                            'ค้นค้าเอกสารที่ไม่สมบรูณ์ '*CH 06-12-2014
                            Dim oDbTblNotComd = oDatabase.C_CALoExecuteReader(C_GETtCmdCompleteSale(tSaleDate, tBchCode))
                            If oDbTblNotComd IsNot Nothing AndAlso oDbTblNotComd.Rows.Count > 0 Then
                                'Write Log Sale
                                C_CALbSaveLog(tSaleDate, oDbTblNotComd)

                                'If cCNVB.bVB_Auto Then Return False 'Auto ออกจากการ Export Sale '*CH 09-12-2014
                                If Not cCNVB.bVB_Auto Then
                                    'If cCNVB.bVB_DocNotComplete AndAlso cCNVB.tVB_MsgExportSale <> "" AndAlso Not cCNVB.bVB_StaCheckExpContinue Then
                                    '    cCNVB.bVB_StaCheckExpContinue = True
                                    '    If MessageBox.Show(IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, cCNVB.tVB_MsgExportSale.Split(";")(0), cCNVB.tVB_MsgExportSale.Split(";")(1)), _
                                    '                    IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, "การขาย", "Sale and Return"), _
                                    '                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = MsgBoxResult.No Then
                                    '        cCNVB.bVB_ExportContinue = False '*CH 09-12-2014 Status Export Error
                                    '        Return False
                                    '    Else
                                    '        cCNVB.bVB_ExportContinue = True
                                    '    End If
                                    'End If
                                Else
                                    'Write Log sms '*CH 26-12-2014
                                    If Not bWriteLogExp = False Then
                                        Dim tSql = "INSERT INTO TCNTLogSms "
                                        tSql &= "(FTLogCode, FTLogMsg, FTLogStaPrc, FTLogRmk, FDLogIns, FDLogUpd, FTLogMobile) "
                                        tSql &= "VALUES "
                                        tSql &= "('" & Format(Now, "yyyyMMddHHmmss") & "','" & cCNMS.tMS_CN113.Split(";")(0) & "','1','','" & Format(Now, "yyyy-MM-dd HH:mm:ss") & "','','')"
                                        oDatabase.C_CALnExecuteNonQuery(tSql)
                                    End If
                                End If

                                bWriteLogExp = True
                            End If
                            cLog.C_CALxWriteLog("เก็บเลขที่เอกสาร(DocNo)")
                            aDocNo = New List(Of String)
                            'เก็บเลขที่เอกสาร
                            Dim tSqlSalDocNo As String = "SELECT FTShdDocNo FROM " & tHDTable & " "
                            tSqlSalDocNo &= "WHERE CONVERT(VARCHAR(10),FDShdDocDate,120)='" & tSaleDate & "' "
                            tSqlSalDocNo &= "AND FTBchCode='" & tBchCode & "' "
                            tSqlSalDocNo &= tDocWhere
                            tSqlSalDocNo &= tNotSalDoc
                            'tSqlSalDocNo &= "AND FTShdDocNo  IN ( SELECT FTShdDocNo FROM TCNMTmpSalHD Where FTShdDocTime NOT IN ( SELECT FDDateUpd FROM TCNMTmpSalHD) AND FTstatus = '0')" 
                            'tSqlSalDocNo &= "AND FTShdDocNo NOT IN (" & C_GETtCmdCompleteSale(tSaleDate, "FTShdDocNo") & ") "
                            'tSqlSalDocNo &= "AND FTShdAEType IN ('1','9',NULL) "
                            tSqlSalDocNo &= "AND FTShdDocType IN ('1','9',NULL) "
                            tSqlSalDocNo &= "ORDER BY FTShdDocTime"
                            Dim oDTSalDocNo = oDatabase.C_CALoExecuteReader(tSqlSalDocNo)
                            If oDTSalDocNo IsNot Nothing Then
                                If oDTSalDocNo.Rows.Count > 0 Then
                                    nRow = oDTSalDocNo.Rows.Count
                                    For Each oItem In oDTSalDocNo.Rows
                                        aDocNo.Add(oItem("FTShdDocNo"))
                                    Next
                                Else
                                    aDocNo = New List(Of String)
                                End If
                            End If

                            Dim oFileInfoHD As cFileInfo = Nothing 'ประกาศไว้เริ่มต้น *CH 19-10-2014
                            oFileInfoHD = New cFileInfo
                            ' oFileInfo.tPathFile = String.Format(tFullPath, tCompNo & tBchCode, tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))
                            oFileInfoHD.tPathFile = String.Format(tFullPath, "MMS", "HD", tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))
                            cCNVB.tVB_PathControl = String.Format(tPathControl, "MMS", tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))
                            Dim oFileInfoDT As cFileInfo = Nothing 'ประกาศไว้เริ่มต้น *CH 19-10-2014
                            oFileInfoDT = New cFileInfo
                            ' oFileInfo.tPathFile = String.Format(tFullPath, tCompNo & tBchCode, tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))
                            oFileInfoDT.tPathFile = String.Format(tFullPath, "MMS", "DT", tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))

                            'oFileInfo.tBchCode = tCompNo & tBchCode
                            oFileInfoHD.tBchCode = tBchMapping ' PAN 2016-07-01 Mapping BchCode
                            'คำนวน Propress Bar '*CH 19-12-2014
                            Dim nSubPropress As Double = nNextVal / nRow
                            Dim nNextSubVal As Double = nSubPropress
                            nSubPropress += nFixSubVal
                            nFixSubVal += nNextVal
                            Dim oFileInfoContrlHD As cListcontrol = Nothing
                            oFileInfoContrlHD = New cListcontrol

                            Dim oFileInfoContrlDT As cListcontrol = Nothing
                            oFileInfoContrlDT = New cListcontrol
                            C_CRTxCreateTempList()

                            For Each tDocNo In aDocNo

                                '*CH 19-12-2014
                                'Thread.Sleep(200)
                                If nSubPropress > 100 Then nSubPropress = 100
                                If Not cCNVB.bVB_Auto Then wExports.oW_BackgroudWord.ReportProgress(nSubPropress)
                                nSubPropress += nNextSubVal
                                ''หา Rcvcode = 011
                                'Dim tRcvCode As String = ""
                                Dim tSql As String = ""
                                Dim tSqlDT As String = ""
                                Dim tSize As String = ""

                                cLog.C_CALxWriteLog("C_GETtCmdHead")
                                tSql = C_GETtCmdHead(tDocNo, tCompNo, tBchMapping)
                                ' tSql &= "*EOF*"
                                tSqlDT = C_GETtCmdDT(tDocNo, tCompNo, tBchMapping)
                                'tSqlDT &= "*EOF*"

                                'เขียนไฟล์ HEAD,ITEM,TENDER,TAX,DISCOUNT,LOYALTY_HEAD,LOYALTY_ITEM '*CH 16-10-2014
                                'Dim tSql As String = C_GETtCmdHead(tDocNo, tCompNo, tBchCode)
                                oDataTemp = oDatabase.C_CALoExecuteReader(tSql)
                                If Not oDataTemp Is Nothing Then
                                    If oDataTemp.Rows.Count > 0 Then

                                        'Set FileName By DateTime
                                        'tFileName = String.Format(tFullPath, tCompNo & tBchCode, tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))
                                        cLog.C_CALxWriteLog("GetFileName")
                                        tFileNameHD = String.Format(tFullPath, "MMS", "HD", tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))
                                        cLog.C_CALxWriteLog("Temp To File Text")


                                        'Temp To File Text
                                        .C_CALxTempToFileText(tFileNameHD, oDataTemp)


                                        If oFileInfoHD IsNot Nothing Then
                                            oFileInfoHD.nItem = oDataTemp.Rows.Count
                                            aFileExportHD.Add(oFileInfoHD)
                                            oFileInfoHD = Nothing
                                        End If

                                        'Insert Log
                                        .C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, tBchMapping, Format(dDateNow, "dd/MM/yyyy") & " " & tHDTable & " (" & oDataTemp.Rows.Count & ")" & " " & tDocNo, tFileNameHD.Replace(".tmp", ".txt")))

                                        '.C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, tBchCode, Format(dDateNow, "dd/MM/yyyy") & " " & tHDTable & " (" & oItemFile.nItem & ")", tXmlPath))

                                        nIndex += 1
                                    End If
                                End If

                                '------------------------------------------
                                oDataTemp = oDatabase.C_CALoExecuteReader(tSqlDT)
                                If Not oDataTemp Is Nothing Then
                                    If oDataTemp.Rows.Count > 0 Then

                                        'Set FileName By DateTime
                                        'tFileName = String.Format(tFullPath, tCompNo & tBchCode, tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))
                                        cLog.C_CALxWriteLog("GetFileName")
                                        tFileNameDT = String.Format(tFullPath, "MMS", "DT", tSaleDate.Replace("-", "") & Format(dDateNow, "HHmmss"))

                                        cLog.C_CALxWriteLog("Temp To File Text")
                                        'Temp To File Text
                                        .C_CALxTempToFileText(tFileNameDT, oDataTemp)

                                        '
                                        If oFileInfoDT IsNot Nothing Then
                                            oFileInfoDT.nItem = oDataTemp.Rows.Count
                                            aFileExportDT.Add(oFileInfoDT)
                                            oFileInfoDT = Nothing
                                        End If

                                        .C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, tBchMapping, Format(dDateNow, "dd/MM/yyyy") & " " & tDTTable & " (" & oDataTemp.Rows.Count & ")" & " " & tDocNo, tFileNameDT.Replace(".tmp", ".txt")))
                                        'Insert Log
                                        ' .C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, tBchCode, Format(tSaleDate, "dd/MM/yyyy") & tHDTable & " (" & oDataTemp.Rows.Count & ")", tFileName.Replace(".tmp", ".txt")))
                                        nIndex += 1
                                        '.C_WriterControl(tFileName, cCNVB.nVB_Line)

                                    End If
                                End If
                                '    Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                                '        oSQLConn.Open()
                                '        Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                                '        Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                                '        While oSQLRdWhe.Read()
                                '            tDesc &= oSQLRdWhe(0)
                                '        End While
                                '    End Using

                                'Next

                                'If File.Exists(tFileLog) = False Then tFileLog = ""
                                'oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 1, Path.GetFileName(tFullPath), tDesc, tFileLog))
                                'aSqlB4.Clear()




                                'Insert Log Export Sale '*CH 05-12-2014
                                cLog.C_CALxWriteLog("C_INSxLogExport")
                                '.C_INSxLogExport(tC_ExpTbl, "FTShdDocNo", tBchCode, tDocNo, "AdaLinkPTG" & IIf(cCNVB.bVB_Auto, "Auto", "Manual"))
                                '.C_INSxLogExport("TLNKLOG", "FTShdDocNo", tBchCode, tDocNo, "AdaLinkPTG" & IIf(cCNVB.bVB_Auto, "Auto", "Manual"))
                                .C_INSxLogExport(tC_ExpTbl, "FTShdDocNo", tBchMapping, tDocNo, "AdaLinkPTG" & "Manual")  'PAN 2017-04-12 แก้ไขตามPTG  Insert ลง SaleHis
                            Next
                        Next

                            '*CH 19-12-2014
                            nValPropress += nNextVal

                        Next

                    'Thread.Sleep(200)
                    'RaiseEvent EventProgress(80, 100)


                    For Each oItemFile In aFileExportHD
                        'tFileName = Path.GetFileName(oItemFile.tPathFile).Replace(".tmp", ".txt")
                        'My.Computer.FileSystem.RenameFile(oItemFile.tPathFile, tFileName)
                        cLog.C_CALxWriteLog("FileNameTemp")
                        Dim tFileExport As String = oItemFile.tPathFile.ToString
                        Dim oRdr = New StreamReader(tFileExport, System.Text.Encoding.Default)
                        Dim line As String
                        ' Read the stream to a string and write the string to the console.
                        line = oRdr.ReadToEnd()
                        Console.WriteLine(line)
                        oRdr.Dispose()
                        oRdr.Close()
                        oRdr = Nothing

                        Dim tData As String = line & "*EOF*"
                        Dim tResult As String
                        tResult = Path.GetFileName(tFileExport)
                        'FileCopy(tFileNameHD, tResult)


                        tStrNameHD = tResult.Replace(".tmp", ".xml")
                        tTxtPathHD = tOutPath & "\" & tStrNameHD
                        If Not IO.Directory.Exists(tOutPath) Then
                            IO.Directory.CreateDirectory(tOutPath)
                        End If

                        Using oStreamWriter As New StreamWriter(tTxtPathHD, True, System.Text.Encoding.Default)
                            With oStreamWriter
                                .WriteLine(tData)
                                .Flush()
                                .Close()
                            End With
                        End Using

                        If File.Exists(tFileNameHD) Then
                            File.Delete(tFileNameHD)
                        End If
                        cLog.C_CALxWriteLog("Insert Log")
                    Next




                    For Each oItemFile In aFileExportDT
                        'tFileName = Path.GetFileName(oItemFile.tPathFile).Replace(".tmp", ".txt")
                        'My.Computer.FileSystem.RenameFile(oItemFile.tPathFile, tFileName)

                        Dim tFileExport As String = oItemFile.tPathFile.ToString
                        Dim oRdr = New StreamReader(tFileExport, System.Text.Encoding.Default)
                        Dim line As String
                        ' Read the stream to a string and write the string to the console.
                        line = oRdr.ReadToEnd()
                        Console.WriteLine(line)
                        oRdr.Dispose()
                        oRdr.Close()
                        oRdr = Nothing

                        Dim tData As String = line & "*EOF*"
                        Dim tResult As String
                        tResult = Path.GetFileName(tFileExport)
                        'FileCopy(oItemFile.tPathFile, tResult)
                        'If File.Exists(oItemFile.tPathFile) Then
                        '    File.Delete(oItemFile.tPathFile)
                        'End If

                        tStrNameDT = tResult.Replace(".tmp", ".txt")



                        tTxtPathDT = tOutPath & "\" & tStrNameDT
                        If Not IO.Directory.Exists(tOutPath) Then
                            IO.Directory.CreateDirectory(tOutPath)
                        End If

                        Using oStreamWriter As New StreamWriter(tTxtPathDT, True, System.Text.Encoding.Default)
                            With oStreamWriter
                                .WriteLine(tData)
                                .Flush()
                                .Close()
                            End With
                        End Using

                        If File.Exists(tFileNameDT) Then
                            File.Delete(tFileNameDT)
                        End If

                        '    cLog.C_CALxWriteLog("FileNameTemp")
                        '    Dim tFileNameTemp = Path.GetFileName(oItemFile.tPathFile)
                        '    Dim tPathFile = String.Format(tOutPath, oItemFile.tBchCode)
                        '    If Not IO.Directory.Exists(tPathFile) Then
                        '        IO.Directory.CreateDirectory(tPathFile)
                        '    End If
                        '    tPathFile &= "\" & tFileNameTemp
                        '    FileCopy(oItemFile.tPathFile, tPathFile)
                        '    If File.Exists(oItemFile.tPathFile) Then
                        '        File.Delete(oItemFile.tPathFile)
                        '    End If

                        '    tFileNameTemp = Path.GetFileName(tPathFile).Replace(".tmp", ".txt")
                        '    My.Computer.FileSystem.RenameFile(tPathFile, tFileNameTemp)
                        '    cLog.C_CALxWriteLog("Insert Log")
                        '    'Insert Log
                        '    .C_CALbEXECSQL(False, String.Format(cApp.tSQLCmdLog, 2, oItemFile.tBchCode, Format(dDateNow, "dd/MM/yyyy") & " " & tHDTable & " (" & oItemFile.nItem & ")", tPathFile.Replace(".tmp", ".txt")))
                        'Next


                        ' .C_WriterControl(tTxtPath, cCNVB.tVB_PathControl, aFileContrlDT)

                    Next
                    oDatabase.C_CALnExecuteNonQuery("DELETE FROM TempList")

                    .C_SETxDataControl(tTxtPathHD, tStrNameHD)
                    .C_SETxDataControl(tTxtPathDT, tStrNameDT)







                    .C_WriterControl(cCNVB.tVB_PathControl)
                    Dim tFileCT As String
                    tFileCT = Path.GetFileName(cCNVB.tVB_PathControl)
                    Dim tNameCT As String = tFileCT.Replace(".tmp", ".txt")
                    Dim tTxtPathCT = tOutPath & "\" & tNameCT
                    If Not IO.Directory.Exists(tOutPath) Then
                        IO.Directory.CreateDirectory(tOutPath)
                    End If
                    FileCopy(cCNVB.tVB_PathControl, tTxtPathCT)
                    If File.Exists(cCNVB.tVB_PathControl) Then
                        File.Delete(cCNVB.tVB_PathControl)
                    End If




                    aBchCode.Clear()
                    aSaleDate.Clear()
                End If
            End With

        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function C_GETtCmdCompleteSale(ByVal ptDocDate As String, ByVal ptBchCode As String, Optional ByVal ptFieldSel As String = "") As String
        Dim oSql As System.Text.StringBuilder
        'View Sale '*CH 26-12-2014 '*Crate by P'Tee
        oSql = New System.Text.StringBuilder
        oSql.AppendLine("If EXISTS (Select * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].VLnk_HD') AND OBJECTPROPERTY(id, N'IsView') = 1)")
                                        oSql.AppendLine("   DROP VIEW [dbo].VLnk_HD")
        oSql.AppendLine("IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].VLnk_DT') AND OBJECTPROPERTY(id, N'IsView') = 1)")
        oSql.AppendLine("   DROP VIEW [dbo].VLnk_DT")
        oSql.AppendLine("IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].VLnk_RC') AND OBJECTPROPERTY(id, N'IsView') = 1)")
        oSql.AppendLine("   DROP VIEW [dbo].VLnk_RC")
        oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

        oSql = New System.Text.StringBuilder
        oSql.AppendLine("CREATE View VLnk_HD AS")
        oSql.AppendLine("   SELECT FTShdDocNo,FDShdDocDate,FTLogCode, SUM(CASE WHEN FTShdDocType='1' THEN FCShdGrand ELSE -FCShdGrand END )AS HD_DataTotal ,SUM(FCShdRnd) AS HD_DataRnd")
        oSql.AppendLine("   ,FTShdDocType AS HD_DocType")
        oSql.AppendLine("   FROM TPSTSalHD WHERE FTBchCode= '" & ptBchCode & "'")
        oSql.AppendLine("   GROUP BY FTShdDocNo,FDShdDocDate,FTLogCode,FTShdDocType")
        oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

        oSql = New System.Text.StringBuilder
        oSql.AppendLine("CREATE View VLnk_DT AS")
        oSql.AppendLine("   SELECT FTShdDocNo,Fdshddocdate")
        oSql.AppendLine("   ,SUM(CASE WHEN FTShdDocType='1' THEN (ISNULL(FCSdtB4DisChg,0)-ISNULL(FCSdtDis,0)+ISNULL(FCSdtChg,0))-ISNULL(fcsdtdisavg,0)-ISNULL(FCSdtFootAvg,0)-ISNULL(FCSdtRePackAvg,0)")
        oSql.AppendLine("   ELSE -((ISNULL(FCSdtB4DisChg,0)-ISNULL(FCSdtDis,0)+ISNULL(FCSdtChg,0))-ISNULL(fcsdtdisavg,0)-ISNULL(FCSdtFootAvg,0)-ISNULL(FCSdtRePackAvg,0)) END )AS DT_DataTotal")
        oSql.AppendLine("   FROM TPSTSalDT")
        oSql.AppendLine("   WHERE FTBchCode= '" & ptBchCode & "'")
        oSql.AppendLine("   AND ISNULL(FTSdtStaPdt,'') NOT IN('4','5')")
        oSql.AppendLine("   GROUP BY FTShdDocNo,Fdshddocdate")
        oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

        oSql = New System.Text.StringBuilder
        oSql.AppendLine("CREATE View VLnk_RC AS")
        oSql.AppendLine("   SELECT HD.FTShdDocNo,HD.FDShdDocDate,SUM(CASE WHEN HD.FTShdDocType='1' THEN FCSrcNet ELSE -FCSrcNet END )AS RC_DataTotal")
        oSql.AppendLine("   ,SUM(CASE WHEN HD.FTShdDocType='1' THEN FCSrcChg ELSE -FCSrcChg END )AS RC_DataChg")
        oSql.AppendLine("   ,SUM(CASE WHEN HD.FTShdDocType='1' THEN FCSrcAmt ELSE -FCSrcAmt END )AS RC_DataAmt")
        oSql.AppendLine("   FROM TPSTSalRC RC INNER JOIN TPSTSalHD HD ON HD.FTShdDocNo =RC.FTShdDocNo")
        oSql.AppendLine("   WHERE  HD.FTBchCode= '" & ptBchCode & "'")
        oSql.AppendLine("   GROUP BY HD.FTShdDocNo,hd.FDShdDocDate")
        oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

        oSql = New System.Text.StringBuilder
        oSql.AppendLine("SELECT * FROM (")
        oSql.AppendLine("SELECT HD.FTShdDocNo,HD_DataTotal,DT_DataTotal,RC_DataTotal,ROUND(HD_DataTotal-RC_DataTotal,2) AS ZT13")
        oSql.AppendLine("FROM VLnk_HD HD INNER JOIN VLnk_DT DT ON HD.FTShdDocNo=DT.FTShdDocNo")
        oSql.AppendLine("INNER JOIN VLnk_RC RC ON HD.FTShdDocNo=RC.FTShdDocNo")
        oSql.AppendLine("WHERE HD.FDShdDocDate='" & ptDocDate & "'")
        oSql.AppendLine(")SALE")
        oSql.AppendLine("WHERE ZT13 <> 0")

        Return oSql.ToString
    End Function

    Private Function C_GETtCmdHead(ptDocNo As String, ptCompNo As String, ByVal ptBchCode As String) As String  '***PAN 2017-03-30
        Dim oSBCmd As New System.Text.StringBuilder("")
        ptBchCode = "'" & ptBchCode & "'"
        'Dim tPdtVatatble As String = "( (ROUND((ISNULL(FCSdtB4DisChg,0) - ISNULL(FCSdtDis,0) + ISNULL(FCSdtChg,0)) - (ISNULL(FCSdtFootAvg,0) + ISNULL(FCSdtRepackAvg,0) + ISNULL(FCSdtDisAvg,0)) ,2) ) - FCSdtVat )" '*CH 27-12-2014
        'Dim tPdtNoVat As String = "ROUND( ISNULL( DT.FCSdtSetPrice, 0 ) , @rnd )"
        Dim tPdtNoVat As String = "ROUND(ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1)), @rnd )" '*TON 59-03-02
        'Dim tPdtVat As String = "ROUND( ( ISNULL( DT.FCSdtSetPrice, 0 ) * 100 ) / ( 100 + @vatrate ) , @rnd )"
        ' Dim tPdtVat As String = "ROUND(((ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1)))* 100)/(100+@vatrate),@rnd)" '*TON 59-03-02
        Dim tPdtVat As String = "ROUND((ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1))),@rnd)" '*PAN 59-05-17 session ITEM / ฟิดล์ "ACTUALUNITPRICE" เปลี่ยนเป็น ไม่ต้องถอด VAT
        Dim tPdtVatatble As String = ""  '*Em 59-06-19  

        '*Em 59-06-19
        tPdtVatatble = "(CASE WHEN DT.FTSdtVatType = '1' THEN "
        tPdtVatatble &= "((ISNULL(DT.FCSdtB4DisChg,0)-ISNULL(DT.FCSdtDis,0)+ISNULL(DT.FCSdtChg,0)-ISNULL(DT.FCSdtFootAvg,0)+ISNULL(DT.FCSdtRepackAvg,0) + ISNULL(DT.FCSdtDisAvg,0)) * 100)/(100+@vatrate)"
        tPdtVatatble &= "ELSE ISNULL(DT.FCSdtB4DisChg,0)-ISNULL(DT.FCSdtDis,0)+ISNULL(DT.FCSdtChg,0)-ISNULL(DT.FCSdtFootAvg,0)+ISNULL(DT.FCSdtRepackAvg,0) + ISNULL(DT.FCSdtDisAvg,0) END)"
        '+++++++++++++++++ 
        Dim tRnd As String = ""
        Dim oDbTbl = oDatabase.C_CALoExecuteReader("SELECT ISNULL( ( SELECT ISNULL( FTSysUsrValue, 2 ) FROM TSysConfig WHERE FTSysCode IN ( 'ADecPntSF' ) ), 2 )")
        If oDbTbl.Rows.Count > 0 Then
            tRnd = oDbTbl.Rows(0)(0)
        End If
        '*********************************
        '1.5702.1.29 ไม่มี ZT13
        'หาค่า Diff ของ Tender '*CH 18-12-2014
        oSBCmd.AppendLine("DECLARE @DocNo AS VARCHAR(50)")
        oSBCmd.AppendLine("SET @DocNo = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SELECT CASE WHEN HD_DocType = 1 THEN ZT13 ELSE -ZT13 END ZT13 FROM (")
        oSBCmd.AppendLine("SELECT HD.FTShdDocNo,HD_DataRnd,RC_DataAmt,RC_DataChg,RC_DataTotal,HD_DocType")
        oSBCmd.AppendLine(",ROUND(RC_DataAmt-RC_DataChg-RC_DataTotal ,2) AS ZT13")
        oSBCmd.AppendLine("FROM VLnk_HD HD INNER JOIN VLnk_DT DT ON HD.FTShdDocNo=DT.FTShdDocNo")
        oSBCmd.AppendLine("INNER JOIN VLnk_RC RC ON HD.FTShdDocNo=RC.FTShdDocNo")
        oSBCmd.AppendLine("WHERE HD.FTShdDocNo = @DocNo")
        oSBCmd.AppendLine(")SALE")
        oSBCmd.AppendLine("WHERE ZT13 <> 0")
        Dim oDataTemp = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)
        Dim cDiffNet As Double = 0
        If oDataTemp IsNot Nothing AndAlso oDataTemp.Rows.Count > 0 Then
            'cDiffNet = Convert.ToDouble(oDataTemp.Rows(0)("ZT13"))
            cDiffNet = Convert.ToDouble(oDataTemp.Rows(0)("ZT13")) * -1     '*Em 58-02-06  ต้องการให้กลับค่าจากเดิม
        End If
        'If cDiffNet < 0 Then cDiffNet *= -1 'ไม่ต้องให้เป็นค่าที่ไม่ติดลบ '*CH 25-12-2014

        oSBCmd = New System.Text.StringBuilder("")
        oSBCmd.AppendLine("Declare @code AS VARCHAR(30)")
        oSBCmd.AppendLine("Declare @Head As VARCHAR(25)")
        oSBCmd.AppendLine("Declare @Item AS VARCHAR(25)")
        oSBCmd.AppendLine("Declare @Tender As VARCHAR(25)")
        oSBCmd.AppendLine("Declare @Tender2 AS VARCHAR(25)")
        oSBCmd.AppendLine("Declare @rnd AS INTEGER")
        oSBCmd.AppendLine("Declare @vatrate AS DECIMAL")
        oSBCmd.AppendLine("Declare @deli As VARCHAR(10)")
        oSBCmd.AppendLine("Declare @deli2 AS VARCHAR(10)")
        oSBCmd.AppendLine("Declare @comp As VARCHAR(10)")
        oSBCmd.AppendLine("Declare @option_weight_LEN AS VARCHAR(10)")
        oSBCmd.AppendLine("Declare @tTranType As VARCHAR(5)")
        oSBCmd.AppendLine("Declare @tTranType2 AS VARCHAR(5)")
        oSBCmd.AppendLine("Declare @tRate As VARCHAR(5)")

        '-----------------------------------------
        oSBCmd.AppendLine(" Declare @CmpTax As VARCHAR(15)")
        oSBCmd.AppendLine("Declare @Char varchar(100)")
        '--Set @Char = ( SELECT FTShdDisChgTxt FROM TPSTSALHD   WHERE FTShdDocNo = @code )") '-- WHERE FTShdDocNo = 'S1701001001-0016465' )")
        oSBCmd.AppendLine("Declare @Char2 varchar(100)")
        oSBCmd.AppendLine("Declare @VOID Varchar(10)")
        oSBCmd.AppendLine("Declare @POSNo Varchar(10)")
        '---------------------------------------
        oSBCmd.AppendLine("SET @code = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SET @deli2 = '|'")
        oSBCmd.AppendLine("SET @Head = 'B' ")
        oSBCmd.AppendLine("SET @tRate = 'THB'")
        'oSBCmd.AppendLine("Set @Char = ( Select FTShdDisChgTxt FROM TPSTSALHD   WHERE FTShdDocNo = @code ) -- WHERE FTShdDocNo = 'S1701001001-0016465' )")
        oSBCmd.AppendLine("SET @Char = ISNULL(( Select ISNULL( FTShdDisChgTxt,'') FROM TPSTSALHD   WHERE FTShdDocNo = @code ),'')") '-- WHERE FTShdDocNo = 'S1701001001-0016465' )
        ' oSBCmd.AppendLine("Set @Char2 = ISNULL((SELECT CHARINDEX('%', @Char) ),'')")
        oSBCmd.AppendLine("SET @Char2 = ISNULL((SELECT CHARINDEX('%', @Char) ),'')")
        oSBCmd.AppendLine("Set @VOID = (Select Count(FTSdtStaPdt) FROM TPSTSALDT  where FTSdtStaPdt =   '4' AND  FTShdDocNo =  @code)")
        oSBCmd.AppendLine("SET @POSNo = ISNULL((Select FTPosCode From TPSTSalHD  WHERE FTShdDocNo =  @code),'')")
        ' oSBCmd.AppendLine("Set @CmpTax = ISNULL( ( Select  ISNULL( FTCmpTaxNo, '' ) FROM TCNMComp ), '' )")
        oSBCmd.AppendLine("SET @CmpTax = ISNULL((Select FTPosRegNo from TPSMPos Where FTPosCode = @POSNo),'')")



        oSBCmd.AppendLine("SELECT xSale.xData FROM (")
        '### Head
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Head + @deli2  +")    '[1]Type
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli2 +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("'002' + @deli2 +")
        'oSBCmd.AppendLine(" SUBSTRING(ISNULL(HD.FTPosCode, '' ),2,3) + @deli2  +")
        oSBCmd.AppendLine("Right(ISNULL(@POSNo, '' ),2) + @deli2  +")
        oSBCmd.AppendLine("@CmpTax + @deli2  +")
        oSBCmd.AppendLine("ISNULL(Convert(VARCHAR(10), HD.FDShdDocDate, 121), '' )  + @deli2 +")
        'oSBCmd.AppendLine("RIGHT(ISNULL(HD.FTShdDocNo, '' ),7) + @deli2  +")
        oSBCmd.AppendLine("LEFT(ISNULL(HD.FTShdDocNo,''),1) + RIGHT(ISNULL(HD.FTShdDocNo, '' ),6) + @deli2  +") 'PAN 2017-04-12 แก้ตาม PTG P'ป่าน
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(HD.FCShdTotal,''))) + @deli2  +")
        oSBCmd.AppendLine("ISNULL(Convert(VARCHAR(10), HD.FDShdDocDate, 121), '' ) + ' ' + ( ISNULL( HD.FTShdDocTime, '' ) ) + @deli2 +")
        'oSBCmd.AppendLine("(CASE ISNULL(@Char2 ,'') WHEN 0 Then 'B' ELSE '%' END) + @deli2 + ") '--SH_DISFG 
        ' oSBCmd.AppendLine("(CASE ISNULL(@Char2 ,'') WHEN '' Then CASE WHEN @Char2 > 1 Then 'B'  ELSE '%'  END ELSE '' END) + @deli2 + ")
        oSBCmd.AppendLine("(CASE WHEN @Char2 = 0 And @Char <> ''  Then 'B' WHEN @Char2 > 0 AND @Char <> '' Then '%'    ELSE '' END)   + @deli2 + ")
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(HD.FCShdDis,''))) + @deli2  + ") '--SH_DISCNT
        'oSBCmd.AppendLine("(CASE WHEN @Char2 = 0 And @Char <> ''  Then Convert(VARCHAR,Convert(Decimal(18, 0),ISNULL(HD.FCShdDis,''))) WHEN @Char2 > 0 AND @Char <> '' Then REPLACE(HD.FTShdDisChgTxt,'%','')    ELSE '' END)   + @deli2 + ")  ถ้าเปลี่ยนให้ใส่ %
        oSBCmd.AppendLine(" '' + @deli2  + ") '--SH_SLDISC
        oSBCmd.AppendLine(" Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(HD.FCShdGrand,''))) + @deli2  + ") '--SH_NETAMT
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(HD.FCShdMnyCsh,''))) + @deli2  +") '--SH_PAYCASH
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(HD.FCShdMnyCpn,''))) + @deli2  + ") '--SH_CUPON
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(HD.FCShdMnyCrd,''))) + @deli2  +") '--SH_CREDIT
        'oSBCmd.AppendLine("ISNULL(RC.FTSrcRef,'') + @deli2  + ") '--SH_CREDNO
        oSBCmd.AppendLine("ISNULL(( Select  ISNULL( FTSrcRef, '' ) FROM TPSTSALRC Where FTShdDocNo = @Code AND FTRcvCode = '002' ), '' ) + @deli2  + ")
        oSBCmd.AppendLine("ISNULL(HD.FTCstCode,'') + @deli2  + ") '--SH_MEMBER
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(HD.FCShdChn,''))) + @deli2  + ") '--SH_CHANGE
        oSBCmd.AppendLine("ISNULL(@VOID, 0) + @deli2  + ") '--SH_VOID
        ' oSBCmd.AppendLine("'' + @deli2  + ") '--SH_REFER
        'oSBCmd.AppendLine("(CASE WHEN HD.FTShdDocType = 9 THEN  HD.FTShdPosCN ELSE ''  END) + @deli2  +  ") '--SH_REFER PAN 2017-05-03
        oSBCmd.AppendLine("(CASE WHEN HD.FTShdDocType = 9 THEN  LEFT(ISNULL(HD.FTShdPosCN,''),1) + RIGHT(ISNULL(HD.FTShdPosCN, '' ),6) ELSE ''  END) + @deli2  +   ") '--SH_REFER PAN 2017-05-03 7หลัก
        ' oSBCmd.AppendLine("'' + @deli2  + ") '--SH_RESU  'Map Table
        oSBCmd.AppendLine("ISNULL(( Select Top 1 Right (ISNULL( FTRsnCode, '' ),1) FROM TPSTVoidDT Where FTShdDocNo = @Code ), '' ) + @deli2  +  ")

        oSBCmd.AppendLine("'R'+ @deli2 +")
        oSBCmd.AppendLine("'0'+ @deli2 +")
        'oSBCmd.AppendLine("ISNULL(HD.FTLogCode,'') + @deli2  + ") '--SH_CLERK
        oSBCmd.AppendLine("Right(ISNULL(HD.FTLogCode,''),2) + @deli2  + ")
        oSBCmd.AppendLine("ISNULL(HD.FTUsrCode + ' ', '' )  ")
        oSBCmd.AppendLine("AS xData, @Head xType, HD.FTShdDocNo, 0 FNSdtSeqNo")
        oSBCmd.AppendLine("From TPSTSalHD HD LEFT OUTER Join TSysUser USR On ( HD.FTUsrCode = USR.FTUsrCode )")
        'oSBCmd.AppendLine("Left OUTER JOIN TPSTSalRC RC ON ( RC.FTShdDocNo = HD.FTShdDocNo )")
        ' - -Left() OUTER JOIN TPSTSalDT DT On ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE CASE WHEN ISNULL(HD.FTShdDocType,'')<>'' THEN HD.FTShdDocType ELSE  HD.FTShdDocType END IN ('1','9')")
        oSBCmd.AppendLine(") xSale")
        oSBCmd.AppendLine("WHERE @code In ( FTShdDocNo, '' )")
        oSBCmd.AppendLine("ORDER BY")
        oSBCmd.AppendLine("xSale.FTShdDocNo Asc")
        oSBCmd.AppendLine(", ( CASE xType")
        oSBCmd.AppendLine("WHEN @Head THEN 1")
        oSBCmd.AppendLine("Else 999 End )")
        oSBCmd.AppendLine(", xSale.FNSdtSeqNo ASC")
        Return oSBCmd.ToString()
    End Function
    Private Function C_GETtCmdDT(ptDocNo As String, ptCompNo As String, ByVal ptBchCode As String) As String  '***PAN 2017-03-30
        Dim oSBCmd As New System.Text.StringBuilder("")
        ptBchCode = "'" & ptBchCode & "'"
        'Dim tPdtVatatble As String = "( (ROUND((ISNULL(FCSdtB4DisChg,0) - ISNULL(FCSdtDis,0) + ISNULL(FCSdtChg,0)) - (ISNULL(FCSdtFootAvg,0) + ISNULL(FCSdtRepackAvg,0) + ISNULL(FCSdtDisAvg,0)) ,2) ) - FCSdtVat )" '*CH 27-12-2014
        'Dim tPdtNoVat As String = "ROUND( ISNULL( DT.FCSdtSetPrice, 0 ) , @rnd )"
        Dim tPdtNoVat As String = "ROUND(ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1)), @rnd )" '*TON 59-03-02
        'Dim tPdtVat As String = "ROUND( ( ISNULL( DT.FCSdtSetPrice, 0 ) * 100 ) / ( 100 + @vatrate ) , @rnd )"
        ' Dim tPdtVat As String = "ROUND(((ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1)))* 100)/(100+@vatrate),@rnd)" '*TON 59-03-02
        Dim tPdtVat As String = "ROUND((ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1))),@rnd)" '*PAN 59-05-17 session ITEM / ฟิดล์ "ACTUALUNITPRICE" เปลี่ยนเป็น ไม่ต้องถอด VAT
        Dim tPdtVatatble As String = ""  '*Em 59-06-19  

        '*Em 59-06-19
        tPdtVatatble = "(CASE WHEN DT.FTSdtVatType = '1' THEN "
        tPdtVatatble &= "((ISNULL(DT.FCSdtB4DisChg,0)-ISNULL(DT.FCSdtDis,0)+ISNULL(DT.FCSdtChg,0)-ISNULL(DT.FCSdtFootAvg,0)+ISNULL(DT.FCSdtRepackAvg,0) + ISNULL(DT.FCSdtDisAvg,0)) * 100)/(100+@vatrate)"
        tPdtVatatble &= "ELSE ISNULL(DT.FCSdtB4DisChg,0)-ISNULL(DT.FCSdtDis,0)+ISNULL(DT.FCSdtChg,0)-ISNULL(DT.FCSdtFootAvg,0)+ISNULL(DT.FCSdtRepackAvg,0) + ISNULL(DT.FCSdtDisAvg,0) END)"
        '+++++++++++++++++ 
        Dim tRnd As String = ""
        Dim oDbTbl = oDatabase.C_CALoExecuteReader("SELECT ISNULL( ( SELECT ISNULL( FTSysUsrValue, 2 ) FROM TSysConfig WHERE FTSysCode IN ( 'ADecPntSF' ) ), 2 )")
        If oDbTbl.Rows.Count > 0 Then
            tRnd = oDbTbl.Rows(0)(0)
        End If
        '*********************************
        '1.5702.1.29 ไม่มี ZT13
        'หาค่า Diff ของ Tender '*CH 18-12-2014
        oSBCmd.AppendLine("DECLARE @DocNo AS VARCHAR(50)")
        oSBCmd.AppendLine("SET @DocNo = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SELECT CASE WHEN HD_DocType = 1 THEN ZT13 ELSE -ZT13 END ZT13 FROM (")
        oSBCmd.AppendLine("SELECT HD.FTShdDocNo,HD_DataRnd,RC_DataAmt,RC_DataChg,RC_DataTotal,HD_DocType")
        oSBCmd.AppendLine(",ROUND(RC_DataAmt-RC_DataChg-RC_DataTotal ,2) AS ZT13")
        oSBCmd.AppendLine("FROM VLnk_HD HD INNER JOIN VLnk_DT DT ON HD.FTShdDocNo=DT.FTShdDocNo")
        oSBCmd.AppendLine("INNER JOIN VLnk_RC RC ON HD.FTShdDocNo=RC.FTShdDocNo")
        oSBCmd.AppendLine("WHERE HD.FTShdDocNo = @DocNo")
        oSBCmd.AppendLine(")SALE")
        oSBCmd.AppendLine("WHERE ZT13 <> 0")
        Dim oDataTemp = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)
        Dim cDiffNet As Double = 0
        If oDataTemp IsNot Nothing AndAlso oDataTemp.Rows.Count > 0 Then
            'cDiffNet = Convert.ToDouble(oDataTemp.Rows(0)("ZT13"))
            cDiffNet = Convert.ToDouble(oDataTemp.Rows(0)("ZT13")) * -1     '*Em 58-02-06  ต้องการให้กลับค่าจากเดิม
        End If
        'If cDiffNet < 0 Then cDiffNet *= -1 'ไม่ต้องให้เป็นค่าที่ไม่ติดลบ '*CH 25-12-2014

        oSBCmd = New System.Text.StringBuilder("")
        oSBCmd.AppendLine("Declare @code AS VARCHAR(30)")
        oSBCmd.AppendLine("Declare @Item AS VARCHAR(25)")
        oSBCmd.AppendLine("Declare @deli2 AS VARCHAR(10)")
        oSBCmd.AppendLine(" Declare @CmpTax As VARCHAR(15)")
        'oSBCmd.AppendLine(" Declare @VOID Varchar(10)")
        oSBCmd.AppendLine("Declare @Char varchar(100)")
        oSBCmd.AppendLine("Declare @Char2 varchar(100)")
        oSBCmd.AppendLine("Declare @VOID Varchar(10)")
        oSBCmd.AppendLine("Declare @POSNo Varchar(10)")



        oSBCmd.AppendLine("SET @code = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SET @deli2 = '|'")
        oSBCmd.AppendLine("SET @Item  = 'B' ")
        ' oSBCmd.AppendLine("SET @CmpTax = ISNULL( ( SELECT  ISNULL( FTCmpTaxNo, '' ) FROM TCNMComp ), '' )")
        oSBCmd.AppendLine("Set @VOID = (SELECT Count(FTSdtStaPdt) FROM TPSTSALDT  where FTSdtStaPdt =   '4' AND  FTShdDocNo =  @code)")

        oSBCmd.AppendLine("SET @Char = ISNULL(( Select ISNULL( FTShdDisChgTxt,'') FROM TPSTSALHD   WHERE FTShdDocNo = @code ),'')") ' -- WHERE FTShdDocNo = 'S1701001001-0016465' )
        oSBCmd.AppendLine("SET @Char2 = ISNULL((SELECT CHARINDEX('%', @Char) ),'')")
        oSBCmd.AppendLine("Set @POSNo = ISNULL((Select FTPosCode From TPSTSalHD  WHERE FTShdDocNo =  @code),'')")
        oSBCmd.AppendLine("Set @CmpTax = ISNULL((Select FTPosRegNo from TPSMPos Where FTPosCode = @POSNo),'')")
        oSBCmd.AppendLine("SELECT xSale.xData FROM (")
        '### DT
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Item + @deli2 +")
        ' oSBCmd.AppendLine("'001'+ @deli2 +")
        oSBCmd.AppendLine(ptBchCode & "+ @deli2 +")
        oSBCmd.AppendLine("'002' + @deli2 +")
        oSBCmd.AppendLine("RIGHT(ISNULL(HD.FTPosCode, '' ),2) + @deli2  +")
        oSBCmd.AppendLine("@CmpTax + @deli2  +")
        oSBCmd.AppendLine("ISNULL(Convert(VARCHAR(10), HD.FDShdDocDate, 121), '' )  + @deli2 +")
        'oSBCmd.AppendLine("RIGHT(ISNULL(HD.FTShdDocNo, '' ),7) + @deli2  +")
        oSBCmd.AppendLine("LEFT(ISNULL(HD.FTShdDocNo,''),1) + RIGHT(ISNULL(HD.FTShdDocNo, '' ),6) + @deli2  +") 'PAN 2017-04-12 แก้ตาม PTG P'ป่าน
        oSBCmd.AppendLine("ISNULL(Convert(VARCHAR(10), HD.FDShdDocDate, 121), '' ) + ' ' + ( ISNULL( HD.FTShdDocTime, '' ) ) + @deli2 +")
        oSBCmd.AppendLine("ISNULL(DT.FTSdtBarCode,'') + @deli2 +")
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(DT.FCSdtQty,''))) + @deli2  + ")
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(DT.FCSdtSetPrice,''))) + @deli2  + ")
        ' oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(DT.FCSdtNet,''))) + @deli2  +")
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(DT.FCSdtVatable,'') + ISNULL(DT.FCSdtVat,'')  )) + @deli2  + ") 'PAN 2017-05-03  P'ป่าน PTG
        oSBCmd.AppendLine("'' + @deli2  + ")
        oSBCmd.AppendLine("'' + @deli2  + ")
        oSBCmd.AppendLine("(CASE WHEN DT.FCSDTDIS = 0 THEN CASE WHEN DT.FTSDTSTAPDT = '1' THEN 'S' ELSE 'V' END ELSE 'I' END  ) + @deli2  + ")
        oSBCmd.AppendLine("ISNULL(FTSdtVatType,'') +  @deli2 + ")
        'oSBCmd.AppendLine("'' + @deli2  + ")
        oSBCmd.AppendLine("ISNULL(HD.FTUsrCode + ' ', '' )   + @deli2 + ")
        oSBCmd.AppendLine("'' + @deli2  +")
        oSBCmd.AppendLine("Convert(VARCHAR, Convert(Decimal(18, " & tRnd & "), ISNULL(DT.FNSdtSeqNo,''))) + @deli2  + ")
        oSBCmd.AppendLine("'0' + @deli2  +  ")
        oSBCmd.AppendLine("ISNULL((CASE WHEN DT.FTPmhCode <> '' THEN  'P' ELSE ''  END),'')  ") 'PAN 2017-05-03 
        oSBCmd.AppendLine("AS xData, @Item xType, DT.FTShdDocNo, DT.FNSdtSeqNo FNSdtSeqNo")
        oSBCmd.AppendLine("From TPSTSalDT DT")
        oSBCmd.AppendLine("Left OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("Left OUTER JOIN TCNMPdt PDT On ( DT.FTPdtCode = PDT.FTPdtCode )")
        oSBCmd.AppendLine("WHERE ISNULL(DT.FTSdtStaPdt, '' ) IN ('1','2','3','4')")
        oSBCmd.AppendLine("And @code in ( HD.FTShdDocNo, '' )")
        oSBCmd.AppendLine("And CASE WHEN ISNULL(HD.FTShdDocType,'')<>'' THEN HD.FTShdDocType ELSE  HD.FTShdDocType END IN ('1','9')")
        oSBCmd.AppendLine(") xSale")
        oSBCmd.AppendLine("WHERE @code in ( FTShdDocNo, '' )")
        oSBCmd.AppendLine("ORDER BY")
        oSBCmd.AppendLine("xSale.FTShdDocNo Asc")
        oSBCmd.AppendLine(", ( CASE xType")
        oSBCmd.AppendLine("WHEN @Item THEN 2")
        oSBCmd.AppendLine("Else 999 End )")
        oSBCmd.AppendLine(", xSale.FNSdtSeqNo ASC")
        Return oSBCmd.ToString()
    End Function
    Private Function C_GETtCmdHead2(ptDocNo As String, ptCompNo As String, ByVal ptBchCode As String) As String 'PAN **** Type 011 2016-05-24
        ptBchCode = "'" & ptBchCode & "'"
        Dim oSBCmd As New System.Text.StringBuilder("")

        'Dim tPdtVatatble As String = "( (ROUND((ISNULL(FCSdtB4DisChg,0) - ISNULL(FCSdtDis,0) + ISNULL(FCSdtChg,0)) - (ISNULL(FCSdtFootAvg,0) + ISNULL(FCSdtRepackAvg,0) + ISNULL(FCSdtDisAvg,0)) ,2) ) - FCSdtVat )" '*CH 27-12-2014
        'Dim tPdtNoVat As String = "ROUND( ISNULL( DT.FCSdtSetPrice, 0 ) , @rnd )"
        Dim tPdtNoVat As String = "ROUND(ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1)), @rnd )" '*TON 59-03-02
        'Dim tPdtVat As String = "ROUND( ( ISNULL( DT.FCSdtSetPrice, 0 ) * 100 ) / ( 100 + @vatrate ) , @rnd )"
        ' Dim tPdtVat As String = "ROUND(((ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1)))* 100)/(100+@vatrate),@rnd)" '*TON 59-03-02
        Dim tPdtVat As String = "ROUND((ISNULL(DT.FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(DT.FCSdtQty,1))),@rnd)" '*PAN 59-05-17 session ITEM / ฟิดล์ "ACTUALUNITPRICE" เปลี่ยนเป็น ไม่ต้องถอด VAT
        Dim tPdtVatatble As String = ""  '*Em 59-06-19  

        '*Em 59-06-19
        tPdtVatatble = "(CASE WHEN DT.FTSdtVatType = '1' THEN "
        tPdtVatatble &= "((ISNULL(DT.FCSdtB4DisChg,0)-ISNULL(DT.FCSdtDis,0)+ISNULL(DT.FCSdtChg,0)-ISNULL(DT.FCSdtFootAvg,0)+ISNULL(DT.FCSdtRepackAvg,0) + ISNULL(DT.FCSdtDisAvg,0)) * 100)/(100+@vatrate)"
        tPdtVatatble &= "ELSE ISNULL(DT.FCSdtB4DisChg,0)-ISNULL(DT.FCSdtDis,0)+ISNULL(DT.FCSdtChg,0)-ISNULL(DT.FCSdtFootAvg,0)+ISNULL(DT.FCSdtRepackAvg,0) + ISNULL(DT.FCSdtDisAvg,0) END)"
        '+++++++++++++++++

        '1.5702.1.29 ไม่มี ZT13
        'หาค่า Diff ของ Tender '*CH 18-12-2014
        oSBCmd.AppendLine("DECLARE @DocNo AS VARCHAR(50)")
        oSBCmd.AppendLine("SET @DocNo = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SELECT CASE WHEN HD_DocType = 1 THEN ZT13 ELSE -ZT13 END ZT13 FROM (")
        oSBCmd.AppendLine("SELECT HD.FTShdDocNo,HD_DataRnd,RC_DataAmt,RC_DataChg,RC_DataTotal,HD_DocType")
        oSBCmd.AppendLine(",ROUND(RC_DataAmt-RC_DataChg-RC_DataTotal ,2) AS ZT13")
        oSBCmd.AppendLine("FROM VLnk_HD HD INNER JOIN VLnk_DT DT ON HD.FTShdDocNo=DT.FTShdDocNo")
        oSBCmd.AppendLine("INNER JOIN VLnk_RC RC ON HD.FTShdDocNo=RC.FTShdDocNo")
        oSBCmd.AppendLine("WHERE HD.FTShdDocNo = @DocNo")
        oSBCmd.AppendLine(")SALE")
        oSBCmd.AppendLine("WHERE ZT13 <> 0")
        Dim oDataTemp = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)
        Dim cDiffNet As Double = 0
        If oDataTemp IsNot Nothing AndAlso oDataTemp.Rows.Count > 0 Then
            'cDiffNet = Convert.ToDouble(oDataTemp.Rows(0)("ZT13"))
            cDiffNet = Convert.ToDouble(oDataTemp.Rows(0)("ZT13")) * -1     '*Em 58-02-06  ต้องการให้กลับค่าจากเดิม
        End If
        'If cDiffNet < 0 Then cDiffNet *= -1 'ไม่ต้องให้เป็นค่าที่ไม่ติดลบ '*CH 25-12-2014

        oSBCmd = New System.Text.StringBuilder("")
        oSBCmd.AppendLine("DECLARE @code AS VARCHAR(30)")
        oSBCmd.AppendLine("DECLARE @Head AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @Item AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @Tender AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @Tender2 AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @rnd AS INTEGER")
        oSBCmd.AppendLine("DECLARE @vatrate AS DECIMAL")
        oSBCmd.AppendLine("DECLARE @deli AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @deli2 AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @deli3 AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @comp AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @option_weight_LEN AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @tTranType AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @tTranType2 AS VARCHAR(5)") ' PAN 2016-06-16 
        oSBCmd.AppendLine("DECLARE @tRate AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @VoucherDisTenderCode AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VoucherMPointTenderCode AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @tEventNum AS VARCHAR(1)")
        oSBCmd.AppendLine("DECLARE @CreditCardPersonalLoanTenderCode AS VARCHAR(25)") 'AEON,First Choic *CH 29-11-2014
        oSBCmd.AppendLine("DECLARE @VoucherCashCouponTenderCode  AS VARCHAR(25)") 'Cash/Corp coupon *CH 29-11-2014
        oSBCmd.AppendLine("DECLARE @CreditCardCredit  AS VARCHAR(25)") '*TON 59-03-03
        oSBCmd.AppendLine("DECLARE @VoucherBankTransferTenderCode   AS VARCHAR(25)") '*TON 59-03-03
        '*TON 59-03-14
        oSBCmd.AppendLine("DECLARE @CRTEAEOV   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTEUOBV   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTELOBL   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTELOBY   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTELOKB   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTELOFC   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTELOAE   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTECRED   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VOTECOUP   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VOTECSCP   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VOTEBTNF   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VOTEBTNF1    AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VOTEBTNF2    AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VOTEBTNF3    AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @VOTEBMNF   AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @CRTEUOBM   AS VARCHAR(25)")

        oSBCmd.AppendLine("SET @code = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SET @deli2 = ':'")
        oSBCmd.AppendLine("SET @Head = 'HEAD' + @deli2")
        oSBCmd.AppendLine("SET @Item = 'ITEM' + @deli2")
        oSBCmd.AppendLine("SET @Tender = 'TENDER' + @deli2")
        oSBCmd.AppendLine("SET @Tender2 = 'TENDER2' + @deli2")
        oSBCmd.AppendLine("SET @option_weight_LEN = ISNULL( ( SELECT FTSysUsrValue FROM TSysConfig WHERE FTSysCode = 'ALenScales' AND FTSysSeq = '001' ), 7 )")
        oSBCmd.AppendLine("SET @rnd = ISNULL( ( SELECT ISNULL( FTSysUsrValue, 2 ) FROM TSysConfig WHERE FTSysCode IN ( 'ADecPntSF' ) ), 2 )")
        oSBCmd.AppendLine("SET @vatrate = ISNULL( ( SELECT TOP 1 isnull( FCCmpVatAmt, 7 ) FROM TCNMComp ), 7 )")
        oSBCmd.AppendLine("SET @comp = '" & ptCompNo & "'")
        oSBCmd.AppendLine("SET @tTranType = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPENOM')")
        oSBCmd.AppendLine("SET @tTranType2 = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS')") 'PAN 2016-06-16 
        oSBCmd.AppendLine("SET @tRate = 'THB'")
        oSBCmd.AppendLine("SET @deli = '" & tSpr & "'")
        oSBCmd.AppendLine("SET @deli3 = '-'")
        oSBCmd.AppendLine("SET @VoucherDisTenderCode = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTECOUP')")
        oSBCmd.AppendLine("SET @VoucherMPointTenderCode  = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTEMPOI')")
        oSBCmd.AppendLine("SET @tEventNum  = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='EVENTNO')")
        oSBCmd.AppendLine("SET @CreditCardPersonalLoanTenderCode = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTELOAN')") 'AEON, First Choice *CH 29-11-2014
        oSBCmd.AppendLine("SET @VoucherCashCouponTenderCode = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTECSCP')") 'Cash/Corp coupon *CH 29-11-2014
        oSBCmd.AppendLine("SET @CreditCardCredit = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTECRED')") '*TON 59-03-03
        oSBCmd.AppendLine("SET @VoucherBankTransferTenderCode = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTEBTNF')") '*TON 59-03-03
        '*TON 59-03-14
        oSBCmd.AppendLine("SET @CRTEAEOV = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTEAEOV')")
        oSBCmd.AppendLine("SET @CRTEUOBV = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTEUOBV')")
        oSBCmd.AppendLine("SET @CRTELOBL = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTELOBL')")
        oSBCmd.AppendLine("SET @CRTELOBY = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTELOBY')")
        oSBCmd.AppendLine("SET @CRTELOKB = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTELOKB')")
        oSBCmd.AppendLine("SET @CRTELOFC = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTELOFC')")
        oSBCmd.AppendLine("SET @CRTELOAE = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTELOAE')")
        oSBCmd.AppendLine("SET @CRTECRED = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTECRED')")
        oSBCmd.AppendLine("SET @VOTECOUP = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTECOUP')")
        oSBCmd.AppendLine("SET @VOTECSCP = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTECSCP')")
        oSBCmd.AppendLine("SET @VOTEBTNF = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTEBTNF')")
        oSBCmd.AppendLine("SET @VOTEBTNF1  = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTEBTNF1')")
        oSBCmd.AppendLine("SET @VOTEBTNF2  = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTEBTNF2')")
        oSBCmd.AppendLine("SET @VOTEBTNF3  = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTEBTNF3')")
        oSBCmd.AppendLine("SET @VOTEBMNF = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='VOTEBMNF')")
        oSBCmd.AppendLine("SET @CRTEUOBM = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='CRTEUOBM')")
        oSBCmd.AppendLine("SELECT xSale.xData FROM (")
        '### Head
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Head + @deli  +")    '[1]Type
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') + @deli +")   '[4]TRANSACTIONTYPECODE NEW '*TON 59-03-02
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDDateIns, 112 ), '' ) + REPLACE( ISNULL( HD.FTTimeIns, '' ), ':', '' ) + @deli +")  '[7]BEGINDATETIMESTAMP
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDDateIns, 112 ), '' ) + REPLACE( ISNULL( HD.FTTimeIns, '' ), ':', '' ) + @deli +")  '[8]ENDDATETIMESTAMP
        'oSBCmd.AppendLine("CASE @tEventNum WHEN '1' THEN @comp + ISNULL( HD.FTBchCode, '' ) ELSE (SELECT TOP 1 FTCmpDirector FROM TCNMComp) END + @deli +")   '[9]DEPARTMENT	'Wave2 ใช้ siteCode, Wave3 ใช้ชื่อผู้ประกอบการ หน้าบริษัท
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[9]DEPARTMENT New = [2]RETAILSTOREID '*TON 59-03-02
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("'' + @deli +")   '[10]OPERATORQUALIFIER	'blank
        oSBCmd.AppendLine("ISNULL( HD.FTUsrCode + ' ', '' ) + ISNULL( LEFT( USR.FTUsrName, 26 ), '' ) + @deli +")   '[11]OPERATORID
        oSBCmd.AppendLine("@tRate + @deli +")    '[12]TRANSACTIONCURRENCY
        oSBCmd.AppendLine("'' + @deli +")   '[13]PARTNERQUALIFIER	-blank
        oSBCmd.AppendLine("ISNULL( FTCstCode, '' ) + @deli +")    '[14]PARTNERID
        oSBCmd.AppendLine("ISNULL( HD.FTShdRefInt, '' )")    '[15]SoNo '*TON 59-03-02
        oSBCmd.AppendLine("AS xData, @Head xType, HD.FTShdDocNo, 0 FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalHD HD LEFT OUTER JOIN TSysUser USR ON ( HD.FTUsrCode = USR.FTUsrCode )")
        oSBCmd.AppendLine("WHERE CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-02-07

        '### Item
        oSBCmd.AppendLine("UNION")
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Item + @deli +")    '[1]Type
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') + @deli +")   '[4]TRANSACTIONTYPECODE NEW '*TON 59-03-02
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) + @deli +")   '[7]RETAILSEQUENCENUMBER
        oSBCmd.AppendLine("( CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 9 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='SALETYPE002')")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='SALETYPE001') END ) + @deli +")  '[8]RETAILTYPECODE
        oSBCmd.AppendLine("'' + @deli +")   '[9]RETAILREASONCODE	'blank
        oSBCmd.AppendLine("'1' + @deli +")  '[10]ITEMIDQUALIFIER
        'oSBCmd.AppendLine("( CASE PDT.FTPdtSaleType WHEN '3' THEN LEFT( DT.FTSdtBarCode, @option_weight_LEN ) ELSE DT.FTSdtBarCode END ) + @deli +")    '[11]ITEMID
        oSBCmd.AppendLine("DT.FTSdtBarCode + @deli +")    '[11]ITEMID 'สินค้าเครื่องชั่ง ส่ง barcode ตามที่ Scan ขาย '*CH 22-01-2015
        oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( DT.FCSdtQty, 0 ) )) + @deli +")  '[12]RETAILQUANTITY  '*Em 57-12-12
        oSBCmd.AppendLine("ISNULL( DT.FTPunCode, '' ) + @deli +")   '[13]SALESUNITOFMEASURE
        oSBCmd.AppendLine("'' + @deli +")    '[14]SALESUNITOFMEASURE_ISO	'blank
        'oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2),CASE WHEN ( ISNULL(DT.FTSdtStaPdt,'') IN ('5')) THEN ISNULL(DT.FCSdtB4DisChg,0) ") '[15]SALESAMOUNT New '*TON 59-03-02
        'oSBCmd.AppendLine("WHEN ( ISNULL(DT.FCSdtVatable,0) = 0 ) AND ( ISNULL(DT.FCSdtDisAvg,0) > 0) THEN ISNULL(DT.FCSdtB4DisChg,0) ") '[15]
        'oSBCmd.AppendLine("ELSE ( ISNULL(DT.FCSdtVatable,0) ) END )) + @deli +") '[15]
        '*CH 23-12-2014 ใช้ B4DisChg - Dis ถอด Vat
        oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ")      '*Em 57-12-12
        oSBCmd.AppendLine("( CASE WHEN ( ISNULL( DT.FTSdtStaPdt, '' ) IN ( '5' ) )")
        oSBCmd.AppendLine("THEN CONVERT( DECIMAL(18,2), ISNULL( DT.FCSdtB4DisChg, 0 ) )")
        'oSBCmd.AppendLine("ELSE CONVERT( DECIMAL(18,2), ( ( ISNULL(FCSdtB4DisChg,0)-ISNULL(FCSdtDis,0)+ISNULL(FCSdtChg,0))-ISNULL(FCSdtDisAvg,0)-ISNULL(FCSdtFootAvg,0)-ISNULL(FCSdtRePackAvg,0) * 100 ) / ( 100 + @vatrate ) )END ),128 )) + @deli +") '[15]SALESAMOUNT
        'oSBCmd.AppendLine("ELSE CONVERT( DECIMAL(18,2), ISNULL( DT.FCSdtVatable, 0 ) )END ),128 )) + @deli +") '[15]SALESAMOUNT
        'oSBCmd.AppendLine("ELSE CONVERT( DECIMAL(18,2), CASE WHEN DT.FTSdtVatType = '1' THEN " & tPdtVat & " ELSE " & tPdtNoVat & " END )END ),128 )) + @deli +") '[15]SALESAMOUNT
        oSBCmd.AppendLine("ELSE CONVERT( DECIMAL(18,2), " & tPdtVatatble & " )END ),128 )) + @deli +") '[15]SALESAMOUNT
        'oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * CONVERT( DECIMAL(18,2), " & tPdtVatatble & " )END ),128 )) + @deli +") '[15]SALESAMOUNT
        oSBCmd.AppendLine("'' + @deli +")   '[16]NORMALSALESAMOUNT    'blank
        oSBCmd.AppendLine("'' + @deli +")   '[17]COST	'blank
        oSBCmd.AppendLine("'' + @deli +")   '[18]BATCHID	'blank
        oSBCmd.AppendLine("ISNULL( HD.FTShdPosCN, '' ) + @deli +")  '[19]SERIALNUMBER
        'oSBCmd.AppendLine("ISNULL( DT.FTPmhCode, '' ) + @deli +")   '[20]PROMOTIONID
        oSBCmd.AppendLine("( CASE ISNULL( DT.FCSdtDisAvg, 0 ) WHEN 0 THEN ISNULL( DT.FTSdtTaxInv, '') ELSE ISNULL( DT.FTPmhCode, '' ) END ) + @deli +")   'Price Off '*CH 13-01-2015 '[20]PROMOTIONID
        oSBCmd.AppendLine("'' + @deli +")   '[21]ITEMIDENTRYMETHODCODE	'blank
        'oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ROUND( ( ISNULL( DT.FCSdtSetPrice, 0 ) * 100 ) / ( 100 + @vatrate ) , @rnd ) ), 128) + @deli +") '[22]ACTUALUNITPRICE '*CH 03-12-2014 เพิ่ม Convert Decimal
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE WHEN DT.FTSdtVatType = '1' THEN " & tPdtVat & " ELSE " & tPdtNoVat & " END ) ), 128) + @deli +") '[22]ACTUALUNITPRICE 'สินค้า Non VAT ที่ฟิลด์ราคา ไม่ต้องถอด VAT อีก '*CH 22-01-2015 
        oSBCmd.AppendLine("'' + @deli +")   '[23]UNITS	'blank
        oSBCmd.AppendLine("''") '[24]SCANTIME	'blank
        oSBCmd.AppendLine("AS xData, @Item xType, DT.FTShdDocNo, DT.FNSdtSeqNo FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalDT DT")
        oSBCmd.AppendLine("LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("LEFT OUTER JOIN TCNMPdt PDT ON ( DT.FTPdtCode = PDT.FTPdtCode )")
        'oSBCmd.AppendLine("WHERE ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4', '5' ) OR ( ( ISNULL( DT.FTSdtStaPdt, '' ) IN ( '5' ) ) AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 ) ) )")  '*Tee YYYYYYYYYYYYYYYYYYYYYYY ทำไมต้อง DisAvg>0
        'oSBCmd.AppendLine("WHERE ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4', '5' ) OR ( ( ISNULL( DT.FTSdtStaPdt, '' ) IN ( '5' ) )")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 OR ISNULL( DT.FCShdDiscGP1, 0 ) > 0 ) ) )")  'Price Off '*CH 13-01-2015
        'oSBCmd.AppendLine("WHERE ( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) )")
        oSBCmd.AppendLine("WHERE ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3')")
        oSBCmd.AppendLine("AND @code in ( HD.FTShdDocNo, '' )")
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07

        '### Item 011
        oSBCmd.AppendLine("UNION")
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Item + @deli +")
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +")
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")
        'oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') + @deli +")
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")
        'oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( RC.FNSrcSeqNo, 0 ) ASC) ) + @deli +")
        oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( RC.FNSrcSeqNo, 0 ) ASC)+(SELECT COUNT(FTShdDocNo) FROM TPSTSalDT WHERE FTShdDocNo = HD.FTShdDocNo AND ISNULL( FTSdtStaPdt, '' ) IN ('1','2','3')) ) + @deli +") '*Em 59-06-19 ให้ลำดับรายการต่อจาก DT
        'oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (ISNULL( RC.FCSrcAmt, 0 ) - ISNULL( RC.FCSrcChg, 0 ))),128) + @deli +")
        oSBCmd.AppendLine("( CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 9 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='SALETYPE002')")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='SALETYPE001') END ) + @deli +")  '[8]RETAILTYPECODE
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("'1' + @deli +")
        oSBCmd.AppendLine("(Select Case when ISNULL (FTSysUsrValue,'' ) <> ''  then FTSysUsrValue ELSE 'Deposit' END  FROM TSysConfig WHERE FTSysCode = 'ATOSM1') + @deli + ")
        oSBCmd.AppendLine("'1' + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        'oSBCmd.AppendLine("@deli3 + CONVERT( VARCHAR, CONVERT( DECIMAL(18,2),ISNULL ( RC.FCSrcAmt, 0 ) - ISNULL( RC.FCSrcChg, 0 )),128)  + @deli +")
        oSBCmd.AppendLine("@deli3 + CONVERT( VARCHAR, CONVERT( DECIMAL(18,2),ROUND( ( (ISNULL ( RC.FCSrcAmt, 0 ) - ISNULL( RC.FCSrcChg, 0 )) * 100 ) / ( 100 + @vatrate ) , @rnd ) ),128)  + @deli +")    '*Em 59-06-19  ให้ส่งมูลค่าถอด Vat ไป
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine("@deli3 + CONVERT( VARCHAR, CONVERT( DECIMAL(18,2),ISNULL ( RC.FCSrcAmt, 0 ) - ISNULL( RC.FCSrcChg, 0 )),128)  + @deli +")
        oSBCmd.AppendLine("'' + @deli +")
        oSBCmd.AppendLine(" ''")
        oSBCmd.AppendLine("AS xData, @Tender xType, RC.FTShdDocNo, RC.FNSrcSeqNo FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalRC RC LEFT OUTER JOIN TPSTSalHD HD ON ( RC.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE RC.FTRcvCode NOT IN ( '004' ) AND @code in ( HD.FTShdDocNo, '' )")
        oSBCmd.AppendLine("AND RC.FTRcvCode = '011'")
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')")



        '--------------------------------------------------------------------------------------------
        '### Tender
        oSBCmd.AppendLine("UNION")
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Tender + @deli +")  '[1]Type
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') + @deli +")   '[4]TRANSACTIONTYPECODE NEW '*TON 59-03-02
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( RC.FNSrcSeqNo, 0 ) ASC) ) + @deli +")   '[7]TENDERSEQUENCENUMBER
        '[8]TENDERTYPECODE NEW '*TON 03-14
        oSBCmd.AppendLine("(CASE RC.FTRcvCode")
        oSBCmd.AppendLine("WHEN '001' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE001')/*Cash*/")
        oSBCmd.AppendLine("WHEN '002' THEN ")
        oSBCmd.AppendLine("CASE ")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'AMEX' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE003')   /*AMEX*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'BBLET', 'BBLVS' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE017')/*BBL Visa*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'BBLMC' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE018')/*BBL Master*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'BAYET', 'BAYVS' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE019') /*BAY Visa*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'BAYMC' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE020') /*BAY Master*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'TFBET', 'TFBVS' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE021') /*K bank Visa*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'TFBMC' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE022')/*K bank Master*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'SCBET', 'SCBVS' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE023')/*SCB Visa*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'SCBMC' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE024')/*SCB Master*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTEAEOV ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE025')/*AEON Visa*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTEUOBV ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE026')/*UOB Visa*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'KTBET', 'KTBVS' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE027')/*KTC Visa*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'KTBMC' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE028') /*KTC Master*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTELOBL ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE029')/*BBL 0%*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTELOBY ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE030')/*BAY 0%*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTELOKB ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE031') /*K bank 0%*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTELOFC ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE032')/*First Choice 0%*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTELOAE ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE033')/*AEON 0%*/  ")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTECRED ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE016')/*CreditZeroDay*/")
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CRTEUOBM ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE036')/*UOB Master*/") 'PAN 59-03-25 เพิ่ม UOB , JCB 
        oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'JCB' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE037')/*JCB*/")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE035') /*Other tender VISA Master*/")
        oSBCmd.AppendLine("END")
        oSBCmd.AppendLine("WHEN '003' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE014')/*Cheque*/")
        'oSBCmd.AppendLine("WHEN '005' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE015')/*BankTransfer*/")
        oSBCmd.AppendLine("WHEN '008' THEN")
        oSBCmd.AppendLine("CASE")
        oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VOTECOUP ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE008')/*SupplierCoupon*/")
        oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VOTECSCP ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE012')/*Cash/Corp coupon*/")
        'oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VOTEBTNF ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE015')/*BankTransfer*/")
        oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VOTEBTNF1 ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE038')/*Bank transfer(KBA-บางพลี)*/")
        oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VOTEBTNF2 ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE039')/*Bank transfer(KBA-มาบตาพุด)*/")
        oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VOTEBTNF3 ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE040')/*Bank transfer(KBA-ศรีราชา)*/")
        oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VOTEBMNF ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE034')/*MoneyTransfer*/")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE007')/*VoucherTender*/")
        oSBCmd.AppendLine("END")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE099')/*Other tender*/ ")
        oSBCmd.AppendLine("END) + @deli + ") 'END [8]TENDERTYPECODE new
        'oSBCmd.AppendLine("( CASE RC.FTRcvCode")   '[8]TENDERTYPECODE
        'oSBCmd.AppendLine("WHEN '001' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TENDTYPE001')  /*Cash*/")
        'oSBCmd.AppendLine("WHEN '002' THEN  /*Credit Card*/")
        'oSBCmd.AppendLine("CASE")
        'oSBCmd.AppendLine("WHEN RIGHT( RC.FTBnkCode, 2 ) IN ( 'ET', 'VS', 'MC' ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE002')")  'VISA Electron, VISA, MasterCard
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'VISA', 'MAS' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE002')") 'VISA, MasterCard
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'AMEX' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE003')")
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'DIN' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE004')")
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'CUP' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE005') /*62%-1*/")
        'oSBCmd.AppendLine("WHEN ( LEFT( ISNULL( RC.FTSrcRef, '' ), 4 ) BETWEEN '6200' AND '6299' ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE005') /*62%-2*/")
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'JCB' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE006')")
        ''oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'AMEX' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE003')")
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( 'MPoint' ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE009') /*M-Point*/")
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CreditCardPersonalLoanTenderCode ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE011')") 'AEON, First Choice *CH 29-11-2014
        'oSBCmd.AppendLine("WHEN ( RC.FTBnkCode IN ( @CreditCardCredit ) ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE016')") '*TON 59-03-03
        'oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE099')")
        'oSBCmd.AppendLine("End")
        'oSBCmd.AppendLine("WHEN '003' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TENDTYPE014')  /*Cheque*/")
        'oSBCmd.AppendLine("WHEN '005' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TENDTYPE015')  /*BankTransfer*/")
        'oSBCmd.AppendLine("WHEN '008' THEN 	/*Voucher*/")
        'oSBCmd.AppendLine("CASE")
        'oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VoucherDisTenderCode ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE008')")   'DiscountTender
        'oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VoucherMPointTenderCode ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE009')")    'M-PointTender
        'oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VoucherCashCouponTenderCode ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE012')") 'Cash/Corp coupon *CH 29-11-2014
        'oSBCmd.AppendLine("WHEN ISNULL( RC.FTBnkCode, '' ) IN ( @VoucherBankTransferTenderCode ) THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE015')") '*TON 59-03-03
        'oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE007')")    'VoucherTender
        'oSBCmd.AppendLine("End")
        'oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE009') END ) + @deli +")
        'oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( RC.FCSrcNet, 0 ) ),128) + @deli +") '[9]TENDERAMOUNT   '*Em 57-10-24  ต้องใส่ Style เข้าไปด้วยไม่อย่างนั้นจะถูกตัดเหลือแค่ 6 หลัก ไม่รวมจุดทศนิยม
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (ISNULL( RC.FCSrcAmt, 0 ) - ISNULL( RC.FCSrcChg, 0 ))),128) + @deli +") '[9]TENDERAMOUNT   '*Em 58-02-06  ยอดที่ชำระ - เงินทอน
        oSBCmd.AppendLine("@tRate + @deli +")   '[10]TENDERCURRENCY
        oSBCmd.AppendLine("'' + @deli +")   '[11]TENDERCURRENCY_ISO	'blank
        oSBCmd.AppendLine("CASE")   '[12]TENDERID
        oSBCmd.AppendLine("WHEN ( RC.FTRcvCode = '008' ) THEN")
        oSBCmd.AppendLine("CASE WHEN ISNULL( RC.FTBnkCode, '' ) NOT IN ( SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='VOTEMPOI' ) THEN RC.FTSrcRef ELSE '' END") '[12]TENDERID New '*TON 59-03-02
        'oSBCmd.AppendLine("CASE WHEN ISNULL( RC.FTBnkCode, '' ) NOT IN ( @VoucherMPointTenderCode ) THEN RC.FTSrcRef ELSE '' END")
        oSBCmd.AppendLine("ELSE ''")
        oSBCmd.AppendLine("END")
        oSBCmd.AppendLine("AS xData, @Tender xType, RC.FTShdDocNo, RC.FNSrcSeqNo FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalRC RC LEFT OUTER JOIN TPSTSalHD HD ON ( RC.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE RC.FTRcvCode NOT IN ( '004','011' ) AND @code in ( HD.FTShdDocNo, '' )")    'ไม่รวม (คูปองส่วนลด)
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07
        '-----------------------------------------------------------------------------------------


        oSBCmd.AppendLine("UNION")

        oSBCmd.AppendLine("SELECT ")
        oSBCmd.AppendLine("@Tender + @deli +")  '[1]Type
        ' oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( HD.FTShdDocNo, 0 ) ASC) +")
        oSBCmd.AppendLine("(SELECT COUNT( RC.FNSrcSeqNo )")
        oSBCmd.AppendLine("FROM TPSTSalRC RC LEFT OUTER JOIN TPSTSalHD HD ON ( RC.FTShdDocNo = HD.FTShdDocNo )")
        'oSBCmd.AppendLine("WHERE RC.FTRcvCode NOT IN ( '004' ) AND @code in ( HD.FTShdDocNo, '' ))")
        oSBCmd.AppendLine("WHERE RC.FTRcvCode NOT IN ( '004','011' ) AND @code in ( HD.FTShdDocNo, '' ))")  '*Em 59-06-19  ต้องไม่รวมมัดจำ
        oSBCmd.AppendLine(") + @deli +")   '[7]TENDERSEQUENCENUMBER
        oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE010') + @deli +")  '[8]TENDERTYPECODE
        oSBCmd.AppendLine("CONVERT( VARCHAR, ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ( ISNULL( rHD.FCShdRnd, 0 ) * -1 ) ) + @deli +")    '[9]TENDERAMOUNT
        oSBCmd.AppendLine("@tRate + @deli +")   '[10]TENDERCURRENCY
        oSBCmd.AppendLine("'' + @deli +")   '[11]TENDERCURRENCY_ISO	'blank
        oSBCmd.AppendLine("''") '[12]TENDERID
        oSBCmd.AppendLine("AS xData, @Tender2 xType, rHD.FTShdDocNo, 9999 AS FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalHD rHD LEFT OUTER JOIN TPSTSalHD HD ON ( rHD.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE ISNULL( rHD.FCShdRnd, 0 ) <> 0 AND @code in ( HD.FTShdDocNo, '' )")
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07

        'แสดง ZT13 Diff ของ Tender '*CH 18-12-2014
        If cDiffNet <> 0 Then
            oSBCmd.AppendLine("UNION")
            oSBCmd.AppendLine("SELECT ")
            oSBCmd.AppendLine("@Tender + @deli +")  '[1]Type
            'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
            oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
            oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
            'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
            'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
            oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
            oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
            oSBCmd.AppendLine("ELSE")
            oSBCmd.AppendLine("(CASE HD.FTShdDocType")
            oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
            oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
            oSBCmd.AppendLine("End)")
            oSBCmd.AppendLine("END )+ @deli + ")
            oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
            oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
            oSBCmd.AppendLine("'1' + @deli +")   '[7]TENDERSEQUENCENUMBER
            oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TENDTYPE013') + @deli +")  '[8]TENDERTYPECODE
            oSBCmd.AppendLine("'" & cDiffNet.ToString("#,##0." & StrDup(2, "0")) & "' + @deli +")    '[9]TENDERAMOUNT
            oSBCmd.AppendLine("@tRate + @deli +")   '[10]TENDERCURRENCY
            oSBCmd.AppendLine("'' + @deli +")   '[11]TENDERCURRENCY_ISO	'blank
            oSBCmd.AppendLine("''") '[12]TENDERID
            oSBCmd.AppendLine("AS xData, @Tender2 xType, rHD.FTShdDocNo, 9999 AS FNSdtSeqNo")
            oSBCmd.AppendLine("FROM TPSTSalHD rHD LEFT OUTER JOIN TPSTSalHD HD ON ( rHD.FTShdDocNo = HD.FTShdDocNo )")
            oSBCmd.AppendLine("WHERE @code in ( HD.FTShdDocNo, '' )")
            oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07
        End If

        oSBCmd.AppendLine(") xSale")
        oSBCmd.AppendLine("WHERE @code in ( FTShdDocNo, '' )")
        oSBCmd.AppendLine("ORDER BY")
        oSBCmd.AppendLine("xSale.FTShdDocNo Asc")
        oSBCmd.AppendLine(", ( CASE xType")
        oSBCmd.AppendLine("WHEN @Head THEN 1")
        oSBCmd.AppendLine("WHEN @Item THEN 2")
        oSBCmd.AppendLine("WHEN @Tender THEN 3")
        oSBCmd.AppendLine("WHEN @Tender2 THEN 4")
        oSBCmd.AppendLine("ELSE 999 END )")
        oSBCmd.AppendLine(", xSale.FNSdtSeqNo ASC")
        Return oSBCmd.ToString()
    End Function
    Private Sub C_GETxCmdTax(ptDocNo As String, ptCompNo As String, ptFileName As String, ptBchCode As String)
        ptBchCode = "'" & ptBchCode & "'"
        Dim oSBCmd As New System.Text.StringBuilder("")
        oSBCmd.AppendLine("DECLARE @code AS VARCHAR(30)")
        oSBCmd.AppendLine("DECLARE @Tax AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @vatrate AS DECIMAL")
        oSBCmd.AppendLine("DECLARE @deli AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @deli2 AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @comp AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @option_weight_LEN AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @tTranType AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @tRate AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @tVatAmt AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @rnd AS INTEGER")   '*Em 59-06-19
        oSBCmd.AppendLine("SET @code = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SET @deli2 = ':'")
        oSBCmd.AppendLine("SET @Tax = 'TAX' + @deli2")
        oSBCmd.AppendLine("SET @option_weight_LEN = ISNULL( ( SELECT FTSysUsrValue FROM TSysConfig WHERE FTSysCode = 'ALenScales' AND FTSysSeq = '001' ), 7 )")
        oSBCmd.AppendLine("SET @vatrate = ISNULL( ( SELECT TOP 1 isnull( FCCmpVatAmt, 7 ) FROM TCNMComp ), 7 )")
        oSBCmd.AppendLine("SET @comp = '" & ptCompNo & "'")
        oSBCmd.AppendLine("SET @tTranType = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPE')")
        oSBCmd.AppendLine("SET @tRate = 'THB'")
        oSBCmd.AppendLine("SET @deli = '" & tSpr & "'")
        oSBCmd.AppendLine("SET @tVatAmt = CONVERT( VARCHAR, (SELECT ISNULL(FCCmpVatAmt,'') FROM TCNMComp))")
        oSBCmd.AppendLine("SET @vatrate = ISNULL( ( SELECT TOP 1 isnull( FCCmpVatAmt, 7 ) FROM TCNMComp ), 7 )") '*Em 59-06-19
        oSBCmd.AppendLine("SET @rnd = ISNULL( ( SELECT ISNULL( FTSysUsrValue, 2 ) FROM TSysConfig WHERE FTSysCode IN ( 'ADecPntSF' ) ), 2 )") '*Em 59-06-19
        oSBCmd.AppendLine("SELECT xSale.xData FROM (")
        '### Tax
        'oSBCmd.AppendLine("UNION")
        oSBCmd.AppendLine("SELECT ")
        oSBCmd.AppendLine("@Tax + @deli +") '[1]Type
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') + @deli +")   '[4]TRANSACTIONTYPECODE NEW '*TON 59-03-02
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        'oSBCmd.AppendLine("CONVERT( VARCHAR, ISNULL( DT.FNSdtSeqNo, 0 ) ) + @deli +")   '[7]RETAILSEQUENCENUMBER
        oSBCmd.AppendLine("'{0}' + @deli +")   '[7]RETAILSEQUENCENUMBER
        oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) + @deli +")  '[8]TAXSEQUENCENUMBER
        oSBCmd.AppendLine("CASE DT.FTSdtVatType WHEN '2' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE001')")
        oSBCmd.AppendLine("ELSE CASE @tVatAmt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE001')")
        oSBCmd.AppendLine("WHEN '0' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE002')")
        oSBCmd.AppendLine("WHEN '7' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE003')")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE004') END ")   '[9]TAXTYPECODE     
        oSBCmd.AppendLine("END + @deli + ")
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( DT.FCSdtVat, 0 ) ),128 )")  '[10]TAXAMOUNT
        oSBCmd.AppendLine("AS xData, @Tax xType, DT.FTShdDocNo, DT.FNSdtSeqNo FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE ISNULL( DT.FTSdtVatType, '2' ) NOT IN ( '2' )")
        'oSBCmd.AppendLine("AND ISNULL( DT.FCSdtVat, 0 ) > 0")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4', '5' ) OR ( ( ISNULL( DT.FTSdtStaPdt, '' ) IN ( '5' ) ) AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 ) ) )")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) )")
        oSBCmd.AppendLine("AND ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3')")
        oSBCmd.AppendLine("AND @code in ( HD.FTShdDocNo, '' )")
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07

        '*Em 59-06-17 ++++++++++
        oSBCmd.AppendLine("UNION")
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Tax + @deli +")
        ' oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +")
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")
        oSBCmd.AppendLine("'{0}' + @deli +")
        oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( RC.FNSrcSeqNo, 0 ) ASC)+(SELECT COUNT(FTShdDocNo) FROM TPSTSalDT WHERE FTShdDocNo = HD.FTShdDocNo AND ISNULL( FTSdtStaPdt, '' ) IN ('1','2','3') AND ISNULL(FTSdtVatType,'2') = '1') ) + @deli +")
        oSBCmd.AppendLine("CASE '1' WHEN '2' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE001')")
        oSBCmd.AppendLine("ELSE CASE @tVatAmt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE001')")
        oSBCmd.AppendLine("WHEN '0' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE002')")
        oSBCmd.AppendLine("WHEN '7' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE003')")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TAXTYPE004') END ")
        oSBCmd.AppendLine("END + @deli + ")
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN 1 ELSE -1 END ) * (ROUND((ISNULL ( RC.FCSrcAmt, 0 ) - ISNULL( RC.FCSrcChg, 0 ))-( ( (ISNULL ( RC.FCSrcAmt, 0 ) - ISNULL( RC.FCSrcChg, 0 )) * 100 ) / ( 100 + @vatrate )) , @rnd )) ),128 )")
        oSBCmd.AppendLine("AS xData, @Tax xType, RC.FTShdDocNo, RC.FNSrcSeqNo FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalRC RC LEFT OUTER JOIN TPSTSalHD HD ON ( RC.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE @code in ( HD.FTShdDocNo, '' )")
        oSBCmd.AppendLine("AND RC.FTRcvCode = '011'")
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')")
        '+++++++++++++++++++++++

        oSBCmd.AppendLine(") xSale")
        oSBCmd.AppendLine("WHERE @code in ( FTShdDocNo, '' )")
        oSBCmd.AppendLine("ORDER BY")
        oSBCmd.AppendLine("xSale.FTShdDocNo Asc")
        'oSBCmd.AppendLine(", xSale.FNSdtSeqNo ASC")
        'Return oSBCmd.ToString()

        Dim oDataTemp = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)
        Dim tSqlSeq = "SELECT xDT.nSeq "
        tSqlSeq &= "FROM ("
        tSqlSeq &= "SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) AS nSeq"
        tSqlSeq &= ",DT.FTSdtVatType,DT.FCSdtVat "
        tSqlSeq &= "FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo ) "
        tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' "
        'tSqlSeq &= "AND ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4', '5' ) OR ( ( ISNULL( DT.FTSdtStaPdt, '' ) IN ( '5' ) ) AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 ) ) )"  '*Tee YYYYYYYYYYYY
        'tSqlSeq &= "AND ( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) )"
        tSqlSeq &= "AND ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3')"
        tSqlSeq &= "AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')" '*TON 59-03-07
        '*Em 59-06-19 +++++++
        tSqlSeq &= "UNION "
        tSqlSeq &= "SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( RC.FNSrcSeqNo, 0 ) ASC)+(SELECT COUNT(FTShdDocNo) FROM TPSTSalDT WHERE FTShdDocNo = HD.FTShdDocNo AND ISNULL( FTSdtStaPdt, '' ) IN ('1','2','3') AND ISNULL(FTSdtVatType,'2') = '1'))  AS nSeq "
        tSqlSeq &= ",'' AS FTSdtVatType,0 AS FCSdtVat "
        tSqlSeq &= "FROM TPSTSalRC RC LEFT OUTER JOIN TPSTSalHD HD ON ( RC.FTShdDocNo = HD.FTShdDocNo ) "
        tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' "
        tSqlSeq &= "AND RC.FTRcvCode = '011'"
        tSqlSeq &= "AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')"
        '++++++++++++++++++++
        tSqlSeq &= ") xDT "
        tSqlSeq &= "WHERE ISNULL( xDT.FTSdtVatType, '2' ) NOT IN ( '2' ) "
        'tSqlSeq &= "AND ISNULL( xDT.FCSdtVat, 0 ) > 0"
        Dim oDbTblSeq = oDatabase.C_CALoExecuteReader(tSqlSeq)
        Dim oDbTblRep = New DataTable
        oDbTblRep.Columns.Add("xData")

        If Not oDataTemp Is Nothing Then
            If oDataTemp.Rows.Count > 0 Then
                'Replace SeqNo Tender
                If oDataTemp.Rows.Count <> oDbTblSeq.Rows.Count Then Exit Sub
                For nTmp = 0 To oDataTemp.Rows.Count - 1
                    Dim oDbRow As DataRow
                    'oDataTemp.Rows(nTmp).Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oDbTblSeq.Rows(nTmp).Item("nSeq"))

                    oDbRow = oDbTblRep.NewRow()
                    oDbRow.Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oDbTblSeq.Rows(nTmp).Item("nSeq"))
                    oDbTblRep.Rows.Add(oDbRow)
                Next

                Dim oExportTable = New cExport()
                With oExportTable
                    'Temp To File Text
                    .C_CALxTempToFileText(ptFileName, oDbTblRep)
                End With
            End If
        End If
    End Sub

    Private Sub C_GETxCmdDiscount(ptDocNo As String, ptCompNo As String, ptFileName As String, ptBchCode As String)
        ptBchCode = "'" & ptBchCode & "'"
        Dim oSBCmd As New System.Text.StringBuilder("")
        oSBCmd.AppendLine("DECLARE @code AS VARCHAR(30)")
        oSBCmd.AppendLine("DECLARE @Discount AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @Discount2 AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @rnd AS INTEGER")
        oSBCmd.AppendLine("DECLARE @vatrate AS DECIMAL")
        oSBCmd.AppendLine("DECLARE @deli AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @deli2 AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @comp AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @option_weight_LEN AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @tTranType AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @tRate AS VARCHAR(5)")
        'oSBCmd.AppendLine("DECLARE @nRowDis AS INTEGER")
        oSBCmd.AppendLine("SET @code = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SET @deli2 = ':'")
        oSBCmd.AppendLine("SET @Discount = 'DISCOUNT' + @deli2")
        oSBCmd.AppendLine("SET @Discount2 = 'DISCOUNT2' + @deli2")
        oSBCmd.AppendLine("SET @option_weight_LEN = ISNULL( ( SELECT FTSysUsrValue FROM TSysConfig WHERE FTSysCode = 'ALenScales' AND FTSysSeq = '001' ), 7 )")
        oSBCmd.AppendLine("SET @rnd = ISNULL( ( SELECT ISNULL( FTSysUsrValue, 2 ) FROM TSysConfig WHERE FTSysCode IN ( 'ADecPntSF' ) ), 2 )")
        oSBCmd.AppendLine("SET @vatrate = ISNULL( ( SELECT TOP 1 isnull( FCCmpVatAmt, 7 ) FROM TCNMComp ), 7 )")
        oSBCmd.AppendLine("SET @comp = '" & ptCompNo & "'")
        oSBCmd.AppendLine("SET @tTranType = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPENOM')") '*TON 59-03-02
        oSBCmd.AppendLine("SET @tRate = 'THB'")
        oSBCmd.AppendLine("SET @deli = '" & tSpr & "'")
        'oSBCmd.AppendLine("SET @nRowDis = ISNULL((SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( HD.FTShdDocNo, 0 ) ASC) )")
        'oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        'oSBCmd.AppendLine("WHERE ISNULL( DT.FCSdtDisAvg, 0 ) > 0 AND HD.FTShdDocNo = @code),0)")

        oSBCmd.AppendLine("SELECT xSale.xData,xSale.FTSdtBarCode FROM (")

        '### Discount
        '001
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Discount + @deli +")    '[1]Type
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("'{0}' + @deli +")   '[7]RETAILSEQUENCENUMBER
        oSBCmd.AppendLine("'{1}' + @deli +")    '[8]DISCOUNTSEQUENCENUMBER
        oSBCmd.AppendLine("CASE WHEN ISNULL( DT.FCSdtDisAvg, 0) > 0 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='DISCTYPE001') END + @deli +")   '[9]DISCOUNTTYPECODE	'ส่วนลด
        'oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='DISCTYPE002') + @deli +")   '[9]DISCOUNTTYPECODE	'ส่วนลด
        oSBCmd.AppendLine("'' + @deli +")   '[10]DISCOUNTREASONCODE	'blank
        'oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (ISNULL( DT.FCSdtDis, 0 ) - ISNULL( DT.FCSdtChg, 0 )  + ISNULL( DT.FCSdtFootAvg, 0 ) + ISNULL( DT.FCSdtRePackAvg, 0 ))) ) + @deli +") '[11]REDUCTIONAMOUNT   '*Em 57-12-12
        oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ( ISNULL( DT.FCSdtDisAvg, 0 ) ) ) ) + @deli +")  '[11]REDUCTIONAMOUNT  'Price Off '*CH 13-01-2015
        oSBCmd.AppendLine("'' + @deli +")   '[12]STOREFINANCIALLEDGERACCOUNTID	'blank
        oSBCmd.AppendLine("''") '[13]DISCOUNTID	'blank
        oSBCmd.AppendLine("AS xData, @Discount2 xType, DT.FTShdDocNo, DT.FNSdtSeqNo FNSdtSeqNo,DT.FTSdtBarCode")
        oSBCmd.AppendLine(",CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ( ISNULL( DT.FCSdtDisAvg, 0 ) ) ) ) AS Amt")
        oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE HD.FTShdDocNo = @code")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FCSdtDis, 0 ) - ISNULL( DT.FCSdtChg, 0 ) ) + ( ISNULL( DT.FCSdtFootAvg, 0 ) + ISNULL( DT.FCSdtRePackAvg, 0 ) ) > 0")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4' ) )")
        oSBCmd.AppendLine("AND ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3')")
        oSBCmd.AppendLine("AND ( ISNULL( DT.FCSdtDisAvg, 0 )) > 0 ") '*TON 59-03-03
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07

        oSBCmd.AppendLine("UNION")
        '003
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Discount + @deli +")    '[1]Type
        ' oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("'{0}' + @deli +")   '[7]RETAILSEQUENCENUMBER
        oSBCmd.AppendLine("'{1}' + @deli +")   '[8]DISCOUNTSEQUENCENUMBER
        oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'') ='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='DISCTYPE003') + @deli +")  '[9]DISCOUNTTYPECODE	'โปรโมชั่น
        oSBCmd.AppendLine("'' + @deli +")   '[10]DISCOUNTREASONCODE   'blank
        'oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( DT.FCSdtDisAvg, 0 )) ) + @deli +")  '[11]REDUCTIONAMOUNT  '*Em 57-12-12
        'oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ( ISNULL( DT.FCSdtDisAvg, 0 ) + ISNULL( DT.FCShdDiscGP1, 0 ) ) ) ) + @deli +")  '[11]REDUCTIONAMOUNT  'Price Off '*CH 13-01-2015
        oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (ISNULL( DT.FCSdtDis, 0 ) - ISNULL( DT.FCSdtChg, 0 )  + ISNULL( DT.FCSdtFootAvg, 0 ) + ISNULL( DT.FCSdtRePackAvg, 0 ))) ) + @deli +") '[11]REDUCTIONAMOUNT   '*Em 57-12-12
        oSBCmd.AppendLine("'' + @deli +")   '[12]STOREFINANCIALLEDGERACCOUNTID	'blank
        oSBCmd.AppendLine("''") '[13]DISCOUNTID	'blank
        oSBCmd.AppendLine("AS xData, @Discount xType, DT.FTShdDocNo, DT.FNSdtSeqNo FNSdtSeqNo,DT.FTSdtBarCode")

        oSBCmd.AppendLine(",CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (ISNULL( DT.FCSdtDis, 0 ) - ISNULL( DT.FCSdtChg, 0 )  + ISNULL( DT.FCSdtFootAvg, 0 ) + ISNULL( DT.FCSdtRePackAvg, 0 ) ) ) ) AS Amt")
        oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE HD.FTShdDocNo = @code")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '5' ) ) AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 )")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 OR ISNULL( DT.FCShdDiscGP1, 0 ) > 0 )") 'Price Off '*CH 13-01-2015
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) )")
        oSBCmd.AppendLine("AND ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3')")
        oSBCmd.AppendLine("AND ABS(( ISNULL( DT.FCSdtDis, 0 ) - ISNULL( DT.FCSdtChg, 0 ) ) + ( ISNULL( DT.FCSdtFootAvg, 0 ) + ISNULL( DT.FCSdtRePackAvg, 0 ) )) > 0") '*TON 59-03-03
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07

        oSBCmd.AppendLine("UNION")
        '004
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@Discount + @deli +")    '[1]Type
        'oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine(ptBchCode & "+ @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        'oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        'PAN 2016-06-16 Check HD.FTShdRefInt = ''[ TRANSTYPENOM] <> ''[TRANSTYPEDPS] Type9[TRANSTYPEDPF]
        oSBCmd.AppendLine("(CASE HD.FTShdRefInt")
        oSBCmd.AppendLine("WHEN '' THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPENOM') ")
        oSBCmd.AppendLine("ELSE")
        oSBCmd.AppendLine("(CASE HD.FTShdDocType")
        oSBCmd.AppendLine("WHEN 1 THEN (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='TRANSTYPEDPS') ")
        oSBCmd.AppendLine("ELSE (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPEDPF')")
        oSBCmd.AppendLine("End)")
        oSBCmd.AppendLine("END )+ @deli + ")
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("'{0}' + @deli +")   '[7]RETAILSEQUENCENUMBER
        oSBCmd.AppendLine("'{1}' + @deli +")   '[8]DISCOUNTSEQUENCENUMBER
        oSBCmd.AppendLine("(SELECT CASE WHEN ISNULL(FTLNMUsrValue,'') ='' THEN FTLNMDefValue ELSE FTLNMUsrValue END from TLNKMapping WHERE FTLNMCode='DISCTYPE004') + @deli +")  '[9]DISCOUNTTYPECODE	'โปรโมชั่น
        oSBCmd.AppendLine("'' + @deli +")   '[10]DISCOUNTREASONCODE   'blank
        'oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( DT.FCSdtDisAvg, 0 )) ) + @deli +")  '[11]REDUCTIONAMOUNT  '*Em 57-12-12
        oSBCmd.AppendLine("CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (((ISNULL(FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(FCSdtQty,1))-ISNULL(FCSdtSetPrice,0)))*ISNULL(FCSdtQty,0) ) ) ) + @deli +")  '[11]REDUCTIONAMOUNT  'Price Off '*CH 13-01-2015
        oSBCmd.AppendLine("'' + @deli +")   '[12]STOREFINANCIALLEDGERACCOUNTID	'blank
        oSBCmd.AppendLine("''") '[13]DISCOUNTID	'blank
        oSBCmd.AppendLine("AS xData, @Discount xType, DT.FTShdDocNo, DT.FNSdtSeqNo FNSdtSeqNo,DT.FTSdtBarCode")
        oSBCmd.AppendLine(",CONVERT(VARCHAR,CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (((ISNULL(FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(FCSdtQty,1))-ISNULL(FCSdtSetPrice,0)))*ISNULL(FCSdtQty,0) ) ) ) AS Amt")
        oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("WHERE HD.FTShdDocNo = @code")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '5' ) ) AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 )")
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 OR ISNULL( DT.FCShdDiscGP1, 0 ) > 0 )") 'Price Off '*CH 13-01-2015
        'oSBCmd.AppendLine("AND ( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) )")
        oSBCmd.AppendLine("AND ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3')")
        oSBCmd.AppendLine("AND (ABS(((ISNULL(FCSdtSalePrice,0)-(ISNULL(DT.FCShdDiscGP1,0)/ISNULL(FCSdtQty,1))-ISNULL(FCSdtSetPrice,0)))*ISNULL(FCSdtQty,0))) > 0") '*TON 59-03-03
        oSBCmd.AppendLine("AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9')") '*TON 59-03-07

        oSBCmd.AppendLine(") xSale")
        oSBCmd.AppendLine("WHERE @code in ( FTShdDocNo, '' )")
        oSBCmd.AppendLine("AND xSale.Amt <> CONVERT(VARCHAR, CONVERT(DECIMAL(18,2), 0) )")
        oSBCmd.AppendLine("ORDER BY")
        oSBCmd.AppendLine("xSale.FTShdDocNo Asc")
        oSBCmd.AppendLine(", xSale.FNSdtSeqNo ASC")

        Dim oDataTemp = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)
        'Dim tSqlSeq = "SELECT xDT.nSeq FROM("
        'tSqlSeq &= "SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) AS nSeq "
        'tSqlSeq &= ",DT.FCSdtDis,DT.FCSdtChg,DT.FCSdtFootAvg,DT.FCSdtRePackAvg,DT.FCSdtDisAvg,DT.FCShdDiscGP1 "
        ''tSqlSeq &= ",DT.FTSdtBarCode "
        'tSqlSeq &= "FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo ) "
        'tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' "
        'tSqlSeq &= "AND ( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4', '5' ) OR ( ( ISNULL( DT.FTSdtStaPdt, '' ) IN ( '5' ) ) AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 ) ) "
        'tSqlSeq &= ") xDT "
        'tSqlSeq &= "WHERE (( ISNULL( xDT.FCSdtDis, 0 ) - ISNULL( xDT.FCSdtChg, 0 ) ) + ( ISNULL( xDT.FCSdtFootAvg, 0 ) + ISNULL( xDT.FCSdtRePackAvg, 0 ) ) > 0 "
        'tSqlSeq &= "OR ( ISNULL( xDT.FCSdtDisAvg, 0 ) > 0 OR ISNULL( xDT.FCShdDiscGP1, 0 ) > 0 ) )"

        Dim tSqlSeq = ""
        tSqlSeq &= "SELECT xSeq.nSeq FROM(" & vbCrLf
        tSqlSeq &= "/* ZD01 */" & vbCrLf
        tSqlSeq &= "SELECT xDT.nSeq,xDT.FNSdtSeqNo FROM( "
        tSqlSeq &= "SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) AS nSeq,"
        tSqlSeq &= "DT.FCSdtDis,DT.FCSdtChg,DT.FCSdtFootAvg,DT.FCSdtRePackAvg,DT.FCSdtDisAvg,DT.FCShdDiscGP1,DT.FNSdtSeqNo "
        tSqlSeq &= "FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo ) "
        tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' AND "
        'tSqlSeq &= "( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4' ) OR ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 OR ISNULL( DT.FCShdDiscGP1, 0 ) > 0 ) ) "
        'tSqlSeq &= "( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) ) "
        tSqlSeq &= "ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') "
        tSqlSeq &= "AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9') " '*TON 59-03-07
        tSqlSeq &= ") xDT "
        tSqlSeq &= "WHERE ( ISNULL( xDT.FCSdtDisAvg, 0 )) > 0 "
        tSqlSeq &= "UNION ALL" & vbCrLf
        tSqlSeq &= "/* ZD03 */" & vbCrLf
        tSqlSeq &= "SELECT xDT.nSeq,xDT.FNSdtSeqNo FROM( "
        tSqlSeq &= "SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) AS nSeq,"
        tSqlSeq &= "DT.FCSdtDis,DT.FCSdtChg,DT.FCSdtFootAvg,DT.FCSdtRePackAvg,DT.FCSdtDisAvg,DT.FCShdDiscGP1,DT.FNSdtSeqNo "
        tSqlSeq &= "FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo ) "
        tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' AND "
        'tSqlSeq &= "( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4' ) OR ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 OR ISNULL( DT.FCShdDiscGP1, 0 ) > 0 ) ) "
        'tSqlSeq &= "( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) ) "
        tSqlSeq &= "ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') "
        tSqlSeq &= "AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9') " '*TON 59-03-07
        tSqlSeq &= ") xDT "
        tSqlSeq &= "WHERE ABS(( ISNULL( xDT.FCSdtDis, 0 ) - ISNULL( xDT.FCSdtChg, 0 ) ) + ( ISNULL( xDT.FCSdtFootAvg, 0 ) + ISNULL( xDT.FCSdtRePackAvg, 0 ) )) > 0" & vbCrLf
        'tSqlSeq &= ") xSeq "
        'tSqlSeq &= "ORDER BY xSeq.FNSdtSeqNo"
        '*TON 59-03-09
        tSqlSeq &= "UNION ALL" & vbCrLf
        tSqlSeq &= "/* ZD04 */" & vbCrLf
        tSqlSeq &= "SELECT xDT.nSeq,xDT.FNSdtSeqNo FROM( "
        tSqlSeq &= "SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) AS nSeq,"
        tSqlSeq &= "DT.FCSdtDis,DT.FCSdtChg,DT.FCSdtFootAvg,DT.FCSdtRePackAvg,DT.FCSdtDisAvg,DT.FCShdDiscGP1,DT.FNSdtSeqNo,DT.FCSdtSalePrice,DT.FCSdtSetPrice,DT.FCSdtQty "
        tSqlSeq &= "FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo ) "
        tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' AND "
        'tSqlSeq &= "( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( '4' ) OR ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 OR ISNULL( DT.FCShdDiscGP1, 0 ) > 0 ) ) "
        'tSqlSeq &= "( ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') OR ( DT.FTSdtStaPdt = '5' AND DT.FTSdtStaPrcStk = '1' ) ) "
        tSqlSeq &= "ISNULL( DT.FTSdtStaPdt, '' ) IN ('1','2','3') "
        tSqlSeq &= "AND CASE WHEN ISNULL(HD.FTShdAEType,'')<>'' THEN HD.FTShdAEType ELSE  HD.FTShdDocType END IN ('1','9') " '*TON 59-03-07
        tSqlSeq &= ") xDT "
        tSqlSeq &= "WHERE (ABS(((ISNULL(xDT.FCSdtSalePrice,0)-(ISNULL(xDT.FCShdDiscGP1,0)/ISNULL(xDT.FCSdtQty,1))-ISNULL(xDT.FCSdtSetPrice,0)))*ISNULL(xDT.FCSdtQty,0))) > 0" & vbCrLf
        tSqlSeq &= ") xSeq "
        tSqlSeq &= "ORDER BY xSeq.FNSdtSeqNo"
        Dim oDbTblSeq = oDatabase.C_CALoExecuteReader(tSqlSeq)
        Dim oDbTblRep = New DataTable
        oDbTblRep.Columns.Add("xData")

        If Not oDataTemp Is Nothing Then
            If oDataTemp.Rows.Count > 0 Then
                'Replace SeqNo Tender
                If oDataTemp.Rows.Count <> oDbTblSeq.Rows.Count Then Exit Sub
                For nTmp = 0 To oDataTemp.Rows.Count - 1
                    Dim oDbRow As DataRow
                    'oDataTemp.Rows(nTmp).Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oDbTblSeq.Rows(nTmp).Item("nSeq"))

                    'รายการซ้ำกัน Seq ผิด
                    'Dim oSeq = (From oItemSeq In oDbTblSeq Where oItemSeq.Item("FTSdtBarCode") = oDataTemp.Rows(nTmp).Item("FTSdtBarCode")).FirstOrDefault
                    'oDbRow = oDbTblRep.NewRow()
                    'If oSeq Is Nothing Then
                    '    oDbRow.Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oDbTblSeq.Rows(nTmp).Item("nSeq"), nTmp + 1)
                    'Else
                    '    oDbRow.Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oSeq.ItemArray(0), nTmp + 1)
                    'End If

                    oDbRow = oDbTblRep.NewRow()
                    oDbRow.Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oDbTblSeq.Rows(nTmp).Item("nSeq"), nTmp + 1)
                    oDbTblRep.Rows.Add(oDbRow)
                Next

                Dim oExportTable = New cExport()
                With oExportTable
                    'Temp To File Text
                    .C_CALxTempToFileText(ptFileName, oDbTblRep)
                End With
            End If
        End If
    End Sub

    Private Sub C_GETxCmdLoyaltyH(ptDocNo As String, ptCompNo As String, ptFileName As String)
        Dim oSBCmd As New System.Text.StringBuilder("")
        'Query Point Old
        oSBCmd.AppendLine("SELECT FTBchCode,FTCstCode,FTPntRefDoc,FNPntPointOld FROM TCNTCstPoint WHERE FTPntRefDoc = '" & ptDocNo & "'")
        Dim oDataPoint = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)
        Dim nPointOld As Integer = 0
        Dim bHavDoc As Boolean = False
        If Not oDataPoint Is Nothing Then
            If oDataPoint.Rows.Count > 0 Then
                nPointOld = Convert.ToInt32(oDataPoint.Rows(0).Item("FNPntPointOld"))
                bHavDoc = True
            End If
        End If
        If Not bHavDoc Then
            oDataPoint = New DataTable
            oDataPoint = oDatabase.C_CALoExecuteReader(oSBCmd.ToString, "", True)
            If Not oDataPoint Is Nothing Then
                If oDataPoint.Rows.Count > 0 Then
                    nPointOld = Convert.ToInt32(oDataPoint.Rows(0).Item("FNPntPointOld"))
                    bHavDoc = True
                End If
            End If
        End If

        oSBCmd = New System.Text.StringBuilder("")
        oSBCmd.AppendLine("DECLARE @code AS VARCHAR(30)")
        oSBCmd.AppendLine("DECLARE @LoyaltyH AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @Tender2 AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @rnd AS INTEGER")
        oSBCmd.AppendLine("DECLARE @vatrate AS DECIMAL")
        oSBCmd.AppendLine("DECLARE @deli AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @deli2 AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @comp AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @option_weight_LEN AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @tTranType AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @tRate AS VARCHAR(5)")
        'oSBCmd.AppendLine("DECLARE @nRowDis AS INTEGER")
        oSBCmd.AppendLine("DECLARE @nPointOld AS INTEGER")
        oSBCmd.AppendLine("SET @code = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SET @deli2 = ':'")
        oSBCmd.AppendLine("SET @LoyaltyH = 'LOYALTY_HEAD' + @deli2")
        oSBCmd.AppendLine("SET @option_weight_LEN = ISNULL( ( SELECT FTSysUsrValue FROM TSysConfig WHERE FTSysCode = 'ALenScales' AND FTSysSeq = '001' ), 7 )")
        oSBCmd.AppendLine("SET @rnd = ISNULL( ( SELECT ISNULL( FTSysUsrValue, 2 ) FROM TSysConfig WHERE FTSysCode IN ( 'ADecPntSF' ) ), 2 )")
        oSBCmd.AppendLine("SET @vatrate = ISNULL( ( SELECT TOP 1 isnull( FCCmpVatAmt, 7 ) FROM TCNMComp ), 7 )")
        oSBCmd.AppendLine("SET @comp = '" & ptCompNo & "'")
        oSBCmd.AppendLine("SET @tTranType = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPE')")
        oSBCmd.AppendLine("SET @tRate = 'THB'")
        oSBCmd.AppendLine("SET @deli = '" & tSpr & "'")
        'oSBCmd.AppendLine("SET @nRowDis = ISNULL((SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( HD.FTShdDocNo, 0 ) ASC) )")
        'oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        'oSBCmd.AppendLine("WHERE ISNULL( DT.FCSdtDisAvg, 0 ) > 0 AND HD.FTShdDocNo = @code),0)")
        oSBCmd.AppendLine("SET @nPointOld = " & nPointOld & "")

        oSBCmd.AppendLine("SELECT xSale.xData FROM (")

        '### LOYALTY_HEAD
        oSBCmd.AppendLine("SELECT ")
        oSBCmd.AppendLine("@LoyaltyH + @deli +")    '[1]Type
        oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( HD.FTShdDocNo, 0 ) ASC) ) + @deli +")   '[7]LOYALTYSEQUENCENUMBER
        oSBCmd.AppendLine("ISNULL( CST.FTCstCrdNo, '' ) + @deli +") '[8]CUSTOMERCARDNUMBER
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,0),")
        oSBCmd.AppendLine("( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * (ISNULL( FCShdMnyCxx, 0 ) - ") 'แต้มรวม
        oSBCmd.AppendLine("ISNULL( (")
        oSBCmd.AppendLine("SELECT SUM( ISNULL( PD.FCSpdPoint, 0 ) ) FROM TPSTSalPD PD WHERE  HD.FTShdDocNo = PD.FTShdDocNo AND ISNULL( PD.FCSpdPoint, 0 ) > 0 ), 0 ")
        oSBCmd.AppendLine(") ) ),128 ) + @deli + /*ลบ แต้มโปรโมชั่น*/")  '[9]LOYALTYPOINTSAWARDED
        oSBCmd.AppendLine("@comp + @deli +")    '[10]LOYALTYPROGRAMID
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( (")
        oSBCmd.AppendLine("SELECT SUM( ISNULL( dt.FCSdtNet, 0 ) ) - (SUM( ISNULL( dt.FCSdtFootAvg, 0 ) ) + SUM( ISNULL( dt.FCSdtRePackAvg, 0 ) ) + SUM( ISNULL( dt.FCSdtDisAvg, 0 ) ) )")
        oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TCNMPdt PDT ON ( DT.FTPdtCode = PDT.FTPdtCode )")
        oSBCmd.AppendLine("WHERE ( DT.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("AND ISNULL( PDT.FTPdtPoint, '' ) IN ( '1' )")
        oSBCmd.AppendLine("AND pdt.FTPdtCode = DT.FTPdtCode ), 0 ) ),128 ) + @deli +")    '[11]ELIGIBLEAMOUNT
        oSBCmd.AppendLine("CONVERT ( VARCHAR, CONVERT( DECIMAL(18,0), ( ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( FCShdMnyCxx, 0 ) ) + @nPointOld),128 ) + @deli +")   '[12]LOYALTYPOINTSTOTAL
        oSBCmd.AppendLine("''") '[13]CUSTOMERCARDTYPE	'blank
        oSBCmd.AppendLine("AS xData, @LoyaltyH xType, HD.FTShdDocNo, 0 FNSdtSeqNo")
        oSBCmd.AppendLine("FROM TPSTSalHD HD LEFT OUTER JOIN TCNMCst CST ON ( HD.FTCstCode = CST.FTCstCode )")
        oSBCmd.AppendLine("WHERE ISNULL( CST.FTCstCode, '' ) NOT IN ( '' )")
        oSBCmd.AppendLine("AND ISNULL( HD.FCShdMnyCxx, 0 ) > 0")
        oSBCmd.AppendLine("AND HD.FTShdDocNo = @code")

        oSBCmd.AppendLine(") xSale")
        oSBCmd.AppendLine("WHERE @code in ( FTShdDocNo, '' )")
        oSBCmd.AppendLine("ORDER BY")
        oSBCmd.AppendLine("xSale.FTShdDocNo Asc")
        oSBCmd.AppendLine(", xSale.FNSdtSeqNo ASC")

        Dim oDataTemp = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)

        If Not oDataTemp Is Nothing Then
            If oDataTemp.Rows.Count > 0 Then
                Dim oExportTable = New cExport()
                With oExportTable
                    'Temp To File Text
                    .C_CALxTempToFileText(ptFileName, oDataTemp)
                End With
            End If
        End If
    End Sub

    Private Sub C_GETxCmdLoyaltyD(ptDocNo As String, ptCompNo As String, ptFileName As String)
        Dim oSBCmd As New System.Text.StringBuilder("")
        oSBCmd.AppendLine("DECLARE @code AS VARCHAR(30)")
        oSBCmd.AppendLine("DECLARE @LoyaltyD AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @Tender2 AS VARCHAR(25)")
        oSBCmd.AppendLine("DECLARE @rnd AS INTEGER")
        oSBCmd.AppendLine("DECLARE @vatrate AS DECIMAL")
        oSBCmd.AppendLine("DECLARE @deli AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @deli2 AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @comp AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @option_weight_LEN AS VARCHAR(10)")
        oSBCmd.AppendLine("DECLARE @tTranType AS VARCHAR(5)")
        oSBCmd.AppendLine("DECLARE @tRate AS VARCHAR(5)")
        'oSBCmd.AppendLine("DECLARE @nRowDis AS INTEGER")
        oSBCmd.AppendLine("SET @code = '" & ptDocNo & "'")
        oSBCmd.AppendLine("SET @deli2 = ':'")
        oSBCmd.AppendLine("SET @LoyaltyD = 'LOYALTY_ITEM' + @deli2")
        oSBCmd.AppendLine("SET @option_weight_LEN = ISNULL( ( SELECT FTSysUsrValue FROM TSysConfig WHERE FTSysCode = 'ALenScales' AND FTSysSeq = '001' ), 7 )")
        oSBCmd.AppendLine("SET @rnd = ISNULL( ( SELECT ISNULL( FTSysUsrValue, 2 ) FROM TSysConfig WHERE FTSysCode IN ( 'ADecPntSF' ) ), 2 )")
        oSBCmd.AppendLine("SET @vatrate = ISNULL( ( SELECT TOP 1 isnull( FCCmpVatAmt, 7 ) FROM TCNMComp ), 7 )")
        oSBCmd.AppendLine("SET @comp = '" & ptCompNo & "'")
        oSBCmd.AppendLine("SET @tTranType = (SELECT CASE WHEN ISNULL(FTLNMUsrValue,'')='' THEN FTLNMDefValue ELSE FTLNMUsrValue END FROM TLNKMapping WHERE FTLNMCode='TRANSTYPE')")
        oSBCmd.AppendLine("SET @tRate = 'THB'")
        oSBCmd.AppendLine("SET @deli = '" & tSpr & "'")
        'oSBCmd.AppendLine("SET @nRowDis = ISNULL((SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( HD.FTShdDocNo, 0 ) ASC) )")
        'oSBCmd.AppendLine("FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo )")
        'oSBCmd.AppendLine("WHERE ISNULL( DT.FCSdtDisAvg, 0 ) > 0 AND HD.FTShdDocNo = @code),0)")

        oSBCmd.AppendLine("SELECT xSale.xData FROM (")

        ''### LOYALTY_ITEM
        'oSBCmd.AppendLine("")
        oSBCmd.AppendLine("SELECT")
        oSBCmd.AppendLine("@LoyaltyD + @deli +")    '[1]Type
        oSBCmd.AppendLine("@comp + ISNULL( HD.FTBchCode, '' ) + @deli +") '[2]RETAILSTOREID
        oSBCmd.AppendLine("ISNULL( CONVERT( VARCHAR(8), HD.FDShdDocDate, 112 ), '' ) + REPLACE( ISNULL( HD.FTShdDocTime, '' ), ':', '' ) + @deli +")  '[3]BUSINESSDAYDATE
        oSBCmd.AppendLine("@tTranType + @deli +")   '[4]TRANSACTIONTYPECODE
        oSBCmd.AppendLine("ISNULL( HD.FTPosCode, '' ) + @deli +")   '[5]WORKSTATIONID
        oSBCmd.AppendLine("ISNULL( HD.FTShdDocNo, '' ) + @deli +")  '[6]TRANSACTIONSEQUENCENUMBER
        oSBCmd.AppendLine("'{0}' + @deli +")    '[7]RETAILSEQUENCENUMBER
        oSBCmd.AppendLine("'{1}' + @deli +")  '[8]LOYALTYSEQUENCENUMBER
        oSBCmd.AppendLine("ISNULL( CST.FTCstCrdNo, '' ) + @deli +") '[9]CUSTOMERCARDNUMBER
        oSBCmd.AppendLine("@comp + @deli +")    '[10]LOYALTYPROGRAMID
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) *  ISNULL( PD.FCSpdPoint, 0 ) ),128 ) + @deli +")   '[11]LOYALTYPOINTSAWARDED
        oSBCmd.AppendLine("CONVERT( VARCHAR, CONVERT( DECIMAL(18,2), ( CASE HD.FTShdDocType WHEN 9 THEN -1 ELSE 1 END ) * ISNULL( PD.FCSdtNet, 0 ) ),128 ) + @deli +") '[12]ELIGIBLEAMOUNT
        oSBCmd.AppendLine("'' + @deli +")   '[13]LOYALTYPOINTSTOTAL	'blank
        oSBCmd.AppendLine("''") '[14]CUSTOMERCARDTYPE	'blank
        oSBCmd.AppendLine("AS xData")
        oSBCmd.AppendLine(", PD.FTShdDocNo, PD.FNSdtSeqNo FNSdtSeqNo, @LoyaltyD xType")
        oSBCmd.AppendLine("FROM TPSTSalPD PD")
        oSBCmd.AppendLine("LEFT OUTER JOIN TPSTSalHD HD ON ( PD.FTShdDocNo = HD.FTShdDocNo )")
        oSBCmd.AppendLine("LEFT OUTER JOIN TCNMCst CST ON ( HD.FTCstCode = CST.FTCstCode )")
        oSBCmd.AppendLine("WHERE ISNULL( CST.FTCstCode, '' ) NOT IN ( '' )")
        oSBCmd.AppendLine("AND ISNULL( PD.FCSpdPoint, 0 ) > 0")
        oSBCmd.AppendLine("AND HD.FTShdDocNo = @code")

        oSBCmd.AppendLine(") xSale")
        oSBCmd.AppendLine("WHERE @code in ( FTShdDocNo, '' )")
        oSBCmd.AppendLine("ORDER BY")
        oSBCmd.AppendLine("xSale.FTShdDocNo Asc")
        oSBCmd.AppendLine(", xSale.FNSdtSeqNo ASC")

        Dim oDataTemp = oDatabase.C_CALoExecuteReader(oSBCmd.ToString)
        Dim tSqlSeq = "Select xDT.nSeq "
        tSqlSeq &= "FROM ("
        tSqlSeq &= "SELECT CONVERT( VARCHAR, ROW_NUMBER() OVER(ORDER BY ISNULL( DT.FNSdtSeqNo, 0 ) ASC) ) AS nSeq,DT.FTSdtBarCode "
        tSqlSeq &= "FROM TPSTSalDT DT LEFT OUTER JOIN TPSTSalHD HD ON ( DT.FTShdDocNo = HD.FTShdDocNo ) "
        tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' AND "
        tSqlSeq &= "( ISNULL( DT.FTSdtStaPdt, '' ) NOT IN ( 4, 5 ) OR ( ( ISNULL( DT.FTSdtStaPdt, '' ) IN ( '5' ) ) AND ( ISNULL( DT.FCSdtDisAvg, 0 ) > 0 ) ) )"
        tSqlSeq &= ") xDT "
        tSqlSeq &= "WHERE xDT.FTSdtBarCode IN ("
        tSqlSeq &= "SELECT PD.FTSpdBarCode "
        tSqlSeq &= "FROM TPSTSalPD PD "
        tSqlSeq &= "LEFT OUTER JOIN TPSTSalHD HD ON ( PD.FTShdDocNo = HD.FTShdDocNo ) "
        tSqlSeq &= "LEFT OUTER JOIN TCNMCst CST ON ( HD.FTCstCode = CST.FTCstCode ) "
        tSqlSeq &= "WHERE HD.FTShdDocNo = '" & ptDocNo & "' "
        tSqlSeq &= "AND ISNULL( CST.FTCstCode, '' ) NOT IN ( '' ) "
        tSqlSeq &= "AND ISNULL( PD.FCSpdPoint, 0 ) > 0"
        tSqlSeq &= ")"
        Dim oDbTblSeq = oDatabase.C_CALoExecuteReader(tSqlSeq)
        Dim oDbTblRep = New DataTable
        oDbTblRep.Columns.Add("xData")

        If Not oDataTemp Is Nothing Then
            If oDataTemp.Rows.Count > 0 Then
                'Replace SeqNo Tender
                If oDataTemp.Rows.Count <> oDbTblSeq.Rows.Count Then Exit Sub
                For nTmp = 0 To oDataTemp.Rows.Count - 1
                    Dim oDbRow As DataRow
                    'oDataTemp.Rows(nTmp).Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oDbTblSeq.Rows(nTmp).Item("nSeq"))

                    oDbRow = oDbTblRep.NewRow()
                    oDbRow.Item("xData") = String.Format(oDataTemp.Rows(nTmp).Item("xData"), oDbTblSeq.Rows(nTmp).Item("nSeq"), nTmp + 1)
                    oDbTblRep.Rows.Add(oDbRow)
                Next

                Dim oExportTable = New cExport()
                With oExportTable
                    'Temp To File Text
                    .C_CALxTempToFileText(ptFileName, oDbTblRep)
                End With
            End If
        End If
    End Sub
    Public Sub C_CRTxCreateTempList()
        Dim oDatabase As New cDatabaseLocal
        Dim oSql As System.Text.StringBuilder

        Try
            'oSql = New System.Text.StringBuilder
            'oSql.AppendLine("IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].TempList') AND OBJECTPROPERTY(id, N'IsView') = 1)")
            'oSql.AppendLine("DROP TABLE [dbo].TempList")
            'oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
            oSql = New System.Text.StringBuilder
            oSql.AppendLine("IF NOT EXISTS(SELECT * FROM master.dbo.sysdatabases WHERE name = 'TempList]')")
            oSql.AppendLine("BEGIN")
            oSql.AppendLine("CREATE TABLE [dbo].[TempList](")
            oSql.AppendLine("[FTFileName] [varchar](255) Not NULL,")
            oSql.AppendLine("[FTFileRecord] [varchar](255) Not NULL,")
            oSql.AppendLine("[FTFilesize] [varchar](255) NULL,")
            oSql.AppendLine("CONSTRAINT [PK_TempList] PRIMARY KEY CLUSTERED (")
            oSql.AppendLine("[FTFileName] ASC,[FTFileRecord] ASC")
            oSql.AppendLine(")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]")
            oSql.AppendLine(") ON [PRIMARY]")
            oSql.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

        Catch ex As Exception
        Finally
            oDatabase = Nothing
            oSql = Nothing
        End Try
    End Sub
    Public Sub C_CALbSaveLog(ByVal ptDocDate As String, ByVal poDbTbl As DataTable)
        Dim tFmtLineHead As String = "[Export Date] {0}" & vbTab & "[Export Time] {1}" & vbTab & "[Export By] {2}"
        Dim tFmtLineColumn As String = "[Seq]" & vbTab & "[Doc Date]" & vbTab & "[Doc No.]" & vbTab & "[Total HD.]" & vbTab & "[Total DT.]" & vbTab & "[Total RC.]" ' & vbTab & "[ZT13]"
        Dim tFmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}" & vbTab & "{4}" & vbTab & "{5}" ' & vbTab & "{6}"
        Dim tDocDate As String = ptDocDate.Split("-")(2) & "/" & ptDocDate.Split("-")(1) & "/" & ptDocDate.Split("-")(0)
        Try
            'Count จำนวน Error มากว่า 0 ถึงจะสร้าง File Log
            If poDbTbl Is Nothing Then
                Exit Sub
            End If

            'ถ้าไม่มี Directory ให้สร้างขึ้นใหม่ '*CH 17-10-2014
            Dim tPath As String = ""
            Dim atPath As String() = tPathFileLog.Replace("/", "\").Split("\")
            For nPath = 0 To atPath.Length - 2
                tPath &= atPath(nPath) & "\"
            Next
            tPath = Left(tPath, tPath.Length - 1)
            If Not IO.Directory.Exists(tPath) Then
                IO.Directory.CreateDirectory(tPath)
            End If

            'ถ้าไม่มีไฟล์ให้สร้างขึ้นใหม่ '*CH 17-10-2014
            If Not File.Exists(tPathFileLog) Then
                Using oStreamWriter As New StreamWriter(tPathFileLog, False, System.Text.ASCIIEncoding.Default)
                    oStreamWriter.Close()
                End Using
            End If
            cCNVB.bVB_DocNotComplete = True
            cCNVB.tVB_MsgExportSale = String.Format(cCNMS.tMS_CN112, tPathFileLog)

            If poDbTbl.Rows.Count > 0 Then
                Using oStreamWriter As StreamWriter = File.AppendText(tPathFileLog)
                    With oStreamWriter
                        If Not bWriteLogExp Then
                            .WriteLine(String.Format(tFmtLineHead, Format(Date.Now, "dd/MM/yyyy"), Format(Date.Now, "HH:mm:ss"), My.Computer.Name))
                            .WriteLine(tFmtLineColumn)
                            .Flush()
                        End If
                        For Each oLog In poDbTbl.Rows '.OrderBy(Function(c) c.nLineNo)
                            oStreamWriter.WriteLine(String.Format(tFmtLineData, nSeqLogExp, tDocDate, oLog("FTShdDocNo"), oLog("HD_DataTotal"), oLog("DT_DataTotal"), oLog("RC_DataTotal"))) ', oLog("ZT13")
                            .Flush()
                            nSeqLogExp += 1
                        Next
                        .Close()
                    End With
                End Using
            End If
        Catch ex As Exception
        End Try
    End Sub

    Class cFileInfo
        Property tPathFile As String = ""
        Property tBchCode As String = ""
        Property nItem As Integer = 0
        Property tDocDate As String = ""
        Property tPathControl As String = ""
    End Class

    Class cListcontrol
        Property tPathFile As String = ""
        Property tSize As String = ""
        Property tRecord As String = ""
    End Class

#Region "ZSDINT005"
    Public Function C_CRTbCrateFileSale(ByRef poItem As List(Of cExportTemplate.cItem), ByVal ptPlant As String, ByVal pnRunning As Integer,
                                        Optional ptFlagFunc As String = cExportTemplate.cTableCode.ZSDINT005) As Boolean
        Dim bPrcCrt As Boolean = True
        Dim oZSDINT005 As New cZSDINT005
        Try
            If Not cCNVB.bVB_Auto Then
                If ptFlagFunc = cExportTemplate.cTableCode.ZSDINT005 Then wExports.oW_BackgroudWord.ReportProgress(0)
            End If

            'Get Data
            oZSDINT005 = C_GEToSaleZSDINT005(poItem, ptPlant)
            If Not cCNVB.bVB_Auto Then
                If ptFlagFunc = cExportTemplate.cTableCode.ZSDINT005 Then wExports.oW_BackgroudWord.ReportProgress(20)
            End If


            'Create File
            bPrcCrt = C_SETbCreateFileXml(oZSDINT005, poItem, pnRunning)
            If Not cCNVB.bVB_Auto Then
                If ptFlagFunc = cExportTemplate.cTableCode.ZSDINT005 Then wExports.oW_BackgroudWord.ReportProgress(40)
            End If


            ''Upsert To Log
            'bPrcCrt = C_SAVbTempZSDINT005(tFullPath, poItem)
            If Not cCNVB.bVB_Auto Then
                If ptFlagFunc = cExportTemplate.cTableCode.ZSDINT005 Then wExports.oW_BackgroudWord.ReportProgress(60)
            End If

        Catch ex As Exception
            bPrcCrt = False
        End Try
        Return bPrcCrt
    End Function

    ''' <summary>
    ''' Get Sale for Export
    ''' </summary>
    ''' <param name="poItem">List(Of cExportTemplate.cItem)</param>
    ''' <returns>Model cZSDINT005</returns>
    Private Function C_GEToSaleZSDINT005(ByRef poItem As List(Of cExportTemplate.cItem), ByVal ptPlant As String) As cZSDINT005
        'Dim oConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim oZSDINT005 As New cZSDINT005
        Dim oDocNo As New List(Of cExportTemplate.cItem)
        Dim tCmdHD As String = ""
        Dim tCmdWhe As String = ""
        Try
            'If oConn.State = ConnectionState.Closed Then oConn.Open()

            'Header
            'tCmdHD = "'" & cCNVB.tVB_StoreNo & "' AS RETAILSTOREID,"
            tCmdHD = "'" & ptPlant & "' AS RETAILSTOREID,"
            tCmdHD &= "CONVERT(VARCHAR(8), Shd.FDShdDocDate, 112) AS BUSINESSDAYDATE,"
            tCmdHD &= "CASE Shd.FTShdDocType WHEN '1' THEN '" & cCNVB.tVB_SaleCode & "' WHEN '9' THEN '" & cCNVB.tVB_ReturnCode & "' END AS TRANSACTIONTYPECODE,"
            tCmdHD &= "Shd.FTPosCode AS WORKSTATIONID,"
            tCmdHD &= "Shd.FTShdDocNo,"
            tCmdHD &= "SUBSTRING(Shd.FTShdDocNo,1,1) + SUBSTRING(Shd.FTShdDocNo,6,LEN(Shd.FTShdDocNo)) AS TRANSACTIONSEQUENCENUMBER"

            'Conition
            oDocNo = (From oDoc In poItem Where oDoc.nStaCrtFile = "1").ToList
            tCmdWhe = "Shd.FTShdDocNo IN ("
            For Each oItem In oDocNo
                tCmdWhe &= "'" & oItem.tItem & "',"
            Next
            tCmdWhe = Left(tCmdWhe, tCmdWhe.Length - 1)
            tCmdWhe &= ")"
            tCmdWhe &= "AND Shd.FTShdDocType IN ('1','9')"

#Region "DocNo"
            Dim tSqlDoc As String = ""
            tSqlDoc = "Shd.FTShdDocNo,ISNULL(Shd.FTShdPosCN,'')FTShdPosCN"
            oSql = New System.Text.StringBuilder
            oDbTbl = New DataTable
            oSql.AppendLine("--## GetDocNo ##")
            oSql.AppendLine("SELECT " & tSqlDoc)
            oSql.AppendLine("FROM TPSTSalHD Shd")
            oSql.AppendLine("WHERE {1}")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT " & tSqlDoc)
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd")
                oSql.AppendLine("WHERE {1}")
            Next
            oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
            oZSDINT005.oDocNo = New List(Of cDocNo)
            Dim oGetDoc = From oRow As DataRow In oDbTbl.Rows
                          Select New cDocNo With {
                              .FTShdDocNo = oRow("FTShdDocNo"),
                              .FTShdPosCN = oRow("FTShdPosCN")
                              }
            If oGetDoc IsNot Nothing AndAlso oGetDoc.Count > 0 Then
                oZSDINT005.oDocNo = oGetDoc.ToList
            End If
#End Region

#Region "Transaction"
            Dim tSqlTransac As String = ""
            tSqlTransac = "Shd.FDDateIns + Shd.FTTimeIns AS BEGINDATETIMESTAMP,"
            tSqlTransac &= "Shd.FDDateUpd + Shd.FTTimeUpd AS ENDDATETIMESTAMP,"
            tSqlTransac &= "Shd.FTUsrCode AS OPERATORID,"
            tSqlTransac &= "'THB' AS TRANSACTIONCURRENCY," 'Fix
            tSqlTransac &= "'THB' AS TRANSACTIONCURRENCY_ISO," 'Fix
            tSqlTransac &= "'' AS PARTNERID," 'Fix
            tSqlTransac &= "Shd.FCShdVat"
            'tSqlTransac &= "LEFT(Shd.FTShdDocNo,1) + RIGHT(Shd.FTShdDocNo, LEN(Shd.FTShdDocNo) - 5) AS ZREFERENCE0,"
            'tSqlTransac &= "CASE Shd.FTShdDocType WHEN '1' THEN ISNULL(Sdc.FTSdcRmk,'') WHEN '9' THEN ISNULL(FTShdPosCN,'') END AS ZREFERENCE1"
            oSql = New System.Text.StringBuilder
            oDbTbl = New DataTable
            oSql.AppendLine("--## E1BPTRANSACTION ##")
            oSql.AppendLine("SELECT {0},")
            oSql.AppendLine(tSqlTransac)
            oSql.AppendLine("FROM TPSTSalHD Shd")
            'oSql.AppendLine("LEFT JOIN (SELECT DISTINCT FTShdDocNo,FTSdcRmk FROM TPSTSalDC) Sdc ON Shd.FTShdDocNo = Sdc.FTShdDocNo")
            oSql.AppendLine("WHERE {1}")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine(tSqlTransac)
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd")
                'oSql.AppendLine("LEFT JOIN (SELECT DISTINCT FTShdDocNo,FTSdcRmk FROM TSDC" & tPosCode & ") Sdc ON Shd.FTShdDocNo = Sdc.FTShdDocNo")
                oSql.AppendLine("WHERE {1}")
            Next
            oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
            oZSDINT005.oTransaction = New List(Of cTransaction)
            Dim oTran = From oRow As DataRow In oDbTbl.Rows
                        Select New cTransaction With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .BEGINDATETIMESTAMP = oRow("BEGINDATETIMESTAMP"),
                              .ENDDATETIMESTAMP = oRow("ENDDATETIMESTAMP"),
                              .OPERATORID = oRow("OPERATORID"),
                              .TRANSACTIONCURRENCY = oRow("TRANSACTIONCURRENCY"),
                              .TRANSACTIONCURRENCY_ISO = oRow("TRANSACTIONCURRENCY_ISO"),
                              .PARTNERID = oRow("PARTNERID"),
                              .FTShdDocNo = oRow("FTShdDocNo"),
                              .FCShdVat = Convert.ToDouble(oRow("FCShdVat"))
                              }
            '.ZREFERENCE0 = oRow("ZREFERENCE0"), 
            '.ZREFERENCE1 = oRow("ZREFERENCE1")
            If oTran IsNot Nothing AndAlso oTran.Count > 0 Then
                oZSDINT005.oTransaction = oTran.ToList
            End If
#End Region

#Region "CustomerDetails"
            oSql = New System.Text.StringBuilder
            oDbTbl = New DataTable
            oSql.AppendLine("--## E1BPCUSTOMERDETAILS ##")
            oSql.AppendLine("SELECT {0},")
            oSql.AppendLine("	'CUST' AS CUSTOMERINFOTYPE, --Fix")
            oSql.AppendLine("	'CUSTID' AS DATAELEMENTID, --Fix")
            oSql.AppendLine("	Shd.FTCstCode AS DATAELEMENTVALUE")
            oSql.AppendLine("FROM TPSTSalHD Shd")
            oSql.AppendLine("WHERE {1}")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine("	'CUST' AS CUSTOMERINFOTYPE, --Fix")
                oSql.AppendLine("	'CUSTID' AS DATAELEMENTID, --Fix")
                oSql.AppendLine("	Shd.FTCstCode AS DATAELEMENTVALUE")
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd")
                oSql.AppendLine("WHERE {1}")
            Next
            oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
            oZSDINT005.oCustomerDetails = New List(Of cCustomerDetails)
            Dim oCstDT = From oRow As DataRow In oDbTbl.Rows
                         Select New cCustomerDetails With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .CUSTOMERINFOTYPE = oRow("CUSTOMERINFOTYPE"),
                              .DATAELEMENTID = oRow("DATAELEMENTID"),
                              .DATAELEMENTVALUE = oRow("DATAELEMENTVALUE"),
                              .FTShdDocNo = oRow("FTShdDocNo")
                              }
            If oCstDT IsNot Nothing AndAlso oCstDT.Count > 0 Then
                oZSDINT005.oCustomerDetails = oCstDT.ToList
            End If
#End Region

#Region "RetailLineItem"
            Dim tSqlRetLine As String = ""
            tSqlRetLine = "Sdt.FNSdtSeqNo AS RETAILSEQUENCENUMBER,"
            'tSqlRetLine &= "CASE Shd.FTShdDocType WHEN '1' THEN '" & cCNVB.tVB_SaleItemCode & "' WHEN '9' THEN '" & cCNVB.tVB_ReturnItemCode & "' END AS RETAILTYPECODE,"
            tSqlRetLine &= "CASE Shd.FTShdDocType" & vbCrLf
            tSqlRetLine &= "WHEN '1' THEN" & vbCrLf
            tSqlRetLine &= "    CASE Pdt.FTPdtType " & vbCrLf
            'tSqlRetLine &= "    WHEN '1' THEN '" & cCNVB.tVB_SaleItemCode & "'" & vbCrLf
            tSqlRetLine &= "    WHEN '1' THEN CASE WHEN Sdt.FCSdtNet > 0 THEN '" & cCNVB.tVB_SaleItemCode & "' ELSE '" & cCNVB.tVB_SaleFreeItemCode & "' END" & vbCrLf
            tSqlRetLine &= "    WHEN '2' THEN '" & cCNVB.tVB_SaleServiceItemCode & "'" & vbCrLf
            tSqlRetLine &= "    WHEN '4' THEN '" & cCNVB.tVB_SaleFreeItemCode & "' END" & vbCrLf
            tSqlRetLine &= "WHEN '9' THEN" & vbCrLf
            tSqlRetLine &= "    CASE Pdt.FTPdtType" & vbCrLf
            'tSqlRetLine &= "    WHEN '1' THEN '" & cCNVB.tVB_ReturnItemCode & "'" & vbCrLf
            tSqlRetLine &= "    WHEN '1' THEN CASE WHEN Sdt.FCSdtNet > 0 THEN '" & cCNVB.tVB_ReturnItemCode & "' ELSE '" & cCNVB.tVB_ReturnFreeItemCode & "' END" & vbCrLf
            tSqlRetLine &= "    WHEN '2' THEN '" & cCNVB.tVB_ReturnServiceItemCode & "'" & vbCrLf
            tSqlRetLine &= "    WHEN '4' THEN '" & cCNVB.tVB_ReturnFreeItemCode & "' END" & vbCrLf
            tSqlRetLine &= "END AS RETAILTYPECODE," & vbCrLf
            tSqlRetLine &= "'2' AS ITEMIDQUALIFIER," 'Fix
            tSqlRetLine &= "Sdt.FTPdtCode AS ITEMID,"
            tSqlRetLine &= "Sdt.FCSdtQty AS RETAILQUANTITY,"
            tSqlRetLine &= "Pun.FTPunName AS SALESUNITOFMEASURE,"
            tSqlRetLine &= "CASE Shd.FTShdDocType"
            tSqlRetLine &= " WHEN '1' THEN Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg)"
            tSqlRetLine &= " WHEN '9' THEN (Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg)) * (-1) END AS NORMALSALESAMOUNT,"
            tSqlRetLine &= "Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg) AS SALESAMOUNT,"
            tSqlRetLine &= "RIGHT(Sdt.FTSrnCode,18) AS SERIALNUMBER,"
            tSqlRetLine &= "ISNULL(Pmh.FTPmhRmk,'') AS PROMOTIONID,"
            tSqlRetLine &= "CASE Shd.FTShdDocType WHEN '1' THEN Sdt.FCSdtSetPrice WHEN '9' THEN Sdt.FCSdtSetPrice * (-1) END AS ACTUALUNITPRICE,"
            tSqlRetLine &= "Sdt.FCSdtQtyAll AS UNITS,"
            tSqlRetLine &= "Shd.FCShdVatRate,Shd.FTShdDocType"
            oSql = New System.Text.StringBuilder
            oDbTbl = New DataTable
            oSql.AppendLine("--## E1BPRETAILLINEITEM ##")
            oSql.AppendLine("SELECT DISTINCT {0},")
            oSql.AppendLine(tSqlRetLine)
            oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            oSql.AppendLine("	LEFT JOIN TPSTSalPD Spd ON Sdt.FTShdDocNo = Spd.FTShdDocNo")
            oSql.AppendLine("	LEFT JOIN TCNTPmtHD Pmh ON Spd.FTPmhCode = Pmh.FTPmhCode")
            oSql.AppendLine("	LEFT JOIN TCNMPdtUnit Pun ON Sdt.FTPunCode = Pun.FTPunCode")
            oSql.AppendLine("	LEFT JOIN TCNMPdt Pdt ON Sdt.FTPdtCode = Pdt.FTPdtCode")
            oSql.AppendLine("	INNER JOIN (")
            oSql.AppendLine("		SELECT FTSdtBarCode,SUM(FCSdtQty)FCSdtQty")
            oSql.AppendLine("		FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            oSql.AppendLine("		WHERE {1}")
            oSql.AppendLine("		Group By FTSdtBarCode")
            oSql.AppendLine("	) SdtX On Sdt.FTSdtBarCode = SdtX.FTSdtBarCode")
            oSql.AppendLine("WHERE {1}")
            oSql.AppendLine("AND SdtX.FCSdtQty > 0")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT DISTINCT {0},")
                oSql.AppendLine(tSqlRetLine)
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TSPD" & tPosCode & " Spd ON Sdt.FTShdDocNo = Spd.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TCNTPmtHD Pmh ON Spd.FTPmhCode = Pmh.FTPmhCode")
                oSql.AppendLine("	LEFT JOIN TCNMPdtUnit Pun ON Sdt.FTPunCode = Pun.FTPunCode")
                oSql.AppendLine("	LEFT JOIN TCNMPdt Pdt ON Sdt.FTPdtCode = Pdt.FTPdtCode")
                oSql.AppendLine("	INNER JOIN (")
                oSql.AppendLine("		SELECT FTSdtBarCode,SUM(FCSdtQty)FCSdtQty")
                oSql.AppendLine("		FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                oSql.AppendLine("		WHERE {1}")
                oSql.AppendLine("		Group By FTSdtBarCode")
                oSql.AppendLine("	) SdtX On Sdt.FTSdtBarCode = SdtX.FTSdtBarCode")
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("AND SdtX.FCSdtQty > 0")
            Next
            oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
            oZSDINT005.oRetailLineItem = New List(Of cRetailLineItem)
            Dim oRetail = From oRow As DataRow In oDbTbl.Rows
                          Select New cRetailLineItem With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .RETAILSEQUENCENUMBER = oRow("RETAILSEQUENCENUMBER"),
                              .RETAILTYPECODE = oRow("RETAILTYPECODE"),
                              .ITEMIDQUALIFIER = oRow("ITEMIDQUALIFIER"),
                              .ITEMID = oRow("ITEMID"),
                              .RETAILQUANTITY = oRow("RETAILQUANTITY"),
                              .SALESUNITOFMEASURE = oRow("SALESUNITOFMEASURE"),
                              .SALESAMOUNT = Convert.ToDouble(oRow("SALESAMOUNT")).ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
                              .NORMALSALESAMOUNT = Convert.ToDouble(oRow("NORMALSALESAMOUNT")).ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
                              .SERIALNUMBER = oRow("SERIALNUMBER"),
                              .PROMOTIONID = oRow("PROMOTIONID"),
                              .ACTUALUNITPRICE = Convert.ToDouble(oRow("ACTUALUNITPRICE")).ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
                              .UNITS = oRow("UNITS"),
                              .FCShdVatRate = Convert.ToDouble(oRow("FCShdVatRate")),
                              .FTShdDocType = oRow("FTShdDocType"),
                              .FTShdDocNo = oRow("FTShdDocNo")
                              }
            If oRetail IsNot Nothing AndAlso oRetail.Count > 0 Then
                oZSDINT005.oRetailLineItem = oRetail.ToList
            End If
#End Region

#Region "ItemDiscount"
            oSql = New System.Text.StringBuilder
            oDbTbl = New DataTable
            'oSql.AppendLine("--## E1BPLINEITEMDISCOUNT ##")
            'oSql.AppendLine("Select {0},")
            'oSql.AppendLine("	Sdt.FNSdtSeqNo As RETAILSEQUENCENUMBER,")
            'oSql.AppendLine("	Sdc.FNSdcSeqNo As DISCOUNTSEQUENCENUMBER,")
            'oSql.AppendLine("	Case Sdc.FTSdcDisType When '1' THEN 'ZDM2' WHEN '2' THEN 'ZDM1' END AS DISCOUNTTYPECODE,")
            ''oSql.AppendLine("	FCSdcAmt * (-1) AS REDUCTIONAMOUNT")
                    'oSql.AppendLine("	CASE Shd.FTShdDocType WHEN '1' THEN Sdc.FCSdcAmt WHEN '9' THEN Sdc.FCSdcAmt * (-1) END AS REDUCTIONAMOUNT,") '*CH 18-09-2017
                    'oSql.AppendLine("	Shd.FCShdVatRate") '*CH 18-09-2017
                    'oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                    'oSql.AppendLine("	INNER JOIN TPSTSalDC Sdc ON Sdt.FTShdDocNo = Sdc.FTShdDocNo AND Sdt.FNSdtSeqNo = Sdc.FNSdtSeqNo AND Sdt.FTPdtCode = Sdc.FTPdtCode")
                    'oSql.AppendLine("WHERE {1}")
                    ''Temp Sale
                    'For Each tPosCode In cCNVB.oVB_PosCode
                    '    oSql.AppendLine("UNION ALL")
                    '    oSql.AppendLine("SELECT {0},")
                    '    oSql.AppendLine("	Sdt.FNSdtSeqNo AS RETAILSEQUENCENUMBER,")
                    '    oSql.AppendLine("	Sdc.FNSdcSeqNo AS DISCOUNTSEQUENCENUMBER,")
                    '    oSql.AppendLine("	CASE Sdc.FTSdcDisType WHEN '1' THEN 'ZDM2' WHEN '2' THEN 'ZDM1' END AS DISCOUNTTYPECODE,")
                    '    'oSql.AppendLine("	FCSdcAmt * (-1) AS REDUCTIONAMOUNT")
                    '    oSql.AppendLine("	CASE Shd.FTShdDocType WHEN '1' THEN Sdc.FCSdcAmt WHEN '9' THEN Sdc.FCSdcAmt * (-1) END AS REDUCTIONAMOUNT,") '*CH 18-09-2017
                    '    oSql.AppendLine("	Shd.FCShdVatRate") '*CH 18-09-2017
                    '    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                    '    oSql.AppendLine("	INNER JOIN TSDC" & tPosCode & " Sdc ON Sdt.FTShdDocNo = Sdc.FTShdDocNo AND Sdt.FNSdtSeqNo = Sdc.FNSdtSeqNo AND Sdt.FTPdtCode = Sdc.FTPdtCode")
                    '    oSql.AppendLine("WHERE {1}")
                    'Next

                    Dim tCmdStd As String = ""
            tCmdStd &= "	Sdt.FNSdtSeqNo AS RETAILSEQUENCENUMBER," & vbCrLf
            tCmdStd &= "	Sdt.FNSdtSeqNo AS DISCOUNTSEQUENCENUMBER," & vbCrLf
            'tCmdStd &= "	Sdc.FNSdcSeqNo AS DISCOUNTSEQUENCENUMBER," & vbCrLf
            'tCmdStd &= "	CASE Sdc.FTSdcType" & vbCrLf
            'tCmdStd &= "	WHEN '1' THEN CASE Sdc.FTSdcDisType WHEN '1' THEN 'ZDO2' WHEN '2' THEN 'ZDO1' END --By Point" & vbCrLf
            'tCmdStd &= "	WHEN '2' THEN CASE Sdc.FTSdcDisType WHEN '1' THEN 'ZDM2' WHEN '2' THEN 'ZDM1' END --OnTop" & vbCrLf
            'tCmdStd &= "	END AS DISCOUNTTYPECODE," & vbCrLf
            'tCmdStd &= "	'ZDM1' AS DISCOUNTTYPECODE," & vbCrLf
            tCmdStd &= "	CASE ISNULL(Map.FTLNMDefValue,'') WHEN '' THEN 'ZDM1' ELSE Map.FTLNMDefValue END AS DISCOUNTTYPECODE,"
            'tCmdStd &= "	CASE Shd.FTShdDocType" & vbCrLf
            'tCmdStd &= "	WHEN '1' THEN (ISNULL(Sdt.FCSdtDis,0) - ISNULL(Sdt.FCSdtChg,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0)" & vbCrLf
            'tCmdStd &= "	WHEN '9' THEN ((ISNULL(Sdt.FCSdtDis,0) - ISNULL(Sdt.FCSdtChg,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0)) * (-1) END AS REDUCTIONAMOUNT," & vbCrLf
            'tCmdStd &= "	((ISNULL(Sdt.FCSdtDis,0) - ISNULL(Sdt.FCSdtChg,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0)) AS REDUCTIONAMOUNT," & vbCrLf
            tCmdStd &= "	((ISNULL(Sdt.FCSdtDis,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0)) AS REDUCTIONAMOUNT," & vbCrLf
            tCmdStd &= "	Shd.FCShdVatRate,Shd.FTShdDocType"

            Dim tCmdPmt As String = ""
            tCmdPmt &= "	Sdt.FNSdtSeqNo AS RETAILSEQUENCENUMBER," & vbCrLf
            tCmdPmt &= "	Sdt.FNSdtSeqNo DISCOUNTSEQUENCENUMBER," & vbCrLf
            tCmdPmt &= "	CASE WHEN Sdt.FCSdtDisAvg > 0" & vbCrLf
            'tCmdPmt &= "	THEN (CASE WHEN ISNULL(Spd.FCSpdGetCond,0) IN (1,3) THEN 'ZDP1' ELSE 'ZDP2' END)" & vbCrLf
            tCmdPmt &= "	THEN 'ZDP1'" & vbCrLf
            tCmdPmt &= "	ELSE '' END AS DISCOUNTTYPECODE," & vbCrLf
            'tCmdPmt &= "	CASE Shd.FTShdDocType WHEN '1' THEN Spd.FCSdtDisAvg WHEN '9' THEN Spd.FCSdtDisAvg * (-1) END AS REDUCTIONAMOUNT," & vbCrLf
            tCmdPmt &= "	Spd.FCSdtDisAvg AS REDUCTIONAMOUNT," & vbCrLf
            tCmdPmt &= "	Shd.FCShdVatRate,Shd.FTShdDocType"

            Dim tCmdVat As String = ""
            tCmdVat &= "	Sdt.FNSdtSeqNo AS RETAILSEQUENCENUMBER," & vbCrLf
            tCmdVat &= "	Sdt.FNSdtSeqNo DISCOUNTSEQUENCENUMBER," & vbCrLf
            tCmdVat &= "	'ZMW1' AS DISCOUNTTYPECODE," & vbCrLf
            'tCmdVat &= "	Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg) AS REDUCTIONAMOUNT," & vbCrLf
            tCmdVat &= "	(Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg)) AS REDUCTIONAMOUNT," & vbCrLf
            'tCmdVat &= "	Shd.FCShdVatRate,Shd.FTShdDocType"
            tCmdVat &= "	Shd.FCShdVatRate,CONVERT(VARCHAR(255), ISNULL(Sdt.FCSdtDis,0) + ISNULL(Sdt.FCSdtDisAvg,0) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0)) + ';' + "
            tCmdVat &= "CONVERT(VARCHAR(255), ISNULL(Sdt.FCSdtChg,0)) + ';' + Shd.FTShdDocType"

            Dim tCmdChg As String = ""
            tCmdChg &= "	Sdt.FNSdtSeqNo AS RETAILSEQUENCENUMBER," & vbCrLf
            tCmdChg &= "	Sdt.FNSdtSeqNo DISCOUNTSEQUENCENUMBER," & vbCrLf
            tCmdChg &= "	'ZSI1' AS DISCOUNTTYPECODE," & vbCrLf
            tCmdChg &= "	Sdt.FCSdtChg AS REDUCTIONAMOUNT," & vbCrLf
            tCmdChg &= "	Shd.FCShdVatRate,Shd.FTShdDocType"

            oSql.AppendLine("--## E1BPLINEITEMDISCOUNT ##")
            '### Discount Standard ###'
            oSql.AppendLine("SELECT {0},")
            oSql.AppendLine(tCmdStd)
            oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            oSql.AppendLine("	LEFT JOIN TPSTSalDC Sdc ON Sdt.FTShdDocNo = Sdc.FTShdDocNo AND Sdt.FNSdtSeqNo = Sdc.FNSdtSeqNo AND Sdt.FTPdtCode = Sdc.FTPdtCode")
            oSql.AppendLine("	LEFT JOIN TLNKMapping Map ON Sdc.FTSdcRsnCode = Map.FTLNMUsrValue")
            oSql.AppendLine("WHERE {1}")
            'oSql.AppendLine("	And (ISNULL(Sdt.FCSdtDis, 0) - ISNULL(Sdt.FCSdtChg,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0) > 0")
            oSql.AppendLine("	And (ISNULL(Sdt.FCSdtDis,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0) > 0")
            oSql.AppendLine("	And (ISNULL(Map.FTLNMDefValue,'') = '' OR ISNULL(Map.FTLNMDefValue,'') <> '' OR ISNULL(Map.FTLNMType,'') = '4')")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine(tCmdStd)
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TSDC" & tPosCode & " Sdc ON Sdt.FTShdDocNo = Sdc.FTShdDocNo AND Sdt.FNSdtSeqNo = Sdc.FNSdtSeqNo AND Sdt.FTPdtCode = Sdc.FTPdtCode")
                oSql.AppendLine("	LEFT JOIN TLNKMapping Map ON Sdc.FTSdcRsnCode = Map.FTLNMUsrValue")
                oSql.AppendLine("WHERE {1}")
                'oSql.AppendLine("	AND (ISNULL(Sdt.FCSdtDis,0) - ISNULL(Sdt.FCSdtChg,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0) > 0")
                oSql.AppendLine("	AND (ISNULL(Sdt.FCSdtDis,0)) + ISNULL(Sdt.FCSdtFootAvg,0) + ISNULL(Sdt.FCSdtRepackAvg,0) > 0")
                oSql.AppendLine("	AND (ISNULL(Map.FTLNMDefValue,'') = '' OR ISNULL(Map.FTLNMDefValue,'') <> '' OR ISNULL(Map.FTLNMType,'') = '4')")
            Next
            '### End Discount Standard ###'
            '### Discount Promotion ###'
            oSql.AppendLine("UNION ALL")
            oSql.AppendLine("SELECT {0},")
            oSql.AppendLine(tCmdPmt)
            oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            oSql.AppendLine("	INNER JOIN TPSTSalPD Spd ON Sdt.FTShdDocNo = Spd.FTShdDocNo AND Sdt.FNSdtSeqNo = Spd.FNSdtSeqNo")
            oSql.AppendLine("WHERE {1}")
            oSql.AppendLine("	AND ISNULL(Spd.FCSpdGetCond,0) IN (1,2,3)")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine(tCmdPmt)
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                oSql.AppendLine("	INNER JOIN TSPD" & tPosCode & " Spd ON Sdt.FTShdDocNo = Spd.FTShdDocNo AND Sdt.FNSdtSeqNo = Spd.FNSdtSeqNo")
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("	AND ISNULL(Spd.FCSpdGetCond,0) IN (1,2,3)")
            Next
            '### End Discount Promotion ###'
            '### VAT ###'
            oSql.AppendLine("UNION ALL")
            oSql.AppendLine("SELECT {0},")
            oSql.AppendLine(tCmdVat)
            oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            oSql.AppendLine("WHERE {1}")
            oSql.AppendLine("	AND Sdt.FTSdtVatType = '1'")
            oSql.AppendLine("	AND (Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg)) > 0")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine(tCmdVat)
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("	AND Sdt.FTSdtVatType = '1'")
                oSql.AppendLine("	AND (Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg)) > 0")
            Next
            '### End VAT ###'
            '### Charge ###'
            oSql.AppendLine("UNION ALL")
            oSql.AppendLine("SELECT {0},")
            oSql.AppendLine(tCmdChg)
            oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            oSql.AppendLine("WHERE {1}")
            oSql.AppendLine("	AND ISNULL(Sdt.FCSdtChg,0) > 0")
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine(tCmdChg)
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("	AND ISNULL(Sdt.FCSdtChg,0) > 0")
            Next
            '### End Charge ###'
            oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
            oZSDINT005.oLineItemDiscount = New List(Of cLineItemDiscount)
            Dim oDis = From oRow As DataRow In oDbTbl.Rows
                       Select New cLineItemDiscount With {
                           .RETAILSTOREID = oRow("RETAILSTOREID"),
                           .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                           .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                           .WORKSTATIONID = oRow("WORKSTATIONID"),
                           .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                           .RETAILSEQUENCENUMBER = oRow("RETAILSEQUENCENUMBER"),
                           .DISCOUNTSEQUENCENUMBER = oRow("DISCOUNTSEQUENCENUMBER"),
                           .DISCOUNTTYPECODE = oRow("DISCOUNTTYPECODE"),
                           .REDUCTIONAMOUNT = oRow("REDUCTIONAMOUNT"),
                           .FCShdVatRate = Convert.ToDouble(oRow("FCShdVatRate")),
                           .FTShdDocType = oRow("FTShdDocType"),
                           .FTShdDocNo = oRow("FTShdDocNo")
                           }
            If oDis IsNot Nothing AndAlso oDis.Count > 0 Then
                oZSDINT005.oLineItemDiscount = oDis.ToList
            End If
#End Region

#Region "ItemCommission"
            oSql = New System.Text.StringBuilder
            oDbTbl = New DataTable
            oSql.AppendLine("--## E1BPLINEITEMCOMMISSI ##")
            oSql.AppendLine("SELECT {0},")
            oSql.AppendLine("	Sdt.FNSdtSeqNo AS COMMISIONSEQUENCENUMBER,")
            oSql.AppendLine("	'' AS COMMEMPLOYEEQUAL, --Fix")
            oSql.AppendLine("	ISNULL(FTSpnCode,'') AS COMMISSIONEMPLOYEEID,")
            oSql.AppendLine("	0 AS COMMISSIONAMOUNT --Fix")
            oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            oSql.AppendLine("WHERE {1}")
            oSql.AppendLine("	AND ISNULL(FTSpnCode,'') <> ''") '*CH 06-09-2017
            'Temp Sale
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine("	Sdt.FNSdtSeqNo AS COMMISIONSEQUENCENUMBER,")
                oSql.AppendLine("	'' AS COMMEMPLOYEEQUAL, --Fix")
                oSql.AppendLine("	ISNULL(FTSpnCode,'') AS COMMISSIONEMPLOYEEID,")
                oSql.AppendLine("	0 AS COMMISSIONAMOUNT --Fix")
                oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("	AND ISNULL(FTSpnCode,'') <> ''") '*CH 06-09-2017
            Next
            oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
            oZSDINT005.oLineItemCommissi = New List(Of cLineItemCommissi)
            Dim oCommis = From oRow As DataRow In oDbTbl.Rows
                          Select New cLineItemCommissi With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .COMMISIONSEQUENCENUMBER = oRow("COMMISIONSEQUENCENUMBER"),
                              .COMMEMPLOYEEQUAL = oRow("COMMEMPLOYEEQUAL"),
                              .COMMISSIONEMPLOYEEID = oRow("COMMISSIONEMPLOYEEID"),
                              .COMMISSIONAMOUNT = oRow("COMMISSIONAMOUNT"),
                              .FTShdDocNo = oRow("FTShdDocNo")
                           }
            If oCommis IsNot Nothing AndAlso oCommis.Count > 0 Then
                oZSDINT005.oLineItemCommissi = oCommis.ToList
            End If
#End Region

#Region "TransactExtension"
            'oSql = New System.Text.StringBuilder
            'oDbTbl = New DataTable
            'oSql.AppendLine("--## E1BPTRANSACTEXTENSIO ##")
            'oSql.AppendLine("SELECT {0},")
            'oSql.AppendLine("	'QUOT' AS FIELDGROUP, --Fix")
            'oSql.AppendLine("	'QUOTNO' AS FIELDNAME, --Fix")
            'oSql.AppendLine("	ISNULL(Sdt.FTSdtRmk,'') AS FIELDVALUE")
            'oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            'oSql.AppendLine("WHERE {1}")
            'oSql.AppendLine("	AND ISNULL(Sdt.FTSdtRmk,'') <> ''")
            'oSql.AppendLine("	AND Sdt.FNSdtSeqNo = 1")
            ''Temp Sale
            'For Each tPosCode In cCNVB.oVB_PosCode
            '    oSql.AppendLine("UNION ALL")
            '    oSql.AppendLine("SELECT {0},")
            '    oSql.AppendLine("	'QUOT' AS FIELDGROUP, --Fix")
            '    oSql.AppendLine("	'QUOTNO' AS FIELDNAME, --Fix")
            '    oSql.AppendLine("	ISNULL(Sdt.FTSdtRmk,'') AS FIELDVALUE")
            '    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo")
            '    oSql.AppendLine("WHERE {1}")
            '    oSql.AppendLine("	AND ISNULL(Sdt.FTSdtRmk,'') <> ''")
            '    oSql.AppendLine("	AND Sdt.FNSdtSeqNo = 1")
            'Next
            'oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
            'oZSDINT005.oTransactExtensio = New List(Of cTransactExtensio)
            'Dim oTranEx = From oRow As DataRow In oDbTbl.Rows
            '              Select New cTransactExtensio With {
            '                  .RETAILSTOREID = oRow("RETAILSTOREID"),
            '                  .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
            '                  .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
            '                  .WORKSTATIONID = oRow("WORKSTATIONID"),
            '                  .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
            '                  .FIELDGROUP = oRow("FIELDGROUP"),
            '                  .FIELDNAME = oRow("FIELDNAME"),
            '                  .FIELDVALUE = oRow("FIELDVALUE")
            '               }
            'If oTranEx IsNot Nothing AndAlso oTranEx.Count > 0 Then
            '    oZSDINT005.oTransactExtensio = oTranEx.ToList
            'End If
#End Region

#Region "ItemExtension"
            Dim otCmdItemExt As New List(Of String())
            Dim atCmdItemExt(5) As String '0:Object, 1:Data, 2:Join Table,3:Join TempTable, 4:Condition
            'ข้อมูลรหัส invoice number
            atCmdItemExt(0) = "Invoice"
            atCmdItemExt(1) = "'1' AS RETAILSEQUENCENUMBER," 'Fix
            atCmdItemExt(1) &= "'REF0' AS FIELDGROUP,"
            atCmdItemExt(1) &= "'INVNO' AS FIELDNAME,"
            atCmdItemExt(1) &= "SUBSTRING(Shd.FTShdDocNo,1,1) + SUBSTRING(Shd.FTShdDocNo,6,LEN(Shd.FTShdDocNo)) AS FIELDVALUE"
            atCmdItemExt(2) = ""
            atCmdItemExt(3) = ""
            'atCmdItemExt(4) = "AND ISNULL(Shd.FTShdPosCN,'') = ''"
            atCmdItemExt(4) = "AND Shd.FTShdDocType IN ('1','9')"
            otCmdItemExt.Add(atCmdItemExt)

            'ข้อมูลรหัส invoice number SXXXXXXXXXXX กรณี Return
            ReDim atCmdItemExt(5)
            atCmdItemExt(0) = "Return"
            atCmdItemExt(1) = "'1' AS RETAILSEQUENCENUMBER," 'Fix
            atCmdItemExt(1) &= "'REF1' AS FIELDGROUP,"
            atCmdItemExt(1) &= "'OINVNO' AS FIELDNAME,"
            atCmdItemExt(1) &= "SUBSTRING(Shd.FTShdPosCN,1,1) + SUBSTRING(Shd.FTShdPosCN,6,LEN(Shd.FTShdPosCN)) AS FIELDVALUE"
            atCmdItemExt(2) = ""
            atCmdItemExt(3) = ""
            atCmdItemExt(4) = "AND ISNULL(Shd.FTShdPosCN,'') <> ''"
            otCmdItemExt.Add(atCmdItemExt)

            'OnTop
            ReDim atCmdItemExt(5)
            atCmdItemExt(0) = "OnTop"
            atCmdItemExt(1) = "'1' AS RETAILSEQUENCENUMBER," 'Fix
            atCmdItemExt(1) &= "'REF1' AS FIELDGROUP,"
            atCmdItemExt(1) &= "'OINVNO' AS FIELDNAME,"
            atCmdItemExt(1) &= "Sdc.FTSdcRmk AS FIELDVALUE"
            atCmdItemExt(2) = "INNER JOIN (SELECT DISTINCT FTShdDocNo,FTSdcRmk FROM TPSTSalDC) Sdc ON Shd.FTShdDocNo = Sdc.FTShdDocNo"
            atCmdItemExt(3) = "INNER JOIN (SELECT DISTINCT FTShdDocNo,FTSdcRmk FROM TSDC{0}) Sdc ON Shd.FTShdDocNo = Sdc.FTShdDocNo"
            atCmdItemExt(4) = ""
            otCmdItemExt.Add(atCmdItemExt)

            'ข้อมูลเลขที่ Quotation  Case ซ่อมกล้อง
            ReDim atCmdItemExt(5)
            atCmdItemExt(0) = "Quotation"
            atCmdItemExt(1) = "'1' AS RETAILSEQUENCENUMBER," 'Fix
            atCmdItemExt(1) &= "'QUOT' AS FIELDGROUP,"
            atCmdItemExt(1) &= "'QUOTNO' AS FIELDNAME,"
            atCmdItemExt(1) &= "Sdt.FTSdtRmk AS FIELDVALUE"
            atCmdItemExt(2) = "INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo"
            atCmdItemExt(3) = "INNER JOIN TSDT{0} Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo"
            atCmdItemExt(4) = "AND Sdt.FNSdtSeqNo = 1 AND ISNULL(Sdt.FTSdtRmk,'') <> ''"
            otCmdItemExt.Add(atCmdItemExt)

            'กรณี หมายเหตุ อื่น ๆ ใน รายสินค้า   อาจจะเป็นสินค้าแถม หรือ กรณีอื่น 
            ReDim atCmdItemExt(5)
            atCmdItemExt(0) = "Remark"
            atCmdItemExt(1) = "Sdt.FNSdtSeqNo AS RETAILSEQUENCENUMBER,"
            atCmdItemExt(1) &= "'RMK' AS FIELDGROUP,"
            atCmdItemExt(1) &= "'RMK01-RMK08' AS FIELDNAME,"
            atCmdItemExt(1) &= "Sdt.FTSdtRmk AS FIELDVALUE"
            atCmdItemExt(2) = "INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo"
            atCmdItemExt(3) = "INNER JOIN TSDT{0} Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo"
            atCmdItemExt(4) = "AND Sdt.FNSdtSeqNo <> 1 AND ISNULL(Sdt.FTSdtRmk,'') <> ''"
            otCmdItemExt.Add(atCmdItemExt)

            oZSDINT005.oLineItemExtensio = New List(Of cLineItemExtensio)
            For Each atItem In otCmdItemExt
                oSql = New System.Text.StringBuilder
                oDbTbl = New DataTable
                oSql.AppendLine("--## E1BPLINEITEMEXTENSIO ##")
                oSql.AppendLine("SELECT {0},")
                oSql.AppendLine(atItem(1))
                oSql.AppendLine("FROM TPSTSalHD Shd " & atItem(2))
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("	" & atItem(4))
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT {0},")
                    oSql.AppendLine(atItem(1))
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd " & System.String.Format(atItem(3), tPosCode))
                    oSql.AppendLine("WHERE {1}")
                    oSql.AppendLine("	" & atItem(4))
                Next
                oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
                Dim oItemEx = From oRow As DataRow In oDbTbl.Rows
                              Select New cLineItemExtensio With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .RETAILSEQUENCENUMBER = oRow("RETAILSEQUENCENUMBER"),
                              .FIELDGROUP = oRow("FIELDGROUP"),
                              .FIELDNAME = oRow("FIELDNAME"),
                              .FIELDVALUE = oRow("FIELDVALUE"),
                              .FTShdDocNo = oRow("FTShdDocNo")
                           }
                If oItemEx IsNot Nothing AndAlso oItemEx.Count > 0 Then
                    For nItem = 0 To oItemEx.Count - 1
                        oZSDINT005.oLineItemExtensio.Add(oItemEx(nItem))
                    Next
                End If
            Next
#End Region

#Region "Tender"
            Dim tSqlTender As String = ""
            If oRetail(0).FTShdDocType = "1" Then
                'ใช้รหัสบัตร 3 ตำแหน่ง '*CH 12-10-2017
                tSqlTender &= "Src.FNSrcSeqNo AS TENDERSEQUENCENUMBER,"
                tSqlTender &= "CASE Src.FTRcvCode "
                tSqlTender &= "WHEN '001' THEN 'ZCSH' "
                'tSqlTender &= "WHEN '002' THEN 'Z' + LEFT(ISNULL(Src.FTSrcBnkBch,''),3) " 'Real Bank Code
                tSqlTender &= "WHEN '002' THEN 'Z' + LEFT(ISNULL(Edc.FTBnkCode,''),3) " 'Real Bank Code
                'tSqlTender &= "WHEN '009' THEN 'Y' + LEFT(ISNULL(Src.FTBnkCode,''),3) "
                'tSqlTender &= "WHEN '009' THEN CASE WHEN PosE.FNEdcPort = 0 THEN 'Y' ELSE 'Z' END + LEFT(ISNULL(Src.FTBnkCode,''),3) " 'ผ่อนชำระ OffLine Y, OnLine Z
                tSqlTender &= "WHEN '009' THEN CASE WHEN PosE.FNEdcPort = 0 THEN 'Y' ELSE 'Z' END + LEFT(ISNULL(Edc.FTBnkCode,''),3) " 'ผ่อนชำระ OffLine Y, OnLine Z
                tSqlTender &= "ELSE '' END TENDERTYPECODE,"
                tSqlTender &= "Src.FCSrcNet AS TENDERAMOUNT,"
                tSqlTender &= "'THB' AS TENDERCURRENCY,"
                tSqlTender &= "CASE Src.FTRcvCode WHEN '002' THEN ISNULL(Src.FTBnkCode,'') WHEN '009' THEN ISNULL(Src.FTSrcBnkBch,'') ELSE '' END AS TENDERID,"
                tSqlTender &= "'' AS ACCOUNTNUMBER,"
                tSqlTender &= "ISNULL(Src.FTXRCRmk,'') AS REFERENCEID,"
                tSqlTender &= "ISNULL(Edc.FTEdcOther,'') AS FTEdcOther"
                oSql = New System.Text.StringBuilder
                oDbTbl = New DataTable
                oSql.AppendLine("--## E1BPTENDER ##")
                oSql.AppendLine("SELECT DISTINCT {0},")
                ''oSql.AppendLine("	Shd.FTShdDocNo AS TENDERSEQUENCENUMBER,")
                'oSql.AppendLine("	Src.FNSrcSeqNo AS TENDERSEQUENCENUMBER,")
                ''oSql.AppendLine("	ISNULL('Y' + Src.FTBnkCode,'') AS TENDERTYPECODE,")
                'oSql.AppendLine("	CASE Src.FTRcvCode")
                'oSql.AppendLine("	WHEN '001' THEN 'ZCSH'")
                'oSql.AppendLine("	WHEN '002' THEN 'Z' + Src.FTBnkCode")
                'oSql.AppendLine("	WHEN '009' THEN 'Y' + Src.FTBnkCode")
                'oSql.AppendLine("	ELSE '' END TENDERTYPECODE,") '*CH 20-09-2017
                'oSql.AppendLine("	Src.FCSrcNet AS TENDERAMOUNT,")
                'oSql.AppendLine("	'THB' AS TENDERCURRENCY,")
                'oSql.AppendLine("	ISNULL(Src.FTSrcBnkBch,'') AS TENDERID,")
                'oSql.AppendLine("	'' AS ACCOUNTNUMBER,")
                'oSql.AppendLine("	ISNULL(Src.FTXRCRmk,'') AS REFERENCEID")
                oSql.AppendLine(tSqlTender)
                oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalRC Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                oSql.AppendLine("WHERE {1}")
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT DISTINCT {0}, ")
                    oSql.AppendLine(tSqlTender)
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSRC" & tPosCode & " Src On Shd.FTShdDocNo = Src.FTShdDocNo")
                    oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                    oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                    oSql.AppendLine("WHERE {1}")
                Next
                oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
                oZSDINT005.oTender = New List(Of cTender)
                Dim oTender = From oRow As DataRow In oDbTbl.Rows
                              Select New cTender With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .TENDERSEQUENCENUMBER = oRow("TENDERSEQUENCENUMBER"),
                              .TENDERTYPECODE = oRow("TENDERTYPECODE"),
                              .TENDERAMOUNT = oRow("TENDERAMOUNT"),
                              .TENDERCURRENCY = oRow("TENDERCURRENCY"),
                              .TENDERID = oRow("TENDERID"),
                              .ACCOUNTNUMBER = oRow("ACCOUNTNUMBER"),
                              .REFERENCEID = oRow("REFERENCEID"),
                              .FTEdcOther = oRow("FTEdcOther"),
                              .FTShdDocNo = oRow("FTShdDocNo")
                           }
                If oTender IsNot Nothing AndAlso oTender.Count > 0 Then
                    oZSDINT005.oTender = oTender.ToList
                End If
            Else
                '*CH 19-12-2017
                'Get SaleBill 
                tSqlTender &= "Src.FNSrcSeqNo AS TENDERSEQUENCENUMBER,"
                tSqlTender &= "CASE Src.FTRcvCode "
                tSqlTender &= "WHEN '001' THEN 'ZCSH' "
                tSqlTender &= "WHEN '002' THEN 'Z' + LEFT(ISNULL(Edc.FTBnkCode,''),3) " 'Real Bank Code
                tSqlTender &= "WHEN '009' THEN CASE WHEN PosE.FNEdcPort = 0 THEN 'Y' ELSE 'Z' END + LEFT(ISNULL(Edc.FTBnkCode,''),3) " 'ผ่อนชำระ OffLine Y, OnLine Z
                tSqlTender &= "ELSE '' END TENDERTYPECODE,"
                tSqlTender &= "Src.FCSrcNet AS TENDERAMOUNT,"
                tSqlTender &= "'THB' AS TENDERCURRENCY,"
                tSqlTender &= "CASE Src.FTRcvCode WHEN '002' THEN ISNULL(Src.FTBnkCode,'') WHEN '009' THEN ISNULL(Src.FTSrcBnkBch,'') ELSE '' END AS TENDERID,"
                tSqlTender &= "'' AS ACCOUNTNUMBER,"
                tSqlTender &= "ISNULL(Src.FTXRCRmk,'') AS REFERENCEID,"
                tSqlTender &= "ISNULL(Edc.FTEdcOther,'') AS FTEdcOther,"
                tSqlTender &= "Src.FTRcvCode"
                oSql = New System.Text.StringBuilder
                oDbTbl = New DataTable
                oSql.AppendLine("--## E1BPTENDER ##")
                oSql.AppendLine("SELECT DISTINCT {0},")
                oSql.AppendLine(tSqlTender)
                oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalRC Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                oSql.AppendLine("WHERE (Shd.FTShdDocNo IN (SELECT FTShdPosCN FROM TPSTSalHD Shd WHERE {1})")
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("	OR Shd.FTShdDocNo IN (SELECT FTShdPosCN FROM TSHD" & tPosCode & " Shd WHERE {1})")
                Next
                oSql.AppendLine(")")
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT DISTINCT {0}, ")
                    oSql.AppendLine(tSqlTender)
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSRC" & tPosCode & " Src On Shd.FTShdDocNo = Src.FTShdDocNo")
                    oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                    oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                    oSql.AppendLine("WHERE Shd.FTShdDocNo IN (SELECT FTShdPosCN FROM TSHD" & tPosCode & " Shd WHERE {1})")
                Next
                oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
                oZSDINT005.oTender = New List(Of cTender)
                Dim oTender = From oRow As DataRow In oDbTbl.Rows
                              Select New cTender With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .TENDERSEQUENCENUMBER = oRow("TENDERSEQUENCENUMBER"),
                              .TENDERTYPECODE = oRow("TENDERTYPECODE"),
                              .TENDERAMOUNT = oRow("TENDERAMOUNT"),
                              .TENDERCURRENCY = oRow("TENDERCURRENCY"),
                              .TENDERID = oRow("TENDERID"),
                              .ACCOUNTNUMBER = oRow("ACCOUNTNUMBER"),
                              .REFERENCEID = oRow("REFERENCEID"),
                              .FTEdcOther = oRow("FTEdcOther"),
                              .FTShdDocNo = oRow("FTShdDocNo"),
                              .FTRcvCode = oRow("FTRcvCode")
                           }
                If oTender IsNot Nothing AndAlso oTender.Count > 0 Then
                    oZSDINT005.oTender = oTender.ToList
                End If

                'Get ReturnBill
                tSqlTender = "Src.FNSrcSeqNo AS TENDERSEQUENCENUMBER,"
                tSqlTender &= "Src.FCSrcNet AS TENDERAMOUNT,"
                tSqlTender &= "Src.FTRcvCode"
                oSql = New System.Text.StringBuilder
                oDbTbl = New DataTable
                oSql.AppendLine("--## E1BPTENDER ##")
                oSql.AppendLine("SELECT DISTINCT {0},")
                oSql.AppendLine(tSqlTender)
                oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalRC Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                oSql.AppendLine("WHERE {1}")
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT DISTINCT {0}, ")
                    oSql.AppendLine(tSqlTender)
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSRC" & tPosCode & " Src On Shd.FTShdDocNo = Src.FTShdDocNo")
                    oSql.AppendLine("WHERE {1}")
                Next
                oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
                oZSDINT005.oTenderR = New List(Of cTender)
                Dim oTenderR = From oRow As DataRow In oDbTbl.Rows
                               Select New cTender With {
                              .RETAILSTOREID = oRow("RETAILSTOREID"),
                              .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                              .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                              .WORKSTATIONID = oRow("WORKSTATIONID"),
                              .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                              .TENDERSEQUENCENUMBER = oRow("TENDERSEQUENCENUMBER"),
                              .TENDERAMOUNT = oRow("TENDERAMOUNT"),
                              .FTShdDocNo = oRow("FTShdDocNo"),
                              .FTRcvCode = oRow("FTRcvCode")
                           }
                If oTenderR IsNot Nothing AndAlso oTenderR.Count > 0 Then
                    oZSDINT005.oTenderR = oTenderR.ToList
                End If
            End If
#End Region

#Region "CreditCard"
            Dim tSqlCrd As String = ""
            oSql = New System.Text.StringBuilder
            tSqlCrd = "Src.FNSrcSeqNo As TENDERSEQUENCENUMBER,"
            tSqlCrd &= "Src.FTSrcRef As CARDNUMBER,"
            tSqlCrd &= "'***' AS CARDNUMBERSUFFIX," 'Fix
            tSqlCrd &= "'99991231' AS CARDEXPIRATIONDATE," 'Fix
            tSqlCrd &= "'DUMMY' AS CARDHOLDERNAME," 'Fix
            tSqlCrd &= "ISNULL(Src.FTXrcRmk,'') AS ADJUDICATIONCODE,"
            'tSqlCrd &= "ISNULL(Src.FTSrcRetDocRef,'') AS AUTHORIZINGTERMID,"
            tSqlCrd &= "ISNULL(Edc.FTEdcOther,'') AS AUTHORIZINGTERMID,"
            tSqlCrd &= "Shd.FDShdDocDate AS AUTHORIZATIONDATETIME,"
            'tSqlCrd &= "ISNULL(Src.FTBnkCode,'') AS MEDIAISSUERID"
            tSqlCrd &= "CASE Src.FTRcvCode "
            tSqlCrd &= "WHEN '002' THEN ISNULL(Src.FTSrcBnkBch,'') "
            tSqlCrd &= "WHEN '009' THEN ISNULL(Src.FTBnkCode,'') END AS MEDIAISSUERID,"
            tSqlCrd &= "ISNULL(PosE.FNEdcPort,'')FNEdcPort,Src.FTRcvCode"
            If oRetail(0).FTShdDocType = "1" Then
                oDbTbl = New DataTable
                oSql.AppendLine("--## E1BPCREDITCARD ##")
                oSql.AppendLine("SELECT {0}, ")
                oSql.AppendLine(tSqlCrd)
                oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalRC Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("AND Src.FTRcvCode IN ('002','009')")
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT {0},")
                    oSql.AppendLine(tSqlCrd)
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSRC" & tPosCode & " Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                    oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                    oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                    oSql.AppendLine("WHERE {1}")
                    oSql.AppendLine("AND Src.FTRcvCode IN ('002','009')")
                Next
                oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
                oZSDINT005.oCreditCard = New List(Of cCreditCard)
                Dim oCreditCard = From oRow As DataRow In oDbTbl.Rows
                                  Select New cCreditCard With {
                                  .RETAILSTOREID = oRow("RETAILSTOREID"),
                                  .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                                  .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                                  .WORKSTATIONID = oRow("WORKSTATIONID"),
                                  .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                                  .CARDNUMBER = oRow("CARDNUMBER"),
                                  .CARDNUMBERSUFFIX = oRow("CARDNUMBERSUFFIX"),
                                  .CARDEXPIRATIONDATE = oRow("CARDEXPIRATIONDATE"),
                                  .CARDHOLDERNAME = oRow("CARDHOLDERNAME"),
                                  .ADJUDICATIONCODE = oRow("ADJUDICATIONCODE"),
                                  .AUTHORIZINGTERMID = oRow("AUTHORIZINGTERMID"),
                                  .AUTHORIZATIONDATETIME = oRow("AUTHORIZATIONDATETIME"),
                                  .MEDIAISSUERID = oRow("MEDIAISSUERID"),
                                  .FTShdDocNo = oRow("FTShdDocNo"),
                                  .TENDERSEQUENCENUMBER = oRow("TENDERSEQUENCENUMBER"),
                                  .FNEdcPort = oRow("FNEdcPort")
                               }
                If oCreditCard IsNot Nothing AndAlso oCreditCard.Count > 0 Then
                    oZSDINT005.oCreditCard = oCreditCard.ToList
                End If
            Else
                'Return
                'Get Sale
                oDbTbl = New DataTable
                oSql.AppendLine("--## E1BPCREDITCARD ##")
                oSql.AppendLine("SELECT {0}, ")
                oSql.AppendLine(tSqlCrd)
                oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalRC Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                oSql.AppendLine("WHERE Src.FTRcvCode IN ('002','009')")
                oSql.AppendLine("	AND (Shd.FTShdDocNo IN (SELECT FTShdPosCN FROM TPSTSalHD Shd WHERE {1})")
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("	OR Shd.FTShdDocNo IN (SELECT FTShdPosCN FROM TSHD" & tPosCode & " Shd WHERE {1})")
                Next
                oSql.AppendLine("	)")
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT {0},")
                    oSql.AppendLine(tSqlCrd)
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSRC" & tPosCode & " Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                    oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                    oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                    oSql.AppendLine("WHERE Src.FTRcvCode IN ('002','009')")
                    oSql.AppendLine("AND Shd.FTShdDocNo IN (SELECT FTShdPosCN FROM TSHD" & tPosCode & " Shd WHERE {1})")
                Next
                oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
                oZSDINT005.oCreditCard = New List(Of cCreditCard)
                Dim oCreditCard = From oRow As DataRow In oDbTbl.Rows
                                  Select New cCreditCard With {
                                  .RETAILSTOREID = oRow("RETAILSTOREID"),
                                  .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                                  .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                                  .WORKSTATIONID = oRow("WORKSTATIONID"),
                                  .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                                  .CARDNUMBER = oRow("CARDNUMBER"),
                                  .CARDNUMBERSUFFIX = oRow("CARDNUMBERSUFFIX"),
                                  .CARDEXPIRATIONDATE = oRow("CARDEXPIRATIONDATE"),
                                  .CARDHOLDERNAME = oRow("CARDHOLDERNAME"),
                                  .ADJUDICATIONCODE = oRow("ADJUDICATIONCODE"),
                                  .AUTHORIZINGTERMID = oRow("AUTHORIZINGTERMID"),
                                  .AUTHORIZATIONDATETIME = oRow("AUTHORIZATIONDATETIME"),
                                  .MEDIAISSUERID = oRow("MEDIAISSUERID"),
                                  .FTShdDocNo = oRow("FTShdDocNo"),
                                  .TENDERSEQUENCENUMBER = oRow("TENDERSEQUENCENUMBER"),
                                  .FNEdcPort = oRow("FNEdcPort"),
                                  .FTRcvCode = oRow("FTRcvCode")
                               }
                If oCreditCard IsNot Nothing AndAlso oCreditCard.Count > 0 Then
                    oZSDINT005.oCreditCard = oCreditCard.ToList
                End If

                'Get Return
                oDbTbl = New DataTable
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("--## E1BPCREDITCARD ##")
                oSql.AppendLine("SELECT {0}, ")
                oSql.AppendLine(tSqlCrd)
                oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalRC Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                oSql.AppendLine("WHERE {1}")
                oSql.AppendLine("AND Src.FTRcvCode IN ('002','009')")
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT {0},")
                    oSql.AppendLine(tSqlCrd)
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSRC" & tPosCode & " Src ON Shd.FTShdDocNo = Src.FTShdDocNo")
                    oSql.AppendLine("	LEFT JOIN TPSMEdc Edc ON Src.FTSrcRetDocRef = Edc.FTEdcCode")
                    oSql.AppendLine("	LEFT JOIN TPSMPosEdc PosE ON Shd.FTPosCode = PosE.FTPosCode AND Edc.FTEdcCode = PosE.FTEdcCode")
                    oSql.AppendLine("WHERE {1}")
                    oSql.AppendLine("AND Src.FTRcvCode IN ('002','009')")
                Next
                oDbTbl = oDatabase.C_CALoExecuteReader(System.String.Format(oSql.ToString, tCmdHD, tCmdWhe))
                oZSDINT005.oCreditCardR = New List(Of cCreditCard)
                Dim oCreditCardR = From oRow As DataRow In oDbTbl.Rows
                                   Select New cCreditCard With {
                                  .RETAILSTOREID = oRow("RETAILSTOREID"),
                                  .BUSINESSDAYDATE = oRow("BUSINESSDAYDATE"),
                                  .TRANSACTIONTYPECODE = oRow("TRANSACTIONTYPECODE"),
                                  .WORKSTATIONID = oRow("WORKSTATIONID"),
                                  .TRANSACTIONSEQUENCENUMBER = oRow("TRANSACTIONSEQUENCENUMBER"),
                                  .CARDNUMBER = oRow("CARDNUMBER"),
                                  .CARDNUMBERSUFFIX = oRow("CARDNUMBERSUFFIX"),
                                  .CARDEXPIRATIONDATE = oRow("CARDEXPIRATIONDATE"),
                                  .CARDHOLDERNAME = oRow("CARDHOLDERNAME"),
                                  .ADJUDICATIONCODE = oRow("ADJUDICATIONCODE"),
                                  .AUTHORIZINGTERMID = oRow("AUTHORIZINGTERMID"),
                                  .AUTHORIZATIONDATETIME = oRow("AUTHORIZATIONDATETIME"),
                                  .MEDIAISSUERID = oRow("MEDIAISSUERID"),
                                  .FTShdDocNo = oRow("FTShdDocNo"),
                                  .TENDERSEQUENCENUMBER = oRow("TENDERSEQUENCENUMBER"),
                                  .FNEdcPort = oRow("FNEdcPort")
                               }
                If oCreditCardR IsNot Nothing AndAlso oCreditCardR.Count > 0 Then
                    oZSDINT005.oCreditCardR = oCreditCardR.ToList
                End If
            End If

#End Region
        Catch ex As Exception
        Finally
            'If oConn.State = ConnectionState.Open Then oConn.Close()
            'oConn = Nothing
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
            oDocNo = Nothing
        End Try
        Return oZSDINT005
    End Function

    '''' <summary>
    '''' Create File Xml
    '''' </summary>
    '''' <returns>Boolean True:Success, False:Error</returns>
    'Private Function C_SETbCreateFileXml(ByVal poZSDINT005 As cZSDINT005, ByRef poItem As List(Of cExportTemplate.cItem)) As Boolean
    '    Dim oItem As New List(Of cExportTemplate.cItem)
    '    Dim bCrtXml As Boolean = True
    '    Dim tData As String = ""
    '    Dim tFileNameTemp As String = ""
    '    Try
    '        oItem = (From oDoc In poItem Where oDoc.nStaCrtFile = "1" Order By oDoc.tDocDate, oDoc.tDocTime).ToList

    '        'ปรับโครงสร้างไฟล์ 1.กำหนด Element ใหม่ 2.ลบ POSDW- ออก
    '        tData = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf
    '        tData &= "<ns0:MT_POSSales xmlns:ns0=""urn:bigcamera.co.th:ecc:POSSales"">" & vbCrLf
    '        'tData = "<POSDW-POSTR_CREATEMULTIPLE03>" & vbCrLf
    '        For Each oDocNo In oItem
    '            'tData &= vbTab & "<POSDW-E1BPTRANSACTION>" & vbCrLf
    '            tData &= vbTab & "<E1BPTRANSACTION>" & vbCrLf
    '            'E1BPTRANSACTION
    '            Dim oTran = (From o In poZSDINT005.oTransaction Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oTran IsNot Nothing And oTran.Count > 0 Then
    '                For nI = 0 To oTran.Count - 1
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPTRANSACTION>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPTRANSACTION>" & vbCrLf
    '                    tData &= String.Format(tC_Tran, oTran(nI).RETAILSTOREID, oTran(nI).BUSINESSDAYDATE, oTran(nI).TRANSACTIONTYPECODE, oTran(nI).WORKSTATIONID,
    '                                           oTran(nI).TRANSACTIONSEQUENCENUMBER, Convert.ToDateTime(oTran(nI).BEGINDATETIMESTAMP).ToString("yyyyMMddHHmmss"),
    '                                           Convert.ToDateTime(oTran(nI).ENDDATETIMESTAMP).ToString("yyyyMMddHHmmss"), oTran(nI).OPERATORID,
    '                                           oTran(nI).TRANSACTIONCURRENCY, oTran(nI).TRANSACTIONCURRENCY_ISO, oTran(nI).PARTNERID)
    '                    'oTran(nI).TRANSACTIONCURRENCY, oTran(nI).TRANSACTIONCURRENCY_ISO, oTran(nI).ZREFERENCE0, oTran(nI).ZREFERENCE1, oTran(nI).PARTNERID)
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPTRANSACTION>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPTRANSACTION>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPTRANSACTION>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPTRANSACTION>" & vbCrLf
    '                tData &= String.Format(tC_Tran, "", "", "", "", "", "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPTRANSACTION>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPTRANSACTION>" & vbCrLf
    '            End If

    '            'E1BPCUSTOMERDETAILS
    '            Dim oCst = (From o In poZSDINT005.oCustomerDetails Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oCst IsNot Nothing And oCst.Count > 0 Then
    '                For ni = 0 To oCst.Count - 1
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPCUSTOMERDETAILS>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPCUSTOMERDETAILS>" & vbCrLf
    '                    tData &= String.Format(tC_Cust, oCst(ni).RETAILSTOREID, oCst(ni).BUSINESSDAYDATE, oCst(ni).TRANSACTIONTYPECODE, oCst(ni).WORKSTATIONID,
    '                                           oCst(ni).TRANSACTIONSEQUENCENUMBER, oCst(ni).CUSTOMERINFOTYPE, oCst(ni).DATAELEMENTID, oCst(ni).DATAELEMENTVALUE)
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPCUSTOMERDETAILS>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPCUSTOMERDETAILS>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPCUSTOMERDETAILS>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPCUSTOMERDETAILS>" & vbCrLf
    '                tData &= String.Format(tC_Cust, "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPCUSTOMERDETAILS>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPCUSTOMERDETAILS>" & vbCrLf
    '            End If

    '            'E1BPRETAILLINEITEM
    '            Dim oRetail = (From o In poZSDINT005.oRetailLineItem Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oRetail IsNot Nothing And oRetail.Count > 0 Then
    '                For nI = 0 To oRetail.Count - 1
    '                    'ราคาสินค้าก่อนลดไม่รวมภาษี '*CH 19-09-2017
    '                    Dim cNormalSalAmt As Double = 0.00
    '                    cNormalSalAmt = Convert.ToDouble(oRetail(nI).NORMALSALESAMOUNT) * 100.0 / (100.0 + oRetail(nI).FCShdVatRate) '(100.0 + cCNVB.cVB_CmpVatAmt)
    '                    If oRetail(nI).FTShdDocType = "9" Then cNormalSalAmt *= -1 '*CH เมื่อเป็นเอกสารคืนให้ติดลบ
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPRETAILLINEITEM>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPRETAILLINEITEM>" & vbCrLf
    '                    tData &= String.Format(tC_Retail, oRetail(nI).RETAILSTOREID, oRetail(nI).BUSINESSDAYDATE, "1001", oRetail(nI).WORKSTATIONID,
    '                                           oRetail(nI).TRANSACTIONSEQUENCENUMBER, oRetail(nI).RETAILSEQUENCENUMBER, oRetail(nI).RETAILTYPECODE, oRetail(nI).ITEMIDQUALIFIER,
    '                                           oRetail(nI).ITEMID, oRetail(nI).RETAILQUANTITY, oRetail(nI).SALESUNITOFMEASURE, oRetail(nI).SALESAMOUNT, cNormalSalAmt.ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
    '                                           oRetail(nI).SERIALNUMBER, oRetail(nI).PROMOTIONID, oRetail(nI).ACTUALUNITPRICE, oRetail(nI).UNITS)
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPRETAILLINEITEM>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPRETAILLINEITEM>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPRETAILLINEITEM>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPRETAILLINEITEM>" & vbCrLf
    '                tData &= String.Format(tC_Retail, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPRETAILLINEITEM>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPRETAILLINEITEM>" & vbCrLf
    '            End If

    '            'E1BPLINEITEMDISCOUNT
    '            Dim oDiscount = (From o In poZSDINT005.oLineItemDiscount Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oDiscount IsNot Nothing And oDiscount.Count > 0 Then
    '                For nI = 0 To oDiscount.Count - 1
    '                    'ส่วนลดไม่รวมภาษี '*CH 19-09-2017
    '                    Dim tZMW1 As String = oDiscount(nI).DISCOUNTTYPECODE
    '                    Dim cDisAmt As Double = Convert.ToDouble(oDiscount(nI).REDUCTIONAMOUNT)
    '                    'Dim cSalRetrn As Double = 1
    '                    'If cDisAmt < 0 Then
    '                    '    cSalRetrn = -1
    '                    '    cDisAmt *= -1
    '                    'End If
    '                    'If tZMW1 = "ZMW1" Then
    '                    '    Dim cSale As Double = cDisAmt
    '                    '    Dim cNormal As Double = ((cDisAmt * 100) / (100 + oDiscount(nI).FCShdVatRate))
    '                    '    cDisAmt = cSale - cNormal
    '                    'Else
    '                    '    cDisAmt = ((cDisAmt * 100) / (100 + oDiscount(nI).FCShdVatRate)) * cSalRetrn
    '                    'End If
    '                    Select Case tZMW1.ToUpper
    '                        Case "ZMW1" 'VAT
    '                            Dim cSale As Double = cDisAmt
    '                            Dim cNormal As Double = ((cDisAmt * 100) / (100 + oDiscount(nI).FCShdVatRate))
    '                            cDisAmt = cSale - cNormal
    '                            If oDiscount(nI).FTShdDocType = "9" Then cDisAmt *= -1 'Return
    '                        Case Else 'Discount (Ontop,Pmt)
    '                            cDisAmt = ((cDisAmt * 100) / (100 + oDiscount(nI).FCShdVatRate))
    '                            If oDiscount(nI).FTShdDocType = "1" Then cDisAmt *= -1 'Sale
    '                    End Select
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPLINEITEMDISCOUNT>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPLINEITEMDISCOUNT>" & vbCrLf
    '                    tData &= String.Format(tC_Discount, oDiscount(nI).RETAILSTOREID, oDiscount(nI).BUSINESSDAYDATE, oDiscount(nI).TRANSACTIONTYPECODE, oDiscount(nI).WORKSTATIONID,
    '                                           oDiscount(nI).TRANSACTIONSEQUENCENUMBER, oDiscount(nI).RETAILSEQUENCENUMBER, oDiscount(nI).DISCOUNTSEQUENCENUMBER,
    '                                           oDiscount(nI).DISCOUNTTYPECODE, cDisAmt.ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")))
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPLINEITEMDISCOUNT>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPLINEITEMDISCOUNT>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPLINEITEMDISCOUNT>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPLINEITEMDISCOUNT>" & vbCrLf
    '                tData &= String.Format(tC_Discount, "", "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPLINEITEMDISCOUNT>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPLINEITEMDISCOUNT>" & vbCrLf
    '            End If

    '            'E1BPLINEITEMCOMMISSI
    '            Dim oCommis = (From o In poZSDINT005.oLineItemCommissi Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oCommis IsNot Nothing And oCommis.Count > 0 Then
    '                For nI = 0 To oCommis.Count - 1
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPLINEITEMCOMMISSI>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPLINEITEMCOMMISSI>" & vbCrLf
    '                    tData &= String.Format(tC_Commis, oCommis(nI).RETAILSTOREID, oCommis(nI).BUSINESSDAYDATE, oCommis(nI).TRANSACTIONTYPECODE, oCommis(nI).WORKSTATIONID,
    '                                           oCommis(nI).TRANSACTIONSEQUENCENUMBER, oCommis(nI).COMMISIONSEQUENCENUMBER, oCommis(nI).COMMEMPLOYEEQUAL,
    '                                           oCommis(nI).COMMISSIONEMPLOYEEID, oCommis(nI).COMMISSIONAMOUNT)
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPLINEITEMCOMMISSI>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPLINEITEMCOMMISSI>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPLINEITEMCOMMISSI>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPLINEITEMCOMMISSI>" & vbCrLf
    '                tData &= String.Format(tC_Commis, "", "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPLINEITEMCOMMISSI>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPLINEITEMCOMMISSI>" & vbCrLf
    '            End If

    '            ''E1BPTRANSACTEXTENSIO
    '            'Dim oTranExt = (From o In poZSDINT005.oTransactExtensio Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            'If oTranExt IsNot Nothing And oTranExt.Count > 0 Then
    '            '    'For nI = 0 To oTranExt.Count - 1
    '            '    '    tData &= vbTab & vbTab & "<POSDW-E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    '    tData &= String.Format(tC_TranExt, oTranExt(nI).RETAILSTOREID, oTranExt(nI).BUSINESSDAYDATE, oTranExt(nI).TRANSACTIONTYPECODE, oTranExt(nI).WORKSTATIONID,
    '            '    '                           oTranExt(nI).TRANSACTIONSEQUENCENUMBER, oTranExt(nI).FIELDGROUP, oTranExt(nI).FIELDNAME, oTranExt(nI).FIELDVALUE)
    '            '    '    tData &= vbTab & vbTab & "</POSDW-E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    'Next
    '            '    'tData &= vbTab & vbTab & "<POSDW-E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    tData &= vbTab & vbTab & "<E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    tData &= String.Format(tC_TranExt, oTranExt(0).RETAILSTOREID, oTranExt(0).BUSINESSDAYDATE, oTranExt(0).TRANSACTIONTYPECODE, oTranExt(0).WORKSTATIONID,
    '            '                           oTranExt(0).TRANSACTIONSEQUENCENUMBER, oTranExt(0).FIELDGROUP, oTranExt(0).FIELDNAME, oTranExt(0).FIELDVALUE)
    '            '    'tData &= vbTab & vbTab & "</POSDW-E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    tData &= vbTab & vbTab & "</E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            'Else
    '            '    'tData &= vbTab & vbTab & "<POSDW-E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    tData &= vbTab & vbTab & "<E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    tData &= String.Format(tC_TranExt, "", "", "", "", "", "", "", "")
    '            '    'tData &= vbTab & vbTab & "</POSDW-E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            '    tData &= vbTab & vbTab & "</E1BPTRANSACTEXTENSIO>" & vbCrLf
    '            'End If

    '            'E1BPLINEITEMEXTENSIO
    '            Dim oItemExt = (From o In poZSDINT005.oLineItemExtensio Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oItemExt IsNot Nothing And oItemExt.Count > 0 Then
    '                For nI = 0 To oItemExt.Count - 1
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPLINEITEMEXTENSIO>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPLINEITEMEXTENSIO>" & vbCrLf
    '                    tData &= String.Format(tC_ItemExt, oItemExt(nI).RETAILSTOREID, oItemExt(nI).BUSINESSDAYDATE, oItemExt(nI).TRANSACTIONTYPECODE, oItemExt(nI).WORKSTATIONID,
    '                                           oItemExt(nI).TRANSACTIONSEQUENCENUMBER, oItemExt(nI).RETAILSEQUENCENUMBER, oItemExt(nI).FIELDGROUP, oItemExt(nI).FIELDNAME, oItemExt(nI).FIELDVALUE)
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPLINEITEMEXTENSIO>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPLINEITEMEXTENSIO>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPLINEITEMEXTENSIO>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPLINEITEMEXTENSIO>" & vbCrLf
    '                tData &= String.Format(tC_ItemExt, "", "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPLINEITEMEXTENSIO>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPLINEITEMEXTENSIO>" & vbCrLf
    '            End If
    '            'tData &= vbTab & vbTab & "<POSDW-E1BPLINEITEMEXTENSIO>" & vbCrLf
    '            'tData &= String.Format(tC_ItemExt, "", "", "", "", "", "", "", "")
    '            'tData &= vbTab & vbTab & "</POSDW-E1BPLINEITEMEXTENSIO>" & vbCrLf

    '            'E1BPTENDER
    '            Dim oTender = (From o In poZSDINT005.oTender Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oTender IsNot Nothing And oTender.Count > 0 Then
    '                For nI = 0 To oTender.Count - 1
    '                    Dim tRefID As String = ""
    '                    Dim atRefID As String() = oTender(nI).REFERENCEID.Split(";")
    '                    'Select Case atRefID.Count
    '                    '    Case = 4
    '                    '        'tRefID = atRefID(2) & atRefID(3).PadLeft(6 - atRefID(2).Length, "0")
    '                    '        'Get FTTrmName By Code '*CH 24-10-2017
    '                    '        Dim oSql As New System.Text.StringBuilder
    '                    '        Dim oDatabase As New cDatabaseLocal
    '                    '        Dim oDbTbl As New DataTable
    '                    '        oSql.AppendLine("SELECT FTTrmCode,FTTrmName FROM TCNMTrmCharge WHERE FTTrmCode = '" & atRefID(2) & "'")
    '                    '        oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
    '                    '        If oDbTbl IsNot Nothing AndAlso oDbTbl.Rows.Count > 0 Then
    '                    '            tRefID = oDbTbl.Rows(0)("FTTrmName")
    '                    '        Else
    '                    '            tRefID = atRefID(2)
    '                    '        End If
    '                    '        tRefID &= atRefID(3).PadLeft(3, "0")

    '                    '        oSql = Nothing
    '                    '        oDatabase = Nothing
    '                    '        oDbTbl = Nothing

    '                    '        'Case = 2 : tRefID = atRefID(0).PadLeft(3, "0") & atRefID(1).PadLeft(3, "0")
    '                    '        'Case = 0 : tRefID = atRefID(0)
    '                    '        'Case Else : tRefID = oTender(nI).REFERENCEID
    '                    'End Select
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPTENDER>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPTENDER>" & vbCrLf
    '                    tData &= String.Format(tC_Tender, oTender(nI).RETAILSTOREID, oTender(nI).BUSINESSDAYDATE, oTender(nI).TRANSACTIONTYPECODE, oTender(nI).WORKSTATIONID,
    '                                           oTender(nI).TRANSACTIONSEQUENCENUMBER, oTender(nI).TENDERSEQUENCENUMBER, oTender(nI).TENDERTYPECODE,
    '                                           Convert.ToDouble(oTender(nI).TENDERAMOUNT).ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
    '                                           oTender(nI).TENDERCURRENCY, oTender(nI).TENDERID, oTender(nI).ACCOUNTNUMBER, tRefID)
    '                    'oTender(nI).TENDERCURRENCY, oTender(nI).FTEdcOther, oTender(nI).ACCOUNTNUMBER, tRefID)
    '                    'oTender(nI).TENDERCURRENCY, oTender(nI).TENDERID, oTender(nI).ACCOUNTNUMBER, tRefID & oTender(nI).FTEdcOther)
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPTENDER>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPTENDER>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPTENDER>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPTENDER>" & vbCrLf
    '                tData &= String.Format(tC_Tender, "", "", "", "", "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPTENDER>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPTENDER>" & vbCrLf
    '            End If

    '            'E1BPCREDITCARD
    '            Dim oCredit = (From o In poZSDINT005.oCreditCard Where o.TRANSACTIONSEQUENCENUMBER = oDocNo.tItem).ToList
    '            If oCredit IsNot Nothing And oCredit.Count > 0 Then
    '                For nI = 0 To oCredit.Count - 1
    '                    Dim tAdjCode As String = ""
    '                    Dim atAdjCode As String() = oCredit(nI).ADJUDICATIONCODE.Split(";")
    '                    Select Case atAdjCode.Count
    '                        Case > 0 : tAdjCode = atAdjCode(0)
    '                        Case = 0 : tAdjCode = atAdjCode(0)
    '                        Case Else : tAdjCode = oCredit(nI).ADJUDICATIONCODE
    '                    End Select
    '                    'tData &= vbTab & vbTab & "<POSDW-E1BPCREDITCARD>" & vbCrLf
    '                    tData &= vbTab & vbTab & "<E1BPCREDITCARD>" & vbCrLf
    '                    tData &= String.Format(tC_Credit, oCredit(nI).RETAILSTOREID, oCredit(nI).BUSINESSDAYDATE, oCredit(nI).TRANSACTIONTYPECODE, oCredit(nI).WORKSTATIONID,
    '                                           oCredit(nI).TRANSACTIONSEQUENCENUMBER, oCredit(nI).PAYMENTCARD, oCredit(nI).CARDNUMBER, oCredit(nI).CARDNUMBERSUFFIX,
    '                                           oCredit(nI).CARDEXPIRATIONDATE, oCredit(nI).CARDHOLDERNAME, tAdjCode, oCredit(nI).AUTHORIZINGTERMID,
    '                                           oCredit(nI).AUTHORIZATIONDATETIME, oCredit(nI).MEDIAISSUERID, "*****")
    '                    'tData &= vbTab & vbTab & "</POSDW-E1BPCREDITCARD>" & vbCrLf
    '                    tData &= vbTab & vbTab & "</E1BPCREDITCARD>" & vbCrLf
    '                Next
    '            Else
    '                'tData &= vbTab & vbTab & "<POSDW-E1BPCREDITCARD>" & vbCrLf
    '                tData &= vbTab & vbTab & "<E1BPCREDITCARD>" & vbCrLf
    '                tData &= String.Format(tC_Credit, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
    '                'tData &= vbTab & vbTab & "</POSDW-E1BPCREDITCARD>" & vbCrLf
    '                tData &= vbTab & vbTab & "</E1BPCREDITCARD>" & vbCrLf
    '            End If

    '            'tData &= vbTab & "</POSDW-E1BPTRANSACTION>" '& vbCrLf
    '            tData &= vbTab & "</E1BPTRANSACTION>" '& vbCrLf
    '            'Create File
    '            C_CALxDataToFileText(tFullPath, tData)
    '            tData = ""
    '        Next
    '        'tData &= "</POSDW-POSTR_CREATEMULTIPLE03>"
    '        tData &= "</ns0:MT_POSSales>"

    '        'Create File
    '        C_CALxDataToFileText(tFullPath, tData)

    '        'Rename TypeFile
    '        tFileNameTemp = Path.GetFileName(tFullPath).Replace(".tmp", ".xml")
    '        My.Computer.FileSystem.RenameFile(tFullPath, tFileNameTemp)
    '        tFullPath = tFullPath.Replace(".tmp", ".xml")
    '    Catch ex As Exception
    '        bCrtXml = False
    '        Throw New Exception(ex.Message)
    '    Finally
    '        oItem = Nothing
    '    End Try
    '    Return bCrtXml
    'End Function
    ''' <summary>
    ''' Create File Xml
    ''' </summary>
    ''' <returns>Boolean True:Success, False:Error</returns>
    Private Function C_SETbCreateFileXml(ByVal poZSDINT005 As cZSDINT005, ByRef poItem As List(Of cExportTemplate.cItem), ByVal pnRunning As Integer) As Boolean
        Dim oItem As New List(Of cExportTemplate.cItem)
        Dim bCrtXml As Boolean = True
        Dim tData As String = ""
        Dim tFileNameTemp As String = ""
        Dim tPath As String = ""
        Dim tPathFile As String = ""
        Dim nRunning As Integer = 0
        Dim tSeqNo As String = ""
        Dim tSeqDocNo As String = ""
        Dim cZDIFX As Double = 0.00
        Dim cZDIFY As Double = 0.00
        Dim cVatHD As Double = 0.00
        Try
            oItem = (From oDoc In poItem Where oDoc.nStaCrtFile = "1" Order By oDoc.tDocDate, oDoc.tDocTime).ToList

            nRunning = pnRunning
            For Each oDocNo In oItem
                cZDIFX = 0.0
                cZDIFY = 0.0
                cVatHD = 0.0

                '*CH 11-12-2017 Get SeqNo BY Doc
                'TRANSACTIONSEQUENCENUMBER Replace SYYBCHAA123456Y 
                tSeqNo = C_GETtSeqNoZSDINT005(oDocNo.tItem)
                tSeqDocNo = Left(oDocNo.tItem, 1) & Now.ToString("yy") & Mid(oDocNo.tItem, 6, 3) & Mid(oDocNo.tItem, 10, 2) & Right(oDocNo.tItem, 6) & tSeqNo

                tFileNameTemp = String.Format(tFullPath, nRunning.ToString.PadLeft(4, "0"))

                tData = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf
                tData &= "<ns0:MT_POSSales xmlns:ns0=""urn:bigcamera.co.th:ecc:POSSales"">" & vbCrLf

                'E1BPTRANSACTION
                Dim oTran = (From o In poZSDINT005.oTransaction Where o.FTShdDocNo = oDocNo.tItem).ToList
                If oTran IsNot Nothing And oTran.Count > 0 Then
                    For nI = 0 To oTran.Count - 1
                        tData &= vbTab & "<E1BPTRANSACTION>" & vbCrLf
                        tData &= String.Format(tC_Tran, oTran(nI).RETAILSTOREID, oTran(nI).BUSINESSDAYDATE, oTran(nI).TRANSACTIONTYPECODE, oTran(nI).WORKSTATIONID,
                                               tSeqDocNo, Convert.ToDateTime(oTran(nI).BEGINDATETIMESTAMP).ToString("yyyyMMddHHmmss"),
                                               Convert.ToDateTime(oTran(nI).ENDDATETIMESTAMP).ToString("yyyyMMddHHmmss"), oTran(nI).OPERATORID,
                                               oTran(nI).TRANSACTIONCURRENCY, oTran(nI).TRANSACTIONCURRENCY_ISO, oTran(nI).PARTNERID)
                        tData &= vbTab & "</E1BPTRANSACTION>" & vbCrLf

                        cVatHD += oTran(nI).FCShdVat
                    Next
                Else
                    tData &= vbTab & "<E1BPTRANSACTION>" & vbCrLf
                    tData &= String.Format(tC_Tran, "", "", "", "", "", "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPTRANSACTION>" & vbCrLf
                End If

                'E1BPCUSTOMERDETAILS
                Dim oCst = (From o In poZSDINT005.oCustomerDetails Where o.FTShdDocNo = oDocNo.tItem).ToList
                If oCst IsNot Nothing And oCst.Count > 0 Then
                    For ni = 0 To oCst.Count - 1
                        tData &= vbTab & "<E1BPCUSTOMERDETAILS>" & vbCrLf
                        tData &= String.Format(tC_Cust, oCst(ni).RETAILSTOREID, oCst(ni).BUSINESSDAYDATE, oCst(ni).TRANSACTIONTYPECODE, oCst(ni).WORKSTATIONID,
                                               tSeqDocNo, oCst(ni).CUSTOMERINFOTYPE, oCst(ni).DATAELEMENTID, oCst(ni).DATAELEMENTVALUE)
                        tData &= vbTab & "</E1BPCUSTOMERDETAILS>" & vbCrLf
                    Next
                Else
                    tData &= vbTab & "<E1BPCUSTOMERDETAILS>" & vbCrLf
                    tData &= String.Format(tC_Cust, "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPCUSTOMERDETAILS>" & vbCrLf
                End If

                'E1BPRETAILLINEITEM
                Dim oRetail = (From o In poZSDINT005.oRetailLineItem Where o.FTShdDocNo = oDocNo.tItem).ToList
                If oRetail IsNot Nothing And oRetail.Count > 0 Then
                    For nI = 0 To oRetail.Count - 1
                        'ราคาสินค้าก่อนลดไม่รวมภาษี '*CH 19-09-2017
                        Dim cSALESAMOUNT As Double = 0.00
                        Dim cNormalSalAmt As Double = 0.00

                        'If oRetail(nI).RETAILTYPECODE = cCNVB.tVB_SaleItemCode OrElse oRetail(nI).RETAILTYPECODE = cCNVB.tVB_ReturnCode Then
                        If oRetail(nI).RETAILTYPECODE = cCNVB.tVB_SaleItemCode OrElse oRetail(nI).RETAILTYPECODE = cCNVB.tVB_SaleFreeItemCode OrElse
                           oRetail(nI).RETAILTYPECODE = cCNVB.tVB_SaleServiceItemCode OrElse oRetail(nI).RETAILTYPECODE = cCNVB.tVB_ReturnItemCode OrElse
                           oRetail(nI).RETAILTYPECODE = cCNVB.tVB_ReturnFreeItemCode OrElse oRetail(nI).RETAILTYPECODE = cCNVB.tVB_ReturnServiceItemCode Then
                            cSALESAMOUNT = Convert.ToDouble(oRetail(nI).SALESAMOUNT) * 100.0 / (100.0 + oRetail(nI).FCShdVatRate) '(100.0 + cCNVB.cVB_CmpVatAmt)
                            cNormalSalAmt = Convert.ToDouble(oRetail(nI).NORMALSALESAMOUNT)

                            cZDIFX += Math.Round(cSALESAMOUNT, 2)

                            '*CH เมื่อเป็นเอกสารคืนให้ติดลบ
                            If oRetail(nI).FTShdDocType = "9" Then cSALESAMOUNT *= -1
                        End If
                        tData &= vbTab & "<E1BPRETAILLINEITEM>" & vbCrLf
                        tData &= String.Format(tC_Retail, oRetail(nI).RETAILSTOREID, oRetail(nI).BUSINESSDAYDATE, oRetail(nI).TRANSACTIONTYPECODE, oRetail(nI).WORKSTATIONID,
                                               tSeqDocNo, oRetail(nI).RETAILSEQUENCENUMBER, oRetail(nI).RETAILTYPECODE, oRetail(nI).ITEMIDQUALIFIER,
                                               oRetail(nI).ITEMID, oRetail(nI).RETAILQUANTITY, oRetail(nI).SALESUNITOFMEASURE,
                                               cSALESAMOUNT.ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")), cNormalSalAmt.ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
                                               oRetail(nI).SERIALNUMBER, oRetail(nI).PROMOTIONID, oRetail(nI).ACTUALUNITPRICE, oRetail(nI).UNITS)
                        tData &= vbTab & "</E1BPRETAILLINEITEM>" & vbCrLf
                    Next
                Else
                    tData &= vbTab & "<E1BPRETAILLINEITEM>" & vbCrLf
                    tData &= String.Format(tC_Retail, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPRETAILLINEITEM>" & vbCrLf
                End If

                'E1BPLINEITEMDISCOUNT
                Dim oDiscount = (From o In poZSDINT005.oLineItemDiscount Where o.FTShdDocNo = oDocNo.tItem).ToList
                If oDiscount IsNot Nothing And oDiscount.Count > 0 Then
                    For nI = 0 To oDiscount.Count - 1
                        'ส่วนลดไม่รวมภาษี '*CH 19-09-2017
                        Dim tZMW1 As String = oDiscount(nI).DISCOUNTTYPECODE
                        Dim cDisAmt As Double = Convert.ToDouble(oDiscount(nI).REDUCTIONAMOUNT)
                        Select Case tZMW1.ToUpper
                            Case "ZMW1" 'VAT
                                Dim atDisChgType As String() = oDiscount(nI).FTShdDocType.Split(";")
                                Dim cSale As Double = cDisAmt
                                Dim cDis As Double = ((Convert.ToDouble(atDisChgType(0)) * 100) / (100 + oDiscount(nI).FCShdVatRate))
                                Dim cChg As Double = ((Convert.ToDouble(atDisChgType(1)) * 100) / (100 + oDiscount(nI).FCShdVatRate))
                                Dim tType As String = atDisChgType(2)
                                Dim cNormal As Double = ((cDisAmt * 100) / (100 + oDiscount(nI).FCShdVatRate))
                                'cDisAmt = cSale - cNormal
                                Dim nCountVat = (From oI In oDiscount Where oI.DISCOUNTTYPECODE.ToUpper = "ZMW1").Count
                                If nI < oDiscount.Count - 1 Then
                                    cDisAmt = Math.Round((cNormal - cDis + cChg) * oDiscount(nI).FCShdVatRate / 100, 2)
                                    cVatHD -= cDisAmt
                                Else
                                    cDisAmt = cVatHD
                                End If

                                'cZDIFX += (cNormal - cDis + cChg)
                                cZDIFX += Math.Round(cDisAmt, 2)

                                If tType = "9" Then cDisAmt *= -1 'Return
                            Case "ZSI1" 'Charge
                                cDisAmt = ((cDisAmt * 100) / (100 + oDiscount(nI).FCShdVatRate))
                                cZDIFX += Math.Round(cDisAmt, 2)

                                If oDiscount(nI).FTShdDocType = "9" Then cDisAmt *= -1 'Charge
                            Case Else 'Discount (Ontop,Pmt)
                                cDisAmt = ((cDisAmt * 100) / (100 + oDiscount(nI).FCShdVatRate))
                                cZDIFY += Math.Round(cDisAmt, 2)

                                If oDiscount(nI).FTShdDocType = "1" Then cDisAmt *= -1 'Sale
                        End Select

                        If cDisAmt <> 0 Then
                            tData &= vbTab & "<E1BPLINEITEMDISCOUNT>" & vbCrLf
                            tData &= String.Format(tC_Discount, oDiscount(nI).RETAILSTOREID, oDiscount(nI).BUSINESSDAYDATE, oDiscount(nI).TRANSACTIONTYPECODE, oDiscount(nI).WORKSTATIONID,
                                                   tSeqDocNo, oDiscount(nI).RETAILSEQUENCENUMBER, nI + 1,
                                                   oDiscount(nI).DISCOUNTTYPECODE, cDisAmt.ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")))
                            'tSeqDocNo, oDiscount(nI).RETAILSEQUENCENUMBER, oDiscount(nI).DISCOUNTSEQUENCENUMBER,
                            tData &= vbTab & "</E1BPLINEITEMDISCOUNT>" & vbCrLf
                        Else
                            If oDiscount.Count = 1 Then
                                tData &= vbTab & "<E1BPLINEITEMDISCOUNT>" & vbCrLf
                                tData &= String.Format(tC_Discount, "", "", "", "", "", "", "", "", "")
                                tData &= vbTab & "</E1BPLINEITEMDISCOUNT>" & vbCrLf
                            End If
                        End If
                    Next
                Else
                    tData &= vbTab & "<E1BPLINEITEMDISCOUNT>" & vbCrLf
                    tData &= String.Format(tC_Discount, "", "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPLINEITEMDISCOUNT>" & vbCrLf
                End If

                'E1BPLINEITEMCOMMISSI
                Dim oCommis = (From o In poZSDINT005.oLineItemCommissi Where o.FTShdDocNo = oDocNo.tItem).ToList
                If oCommis IsNot Nothing And oCommis.Count > 0 Then
                    For nI = 0 To oCommis.Count - 1
                        tData &= vbTab & "<E1BPLINEITEMCOMMISSI>" & vbCrLf
                        tData &= String.Format(tC_Commis, oCommis(nI).RETAILSTOREID, oCommis(nI).BUSINESSDAYDATE, oCommis(nI).TRANSACTIONTYPECODE, oCommis(nI).WORKSTATIONID,
                                               tSeqDocNo, oCommis(nI).COMMISIONSEQUENCENUMBER, oCommis(nI).COMMEMPLOYEEQUAL,
                                               oCommis(nI).COMMISSIONEMPLOYEEID, oCommis(nI).COMMISSIONAMOUNT)
                        tData &= vbTab & "</E1BPLINEITEMCOMMISSI>" & vbCrLf
                    Next
                Else
                    tData &= vbTab & "<E1BPLINEITEMCOMMISSI>" & vbCrLf
                    tData &= String.Format(tC_Commis, "", "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPLINEITEMCOMMISSI>" & vbCrLf
                End If

                'E1BPLINEITEMEXTENSIO
                Dim oItemExt = (From o In poZSDINT005.oLineItemExtensio Where o.FTShdDocNo = oDocNo.tItem).ToList
                If oItemExt IsNot Nothing And oItemExt.Count > 0 Then
                    For nI = 0 To oItemExt.Count - 1
                        tData &= vbTab & "<E1BPLINEITEMEXTENSIO>" & vbCrLf
                        tData &= String.Format(tC_ItemExt, oItemExt(nI).RETAILSTOREID, oItemExt(nI).BUSINESSDAYDATE, oItemExt(nI).TRANSACTIONTYPECODE, oItemExt(nI).WORKSTATIONID,
                                               tSeqDocNo, oItemExt(nI).RETAILSEQUENCENUMBER, oItemExt(nI).FIELDGROUP, oItemExt(nI).FIELDNAME, oItemExt(nI).FIELDVALUE)
                        tData &= vbTab & "</E1BPLINEITEMEXTENSIO>" & vbCrLf
                    Next
                Else
                    tData &= vbTab & "<E1BPLINEITEMEXTENSIO>" & vbCrLf
                    tData &= String.Format(tC_ItemExt, "", "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPLINEITEMEXTENSIO>" & vbCrLf
                End If

                'E1BPTENDER
                'Dim oTender = (From o In poZSDINT005.oTender Where o.FTShdDocNo = oDocNo.tItem).ToList
                Dim oTender As New List(Of cTender)
                Select Case oRetail(0).FTShdDocType
                    Case "1" 'Sale
                        oTender = (From o In poZSDINT005.oTender Where o.FTShdDocNo = oDocNo.tItem).ToList
                    Case "9" 'Return
                        Dim tShdDocNo = (From o In poZSDINT005.oDocNo Where o.FTShdDocNo = oDocNo.tItem Select o.FTShdPosCN).FirstOrDefault
                        oTender = (From o In poZSDINT005.oTender Where o.FTShdDocNo = tShdDocNo).ToList
                End Select
                Dim nRunSeqCrd As Integer = 1
                Dim nRunSeqInstall As Integer = 1
                If oTender IsNot Nothing And oTender.Count > 0 Then
                    If oRetail(0).FTShdDocType = "9" Then
                        'Return
                        Dim oTenderR = (From o In poZSDINT005.oTenderR Where o.FTShdDocNo = oDocNo.tItem).ToList
                        If oTenderR IsNot Nothing And oTenderR.Count > 0 Then
                            Dim cRtnAmt As Double = 0
                            Dim cRtnCrd As Double = 0
                            Dim cRtnInstall As Double = 0
                            For nI = 0 To oTenderR.Count - 1
                                Select Case oTenderR(nI).FTRcvCode
                                    Case "001" 'Cash
                                        cRtnAmt += Convert.ToDouble(oTenderR(nI).TENDERAMOUNT)
                                    Case "002" 'CreditCard
                                        cRtnCrd += Convert.ToDouble(oTenderR(nI).TENDERAMOUNT)
                                    Case "009" 'Installment
                                        cRtnInstall += Convert.ToDouble(oTenderR(nI).TENDERAMOUNT)
                                End Select
                            Next

                            'Sum Sale Receive
                            Dim cSalAmt As Double = 0
                            Dim cSalCrd As Double = 0
                            Dim cSalInstall As Double = 0
                            For nI = 0 To oTender.Count - 1
                                Select Case oTender(nI).FTRcvCode
                                    Case "001" 'Cash
                                        cSalAmt += Convert.ToDouble(oTender(nI).TENDERAMOUNT)
                                    Case "002" 'CreditCard
                                        cSalCrd += Convert.ToDouble(oTender(nI).TENDERAMOUNT)
                                    Case "009" 'Installment
                                        cSalInstall += Convert.ToDouble(oTender(nI).TENDERAMOUNT)
                                End Select
                            Next

                            '#### Set Tender Amount
                            'Creditcard
                            Select Case True
                                Case cRtnCrd = cSalCrd
                                Case cRtnCrd < cSalCrd
                                    Dim oITender = (From oI In oTender Where oI.FTRcvCode = "002").ToList
                                    For nI = 0 To oITender.Count - 1
                                        If (cRtnCrd - Convert.ToDouble(oITender(nI).TENDERAMOUNT)) >= 0 Then
                                            oITender(nI).TENDERAMOUNT = Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                            cRtnCrd -= Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                        Else
                                            oITender(nI).TENDERAMOUNT = cRtnCrd
                                            cRtnCrd = 0
                                        End If
                                    Next
                                Case cRtnCrd > cSalCrd
                                    cRtnCrd -= cSalCrd
                                    cRtnAmt += cRtnCrd
                            End Select

                            'Installment
                            Select Case True
                                Case cRtnInstall = cSalInstall
                                Case cRtnInstall < cSalInstall
                                    Dim oITender = (From oI In oTender Where oI.FTRcvCode = "009").ToList
                                    For nI = 0 To oITender.Count - 1
                                        If (cRtnInstall - Convert.ToDouble(oITender(nI).TENDERAMOUNT)) >= 0 Then
                                            oITender(nI).TENDERAMOUNT = Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                            cRtnInstall -= Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                        Else
                                            oITender(nI).TENDERAMOUNT = cRtnInstall
                                            cRtnInstall = 0
                                            If cRtnAmt = 0 And cRtnInstall = 0 Then cSalAmt = 0
                                        End If
                                    Next
                                Case cRtnInstall > cSalInstall
                                    cRtnInstall -= cSalInstall
                                    cRtnAmt += cRtnInstall
                            End Select

                            'Cash
                            Select Case True
                                Case cRtnAmt = cSalAmt
                                Case cRtnAmt < cSalAmt
                                    Dim oITender = (From oI In oTender Where oI.FTRcvCode = "001").ToList
                                    For nI = 0 To oITender.Count - 1
                                        If (cRtnAmt - Convert.ToDouble(oITender(nI).TENDERAMOUNT)) >= 0 Then
                                            oITender(nI).TENDERAMOUNT = Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                            cRtnAmt -= Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                        Else
                                            oITender(nI).TENDERAMOUNT = cRtnAmt
                                            cRtnAmt = 0
                                        End If
                                    Next
                                Case cRtnAmt > cSalAmt
                                    Dim oITender = (From oI In oTender Where oI.FTRcvCode = "001").ToList
                                    For nI = 0 To oITender.Count - 1
                                        If (cRtnAmt - Convert.ToDouble(oITender(nI).TENDERAMOUNT)) >= 0 Then
                                            oITender(nI).TENDERAMOUNT = Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                            cRtnAmt -= Convert.ToDouble(oITender(nI).TENDERAMOUNT)
                                        Else
                                            oITender(nI).TENDERAMOUNT = cRtnAmt
                                            cRtnAmt = 0
                                        End If
                                    Next
                                    oITender(oITender.Count - 1).TENDERAMOUNT += cRtnAmt
                            End Select

                            'Running SequenceNumber
                            Dim oItemTender = (From oI In oTender Where oI.TENDERAMOUNT <> "0").ToList
                            For nI = 0 To oItemTender.Count - 1
                                oItemTender(nI).TENDERSEQUENCENUMBER = nI + 1
                            Next
                            For nI = 0 To oTender.Count - 1
                                oTender(nI).RETAILSTOREID = oTenderR(0).RETAILSTOREID
                                oTender(nI).BUSINESSDAYDATE = oTenderR(0).BUSINESSDAYDATE
                                oTender(nI).TRANSACTIONTYPECODE = oTenderR(0).TRANSACTIONTYPECODE
                                oTender(nI).WORKSTATIONID = oTenderR(0).WORKSTATIONID
                            Next

                            nRunSeqCrd = (From oI In oItemTender Where oI.FTRcvCode = "002" Select Convert.ToInt32(oI.TENDERSEQUENCENUMBER)).FirstOrDefault
                            nRunSeqInstall = (From oI In oItemTender Where oI.FTRcvCode = "009" Select Convert.ToInt32(oI.TENDERSEQUENCENUMBER)).FirstOrDefault
                        End If
                    End If

                    For nI = 0 To oTender.Count - 1
                        Dim tRefID As String = ""
                        Dim atRefID As String() = oTender(nI).REFERENCEID.Split(";")
                        Select Case atRefID.Count
                            Case = 4
                                'tRefID = atRefID(2) & atRefID(3).PadLeft(6 - atRefID(2).Length, "0")
                                'Get FTTrmName By Code '*CH 24-10-2017
                                Dim oSql As New System.Text.StringBuilder
                                Dim oDatabase As New cDatabaseLocal
                                Dim oDbTbl As New DataTable
                                oSql.AppendLine("SELECT FTTrmCode,FTTrmName FROM TCNMTrmCharge WHERE FTTrmCode = '" & atRefID(2) & "'")
                                oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
                                If oDbTbl IsNot Nothing AndAlso oDbTbl.Rows.Count > 0 Then
                                    tRefID = oDbTbl.Rows(0)("FTTrmName")
                                Else
                                    tRefID = atRefID(2)
                                End If
                                tRefID &= atRefID(3).PadLeft(3, "0")

                                oSql = Nothing
                                oDatabase = Nothing
                                oDbTbl = Nothing
                        End Select

                        cZDIFY += Math.Round(Convert.ToDouble(oTender(nI).TENDERAMOUNT), 2)

                        If Convert.ToDouble(oTender(nI).TENDERAMOUNT) <> 0 Then
                            tData &= vbTab & "<E1BPTENDER>" & vbCrLf
                            tData &= String.Format(tC_Tender, oTender(nI).RETAILSTOREID, oTender(nI).BUSINESSDAYDATE, oTender(nI).TRANSACTIONTYPECODE, oTender(nI).WORKSTATIONID,
                                                   tSeqDocNo, oTender(nI).TENDERSEQUENCENUMBER, oTender(nI).TENDERTYPECODE,
                                                   Convert.ToDouble(oTender(nI).TENDERAMOUNT).ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
                                                   oTender(nI).TENDERCURRENCY, oTender(nI).TENDERID, oTender(nI).ACCOUNTNUMBER, tRefID)
                            tData &= vbTab & "</E1BPTENDER>" & vbCrLf
                        Else
                            If oTender.Count = 1 Then
                                tData &= vbTab & "<E1BPTENDER>" & vbCrLf
                                tData &= String.Format(tC_Tender, "", "", "", "", "", "", "", "", "", "", "", "")
                                tData &= vbTab & "</E1BPTENDER>" & vbCrLf
                            End If
                        End If
                    Next

                    'ZDIF
                    If oTender.Count > 0 And Math.Round(cZDIFX, 2) <> Math.Round(cZDIFY, 2) Then
                        tData &= vbTab & "<E1BPTENDER>" & vbCrLf
                        tData &= String.Format(tC_Tender, oTender(0).RETAILSTOREID, oTender(0).BUSINESSDAYDATE, oTender(0).TRANSACTIONTYPECODE, oTender(0).WORKSTATIONID,
                                               tSeqDocNo, oTender.Count + 1, "ZDIF", Convert.ToDouble(cZDIFX - cZDIFY).ToString("0." & StrDup(cCNVB.nVB_DecShw, "0")),
                                               "THB", "", "", "")
                        tData &= vbTab & "</E1BPTENDER>" & vbCrLf
                    End If
                Else
                    tData &= vbTab & "<E1BPTENDER>" & vbCrLf
                    tData &= String.Format(tC_Tender, "", "", "", "", "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPTENDER>" & vbCrLf
                End If

                'E1BPCREDITCARD
                'Dim oCredit = (From o In poZSDINT005.oCreditCard Where o.FTShdDocNo = oDocNo.tItem).ToList
                Dim oCredit As New List(Of cCreditCard)
                Select Case oRetail(0).FTShdDocType
                    Case "1" 'Sale
                        oCredit = (From o In poZSDINT005.oCreditCard Where o.FTShdDocNo = oDocNo.tItem).ToList
                    Case "9" 'Return
                        Dim tShdDocNo = (From o In poZSDINT005.oDocNo Where o.FTShdDocNo = oDocNo.tItem Select o.FTShdPosCN).FirstOrDefault
                        oCredit = (From o In poZSDINT005.oCreditCard Where o.FTShdDocNo = tShdDocNo).ToList
                End Select
                If oCredit IsNot Nothing And oCredit.Count > 0 Then
                    If oRetail(0).FTShdDocType = "9" Then
                        Dim oCreditR = (From o In poZSDINT005.oCreditCardR Where o.FTShdDocNo = oDocNo.tItem).ToList
                        If oCreditR IsNot Nothing And oCreditR.Count > 0 Then
                            Dim nCountCrd = (From oI In oTender Where oI.FTRcvCode = "002" And oI.TENDERAMOUNT <> "0" Select oI.FTShdDocNo).Count
                            Dim nCountInstall = (From oI In oTender Where oI.FTRcvCode = "009" And oI.TENDERAMOUNT <> "0" Select oI.FTShdDocNo).Count
                            If oCredit.Count > (nCountCrd + nCountInstall) Then
                                For nI = (nCountCrd + nCountInstall) - 1 To oCredit.Count - 1
                                    oCredit(nI).CARDHOLDERNAME = "Hold"
                                Next
                            End If

                            For nI = 0 To oCredit.Count - 1
                                oCredit(nI).RETAILSTOREID = oCreditR(0).RETAILSTOREID
                                oCredit(nI).BUSINESSDAYDATE = oCreditR(0).BUSINESSDAYDATE
                                oCredit(nI).TRANSACTIONTYPECODE = oCreditR(0).TRANSACTIONTYPECODE
                                oCredit(nI).WORKSTATIONID = oCreditR(0).WORKSTATIONID
                            Next

                            Dim oCreditCrd = (From oI In oCredit Where oI.FTRcvCode = "002").ToList
                            For nI = 0 To oCreditCrd.Count - 1
                                oCreditCrd(nI).TENDERSEQUENCENUMBER = nRunSeqCrd
                                nRunSeqCrd += 1
                            Next
                            Dim oCreditInstall = (From oI In oCredit Where oI.FTRcvCode = "009").ToList
                            For nI = 0 To oCreditInstall.Count - 1
                                oCreditInstall(nI).TENDERSEQUENCENUMBER = nRunSeqInstall
                                nRunSeqInstall += 1
                            Next
                        Else
                            For nI = 0 To oCredit.Count - 1
                                oCredit(nI).CARDHOLDERNAME = "Hold"
                            Next
                        End If
                    End If

                    For nI = 0 To oCredit.Count - 1
                        Dim tAdjCode As String = ""
                        Dim atAdjCode As String() = oCredit(nI).ADJUDICATIONCODE.Split(";")
                        Select Case atAdjCode.Count
                            Case > 0 : tAdjCode = atAdjCode(0)
                            Case = 0 : tAdjCode = atAdjCode(0)
                            Case Else : tAdjCode = oCredit(nI).ADJUDICATIONCODE
                        End Select
                        'Check Type of Creditcard
                        Dim nChkCrd As Integer = 0
                        Dim tPAYMENTCARD As String = ""
                        If IsNumeric(oCredit(nI).CARDNUMBER) Then
                            nChkCrd = Convert.ToInt32(Left(oCredit(nI).CARDNUMBER, 4))
                            Select Case True
                                'Case nChkCrd = 1800, nChkCrd = 2131, nChkCrd >= 3528 And nChkCrd <= 3589
                                '    tPAYMENTCARD = "JCB"
                                'Case nChkCrd = 2014, nChkCrd = 2149
                                '    tPAYMENTCARD = "enR"
                                'Case nChkCrd >= 3000 And nChkCrd <= 3059, nChkCrd >= 3600 And nChkCrd <= 3699, nChkCrd >= 3800 And nChkCrd <= 3889
                                '    tPAYMENTCARD = "DIN"
                                Case nChkCrd >= 3400 And nChkCrd <= 3499, nChkCrd >= 3700 And nChkCrd <= 3799
                                    tPAYMENTCARD = "AMEX"
                                'Case nChkCrd >= 3890 And nChkCrd <= 3899
                                '    tPAYMENTCARD = "caB"
                                Case nChkCrd >= 4000 And nChkCrd <= 4999
                                    tPAYMENTCARD = "VISA"
                                Case nChkCrd >= 5100 And nChkCrd <= 5599
                                    tPAYMENTCARD = "MAST"
                                    'Case nChkCrd = 5610
                                    '    tPAYMENTCARD = "ABC"
                                    'Case nChkCrd = 6011
                                    '    tPAYMENTCARD = "Disc"
                                    'Case nChkCrd = 8521
                                    '    tPAYMENTCARD = "AtEase" 
                            End Select
                        End If

                        Dim bCrtEmpty As Boolean = False
                        If oCredit(nI).CARDHOLDERNAME <> "Hold" Then
                            If oCredit(nI).MEDIAISSUERID = "EON" And oCredit(nI).FNEdcPort = "0" Then
                                bCrtEmpty = True
                            Else
                                tData &= vbTab & "<E1BPCREDITCARD>" & vbCrLf
                                tData &= String.Format(tC_Credit, oCredit(nI).RETAILSTOREID, oCredit(nI).BUSINESSDAYDATE, oCredit(nI).TRANSACTIONTYPECODE, oCredit(nI).WORKSTATIONID,
                                                       tSeqDocNo, tPAYMENTCARD, oCredit(nI).CARDNUMBER, oCredit(nI).CARDNUMBERSUFFIX,
                                                       oCredit(nI).CARDEXPIRATIONDATE, oCredit(nI).CARDHOLDERNAME, tAdjCode, oCredit(nI).AUTHORIZINGTERMID,
                                                       oCredit(nI).BUSINESSDAYDATE & Now.ToString("HHmmss"), oCredit(nI).MEDIAISSUERID, "*****", oCredit(nI).TENDERSEQUENCENUMBER)
                                tData &= vbTab & "</E1BPCREDITCARD>" & vbCrLf
                            End If
                        Else
                            bCrtEmpty = True
                        End If
                        If bCrtEmpty Then
                            If oCredit.Count = 1 Then
                                tData &= vbTab & "<E1BPCREDITCARD>" & vbCrLf
                                tData &= String.Format(tC_Credit, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
                                tData &= vbTab & "</E1BPCREDITCARD>" & vbCrLf
                            End If
                        End If
                    Next
                Else
                    tData &= vbTab & "<E1BPCREDITCARD>" & vbCrLf
                    tData &= String.Format(tC_Credit, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
                    tData &= vbTab & "</E1BPCREDITCARD>" & vbCrLf
                End If
                tData &= "</ns0:MT_POSSales>"

                'Create File
                C_CALxDataToFileText(tFileNameTemp, tData)

                'Rename TypeFile
                tPathFile = Path.GetFileName(tFileNameTemp).Replace(".tmp", ".xml")
                My.Computer.FileSystem.RenameFile(tFileNameTemp, tPathFile)

                C_SAVbTempZSDINT005(tPathFile, oDocNo, nRunning, tSeqDocNo, tSeqNo)
                nRunning += 1
            Next
        Catch ex As Exception
            bCrtXml = False
            Throw New Exception(ex.Message)
        Finally
            oItem = Nothing
        End Try
        Return bCrtXml
    End Function

    ''' <summary>
    ''' Save Data to XmlFile
    ''' </summary>
    ''' <param name="ptFileName">FullPath</param>
    ''' <param name="ptData">Data to save</param>
    Private Sub C_CALxDataToFileText(ptFileName As String, ByVal ptData As String)
        Try 'ถ้าไม่มี Directory ให้สร้างขึ้นใหม่ '*CH 17-10-2014
            Dim tPath As String = ""
            Dim atPath As String() = ptFileName.Replace("/", "\").Split("\")
            For nPath = 0 To atPath.Length - 2
                tPath &= atPath(nPath) & "\"
            Next
            tPath = Left(tPath, tPath.Length - 1)
            If Not IO.Directory.Exists(tPath) Then
                IO.Directory.CreateDirectory(tPath)
            End If

            'ถ้าไม่มีไฟล์ให้สร้างขึ้นใหม่ '*CH 17-10-2014
            If Not File.Exists(ptFileName) Then
                Using oStreamWriter As New StreamWriter(ptFileName, False, New System.Text.UTF8Encoding(False))
                    oStreamWriter.Close()
                End Using
            End If

            Using oStreamWriter As New StreamWriter(ptFileName, True, New System.Text.UTF8Encoding(False))
                With oStreamWriter
                    .WriteLine(ptData)
                    .Flush()
                    .Close()
                End With
            End Using

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Upsert to TLGTZSDINT005
    ''' </summary>
    Private Function C_SAVbTempZSDINT005(ByVal ptFullPath As String, ByVal poItem As cExportTemplate.cItem, ByVal pnRunning As Integer,
                                         ByVal ptShdTaxInv As String, ByVal pnSeqNo As Integer) As Boolean
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDocNo As New List(Of cExportTemplate.cItem)
        Dim tCmdWhe As String = ""
        Dim tFileName As String = ""
        Dim bStaSav As Boolean = True
        Dim tSql As String = ""
        Dim nAffect As Integer = 0
        Try
            'FileName
            tFileName = Path.GetFileName(ptFullPath)

            'Conition
            tCmdWhe = "Shd.FTShdDocNo = '" & poItem.tItem & "'"
            tCmdWhe &= "AND Shd.FTShdDocType IN ('1','9')"

            'Update
            'Set Field Query Update '*CH 10-10-2017
            'tSql = "Sdt.FTBchCode,Sdt.FTShdDocNo,LEFT(Sdt.FTShdDocNo,1) + RIGHT(Sdt.FTShdDocNo, LEN(Sdt.FTShdDocNo) - 5) AS FTShdTaxInv,Sdt.FTPdtCode,"
            tSql = "Sdt.FTBchCode,Sdt.FTShdDocNo,'" & ptShdTaxInv & "' AS FTShdTaxInv,Sdt.FTPdtCode,"
            tSql &= "ISNULL(Pdt.FTPdtRmk,'') AS FTMcCode,Shd.FDShdDocDate,Shd.FTShdDocTime,"
            tSql &= "Sdt.FCSdtQty,Shd.FTLogCode,Sdt.FNSdtSeqNo,"
            tSql &= "CASE Shd.FTShdDocType "
            tSql &= "WHEN '1' THEN Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg) "
            tSql &= "WHEN '9' THEN (Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg)) * (-1) END AS SALESAMOUNT"

            oSql.AppendLine("UPDATE ZSDINT005 Set")
            oSql.AppendLine("	ZSDINT005.FTShdTaxInv = Sal.FTShdTaxInv,")
            oSql.AppendLine("	ZSDINT005.FTMcCode = Sal.FTMcCode,")
            oSql.AppendLine("	ZSDINT005.FDShdSaleDate = Sal.FDShdDocDate,")
            oSql.AppendLine("	ZSDINT005.FTShdSaleTime = Sal.FTShdDocTime,")
            oSql.AppendLine("	ZSDINT005.FCSdtQty = Sal.FCSdtQty,")
            oSql.AppendLine("	ZSDINT005.FCSdtNet = Sal.SALESAMOUNT,")
            oSql.AppendLine("	ZSDINT005.FTExpFileName = Case When '" & tFileName & "' <> '' THEN '" & tFileName & "' ELSE ZSDINT005.FTExpFileName END,")
            oSql.AppendLine("	ZSDINT005.FTExpStaSend = '',")
            oSql.AppendLine("	ZSDINT005.FTExpRunning = '" & pnRunning.ToString.PadLeft(4, "0") & "',")
            oSql.AppendLine("	ZSDINT005.FNExpSeqNo = ISNULL(ZSDINT005.FNExpSeqNo,0) + 1,")
            oSql.AppendLine("	ZSDINT005.FDDateUpd = CONVERT(DATE, GETDATE()),")
            oSql.AppendLine("	ZSDINT005.FTTimeUpd = CONVERT(VARCHAR(8), GETDATE(), 108),")
            oSql.AppendLine("	ZSDINT005.FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
            oSql.AppendLine("FROM TLGTZSDINT005 ZSDINT005 INNER JOIN (")
            oSql.AppendLine("	SELECT " & tSql)
            oSql.AppendLine("	FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo ")
            oSql.AppendLine("		INNER JOIN TCNMPdt Pdt ON Sdt.FTPdtCode = Pdt.FTPdtCode")
            oSql.AppendLine("	WHERE {0}")
            For Each tPosCode In cCNVB.oVB_PosCode
                oSql.AppendLine("UNION ALL")
                oSql.AppendLine("	SELECT " & tSql)
                oSql.AppendLine("	FROM TSHD" & tPosCode & "  Shd INNER JOIN TSDT" & tPosCode & "  Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo ")
                oSql.AppendLine("		INNER JOIN TCNMPdt Pdt ON Sdt.FTPdtCode = Pdt.FTPdtCode")
                oSql.AppendLine("	WHERE {0}")
            Next
            oSql.AppendLine(") Sal ON ZSDINT005.FTPdtCode = Sal.FTPdtCode")
            oSql.AppendLine("AND ZSDINT005.FTBchCode = Sal.FTBchCode")
            oSql.AppendLine("AND ZSDINT005.FTShdDocNo = Sal.FTShdDocNo")
            'oSql.AppendLine("AND ZSDINT005.FTShdTaxInv = Sal.FTShdTaxInv")
            oSql.AppendLine("AND ZSDINT005.FNSdtSeqNo = Sal.FNSdtSeqNo")
            nAffect = oDatabase.C_CALnExecuteNonQuery(String.Format(oSql.ToString, tCmdWhe))

            If nAffect < 1 Then
                'Insert
                'Set Field Query Insert '*CH 10-10-2017
                'tSql = "Sdt.FTBchCode,Sdt.FTShdDocNo,LEFT(Sdt.FTShdDocNo,1) + RIGHT(Sdt.FTShdDocNo, LEN(Sdt.FTShdDocNo) - 5),"
                tSql = "Sdt.FTBchCode,Sdt.FTShdDocNo,'" & ptShdTaxInv & "',"
                tSql &= "Sdt.FTPdtCode,Sdt.FNSdtSeqNo,ISNULL(Pdt.FTPdtRmk,''),"
                tSql &= "Shd.FDShdDocDate,Shd.FTShdDocTime,Sdt.FCSdtQty,"
                tSql &= "CASE Shd.FTShdDocType "
                tSql &= "WHEN '1' THEN Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg) "
                tSql &= "WHEN '9' THEN (Sdt.FCSdtNet + (Sdt.FCSdtDis - Sdt.FCSdtChg)) * (-1) END,"
                tSql &= "Shd.FTLogCode,'" & tFileName & "' AS FTExpFileName,"
                tSql &= "'' AS FTExpStaSend,'" & pnRunning.ToString.PadLeft(4, "0") & "'," & pnSeqNo & ","
                tSql &= "CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(8), GETDATE(), 108),'" & cCNVB.tVB_UserName & "',"
                tSql &= "CONVERT(DATE, GETDATE()),CONVERT(VARCHAR(8), GETDATE(), 108),'" & cCNVB.tVB_UserName & "'"

                oSql = New System.Text.StringBuilder
                oSql.AppendLine("INSERT INTO TLGTZSDINT005 (")
                oSql.AppendLine("	FTBchCode,FTShdDocNo,FTShdTaxInv,")
                oSql.AppendLine("	FTPdtCode,FNSdtSeqNo,FTMcCode,")
                oSql.AppendLine("	FDShdSaleDate,FTShdSaleTime,FCSdtQty,")
                oSql.AppendLine("	FCSdtNet,FTLogCode,FTExpFileName,")
                oSql.AppendLine("	FTExpStaSend,FTExpRunning,FNExpSeqNo,")
                oSql.AppendLine("	FDDateUpd,FTTimeUpd,FTWhoUpd,")
                oSql.AppendLine("	FDDateIns,FTTimeIns,FTWhoIns)")
                oSql.AppendLine("SELECT " & tSql) '*CH 10-10-2017
                oSql.AppendLine("FROM TPSTSalHD Shd INNER JOIN TPSTSalDT Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo ")
                oSql.AppendLine("	INNER JOIN TCNMPdt Pdt ON Sdt.FTPdtCode = Pdt.FTPdtCode")
                oSql.AppendLine("WHERE {0}")
                oSql.AppendLine("	AND Shd.FTShdDocNo NOT IN (SELECT FTShdDocNo FROM TLGTZSDINT005)")
                'Temp Sale
                For Each tPosCode In cCNVB.oVB_PosCode
                    oSql.AppendLine("UNION ALL")
                    oSql.AppendLine("SELECT " & tSql) '*CH 10-10-2017
                    oSql.AppendLine("FROM TSHD" & tPosCode & " Shd INNER JOIN TSDT" & tPosCode & " Sdt ON Shd.FTShdDocNo = Sdt.FTShdDocNo ")
                    oSql.AppendLine("	INNER JOIN TCNMPdt Pdt ON Sdt.FTPdtCode = Pdt.FTPdtCode")
                    oSql.AppendLine("WHERE {0}")
                    oSql.AppendLine("	AND Shd.FTShdDocNo NOT IN (SELECT FTShdDocNo FROM TLGTZSDINT005)")
                Next
                oDatabase.C_CALnExecuteNonQuery(String.Format(oSql.ToString, tCmdWhe))
            End If

        Catch ex As Exception
            bStaSav = False
            Throw New Exception(ex.Message)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDocNo = Nothing
        End Try
        Return bStaSav
    End Function

    ''' <summary>
    ''' Send file to FTP
    ''' </summary>
    Public Function C_PRCbSendFile2FTP(ByRef poItem As List(Of cExportTemplate.cItem), ByRef Optional poPathFile As List(Of String) = Nothing)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDocNo As New List(Of cExportTemplate.cItem)
        Dim oDbTbl As New DataTable
        Dim tCmdWhe As String = ""
        Dim bStaSend As Boolean = True
        Try
            'Conition
            oDocNo = (From oDoc In poItem Where oDoc.nStaSend = "1").ToList
            tCmdWhe = "FTShdDocNo IN ("
            For Each oItem In oDocNo
                tCmdWhe &= "'" & oItem.tItem & "',"
            Next
            tCmdWhe = Left(tCmdWhe, tCmdWhe.Length - 1)
            tCmdWhe &= ")"

            'Get File Name
            oSql.AppendLine("SELECT DISTINCT FTExpFileName FROM TLGTZSDINT005")
            oSql.AppendLine("WHERE {0}")
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe))

            If oDbTbl.Rows.Count > 0 Then
                For nRow = 0 To oDbTbl.Rows.Count - 1
                    If oDbTbl.Rows(nRow)("FTExpFileName").ToString <> "" Then
                        bStaSend = cCNSP.SP_PRCbSendFile2FTP(AdaConfig.cConfig.oConfigXml.tOutbox & "\Sale\" & oDbTbl.Rows(nRow)("FTExpFileName").ToString)
                        If bStaSend Then
                            'Update StaSend
                            oSql = New System.Text.StringBuilder
                            oSql.AppendLine("UPDATE TLGTZSDINT005 SET")
                            oSql.AppendLine("	FTExpStaSend = '1',")
                            oSql.AppendLine("	FDDateUpd = CONVERT(DATE, GETDATE()),")
                            oSql.AppendLine("	FTTimeUpd = CONVERT(VARCHAR(8), GETDATE(), 108),")
                            oSql.AppendLine("	FTWhoUpd = '" & cCNVB.tVB_UserName & "'")
                            oSql.AppendLine("WHERE FTExpFileName = '" & oDbTbl.Rows(nRow)("FTExpFileName").ToString & "'")
                            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

                            'Return Path File
                            poPathFile.Add(AdaConfig.cConfig.oConfigXml.tOutbox & "\Sale\" & oDbTbl.Rows(nRow)("FTExpFileName").ToString)
                        Else
                            'Error Send to FTP
                            Exit For
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            bStaSend = False
            Throw New Exception(ex.Message)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDocNo = Nothing
            oDbTbl = Nothing
        End Try
        Return bStaSend
    End Function

    ''' <summary>
    ''' Save Log Create File And Send File
    ''' </summary>
    Public Sub C_SAVxLog(ByVal poItem As List(Of cExportTemplate.cItem), ByVal pdNow As DateTime)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tCmdWhe As String = ""
        Dim tPath As String = ""
        'Dim tLogName As String = ""
        Dim tStaSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaSendTH, tC_StaSendEN)
        Dim tStaUnSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaUnSendTH, tC_StaUnSendEN)
        Dim tFmtLineHead As String = "[Export Date] {0}" & vbTab & "[Export Time] {1}" & vbTab & "[Export By] {2}"
        Dim tFmtLineColumn As String = "[Branch Code]" & vbTab & "[DocNo.]" & vbTab & "[Product Code]" & vbTab & "[Export File Name]" & vbTab & "[Status Send]"
        Dim tFmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}" & vbTab & "{4}"
        Dim tFmtLine As String = "====================================================================================="
        Try
            oSql.AppendLine("SELECT FTBchCode,FTShdDocNo,FTPdtCode,FTExpFileName,")
            oSql.AppendLine("   CASE WHEN FTExpStaSend = '1' THEN '" & tStaSend & "' ELSE '" & tStaUnSend & "' END FTExpStaSend")
            oSql.AppendLine("FROM TLGTZSDINT005")
            oSql.AppendLine("WHERE FTShdDocNo IN ({0})")
            For Each oItem In poItem
                tCmdWhe &= "'" & oItem.tItem & "',"
            Next
            tCmdWhe = Left(tCmdWhe, tCmdWhe.Length - 1)
            oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe))

            If oDbTbl.Rows.Count > 0 Then
                tPath = Path.GetDirectoryName(tPathFileLog)
                If Not IO.Directory.Exists(tPath) Then
                    IO.Directory.CreateDirectory(tPath)
                End If

                'Create File Log
                'tLogName = String.Format(cLogTemplate.tC_FileMaster, tC_LogName)
                If File.Exists(tPathFileLog) = False Then
                    Using oStreamWriter As StreamWriter = File.CreateText(tPathFileLog)
                        oStreamWriter.Close()
                    End Using
                End If

                Using oStreamWriter As StreamWriter = File.AppendText(tPathFileLog)
                    With oStreamWriter
                        .WriteLine(tFmtLine)
                        .WriteLine(String.Format(tFmtLineHead, pdNow.ToString("dd/MM/yyyy"), pdNow.ToString("HH:mm:ss"), My.Computer.Name))
                        .WriteLine(tFmtLine)
                        .WriteLine(tFmtLineColumn)
                        .Flush()
                        For nRow = 0 To oDbTbl.Rows.Count - 1
                            .WriteLine(String.Format(tFmtLineData, oDbTbl.Rows(nRow)("FTBchCode"), oDbTbl.Rows(nRow)("FTShdDocNo"), oDbTbl.Rows(nRow)("FTPdtCode"),
                                                     oDbTbl.Rows(nRow)("FTExpFileName"), oDbTbl.Rows(nRow)("FTExpStaSend")))
                            .Flush()
                        Next
                        .WriteLine("")
                        .Close()
                    End With
                End Using

                'Insert Log
                oDbTbl = New DataTable
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("SELECT DISTINCT FTExpFileName,CASE WHEN FTExpStaSend = '1' THEN '" & tStaSend & "' ELSE '" & tStaUnSend & "' END FTExpStaSend")
                oSql.AppendLine("FROM TLGTZSDINT005")
                oSql.AppendLine("WHERE FTShdDocNo IN ({0})")
                oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe))
                If oDbTbl.Rows.Count > 0 Then
                    For nRow = 0 To oDbTbl.Rows.Count - 1
                        oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 2, oDbTbl(nRow)("FTExpFileName"), oDbTbl.Rows(nRow)("FTExpStaSend"), tPathFileLog))
                    Next
                End If
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Get SeqNo generate file to TLGTZSDINT005
    ''' </summary>
    Private Function C_GETtSeqNoZSDINT005(ByVal ptShdDocNo As String) As String
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tSeqNo As String = ""
        Try
            oSql.Append("SELECT TOP 1 ISNULL(FNExpSeqNo,0) AS FNExpSeqNo FROM TLGTZSDINT005 WHERE FTShdDocNo = '" & ptShdDocNo & "'")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl IsNot Nothing Then
                If oDbTbl.Rows.Count > 0 Then
                    tSeqNo = Convert.ToInt32(oDbTbl.Rows(0)("FNExpSeqNo")) + 1
                    If tSeqNo = "1" AndAlso cCNVB.bVB_BchHQ Then tSeqNo = 2
                Else
                    tSeqNo = 1
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return tSeqNo
    End Function
#End Region

#Region "ZSDINT006"
    Public Function C_CRTbCrateFileReconcile(ByRef poZSDINT006 As List(Of cZSDINT006), ByVal ptPlant As String) As Boolean
        Dim bPrcCrt As Boolean = True
        Dim oZSDINT006 As New List(Of cZSDINT006)
        Try
            'Get Data
            oZSDINT006 = C_GEToSaleZSDINT006(ptPlant)
            If Not cCNVB.bVB_Auto Then wExpReconcile.oW_BackgroudWord.ReportProgress(60)

            'Create File
            If oZSDINT006.Count > 0 Then bPrcCrt = C_SETbCreateFile006(oZSDINT006)
            If Not cCNVB.bVB_Auto Then wExpReconcile.oW_BackgroudWord.ReportProgress(80)

            poZSDINT006 = oZSDINT006
        Catch ex As Exception
            bPrcCrt = False
        End Try
        Return bPrcCrt
    End Function

    ''' <summary>
    ''' Get Sale for Export
    ''' </summary>
    ''' <returns>Model cZSDINT006</returns>
    Private Function C_GEToSaleZSDINT006(ByVal ptPlant As String) As List(Of cZSDINT006)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim oZSDINT006 As New List(Of cZSDINT006)
        Try
            'oSql.AppendLine("SELECT FDShdSaleDate AS MKPF_BUDAT,'" & cCNVB.tVB_StoreNo & "' AS T001W_WERKS,FTShdTaxInv AS BKPF_XBLNR,")
            oSql.AppendLine("SELECT FDShdSaleDate AS MKPF_BUDAT,'" & ptPlant & "' AS T001W_WERKS,") 'FTShdTaxInv AS BKPF_XBLNR,")
            oSql.AppendLine("SUBSTRING(FTShdDocNo,1,1) + SUBSTRING(FTShdDocNo,6,LEN(FTShdDocNo)) AS BKPF_XBLNR,")
            oSql.AppendLine("FTShdTaxInv AS TICKETNO,FTExpFileName AS PFILE,FTPdtCode AS MARA_MATNR,")
            oSql.AppendLine("FTMCCode AS T023D_MATKL,FCSdtQty AS VBRP_FKLMG,FCSdtNet AS VBRP_NETWR")
            oSql.AppendLine("FROM TLGTZSDINT005")
            oSql.AppendLine("WHERE 1=1")
            If tDateFrom <> "" And tDateTo <> "" Then
                oSql.AppendLine("AND FDShdSaleDate BETWEEN '" & tDateFrom & "' AND '" & tDateTo & "'")
            End If
            oSql.AppendLine("	AND FTExpFileName <> ''")
            oSql.AppendLine("	AND FTExpStaSend = '1'")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            oZSDINT006 = New List(Of cZSDINT006)
            Dim oItem = From oRow As DataRow In oDbTbl.Rows
                        Select New cZSDINT006 With {
                              .MKPF_BUDAT = Convert.ToDateTime(oRow("MKPF_BUDAT")).ToString("dd.MM.yyyy"),
                              .T001W_WERKS = oRow("T001W_WERKS"),
                              .BKPF_XBLNR = oRow("BKPF_XBLNR"),
                              .TICKETNO = oRow("TICKETNO"),
                              .PFILE = oRow("PFILE"),
                              .MARA_MATNR = oRow("MARA_MATNR"),
                              .T023D_MATKL = oRow("T023D_MATKL"),
                              .VBRP_FKLMG = oRow("VBRP_FKLMG"),
                              .VBRP_NETWR = Convert.ToDouble(oRow("VBRP_NETWR")).ToString("0." & StrDup(cCNVB.nVB_DecShw, "0"))
                           }
            If oItem IsNot Nothing AndAlso oItem.Count > 0 Then
                oZSDINT006 = oItem.ToList
            End If
        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
        Return oZSDINT006
    End Function

    ''' <summary>
    ''' Create File Reconcile
    ''' </summary>
    ''' <returns>Boolean True:Success, False:Error</returns>
    Private Function C_SETbCreateFile006(ByVal poZSDINT006 As List(Of cZSDINT006)) As Boolean
        Dim bCrtXml As Boolean = True
        Dim tData As String = ""
        Try
            Dim tPath As String = ""
            Dim atPath As String() = tFullPath.Replace("/", "\").Split("\")
            For nPath = 0 To atPath.Length - 2
                tPath &= atPath(nPath) & "\"
            Next
            tPath = Left(tPath, tPath.Length - 1)
            If Not IO.Directory.Exists(tPath) Then
                IO.Directory.CreateDirectory(tPath)
            End If

            'ถ้าไม่มีไฟล์ให้สร้างขึ้นใหม่ '*CH 17-10-2014
            If Not File.Exists(tFullPath) Then
                Using oStreamWriter As New StreamWriter(tFullPath, False, New System.Text.UTF8Encoding(False))
                    oStreamWriter.Close()
                End Using
            End If

            Using oStreamWriter As New StreamWriter(tFullPath, True, New System.Text.UTF8Encoding(False))
                With oStreamWriter
                    For Each oZSDINT006 In poZSDINT006
                        tData = oZSDINT006.MKPF_BUDAT & tSpr & oZSDINT006.T001W_WERKS & tSpr & oZSDINT006.BKPF_XBLNR & tSpr & oZSDINT006.TICKETNO & tSpr & oZSDINT006.PFILE & tSpr
                        tData &= oZSDINT006.MARA_MATNR & tSpr & oZSDINT006.T023D_MATKL & tSpr & oZSDINT006.VBRP_FKLMG & tSpr & oZSDINT006.VBRP_NETWR & vbCrLf
                        .WriteLine(tData)
                        .Flush()
                    Next
                    .Close()
                End With
            End Using
        Catch ex As Exception
            bCrtXml = False
            Throw New Exception(ex.Message)
        Finally
        End Try
        Return bCrtXml
    End Function

    ''' <summary>
    ''' Send file Reconcile to to FTP
    ''' </summary>
    Public Function C_PRCbSendFileReconcile2FTP()
        Dim bStaSend As Boolean = True
        Try
            If File.Exists(tFullPath) Then
                bStaSend = cCNSP.SP_PRCbSendFile2FTP(tFullPath)
            End If
        Catch ex As Exception
            bStaSend = False
            Throw New Exception(ex.Message)
        Finally
        End Try
        Return bStaSend
    End Function

    ''' <summary>
    ''' Save Log Create File And Send File
    ''' </summary>
    Public Sub C_SAVxLogReconcile(ByVal poItem As List(Of cExportTemplate.cItem), ByVal poZSDINT006 As List(Of cZSDINT006), ByVal pdNow As DateTime, ByVal pbStaPrc As Boolean)
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbTbl As New DataTable
        Dim tCmdWhe As String = ""
        Dim tPath As String = ""
        'Dim tLogName As String = ""
        Dim tStaSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaSendTH, tC_StaSendEN)
        Dim tStaUnSend As String = IIf(AdaConfig.cConfig.oApplication.nLanguage = 1, tC_StaUnSendTH, tC_StaUnSendEN)
        Dim tFmtLineHead As String = "[Export Date] {0}" & vbTab & "[Export Time] {1}" & vbTab & "[Export By] {2}"
        Dim tFmtLineColumn As String = "[Branch Code]" & vbTab & "[DocNo.]" & vbTab & "[Product Code]" & vbTab & "[Export File Name]" & vbTab & "[Status Send]"
        Dim tFmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}" & vbTab & "{4}"
        Dim tFmtLine As String = "====================================================================================="
        Try
            tPath = Path.GetDirectoryName(tPathFileLog)
            If Not IO.Directory.Exists(tPath) Then
                IO.Directory.CreateDirectory(tPath)
            End If

            'Create Log File
            If File.Exists(tPathFileLog) = False Then
                Using oStreamWriter As StreamWriter = File.CreateText(tPathFileLog)
                    oStreamWriter.Close()
                End Using
            End If

            'Export Sale
            If poItem.Count > 0 Then
                oSql.AppendLine("SELECT FTBchCode,FTShdDocNo,FTPdtCode,FTExpFileName,")
                oSql.AppendLine("   CASE WHEN FTExpStaSend = '1' THEN '" & tStaSend & "' ELSE '" & tStaUnSend & "' END FTExpStaSend")
                oSql.AppendLine("FROM TLGTZSDINT005")
                oSql.AppendLine("WHERE FTShdDocNo IN ({0})")
                For Each oItem In poItem
                    tCmdWhe &= "'" & oItem.tItem & "',"
                Next
                tCmdWhe = Left(tCmdWhe, tCmdWhe.Length - 1)
                oDbTbl = oDatabase.C_CALoExecuteReader(String.Format(oSql.ToString, tCmdWhe))

                If oDbTbl.Rows.Count > 0 Then
                    'Create File Log
                    'tLogName = String.Format(cLogTemplate.tC_FileMaster, tC_LogName)
                    Using oStreamWriter As StreamWriter = File.AppendText(tPathFileLog)
                        With oStreamWriter
                            .WriteLine(tFmtLine)
                            .WriteLine(String.Format(tFmtLineHead, pdNow.ToString("dd/MM/yyyy"), pdNow.ToString("HH:mm:ss"), My.Computer.Name))
                            .WriteLine(tFmtLine)
                            .WriteLine(tFmtLineColumn)
                            .Flush()
                            For nRow = 0 To oDbTbl.Rows.Count - 1
                                .WriteLine(String.Format(tFmtLineData, oDbTbl.Rows(nRow)("FTBchCode"), oDbTbl.Rows(nRow)("FTShdDocNo"), oDbTbl.Rows(nRow)("FTPdtCode"),
                                                         oDbTbl.Rows(nRow)("FTExpFileName"), oDbTbl.Rows(nRow)("FTExpStaSend")))
                                .Flush()
                            Next
                            .WriteLine("")
                            .Close()
                        End With
                    End Using
                End If
            End If

            'Export Reconcile
            Dim tStatus As String = ""
            If poZSDINT006.Count > 0 Then
                If pbStaPrc Then
                    tStatus = tStaSend
                Else
                    tStatus = tStaUnSend
                End If
                Using oStreamWriter As StreamWriter = File.AppendText(tPathFileLog)
                    With oStreamWriter
                        tFmtLineColumn = "[DocNo.]" & vbTab & "[TaxInv]" & vbTab & "[Product Code]"
                        tFmtLineData = "{0}" & vbTab & "{1}" & vbTab & "{2}"
                        .WriteLine(tFmtLine)
                        .WriteLine(String.Format(tFmtLineHead, pdNow.ToString("dd/MM/yyyy"), pdNow.ToString("HH:mm:ss"), My.Computer.Name))
                        .WriteLine(tFmtLine)
                        .WriteLine(tFmtLineColumn)
                        .WriteLine(Path.GetFileName(tFullPath) & vbTab & tStatus)
                        .Flush()
                        For Each oItem In poZSDINT006
                            .WriteLine(String.Format(tFmtLineData, oItem.MKPF_BUDAT, oItem.BKPF_XBLNR, oItem.MARA_MATNR))
                            .Flush()
                        Next
                        .WriteLine("")
                        .Close()
                    End With
                End Using
                oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 2, Path.GetFileName(tFullPath), tStatus, tPathFileLog))
            Else
                Using oStreamWriter As StreamWriter = File.AppendText(tPathFileLog)
                    With oStreamWriter
                        .WriteLine(tFmtLine)
                        .WriteLine(String.Format(tFmtLineHead, pdNow.ToString("dd/MM/yyyy"), pdNow.ToString("HH:mm:ss"), My.Computer.Name))
                        .WriteLine(tFmtLine)
                        .WriteLine("ZSDINT006 : " & cCNMS.tMS_CN703.Split(";")(AdaConfig.cConfig.oApplication.nLanguage - 1))
                        .WriteLine("")
                        .Close()
                    End With
                End Using
                oDatabase.C_CALnExecuteNonQuery(String.Format(cApp.tSQLCmdLog, 2, Path.GetFileName(tFullPath), tStaUnSend, tPathFileLog))
            End If

        Catch ex As Exception
        Finally
            oSql = Nothing
            oDatabase = Nothing
            oDbTbl = Nothing
        End Try
    End Sub

#End Region
End Class
