﻿Imports System.IO
Imports System.Text
Public Class cExpTransaction
    'Create TempTable
    Private oC_DbTblTemp As New DataTable
    Private tC_PmtRmk As String = ""
    Private tC_TypePmt As String = ""
    Public Shared tC_ShareTranDate As String
    Public Shared tC_WrieLog As Integer

    ''' <summary>
    ''' Select Data Export And Insert To TempTable
    ''' </summary>
    ''' <param name="PtTranDate">Date Export</param>
    ''' <returns></returns>
    Public Function C_SETbExpTran(ByVal PtTranDate As String) As Boolean
        Dim bResult As Boolean = True
        Dim oDbTblGReceive As New DataTable
        Dim oDbTblGReturn As New DataTable
        Dim oDbtblSaleAndReturn As New DataTable
        Dim oDbtblPromotions As New DataTable
        Dim oSQL As New StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim oDbtblCheckProm As New DataTable
        Dim tPath As String = ""
        Dim tDir = ""
        'get ค่าที่ User Setting  มา
        Dim tPlant As String = AdaConfig.cConfig.oUrsDefault.tPlan
        Dim tStorage As String = AdaConfig.cConfig.oUrsDefault.tStorage
        Dim tRPlant As String = AdaConfig.cConfig.oUrsDefault.tRecieve


        'แปลงวันที่ จาก String 

        Try
            oC_DbTblTemp = C_SEToColumnTran()
            'GroupA = ใบรับเข้า
            oSQL.Clear()
            oSQL.AppendLine(" SELECT")
            oSQL.AppendLine(" '01' AS Record_Id,'" & tPlant & "' AS PLANT,'" & tStorage & "' AS STGE_LOC,")
            oSQL.AppendLine(" DT.FTPthDocNo,DT.FNPtdSeqNo, (CONVERT(VARCHAR,DT.FDPthDocDate,112)) AS FDPthDocDate, ")
            oSQL.AppendLine(" REPLACE(CONVERT(VARCHAR,HD.FTPthDocTime, 108),':','') AS FTPthDocTime, DT.FTPdtCode,")
            oSQL.AppendLine(" CASE DT.FTPthDocType WHEN '1' THEN 101 END AS FTPthDocType, DT.FCPtdQtyAll")
            oSQL.AppendLine(" FROM")
            oSQL.AppendLine(" dbo.TCNTPdtTnfHD AS HD ")
            oSQL.AppendLine(" INNER JOIN")
            oSQL.AppendLine(" dbo.TCNTPdtTnfDT AS DT ON HD.FTPthDocNo = DT.FTPthDocNo")
            oSQL.AppendLine(" WHERE (DT.FTPthDocType = N'1')  AND (CONVERT(VARCHAR,DT.FDPthDocDate,103) = '" & PtTranDate & "' )")
            oSQL.AppendLine(" AND (HD.FTPthStaDoc = N'1') ")

            oDbTblGReceive = oDatabase.C_CALoExecuteReader(oSQL.ToString)

            If oDbTblGReceive.Rows.Count > 0 Then
                For nRow As Integer = 0 To oDbTblGReceive.Rows.Count - 1
                    Dim oRow As DataRow = oC_DbTblTemp.NewRow()

                    Dim tPtdSeqNo As String = Right("00000" + oDbTblGReceive.Rows(nRow)("FNPtdSeqNo").ToString, 5)
                    oRow("Record_ID") = oDbTblGReceive.Rows(nRow)("Record_Id").ToString
                    oRow("TSM_Doc") = oDbTblGReceive.Rows(nRow)("FTPthDocNo").ToString
                    oRow("TSM_Item") = tPtdSeqNo
                    oRow("PSTNG_DATE") = oDbTblGReceive.Rows(nRow)("FDPthDocDate").ToString
                    oRow("PSTNG_TIME") = oDbTblGReceive.Rows(nRow)("FTPthDocTime").ToString
                    oRow("REF_DOC_NO") = ""
                    oRow("GR_GI_SLIP_NO") = ""
                    oRow("MATERIAL") = oDbTblGReceive.Rows(nRow)("FTPdtCode").ToString
                    oRow("PLANT") = oDbTblGReceive.Rows(nRow)("PLANT").ToString
                    oRow("STGE_LOC") = oDbTblGReceive.Rows(nRow)("STGE_LOC").ToString
                    oRow("CUST_VEN") = ""
                    oRow("BATCH") = ""
                    oRow("MOVE_TYPE") = oDbTblGReceive.Rows(nRow)("FTPthDocType").ToString
                    oRow("ENTRY_QNT") = oDbTblGReceive.Rows(nRow)("FCPtdQtyAll").ToString
                    oRow("PO_NUMBER") = oDbTblGReceive.Rows(nRow)("FTPthDocNo").ToString
                    oRow("PO_ITEM") = tPtdSeqNo
                    oRow("ITEM_TEXT") = oDbTblGReceive.Rows(nRow)("FTPthDocNo").ToString + "-" + tPtdSeqNo
                    oRow("GR_RCPT") = ""
                    oRow("UNLOAD_PT") = ""
                    oRow("MOVE_PLANT") = ""
                    oRow("MOVE_STLOC") = ""
                    oRow("MOVE_BATCH") = ""
                    oRow("MOVE_REAS") = ""

                    oC_DbTblTemp.Rows.Add(oRow)
                Next


            End If

            'End GroupA = ใบรับเข้า

            'GroupB = ใบเบิกออก
            oSQL.Clear()
            oSQL.AppendLine("SELECT")
            oSQL.AppendLine(" '01' AS Record_Id,'" & tPlant & "' AS PLANT,'" & tStorage & "' AS STGE_LOC,'" & tRPlant & "' AS MOVE_PLANT,")
            oSQL.AppendLine(" DT.FTPthDocNo,DT.FNPtdSeqNo,(CONVERT(VARCHAR,DT.FDPthDocDate,112)) AS FDPthDocDate, ")
            oSQL.AppendLine(" REPLACE(CONVERT(VARCHAR,HD.FTPthDocTime, 108),':','') AS FTPthDocTime, DT.FTPdtCode,")
            oSQL.AppendLine(" CASE DT.FTPthDocType WHEN '2' THEN 351 END AS FTPthDocType, DT.FCPtdQtyAll ")
            oSQL.AppendLine(" FROM ")
            oSQL.AppendLine(" dbo.TCNTPdtTnfHD AS HD INNER JOIN")
            oSQL.AppendLine(" dbo.TCNTPdtTnfDT AS DT ON HD.FTPthDocNo = DT.FTPthDocNo")
            oSQL.AppendLine(" WHERE (DT.FTPthDocType = N'2') AND (CONVERT(VARCHAR,DT.FDPthDocDate,103) ='" & PtTranDate & "')")
            oSQL.AppendLine(" AND (HD.FTPthStaDoc = N'1') AND (HD.FTTrnCode = N'SAP')  ")

            oDbTblGReturn = oDatabase.C_CALoExecuteReader(oSQL.ToString)
            If oDbTblGReturn.Rows.Count > 0 Then

                Dim tRunning As String = ""
                '  tRunning = "6600" + C_GENtRunningNum()

                For nRow As Integer = 0 To oDbTblGReturn.Rows.Count - 1

                    Dim nCount As Integer = oC_DbTblTemp.Rows.Count - 1 'สำหรับ Get ค่า row ที่ insert ไปก่อนหน้า 
                    Dim oRow As DataRow = oC_DbTblTemp.NewRow()
                    Dim tFTPthDocNo As String = oDbTblGReturn.Rows(nRow)("FTPthDocNo").ToString
                    Dim tPtdSeqNo As String = Right("00000" + oDbTblGReturn.Rows(nRow)("FNPtdSeqNo").ToString, 5)

                    oRow("Record_ID") = oDbTblGReturn.Rows(nRow)("Record_Id").ToString
                    oRow("TSM_Doc") = tFTPthDocNo
                    oRow("TSM_Item") = tPtdSeqNo
                    oRow("PSTNG_DATE") = oDbTblGReturn.Rows(nRow)("FDPthDocDate").ToString
                    oRow("PSTNG_TIME") = oDbTblGReturn.Rows(nRow)("FTPthDocTime").ToString
                    oRow("REF_DOC_NO") = ""
                    oRow("GR_GI_SLIP_NO") = ""
                    oRow("MATERIAL") = oDbTblGReturn.Rows(nRow)("FTPdtCode").ToString
                    oRow("PLANT") = oDbTblGReturn.Rows(nRow)("PLANT").ToString
                    oRow("STGE_LOC") = oDbTblGReturn.Rows(nRow)("STGE_LOC").ToString
                    oRow("CUST_VEN") = ""
                    oRow("BATCH") = ""
                    oRow("MOVE_TYPE") = oDbTblGReturn.Rows(nRow)("FTPthDocType").ToString
                    oRow("ENTRY_QNT") = oDbTblGReturn.Rows(nRow)("FCPtdQtyAll").ToString
                    oRow("PO_NUMBER") = "6600" + C_GENtRunningNum(tFTPthDocNo)

                    'Check ข้อมูล ว่า FTPthDocNo มีค่า = ค่าที่ Insert ก่อนหน้าหรือไม่ ถ้าใช่ ให้ใช้  Running Number ตัวเดิม

                    'If oC_DbTblTemp.Rows.Count = 0 Then
                    '    '    oRow("PO_NUMBER") = "6600" + C_GENtRunningNum()
                    '    tRunning = "6600" + C_GENtRunningNum()
                    '    oRow("PO_NUMBER") = tRunning
                    'Else
                    '    If tFTPthDocNo = oC_DbTblTemp.Rows(nCount).Item("TSM_Doc") Then
                    '        oRow("PO_NUMBER") = tRunning
                    '    Else
                    '        oRow("PO_NUMBER") = "6600" + C_GENtRunningNum()
                    '        tRunning = oRow("PO_NUMBER")
                    '    End If

                    'End If


                    oRow("PO_ITEM") = tPtdSeqNo
                    oRow("ITEM_TEXT") = oDbTblGReturn.Rows(nRow)("FTPthDocNo").ToString + "-" + tPtdSeqNo
                    oRow("GR_RCPT") = ""
                    oRow("UNLOAD_PT") = ""
                    oRow("MOVE_PLANT") = oDbTblGReturn.Rows(nRow)("MOVE_PLANT").ToString
                    oRow("MOVE_STLOC") = ""
                    oRow("MOVE_BATCH") = ""
                    oRow("MOVE_REAS") = ""
                    oC_DbTblTemp.Rows.Add(oRow)
                Next
            End If

            'END GroupB = ใบเบิกออก

            'Group D ข้อมูลขาย/คืน
            oSQL.Clear()
            oSQL.AppendLine("SELECT '01' AS Record_Id ,'" & tPlant & "' AS PLANT,'" & tStorage & "' AS STGE_LOC,")
            oSQL.AppendLine(" SDT.FTShdDocNo,SDT.FNSdtSeqNo,CONVERT(VARCHAR,SHD.FDShdDocDate,112) AS FDShdDocDate,")
            oSQL.AppendLine(" REPLACE(CONVERT(VARCHAR,SHD.FTShdDocTime,108),':','') AS FTShdDocTime ,SDT.FTPdtCode,")
            oSQL.AppendLine(" SDT.FTShdDocType,SDT.FTSdtStaPdt,ISNULL(SDT.FTPmhCode,'') AS FTPmhCode ,SDT.FCSdtQtyAll")
            oSQL.AppendLine(" FROM dbo.TPSTSalDT AS SDT")
            oSQL.AppendLine(" INNER JOIN dbo.TPSTSalHD AS SHD")
            oSQL.AppendLine(" ON  SDT.FTShdDocNo = SHD.FTShdDocNo")
            oSQL.AppendLine(" WHERE (CONVERT(VARCHAR, SDT.FDShdDocDate, 103) = '" & PtTranDate & "') ")
            oDbtblSaleAndReturn = oDatabase.C_CALoExecuteReader(oSQL.ToString)
            If oDbtblSaleAndReturn.Rows.Count > 0 Then
                For nRow As Integer = 0 To oDbtblSaleAndReturn.Rows.Count - 1
                    Dim oRow As DataRow = oC_DbTblTemp.NewRow()

                    Dim tSdtSeqNo As String = Right("00000" + oDbtblSaleAndReturn.Rows(nRow)("FNSdtSeqNo").ToString, 5)

                    oRow("Record_ID") = oDbtblSaleAndReturn.Rows(nRow)("Record_Id").ToString
                    oRow("TSM_Doc") = oDbtblSaleAndReturn.Rows(nRow)("FTShdDocNo").ToString
                    oRow("TSM_Item") = tSdtSeqNo
                    oRow("PSTNG_DATE") = oDbtblSaleAndReturn.Rows(nRow)("FDShdDocDate").ToString
                    oRow("PSTNG_TIME") = oDbtblSaleAndReturn.Rows(nRow)("FTShdDocTime").ToString
                    oRow("REF_DOC_NO") = ""
                    oRow("GR_GI_SLIP_NO") = ""
                    oRow("MATERIAL") = oDbtblSaleAndReturn.Rows(nRow)("FTPdtCode").ToString
                    oRow("PLANT") = oDbtblSaleAndReturn.Rows(nRow)("PLANT").ToString
                    oRow("STGE_LOC") = oDbtblSaleAndReturn.Rows(nRow)("STGE_LOC").ToString
                    oRow("CUST_VEN") = ""
                    oRow("BATCH") = ""

                    Dim tDoctype As String = oDbtblSaleAndReturn.Rows(nRow)("FTShdDocType").ToString
                    Dim tStapdt As String = oDbtblSaleAndReturn.Rows(nRow)("FTSdtStaPdt").ToString



                    oRow("ENTRY_QNT") = oDbtblSaleAndReturn.Rows(nRow)("FCSdtQtyAll").ToString
                    oRow("PO_NUMBER") = ""
                    oRow("PO_ITEM") = ""
                    oRow("ITEM_TEXT") = oDbtblSaleAndReturn.Rows(nRow)("FTShdDocNo").ToString + "-" + tSdtSeqNo
                    oRow("GR_RCPT") = ""


                    If tDoctype = 1 And tStapdt = 1 Then
                        'check ว่ามี Promotion ไหม
                        If oDbtblSaleAndReturn.Rows(nRow)("FTPmhCode").ToString <> "" Then

                            Dim tDocNo As String = oDbtblSaleAndReturn.Rows(nRow)("FTShdDocNo").ToString
                            Dim tSeqNo As String = oDbtblSaleAndReturn.Rows(nRow)("FNSdtSeqNo").ToString
                            Dim tPmtCode As String = oDbtblSaleAndReturn.Rows(nRow)("FTPmhCode").ToString

                            C_GETxPromotion(tDocNo, tSeqNo, tPmtCode, "Sale")
                            oRow("UNLOAD_PT") = tC_PmtRmk
                            oRow("MOVE_TYPE") = tC_TypePmt

                        Else

                            oRow("MOVE_TYPE") = "951"

                        End If

                    ElseIf tDoctype = 9 And tStapdt = 2 Then

                        If oDbtblSaleAndReturn.Rows(nRow)("FTPmhCode").ToString <> "" Then

                            Dim tDocNo As String = oDbtblSaleAndReturn.Rows(nRow)("FTShdDocNo").ToString
                            Dim tSeqNo As String = oDbtblSaleAndReturn.Rows(nRow)("FNSdtSeqNo").ToString
                            Dim tPmtCode As String = oDbtblSaleAndReturn.Rows(nRow)("FTPmhCode").ToString

                            C_GETxPromotion(tDocNo, tSeqNo, tPmtCode, "ReturnR")

                            oRow("UNLOAD_PT") = tC_PmtRmk
                            oRow("MOVE_TYPE") = tC_TypePmt
                        Else

                            oRow("MOVE_TYPE") = "953"

                        End If


                    ElseIf tDoctype = 1 And tStapdt = 3 Then
                        oRow("MOVE_TYPE") = "991"

                    ElseIf tDoctype = 9 And tStapdt = 3 Then

                        If oDbtblSaleAndReturn.Rows(nRow)("FTPmhCode").ToString <> "" Then

                            Dim tDocNo As String = oDbtblSaleAndReturn.Rows(nRow)("FTShdDocNo").ToString
                            Dim tSeqNo As String = oDbtblSaleAndReturn.Rows(nRow)("FNSdtSeqNo").ToString
                            Dim tPmtCode As String = oDbtblSaleAndReturn.Rows(nRow)("FTPmhCode").ToString

                            C_GETxPromotion(tDocNo, tSeqNo, tPmtCode, "Return")

                            oRow("UNLOAD_PT") = tC_PmtRmk
                            oRow("MOVE_TYPE") = tC_TypePmt
                        Else

                            oRow("MOVE_TYPE") = "995"

                        End If

                    Else
                        oRow("MOVE_TYPE") = ""
                    End If


                    oRow("MOVE_PLANT") = ""
                    oRow("MOVE_STLOC") = ""
                    oRow("MOVE_BATCH") = ""
                    oRow("MOVE_REAS") = ""
                    oC_DbTblTemp.Rows.Add(oRow)
                Next
            End If

            'END Group D ข้อมูลขาย/คืน
        Catch ex As Exception
            C_CALxWriteLogEr(ex.Message, tPath, oC_DbTblTemp.Rows.Count, "Expression")
            Return False
        End Try
        Return bResult
    End Function


    ''' <summary>
    ''' หาสินค้าที่เป็นของแถมจาก โปรโมชั่น
    ''' </summary>
    ''' <param name="ptDocNo"> เลขเอกสาร </param>
    ''' <param name="ptSqeNo"> ลำดับ </param>
    ''' <param name="ptPmtCode"> โปรโมชั่นโค้ด </param>
    ''' <param name="ptType"> ประเภท ขาย / คืน</param>
    Public Sub C_GETxPromotion(ByVal ptDocNo As String, ByVal ptSqeNo As String, ByVal ptPmtCode As String, ByVal ptType As String)
        Try
            Dim oDbtblCheckProm As New DataTable
            Dim oDbtblPromotions As New DataTable
            Dim odatabase As New cDatabaseLocal
            Dim oSql As New StringBuilder
            tC_PmtRmk = ""
            tC_TypePmt = ""

            '=== นำรหัส Promotion ไป หาค่า Unloading Point
            oSql.Clear()
            oSql.AppendLine(" SELECT (FCSdtSetPrice - FCSdtDisAvg) AS AmtPromotion FROM TPSTSalPD ")
            oSql.AppendLine("   WHERE (FTShdDocNo = '" & ptDocNo & "' )  ")
            oSql.AppendLine(" AND FNSdtSeqNo = '" & ptSqeNo & "' ")
            oDbtblCheckProm = odatabase.C_CALoExecuteReader(oSql.ToString)

            If oDbtblCheckProm.Rows.Count > 0 Then
                If oDbtblCheckProm.Rows(0)("AmtPromotion") = "0" Then
                    oSql.Clear()
                    oSql.AppendLine(" SELECT PMH.FTPmhRmk")
                    oSql.AppendLine(" FROM  dbo.TPSTSalDT AS PMC INNER JOIN dbo.TCNTPmtHD AS PMH")
                    oSql.AppendLine(" ON PMC.FTPmhCode = PMH.FTPmhCode")
                    oSql.AppendLine(" WHERE PMC.FTPmhCode = '" & ptPmtCode & "' ")
                    oDbtblPromotions = odatabase.C_CALoExecuteReader(oSql.ToString)

                    If oDbtblPromotions.Rows.Count > 0 Then
                        tC_PmtRmk = oDbtblPromotions.Rows(0)("FTPmhRmk").ToString()
                    End If

                    Select Case ptType
                        Case "Sale"
                            tC_TypePmt = "991"
                        Case "Return"
                            tC_TypePmt = "995"
                        Case "ReturnR"
                            tC_TypePmt = "995"
                    End Select

                Else

                    Select Case ptType
                        Case "Sale"
                            tC_TypePmt = "951"
                        Case "Return"
                            tC_TypePmt = "995"
                        Case "ReturnR"
                            tC_TypePmt = "953"
                    End Select

                End If

            End If

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Create Column TempTable.
    ''' </summary>
    ''' <returns></returns>
    Public Function C_SEToColumnTran() As DataTable
        Dim oDbTblCol As New DataTable
        Try
            oDbTblCol.Columns.Add("Record_ID")
            oDbTblCol.Columns.Add("TSM_Doc")
            oDbTblCol.Columns.Add("TSM_Item")
            oDbTblCol.Columns.Add("PSTNG_DATE")
            oDbTblCol.Columns.Add("PSTNG_TIME")
            oDbTblCol.Columns.Add("REF_DOC_NO")
            oDbTblCol.Columns.Add("GR_GI_SLIP_NO")
            oDbTblCol.Columns.Add("MATERIAL")
            oDbTblCol.Columns.Add("PLANT")
            oDbTblCol.Columns.Add("STGE_LOC")
            oDbTblCol.Columns.Add("CUST_VEN")
            oDbTblCol.Columns.Add("BATCH")
            oDbTblCol.Columns.Add("MOVE_TYPE")
            oDbTblCol.Columns.Add("ENTRY_QNT")
            oDbTblCol.Columns.Add("PO_NUMBER")
            oDbTblCol.Columns.Add("PO_ITEM")
            oDbTblCol.Columns.Add("ITEM_TEXT")
            oDbTblCol.Columns.Add("GR_RCPT")
            oDbTblCol.Columns.Add("UNLOAD_PT")
            oDbTblCol.Columns.Add("MOVE_PLANT")
            oDbTblCol.Columns.Add("MOVE_STLOC")
            oDbTblCol.Columns.Add("MOVE_BATCH")
            oDbTblCol.Columns.Add("MOVE_REAS")

            Return oDbTblCol

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    ''' <summary>
    ''' ต่อ String ที่รับค่ามาจาก TempTable
    ''' </summary>
    ''' <param name="ptTranDate">Date Export</param>
    Public Sub C_GETbExpTranToFile(ByVal ptTranDate As String)
        Dim tTrantext As String = ""
        Dim tDir As String = ""
        Dim tPathfile As String = ""
        Dim tCountTemp As String = ""
        Dim tPathfileSr As String = ""
        Dim tShopCode As String = AdaConfig.cConfig.oUrsDefault.tShopCodeTran

        Try
            Dim tConFormat As String = DateTime.ParseExact(ptTranDate, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            'Dim tDate As String = Convert.ToDateTime(tConFormat).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))
            ' tC_ShareTranDate = Convert.ToDateTime(tConFormat).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))

            tCountTemp = oC_DbTblTemp.Rows.Count
            '   tTrantext += "00|SAP|TSM|BFC001|" & Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US")) + vbCr + vbLf
            tTrantext += "00|TSM|SAP|" & tShopCode & "|" & tC_ShareTranDate + vbCr + vbLf

            For Each nRow As DataRow In oC_DbTblTemp.Rows
                For Each nCol As DataColumn In oC_DbTblTemp.Columns
                    tTrantext += nRow(nCol.ColumnName).ToString & ("|")
                Next
                tTrantext += vbCr + vbLf
            Next

            IIf(tCountTemp <> "", tCountTemp, 0)

            tTrantext += "99|" + CType(tCountTemp, String)

            '   tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + Now.ToString("yyyyMMdd")

            tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tC_ShareTranDate

            'tPathfile = tDir + "/" + "BFC_" + Now.ToString("yyyyMMdd") + "_0001_INVENTORY.txt"
            'tPathfileSr = tDir + "/" + "BFC_" + Now.ToString("yyyyMMdd") + "_0001_SERIAL.txt"

            tPathfile = tDir + "/" + "BFC_" + tC_ShareTranDate + "_" & tShopCode & "_INVENTORY.txt"
            tPathfileSr = tDir + "/" + "BFC_" + tC_ShareTranDate + "_" & tShopCode & "_SERIAL.txt"


            'Inventory
            If (Not File.Exists(tPathfile)) Then
                C_SETxWriteExpTran(tTrantext, tDir, tC_ShareTranDate)
            Else
                tC_WrieLog += 1
                C_CALxWriteLogEr("Export ข้อมูลเกิน 1 ครั้ง", tPathfile, "0", "Expression")
            End If

            'Serial
            If (Not File.Exists(tPathfileSr)) Then
                C_GETbExpTranToFileSr(tC_ShareTranDate)
            End If

        Catch ex As Exception
            C_CALxWriteLogEr(ex.Message, tPathfile, oC_DbTblTemp.Rows.Count, "Expression")
        End Try
    End Sub

    ''' <summary>
    ''' Gen Serial Textfile
    ''' </summary>
    Private Sub C_GETbExpTranToFileSr(ByVal ptdate As String)
        Dim tTrantext As String = ""
        Dim tPathSr As String = ""
        Dim tDir As String = ""
        Dim oWriteSr As StreamWriter
        Dim tShopCode As String = AdaConfig.cConfig.oUrsDefault.tShopCodeTran
        Try
            '  tTrantext += "00|SAP|TSM|BFC001|" & Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US")) + vbCr + vbLf
            tTrantext += "00|TSM|SAP|" & tShopCode & "|" & ptdate + vbCr + vbLf
            tTrantext += "99|" + CType(0, String)

            '   tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + Now.ToString("yyyyMMdd")
            '  tPathSr = tDir + "/" + "BFC_" + Now.ToString("yyyyMMdd") + "_0001_SERIAL.txt"

            tDir = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + ptdate
            tPathSr = tDir + "/" + "BFC_" + ptdate + "_" & tShopCode & "_SERIAL.txt"

            If (Not File.Exists(tPathSr)) Then
                File.Create(tPathSr).Dispose()
            End If

            oWriteSr = New StreamWriter(tPathSr, True)
            oWriteSr.Write(tTrantext)
            oWriteSr.Close()


        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Write Textfile
    ''' </summary>
    ''' <param name="ptMsg">Data Write To Textfile</param>
    ''' <param name="ptDir">Path File</param>
    Private Sub C_SETxWriteExpTran(ByVal ptMsg As String, ByVal ptDir As String, ByVal ptdate As String)
        Dim tPathFile As String = ""
        Dim tPathFileSr As String = ""
        Dim oWrite As StreamWriter
        Dim tShopCode As String = AdaConfig.cConfig.oUrsDefault.tShopCodeTran


        Try
            ' tPathFile = ptDir + "/" + "BFC_" + Now.ToString("yyyyMMdd") + "_0001_INVENTORY.txt"
            tPathFile = ptDir + "/" + "BFC_" + ptdate + "_" & tShopCode & "_INVENTORY.txt"

            If (Not System.IO.Directory.Exists(ptDir)) Then
                System.IO.Directory.CreateDirectory(ptDir)
            End If

            ' สร้าง File ถ้ายังไม่ได้สร้าง
            If (Not File.Exists(tPathFile)) Then
                File.Create(tPathFile).Dispose()
            End If

            oWrite = New StreamWriter(tPathFile, True)
            oWrite.Write(ptMsg)
            oWrite.Close()

            C_CALxWriteLogEr("", tPathFile, oC_DbTblTemp.Rows.Count, "Success")
        Catch ex As Exception
            C_CALxWriteLogEr(ex.Message, tPathFile, oC_DbTblTemp.Rows.Count, "Expression")
        End Try
    End Sub

    ''' <summary>
    ''' Gennerate RunningNumber
    ''' </summary>
    ''' <returns></returns>
    Public Function C_GENtRunningNum(ByVal ptDocNo As String) As String

        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oDbTblRunning As New DataTable
            Dim oDbTblRGet As New DataTable
            Dim oCmd As New StringBuilder
            Dim tRunning As String = ""
            Dim tLastrunning As String = ""
            Dim tReSult As String = ""
            Dim nCount As Integer = 0

            oCmd.Clear()
            oCmd.AppendLine(" SELECT  FNNum, FTRunningNumber,FTDocNo")
            oCmd.AppendLine(" FROM TCNTPdtRunning")
            oCmd.AppendLine(" WHERE FTDocNo = '" & ptDocNo & "' ")
            oDbTblRunning = oDatabase.C_CALoExecuteReader(oCmd.ToString)

            If oDbTblRunning.Rows.Count > 0 Then
                tReSult = oDbTblRunning.Rows(0).Item("FTRunningNumber")
            Else

                oCmd.Clear()
                oCmd.AppendLine(" SELECT  FNNum, FTRunningNumber,FTDocNo")
                oCmd.AppendLine(" FROM TCNTPdtRunning")
                oDbTblRGet = oDatabase.C_CALoExecuteReader(oCmd.ToString)

                nCount = oDbTblRGet.Rows.Count
                If nCount = 0 Then
                    tReSult = "000001"
                    C_INSbRunningNumber(tReSult, ptDocNo)
                Else

                    tLastrunning = oDbTblRGet.Rows(nCount - 1)("FTRunningNumber")
                    tRunning = tLastrunning + 1
                    tReSult = Right("000000" + tRunning, 6)
                    C_INSbRunningNumber(tReSult, ptDocNo)
                End If



            End If


            ' End If

            Return tReSult

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Update RunningNumber
    ''' </summary>
    ''' <param name="ptRuningNum">Running Number New for update </param>
    ''' <returns></returns>
    Public Function C_INSbRunningNumber(ByVal ptRuningNum As String, ByVal ptDocNo As String) As Boolean
        Try
            Dim oIns As New StringBuilder
            Dim oDatabase As New cDatabaseLocal

            oIns.Clear()
            '  If ptType = "Insert" Then
            oIns.AppendLine(" Insert Into ")
            oIns.AppendLine(" TCNTPdtRunning (FTRunningNumber,FTDocNo)")
            oIns.AppendLine(" VALUES ('" & ptRuningNum & "','" & ptDocNo & "')")

            'ElseIf ptType = "Update"
            '    oIns.AppendLine(" UPDATE ")
            '    oIns.AppendLine(" TCNTPdtRunning SET FTRunningNumber = '" & ptRuningNum & "' ")
            '    oIns.AppendLine(" WHERE FTRunningNumber = '" & ptOldRunning & "' ")
            'End If


            If oDatabase.C_CALnExecuteNonQuery(oIns.ToString) Then
                Return True
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Sub C_CALxWriteLogEr(ByVal ptMsg As String, ByVal ptFullPath As String,
                                ByVal ptCountRow As String, ByVal ptType As String)

        Dim tC_FileLog As String = AdaConfig.cConfig.tAppPath & "\Log"
        Dim dDateNow As Date = Date.Now
        Dim oHDfile As New StringBuilder
        Dim tDesc As String = ""
        Dim oSQL As New StringBuilder
        Dim tDateTimeNow As String = ""
        Dim oDatabase As New cDatabaseLocal
        Dim tFileName As String = ""

        Try
            If Not IO.Directory.Exists(tC_FileLog) Then
                IO.Directory.CreateDirectory(tC_FileLog)
            End If

            If ptType = "Expression" Then

                tC_FileLog &= "\" & "Log_Stock_Transactions" & Format(Date.Now, "yyyyMMdd-HHmmss") & ".txt"

                If File.Exists(tC_FileLog) = False Then
                    Using oSw As StreamWriter = File.CreateText(tC_FileLog)
                        oSw.Close()
                    End Using

                    oHDfile.Append("[Export Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Export Time] " + Format(Date.Now, "HH: mm:ss") + vbTab + "")
                    oHDfile.Append("[Export By] " + Environment.MachineName + Environment.NewLine + Environment.NewLine + "")
                    oHDfile.Append("[Success]" + vbTab + "0" + Environment.NewLine + "")
                    oHDfile.Append("[Description]" + vbTab + ptMsg)

                    Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                        With oStreamWriter
                            .WriteLine(oHDfile)
                            .Flush()
                            .Close()
                        End With
                    End Using

                End If

                tDesc = "Success(" & ptCountRow & "),Failed(1)"
                tDateTimeNow = Format(Date.Now, "yyyy-MM-dd HH:mm:ss.fff")
                tFileName = Path.GetFileName(ptFullPath)
                oSQL.Clear()
                oSQL.AppendLine("INSERT INTO TLNKLog(uid,FDDateTime,FNType,FTFileName,FTDescription,FTRefer) VALUES")
                oSQL.AppendLine("('" + Guid.NewGuid.ToString + "','" + tDateTimeNow + "',2,'" + tFileName + "','" + tDesc + "','" + tC_FileLog + "')")

                If oSQL.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
                End If

            ElseIf ptType = "Success"

                tC_FileLog &= "\" & "Log_Stock_Transactions" & Format(Date.Now, "yyyyMMdd-HHmmss") & ".txt"

                If File.Exists(tC_FileLog) = False Then
                    Using oSw As StreamWriter = File.CreateText(tC_FileLog)
                        oSw.Close()
                    End Using

                    oHDfile.Append("[Export Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Import Time] " + Format(Date.Now, "HH: mm:ss") + vbTab + "")
                    oHDfile.Append("[Export By] " + Environment.MachineName + Environment.NewLine + Environment.NewLine + "")
                    oHDfile.Append("[Success]" + vbTab + ptCountRow + Environment.NewLine + "")
                    oHDfile.Append("[Description]" + vbTab + ptMsg)

                    Using oStreamWriter As StreamWriter = File.AppendText(tC_FileLog)
                        With oStreamWriter
                            .WriteLine(oHDfile)
                            .Flush()
                            .Close()
                        End With
                    End Using

                End If


                tDesc = "Success(" & ptCountRow & "),Failed(0)"
                tDateTimeNow = Format(Date.Now, "yyyy-MM-dd HH:mm:ss.fff")
                tFileName = Path.GetFileName(ptFullPath)
                oSQL.Clear()
                oSQL.AppendLine("INSERT INTO TLNKLog(uid,FDDateTime,FNType,FTFileName,FTDescription,FTRefer) VALUES")
                oSQL.AppendLine("('" + Guid.NewGuid.ToString + "','" + tDateTimeNow + "',2,'" + tFileName + "','" + tDesc + "','" + tC_FileLog + "')")

                If oSQL.ToString <> "" Then
                    oDatabase.C_CALnExecuteNonQuery(oSQL.ToString.Replace("'NULL'", "NULL"))
                End If

            End If



        Catch ex As Exception
            MessageBox.Show("C_CALxWriteLogEr " + ex.Message)
        End Try
    End Sub

End Class
