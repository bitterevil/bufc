Option Explicit On
Imports System.IO
Imports System.Data.SqlClient

Public Class wMain

#Region "event"

#Region "button"
    Private Sub omnFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles omnFileExit.Click, otoExit.Click
        Me.Close()
    End Sub

    Private Sub otoChgPwd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoChgPwd.Click, omnFileChgPwd.Click
        cLog.C_CALxWriteLog("wChangerPass")
        wChangePass.ShowDialog()
    End Sub

    Private Sub otoHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoHelp.Click, omnHelpHowTo.Click
        Dim newProc As Diagnostics.Process
        Dim tAppPath As String

        tAppPath = Application.StartupPath
        tAppPath &= "\AdaLinkTHSHelp.chm"

        If System.IO.File.Exists(tAppPath) = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN006 & ";" & tAppPath, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Exit Sub
        End If
        newProc = Diagnostics.Process.Start(tAppPath)
    End Sub

#End Region

#Region "form"

    Private Sub wMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If cCNSP.SP_MSGnShowing(cCNMS.tMS_CN003, cCNEN.eEN_MSGStyle.nEN_MSGYesNo) = MsgBoxResult.No Then
                cLog.C_CALxWriteLog("Exit")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub wMain_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub wMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set Culture
        Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-GB")

        Dim oDatabase As New cDatabaseLocal


        'Default Eng
        cCNVB.nVB_CutLng = 2
        cCNSP.SP_LNGxSetDefAftChg()

        Me.Text &= " - [" & My.Application.Info.Version.ToString & "]"

RESTART:

        Try
            AdaConfig.cConfig.C_GETxConfigXml()
            AdaConfig.cConfig.C_GETxConfigXmlUrs()

            'AdaConfig.cConfig.C_GETxConfigUploadXml() 'Upload promotion '*CH 22-12-2014
            'Set Status Bar
            Me.ostMInfo.Text = AdaConfig.cConfig.AdaConfigXml
            Me.ostMDB.Text = AdaConfig.cConfig.oConnSource.tServer & "/" & AdaConfig.cConfig.oConnSource.tCatalog

            'Check Connection
            If AdaConfig.cConfig.C_CHKbSQLSourceConnect = True Then

                If cApp.C_CALaCheckTableLog.Count > 0 Then
                    '�Դ�������� �ʴ� Msg & �ʴ�˹�Ҩ� wSetting �ҡ��� restart
                    cCNSP.SP_MSGnShowing(cCNMS.tMS_CN110, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
                    Dim oFrmSetting As New wSetting()
                    If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        oFrmSetting.Dispose()
                        Application.Restart()
                    Else
                        End
                    End If
                    'cCNSP.SP_MSGnShowing(cCNMS.tMS_CN110, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
                    'End
                End If

                '*EYE 59-03-08
                'Dim tSql As String
                'tSql = "IF  EXISTS(select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TLNKPdt' AND COLUMN_NAME = 'FTPbnCode') "
                'tSql &= "BEGIN "
                'tSql &= "DROP INDEX TLNKPdt.IND_TLNKPdt_FTPbnCode; "
                'tSql &= "ALTER TABLE TLNKPdt DROP COLUMN FTPbnCode; "
                'tSql &= "Alter Table TLNKPdt ADD FTPbnCode VARCHAR(200); "
                'tSql &= "CREATE NONCLUSTERED INDEX IND_TLNKPdt_FTPbnCode ON TLNKPdt (FTPbnCode); "
                'tSql &= "End "
                'oDatabase.C_CALnExecuteNonQuery(tSql)
                'Not check expired date '*CH 10-11-2014
                'If cApp.C_CALxCheckExpire < 1 Then
                '    cCNSP.SP_MSGnShowing(cCNMS.tMS_CN111, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
                '    End
                'End If

                'While True
                '    '��������ѹ�ҡ���� 1 ����ͧ
                '    'http://msdn.microsoft.com/en-us/library/ms130822%28v=sql.105%29.aspx
                '    'syspro.dbid,program_name,name,hostname
                '    Dim tHostName As String = ""
                '    tSql = "SELECT hostname FROM sys.sysprocesses syspro INNER JOIN sys.sysdatabases sysdb ON syspro.dbid=sysdb.dbid WHERE name='" & AdaConfig.cConfig.oConnSource.tCatalog &
                '        "' AND program_name='" & My.Application.Info.ProductName.ToString & "' AND hostname<>HOST_NAME() ORDER BY hostname"

                '    Dim oDTHost = oDatabase.C_CALoExecuteReader(tSql)
                '    If oDTHost IsNot Nothing Then
                '        If oDTHost.Rows.Count > 0 Then
                '            tHostName = oDTHost.Rows(0)("hostname")
                '        End If
                '    End If

                '    If tHostName.Trim.Length > 0 Then
                '        If cCNSP.SP_MSGnShowing(String.Format(cCNMS.tMS_CN107, tHostName.Trim), cCNEN.eEN_MSGStyle.nEN_MSGYesNo) = Windows.Forms.DialogResult.Yes Then
                '            Continue While
                '        Else
                '            End
                '        End If
                '    Else
                '        Exit While
                '    End If
                'End While
            Else
                '�Դ�������� �ʴ� Msg & �ʴ�˹�Ҩ� wSetting �ҡ��� restart
                cCNSP.SP_MSGnShowing(cCNMS.tMS_CN004, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
                Dim oFrmSetting As New wSetting()
                If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.Yes Then
                    oFrmSetting.Dispose()
                    'Application.Restart()
                    GoTo RESTART
                Else
                    End
                End If
            End If

            'SP_ALTxTBLTemp()

            While True
                Dim oLogin As New wLogin

                If oLogin.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    cCNVB.nVB_CutLng = AdaConfig.cConfig.oApplication.nLanguage
                    cCNSP.SP_LNGxSetDefAftChg()
                    cCNSP.SP_FrmSetCapControl(Me)
                    cCNSP.SP_GETxVariable()

                    'Create Table Log
                    oDatabase.C_CALnExecuteNonQuery(cApp.tSQLCmdCheckTableTemp)

                    'Update Mapping '*CH 01-04-2015
                    oDatabase.C_CALnExecuteNonQuery(cApp.tSQLCmdUpdMapping)

                    Exit While
                Else
                    End
                End If
            End While

        Catch ex As Exception
            cCNSP.SP_MSGShowError(ex.Message)
            End
        End Try

        cLog.C_CALxWriteLog("wMain")
    End Sub

    Private Sub otoExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoExport.Click, omnExport.Click
        cLog.C_CALxWriteLog("wExports")
        'wExports.ShowDialog()
        'wExports.Dispose()
        wExports2.ShowDialog()
        wExports2.Dispose()
    End Sub

    Private Sub otoUpload_Click(sender As Object, e As EventArgs) Handles otoUpload.Click, omnUpload.Click
        cLog.C_CALxWriteLog("wUploadPmt")
        wUploadPmt.ShowDialog()
        wUploadPmt.Dispose()
    End Sub

    Private Sub otmForm_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles otmForm.Tick
        Me.ostDate.Text = ": " & Format(Date.Now, "dd/MM/yyyy")
        Me.ostTime.Text = ": " & Format(Date.Now, "HH:mm:ss")
    End Sub

    Private Sub C_FRMxOpenExpReconcile(sender As Object, e As EventArgs) Handles omnExportRecon.Click
        cLog.C_CALxWriteLog("wExpReconcile")
        wExpReconcile.ShowDialog()
        wExpReconcile.Dispose()
    End Sub

    Private Sub C_FRMxOpenExpExpense(sender As Object, e As EventArgs) Handles omnExpExpense.Click
        cLog.C_CALxWriteLog("wExpExpense")
        wExpExpense.ShowDialog()
        wExpExpense.Dispose()
    End Sub

    Private Sub C_FRMxOpenExpCashTnf(sender As Object, e As EventArgs) Handles omnExpCashTnf.Click
        cLog.C_CALxWriteLog("wExpCashTnf")
        wExpCashTnf.ShowDialog()
        wExpCashTnf.Dispose()
    End Sub
#End Region

#End Region

    Private Sub otoChangeLang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoChangeLang.Click
        '����¹����
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then
            cCNVB.nVB_CutLng = 1
            Me.otoChangeLang.Image = Me.oimForm.Images(1)
        Else
            Me.otoChangeLang.Image = Me.oimForm.Images(0)
            cCNVB.nVB_CutLng = 2
        End If
        AdaConfig.cConfig.oApplication.nLanguage = cCNVB.nVB_CutLng
        AdaConfig.cConfig.C_SETbUpdateLang()
        cCNSP.SP_LNGxSetDefAftChg()
        cCNSP.SP_FrmSetCapControl(Me)
        cLog.C_CALxWriteLog("Change Language " & cCNVB.nVB_CutLng)
    End Sub

    Private Sub otoLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoLog.Click, omnToolsLog.Click
        cLog.C_CALxWriteLog("wLog")
        wLog.ShowDialog()
        wLog.Dispose()
    End Sub

    Private Sub omnHelpAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles omnHelpAbout.Click
        cLog.C_CALxWriteLog("wAbout")
        wAbout.ShowDialog()
        wAbout.Dispose()
    End Sub

    'Private Sub omnToolsOption_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles omnToolsOption.Click
    '    Dim oForm As New wConfig
    '    oForm.ShowDialog()
    '    oForm.Dispose()
    '    GC.Collect()
    'End Sub

    Private Sub otoImport_Click(sender As System.Object, e As System.EventArgs) Handles otoImport.Click, omnImport.Click
        cLog.C_CALxWriteLog("wImports")
        wImports.ShowDialog()
        wImports.Dispose()
    End Sub

    Private Sub omnToolsOption_Click(sender As System.Object, e As System.EventArgs) Handles omnToolsOption.Click
        Me.Cursor = Cursors.WaitCursor
        Dim oFrmSetting As New wSetting()
        Me.Cursor = Cursors.Default
        cLog.C_CALxWriteLog("wSetting")
        oFrmSetting.ShowDialog()
        If oFrmSetting.DialogResult = Windows.Forms.DialogResult.Yes Then
0:          Application.Restart()
        End If

        'Me.ostMDB.Text = ": " & AdaConfig.Config.Connection.tServer & "/" & AdaConfig.Config.Connection.tCatalog
    End Sub

    Private Sub omnToolsRebuild_Click(sender As System.Object, e As System.EventArgs) Handles omnToolsRebuild.Click
        cLog.C_CALxWriteLog("wIndex")
        wIndex.ShowDialog()
        wIndex.Dispose()
    End Sub
End Class
