<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wMain))
        Me.otoMainSep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.otoImport = New System.Windows.Forms.ToolStripButton()
        Me.otoMainSep2 = New System.Windows.Forms.ToolStripSeparator()
        Me.otoExport = New System.Windows.Forms.ToolStripButton()
        Me.otoForm = New System.Windows.Forms.ToolStrip()
        Me.otoChangeLang = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.otoChgPwd = New System.Windows.Forms.ToolStripButton()
        Me.otoMainSep0 = New System.Windows.Forms.ToolStripSeparator()
        Me.otoUpload = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.otoLog = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.otoHelp = New System.Windows.Forms.ToolStripButton()
        Me.otoMainSep3 = New System.Windows.Forms.ToolStripSeparator()
        Me.otoExit = New System.Windows.Forms.ToolStripButton()
        Me.omnHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ostTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ostDate = New System.Windows.Forms.ToolStripStatusLabel()
        Me.otmForm = New System.Windows.Forms.Timer(Me.components)
        Me.ottForm = New System.Windows.Forms.ToolTip(Me.components)
        Me.ostMDB = New System.Windows.Forms.ToolStripStatusLabel()
        Me.omnHelpSep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.omnPrc = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnImport = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnPrcSep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.omnExport = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnExportRecon = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnExpExpense = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnExpCashTnf = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnPrcSep2 = New System.Windows.Forms.ToolStripSeparator()
        Me.omnUpload = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnFileExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnFileChgPwd = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnFileSep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.omnForm = New System.Windows.Forms.MenuStrip()
        Me.omnTool = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnToolsLog = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnToolsRebuild = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnToolsSep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.omnToolsOption = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.omnHelpHowTo = New System.Windows.Forms.ToolStripMenuItem()
        Me.ostForm = New System.Windows.Forms.StatusStrip()
        Me.ostMInfo = New System.Windows.Forms.ToolStripStatusLabel()
        Me.oimForm = New System.Windows.Forms.ImageList(Me.components)
        Me.otoForm.SuspendLayout()
        Me.omnForm.SuspendLayout()
        Me.ostForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'otoMainSep1
        '
        Me.otoMainSep1.Name = "otoMainSep1"
        Me.otoMainSep1.Size = New System.Drawing.Size(6, 55)
        '
        'otoImport
        '
        Me.otoImport.AutoSize = False
        Me.otoImport.Image = CType(resources.GetObject("otoImport.Image"), System.Drawing.Image)
        Me.otoImport.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoImport.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoImport.Name = "otoImport"
        Me.otoImport.Size = New System.Drawing.Size(59, 52)
        Me.otoImport.Tag = "2;�����;Import"
        Me.otoImport.Text = "Import"
        Me.otoImport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.otoImport.ToolTipText = "Import Data To AdaSky"
        '
        'otoMainSep2
        '
        Me.otoMainSep2.Name = "otoMainSep2"
        Me.otoMainSep2.Size = New System.Drawing.Size(6, 55)
        Me.otoMainSep2.Visible = False
        '
        'otoExport
        '
        Me.otoExport.AutoSize = False
        Me.otoExport.Image = CType(resources.GetObject("otoExport.Image"), System.Drawing.Image)
        Me.otoExport.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoExport.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoExport.Name = "otoExport"
        Me.otoExport.Size = New System.Drawing.Size(59, 52)
        Me.otoExport.Tag = "2;���͡;Export"
        Me.otoExport.Text = "Export"
        Me.otoExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.otoExport.ToolTipText = "Export Data"
        '
        'otoForm
        '
        Me.otoForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.otoForm.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.otoChangeLang, Me.ToolStripSeparator1, Me.otoChgPwd, Me.otoMainSep0, Me.otoImport, Me.otoMainSep1, Me.otoExport, Me.otoMainSep2, Me.otoUpload, Me.ToolStripSeparator3, Me.otoLog, Me.ToolStripSeparator2, Me.otoHelp, Me.otoMainSep3, Me.otoExit})
        Me.otoForm.Location = New System.Drawing.Point(0, 24)
        Me.otoForm.Name = "otoForm"
        Me.otoForm.Size = New System.Drawing.Size(794, 55)
        Me.otoForm.TabIndex = 9
        Me.otoForm.Text = "ToolStrip"
        '
        'otoChangeLang
        '
        Me.otoChangeLang.AutoSize = False
        Me.otoChangeLang.Image = CType(resources.GetObject("otoChangeLang.Image"), System.Drawing.Image)
        Me.otoChangeLang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoChangeLang.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoChangeLang.Name = "otoChangeLang"
        Me.otoChangeLang.Size = New System.Drawing.Size(73, 52)
        Me.otoChangeLang.Tag = "2;����;Language"
        Me.otoChangeLang.Text = "Language"
        Me.otoChangeLang.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.otoChangeLang.ToolTipText = "Change Language"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 55)
        '
        'otoChgPwd
        '
        Me.otoChgPwd.AutoSize = False
        Me.otoChgPwd.Image = CType(resources.GetObject("otoChgPwd.Image"), System.Drawing.Image)
        Me.otoChgPwd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoChgPwd.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoChgPwd.Name = "otoChgPwd"
        Me.otoChgPwd.Size = New System.Drawing.Size(62, 52)
        Me.otoChgPwd.Tag = "2;����¹����;Change"
        Me.otoChgPwd.Text = "Change"
        Me.otoChgPwd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.otoChgPwd.ToolTipText = "Change Password"
        '
        'otoMainSep0
        '
        Me.otoMainSep0.Name = "otoMainSep0"
        Me.otoMainSep0.Size = New System.Drawing.Size(6, 55)
        '
        'otoUpload
        '
        Me.otoUpload.AutoSize = False
        Me.otoUpload.Image = CType(resources.GetObject("otoUpload.Image"), System.Drawing.Image)
        Me.otoUpload.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoUpload.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoUpload.Name = "otoUpload"
        Me.otoUpload.Size = New System.Drawing.Size(59, 52)
        Me.otoUpload.Tag = "2;�ѻ��Ŵ;Upload"
        Me.otoUpload.Text = "Upload"
        Me.otoUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.otoUpload.ToolTipText = "Upload Promotion"
        Me.otoUpload.Visible = False
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 55)
        '
        'otoLog
        '
        Me.otoLog.AutoSize = False
        Me.otoLog.Image = CType(resources.GetObject("otoLog.Image"), System.Drawing.Image)
        Me.otoLog.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoLog.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoLog.Name = "otoLog"
        Me.otoLog.Size = New System.Drawing.Size(80, 52)
        Me.otoLog.Tag = "2;   ����ѵ�   ; History Log"
        Me.otoLog.Text = " History Log"
        Me.otoLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 55)
        '
        'otoHelp
        '
        Me.otoHelp.AutoSize = False
        Me.otoHelp.Enabled = False
        Me.otoHelp.Image = CType(resources.GetObject("otoHelp.Image"), System.Drawing.Image)
        Me.otoHelp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoHelp.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoHelp.Name = "otoHelp"
        Me.otoHelp.Size = New System.Drawing.Size(59, 52)
        Me.otoHelp.Tag = "2;������;How to"
        Me.otoHelp.Text = "How to"
        Me.otoHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.otoHelp.ToolTipText = "How To"
        Me.otoHelp.Visible = False
        '
        'otoMainSep3
        '
        Me.otoMainSep3.Name = "otoMainSep3"
        Me.otoMainSep3.Size = New System.Drawing.Size(6, 55)
        Me.otoMainSep3.Visible = False
        '
        'otoExit
        '
        Me.otoExit.AutoSize = False
        Me.otoExit.Image = CType(resources.GetObject("otoExit.Image"), System.Drawing.Image)
        Me.otoExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.otoExit.ImageTransparentColor = System.Drawing.Color.Black
        Me.otoExit.Name = "otoExit"
        Me.otoExit.Size = New System.Drawing.Size(59, 52)
        Me.otoExit.Tag = "2;�͡;Exit"
        Me.otoExit.Text = "Exit"
        Me.otoExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'omnHelpAbout
        '
        Me.omnHelpAbout.Name = "omnHelpAbout"
        Me.omnHelpAbout.Size = New System.Drawing.Size(140, 22)
        Me.omnHelpAbout.Tag = "2;����ǡѺ;&About ..."
        Me.omnHelpAbout.Text = "&About ..."
        '
        'ostTime
        '
        Me.ostTime.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.ostTime.Image = CType(resources.GetObject("ostTime.Image"), System.Drawing.Image)
        Me.ostTime.Name = "ostTime"
        Me.ostTime.Size = New System.Drawing.Size(37, 20)
        Me.ostTime.Text = ": "
        '
        'ostDate
        '
        Me.ostDate.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.ostDate.Image = CType(resources.GetObject("ostDate.Image"), System.Drawing.Image)
        Me.ostDate.Name = "ostDate"
        Me.ostDate.Size = New System.Drawing.Size(37, 20)
        Me.ostDate.Text = ": "
        '
        'otmForm
        '
        Me.otmForm.Enabled = True
        '
        'ostMDB
        '
        Me.ostMDB.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.ostMDB.Image = CType(resources.GetObject("ostMDB.Image"), System.Drawing.Image)
        Me.ostMDB.Name = "ostMDB"
        Me.ostMDB.Size = New System.Drawing.Size(37, 20)
        Me.ostMDB.Text = ": "
        '
        'omnHelpSep1
        '
        Me.omnHelpSep1.Name = "omnHelpSep1"
        Me.omnHelpSep1.Size = New System.Drawing.Size(137, 6)
        Me.omnHelpSep1.Visible = False
        '
        'omnPrc
        '
        Me.omnPrc.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.omnImport, Me.omnPrcSep1, Me.omnExport, Me.omnExportRecon, Me.omnExpExpense, Me.omnExpCashTnf, Me.omnPrcSep2, Me.omnUpload})
        Me.omnPrc.Name = "omnPrc"
        Me.omnPrc.Size = New System.Drawing.Size(49, 20)
        Me.omnPrc.Tag = "2;������;&Data"
        Me.omnPrc.Text = "&Data"
        '
        'omnImport
        '
        Me.omnImport.Image = CType(resources.GetObject("omnImport.Image"), System.Drawing.Image)
        Me.omnImport.Name = "omnImport"
        Me.omnImport.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.omnImport.Size = New System.Drawing.Size(224, 22)
        Me.omnImport.Tag = "2;�����...;Import..."
        Me.omnImport.Text = "Import..."
        '
        'omnPrcSep1
        '
        Me.omnPrcSep1.Name = "omnPrcSep1"
        Me.omnPrcSep1.Size = New System.Drawing.Size(221, 6)
        '
        'omnExport
        '
        Me.omnExport.Image = CType(resources.GetObject("omnExport.Image"), System.Drawing.Image)
        Me.omnExport.ImageTransparentColor = System.Drawing.Color.Black
        Me.omnExport.Name = "omnExport"
        Me.omnExport.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.omnExport.Size = New System.Drawing.Size(224, 22)
        Me.omnExport.Tag = "2;���͡��â��...;Export Sale..."
        Me.omnExport.Text = "Export Sale..."
        '
        'omnExportRecon
        '
        Me.omnExportRecon.Image = CType(resources.GetObject("omnExportRecon.Image"), System.Drawing.Image)
        Me.omnExportRecon.Name = "omnExportRecon"
        Me.omnExportRecon.ShortcutKeys = System.Windows.Forms.Keys.F6
        Me.omnExportRecon.Size = New System.Drawing.Size(224, 22)
        Me.omnExportRecon.Tag = "2;���͡��â�� (Reconcile)...;Export Sale (Reconcile)..."
        Me.omnExportRecon.Text = "Export Reconcile"
        Me.omnExportRecon.Visible = False
        '
        'omnExpExpense
        '
        Me.omnExpExpense.Image = CType(resources.GetObject("omnExpExpense.Image"), System.Drawing.Image)
        Me.omnExpExpense.Name = "omnExpExpense"
        Me.omnExpExpense.ShortcutKeys = System.Windows.Forms.Keys.F7
        Me.omnExpExpense.Size = New System.Drawing.Size(224, 22)
        Me.omnExpExpense.Tag = "2;���͡��������...;Export Expense..."
        Me.omnExpExpense.Text = "Export Expense"
        Me.omnExpExpense.Visible = False
        '
        'omnExpCashTnf
        '
        Me.omnExpCashTnf.Image = CType(resources.GetObject("omnExpCashTnf.Image"), System.Drawing.Image)
        Me.omnExpCashTnf.Name = "omnExpCashTnf"
        Me.omnExpCashTnf.ShortcutKeys = System.Windows.Forms.Keys.F8
        Me.omnExpCashTnf.Size = New System.Drawing.Size(224, 22)
        Me.omnExpCashTnf.Tag = "2;���͡�ѹ�֡�Թ�ҡ...;Export Cash Transfer..."
        Me.omnExpCashTnf.Text = "Export Cash Transfer"
        Me.omnExpCashTnf.Visible = False
        '
        'omnPrcSep2
        '
        Me.omnPrcSep2.Name = "omnPrcSep2"
        Me.omnPrcSep2.Size = New System.Drawing.Size(221, 6)
        Me.omnPrcSep2.Visible = False
        '
        'omnUpload
        '
        Me.omnUpload.Image = CType(resources.GetObject("omnUpload.Image"), System.Drawing.Image)
        Me.omnUpload.Name = "omnUpload"
        Me.omnUpload.Size = New System.Drawing.Size(224, 22)
        Me.omnUpload.Tag = "2;�ѻ��Ŵ...;Upload..."
        Me.omnUpload.Text = "Upload..."
        Me.omnUpload.Visible = False
        '
        'omnFileExit
        '
        Me.omnFileExit.Image = CType(resources.GetObject("omnFileExit.Image"), System.Drawing.Image)
        Me.omnFileExit.Name = "omnFileExit"
        Me.omnFileExit.Size = New System.Drawing.Size(195, 22)
        Me.omnFileExit.Tag = "2;�͡;Exit"
        Me.omnFileExit.Text = "Exit"
        '
        'omnFile
        '
        Me.omnFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.omnFileChgPwd, Me.omnFileSep1, Me.omnFileExit})
        Me.omnFile.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.omnFile.Name = "omnFile"
        Me.omnFile.Size = New System.Drawing.Size(42, 20)
        Me.omnFile.Tag = "2;���;&File"
        Me.omnFile.Text = "&File"
        '
        'omnFileChgPwd
        '
        Me.omnFileChgPwd.Image = CType(resources.GetObject("omnFileChgPwd.Image"), System.Drawing.Image)
        Me.omnFileChgPwd.Name = "omnFileChgPwd"
        Me.omnFileChgPwd.Size = New System.Drawing.Size(195, 22)
        Me.omnFileChgPwd.Tag = "2;����¹����;Change Password..."
        Me.omnFileChgPwd.Text = "Change Password..."
        '
        'omnFileSep1
        '
        Me.omnFileSep1.Name = "omnFileSep1"
        Me.omnFileSep1.Size = New System.Drawing.Size(192, 6)
        '
        'omnForm
        '
        Me.omnForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.omnForm.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.omnFile, Me.omnPrc, Me.omnTool, Me.omnHelp})
        Me.omnForm.Location = New System.Drawing.Point(0, 0)
        Me.omnForm.Name = "omnForm"
        Me.omnForm.Padding = New System.Windows.Forms.Padding(11, 2, 0, 2)
        Me.omnForm.Size = New System.Drawing.Size(794, 24)
        Me.omnForm.TabIndex = 8
        Me.omnForm.Text = "MenuStrip"
        '
        'omnTool
        '
        Me.omnTool.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.omnToolsLog, Me.omnToolsRebuild, Me.omnToolsSep1, Me.omnToolsOption})
        Me.omnTool.Name = "omnTool"
        Me.omnTool.Size = New System.Drawing.Size(55, 20)
        Me.omnTool.Tag = "2;����ͧ���;&Tools"
        Me.omnTool.Text = "&Tools"
        '
        'omnToolsLog
        '
        Me.omnToolsLog.Image = CType(resources.GetObject("omnToolsLog.Image"), System.Drawing.Image)
        Me.omnToolsLog.Name = "omnToolsLog"
        Me.omnToolsLog.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.omnToolsLog.Size = New System.Drawing.Size(194, 22)
        Me.omnToolsLog.Tag = "2;����ѵ�...;History Log..."
        Me.omnToolsLog.Text = "&History Log..."
        Me.omnToolsLog.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal
        '
        'omnToolsRebuild
        '
        Me.omnToolsRebuild.Name = "omnToolsRebuild"
        Me.omnToolsRebuild.Size = New System.Drawing.Size(194, 22)
        Me.omnToolsRebuild.Tag = "2;&���ҧ�Ѫ�բ���������...;&Rebuild Index..."
        Me.omnToolsRebuild.Text = "&Rebuild Index..."
        Me.omnToolsRebuild.Visible = False
        '
        'omnToolsSep1
        '
        Me.omnToolsSep1.Name = "omnToolsSep1"
        Me.omnToolsSep1.Size = New System.Drawing.Size(191, 6)
        '
        'omnToolsOption
        '
        Me.omnToolsOption.Checked = True
        Me.omnToolsOption.CheckState = System.Windows.Forms.CheckState.Checked
        Me.omnToolsOption.Image = CType(resources.GetObject("omnToolsOption.Image"), System.Drawing.Image)
        Me.omnToolsOption.Name = "omnToolsOption"
        Me.omnToolsOption.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.omnToolsOption.Size = New System.Drawing.Size(194, 22)
        Me.omnToolsOption.Tag = "2;��õ�駤��...;&Setting..."
        Me.omnToolsOption.Text = "&Setting..."
        '
        'omnHelp
        '
        Me.omnHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.omnHelpHowTo, Me.omnHelpSep1, Me.omnHelpAbout})
        Me.omnHelp.Name = "omnHelp"
        Me.omnHelp.Size = New System.Drawing.Size(49, 20)
        Me.omnHelp.Tag = "2;���������;&Help"
        Me.omnHelp.Text = "&Help"
        '
        'omnHelpHowTo
        '
        Me.omnHelpHowTo.Enabled = False
        Me.omnHelpHowTo.Image = CType(resources.GetObject("omnHelpHowTo.Image"), System.Drawing.Image)
        Me.omnHelpHowTo.ImageTransparentColor = System.Drawing.Color.Black
        Me.omnHelpHowTo.Name = "omnHelpHowTo"
        Me.omnHelpHowTo.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.omnHelpHowTo.Size = New System.Drawing.Size(140, 22)
        Me.omnHelpHowTo.Tag = "2;������;How to"
        Me.omnHelpHowTo.Text = "How to"
        Me.omnHelpHowTo.Visible = False
        '
        'ostForm
        '
        Me.ostForm.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ostForm.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ostMInfo, Me.ostMDB, Me.ostDate, Me.ostTime})
        Me.ostForm.Location = New System.Drawing.Point(0, 543)
        Me.ostForm.Name = "ostForm"
        Me.ostForm.Padding = New System.Windows.Forms.Padding(1, 0, 25, 0)
        Me.ostForm.Size = New System.Drawing.Size(794, 25)
        Me.ostForm.TabIndex = 10
        Me.ostForm.Text = "StatusStrip"
        '
        'ostMInfo
        '
        Me.ostMInfo.BackColor = System.Drawing.SystemColors.Control
        Me.ostMInfo.Name = "ostMInfo"
        Me.ostMInfo.Size = New System.Drawing.Size(657, 20)
        Me.ostMInfo.Spring = True
        Me.ostMInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'oimForm
        '
        Me.oimForm.ImageStream = CType(resources.GetObject("oimForm.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.oimForm.TransparentColor = System.Drawing.Color.Transparent
        Me.oimForm.Images.SetKeyName(0, "Thailand.png")
        Me.oimForm.Images.SetKeyName(1, "United States of America (USA).png")
        Me.oimForm.Images.SetKeyName(2, "eng.png")
        Me.oimForm.Images.SetKeyName(3, "thai.png")
        '
        'wMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(794, 568)
        Me.Controls.Add(Me.otoForm)
        Me.Controls.Add(Me.omnForm)
        Me.Controls.Add(Me.ostForm)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimumSize = New System.Drawing.Size(800, 600)
        Me.Name = "wMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AdaLink@SAP For BUFC"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.otoForm.ResumeLayout(False)
        Me.otoForm.PerformLayout()
        Me.omnForm.ResumeLayout(False)
        Me.omnForm.PerformLayout()
        Me.ostForm.ResumeLayout(False)
        Me.ostForm.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents otoMainSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents otoImport As System.Windows.Forms.ToolStripButton
    Friend WithEvents otoMainSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents otoExport As System.Windows.Forms.ToolStripButton
    Friend WithEvents otoForm As System.Windows.Forms.ToolStrip
    Friend WithEvents otoChgPwd As System.Windows.Forms.ToolStripButton
    Friend WithEvents otoMainSep0 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents otoHelp As System.Windows.Forms.ToolStripButton
    Friend WithEvents otoMainSep3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents otoExit As System.Windows.Forms.ToolStripButton
    Friend WithEvents omnHelpAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ostTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ostDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents otmForm As System.Windows.Forms.Timer
    Friend WithEvents ottForm As System.Windows.Forms.ToolTip
    Friend WithEvents ostMDB As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents omnHelpSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents omnPrc As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnPrcSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents omnExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnFileExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnFileChgPwd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnFileSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents omnForm As System.Windows.Forms.MenuStrip
    Friend WithEvents omnTool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnToolsLog As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnToolsSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents omnToolsOption As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnHelpHowTo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ostForm As System.Windows.Forms.StatusStrip
    Friend WithEvents otoChangeLang As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents otoLog As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents oimForm As System.Windows.Forms.ImageList
    Friend WithEvents ostMInfo As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents omnToolsRebuild As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents omnPrcSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents omnUpload As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents otoUpload As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents omnExportRecon As ToolStripMenuItem
    Friend WithEvents omnExpExpense As ToolStripMenuItem
    Friend WithEvents omnExpCashTnf As ToolStripMenuItem
End Class
