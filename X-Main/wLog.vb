Imports System.IO
Imports System.Windows
Imports C1.Win.C1FlexGrid
Imports System.Data.SqlClient

Public Class wLog

    'ʶҹ��Դ�����
    Private bW_FrmLoad As Boolean = False
    'Caption ogdImport
    Private tW_CaptionImport As String = "Date,Time,File Name,Description,Refer"
    Private tW_CaptionImportThai As String = "�ѹ���,����,�������,��������´,��ҧ�ԧ"
    'Caption ogdExport
    Private tW_CaptionExport As String = "Date,Time,File Name,Description,Refer"
    Private tW_CaptionExportThai As String = "�ѹ���,����,������,��������´,��ҧ�ԧ"

    'Load ��¡�� Import ��� ogdImport
    Private Sub W_SETxListImport()
        With Me.ogdImport
            .AllowResizing = True
            .AllowDragging = False
            Dim tDate As String = ""
            If Me.ockSelAll.Checked = False Then
                tDate = Format(Me.odtLog.Value, "yyyy-MM-dd")
            End If

            Dim oItemImport = cLogTemplate.C_GETaLogImport(tDate)
            .DataSource = oItemImport
            Me.W_SETxCaptionGridImport()
        End With
    End Sub

    'Load ��¡�� Export ��� ogdExport
    Private Sub W_SETxListExport()
        With Me.ogdExport
            .AllowResizing = True
            .AllowDragging = False
            Dim tDate As String = ""
            If Me.ockSelAll.Checked = False Then
                tDate = Format(Me.odtLog.Value, "yyyy-MM-dd")
            End If
            Dim oItemExport = cLogTemplate.C_GETaLogExport(tDate)
            .DataSource = oItemExport
            Me.W_SETxCaptionGridExport()
        End With
    End Sub

    Private Sub W_SETxCaptionGridImport()
        Dim tCapImport As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '1:thai
            tCapImport = tW_CaptionImport
        Else
            tCapImport = tW_CaptionImportThai
        End If
        With Me.ogdImport
            Dim nCol As Integer = 0 'Start Column 0
            For Each tCap In tCapImport.Split(",")
                .Cols(nCol).Caption = tCap
                .Cols(nCol).TextAlignFixed = TextAlignEnum.CenterCenter
                nCol += 1
            Next
            .Cols(0).Width = 80
            .Cols(0).AllowEditing = False
            .Cols(0).TextAlign = TextAlignEnum.CenterCenter
            .Cols(1).Width = 80
            .Cols(1).AllowEditing = False
            .Cols(1).TextAlign = TextAlignEnum.CenterCenter
            .Cols(2).Width = 300 '220
            .Cols(2).AllowEditing = False
            .Cols(3).Width = 230
            .Cols(3).AllowEditing = False
            .Cols(4).Width = 240
            .Cols(4).AllowEditing = True
            .Cols(4).ComboList = "..."
        End With
    End Sub

    Private Sub W_SETxCaptionGridExport()
        Dim tCapExport As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '1:thai
            tCapExport = tW_CaptionExport
        Else
            tCapExport = tW_CaptionExportThai
        End If

        With Me.ogdExport
            Dim nCol As Integer = 0 'Start Column 0
            For Each tCap In tCapExport.Split(",")
                .Cols(nCol).Caption = tCap
                .Cols(nCol).TextAlignFixed = TextAlignEnum.CenterCenter
                nCol += 1
            Next
            .Cols(0).Width = 80
            .Cols(0).AllowEditing = False
            .Cols(0).TextAlign = TextAlignEnum.CenterCenter
            .Cols(1).Width = 80
            .Cols(1).AllowEditing = False
            .Cols(1).TextAlign = TextAlignEnum.CenterCenter
            .Cols(2).Width = 250
            .Cols(2).AllowEditing = False
            ' .Cols(2).TextAlign = TextAlignEnum.CenterCenter
            .Cols(3).Width = 180
            .Cols(3).AllowEditing = False
            .Cols(4).Width = 210
            .Cols(4).AllowEditing = True
            .Cols(4).ComboList = "..."
        End With
    End Sub

    Private Sub ogdExportLog_BeforeEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdExport.BeforeEdit
        With Me.ogdExport
            Select Case e.Col
                Case .Cols("tRefer").Index
                    If .Item(e.Row, "tRefer").ToString.Length = 0 Then
                        e.Cancel = True
                    End If
            End Select
        End With
    End Sub

    Private Sub ogdExportLog_CellButtonClick(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdExport.CellButtonClick
        With Me.ogdExport
            Select Case e.Col
                Case .Cols("tRefer").Index
                    If .Item(.RowSel, "tRefer").ToString.Length > 0 Then
                        Dim tAppPath As String = .Item(.RowSel, "tRefer")
                        If System.IO.File.Exists(tAppPath) = True Then
                            Diagnostics.Process.Start(tAppPath)
                        Else
                            cCNSP.SP_MSGnShowing(tAppPath, cCNEN.eEN_MSGStyle.nEN_MSGWarn, True)
                        End If
                    End If
            End Select
        End With
    End Sub

    Private Sub ogdImport_BeforeEdit(sender As Object, e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdImport.BeforeEdit
        With Me.ogdImport
            Select Case e.Col
                Case .Cols("tRefer").Index
                    If .Item(e.Row, "tRefer").ToString.Length = 0 Then
                        e.Cancel = True
                    End If
            End Select
        End With
    End Sub

    Private Sub ogdImport_CellButtonClick(sender As Object, e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdImport.CellButtonClick
        With Me.ogdImport
            Select Case e.Col
                Case .Cols("tRefer").Index
                    If .Item(.RowSel, "tRefer").ToString.Length > 0 Then
                        Dim tAppPath As String = .Item(.RowSel, "tRefer")
                        If System.IO.File.Exists(tAppPath) = True Then
                            Diagnostics.Process.Start(tAppPath)
                        Else
                            cCNSP.SP_MSGnShowing(tAppPath, cCNEN.eEN_MSGStyle.nEN_MSGWarn, True)
                        End If
                    End If
            End Select
        End With
    End Sub

    Private Sub ockSelAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ockSelAll.CheckedChanged, odtLog.ValueChanged
        Select Case Me.otaLog.SelectedTab.Name
            Case "otaImportLog"
                'Default Import
                Me.W_SETxListImport()
            Case "otaExportLog"
                'Default Import
                Me.W_SETxListExport()
        End Select
    End Sub

    Private Sub wLog_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        '�ʴ����Ңͧ�����
        cCNSP.SP_FrmSetCapControl(Me)
        '��Ǩ�ͺ�Դ��Ͱҹ������
        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
            Me.Close()
        End If

        'Default Import
        Me.W_SETxListImport()
        Me.bW_FrmLoad = True
    End Sub

    Private Sub ocmClose_Click(sender As System.Object, e As System.EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Private Sub otaLog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles otaLog.SelectedIndexChanged
        Select Case Me.otaLog.SelectedTab.Name
            Case "otaImportLog"
                'Default Import
                Me.W_SETxListImport()
            Case "otaExportLog"
                'Default Import
                Me.W_SETxListExport()
        End Select
    End Sub

    Private Sub ocmclearLog_Click(sender As System.Object, e As System.EventArgs) Handles ocmclearLog.Click

        '��������͹�ź Log
        Dim tDateWhere As String = ""
        Select Case Me.otaLog.SelectedTab.Name
            Case "otaImportLog"
                tDateWhere = "FNType=1"
            Case "otaExportLog"
                tDateWhere = "FNType=2"
        End Select

        If Me.ockSelAll.Checked = False Then
            tDateWhere &= " AND CONVERT(VARCHAR(10),FDDateTime,120)='" & Format(Me.odtLog.Value, "yyyy-MM-dd") & "'"
        End If

        'Call Delete Log
        cLogTemplate.C_CALxDeleteLog(tDateWhere)

        'Refresh Grid
        Me.otaLog_SelectedIndexChanged(Me.otaLog, New System.EventArgs)

        cLog.C_CALxWriteLog("wLog > ClearLog")

    End Sub

End Class