<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wLog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wLog))
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.otaLog = New System.Windows.Forms.TabControl()
        Me.otaImportLog = New System.Windows.Forms.TabPage()
        Me.ogdImport = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.otaExportLog = New System.Windows.Forms.TabPage()
        Me.ogdExport = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.opnBottom = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.olaClearLog = New System.Windows.Forms.Label()
        Me.odtLog = New System.Windows.Forms.DateTimePicker()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.ocmclearLog = New System.Windows.Forms.Button()
        Me.ockSelAll = New System.Windows.Forms.CheckBox()
        Me.ottForm = New System.Windows.Forms.ToolTip(Me.components)
        Me.osdForm = New System.Windows.Forms.SaveFileDialog()
        Me.opnForm.SuspendLayout()
        Me.otaLog.SuspendLayout()
        Me.otaImportLog.SuspendLayout()
        CType(Me.ogdImport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.otaExportLog.SuspendLayout()
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnBottom.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnForm
        '
        Me.opnForm.Controls.Add(Me.otaLog)
        Me.opnForm.Controls.Add(Me.opnBottom)
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(0, 0)
        Me.opnForm.Margin = New System.Windows.Forms.Padding(4)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(931, 540)
        Me.opnForm.TabIndex = 0
        '
        'otaLog
        '
        Me.otaLog.Controls.Add(Me.otaImportLog)
        Me.otaLog.Controls.Add(Me.otaExportLog)
        Me.otaLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.otaLog.Location = New System.Drawing.Point(0, 0)
        Me.otaLog.Name = "otaLog"
        Me.otaLog.SelectedIndex = 0
        Me.otaLog.Size = New System.Drawing.Size(931, 489)
        Me.otaLog.TabIndex = 0
        Me.otaLog.Tag = "2"
        '
        'otaImportLog
        '
        Me.otaImportLog.Controls.Add(Me.ogdImport)
        Me.otaImportLog.Location = New System.Drawing.Point(4, 25)
        Me.otaImportLog.Name = "otaImportLog"
        Me.otaImportLog.Padding = New System.Windows.Forms.Padding(3)
        Me.otaImportLog.Size = New System.Drawing.Size(923, 460)
        Me.otaImportLog.TabIndex = 2
        Me.otaImportLog.Tag = "2;�����;Import"
        Me.otaImportLog.Text = "Import"
        Me.otaImportLog.UseVisualStyleBackColor = True
        '
        'ogdImport
        '
        Me.ogdImport.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdImport.ColumnInfo = resources.GetString("ogdImport.ColumnInfo")
        Me.ogdImport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdImport.ExtendLastCol = True
        Me.ogdImport.Location = New System.Drawing.Point(3, 3)
        Me.ogdImport.Margin = New System.Windows.Forms.Padding(4)
        Me.ogdImport.Name = "ogdImport"
        Me.ogdImport.Rows.Count = 21
        Me.ogdImport.Rows.DefaultSize = 21
        Me.ogdImport.Rows.MaxSize = 50
        Me.ogdImport.Rows.MinSize = 21
        Me.ogdImport.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdImport.Size = New System.Drawing.Size(917, 454)
        Me.ogdImport.StyleInfo = resources.GetString("ogdImport.StyleInfo")
        Me.ogdImport.TabIndex = 1
        Me.ogdImport.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'otaExportLog
        '
        Me.otaExportLog.Controls.Add(Me.ogdExport)
        Me.otaExportLog.Location = New System.Drawing.Point(4, 25)
        Me.otaExportLog.Name = "otaExportLog"
        Me.otaExportLog.Padding = New System.Windows.Forms.Padding(3)
        Me.otaExportLog.Size = New System.Drawing.Size(923, 460)
        Me.otaExportLog.TabIndex = 1
        Me.otaExportLog.Tag = "2;���͡;Export"
        Me.otaExportLog.Text = "Export"
        Me.otaExportLog.UseVisualStyleBackColor = True
        '
        'ogdExport
        '
        Me.ogdExport.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdExport.ColumnInfo = resources.GetString("ogdExport.ColumnInfo")
        Me.ogdExport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdExport.ExtendLastCol = True
        Me.ogdExport.Location = New System.Drawing.Point(3, 3)
        Me.ogdExport.Margin = New System.Windows.Forms.Padding(4)
        Me.ogdExport.Name = "ogdExport"
        Me.ogdExport.Rows.Count = 21
        Me.ogdExport.Rows.DefaultSize = 21
        Me.ogdExport.Rows.MaxSize = 50
        Me.ogdExport.Rows.MinSize = 21
        Me.ogdExport.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdExport.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdExport.Size = New System.Drawing.Size(917, 454)
        Me.ogdExport.StyleInfo = resources.GetString("ogdExport.StyleInfo")
        Me.ogdExport.TabIndex = 0
        Me.ogdExport.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'opnBottom
        '
        Me.opnBottom.Controls.Add(Me.Panel1)
        Me.opnBottom.Controls.Add(Me.ockSelAll)
        Me.opnBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.opnBottom.Location = New System.Drawing.Point(0, 489)
        Me.opnBottom.Name = "opnBottom"
        Me.opnBottom.Size = New System.Drawing.Size(931, 51)
        Me.opnBottom.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.olaClearLog)
        Me.Panel1.Controls.Add(Me.odtLog)
        Me.Panel1.Controls.Add(Me.ocmClose)
        Me.Panel1.Controls.Add(Me.ocmclearLog)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel1.Location = New System.Drawing.Point(432, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(499, 51)
        Me.Panel1.TabIndex = 6
        '
        'olaClearLog
        '
        Me.olaClearLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.olaClearLog.BackColor = System.Drawing.Color.Transparent
        Me.olaClearLog.Location = New System.Drawing.Point(36, 16)
        Me.olaClearLog.Name = "olaClearLog"
        Me.olaClearLog.Size = New System.Drawing.Size(120, 19)
        Me.olaClearLog.TabIndex = 2
        Me.olaClearLog.Tag = "2;���͡�ѹ���;Select Date"
        Me.olaClearLog.Text = "Select Date"
        Me.olaClearLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'odtLog
        '
        Me.odtLog.CustomFormat = "dd/MM/yyyy"
        Me.odtLog.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.odtLog.Location = New System.Drawing.Point(162, 14)
        Me.odtLog.Name = "odtLog"
        Me.odtLog.Size = New System.Drawing.Size(89, 22)
        Me.odtLog.TabIndex = 3
        '
        'ocmClose
        '
        Me.ocmClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmClose.Location = New System.Drawing.Point(392, 10)
        Me.ocmClose.Margin = New System.Windows.Forms.Padding(4)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose.TabIndex = 5
        Me.ocmClose.Tag = "2;�Դ;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'ocmclearLog
        '
        Me.ocmclearLog.Location = New System.Drawing.Point(284, 10)
        Me.ocmclearLog.Margin = New System.Windows.Forms.Padding(4)
        Me.ocmclearLog.Name = "ocmclearLog"
        Me.ocmclearLog.Size = New System.Drawing.Size(100, 30)
        Me.ocmclearLog.TabIndex = 4
        Me.ocmclearLog.Tag = "2;��ҧ Log;Clear Log"
        Me.ocmclearLog.Text = "Delete  Log"
        Me.ocmclearLog.UseVisualStyleBackColor = True
        '
        'ockSelAll
        '
        Me.ockSelAll.AutoSize = True
        Me.ockSelAll.BackColor = System.Drawing.Color.Transparent
        Me.ockSelAll.Location = New System.Drawing.Point(12, 13)
        Me.ockSelAll.Name = "ockSelAll"
        Me.ockSelAll.Size = New System.Drawing.Size(113, 20)
        Me.ockSelAll.TabIndex = 1
        Me.ockSelAll.Tag = "2;����ѵԷ�����;All History Log"
        Me.ockSelAll.Text = "All History Log"
        Me.ockSelAll.UseVisualStyleBackColor = False
        '
        'osdForm
        '
        Me.osdForm.DefaultExt = "txt"
        Me.osdForm.Filter = "Text file|*.txt|Xml file|*.xml"
        '
        'wLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmClose
        Me.ClientSize = New System.Drawing.Size(931, 540)
        Me.Controls.Add(Me.opnForm)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimizeBox = False
        Me.Name = "wLog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;����ѵ�;History Log"
        Me.Text = "History Log"
        Me.opnForm.ResumeLayout(False)
        Me.otaLog.ResumeLayout(False)
        Me.otaImportLog.ResumeLayout(False)
        CType(Me.ogdImport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.otaExportLog.ResumeLayout(False)
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnBottom.ResumeLayout(False)
        Me.opnBottom.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents opnForm As System.Windows.Forms.Panel
    Friend WithEvents ocmClose As System.Windows.Forms.Button
    Friend WithEvents ocmclearLog As System.Windows.Forms.Button
    Friend WithEvents ottForm As System.Windows.Forms.ToolTip
    Friend WithEvents otaLog As System.Windows.Forms.TabControl
    Friend WithEvents otaExportLog As System.Windows.Forms.TabPage
    Friend WithEvents ogdExport As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents osdForm As System.Windows.Forms.SaveFileDialog
    Friend WithEvents olaClearLog As System.Windows.Forms.Label
    Friend WithEvents odtLog As System.Windows.Forms.DateTimePicker
    Friend WithEvents ockSelAll As System.Windows.Forms.CheckBox
    Friend WithEvents otaImportLog As System.Windows.Forms.TabPage
    Friend WithEvents ogdImport As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents opnBottom As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
