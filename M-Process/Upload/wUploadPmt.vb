﻿Imports AdaLinkSAPBUFC.AdaConfig
Imports System.ComponentModel

Public Class wUploadPmt

#Region "Variable-Private"
    'ตารางประวัติ
    Private Shared tC_TableHis As String = "TLNKUpPmtHis"

    'สถานะเปิดฟอร์ม
    Private bW_FrmLoad As Boolean = False

    'ภาษา
    Private nW_Lang As Integer = 0 '0:Thai, 1:Eng

    'Caption Grid
    Private Const tW_CaptionUpload As String = "รายการไฟล์;List File,รายการไฟล์;List File,สถานะ;Status,สถานะ;Status"
    Private Const tW_CaptionClrPmt As String = "วันที่นำเข้า;Date insert,วันที่นำเข้า;Date insert,เวลานำเข้า;Time insert,สถานะ;Status,สถานะ;Status"
    Private Const tW_Wait As String = "รอ;Wait"
    Private Const tW_Success As String = "สำเร็จ;Success"
    Private Const tW_Unsuccess As String = "ไม่สำเร็จ;Unsuccessful"

    'Upload promotion
    Public Shared oW_BackgroudWord As BackgroundWorker '*CH 19-12-2014
    Private dW_StartTime As DateTime
    Private dW_EndTime As DateTime

    Public Shared aC_Detail As New List(Of cUploadDetail)
    'Sub Class
    Class cUploadDetail
        Private aStatus As New Dictionary(Of Integer, String)
        Sub New()
            'เพิ่มสถานะ
            aStatus.Add(0, "รอ;Wait")
            aStatus.Add(1, "สำเร็จ;Success")
            aStatus.Add(2, "ไม่สำเร็จ;Unsuccessful")
            aStatus.Add(-1, "ไฟล์ไม่ครบ;Missing")
        End Sub

        Property nC_Select As Integer = 1 'Default:1 Select,0:Deselect
        Property tC_BchCode As String = ""
        Property tC_Name As String = "" 'File Name
        Property tC_FullName As String = "" 'Full Path
        Property nC_Length As Long = 0
        Property tC_Length As String = nC_Length.SP_CNSETtFormatFileSize
        Property tC_Date As String = "" 'จาก Text File
        Property nC_Status As Integer = 0 '0:Wait,1:Upload Complete,2:Upload InComplete,-1:Error
        Property tC_DateIns As String = ""
        Property tC_TimeIns As String = ""
        Property nC_GridRow As Integer = 0

        Public ReadOnly Property tC_Status() As String
            Get
                If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '2:thai
                    Return aStatus(nC_Status).Split(";")(1)
                Else
                    Return aStatus(nC_Status).Split(";")(0)
                End If
            End Get
        End Property
    End Class

    'Clear Promotion
    Private Shared oW_BackgroudWordClr As BackgroundWorker '*CH 19-12-2014
    Private Shared aC_ClrPmtDetail As New List(Of cClearPmt)
    Private Shared tC_StaClrPmt As String = ""
    Private Shared nC_StaClrPmt As Integer = 0
    Private Shared nC_GridRow As Integer = 0
#End Region

#Region "Global Class"

    Private Sub wUploadPmt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then
            nW_Lang = 1
        Else
            nW_Lang = 0
        End If

        'แสดงภาษาของฟอร์ม
        cCNSP.SP_FrmSetCapControl(Me)

        'Get Branch
        C_GETxBranch()

        'Get File Upload
        C_GETxFileUpload()

        'Get History Import Promotion
        C_GETxHisPmt()

        Me.bW_FrmLoad = True
    End Sub

    Private Function C_GETtRowItemValue(ByVal poRow As DataRow, ByVal ptFieldName As String, Optional ByVal pnRtnDataZero As Integer = 0) As String
        'pnRtnDataZero ใช้ในกรณีที่จะคืนค่าประเภทวันที่ เป็นค่า 1111/11/11(ถ้าส่ง ค่าที่ไม่ใช้ 0 มา) ในกรณีที่วันที่เป็น Null 
        'เพื่อให้ทราบว่า 1111/11/11 คือวันที่เป็น Null เวลาแสดงจะได้แสดงเป็นค่าว่าง
        '*Lex 08-03-2014

        Dim tReturn As String = ""

        tReturn = poRow.Item(ptFieldName).ToString
        Select Case Microsoft.VisualBasic.Strings.Left(ptFieldName, 2)
            Case "FD"
                If IsDBNull(tReturn) OrElse tReturn Is Nothing OrElse tReturn = "" Then
                    If pnRtnDataZero = 0 Then   'แสดง วันที่ปัจจุบัน(กรณีเดิม)
                        tReturn = Format(Now, "yyyy/MM/dd")
                    Else                        'แสดง วันที่ในอดีต(กรณีใหม่)
                        tReturn = Format(CDate("1111/11/11"), "yyyy/MM/dd")
                    End If
                End If
            Case "FT"
                If IsDBNull(tReturn) OrElse tReturn Is Nothing OrElse tReturn = "" Then
                    tReturn = ""
                End If
            Case "FN"
                If IsDBNull(tReturn) OrElse tReturn Is Nothing OrElse tReturn = "" Then
                    tReturn = 0
                End If
            Case "FC"
                If IsDBNull(tReturn) OrElse tReturn Is Nothing OrElse tReturn = "" Then
                    tReturn = 0
                End If
        End Select

        Return tReturn
    End Function
#End Region

#Region "Upload promotion"

    Private oBranch As List(Of cBranch)
    Private Class cBranch
        Property tBchCode As String
        Property tBchName As String
    End Class

    Private Sub C_GETxBranch()
        Dim oDatabase As New cDatabaseLocal
        Dim oSql As New System.Text.StringBuilder
        Try
            oBranch = New List(Of cBranch)
            oSql.Append("SELECT FTBchCode,FTBchName FROM TCNMBranch")
            Dim oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            If oDbTbl.Rows.Count > 0 Then
                Dim oResult = From oRow As DataRow In oDbTbl.Rows
                              Select New cBranch With {
                      .tBchCode = C_GETtRowItemValue(oRow, "FTBchCode") _
                    , .tBchName = C_GETtRowItemValue(oRow, "FTBchName")
                }
                If oResult.Count > 0 Then oBranch = oResult.ToList
                Me.ocbBranch.Items.Clear()
                Dim nRow As Integer = 0
                For nRow = 0 To oResult.Count - 1
                    Me.ocbBranch.Items.Add(oBranch(nRow).tBchName)
                Next
                Me.ocbBranch.SelectedIndex = IIf(nRow > 0, 1, 0)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub C_GETxFileUpload()
        'Path, Type
        Me.otbPathFile.Text = cConfig.UploadPmt.tPathPmt
        Me.otbTypeFile.Text = cConfig.UploadPmt.tTypePmt

        'SET Caption
        W_SETxCaptionGridUpload()

        'List File Excel
        W_GETxAllFile()
        Dim nItemUpload = C_GETnItemUpload()
        If nItemUpload = 0 Then
            Me.ockSelectAll.Checked = False
            Me.ocmProcess.Enabled = False
        Else
            Me.ockSelectAll.Checked = True
            Me.ocmProcess.Enabled = True
        End If

    End Sub

    'Set Caption ogdUpload
    Private Sub W_SETxCaptionGridUpload()
        Dim nCol As Integer = 2 'Start Column 2
        For Each tCap In tW_CaptionUpload.Split(",")
            With Me.ogdUploadPmt
                .Cols(nCol).Caption = tCap.Split(";")(nW_Lang)
            End With
            nCol += 1
        Next
    End Sub

    Private Sub ocmBrwPath_Click(sender As Object, e As EventArgs) Handles ocmBrwPath.Click
        Me.ofdForm.SelectedPath = AdaConfig.cConfig.tAppPath
        Me.ofdForm.Description = "Outbox Location..."
        Me.ofdForm.ShowDialog()
        If Me.ofdForm.SelectedPath <> "" Then
            Me.otbPathFile.Text = Me.ofdForm.SelectedPath
            AdaConfig.cConfig.C_SETbUpdateConfigUploadXml(Me.otbPathFile.Text)
            W_GETxAllFile()
            Dim nItemUpload = C_GETnItemUpload()
            If nItemUpload = 0 Then
                Me.ockSelectAll.Checked = False
                Me.ocmProcess.Enabled = False
            Else
                Me.ockSelectAll.Checked = True
                Me.ocmProcess.Enabled = True
            End If
        End If
    End Sub

    Private Sub ocmRefresh_Click(sender As Object, e As EventArgs) Handles ocmRefresh.Click
        W_GETxAllFile()
        Dim nItemUpload = C_GETnItemUpload()
        If nItemUpload = 0 Then
            Me.ockSelectAll.Checked = False
            Me.ocmProcess.Enabled = False
        Else
            Me.ockSelectAll.Checked = True
            Me.ocmProcess.Enabled = True
        End If
        Me.opgForm.Value = 0
        Me.olaValStatus.Text = ""
    End Sub

    Private Sub W_GETxAllFile()
        Dim PathName As String = Me.otbPathFile.Text
        If PathName.Length > 0 Then
            Dim nRow As Integer = 0
            Dim dt As New DataTable
            Dim odiroot As New System.IO.DirectoryInfo(PathName)

            If Me.otbTypeFile.Text = "" Then
                Exit Sub
            End If

            Dim aTemp() As System.IO.FileInfo = odiroot.GetFiles("*.xls")
            Dim aFiles() As System.IO.FileInfo = odiroot.GetFiles("*.xlsx")

            Me.ocmProcess.Enabled = aFiles.Length > 0

            Dim oNameFile As New List(Of String)
            Dim oPathFile As New List(Of String)
            For nRow = 0 To aFiles.Length - 1
                oNameFile.Add(aFiles(nRow).ToString)
                oPathFile.Add(aFiles(nRow).FullName)
            Next
            For nRow = 0 To aTemp.Length - 1
                oNameFile.Add(aTemp(nRow).ToString)
                oPathFile.Add(aTemp(nRow).FullName)
            Next

            ''sort Name File
            'oNameFile.Sort()
            'oPathFile.Sort()
            'Reverse File
            oNameFile.Reverse()
            oPathFile.Reverse()

            Me.ogdUploadPmt.Rows.Count = Me.ogdUploadPmt.Rows.Fixed

            With Me.ogdUploadPmt
                For nRow = 0 To oNameFile.Count - 1
                    .Rows.Add()
                    .Item(nRow + .Rows.Fixed, "nSelect") = True
                    .Item(nRow + .Rows.Fixed, "tItem") = oNameFile(nRow).ToString
                    .Item(nRow + .Rows.Fixed, "tPathFile") = oPathFile(nRow).ToString
                    .Item(nRow + .Rows.Fixed, "tStatus") = tW_Wait.Split(";")(nW_Lang)
                Next
            End With
        End If
    End Sub

    Private Function C_GETnItemUpload()
        Return Me.ogdUploadPmt.Rows.Count
    End Function

    Private Sub ockSelectAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ockSelectAll.CheckedChanged
        If Me.bW_FrmLoad = False Then Exit Sub

        Dim nItemUpload = C_GETnItemUpload()
        If nItemUpload = 0 Then
            'จำนวนไฟล์ 0 ทั้งหมด ให้ ockSelectAll=false
            Me.ockSelectAll.Checked = False
            Exit Sub
        End If

        Dim nSelect As Integer = 0
        Select Case Me.ockSelectAll.CheckState
            Case CheckState.Checked : nSelect = 1
            Case CheckState.Unchecked : nSelect = 0
            Case CheckState.Indeterminate
        End Select

        With Me.ogdUploadPmt
            For nRow = .Rows.Fixed To .Rows.Count - 1
                If .Item(nRow, "tItem").ToString <> "" Then
                    .Item(nRow, "nSelect") = nSelect
                End If
            Next nRow
        End With
    End Sub

    Private Sub ocmProcess_Click(sender As Object, e As EventArgs) Handles ocmProcess.Click
        Dim nSelect As Integer = 0

        'Get Branch code
        Dim tBchName As String = Me.ocbBranch.SelectedItem.ToString()
        Dim oBch = (From oItem In oBranch Where oItem.tBchName = tBchName Select oItem).FirstOrDefault
        Dim tBchCode As String = oBch.tBchCode

        nSelect = C_GETnFile(tBchCode)
        'nSelect = 0 /ไม่เลือกรายการ import ออกจาก Sub 
        If nSelect = 0 Then
            Exit Sub
        End If

        Try
            oW_BackgroudWord = New BackgroundWorker
            oW_BackgroudWord.WorkerReportsProgress = True
            oW_BackgroudWord.WorkerSupportsCancellation = True
            AddHandler oW_BackgroudWord.DoWork, AddressOf C_RUNxDoWork
            AddHandler oW_BackgroudWord.ProgressChanged, AddressOf C_RUNxWordChanged
            AddHandler oW_BackgroudWord.RunWorkerCompleted, AddressOf C_RUNxWorkerCompleted

            Me.Cursor = Cursors.WaitCursor
            Me.ocmProcess.Enabled = False
            Me.ocmClose1.Enabled = False
            Me.ocmClearPmt.Enabled = False
            Me.ocmClose2.Enabled = False
            Me.ockSelectAll.Enabled = False

            Me.opgForm.Maximum = 100
            Me.opgForm.Value = 0

            oW_BackgroudWord.RunWorkerAsync()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Me.ocmClose1.Enabled = True
            Me.ocmClearPmt.Enabled = True
            Me.ocmClose2.Enabled = True
            Me.ockSelectAll.Enabled = True
        End Try
    End Sub

    Private Shared Function C_GETnFile(ByVal ptBchCode As String) As Integer
        aC_Detail.Clear()
        Dim nSelect As Integer = 0
        Dim tDateIns As String = Format(Now, "yyyy/MM/dd")
        Dim tTimeIns As String = Format(Now, "HH:mm:ss")
        With wUploadPmt.ogdUploadPmt
            For nRow = .Rows.Fixed To .Rows.Count - 1
                If .Item(nRow, "tItem").ToString <> "" AndAlso .Item(nRow, "nSelect") Then
                    Dim oFileDetail As New cUploadDetail
                    oFileDetail.tC_Name = .Item(nRow, "tItem")
                    oFileDetail.tC_FullName = .Item(nRow, "tPathFile")
                    oFileDetail.tC_BchCode = ptBchCode
                    oFileDetail.tC_DateIns = tDateIns
                    oFileDetail.tC_TimeIns = tTimeIns
                    oFileDetail.nC_GridRow = nRow
                    aC_Detail.Add(oFileDetail)
                    nSelect += 1
                End If
            Next nRow
        End With
        Return nSelect
    End Function

    Private Sub C_RUNxDoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        dW_StartTime = Now
        cPromotion.C_CALxProcess()
    End Sub
    Private Sub C_RUNxWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim tLogAda As String = ""
        Me.opgForm.Value = 100

        dW_EndTime = Now
        Dim dDiffTime As TimeSpan = dW_EndTime - dW_StartTime

        tLogAda = "[Start : " & Format(dW_StartTime, "HH:mm:ss") & "], "
        tLogAda &= "[End : " & Format(dW_EndTime, "HH:mm:ss") & "], "
        tLogAda &= "[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]"
        cLog.C_CALxWriteLog("wUploadPmt > Process " & tLogAda)
        Me.Cursor = Cursors.Default
        Me.ocmClose1.Enabled = True
        Me.ocmClearPmt.Enabled = True
        Me.ocmClose2.Enabled = True
        Me.ockSelectAll.Enabled = True

        oW_BackgroudWord = Nothing
        cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
    End Sub
    Private Sub C_RUNxWordChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        Me.opgForm.Value = e.ProgressPercentage
        Me.olaValStatus.Text = "[" & cPromotion.tC_File & "] : " & e.ProgressPercentage & " %"
        Dim tStatus As String = ""
        Select Case cPromotion.nC_StaUpload
            Case 1 'สำเร็จ
                tStatus = tW_Success.Split(";")(nW_Lang)
            Case 2 'ไม่สำเร็จ
                tStatus = tW_Unsuccess.Split(";")(nW_Lang)
        End Select
        With Me.ogdUploadPmt
            If tStatus <> "" Then .Item(cPromotion.nC_GridRow, "tStatus") = tStatus
        End With
    End Sub

#End Region

#Region "Clear promotion"

    Public Class cClearPmt
        Property tDateIns As String
        Property tTimeIns As String
        Property nGridRow As Integer
    End Class

    Private Sub C_GETxHisPmt()
        'SET Caption
        W_SETxCaptionGridClrPmt()

        'GET History
        C_GETxHistoryPmt()
    End Sub

    'Set Caption ogdClrPmt
    Private Sub W_SETxCaptionGridClrPmt()
        Dim nCol As Integer = 2 'Start Column 2
        For Each tCap In tW_CaptionClrPmt.Split(",")
            With Me.ogdClrPmt
                .Cols(nCol).Caption = tCap.Split(";")(nW_Lang)
            End With
            nCol += 1
        Next
    End Sub

    Private Sub C_GETxHistoryPmt()
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Try
            oSql.AppendLine("SELECT CONVERT(VARCHAR(10), FDDateIns, 111)FDDateIns,FTTimeIns")
            oSql.AppendLine("FROM " & tC_TableHis)
            oSql.AppendLine("WHERE FTLnkStaPrc = '0'")
            oSql.AppendLine("ORDER BY FDDateIns,FTTimeIns")
            Dim oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            Dim oResult = From oRow As DataRow In oDbTbl.Rows
                          Select New cClearPmt With {
                  .tDateIns = C_GETtRowItemValue(oRow, "FDDateIns") _
                , .tTimeIns = C_GETtRowItemValue(oRow, "FTTimeIns")
            }
            Dim oClrPmt As New List(Of cClearPmt)
            If oResult.Count > 0 Then
                oClrPmt = oResult.ToList
            End If
            Me.ogdClrPmt.DataSource = oResult
            Me.ogdClrPmt.Row = 0 'Default Row 0
            Me.ogdClrPmt.Rows.Count = Me.ogdUploadPmt.Rows.Fixed
            With Me.ogdClrPmt
                For nRow = 0 To oClrPmt.Count - 1
                    .Rows.Add()
                    .Item(nRow + .Rows.Fixed, "tDateIns") = oClrPmt(nRow).tDateIns
                    .Item(nRow + .Rows.Fixed, "tTimeIns") = oClrPmt(nRow).tTimeIns
                    .Item(nRow + .Rows.Fixed, "tStatus") = tW_Wait.Split(";")(nW_Lang)
                Next
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmRefreshClr_Click(sender As Object, e As EventArgs) Handles ocmRefreshClr.Click
        C_GETxHistoryPmt()
        If C_GETnTaskClr() > 1 Then
            ocmClearPmt.Enabled = True
        End If
        Me.opgStaClr.Value = 0
        Me.olaStaClrPmt.Text = ""
    End Sub

    Private Sub ocmClearPmt_Click(sender As Object, e As EventArgs) Handles ocmClearPmt.Click
        Dim nSelect As Integer = 0

        nSelect = C_GETnTaskClr()
        'nSelect = 0 /ไม่เลือกรายการ import ออกจาก Sub 
        If nSelect = 0 Then
            Exit Sub
        End If

        Try
            oW_BackgroudWordClr = New BackgroundWorker
            oW_BackgroudWordClr.WorkerReportsProgress = True
            oW_BackgroudWordClr.WorkerSupportsCancellation = True
            AddHandler oW_BackgroudWordClr.DoWork, AddressOf C_RUNxDoWorkClr
            AddHandler oW_BackgroudWordClr.ProgressChanged, AddressOf C_RUNxWordChangedClr
            AddHandler oW_BackgroudWordClr.RunWorkerCompleted, AddressOf C_RUNxWorkerCompletedClr

            Me.Cursor = Cursors.WaitCursor
            Me.ocmProcess.Enabled = False
            Me.ocmClose1.Enabled = False
            Me.ocmClearPmt.Enabled = False
            Me.ocmClose2.Enabled = False
            Me.ockSelectAll.Enabled = False

            Me.opgStaClr.Maximum = 100
            Me.opgStaClr.Value = 0

            oW_BackgroudWordClr.RunWorkerAsync()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Me.ocmClose1.Enabled = True
            Me.ocmProcess.Enabled = True
            Me.ocmClose2.Enabled = True
            Me.ockSelectAll.Enabled = True
        End Try
    End Sub

    Private Shared Function C_GETnTaskClr() As Integer
        aC_ClrPmtDetail.Clear()
        Dim nSelect As Integer = 0
        With wUploadPmt.ogdClrPmt
            For nRow = .Rows.Fixed To .Rows.Count - 1
                Dim oClrDetail As New cClearPmt
                oClrDetail.tDateIns = .Item(nRow, "tDateIns")
                oClrDetail.tTimeIns = .Item(nRow, "tTimeIns")
                oClrDetail.nGridRow = nRow
                aC_ClrPmtDetail.Add(oClrDetail)
                nSelect += 1
            Next nRow
        End With
        Return nSelect
    End Function

    Private Sub C_RUNxDoWorkClr(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        dW_StartTime = Now
        C_PRCxClearPmt()
    End Sub
    Private Sub C_RUNxWorkerCompletedClr(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim tLogAda As String = ""
        Me.opgStaClr.Value = 100

        dW_EndTime = Now
        Dim dDiffTime As TimeSpan = dW_EndTime - dW_StartTime

        tLogAda = "[Start : " & Format(dW_StartTime, "HH:mm:ss") & "], "
        tLogAda &= "[End : " & Format(dW_EndTime, "HH:mm:ss") & "], "
        tLogAda &= "[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]"
        cLog.C_CALxWriteLog("Clear Promotion > Process " & tLogAda)
        Me.Cursor = Cursors.Default
        Me.ocmClose1.Enabled = True
        Me.ocmProcess.Enabled = True
        Me.ocmClose2.Enabled = True
        Me.ockSelectAll.Enabled = True

        oW_BackgroudWordClr = Nothing
        cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
    End Sub
    Private Sub C_RUNxWordChangedClr(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        Me.opgStaClr.Value = e.ProgressPercentage
        Me.olaStaClrPmt.Text = "[" & wUploadPmt.tC_StaClrPmt & "] : " & e.ProgressPercentage & " %"
        Dim tStatus As String = ""
        Select Case wUploadPmt.nC_StaClrPmt
            Case 1 'สำเร็จ
                tStatus = tW_Success.Split(";")(nW_Lang)
            Case 2 'ไม่สำเร็จ
                tStatus = tW_Unsuccess.Split(";")(nW_Lang)
        End Select
        With Me.ogdClrPmt
            If tStatus <> "" Then .Item(wUploadPmt.nC_GridRow, "tStatus") = tStatus
        End With
    End Sub

    Private Sub C_EVNxTabChange(sender As Object, e As TabControlCancelEventArgs) Handles otaPromotion.Selecting
        If e.TabPageIndex = 1 Then
            C_GETxHistoryPmt()
        End If
    End Sub

    Private Shared Sub C_PRCxClearPmt()
        Dim oSql As New System.Text.StringBuilder
        Dim oDatabase As New cDatabaseLocal
        Dim dDateIns As New DateTime
        Dim tDateIns As String = ""

        'คำนวน Propress Bar '*CH 20-12-2014
        Dim nItem = wUploadPmt.aC_ClrPmtDetail.Count
        nItem = IIf(nItem <= 0, 1, nItem)
        Dim nValPropress = 100 / nItem
        Dim nNextVal = nValPropress

        Try
            For Each oItem In wUploadPmt.aC_ClrPmtDetail
                wUploadPmt.tC_StaClrPmt = oItem.tDateIns & " " & oItem.tTimeIns

                tDateIns = Format(Convert.ToDateTime(oItem.tDateIns), "yyyy/MM/dd")

                'Close Promotion
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("UPDATE TCNTPmtHD SET FTPmhClosed = '1'")
                oSql.AppendLine("WHERE FTPmhCode IN (")
                oSql.AppendLine("Select FTPmhCode")
                oSql.AppendLine("FROM TCNTPmtDT")
                oSql.AppendLine("WHERE FTPmdBarCode IN ( SELECT FTPmdBarCode FROM TCNTPmtDT WHERE FDDateIns = '" & tDateIns & "' AND FTTimeIns = '" & oItem.tTimeIns & "' AND FTWhoIns = 'AdaLinkPTG' )")
                oSql.AppendLine("AND FDDateIns <= '" & tDateIns & "' AND FTTimeIns < '" & oItem.tTimeIns & "' AND FTWhoIns = 'AdaLinkPTG'")
                oSql.AppendLine(")")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

                'Update process
                oSql = New System.Text.StringBuilder
                oSql.AppendLine("UPDATE TLNKUpPmtHis SET FTLnkStaPrc = '1' WHERE FDDateIns = '" & tDateIns & "' AND FTTimeIns = '" & oItem.tTimeIns & "'")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString)

                'Runing Progress  *CH 20-12-2014
                wUploadPmt.nC_StaClrPmt = 1
                wUploadPmt.nC_GridRow = oItem.nGridRow
                wUploadPmt.oW_BackgroudWordClr.ReportProgress(nValPropress)
                nValPropress += nNextVal
            Next
        Catch ex As Exception
            'Runing Progress  *CH 20-12-2014
            wUploadPmt.nC_StaClrPmt = 2
            wUploadPmt.oW_BackgroudWordClr.ReportProgress(nValPropress)
            nValPropress += nNextVal
        Finally
            oSql = Nothing
            oDatabase = Nothing
        End Try
    End Sub
#End Region

#Region "Event Close"

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose1.Click, ocmClose2.Click
        Me.Close()
    End Sub

#End Region

End Class