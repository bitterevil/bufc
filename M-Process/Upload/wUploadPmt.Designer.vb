﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wUploadPmt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wUploadPmt))
        Me.otaPromotion = New System.Windows.Forms.TabControl()
        Me.otaUpload = New System.Windows.Forms.TabPage()
        Me.ocbBranch = New System.Windows.Forms.ComboBox()
        Me.olaBch = New System.Windows.Forms.Label()
        Me.olaValStatus = New System.Windows.Forms.Label()
        Me.ockSelectAll = New System.Windows.Forms.CheckBox()
        Me.ocmRefresh = New System.Windows.Forms.Button()
        Me.ocmProcess = New System.Windows.Forms.Button()
        Me.ocmClose1 = New System.Windows.Forms.Button()
        Me.olaStaImport = New System.Windows.Forms.Label()
        Me.opgForm = New System.Windows.Forms.ProgressBar()
        Me.otbTypeFile = New System.Windows.Forms.TextBox()
        Me.ocmBrwPath = New System.Windows.Forms.Button()
        Me.otbPathFile = New System.Windows.Forms.TextBox()
        Me.olaPathUpload = New System.Windows.Forms.Label()
        Me.ogdUploadPmt = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.otaClear = New System.Windows.Forms.TabPage()
        Me.ocmClearPmt = New System.Windows.Forms.Button()
        Me.ocmClose2 = New System.Windows.Forms.Button()
        Me.olaCapTblClear = New System.Windows.Forms.Label()
        Me.ogdClrPmt = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ofdForm = New System.Windows.Forms.FolderBrowserDialog()
        Me.C1XLBook1 = New C1.C1Excel.C1XLBook()
        Me.ocmRefreshClr = New System.Windows.Forms.Button()
        Me.olaStaClr = New System.Windows.Forms.Label()
        Me.opgStaClr = New System.Windows.Forms.ProgressBar()
        Me.olaStaClrPmt = New System.Windows.Forms.Label()
        Me.otaPromotion.SuspendLayout()
        Me.otaUpload.SuspendLayout()
        CType(Me.ogdUploadPmt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.otaClear.SuspendLayout()
        CType(Me.ogdClrPmt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'otaPromotion
        '
        Me.otaPromotion.Controls.Add(Me.otaUpload)
        Me.otaPromotion.Controls.Add(Me.otaClear)
        Me.otaPromotion.Location = New System.Drawing.Point(12, 12)
        Me.otaPromotion.Name = "otaPromotion"
        Me.otaPromotion.SelectedIndex = 0
        Me.otaPromotion.Size = New System.Drawing.Size(725, 479)
        Me.otaPromotion.TabIndex = 0
        '
        'otaUpload
        '
        Me.otaUpload.Controls.Add(Me.ocbBranch)
        Me.otaUpload.Controls.Add(Me.olaBch)
        Me.otaUpload.Controls.Add(Me.olaValStatus)
        Me.otaUpload.Controls.Add(Me.ockSelectAll)
        Me.otaUpload.Controls.Add(Me.ocmRefresh)
        Me.otaUpload.Controls.Add(Me.ocmProcess)
        Me.otaUpload.Controls.Add(Me.ocmClose1)
        Me.otaUpload.Controls.Add(Me.olaStaImport)
        Me.otaUpload.Controls.Add(Me.opgForm)
        Me.otaUpload.Controls.Add(Me.otbTypeFile)
        Me.otaUpload.Controls.Add(Me.ocmBrwPath)
        Me.otaUpload.Controls.Add(Me.otbPathFile)
        Me.otaUpload.Controls.Add(Me.olaPathUpload)
        Me.otaUpload.Controls.Add(Me.ogdUploadPmt)
        Me.otaUpload.Location = New System.Drawing.Point(4, 22)
        Me.otaUpload.Name = "otaUpload"
        Me.otaUpload.Padding = New System.Windows.Forms.Padding(3)
        Me.otaUpload.Size = New System.Drawing.Size(717, 453)
        Me.otaUpload.TabIndex = 0
        Me.otaUpload.Tag = "2;อัปโหลด;Upload"
        Me.otaUpload.Text = "Upload"
        Me.otaUpload.UseVisualStyleBackColor = True
        '
        'ocbBranch
        '
        Me.ocbBranch.FormattingEnabled = True
        Me.ocbBranch.Location = New System.Drawing.Point(166, 34)
        Me.ocbBranch.Name = "ocbBranch"
        Me.ocbBranch.Size = New System.Drawing.Size(174, 21)
        Me.ocbBranch.TabIndex = 15
        '
        'olaBch
        '
        Me.olaBch.AutoSize = True
        Me.olaBch.Location = New System.Drawing.Point(6, 37)
        Me.olaBch.Name = "olaBch"
        Me.olaBch.Size = New System.Drawing.Size(56, 13)
        Me.olaBch.TabIndex = 14
        Me.olaBch.Tag = "2;สาขาที่มีผล;Branch Affect"
        Me.olaBch.Text = "สาขาที่มีผล"
        '
        'olaValStatus
        '
        Me.olaValStatus.AutoSize = True
        Me.olaValStatus.Location = New System.Drawing.Point(63, 366)
        Me.olaValStatus.Name = "olaValStatus"
        Me.olaValStatus.Size = New System.Drawing.Size(0, 13)
        Me.olaValStatus.TabIndex = 13
        '
        'ockSelectAll
        '
        Me.ockSelectAll.AutoSize = True
        Me.ockSelectAll.Checked = True
        Me.ockSelectAll.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ockSelectAll.Location = New System.Drawing.Point(36, 66)
        Me.ockSelectAll.Name = "ockSelectAll"
        Me.ockSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.ockSelectAll.TabIndex = 12
        Me.ockSelectAll.UseVisualStyleBackColor = True
        '
        'ocmRefresh
        '
        Me.ocmRefresh.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmRefresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmRefresh.Location = New System.Drawing.Point(7, 411)
        Me.ocmRefresh.Name = "ocmRefresh"
        Me.ocmRefresh.Size = New System.Drawing.Size(100, 30)
        Me.ocmRefresh.TabIndex = 9
        Me.ocmRefresh.Tag = "2;รีเฟรช;Refresh"
        Me.ocmRefresh.Text = "Refresh"
        Me.ocmRefresh.UseVisualStyleBackColor = True
        '
        'ocmProcess
        '
        Me.ocmProcess.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmProcess.Location = New System.Drawing.Point(505, 411)
        Me.ocmProcess.Name = "ocmProcess"
        Me.ocmProcess.Size = New System.Drawing.Size(100, 30)
        Me.ocmProcess.TabIndex = 10
        Me.ocmProcess.Tag = "2;ดำเนินการ;Process"
        Me.ocmProcess.Text = "Process"
        Me.ocmProcess.UseVisualStyleBackColor = True
        '
        'ocmClose1
        '
        Me.ocmClose1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmClose1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmClose1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose1.Location = New System.Drawing.Point(611, 411)
        Me.ocmClose1.Name = "ocmClose1"
        Me.ocmClose1.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose1.TabIndex = 11
        Me.ocmClose1.Tag = "2;ปิด;Close"
        Me.ocmClose1.Text = "Close"
        Me.ocmClose1.UseVisualStyleBackColor = True
        '
        'olaStaImport
        '
        Me.olaStaImport.AutoSize = True
        Me.olaStaImport.BackColor = System.Drawing.Color.Transparent
        Me.olaStaImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaStaImport.Location = New System.Drawing.Point(6, 363)
        Me.olaStaImport.Name = "olaStaImport"
        Me.olaStaImport.Size = New System.Drawing.Size(51, 16)
        Me.olaStaImport.TabIndex = 7
        Me.olaStaImport.Tag = "2;สถานะ ; Status"
        Me.olaStaImport.Text = "Status :"
        '
        'opgForm
        '
        Me.opgForm.Location = New System.Drawing.Point(7, 386)
        Me.opgForm.Name = "opgForm"
        Me.opgForm.Size = New System.Drawing.Size(704, 19)
        Me.opgForm.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.opgForm.TabIndex = 8
        '
        'otbTypeFile
        '
        Me.otbTypeFile.BackColor = System.Drawing.SystemColors.ControlLight
        Me.otbTypeFile.Enabled = False
        Me.otbTypeFile.Location = New System.Drawing.Point(626, 9)
        Me.otbTypeFile.Name = "otbTypeFile"
        Me.otbTypeFile.ReadOnly = True
        Me.otbTypeFile.Size = New System.Drawing.Size(83, 20)
        Me.otbTypeFile.TabIndex = 5
        '
        'ocmBrwPath
        '
        Me.ocmBrwPath.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmBrwPath.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ocmBrwPath.Image = CType(resources.GetObject("ocmBrwPath.Image"), System.Drawing.Image)
        Me.ocmBrwPath.Location = New System.Drawing.Point(597, 9)
        Me.ocmBrwPath.Name = "ocmBrwPath"
        Me.ocmBrwPath.Size = New System.Drawing.Size(23, 20)
        Me.ocmBrwPath.TabIndex = 4
        Me.ocmBrwPath.UseVisualStyleBackColor = True
        '
        'otbPathFile
        '
        Me.otbPathFile.Location = New System.Drawing.Point(166, 9)
        Me.otbPathFile.Name = "otbPathFile"
        Me.otbPathFile.Size = New System.Drawing.Size(425, 20)
        Me.otbPathFile.TabIndex = 3
        '
        'olaPathUpload
        '
        Me.olaPathUpload.AutoSize = True
        Me.olaPathUpload.Location = New System.Drawing.Point(6, 13)
        Me.olaPathUpload.Name = "olaPathUpload"
        Me.olaPathUpload.Size = New System.Drawing.Size(153, 13)
        Me.olaPathUpload.TabIndex = 2
        Me.olaPathUpload.Tag = "2;โฟลเดอร์ไฟล์อัปโหลดโปรโมชั่น;Folder upload promotion"
        Me.olaPathUpload.Text = "โฟลเดอร์ไฟล์อัปโหลดโปรโมชั่น"
        '
        'ogdUploadPmt
        '
        Me.ogdUploadPmt.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdUploadPmt.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdUploadPmt.AutoGenerateColumns = False
        Me.ogdUploadPmt.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdUploadPmt.ColumnInfo = resources.GetString("ogdUploadPmt.ColumnInfo")
        Me.ogdUploadPmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdUploadPmt.Location = New System.Drawing.Point(7, 61)
        Me.ogdUploadPmt.Name = "ogdUploadPmt"
        Me.ogdUploadPmt.Rows.Count = 2
        Me.ogdUploadPmt.Rows.DefaultSize = 19
        Me.ogdUploadPmt.Rows.MaxSize = 50
        Me.ogdUploadPmt.Rows.MinSize = 21
        Me.ogdUploadPmt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdUploadPmt.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdUploadPmt.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdUploadPmt.Size = New System.Drawing.Size(704, 299)
        Me.ogdUploadPmt.StyleInfo = resources.GetString("ogdUploadPmt.StyleInfo")
        Me.ogdUploadPmt.TabIndex = 1
        Me.ogdUploadPmt.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'otaClear
        '
        Me.otaClear.Controls.Add(Me.olaStaClrPmt)
        Me.otaClear.Controls.Add(Me.ocmRefreshClr)
        Me.otaClear.Controls.Add(Me.olaStaClr)
        Me.otaClear.Controls.Add(Me.opgStaClr)
        Me.otaClear.Controls.Add(Me.ocmClearPmt)
        Me.otaClear.Controls.Add(Me.ocmClose2)
        Me.otaClear.Controls.Add(Me.olaCapTblClear)
        Me.otaClear.Controls.Add(Me.ogdClrPmt)
        Me.otaClear.Location = New System.Drawing.Point(4, 22)
        Me.otaClear.Name = "otaClear"
        Me.otaClear.Padding = New System.Windows.Forms.Padding(3)
        Me.otaClear.Size = New System.Drawing.Size(717, 453)
        Me.otaClear.TabIndex = 1
        Me.otaClear.Tag = "2;ล้าง;Clear"
        Me.otaClear.Text = "Clear"
        Me.otaClear.UseVisualStyleBackColor = True
        '
        'ocmClearPmt
        '
        Me.ocmClearPmt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmClearPmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClearPmt.Location = New System.Drawing.Point(504, 411)
        Me.ocmClearPmt.Name = "ocmClearPmt"
        Me.ocmClearPmt.Size = New System.Drawing.Size(100, 30)
        Me.ocmClearPmt.TabIndex = 12
        Me.ocmClearPmt.Tag = "2;ดำเนินการ;Process"
        Me.ocmClearPmt.Text = "Process"
        Me.ocmClearPmt.UseVisualStyleBackColor = True
        '
        'ocmClose2
        '
        Me.ocmClose2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmClose2.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmClose2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose2.Location = New System.Drawing.Point(610, 411)
        Me.ocmClose2.Name = "ocmClose2"
        Me.ocmClose2.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose2.TabIndex = 13
        Me.ocmClose2.Tag = "2;ปิด;Close"
        Me.ocmClose2.Text = "Close"
        Me.ocmClose2.UseVisualStyleBackColor = True
        '
        'olaCapTblClear
        '
        Me.olaCapTblClear.AutoSize = True
        Me.olaCapTblClear.Location = New System.Drawing.Point(6, 10)
        Me.olaCapTblClear.Name = "olaCapTblClear"
        Me.olaCapTblClear.Size = New System.Drawing.Size(146, 13)
        Me.olaCapTblClear.TabIndex = 3
        Me.olaCapTblClear.Tag = "2;ล้างโปรโมชั่นตามวันที่นำเข้า;Clear promotion by date insert"
        Me.olaCapTblClear.Text = "Clear promotion by date insert"
        '
        'ogdClrPmt
        '
        Me.ogdClrPmt.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdClrPmt.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdClrPmt.AutoGenerateColumns = False
        Me.ogdClrPmt.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdClrPmt.ColumnInfo = resources.GetString("ogdClrPmt.ColumnInfo")
        Me.ogdClrPmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdClrPmt.Location = New System.Drawing.Point(6, 31)
        Me.ogdClrPmt.Name = "ogdClrPmt"
        Me.ogdClrPmt.Rows.Count = 2
        Me.ogdClrPmt.Rows.DefaultSize = 19
        Me.ogdClrPmt.Rows.MaxSize = 50
        Me.ogdClrPmt.Rows.MinSize = 21
        Me.ogdClrPmt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdClrPmt.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdClrPmt.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdClrPmt.Size = New System.Drawing.Size(704, 329)
        Me.ogdClrPmt.StyleInfo = resources.GetString("ogdClrPmt.StyleInfo")
        Me.ogdClrPmt.TabIndex = 2
        Me.ogdClrPmt.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ocmRefreshClr
        '
        Me.ocmRefreshClr.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmRefreshClr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmRefreshClr.Location = New System.Drawing.Point(7, 411)
        Me.ocmRefreshClr.Name = "ocmRefreshClr"
        Me.ocmRefreshClr.Size = New System.Drawing.Size(100, 30)
        Me.ocmRefreshClr.TabIndex = 16
        Me.ocmRefreshClr.Tag = "2;รีเฟรช;Refresh"
        Me.ocmRefreshClr.Text = "Refresh"
        Me.ocmRefreshClr.UseVisualStyleBackColor = True
        '
        'olaStaClr
        '
        Me.olaStaClr.AutoSize = True
        Me.olaStaClr.BackColor = System.Drawing.Color.Transparent
        Me.olaStaClr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaStaClr.Location = New System.Drawing.Point(6, 363)
        Me.olaStaClr.Name = "olaStaClr"
        Me.olaStaClr.Size = New System.Drawing.Size(51, 16)
        Me.olaStaClr.TabIndex = 14
        Me.olaStaClr.Tag = "2;สถานะ ; Status"
        Me.olaStaClr.Text = "Status :"
        '
        'opgStaClr
        '
        Me.opgStaClr.Location = New System.Drawing.Point(7, 386)
        Me.opgStaClr.Name = "opgStaClr"
        Me.opgStaClr.Size = New System.Drawing.Size(704, 19)
        Me.opgStaClr.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.opgStaClr.TabIndex = 15
        '
        'olaStaClrPmt
        '
        Me.olaStaClrPmt.AutoSize = True
        Me.olaStaClrPmt.Location = New System.Drawing.Point(63, 365)
        Me.olaStaClrPmt.Name = "olaStaClrPmt"
        Me.olaStaClrPmt.Size = New System.Drawing.Size(0, 13)
        Me.olaStaClrPmt.TabIndex = 17
        '
        'wUploadPmt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(747, 503)
        Me.Controls.Add(Me.otaPromotion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wUploadPmt"
        Me.Tag = "2;อัปโหลดโปรโมชั่นจาก Excel;Upload promotion from excel"
        Me.Text = "Upload promotion from excel"
        Me.otaPromotion.ResumeLayout(False)
        Me.otaUpload.ResumeLayout(False)
        Me.otaUpload.PerformLayout()
        CType(Me.ogdUploadPmt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.otaClear.ResumeLayout(False)
        Me.otaClear.PerformLayout()
        CType(Me.ogdClrPmt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents otaPromotion As System.Windows.Forms.TabControl
    Friend WithEvents otaUpload As System.Windows.Forms.TabPage
    Friend WithEvents otaClear As System.Windows.Forms.TabPage
    Friend WithEvents ogdUploadPmt As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents otbPathFile As System.Windows.Forms.TextBox
    Friend WithEvents olaPathUpload As System.Windows.Forms.Label
    Friend WithEvents ocmBrwPath As System.Windows.Forms.Button
    Friend WithEvents otbTypeFile As System.Windows.Forms.TextBox
    Friend WithEvents ocmRefresh As System.Windows.Forms.Button
    Friend WithEvents ocmProcess As System.Windows.Forms.Button
    Friend WithEvents ocmClose1 As System.Windows.Forms.Button
    Friend WithEvents olaStaImport As System.Windows.Forms.Label
    Friend WithEvents opgForm As System.Windows.Forms.ProgressBar
    Friend WithEvents ogdClrPmt As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents olaCapTblClear As System.Windows.Forms.Label
    Friend WithEvents ocmClearPmt As System.Windows.Forms.Button
    Friend WithEvents ocmClose2 As System.Windows.Forms.Button
    Friend WithEvents ockSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents ofdForm As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents olaValStatus As System.Windows.Forms.Label
    Friend WithEvents C1XLBook1 As C1.C1Excel.C1XLBook
    Friend WithEvents ocbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents olaBch As System.Windows.Forms.Label
    Friend WithEvents ocmRefreshClr As System.Windows.Forms.Button
    Friend WithEvents olaStaClr As System.Windows.Forms.Label
    Friend WithEvents opgStaClr As System.Windows.Forms.ProgressBar
    Friend WithEvents olaStaClrPmt As System.Windows.Forms.Label
End Class
