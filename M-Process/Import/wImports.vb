﻿Imports System.IO
Imports C1.Win.C1FlexGrid
Imports System.Threading
Imports System.ComponentModel

Public Class wImports

#Region "Variable-Private"
    'สถานะเปิดฟอร์ม
    Private bW_FrmLoad As Boolean = False
    'Caption ogdForm
    Private tW_CaptionImport As String = "List Import,List Import,Count,Count"
    Private tW_CaptionImportThai As String = "รายการนำเข้า,รายการนำเข้า,จำนวนไฟล์,จำนวนไฟล์"
    'Caption ogdDetail
    Private tW_CaptionDetail As String = "File Name,Date,Size,Status,Status"
    Private tW_CaptionDetailThai As String = "ชื่อไฟล์,วันที่,ขนาดไฟล์,สถานะ,สถานะ"

    Public Shared oW_BackgroudWord As BackgroundWorker '*CH 19-12-2014
    Private dW_StartTime As DateTime
    Private dW_EndTime As DateTime
    Private bW_StaPdt As Boolean = False
    Private Const tW_Success As String = "สำเร็จ;Success"
    Private Const tW_Unsuccess As String = "ไม่สำเร็จ;Unsuccessful"
#End Region

#Region "Sub And Function"
    'Load รายการ Import เข้า ogdForm & ogdDetail
    Private Sub W_SETxListImport()
        With Me.ogdForm
            Dim oItemImport = cImportTemplate.C_GETaItemImport
            .DataSource = oItemImport
            .Row = 0 'Default Row 0
            Me.W_SETxCaptionGridImport()
            'Set Seq No
            For nRow As Integer = 1 To .Rows.Count - 1
                .Item(nRow, 0) = nRow
            Next

            If AdaConfig.cConfig.oApplication.nLanguage = 1 Then '2:thai
                .Cols("tItem").Visible = False
                .Cols("tItemThai").Visible = True
            End If

            Dim oCs As CellStyle = .Styles.Add("ReadOnly")
            oCs.DataType = Type.GetType("System.String")
            oCs.ForeColor = Color.Red
            'cs.Font = New Font(Font, FontStyle.Regular)

            'assign styles to editable cells
            Dim oRgColList As CellRange
            Dim oRgColCount As CellRange
            Dim nLoop As Integer = 1
            For Each oItemRow In oItemImport
                If oItemRow.nCount = 0 Then
                    If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '2:thai
                        oRgColList = .GetCellRange(nLoop, 2)
                    Else
                        oRgColList = .GetCellRange(nLoop, 3)
                    End If
                    oRgColList.Style = .Styles("ReadOnly")
                    oRgColCount = .GetCellRange(nLoop, 4)
                    oRgColCount.Style = .Styles("ReadOnly")
                End If
                nLoop += 1
            Next

            'Update CheckBok
            Select Case oItemImport.Where(Function(c) c.nCount > 0 And c.nSelect = 1).Count
                Case 0
                    Me.ockImportAll.CheckState = CheckState.Unchecked
                Case oItemImport.Where(Function(c) c.nCount > 0).Count
                    Me.ockImportAll.CheckState = CheckState.Checked
                Case Else
                    Me.ockImportAll.CheckState = CheckState.Indeterminate
            End Select

        End With

    End Sub
    'Set Caption ogdForm
    Private Sub W_SETxCaptionGridImport()
        Dim tCapImport As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '1:thai
            tCapImport = tW_CaptionImport
        Else
            tCapImport = tW_CaptionImportThai
        End If

        Dim nCol As Integer = 2 'Start Column 2
        For Each tCap In tCapImport.Split(",")
            With Me.ogdForm
                .Cols(nCol).Caption = tCap
            End With
            nCol += 1
        Next
    End Sub

    'Set Caption ogdDetail
    Private Sub W_SETxCaptionGridDetail()
        Dim tCapDetail As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '2:thai
            tCapDetail = tW_CaptionDetail
        Else
            tCapDetail = tW_CaptionDetailThai
        End If

        Dim nCol As Integer = 2 'Start Column 2
        For Each tCap In tCapDetail.Split(",")
            With Me.ogdDetail
                .Cols(nCol).Caption = tCap
            End With
            nCol += 1
        Next
    End Sub
    'Set Style ogdDetail
    Private Sub W_SETxGridDetailStyles()
        'Set Seq No
        For nRow As Integer = 1 To Me.ogdDetail.Rows.Count - 1
            Me.ogdDetail.Item(nRow, 0) = nRow
        Next

        'Set styles
        With Me.ogdDetail
            Dim oCs As CellStyle = .Styles.Add("ReadOnly")
            oCs.DataType = Type.GetType("System.String")
            oCs.ForeColor = Color.Red

            'assign styles to editable cells
            Dim oRgColFileName As CellRange
            Dim oRgColDate As CellRange
            Dim oRgColSize As CellRange
            Dim oRgColStatus As CellRange
            Dim nLoop As Integer = 1
            For Each oItemRow In CType(Me.ogdDetail.DataSource, List(Of cBaseImport.cImportDetail))
                If oItemRow.nC_Status < 0 Then
                    oRgColFileName = .GetCellRange(nLoop, 2)
                    oRgColFileName.Style = .Styles("ReadOnly")
                    oRgColDate = .GetCellRange(nLoop, 3)
                    oRgColDate.Style = .Styles("ReadOnly")
                    oRgColSize = .GetCellRange(nLoop, 4)
                    oRgColSize.Style = .Styles("ReadOnly")
                    oRgColStatus = .GetCellRange(nLoop, 6)
                    oRgColStatus.Style = .Styles("ReadOnly")
                End If
                nLoop += 1
            Next
            If bW_StaPdt Then
                .Cols(1).Visible = False
                .Cols(2).Width = 320
            Else
                Me.ogdDetail.Cols(1).Visible = True
                .Cols(2).Width = 300
            End If
        End With
    End Sub

    'Call Refresh ogdDetail
    Private Sub W_CALxDetailRefresh()
        'Refresh ข้อมูลใน ogdDetail ทั้งหมด
        With Me.ogdForm
            If .RowSel > -1 Then
                Select Case .Item(.RowSel, .Cols("tCode").Index)
                    Case cImportTemplate.cTableCode.TCNMPdt
                        Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdt.aC_Detail
                    'Case cImportTemplate.cTableCode.TCNMPdtBrand
                    '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdtBrand.aC_Detail
                    'Case cImportTemplate.cTableCode.TCNMPdtUnit
                    '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdtUnit.aC_Detail
                    Case cImportTemplate.cTableCode.TCNMPdtGrp
                        Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdtGrp.aC_Detail
                        'Case cImportTemplate.cTableCode.TCNTCTractor
                        '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNTCTractor.aC_Detail
                        'Case cImportTemplate.cTableCode.TCNMSpl
                        '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMSpl.aC_Detail
                    Case cImportTemplate.cTableCode.TCNTPmt
                        Me.ogdDetail.DataSource = cImportTemplate.oC_TCNTPmt.aC_Detail
                End Select
            End If
        End With
    End Sub

#End Region

#Region "Event"

#Region "Form"

    Private Sub wImports_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'เคลียร์รายการ Import
        cImportTemplate.C_CALxClear()
        Me.Dispose()
    End Sub

    Private Sub wImports_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'แสดงภาษาของฟอร์ม

        cCNSP.SP_FrmSetCapControl(Me)
        'ตรวจสอบติดต่อฐานข้อมูล
        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
            Me.Close()
        End If
        'ตรวจสอบรหัส User
        'If App.oConfigSetting.tUsrCode.Length = 0 Then
        '    cCNSP.SP_MSGnShowing(cCNMS.tMS_CN011, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        '    'ไม่พบให้กำหนด User ในหน้าจอ wSetting
        '    Dim oFrmSetting As New wSetting()
        '    oFrmSetting.bW_Lock = True
        '    oFrmSetting.nW_IndexMenu = 1 'Fix
        '    oFrmSetting.ShowDialog()
        '    oFrmSetting.Dispose()
        '    Me.Close()
        '    Exit Sub
        'End If
        'Load รายการ Import
        Me.W_SETxListImport() 'Load List Import
        Me.bW_FrmLoad = True
        'Fix เลือกรายการแรก
        Me.ogdForm.Row = 1
        ''*TON 59-03-14 -----------------------------------
        'Dim oDatabase As New cDatabaseLocal
        'Dim tSql As String
        'tSql = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'TTmpChkAjp') AND type in (N'U')) "
        'tSql &= " DROP TABLE TTmpChkAjp "
        'tSql &= "CREATE TABLE TTmpChkAjp( "
        'tSql &= "FTTextName varchar(100) NULL, "
        'tSql &= "FTIpdStkCode varchar(20) NULL, "
        'tSql &= "FTPunCode varchar(5) NULL "
        'tSql &= ");"
        'oDatabase.C_CALnExecuteNonQuery(tSql)
        ''------------------------------------------------
    End Sub

#End Region

#Region "Grid"

    Private Sub ogdForm_AfterEdit(sender As Object, e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdForm.AfterEdit
        If Me.bW_FrmLoad = False Then Exit Sub
        Dim oItemImport = cImportTemplate.C_GETaItemImport
        If oItemImport Is Nothing Then Exit Sub
        With Me.ogdForm
            'Update CheckBok
            Select Case oItemImport.Where(Function(c) c.nCount > 0 And c.nSelect = 1).Count
                Case oItemImport.Where(Function(c) c.nCount > 0).Count
                    Me.ockImportAll.CheckState = CheckState.Checked
                Case 0
                    Me.ockImportAll.CheckState = CheckState.Unchecked
                Case Else
                    Me.ockImportAll.CheckState = CheckState.Indeterminate
            End Select

            Dim nSelect As Integer = .Item(e.Row, .Cols("nSelect").Index)
            'Update ogdDetail
            Select Case .Item(e.Row, .Cols("tCode").Index)
                Case cImportTemplate.cTableCode.TCNMPdt
                    For Each oItem In cImportTemplate.oC_TCNMPdt.aC_Detail.Where(Function(c) c.nC_Status = 0)
                        oItem.nC_Select = nSelect
                    Next
                    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdt.aC_Detail

                'Case cImportTemplate.cTableCode.TCNMPdtBrand
                '    For Each oItem In cImportTemplate.oC_TCNMPdtBrand.aC_Detail.Where(Function(c) c.nC_Status = 0)
                '        oItem.nC_Select = nSelect
                '    Next
                '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdtBrand.aC_Detail

                'Case cImportTemplate.cTableCode.TCNMPdtUnit
                '    For Each oItem In cImportTemplate.oC_TCNMPdtUnit.aC_Detail.Where(Function(c) c.nC_Status = 0)
                '        oItem.nC_Select = nSelect
                '    Next
                '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdtUnit.aC_Detail

                Case cImportTemplate.cTableCode.TCNMPdtGrp
                    For Each oItem In cImportTemplate.oC_TCNMPdtGrp.aC_Detail.Where(Function(c) c.nC_Status = 0)
                        oItem.nC_Select = nSelect
                    Next
                    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdtGrp.aC_Detail
                    'Case cImportTemplate.cTableCode.TCNTCTractor
                    '    For Each oItem In cImportTemplate.oC_TCNTCTractor.aC_Detail.Where(Function(c) c.nC_Status = 0)
                    '        oItem.nC_Select = nSelect
                    '    Next
                    '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNTCTractor.aC_Detail

                    'Case cImportTemplate.cTableCode.TCNMSpl
                    '    For Each oItem In cImportTemplate.oC_TCNMSpl.aC_Detail.Where(Function(c) c.nC_Status = 0)
                    '        oItem.nC_Select = nSelect
                    '    Next
                    '    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMSpl.aC_Detail


                    ' .Item(e.Row, .Cols("tSelectCount").Index) = cImportTemplate.oW_TCNTPmt.tW_SelCount
                Case cImportTemplate.cTableCode.TCNTPmt 'Promotion Price '*CH 27-11-2014 
                    For Each oItem In cImportTemplate.oC_TCNTPmt.aC_Detail.Where(Function(c) c.nC_Status = 0)
                        oItem.nC_Select = nSelect
                    Next
                    Me.ogdDetail.DataSource = cImportTemplate.oC_TCNTPmt.aC_Detail
            End Select
        End With

        If Me.ogdDetail.DataSource Is Nothing Then
            Me.ogdDetail.Rows.Count = 1
        Else
            Me.W_SETxGridDetailStyles()
        End If

    End Sub

    Private Sub ogdForm_BeforeEdit(sender As Object, e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdForm.BeforeEdit
        If Me.bW_FrmLoad = False Then Exit Sub
        With Me.ogdForm
            If e.Row > 0 Then
                Select Case .Item(e.Row, .Cols("nCount").Index)
                    Case 0
                        e.Cancel = True
                    Case Else
                        If cImportTemplate.bC_Status = True Then
                            e.Cancel = True
                        End If
                End Select
            End If
        End With
    End Sub

    Private Sub ogdForm_SelChange(sender As Object, e As System.EventArgs) Handles ogdForm.SelChange
        If Me.bW_FrmLoad = False Then Exit Sub
        bW_StaPdt = False
        With Me.ogdForm
            If .RowSel > -1 Then
                Dim nSelect As Integer = .Item(.RowSel, .Cols("nSelect").Index)
                Select Case .Item(.RowSel, .Cols("tCode").Index)
                    Case cImportTemplate.cTableCode.TCNMPdt
                        For Each oItem In cImportTemplate.oC_TCNMPdt.aC_Detail.Where(Function(c) c.nC_Status = 0)
                            oItem.nC_Select = nSelect
                        Next
                        Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdt.aC_Detail
                    Case cImportTemplate.cTableCode.TCNMPdtGrp
                        For Each oItem In cImportTemplate.oC_TCNMPdtGrp.aC_Detail.Where(Function(c) c.nC_Status = 0)
                            oItem.nC_Select = nSelect
                        Next
                        Me.ogdDetail.DataSource = cImportTemplate.oC_TCNMPdtGrp.aC_Detail
                    Case cImportTemplate.cTableCode.TCNTPdtAjp 'Ajp 
                        For Each oItem In cImportTemplate.oC_TCNTPdtAjp.aC_Detail.Where(Function(c) c.nC_Status = 0)
                            oItem.nC_Select = nSelect
                        Next
                        Me.ogdDetail.DataSource = cImportTemplate.oC_TCNTPdtAjp.aC_Detail
                    Case cImportTemplate.cTableCode.TCNTPdtTnf 'Tnf in
                        For Each oItem In cImportTemplate.oC_TCNTPdtTnf.aC_Detail.Where(Function(c) c.nC_Status = 0)
                            oItem.nC_Select = nSelect
                        Next
                        Me.ogdDetail.DataSource = cImportTemplate.oC_TCNTPdtTnf.aC_Detail
                End Select

                If Me.ogdDetail.DataSource Is Nothing Then
                    Me.ogdDetail.Rows.Count = 1
                Else
                    Me.W_SETxGridDetailStyles()
                End If

                Me.W_SETxCaptionGridDetail()
            End If
        End With

    End Sub

    Private Sub ogdDetail_AfterEdit(sender As Object, e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdDetail.AfterEdit
        If Me.bW_FrmLoad = False Then Exit Sub
        With Me.ogdDetail
            Dim tDate As String = .Item(e.Row, .Cols("tC_Date").Index)
            Dim nSelect As String = .Item(e.Row, .Cols("nC_Select").Index)
            'Update ogdDetail
            Select Case Me.ogdForm.Item(Me.ogdForm.RowSel, Me.ogdForm.Cols("tCode").Index)

                'Case cImportTemplate.cTableCode.TCNTPdtAjp
                '    For Each oItem In cImportTemplate.oC_TCNTPdtAjp.aC_Detail.Where(Function(c) c.tC_Date = tDate)
                '        oItem.nC_Select = nSelect
                '    Next
                '    .DataSource = cImportTemplate.oC_TCNTPdtAjp.aC_Detail

                'Case cImportTemplate.cTableCode.TCNMSpl
                '    For Each oItem In cImportTemplate.oC_TCNMSpl.aC_Detail.Where(Function(c) c.tC_Date = tDate)
                '        oItem.nC_Select = nSelect
                '    Next
                '    .DataSource = cImportTemplate.oC_TCNMSpl.aC_Detail

                Case cImportTemplate.cTableCode.TCNMPdt
                    For Each oItem In cImportTemplate.oC_TCNMPdt.aC_Detail.Where(Function(c) c.tC_Date = tDate)
                        oItem.nC_Select = nSelect
                    Next
                    .DataSource = cImportTemplate.oC_TCNMPdt.aC_Detail

                'Case cImportTemplate.cTableCode.TCNMPdtBrand
                '    For Each oItem In cImportTemplate.oC_TCNMPdtBrand.aC_Detail.Where(Function(c) c.tC_Date = tDate)
                '        oItem.nC_Select = nSelect
                '    Next
                '    .DataSource = cImportTemplate.oC_TCNMPdtBrand.aC_Detail

                'Case cImportTemplate.cTableCode.TCNMPdtUnit
                '    For Each oItem In cImportTemplate.oC_TCNMPdtUnit.aC_Detail.Where(Function(c) c.tC_Date = tDate)
                '        oItem.nC_Select = nSelect
                '    Next
                '    .DataSource = cImportTemplate.oC_TCNMPdtUnit.aC_Detail

                Case cImportTemplate.cTableCode.TCNMPdtGrp
                    For Each oItem In cImportTemplate.oC_TCNMPdtGrp.aC_Detail.Where(Function(c) c.tC_Date = tDate)
                        oItem.nC_Select = nSelect
                    Next
                    .DataSource = cImportTemplate.oC_TCNMPdtGrp.aC_Detail

                    'Case cImportTemplate.cTableCode.TCNTCTractor
                    '    For Each oItem In cImportTemplate.oC_TCNTCTractor.aC_Detail.Where(Function(c) c.tC_Date = tDate)
                    '        oItem.nC_Select = nSelect
                    '    Next
                    '    .DataSource = cImportTemplate.oC_TCNTCTractor.aC_Detail

                Case Else

            End Select
            'cImportTemplate.W_CALxReCount()
            Me.ogdForm.Refresh()
        End With

    End Sub

    Private Sub ogdDetail_BeforeEdit(sender As Object, e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdDetail.BeforeEdit
        If Me.bW_FrmLoad = False Then Exit Sub

        'สถานะ Process แล้วให้ e.Cancel = True
        If cImportTemplate.bC_Status = True Then
            e.Cancel = True
            Exit Sub
        End If

        With Me.ogdDetail
            If e.Row > 0 Then
                Select Case .Item(e.Row, .Cols("nC_Status").Index)
                    Case 0
                        'ให้ผ่าน nW_Status=0 กรณี File ครบ
                    Case Else
                        'ไม่ผ่านกรณี File ไม่ครบ เช่น มี HD ไม่มี DT
                        e.Cancel = True
                End Select
            End If
        End With
    End Sub

#End Region

#Region "Buttom"

    Private Sub ocmProcess_Click(sender As System.Object, e As System.EventArgs) Handles ocmProcess.Click
        Dim nSelect As Integer = 0
        Dim nLoop As Integer = 1
        With Me.ogdForm
            For nLoop = 1 To .Rows.Count - 1
                If .Item(nLoop, .Cols("nSelect").Index) = 1 Then
                    nSelect += 1
                End If
            Next
        End With

        'nSelect = 0 /ไม่เลือกรายการ import ออกจาก Sub 
        If nSelect = 0 Then
            Exit Sub
        End If

        cCNVB.nVB_StaImportFailed = 0  ' Reset Value Status Export faild 

        Try
            '*CH 19-12-2014
            oW_BackgroudWord = New BackgroundWorker
            oW_BackgroudWord.WorkerReportsProgress = True
            oW_BackgroudWord.WorkerSupportsCancellation = True
            AddHandler oW_BackgroudWord.DoWork, AddressOf C_RUNxDoWork
            AddHandler oW_BackgroudWord.ProgressChanged, AddressOf C_RUNxWordChanged
            AddHandler oW_BackgroudWord.RunWorkerCompleted, AddressOf C_RUNxWorkerCompleted

            Me.Cursor = Cursors.WaitCursor
            Me.ocmProcess.Enabled = False
            Me.ocmClose.Enabled = False
            Me.ockImportAll.Enabled = False
            Me.opgForm.Maximum = 100
            Me.opgForm.Value = 0

            oW_BackgroudWord.RunWorkerAsync()
        Catch ex As Exception
        Finally
            'Me.Cursor = Cursors.Default
            'Me.ocmClose.Enabled = True
            ''Refresh ข้อมูลใน ogdDetail
            'Me.W_CALxDetailRefresh()
            'cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
            ''cCNSP.SP_MSGnShowing("", cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        End Try
    End Sub

    Private Sub C_RUNxDoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        dW_StartTime = Now
        cImportTemplate.oC_Progress = Me.opgForm
        cCNVB.nVB_ImportPdt = 1
        cCNVB.nVB_ImportAjp = 1
        cCNVB.nVB_ImportTnf = 1
        cImportTemplate.C_CALxProcessAll()

    End Sub
    Private Sub C_RUNxWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim tLogAda As String = ""
        Me.opgForm.Value = 100

        dW_EndTime = Now
        Dim dDiffTime As TimeSpan = dW_EndTime - dW_StartTime

        'tLogAda = "[Start : " & Format(dW_StartTime, "HH:mm:ss") & "], "
        'tLogAda &= "[End : " & Format(dW_EndTime, "HH:mm:ss") & "], "
        'tLogAda &= "[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]"
        'cLog.C_CALxWriteLog("wImports > Process " & tLogAda)
        Me.Cursor = Cursors.Default
        Me.ocmClose.Enabled = True
        'Refresh ข้อมูลใน ogdDetail
        'Me.W_CALxDetailRefresh()

        oW_BackgroudWord = Nothing
        If cCNVB.nVB_StaImportFailed = 1 Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN121, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        Else
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        End If




    End Sub
    Private Sub C_RUNxWordChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        Try
            Me.opgForm.Value = e.ProgressPercentage
            Me.olaValStaImp.Text = cImportTemplate.tC_Status & cImportTemplate.tC_File & " : " & e.ProgressPercentage & " %"
            Dim nLang As Integer = 0
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLang = 1
            Dim tStatus As String = ""
            Select Case cBaseImport.nC_StaUpload
                Case 1 'สำเร็จ
                    tStatus = tW_Success.Split(";")(nLang)
                Case 2 'ไม่สำเร็จ
                    tStatus = tW_Unsuccess.Split(";")(nLang)
            End Select
            With Me.ogdDetail
                If .Rows.Count > 1 AndAlso .Rows.Count >= cBaseImport.nC_GridRow Then
                    If tStatus <> "" Then .Item(cBaseImport.nC_GridRow, "tC_Status") = tStatus
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ocmRefresh_Click(sender As System.Object, e As System.EventArgs) Handles ocmRefresh.Click
        '*EYE 59-03-08
        'Dim oDatabase As New cDatabaseLocal
        'Dim tSql As String = ""
        'tSql = "IF  EXISTS(select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TLNKPdt' AND COLUMN_NAME = 'FTPbnCode') "
        'tSql &= "BEGIN "
        'tSql &= "DROP INDEX TLNKPdt.IND_TLNKPdt_FTPbnCode; "
        'tSql &= "ALTER TABLE TLNKPdt DROP COLUMN FTPbnCode; "
        'tSql &= "Alter Table TLNKPdt ADD FTPbnCode VARCHAR(200); "
        'tSql &= "CREATE NONCLUSTERED INDEX IND_TLNKPdt_FTPbnCode ON TLNKPdt (FTPbnCode); "
        'tSql &= "End "
        'oDatabase.C_CALnExecuteNonQuery(tSql)

        Me.opgForm.Value = 0
        Me.ocmProcess.Enabled = True
        Me.ockImportAll.Enabled = True
        'เคลียร์รายการ Import
        cImportTemplate.C_CALxClear()
        'Load รายการ Import ใหม่
        Me.wImports_Load(Me, New System.EventArgs)
        Me.olaValStaImp.Text = "0 %"
    End Sub

#End Region

#Region "CheckBox"

    Private Sub ockImportAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ockImportAll.CheckedChanged
        If Me.bW_FrmLoad = False Then Exit Sub

        Dim oItemImport = cImportTemplate.C_GETaItemImport
        Dim nSelect As Integer = oItemImport.Where(Function(c) c.nSelect = 1 Or c.nCount > 0).Count
        If nSelect = 0 Then
            'จำนวนไฟล์ 0 ทั้งหมด ให้ ockImportAll=false
            Me.ockImportAll.Checked = False
            Exit Sub
        End If

        Select Case Me.ockImportAll.CheckState
            Case CheckState.Checked
                'ockImportAll=True เลือกทุกรายการที่ จำนวนมากว่า 0
                For Each oItem In oItemImport.Where(Function(c) c.nCount > 0)
                    oItem.nSelect = 1
                Next
            Case CheckState.Unchecked
                'ockImportAll=False ไม่เลือกทุกรายการที่ จำนวนมากว่า 0
                For Each oItem In oItemImport.Where(Function(c) c.nCount > 0)
                    oItem.nSelect = 0
                Next
            Case CheckState.Indeterminate
        End Select

        If Me.ockImportAll.CheckState = CheckState.Checked Or Me.ockImportAll.CheckState = CheckState.Unchecked Then
            Dim nCheck As Integer = 0
            If Me.ockImportAll.CheckState = CheckState.Checked Then
                nCheck = 1
            End If
            'Loop Update nW_Select ของ ogdDetail ทั้งหมด
            For Each oItem In oItemImport.Where(Function(c) c.nCount > 0)
                Select Case oItem.tCode
                    Case cImportTemplate.cTableCode.TCNMPdt
                        For Each oItemDT In cImportTemplate.oC_TCNMPdt.aC_Detail.Where(Function(c) c.nC_Status = 0)
                            oItemDT.nC_Select = nCheck
                        Next
                    Case cImportTemplate.cTableCode.TCNMPdtGrp
                        For Each oItemDT In cImportTemplate.oC_TCNMPdtGrp.aC_Detail.Where(Function(c) c.nC_Status = 0)
                            oItemDT.nC_Select = nCheck
                        Next
                    Case cImportTemplate.cTableCode.TCNTPdtAjp 'Promotion Price '*CH 27-11-2014 
                        For Each oItemDT In cImportTemplate.oC_TCNTPdtAjp.aC_Detail.Where(Function(c) c.nC_Select = 0)
                            oItemDT.nC_Select = nCheck
                        Next
                    Case cImportTemplate.cTableCode.TCNTPdtTnf 'Tnf Pdt In '*CH 27-11-2014 
                        For Each oItemDT In cImportTemplate.oC_TCNTPdtTnf.aC_Detail.Where(Function(c) c.nC_Select = 0)
                            oItemDT.nC_Select = nCheck
                        Next
                End Select
            Next
        End If

        'Refresh ข้อมูลใน ogdDetail
        Me.W_CALxDetailRefresh()

        'Refresh ข้อมูลใน ogdForm
        Me.ogdForm.Refresh()
        'Set Style ogdDetail ใหม่
        Me.W_SETxGridDetailStyles()
    End Sub

#End Region

#End Region

End Class

'.Rows.Fixed = 2
'.Item(0, 0) = " "
'.Item(1, 0) = " "
'.Item(0, 1) = "Item"
'.Item(1, 1) = "Item"
'.Item(0, 2) = "File"
'.Item(1, 2) = "File"
'.Item(0, 3) = "Date"
'.Item(1, 3) = "Date"
'.Item(0, 4) = "Status"
'.Item(1, 4) = "Status"

'.Item(0, 5) = "Count"
'.Item(1, 5) = "Count"
'.Item(0, 6) = "Success"
'.Item(1, 6) = "Insert"
'.Item(0, 7) = "Success"
'.Item(1, 7) = "Update"
'.Item(0, 8) = "Failure"
'.Item(1, 8) = "Failure"
'.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free
'แถวแรก
'.Rows(0).AllowMerging = True

'.Cols(0).AllowMerging = True
'.Cols(2).AllowMerging = True
'.Cols(4).AllowMerging = True
'.Cols(5).AllowMerging = True