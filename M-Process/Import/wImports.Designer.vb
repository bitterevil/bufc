﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wImports
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wImports))
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.olaValStaImp = New System.Windows.Forms.Label()
        Me.ockImportAll = New System.Windows.Forms.CheckBox()
        Me.ogdForm = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ogdDetail = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ocmRefresh = New System.Windows.Forms.Button()
        Me.ocmProcess = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.olaStaImport = New System.Windows.Forms.Label()
        Me.opgForm = New System.Windows.Forms.ProgressBar()
        Me.opnForm.SuspendLayout()
        CType(Me.ogdForm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ogdDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnForm
        '
        Me.opnForm.Controls.Add(Me.olaValStaImp)
        Me.opnForm.Controls.Add(Me.ockImportAll)
        Me.opnForm.Controls.Add(Me.ogdForm)
        Me.opnForm.Controls.Add(Me.ogdDetail)
        Me.opnForm.Controls.Add(Me.ocmRefresh)
        Me.opnForm.Controls.Add(Me.ocmProcess)
        Me.opnForm.Controls.Add(Me.ocmClose)
        Me.opnForm.Controls.Add(Me.olaStaImport)
        Me.opnForm.Controls.Add(Me.opgForm)
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(0, 0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(727, 503)
        Me.opnForm.TabIndex = 1
        '
        'olaValStaImp
        '
        Me.olaValStaImp.AutoSize = True
        Me.olaValStaImp.Location = New System.Drawing.Point(66, 403)
        Me.olaValStaImp.Name = "olaValStaImp"
        Me.olaValStaImp.Size = New System.Drawing.Size(0, 16)
        Me.olaValStaImp.TabIndex = 8
        '
        'ockImportAll
        '
        Me.ockImportAll.AutoSize = True
        Me.ockImportAll.BackColor = System.Drawing.SystemColors.Control
        Me.ockImportAll.Location = New System.Drawing.Point(41, 16)
        Me.ockImportAll.Name = "ockImportAll"
        Me.ockImportAll.Size = New System.Drawing.Size(15, 14)
        Me.ockImportAll.TabIndex = 1
        Me.ockImportAll.UseVisualStyleBackColor = False
        '
        'ogdForm
        '
        Me.ogdForm.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdForm.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdForm.AutoGenerateColumns = False
        Me.ogdForm.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdForm.ColumnInfo = resources.GetString("ogdForm.ColumnInfo")
        Me.ogdForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdForm.Location = New System.Drawing.Point(12, 12)
        Me.ogdForm.Name = "ogdForm"
        Me.ogdForm.Rows.Count = 2
        Me.ogdForm.Rows.DefaultSize = 19
        Me.ogdForm.Rows.MaxSize = 50
        Me.ogdForm.Rows.MinSize = 21
        Me.ogdForm.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdForm.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdForm.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdForm.Size = New System.Drawing.Size(704, 191)
        Me.ogdForm.StyleInfo = resources.GetString("ogdForm.StyleInfo")
        Me.ogdForm.TabIndex = 0
        Me.ogdForm.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ogdDetail
        '
        Me.ogdDetail.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdDetail.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdDetail.AutoGenerateColumns = False
        Me.ogdDetail.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdDetail.ColumnInfo = resources.GetString("ogdDetail.ColumnInfo")
        Me.ogdDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdDetail.HighLight = C1.Win.C1FlexGrid.HighLightEnum.Never
        Me.ogdDetail.Location = New System.Drawing.Point(12, 209)
        Me.ogdDetail.Name = "ogdDetail"
        Me.ogdDetail.Rows.Count = 2
        Me.ogdDetail.Rows.DefaultSize = 21
        Me.ogdDetail.Rows.MaxSize = 50
        Me.ogdDetail.Rows.MinSize = 21
        Me.ogdDetail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdDetail.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdDetail.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdDetail.Size = New System.Drawing.Size(704, 191)
        Me.ogdDetail.StyleInfo = resources.GetString("ogdDetail.StyleInfo")
        Me.ogdDetail.TabIndex = 7
        Me.ogdDetail.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ocmRefresh
        '
        Me.ocmRefresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmRefresh.Location = New System.Drawing.Point(9, 458)
        Me.ocmRefresh.Name = "ocmRefresh"
        Me.ocmRefresh.Size = New System.Drawing.Size(100, 30)
        Me.ocmRefresh.TabIndex = 4
        Me.ocmRefresh.Tag = "2;รีเฟรช;Refresh"
        Me.ocmRefresh.Text = "Refresh"
        Me.ocmRefresh.UseVisualStyleBackColor = True
        '
        'ocmProcess
        '
        Me.ocmProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmProcess.Location = New System.Drawing.Point(510, 458)
        Me.ocmProcess.Name = "ocmProcess"
        Me.ocmProcess.Size = New System.Drawing.Size(100, 30)
        Me.ocmProcess.TabIndex = 5
        Me.ocmProcess.Tag = "2;ดำเนินการ;Process"
        Me.ocmProcess.Text = "Process"
        Me.ocmProcess.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(616, 458)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose.TabIndex = 6
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'olaStaImport
        '
        Me.olaStaImport.AutoSize = True
        Me.olaStaImport.BackColor = System.Drawing.Color.Transparent
        Me.olaStaImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaStaImport.Location = New System.Drawing.Point(9, 403)
        Me.olaStaImport.Name = "olaStaImport"
        Me.olaStaImport.Size = New System.Drawing.Size(51, 16)
        Me.olaStaImport.TabIndex = 2
        Me.olaStaImport.Tag = "2;สถานะ ; Status"
        Me.olaStaImport.Text = "Status :"
        '
        'opgForm
        '
        Me.opgForm.Location = New System.Drawing.Point(9, 421)
        Me.opgForm.Name = "opgForm"
        Me.opgForm.Size = New System.Drawing.Size(707, 19)
        Me.opgForm.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.opgForm.TabIndex = 3
        '
        'wImports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmClose
        Me.ClientSize = New System.Drawing.Size(727, 503)
        Me.Controls.Add(Me.opnForm)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wImports"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;นำเข้า;Import"
        Me.Text = "Import"
        Me.opnForm.ResumeLayout(False)
        Me.opnForm.PerformLayout()
        CType(Me.ogdForm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ogdDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents opnForm As System.Windows.Forms.Panel
    Friend WithEvents ocmRefresh As System.Windows.Forms.Button
    Friend WithEvents ocmProcess As System.Windows.Forms.Button
    Friend WithEvents ocmClose As System.Windows.Forms.Button
    Friend WithEvents olaStaImport As System.Windows.Forms.Label
    Friend WithEvents opgForm As System.Windows.Forms.ProgressBar
    Friend WithEvents ockImportAll As System.Windows.Forms.CheckBox
    Friend WithEvents ogdForm As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents ogdDetail As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents olaValStaImp As System.Windows.Forms.Label
End Class
