Imports System.Text
Imports System.Data
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.Diagnostics
Imports C1.Win.C1FlexGrid

Public Class cFilterRow
    Private _flex As C1FlexGrid
    Private _style As CellStyle
    Private _row As Integer = -1
    ' index of filter row (-1 if none)
    Private _col As Integer = -1
    ' index of filter row cell being edited (-1 if none)
    ' ** ctor

    Public Sub New(ByVal flex As C1.Win.C1FlexGrid.C1FlexGrid)
        ' save reference to grid
        _flex = flex

        ' add filter row
        _row = _flex.Rows.Fixed
        _flex.Rows.Fixed += 1

        ' customize filter row style
        ' note: customize margins to align checkboxes correctly in filter cells,
        ' which have no vertical border <<1.4>>
        _style = _flex.Styles.Add("Filter", _flex.Styles.Frozen)
        _style.BackColor = SystemColors.Info
        _style.ForeColor = SystemColors.InfoText
        _style.Border.Direction = BorderDirEnum.Horizontal
        _style.Margins = New System.Drawing.Printing.Margins(1, 2, 1, 1)
        _flex.Rows(_row).Style = _style

        ' add event handlers
        AddHandler _flex.KeyDown, AddressOf _flex_KeyDown
        AddHandler _flex.BeforeMouseDown, AddressOf _flex_BeforeMouseDown
        AddHandler _flex.RowColChange, AddressOf _flex_RowColChange
        AddHandler _flex.AfterEdit, AddressOf _flex_AfterEdit

        ' initialize boolean cells <<1.4>>
        Dim cs As CellStyle = _flex.Styles.Add("BooleanFilterCell")
        cs.ImageAlign = ImageAlignEnum.CenterCenter
        For Each col As Column In _flex.Cols
            If col.DataType Is GetType(Boolean) Then
                _flex.SetCellCheck(_row, col.Index, CheckEnum.TSGrayed)
                _flex.SetCellStyle(_row, col.Index, cs)
            End If
        Next

        ' move cursor to filter row
        _flex.[Select](_row, _flex.Cols.Fixed)
    End Sub

    ' ** object model

    Public Property Visible() As Boolean
        Get
            Return _flex.Rows(_row).Visible
        End Get
        Set(ByVal value As Boolean)
            _flex.Rows(_row).Visible = value
        End Set
    End Property

    Public Sub Clear()
        For col As Integer = 0 To _flex.Cols.Count - 1
            _flex(_row, col) = Nothing
        Next
        UpdateFilter()
    End Sub

    ' ** event handlers

    ' custom logic for selection, clearing the filter
    Private Sub _flex_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        ' user hit f3 or up arrow on the first scrollable row
        If e.KeyCode = Keys.F3 OrElse (e.KeyCode = Keys.Up AndAlso _flex.Row = _row + 1) Then
            ' select the filter cell and go start editing it
            _flex.[Select](_row, _flex.Col)
            e.Handled = True
        End If

        ' user hit Del when the filter row is selected
        If e.KeyCode = Keys.Delete AndAlso _flex.Row = _row Then
            For col As Integer = _flex.Cols.Fixed To _flex.Cols.Count - 1
                _flex(_row, col) = Nothing
            Next
            UpdateFilter()
            e.Handled = True
        End If
    End Sub

    ' special mouse handling for filter row


    Private Sub _flex_BeforeMouseDown(ByVal sender As Object, ByVal e As BeforeMouseDownEventArgs)
        ' detect clicks on filter row
        Try
            If _flex.MouseRow = -1 Then e.Cancel = True

            If (e.Button And MouseButtons.Left) <> 0 AndAlso _row > 0 AndAlso _flex.MouseRow = _row Then
                ' get the column that was clicked
                Dim ht As HitTestInfo = _flex.HitTest(e.X, e.Y)
                Dim col As Integer = ht.Column

                ' fixed column? select the whole filter row
                If col < _flex.Cols.Fixed Then
                    _flex.[Select](_row, _flex.Cols.Fixed, _row, _flex.Cols.Count - 1)
                    _flex.FinishEditing(True)
                Else

                    ' clicked a cell on the filter row, select it
                    _flex.[Select](_row, col)
                End If

                ' eat the event (no sorting, sizing etc)
                ' unless this is a checkbox <<1.4>>
                If ht.Type <> HitTestTypeEnum.Checkbox Then
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
            e.Cancel = True
        End Try

    End Sub

    ' keep filter row in edit mode
    Private Sub _flex_RowColChange(ByVal sender As Object, ByVal e As EventArgs)
        ' we're only interested in cursor changes
        Try
            If _row > -1 AndAlso (_flex.Row <> _row OrElse _flex.Col <> _col) Then
                ' if the new cell is in the filter row, start editing
                _col = -1
                If _flex.Row = _row Then
                    _col = _flex.Col

                    ' start editing if this is not a checkbox <<1.4>>
                    If Not _flex.Cols(_col).DataType Is GetType(Boolean) Then
                        _flex.StartEditing()
                    End If
                End If

            End If
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try

    End Sub

    ' update the filter after any edits to the filter row
    Private Sub _flex_AfterEdit(ByVal sender As Object, ByVal e As RowColEventArgs)
        If e.Row = _row Then
            UpdateFilter()
        End If
    End Sub

    ' ** utilities

    ' update filter (called after editing the filter row)
    Private Sub UpdateFilter()
        ' make sure we have a filter row
        If _row < 0 Then
            Return
        End If

        ' make sure we have a data view
        Dim dv As DataView = TryCast(_flex.DataSource, DataView)
        If dv Is Nothing Then
            Dim dt As DataTable = TryCast(_flex.DataSource, DataTable)
            If dt IsNot Nothing Then
                dv = dt.DefaultView
            End If
        End If
        If dv Is Nothing Then
            Debug.WriteLine("DataSource should be a DataTable or DataView.")
            Return
        End If

        ' make sure changes are committed to the data source <<1.5>>
        Dim cm As CurrencyManager = DirectCast(_flex.BindingContext(_flex.DataSource, _flex.DataMember), CurrencyManager)
        cm.EndCurrentEdit()

        ' scan each cell in the filter row and build new filter
        Dim sb As New StringBuilder()
        For col As Integer = _flex.Cols.Fixed To _flex.Cols.Count - 1
            ' get column value
            Dim expr As String = _flex.GetDataDisplay(_row, col).TrimEnd()

            ' special handling for boolean columns <<1.4>>
            If _flex.Cols(col).DataType Is GetType(Boolean) Then
                Select Case _flex.GetCellCheck(_row, col)
                    Case CheckEnum.TSChecked
                        expr = "true"
                        Exit Select
                    Case CheckEnum.TSUnchecked
                        expr = "false"
                        Exit Select
                End Select
            End If

            ' ignore empty cells
            If expr.Length = 0 Then
                Continue For
            End If

            ' handle data maps <<1.3>>
            Dim dataMap As IDictionary = _flex.Cols(col).DataMap
            If dataMap IsNot Nothing Then
                For Each key As Object In dataMap.Keys
                    If String.Compare(dataMap(key).ToString(), expr, True) = 0 Then
                        expr = key.ToString()
                        Exit For
                    End If
                Next
            End If

            ' get filter expression
            expr = BuildFilterExpression(col, expr)
            If expr.Length = 0 Then
                Continue For
            End If

            ' concatenate new condition
            If sb.Length > 0 Then
                sb.Append(" And ")
            End If
            sb.AppendFormat("[{0}]{1}", _flex.Cols(col).Name, expr)
        Next

        ' apply filter to current view
        Dim strFilter As String = sb.ToString()
        If strFilter = dv.RowFilter Then
            Return
        End If
        Try
            Dim aWidth As New List(Of Double)
            For nCol As Integer = 0 To _flex.Cols.Count - 1
                aWidth.Add(_flex.Cols(nCol).Width)
            Next

            _flex(_row, 0) = Nothing
            dv.RowFilter = strFilter

            For nCol As Integer = 0 To aWidth.Count - 1
                _flex.Cols(nCol).Width = aWidth(nCol)
            Next
            aWidth.Clear()

            If dv.Count < 1 Then
                ' stay in filter row
                _flex.Row = _row
            End If
        Catch
            _flex(_row, 0) = "Err"
            Debug.WriteLine("Bad filter expression.")
        End Try
    End Sub

    ' Build a filter expression to apply to data table
    '
    ' This takes a value in the filter row and converts it into 
    ' a filter expression. For example:
    '
    ' Text		Filter Expression
    ' -------  -----------------
    ' smith    like 'smith*'
    ' > s      > 's'
    ' 1        = '1'
    ' > 1      > '1'
    '
    Private Function BuildFilterExpression(ByVal col As Integer, ByVal expr As String) As String
        ' operators we recognize
        Dim oper As String = "<>="

        ' no operators? use 'like' for strings, = for other types
        If oper.IndexOf(expr(0)) < 0 Then
            Return If((_flex.Cols(col).DataType Is GetType(String)), String.Format(" like '{0}*'", expr), String.Format(" = '{0}'", expr))
        End If

        ' look for end of operators
        For index As Integer = 0 To expr.Length - 1
            If oper.IndexOf(expr(index)) < 0 Then
                Dim retval As String = expr.Substring(0, index) & " "
                retval += String.Format("'{0}'", expr.Substring(index).Trim())
                Return retval
            End If
        Next

        ' if we got here, the condition must be bad (e.g. ><)
        Debug.WriteLine("Can't build filter expression.")
        Return ""
    End Function
End Class
