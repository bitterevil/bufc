﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wExpDateCD
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.odtDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.olaDateFrom = New System.Windows.Forms.Label()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.ocmOk = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'odtDateFrom
        '
        Me.odtDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDateFrom.Location = New System.Drawing.Point(15, 25)
        Me.odtDateFrom.Name = "odtDateFrom"
        Me.odtDateFrom.Size = New System.Drawing.Size(203, 20)
        Me.odtDateFrom.TabIndex = 11
        '
        'olaDateFrom
        '
        Me.olaDateFrom.AutoSize = True
        Me.olaDateFrom.Location = New System.Drawing.Point(12, 9)
        Me.olaDateFrom.Name = "olaDateFrom"
        Me.olaDateFrom.Size = New System.Drawing.Size(30, 13)
        Me.olaDateFrom.TabIndex = 10
        Me.olaDateFrom.Tag = "2;วันที่;Date"
        Me.olaDateFrom.Text = "Date"
        '
        'ocmCancel
        '
        Me.ocmCancel.BackColor = System.Drawing.Color.Transparent
        Me.ocmCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmCancel.Location = New System.Drawing.Point(118, 67)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(100, 30)
        Me.ocmCancel.TabIndex = 12
        Me.ocmCancel.Tag = "2;ยกเลิก;Cancel"
        Me.ocmCancel.Text = "Cancel"
        Me.ocmCancel.UseVisualStyleBackColor = False
        '
        'ocmOk
        '
        Me.ocmOk.BackColor = System.Drawing.Color.Transparent
        Me.ocmOk.Location = New System.Drawing.Point(12, 67)
        Me.ocmOk.Name = "ocmOk"
        Me.ocmOk.Size = New System.Drawing.Size(100, 30)
        Me.ocmOk.TabIndex = 13
        Me.ocmOk.Tag = "2;ตกลง;OK"
        Me.ocmOk.Text = "Ok"
        Me.ocmOk.UseVisualStyleBackColor = False
        '
        'wExpDateSale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(230, 111)
        Me.Controls.Add(Me.odtDateFrom)
        Me.Controls.Add(Me.olaDateFrom)
        Me.Controls.Add(Me.ocmCancel)
        Me.Controls.Add(Me.ocmOk)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExpDateSale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;เงื่อนไข;Condition"
        Me.Text = "Condition"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents odtDateFrom As DateTimePicker
    Friend WithEvents olaDateFrom As Label
    Friend WithEvents ocmCancel As Button
    Friend WithEvents ocmOk As Button
End Class
