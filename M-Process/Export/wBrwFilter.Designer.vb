﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wBrwFilter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wBrwFilter))
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.ocmRef = New System.Windows.Forms.Button()
        Me.ogdForm = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ocmOK = New System.Windows.Forms.Button()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.opnForm.SuspendLayout()
        CType(Me.ogdForm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnForm
        '
        Me.opnForm.Controls.Add(Me.ocmRef)
        Me.opnForm.Controls.Add(Me.ogdForm)
        Me.opnForm.Controls.Add(Me.ocmOK)
        Me.opnForm.Controls.Add(Me.ocmCancel)
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(0, 0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(540, 452)
        Me.opnForm.TabIndex = 1
        '
        'ocmRef
        '
        Me.ocmRef.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmRef.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmRef.Location = New System.Drawing.Point(12, 407)
        Me.ocmRef.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.ocmRef.Name = "ocmRef"
        Me.ocmRef.Size = New System.Drawing.Size(100, 30)
        Me.ocmRef.TabIndex = 5
        Me.ocmRef.Tag = "2;รีเฟรช;Refresh"
        Me.ocmRef.Text = "Refresh"
        Me.ocmRef.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ocmRef.UseVisualStyleBackColor = True
        '
        'ogdForm
        '
        Me.ogdForm.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdForm.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None
        Me.ogdForm.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.ogdForm.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D
        Me.ogdForm.ColumnInfo = resources.GetString("ogdForm.ColumnInfo")
        Me.ogdForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdForm.Location = New System.Drawing.Point(12, 12)
        Me.ogdForm.Name = "ogdForm"
        Me.ogdForm.Rows.Count = 2
        Me.ogdForm.Rows.DefaultSize = 19
        Me.ogdForm.Rows.MinSize = 21
        Me.ogdForm.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdForm.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdForm.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdForm.Size = New System.Drawing.Size(519, 381)
        Me.ogdForm.StyleInfo = resources.GetString("ogdForm.StyleInfo")
        Me.ogdForm.TabIndex = 4
        Me.ogdForm.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ocmOK
        '
        Me.ocmOK.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmOK.Location = New System.Drawing.Point(317, 407)
        Me.ocmOK.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.ocmOK.Name = "ocmOK"
        Me.ocmOK.Size = New System.Drawing.Size(100, 30)
        Me.ocmOK.TabIndex = 6
        Me.ocmOK.Tag = "2;ตกลง;Ok"
        Me.ocmOK.Text = "OK"
        Me.ocmOK.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ocmOK.UseVisualStyleBackColor = True
        '
        'ocmCancel
        '
        Me.ocmCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmCancel.Location = New System.Drawing.Point(431, 407)
        Me.ocmCancel.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(100, 30)
        Me.ocmCancel.TabIndex = 7
        Me.ocmCancel.Tag = "2;ยกเลิก;Cancel"
        Me.ocmCancel.Text = "Cancel"
        Me.ocmCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ocmCancel.UseVisualStyleBackColor = True
        '
        'wBrwFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmCancel
        Me.ClientSize = New System.Drawing.Size(540, 452)
        Me.Controls.Add(Me.opnForm)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wBrwFilter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;รายการข้อมูล;Browse"
        Me.Text = "Browse"
        Me.opnForm.ResumeLayout(False)
        CType(Me.ogdForm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents opnForm As System.Windows.Forms.Panel
    Friend WithEvents ocmRef As System.Windows.Forms.Button
    Protected Friend WithEvents ogdForm As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents ocmOK As System.Windows.Forms.Button
    Friend WithEvents ocmCancel As System.Windows.Forms.Button
End Class
