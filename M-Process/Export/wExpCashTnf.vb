﻿Imports C1.Win.C1FlexGrid
Imports System.IO
Imports System.ComponentModel

Public Class wExpCashTnf

    Private bW_FrmLoad As Boolean = False
    Private tW_CaptionExport As String = "Money In Bank Doc.;x;Export File Name;Status Send;Create File;Send File"
    Private tW_CaptionExportThai As String = "x;เอกสารนำเงินเข้าธนาคาร;ไฟล์ส่งออก;สถานะการส่งออก;สร้างไฟล์;ส่งไฟล์"
    Public Shared oW_BackgroudWord As BackgroundWorker '*CH 19-12-2014
    Private dW_StartTime As DateTime
    Private dW_EndTime As DateTime
    Public Shared bW_StaProcess As Boolean = False

    Private Sub W_SETxCaptionGridExport()
        Dim tCapImport As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '1:thai
            tCapImport = tW_CaptionExport
        Else
            tCapImport = tW_CaptionExportThai
        End If

        Dim nCol As Integer = 2 'Start Column 2
        For Each tCap In tCapImport.Split(";")
            With Me.ogdForm
                .Cols(nCol).Caption = tCap
            End With
            nCol += 1
        Next
    End Sub

    Private Sub W_SETxListExport()
        With Me.ogdForm
            Dim oItemExport = cExportTemplate.C_GETaItemExport(Me.otbBchCode.Text, Me.odtSalDate.Value.ToString("yyyy-MM-dd"))
            .DataSource = oItemExport
            .Row = 0 'Default Row 0
            Me.W_SETxCaptionGridExport()

            Dim dDateNow As Date = Date.Now
            'Add Default Condition
            For Each oItem In oItemExport
                oItem.nMode = 2 ' 0:All,1:Interval,2:Select
                oItem.tDateFrom = Format(dDateNow, "yyyy-MM-dd")
                oItem.tDateTo = Format(dDateNow, "yyyy-MM-dd")
                oItem.tConditon = cExportTemplate.C_GETtFmtCondition(Format(dDateNow, "dd/MM/yyyy"), Format(dDateNow, "dd/MM/yyyy"))
            Next

            'Set Seq No
            Dim oStyle As C1.Win.C1FlexGrid.CellStyle
            oStyle = .Styles.Add("oStyle")
            oStyle.ForeColor = System.Drawing.Color.Green
            For nRow As Integer = 1 To .Rows.Count - 1
                .Item(nRow, 0) = nRow
                .SetCellStyle(nRow, 5, oStyle)
            Next

            If AdaConfig.cConfig.oApplication.nLanguage = 1 Then '1:thai
                .Cols("tItem").Visible = False
                .Cols("tItemThai").Visible = True
            End If
        End With
    End Sub

#Region "Event Form"

    Private Sub wExports_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Me.Cursor = Cursors.WaitCursor Then
            e.Cancel = True
        End If
    End Sub

    Private Sub wExports_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cCNSP.SP_FrmSetCapControl(Me)

        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
            Exit Sub
        End If

        cExportTemplate.C_CALxClear()
        cCNVB.tVB_Export = cCNVB.tVB_ZGLINT002
        Me.W_SETxListExport() 'Load List Export
        Me.bW_FrmLoad = True
        If Me.ogdForm.Row > 0 Then Me.ogdForm.Row = 1 'Fix Load รายการแรก
    End Sub

#End Region

#Region "Grid"

    Private Sub ogdForm_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdForm.AfterEdit
        Dim bStatus As Boolean = False
        Dim tXmlFile As String = ""
        Try
            With Me.ogdForm
                If .MouseRow > 0 And .MouseCol = 2 Then '.MouseCol = 2 = "เงื่อนไข;Condition"
                    Me.ottForm.SetToolTip(Me.ogdForm, .Item(.MouseRow, .MouseCol))
                End If

                If e.Col = 6 Or e.Col = 7 Then 'Status Create File, Status Send
                    bStatus = Convert.ToBoolean(.Item(e.Row, e.Col))
                    tXmlFile = .Item(e.Row, "tExpFileName").ToString
                    If tXmlFile <> "" Then
                        For nRow = 0 To .Rows.Count - 1
                            If .Item(nRow, "tExpFileName").ToString.ToUpper = tXmlFile.ToUpper Then
                                .Item(nRow, e.Col) = bStatus
                            End If
                        Next
                    End If
                End If
            End With
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Button"

    Private Sub ocmProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmProcess.Click
        Me.Cursor = Cursors.WaitCursor
        Me.ocmProcess.Enabled = False
        Me.ocmClose.Enabled = False
        Try
            '*CH 19-12-2014
            oW_BackgroudWord = New BackgroundWorker
            oW_BackgroudWord.WorkerReportsProgress = True
            oW_BackgroudWord.WorkerSupportsCancellation = True
            AddHandler oW_BackgroudWord.DoWork, AddressOf C_RUNxDoWork
            AddHandler oW_BackgroudWord.ProgressChanged, AddressOf C_RUNxWordChanged
            AddHandler oW_BackgroudWord.RunWorkerCompleted, AddressOf C_RUNxWorkerCompleted

            Me.opgForm.Maximum = 100
            Me.opgForm.Value = 0
            bW_StaProcess = False

            oW_BackgroudWord.RunWorkerAsync()

            'cLog.C_CALxWriteLog("wExports > Process")
            'cExportTemplate.oC_Progress = Me.opgForm
            'cExportTemplate.C_CALxProcessAll()
            'cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        Catch ex As Exception
        Finally
            'Me.ocmProcess.Enabled = True
            'Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub C_RUNxDoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        'Dim oWorker As BackgroundWorker = CType(sender, BackgroundWorker)
        dW_StartTime = Now
        cExportTemplate.oC_Progress = Me.opgForm
        cExportTemplate.C_CALxProcessAll()
    End Sub
    Private Sub C_RUNxWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim tLogAda As String = ""
        Me.opgForm.Value = 100

        dW_EndTime = Now
        Dim dDiffTime As TimeSpan = dW_EndTime - dW_StartTime

        tLogAda = "[Start : " & Format(dW_StartTime, "HH:mm:ss") & "], "
        tLogAda &= "[End : " & Format(dW_EndTime, "HH:mm:ss") & "], "
        tLogAda &= "[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]"
        cLog.C_CALxWriteLog("wExport > Process " & tLogAda)
        Me.ocmProcess.Enabled = True
        Me.ocmClose.Enabled = True
        Me.Cursor = Cursors.Default

        oW_BackgroudWord = Nothing
        If bW_StaProcess Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        Else
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN121, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        End If
        ocmRefresh_Click(Nothing, Nothing)
    End Sub
    Private Sub C_RUNxWordChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        Me.opgForm.Value = e.ProgressPercentage
        Me.olaValStaExp.Text = cExportTemplate.tC_Status & " : " & e.ProgressPercentage & " %"
    End Sub

    ''' <summary>
    ''' Browse Branch
    ''' </summary>
    Private Sub ocmBrwBch_Click(sender As Object, e As EventArgs) Handles ocmBrwBch.Click
        Try
            Dim owBrowse As New wBrowse
            Dim oDatRow As DataRow
            Dim tSql As String
            Dim tFieldBch As String = "FTBchCode,FTBchName"

            tSql = "SELECT " & tFieldBch
            tSql &= " FROM TCNMBranch"
            tSql &= " ORDER BY FTBchCode"

            With owBrowse
                .mCommandText = tSql
                .mTableName = "TCNMBranch"
                .mCaptionName = Me.olaBranch.Text
                .mColumnName = IIf(cCNVB.nVB_CutLng = cCNEN.eLanguage.Lang1, cCNCS.tCS_CapColumnTha, cCNCS.tCS_CapColumnEng)
                .mAlignMent = "L;L"
                .mDefaultColumn = 1
                .mCaptionCol = tFieldBch
                .ShowDialog()
                If .mReturnSelected = True Then
                    oDatRow = .mReturnDatarow
                    Me.otbBchCode.Text = oDatRow("FTBchCode")
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Refresh Grid
    ''' </summary>
    Private Sub ocmRefresh_Click(sender As Object, e As EventArgs) Handles ocmRefresh.Click
        Try
            cExportTemplate.C_CALxClear()
            Me.W_SETxListExport() 'Load List Export
            Me.bW_FrmLoad = True
            If Me.ogdForm.Row > 0 Then Me.ogdForm.Row = 1 'Fix Load รายการแรก
        Catch ex As Exception
        End Try
    End Sub

#End Region

End Class