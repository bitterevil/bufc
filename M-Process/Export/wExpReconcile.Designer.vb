﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wExpReconcile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wExpReconcile))
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.olaValStaExp = New System.Windows.Forms.Label()
        Me.ogdFormExp06 = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ocmProcess = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.olaStaExport = New System.Windows.Forms.Label()
        Me.opgForm = New System.Windows.Forms.ProgressBar()
        Me.omsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.omsUnLock = New System.Windows.Forms.ToolStripMenuItem()
        Me.ottForm = New System.Windows.Forms.ToolTip(Me.components)
        Me.opnForm.SuspendLayout()
        CType(Me.ogdFormExp06, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.omsForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnForm
        '
        Me.opnForm.Controls.Add(Me.olaValStaExp)
        Me.opnForm.Controls.Add(Me.ogdFormExp06)
        Me.opnForm.Controls.Add(Me.ocmProcess)
        Me.opnForm.Controls.Add(Me.ocmClose)
        Me.opnForm.Controls.Add(Me.olaStaExport)
        Me.opnForm.Controls.Add(Me.opgForm)
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(0, 0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(727, 388)
        Me.opnForm.TabIndex = 0
        '
        'olaValStaExp
        '
        Me.olaValStaExp.AutoSize = True
        Me.olaValStaExp.Location = New System.Drawing.Point(66, 292)
        Me.olaValStaExp.Name = "olaValStaExp"
        Me.olaValStaExp.Size = New System.Drawing.Size(0, 13)
        Me.olaValStaExp.TabIndex = 8
        '
        'ogdFormExp06
        '
        Me.ogdFormExp06.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdFormExp06.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdFormExp06.AutoGenerateColumns = False
        Me.ogdFormExp06.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdFormExp06.ColumnInfo = resources.GetString("ogdFormExp06.ColumnInfo")
        Me.ogdFormExp06.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdFormExp06.Location = New System.Drawing.Point(12, 12)
        Me.ogdFormExp06.Name = "ogdFormExp06"
        Me.ogdFormExp06.Rows.Count = 2
        Me.ogdFormExp06.Rows.DefaultSize = 19
        Me.ogdFormExp06.Rows.MaxSize = 50
        Me.ogdFormExp06.Rows.MinSize = 21
        Me.ogdFormExp06.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdFormExp06.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdFormExp06.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdFormExp06.Size = New System.Drawing.Size(704, 275)
        Me.ogdFormExp06.StyleInfo = resources.GetString("ogdFormExp06.StyleInfo")
        Me.ogdFormExp06.TabIndex = 7
        Me.ogdFormExp06.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ocmProcess
        '
        Me.ocmProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmProcess.Location = New System.Drawing.Point(509, 345)
        Me.ocmProcess.Name = "ocmProcess"
        Me.ocmProcess.Size = New System.Drawing.Size(100, 30)
        Me.ocmProcess.TabIndex = 5
        Me.ocmProcess.Tag = "2;ดำเนินการ;Process"
        Me.ocmProcess.Text = "Process"
        Me.ocmProcess.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(615, 345)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose.TabIndex = 6
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'olaStaExport
        '
        Me.olaStaExport.AutoSize = True
        Me.olaStaExport.BackColor = System.Drawing.Color.Transparent
        Me.olaStaExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaStaExport.Location = New System.Drawing.Point(9, 290)
        Me.olaStaExport.Name = "olaStaExport"
        Me.olaStaExport.Size = New System.Drawing.Size(51, 16)
        Me.olaStaExport.TabIndex = 2
        Me.olaStaExport.Tag = "2;สถานะ ; Status"
        Me.olaStaExport.Text = "Status :"
        '
        'opgForm
        '
        Me.opgForm.Location = New System.Drawing.Point(9, 308)
        Me.opgForm.Name = "opgForm"
        Me.opgForm.Size = New System.Drawing.Size(706, 19)
        Me.opgForm.TabIndex = 3
        '
        'omsForm
        '
        Me.omsForm.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.omsUnLock})
        Me.omsForm.Name = "omsForm"
        Me.omsForm.Size = New System.Drawing.Size(112, 26)
        '
        'omsUnLock
        '
        Me.omsUnLock.Name = "omsUnLock"
        Me.omsUnLock.Size = New System.Drawing.Size(111, 22)
        Me.omsUnLock.Text = "Unlock"
        '
        'wExpReconcile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmClose
        Me.ClientSize = New System.Drawing.Size(727, 388)
        Me.Controls.Add(Me.opnForm)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExpReconcile"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออก;Export"
        Me.Text = "Export"
        Me.opnForm.ResumeLayout(False)
        Me.opnForm.PerformLayout()
        CType(Me.ogdFormExp06, System.ComponentModel.ISupportInitialize).EndInit()
        Me.omsForm.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents opnForm As System.Windows.Forms.Panel
    Friend WithEvents opgForm As System.Windows.Forms.ProgressBar
    Friend WithEvents olaStaExport As System.Windows.Forms.Label
    Friend WithEvents ocmClose As System.Windows.Forms.Button
    Friend WithEvents ocmProcess As System.Windows.Forms.Button
    Friend WithEvents ottForm As System.Windows.Forms.ToolTip
    Friend WithEvents omsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents omsUnLock As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ogdFormExp06 As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents olaValStaExp As System.Windows.Forms.Label
End Class
