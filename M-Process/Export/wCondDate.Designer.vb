<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wCondDate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.XpGradientPanel1 = New System.Windows.Forms.Panel()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.ockDateInterval = New System.Windows.Forms.CheckBox()
        Me.ocmOk = New System.Windows.Forms.Button()
        Me.ogbDateInerval = New System.Windows.Forms.GroupBox()
        Me.odtDateTo = New System.Windows.Forms.DateTimePicker()
        Me.odtDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.olaDateTo = New System.Windows.Forms.Label()
        Me.olaDateFrom = New System.Windows.Forms.Label()
        Me.XpGradientPanel1.SuspendLayout()
        Me.ogbDateInerval.SuspendLayout()
        Me.SuspendLayout()
        '
        'XpGradientPanel1
        '
        Me.XpGradientPanel1.Controls.Add(Me.ocmCancel)
        Me.XpGradientPanel1.Controls.Add(Me.ocmOk)
        Me.XpGradientPanel1.Controls.Add(Me.ogbDateInerval)
        Me.XpGradientPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XpGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.XpGradientPanel1.Name = "XpGradientPanel1"
        Me.XpGradientPanel1.Size = New System.Drawing.Size(234, 213)
        Me.XpGradientPanel1.TabIndex = 0
        '
        'ocmCancel
        '
        Me.ocmCancel.BackColor = System.Drawing.Color.Transparent
        Me.ocmCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmCancel.Location = New System.Drawing.Point(123, 173)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(100, 30)
        Me.ocmCancel.TabIndex = 8
        Me.ocmCancel.Tag = "2;¡��ԡ;Cancel"
        Me.ocmCancel.Text = "Cancel"
        Me.ocmCancel.UseVisualStyleBackColor = False
        '
        'ockDateInterval
        '
        Me.ockDateInterval.AutoSize = True
        Me.ockDateInterval.Location = New System.Drawing.Point(12, 1)
        Me.ockDateInterval.Name = "ockDateInterval"
        Me.ockDateInterval.Size = New System.Drawing.Size(15, 14)
        Me.ockDateInterval.TabIndex = 0
        Me.ockDateInterval.Tag = ""
        Me.ockDateInterval.UseVisualStyleBackColor = True
        '
        'ocmOk
        '
        Me.ocmOk.BackColor = System.Drawing.Color.Transparent
        Me.ocmOk.Location = New System.Drawing.Point(12, 173)
        Me.ocmOk.Name = "ocmOk"
        Me.ocmOk.Size = New System.Drawing.Size(100, 30)
        Me.ocmOk.TabIndex = 9
        Me.ocmOk.Tag = "2;��ŧ;OK"
        Me.ocmOk.Text = "Ok"
        Me.ocmOk.UseVisualStyleBackColor = False
        '
        'ogbDateInerval
        '
        Me.ogbDateInerval.BackColor = System.Drawing.Color.Transparent
        Me.ogbDateInerval.Controls.Add(Me.odtDateTo)
        Me.ogbDateInerval.Controls.Add(Me.ockDateInterval)
        Me.ogbDateInerval.Controls.Add(Me.odtDateFrom)
        Me.ogbDateInerval.Controls.Add(Me.olaDateTo)
        Me.ogbDateInerval.Controls.Add(Me.olaDateFrom)
        Me.ogbDateInerval.Location = New System.Drawing.Point(12, 12)
        Me.ogbDateInerval.Name = "ogbDateInerval"
        Me.ogbDateInerval.Size = New System.Drawing.Size(211, 155)
        Me.ogbDateInerval.TabIndex = 6
        Me.ogbDateInerval.TabStop = False
        Me.ogbDateInerval.Tag = "2;      �ѹ���;      Date"
        Me.ogbDateInerval.Text = "      Date"
        '
        'odtDateTo
        '
        Me.odtDateTo.Enabled = False
        Me.odtDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDateTo.Location = New System.Drawing.Point(15, 96)
        Me.odtDateTo.Name = "odtDateTo"
        Me.odtDateTo.Size = New System.Drawing.Size(157, 22)
        Me.odtDateTo.TabIndex = 2
        '
        'odtDateFrom
        '
        Me.odtDateFrom.Enabled = False
        Me.odtDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDateFrom.Location = New System.Drawing.Point(15, 55)
        Me.odtDateFrom.Name = "odtDateFrom"
        Me.odtDateFrom.Size = New System.Drawing.Size(157, 22)
        Me.odtDateFrom.TabIndex = 2
        '
        'olaDateTo
        '
        Me.olaDateTo.AutoSize = True
        Me.olaDateTo.Location = New System.Drawing.Point(12, 80)
        Me.olaDateTo.Name = "olaDateTo"
        Me.olaDateTo.Size = New System.Drawing.Size(55, 16)
        Me.olaDateTo.TabIndex = 1
        Me.olaDateTo.Tag = "2;�֧�ѹ���;To date"
        Me.olaDateTo.Text = "To date"
        '
        'olaDateFrom
        '
        Me.olaDateFrom.AutoSize = True
        Me.olaDateFrom.Location = New System.Drawing.Point(12, 39)
        Me.olaDateFrom.Name = "olaDateFrom"
        Me.olaDateFrom.Size = New System.Drawing.Size(69, 16)
        Me.olaDateFrom.TabIndex = 1
        Me.olaDateFrom.Tag = "2;�ҡ�ѹ���;From date"
        Me.olaDateFrom.Text = "From date"
        '
        'wCondDate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmCancel
        Me.ClientSize = New System.Drawing.Size(234, 213)
        Me.Controls.Add(Me.XpGradientPanel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wCondDate"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;���͹�;Condition"
        Me.Text = "Condition"
        Me.XpGradientPanel1.ResumeLayout(False)
        Me.ogbDateInerval.ResumeLayout(False)
        Me.ogbDateInerval.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XpGradientPanel1 As System.Windows.Forms.Panel
    Friend WithEvents odtDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents olaDateTo As System.Windows.Forms.Label
    Friend WithEvents odtDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents ocmCancel As System.Windows.Forms.Button
    Friend WithEvents ocmOk As System.Windows.Forms.Button
    Friend WithEvents ogbDateInerval As System.Windows.Forms.GroupBox
    Friend WithEvents olaDateFrom As System.Windows.Forms.Label
    Friend WithEvents ockDateInterval As System.Windows.Forms.CheckBox
End Class
