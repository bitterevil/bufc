﻿Imports System.ComponentModel
Imports System.IO.Compression
Imports System.IO
Imports System.Text
Public Class wExports2
    Private tW_AlertDowork As String = ""
    Private nW_StatusDoWork As String = 0 'Status Dowork
    Private tW_CapPackage As String = "รายการ|Package"
    Private tW_CapHDCD As String = "เงื่อนไข|Condition"
    Private tW_CapDetailCD As String = "เลือกทั้งหมดและวันที่ระหว่าง|Select All and date between"
    Private tW_To As String = "ถึง|To"
    Private tW_TranPk As String = "Transaction|Transaction"
    Private tW_SalePk As String = "ขาย|Sale"
    Private tW_PayPk As String = "ชำระเงิน|Payment"
    Private tW_SelectData As String = "กรุณาเลือกรายการที่ต้องการส่งออก|Select at least one Checkbox"
    Private tW_ExportMore As String = "ไม่สามารถ Export ได้ เนื่องจากส่งออกข้อมูลซ้ำ|Export Error"
    Private tW_ExpSuccess As String = "ส่งออกข้อมูลสำเร็จ|Export Success"
    Private tW_FTPErr As String = "เชื่อมต่อ FTP ไม่สำเร็จ|Connect FTP Fail"
    Public tW_TranDate As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    Public tW_SaleDate As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    Public tW_PayDate As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    Public tW_BackupData As String = Now.ToString("yyyyMMddHHmmss", New System.Globalization.CultureInfo("en-US"))
    Private bW_ChkTran As Boolean = True
    Private bW_ChkSale As Boolean = True
    Private bW_ChkPay As Boolean = True
    Private dW_StartTime As DateTime
    Private dW_EndTime As DateTime
    Private tW_FileName As String = ""
    Private Const tW_Success As String = "สำเร็จ;Success"
    Private Const tW_Unsuccess As String = "ไม่สำเร็จ;Unsuccessful"
    Private Const tW_StatusExport As String = "กำลังส่งออกไฟล์;Exporting file"

    'Caption Grid
    Private tW_CaptionExport As String = "List Export,List Export,Count,Count"
    Private tW_CaptionExportThai As String = "รายการส่งออก,รายการส่งออก,จำนวนไฟล์,จำนวนไฟล์"


    Private Sub wExports2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        C_CALxFindTable()
        cCNSP.SP_FrmSetCapControl(Me)
        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
            Exit Sub
        End If

        cExportTemplate.C_CALxClear()
        cCNVB.tVB_Export = cCNVB.tVB_ZSDINT005
        Me.W_SETxGridExport()
        Me.W_SETxListExport() 'Load List Export
        ' W_SETxGrid()
        'Me.bW_FrmLoad = True
        'If Me.ogdForm.Row > 0 Then Me.ogdForm.Row = 1 'Fix Load รายการแรก
        ' obgGroudWord.WorkerReportsProgress = True
        '    opgFrmExp.Maximum = 100
    End Sub

    ''' <summary>
    ''' Zip Folder
    ''' </summary>
    Private Function W_GENbZipfile(Optional ByRef poExp As Exception = Nothing) As Boolean
        Dim tFilePath As String = ""
        Dim tFileName As String = ""
        Dim tNameCtrl As String = ""
        Dim tPathFileZip As String = ""
        Dim tPathFileZipCtrl As String = ""
        Dim tDateFolder As String = ""
        Try

            If cExpTransaction.tC_ShareTranDate = "" Then
                If cExpSale.tC_SaleDate < cExpPayment.tC_Paydate And cExpSale.tC_SaleDate <> "" Then
                    tDateFolder = cExpSale.tC_SaleDate
                ElseIf cExpPayment.tC_Paydate < cExpSale.tC_SaleDate And cExpPayment.tC_Paydate <> "" Then
                    tDateFolder = cExpPayment.tC_Paydate
                Else
                    If ogdExport.Rows(1).Cells(1).Value Then
                        tDateFolder = cExpSale.tC_SaleDate
                    Else
                        tDateFolder = cExpPayment.tC_Paydate
                    End If

                End If
                    Else
                tDateFolder = cExpTransaction.tC_ShareTranDate
            End If

            tFilePath = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tDateFolder
            tPathFileZip = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tDateFolder + ".zip"
            tPathFileZipCtrl = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + tDateFolder + ".zip.ctrl"

            Dim tPathSave As String = AdaConfig.cConfig.oConfigXml.tOutbox
            Dim oZip As New Ionic.Zip.ZipFile()
            Dim oZipCtrl As New Ionic.Zip.ZipFile()

            If (Not System.IO.Directory.Exists(tFilePath)) Then
                Return False
            Else
                oZip.AddDirectory(tFilePath)
                oZipCtrl.AddDirectory(tFilePath)
            End If

            Dim oDir As New System.IO.DirectoryInfo(tFilePath)
            oZip.Save(String.Format("{0}{1}.zip", oDir.Parent.FullName + "\", oDir.Name))
            oZipCtrl.Save(String.Format("{0}{1}.zip.ctrl", oDir.Parent.FullName + "\", oDir.Name))

            tFileName = Path.GetFileName(tPathFileZip)
            tNameCtrl = Path.GetFileName(tPathFileZipCtrl)

            If AdaConfig.cConfig.oConfigXml.tFTPType = "FTP" Then   ' Case Type FTP
                If cCNSP.SP_GETbUploadToFTP(tFileName) = True Then
                    If cCNSP.SP_GETbUploadToFTP(tNameCtrl) Then
                        C_CALxBackupFile(tPathFileZip)
                        C_CALxBackupFileZipCtrl(tPathFileZipCtrl)
                        C_DELxBackupFile(tFilePath)
                    End If
                End If
            Else  ' Case Type sFTP
                If cCNSP.SP_GETbUploadToSFTP(tFileName) = True Then
                    If cCNSP.SP_GETbUploadToSFTP(tNameCtrl) Then
                        C_CALxBackupFile(tPathFileZip)
                        C_CALxBackupFileZipCtrl(tPathFileZipCtrl)
                        C_DELxBackupFile(tFilePath)
                    End If
                End If
            End If

        Catch ex As Exception
            poExp = ex
            Return False
        End Try

        Return True
    End Function

    Public Sub C_CALxBackupFile(ByVal ptFileCopyBk As String)
        Dim tFileName As String = ""
        Try
            If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tW_BackupData & "_Export") Then
                IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tW_BackupData & "_Export")
            End If
            Try
                Dim nIndex As Integer = 0
                tFileName = Path.GetFileName(ptFileCopyBk)
                FileCopy(ptFileCopyBk, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tW_BackupData & "_Export\" & tFileName)
                If File.Exists(ptFileCopyBk) Then
                    File.Delete(ptFileCopyBk)
                End If
                nIndex += 1
            Catch ex As Exception
            End Try
        Catch ex As Exception
        Finally
            tFileName = Nothing
        End Try
    End Sub

    Public Sub C_CALxBackupFileZipCtrl(ByVal ptFileCopyBk As String)
        Dim tFileName As String = ""
        Try
            If Not IO.Directory.Exists(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tW_BackupData & "_Export") Then
                IO.Directory.CreateDirectory(AdaConfig.cConfig.oConfigXml.tBackup & "\" & tW_BackupData & "_Export")
            End If
            Try
                Dim nIndex As Integer = 0
                tFileName = Path.GetFileName(ptFileCopyBk)
                FileCopy(ptFileCopyBk, AdaConfig.cConfig.oConfigXml.tBackup & "\" & tW_BackupData & "_Export\" & tFileName)
                If File.Exists(ptFileCopyBk) Then
                    File.Delete(ptFileCopyBk)
                End If
                nIndex += 1
            Catch ex As Exception
            End Try
        Catch ex As Exception
        Finally
            tFileName = Nothing
        End Try
    End Sub


    Public Sub C_DELxBackupFile(ByVal ptPathFolder As String)
        Try
            System.IO.Directory.Delete(ptPathFolder, True)
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Set datragrid Export
    ''' </summary>
    Public Sub W_SETxGridExport()
        Dim tHeadPackage As String = ""
        Dim tHeadCD As String = ""
        Dim atHeadPackage() As String
        Dim atHeadCD() As String
        Dim nLeng As Integer = 0

        Try
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1
            atHeadPackage = tW_CapPackage.Split("|")
            atHeadCD = tW_CapHDCD.Split("|")
            tHeadPackage = atHeadPackage(nLeng)
            tHeadCD = atHeadCD(nLeng)

            Dim oRowNo As New DataGridViewTextBoxColumn
            oRowNo.HeaderText = ""
            oRowNo.Width = 0
            oRowNo.ReadOnly = True
            oRowNo.Visible = False
            ogdExport.Columns.Add(oRowNo)

            Dim ockExp As New DataGridViewCheckBoxColumn()
            ockExp.HeaderText = ""
            ockExp.Width = 50
            ogdExport.Columns.Add(ockExp)

            Dim oPackage As New DataGridViewTextBoxColumn
            oPackage.HeaderText = tHeadCD
            oPackage.Width = 130
            oPackage.ReadOnly = True
            ogdExport.Columns.Add(oPackage)

            Dim oCondition As New DataGridViewTextBoxColumn
            oCondition.HeaderText = tHeadPackage
            oCondition.Width = 340
            oCondition.ReadOnly = True
            ogdExport.Columns.Add(oCondition)

            Dim ocmDate As New DataGridViewButtonColumn
            ocmDate.HeaderText = ""
            ocmDate.Width = 30
            ogdExport.Columns.Add(ocmDate)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_SETxListExport()

        Dim nLeng As Integer = 0
        Dim atTo() As String
        Dim tTo As String
        Dim atCapDetailCD() As String
        Dim tCapDetailCD As String

        Dim atTranPk() As String
        Dim tTranPk As String
        Dim tDTTranCD As String
        Dim oRowTran As String()
        Dim tNoTran As String = "001"

        Dim atSalePk() As String
        Dim tSalePk As String
        Dim tDTSaleCD As String
        Dim oRowSale As String()
        Dim tNoSale As String = "002"

        Dim atPayPk() As String
        Dim tPayPk As String
        Dim tDTPayCD As String
        Dim oRowPay As String()
        Dim tNoPay As String = "003"

        Try

            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1
            ogdExport.Rows.Clear()
            atTo = tW_To.Split("|")
            atCapDetailCD = tW_CapDetailCD.Split("|")
            atTranPk = tW_TranPk.Split("|")
            atSalePk = tW_SalePk.Split("|")
            atPayPk = tW_PayPk.Split("|")

            tTo = atTo(nLeng)
            tCapDetailCD = atCapDetailCD(nLeng)
            tTranPk = atTranPk(nLeng)
            tSalePk = atSalePk(nLeng)
            tPayPk = atPayPk(nLeng)

            '  tDTTranCD = tCapDetailCD + " " + tW_TranDate + " " + tTo + " " + tW_TranDate
            tDTTranCD = tCapDetailCD + " " + tW_TranDate
            oRowTran = New String() {tNoTran, bW_ChkTran, tTranPk, tDTTranCD}
            ogdExport.Rows.Add(oRowTran)

            ' tDTSaleCD = tCapDetailCD + " " + tW_SaleDate + " " + tTo + " " + tW_SaleDate
            tDTSaleCD = tCapDetailCD + " " + tW_SaleDate
            oRowSale = New String() {tNoSale, bW_ChkSale, tSalePk, tDTSaleCD}
            ogdExport.Rows.Add(oRowSale)

            'tDTPayCD = tCapDetailCD + " " + tW_PayDate + " " + tTo + " " + tW_PayDate
            tDTPayCD = tCapDetailCD + " " + tW_PayDate
            oRowPay = New String() {tNoPay, bW_ChkPay, tPayPk, tDTPayCD}
            ogdExport.Rows.Add(oRowPay)
        Catch ex As Exception
        End Try

    End Sub

    Private Sub ogdExport_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdExport.CellContentClick
        If e.ColumnIndex = 4 Then
            W_SETxDateSale(Convert.ToString(Convert.ToInt32(e.RowIndex) + 1))
        End If
    End Sub

    Private Sub W_SETxDateSale(ByVal ptCase As String)
        Try
            wExpDateCD.tW_SetDate = ptCase
            wExpDateCD.ShowDialog()
            wExpDateCD.Dispose()
            Me.W_SETxListExport()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmProcess_Click(sender As Object, e As EventArgs) Handles ocmProcess.Click
        Try
            If Not obgGroudWord.IsBusy Then
                obgGroudWord.RunWorkerAsync()
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub W_SETxUploadToFTP(ByVal ptFileZip As String)
        Dim tFileName As String = ""
        Try
            Dim tPathOutbox As String = AdaConfig.cConfig.oConfigXml.tOutbox + "\BUFC_" + Now.ToString("yyyyMMdd")
            Dim oDirInfoOutbox As New DirectoryInfo(tPathOutbox)
            Dim oFiles As FileInfo() = oDirInfoOutbox.GetFiles("*.txt")
            Dim oSortFiles = oFiles.OrderBy(Function(o) o.Name)

            For Each oItem In oSortFiles 'oFiles
                tFileName = Path.GetFileName(oItem.FullName)
                tFileName = tFileName.Replace(" ", "")
                If cCNSP.SP_GETbUploadToFTP(tFileName) = True Then

                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_SETxProcessExp()
        Dim tNoGrid As String = ""
        Dim bExpsale As Boolean = True
        Dim oExpSale As New cExpSale
        Dim oExpPayment As New cExpPayment
        Dim oExpTran As New cExpTransaction
        Dim nLeng As Integer = 0
        Dim oExp As New Exception
        Dim tChooseGrid As String = ""


        Try
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1

            ' วนเพื่อหาวันที่ที่เลือก เพื่อนำไปสร้าง Folder 
            For nChoose As Integer = 0 To ogdExport.Rows.Count - 1
                tChooseGrid = ogdExport.Rows(nChoose).Cells(0).Value
                If ogdExport.Rows(nChoose).Cells(1).Value = True Then
                    Select Case tChooseGrid
                        Case "001"
                            cExpTransaction.tC_ShareTranDate = W_SETtTranDate(tW_TranDate)

                        Case "002"
                            cExpSale.tC_SaleDate = W_SETtTranDate(tW_SaleDate)
                        Case "003"
                            cExpPayment.tC_Paydate = W_SETtTranDate(tW_PayDate)
                    End Select
                End If
            Next

            For nChoose As Integer = 0 To ogdExport.Rows.Count - 1
                tW_FileName = ""
                If ogdExport.Rows(nChoose).Cells(1).Value = True Then
                    tNoGrid = ogdExport.Rows(nChoose).Cells(0).Value
                    Select Case tNoGrid
                        Case "001"
                            '   MessageBox.Show("Tran")
                            tW_FileName = tW_TranPk.Split("|")(nLeng)
                            System.Threading.Thread.Sleep(1000)
                            obgGroudWord.ReportProgress(20)

                            If oExpTran.C_SETbExpTran(tW_TranDate) = True Then
                                System.Threading.Thread.Sleep(1000)
                                obgGroudWord.ReportProgress(60)
                                oExpTran.C_GETbExpTranToFile(tW_TranDate)
                            End If

                            System.Threading.Thread.Sleep(1000)
                            obgGroudWord.ReportProgress(100)
                            System.Threading.Thread.Sleep(1000)

                        Case "002"
                            ' เตรียมข้อมูลลง temp
                            tW_FileName = tW_SalePk.Split("|")(nLeng)
                            System.Threading.Thread.Sleep(1000)
                            obgGroudWord.ReportProgress(20)

                            If oExpSale.C_SETbExpSale(tW_SaleDate) = True Then
                                ' Export to text file 
                                System.Threading.Thread.Sleep(1000)
                                obgGroudWord.ReportProgress(60)
                                oExpSale.C_GETbExpSalToFile(tW_SaleDate)
                            End If

                            System.Threading.Thread.Sleep(1000)
                            obgGroudWord.ReportProgress(100)
                            System.Threading.Thread.Sleep(1000)

                        Case "003"
                            '   MessageBox.Show("Pay")
                            tW_FileName = tW_PayPk.Split("|")(nLeng)
                            System.Threading.Thread.Sleep(1000)
                            obgGroudWord.ReportProgress(20)

                            If oExpPayment.C_SETbExpPayment(tW_PayDate) = True Then
                                System.Threading.Thread.Sleep(1000)
                                obgGroudWord.ReportProgress(60)
                                oExpPayment.C_GETbExpPaymentToFile(tW_PayDate)
                            End If

                            System.Threading.Thread.Sleep(1000)
                            obgGroudWord.ReportProgress(100)
                            System.Threading.Thread.Sleep(1000)
                    End Select
                    tNoGrid = ""
                End If
            Next

            nW_StatusDoWork = 0
            For nCheck As Integer = 0 To ogdExport.Rows.Count - 1
                'กรณีที่ Checkbox บน grid มีการเลือก
                If ogdExport.Rows(nCheck).Cells(1).Value = True Then
                    nW_StatusDoWork = 1
                End If
            Next


            If nW_StatusDoWork <> 0 Then
                ' True = Connect FTP + Export Success , False = Not Connect FTP + Export Error
                If W_GENbZipfile(oExp) = False Then
                    nW_StatusDoWork = 2
                End If
            End If

        Catch ex As Exception
        Finally
            tNoGrid = Nothing
        End Try
    End Sub

    Private Sub W_RUNxWordChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        Dim nLeng As Integer = 0
        Try
            Me.opgFrmExp.Value = e.ProgressPercentage
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1
            Me.olaStaExport.Text = tW_StatusExport.Split(";")(nLeng) & tW_FileName & " :  " & e.ProgressPercentage & " %"
            Me.opgFrmExp.Value = 100
            Me.olaStaExport.Refresh()
        Catch ex As Exception
        Finally
            nLeng = Nothing
        End Try
    End Sub

    Private Sub W_RUNxWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Try
            Dim tLogAda As String = ""
            Me.opgFrmExp.Value = 100
            '   oW_BackgroudWord = Nothing
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub ogdExport_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles ogdExport.CellValueChanged
        If e.ColumnIndex = 1 Then
            Dim bChk As Boolean = CBool(ogdExport.Rows(e.RowIndex).Cells(1).Value)

            Select Case (Convert.ToString(Convert.ToInt32(e.RowIndex) + 1))
                Case "1"
                    bW_ChkTran = bChk
                Case "2"
                    bW_ChkSale = bChk
                Case "3"
                    bW_ChkPay = bChk
            End Select
        End If
    End Sub

    Private Sub obgGroudWord_DoWork(sender As Object, e As DoWorkEventArgs) Handles obgGroudWord.DoWork
        Try

            W_SETxProcessExp()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub obgGroudWord_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles obgGroudWord.ProgressChanged

        opgFrmExp.Value = e.ProgressPercentage
        olaStaExport.Text = "กำลังส่งออก File " + tW_FileName + " " + CType(e.ProgressPercentage, String) + " %"
        olaStaExport.Refresh()
    End Sub

    Private Sub obgGroudWord_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles obgGroudWord.RunWorkerCompleted

        Dim nLeng As Integer = 0
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1
        If cExpTransaction.tC_WrieLog > 0 Then
            MessageBox.Show(tW_ExportMore.Split("|")(nLeng), "", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Select Case nW_StatusDoWork
                Case 0
                    MessageBox.Show(tW_SelectData.Split("|")(nLeng), "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case 1
                    MessageBox.Show(tW_ExpSuccess.Split("|")(nLeng), "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case 2
                    MessageBox.Show(tW_FTPErr.Split("|")(nLeng), "", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            End Select
        End If


        'Clear ค่าวันที่ ที่เก็บไว้สร้าง  Folder
        cExpTransaction.tC_WrieLog = 0
        cExpTransaction.tC_ShareTranDate = ""
        cExpSale.tC_SaleDate = ""
        cExpPayment.tC_Paydate = ""

    End Sub

    Public Function W_SETtTranDate(ByVal ptDate) As String
        Try
            Dim tConFormat As String = DateTime.ParseExact(ptDate, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim tDate As String = Convert.ToDateTime(tConFormat).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))

            Return tDate
        Catch ex As Exception
            Return ""
        End Try
    End Function


    ''' <summary>
    ''' สร้าง Table Running Number.
    ''' </summary>
    Public Sub C_CALxFindTable()
        Try
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal

            oSQL.Clear()
            oSQL.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine("WHERE object_id = OBJECT_ID(N'TCNTPdtRunning') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TCNTPdtRunning]")
            oSQL.AppendLine("([FNNum] [int] IDENTITY(1,1) NOT NULL,")
            oSQL.AppendLine("[FTRunningNumber] [varchar](50) NULL,")
            oSQL.AppendLine("[FTDocNo] [varchar](100) NULL")
            oSQL.AppendLine(")")
            oSQL.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)
        Catch ex As Exception

        End Try
    End Sub
End Class