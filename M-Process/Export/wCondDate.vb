Imports System.Data.SqlClient

Class wCondDate
    Property oW_Item As cExportTemplate.cItem
    Private oW_Database As New cDatabaseLocal

    Sub New(opItem As cExportTemplate.cItem)
        oW_Item = opItem
        ' This call is required by the designer.
        InitializeComponent()

        If IsDate(oW_Item.tDateFrom) And IsDate(oW_Item.tDateTo) Then
            Me.ockDateInterval.Checked = True
            Me.odtDateFrom.Value = CDate(oW_Item.tDateFrom)
            Me.odtDateTo.Value = CDate(oW_Item.tDateTo)
        Else
            Me.ockDateInterval.Checked = False
        End If

    End Sub

    Private Sub ocmOk_Click(sender As System.Object, e As System.EventArgs) Handles ocmOk.Click

        With oW_Item
            If Me.ockDateInterval.Checked Then
                .tDateFrom = Format(Me.odtDateFrom.Value, "yyyy-MM-dd")
                .tDateTo = Format(Me.odtDateTo.Value, "yyyy-MM-dd")
                .tConditon = cExportTemplate.C_GETtFmtCondition(Format(Me.odtDateFrom.Value, "dd/MM/yyyy"), Format(Me.odtDateTo.Value, "dd/MM/yyyy"), .tBchFrom, .tBchTo, .tBchSelect)
            Else
                .tConditon = cExportTemplate.C_GETtFmtCondition(ptBchFrom:= .tBchFrom, ptBchTo:= .tBchTo, ptBchSelect:= .tBchSelect)
                .tDateFrom = ""
                .tDateTo = ""
            End If
        End With

        Me.Close()
    End Sub

    Private Sub ockDateInterval_CheckedChanged(sender As Object, e As System.EventArgs) Handles ockDateInterval.CheckedChanged
        Select Case Me.ockDateInterval.Checked
            Case True
                Me.odtDateFrom.Enabled = True
                Me.odtDateTo.Enabled = True
            Case False
                Me.odtDateFrom.Enabled = False
                Me.odtDateTo.Enabled = False
        End Select
    End Sub

    Private Sub ocmCancel_Click(sender As System.Object, e As System.EventArgs) Handles ocmCancel.Click
        Me.Close()
    End Sub

    Private Sub wCondition_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cCNSP.SP_FrmSetCapControl(Me)
    End Sub

    Private Sub odtDateFrom_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles odtDateFrom.ValueChanged
        If DateDiff(DateInterval.Day, odtDateFrom.Value.Date, odtDateTo.Value.Date) < 1 Then
            odtDateTo.Value = odtDateFrom.Value
        End If
    End Sub

    Private Sub odtDateTo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles odtDateTo.ValueChanged
        If DateDiff(DateInterval.Day, odtDateTo.Value.Date, odtDateFrom.Value.Date) > 1 Then
            odtDateFrom.Value = odtDateTo.Value
        End If
    End Sub

End Class