﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wExpCashTnf
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wExpCashTnf))
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.ocmRefresh = New System.Windows.Forms.Button()
        Me.odtSalDate = New System.Windows.Forms.DateTimePicker()
        Me.olaSalDate = New System.Windows.Forms.Label()
        Me.ocmBrwBch = New System.Windows.Forms.Button()
        Me.otbBchCode = New System.Windows.Forms.TextBox()
        Me.olaBranch = New System.Windows.Forms.Label()
        Me.olaValStaExp = New System.Windows.Forms.Label()
        Me.ogdForm = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ocmProcess = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.olaStaExport = New System.Windows.Forms.Label()
        Me.opgForm = New System.Windows.Forms.ProgressBar()
        Me.omsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.omsUnLock = New System.Windows.Forms.ToolStripMenuItem()
        Me.ottForm = New System.Windows.Forms.ToolTip(Me.components)
        Me.opnForm.SuspendLayout()
        CType(Me.ogdForm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.omsForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnForm
        '
        Me.opnForm.BackColor = System.Drawing.SystemColors.Control
        Me.opnForm.Controls.Add(Me.ocmRefresh)
        Me.opnForm.Controls.Add(Me.odtSalDate)
        Me.opnForm.Controls.Add(Me.olaSalDate)
        Me.opnForm.Controls.Add(Me.ocmBrwBch)
        Me.opnForm.Controls.Add(Me.otbBchCode)
        Me.opnForm.Controls.Add(Me.olaBranch)
        Me.opnForm.Controls.Add(Me.olaValStaExp)
        Me.opnForm.Controls.Add(Me.ogdForm)
        Me.opnForm.Controls.Add(Me.ocmProcess)
        Me.opnForm.Controls.Add(Me.ocmClose)
        Me.opnForm.Controls.Add(Me.olaStaExport)
        Me.opnForm.Controls.Add(Me.opgForm)
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(0, 0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(849, 418)
        Me.opnForm.TabIndex = 0
        '
        'ocmRefresh
        '
        Me.ocmRefresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmRefresh.Location = New System.Drawing.Point(12, 376)
        Me.ocmRefresh.Name = "ocmRefresh"
        Me.ocmRefresh.Size = New System.Drawing.Size(100, 30)
        Me.ocmRefresh.TabIndex = 14
        Me.ocmRefresh.Tag = "2;รีเฟรช;Refresh"
        Me.ocmRefresh.Text = "Refresh"
        Me.ocmRefresh.UseVisualStyleBackColor = True
        '
        'odtSalDate
        '
        Me.odtSalDate.CustomFormat = "dd/MM/yyyy"
        Me.odtSalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.odtSalDate.Location = New System.Drawing.Point(371, 12)
        Me.odtSalDate.Name = "odtSalDate"
        Me.odtSalDate.Size = New System.Drawing.Size(100, 20)
        Me.odtSalDate.TabIndex = 13
        '
        'olaSalDate
        '
        Me.olaSalDate.AutoSize = True
        Me.olaSalDate.Location = New System.Drawing.Point(296, 15)
        Me.olaSalDate.Name = "olaSalDate"
        Me.olaSalDate.Size = New System.Drawing.Size(54, 13)
        Me.olaSalDate.TabIndex = 12
        Me.olaSalDate.Tag = "2;วันที่เอกสาร;Doc Date"
        Me.olaSalDate.Text = "Sale Date"
        '
        'ocmBrwBch
        '
        Me.ocmBrwBch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmBrwBch.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ocmBrwBch.Image = CType(resources.GetObject("ocmBrwBch.Image"), System.Drawing.Image)
        Me.ocmBrwBch.Location = New System.Drawing.Point(175, 12)
        Me.ocmBrwBch.Name = "ocmBrwBch"
        Me.ocmBrwBch.Size = New System.Drawing.Size(23, 20)
        Me.ocmBrwBch.TabIndex = 11
        Me.ocmBrwBch.UseVisualStyleBackColor = True
        '
        'otbBchCode
        '
        Me.otbBchCode.Location = New System.Drawing.Point(69, 12)
        Me.otbBchCode.Name = "otbBchCode"
        Me.otbBchCode.Size = New System.Drawing.Size(100, 20)
        Me.otbBchCode.TabIndex = 10
        '
        'olaBranch
        '
        Me.olaBranch.AutoSize = True
        Me.olaBranch.Location = New System.Drawing.Point(9, 15)
        Me.olaBranch.Name = "olaBranch"
        Me.olaBranch.Size = New System.Drawing.Size(41, 13)
        Me.olaBranch.TabIndex = 9
        Me.olaBranch.Tag = "2;สาขา;Branch"
        Me.olaBranch.Text = "Branch"
        '
        'olaValStaExp
        '
        Me.olaValStaExp.AutoSize = True
        Me.olaValStaExp.Location = New System.Drawing.Point(66, 323)
        Me.olaValStaExp.Name = "olaValStaExp"
        Me.olaValStaExp.Size = New System.Drawing.Size(0, 13)
        Me.olaValStaExp.TabIndex = 8
        '
        'ogdForm
        '
        Me.ogdForm.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdForm.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.None
        Me.ogdForm.AutoGenerateColumns = False
        Me.ogdForm.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdForm.ColumnInfo = resources.GetString("ogdForm.ColumnInfo")
        Me.ogdForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdForm.Location = New System.Drawing.Point(12, 43)
        Me.ogdForm.Name = "ogdForm"
        Me.ogdForm.Rows.Count = 2
        Me.ogdForm.Rows.DefaultSize = 19
        Me.ogdForm.Rows.MaxSize = 50
        Me.ogdForm.Rows.MinSize = 21
        Me.ogdForm.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ogdForm.ScrollOptions = C1.Win.C1FlexGrid.ScrollFlags.AlwaysVisible
        Me.ogdForm.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdForm.Size = New System.Drawing.Size(824, 275)
        Me.ogdForm.StyleInfo = resources.GetString("ogdForm.StyleInfo")
        Me.ogdForm.TabIndex = 7
        Me.ogdForm.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ocmProcess
        '
        Me.ocmProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmProcess.Location = New System.Drawing.Point(630, 376)
        Me.ocmProcess.Name = "ocmProcess"
        Me.ocmProcess.Size = New System.Drawing.Size(100, 30)
        Me.ocmProcess.TabIndex = 5
        Me.ocmProcess.Tag = "2;ดำเนินการ;Process"
        Me.ocmProcess.Text = "Process"
        Me.ocmProcess.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(736, 376)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose.TabIndex = 6
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'olaStaExport
        '
        Me.olaStaExport.AutoSize = True
        Me.olaStaExport.BackColor = System.Drawing.Color.Transparent
        Me.olaStaExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaStaExport.Location = New System.Drawing.Point(9, 321)
        Me.olaStaExport.Name = "olaStaExport"
        Me.olaStaExport.Size = New System.Drawing.Size(51, 16)
        Me.olaStaExport.TabIndex = 2
        Me.olaStaExport.Tag = "2;สถานะ ; Status"
        Me.olaStaExport.Text = "Status :"
        '
        'opgForm
        '
        Me.opgForm.Location = New System.Drawing.Point(12, 339)
        Me.opgForm.Name = "opgForm"
        Me.opgForm.Size = New System.Drawing.Size(824, 19)
        Me.opgForm.TabIndex = 3
        '
        'omsForm
        '
        Me.omsForm.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.omsUnLock})
        Me.omsForm.Name = "omsForm"
        Me.omsForm.Size = New System.Drawing.Size(112, 26)
        '
        'omsUnLock
        '
        Me.omsUnLock.Name = "omsUnLock"
        Me.omsUnLock.Size = New System.Drawing.Size(111, 22)
        Me.omsUnLock.Text = "Unlock"
        '
        'wExpCashTnf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmClose
        Me.ClientSize = New System.Drawing.Size(849, 418)
        Me.Controls.Add(Me.opnForm)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExpCashTnf"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออกบันทึกเงินฝาก;Export Cash Transfer"
        Me.Text = "Export"
        Me.opnForm.ResumeLayout(False)
        Me.opnForm.PerformLayout()
        CType(Me.ogdForm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.omsForm.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents opnForm As System.Windows.Forms.Panel
    Friend WithEvents opgForm As System.Windows.Forms.ProgressBar
    Friend WithEvents olaStaExport As System.Windows.Forms.Label
    Friend WithEvents ocmClose As System.Windows.Forms.Button
    Friend WithEvents ocmProcess As System.Windows.Forms.Button
    Friend WithEvents ottForm As System.Windows.Forms.ToolTip
    Friend WithEvents omsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents omsUnLock As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ogdForm As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents olaValStaExp As System.Windows.Forms.Label
    Friend WithEvents otbBchCode As TextBox
    Friend WithEvents olaBranch As Label
    Friend WithEvents odtSalDate As DateTimePicker
    Friend WithEvents olaSalDate As Label
    Friend WithEvents ocmBrwBch As Button
    Friend WithEvents ocmRefresh As Button
End Class
