﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wExports2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ogdExport = New System.Windows.Forms.DataGridView()
        Me.opgFrmExp = New System.Windows.Forms.ProgressBar()
        Me.ocmProcess = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.olaStaExport = New System.Windows.Forms.Label()
        Me.obgGroudWord = New System.ComponentModel.BackgroundWorker()
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ogdExport
        '
        Me.ogdExport.AllowUserToAddRows = False
        Me.ogdExport.AllowUserToDeleteRows = False
        Me.ogdExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdExport.Location = New System.Drawing.Point(15, 12)
        Me.ogdExport.Name = "ogdExport"
        Me.ogdExport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ogdExport.Size = New System.Drawing.Size(593, 249)
        Me.ogdExport.TabIndex = 0
        '
        'opgFrmExp
        '
        Me.opgFrmExp.Location = New System.Drawing.Point(12, 283)
        Me.opgFrmExp.Name = "opgFrmExp"
        Me.opgFrmExp.Size = New System.Drawing.Size(593, 21)
        Me.opgFrmExp.TabIndex = 1
        '
        'ocmProcess
        '
        Me.ocmProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmProcess.Location = New System.Drawing.Point(399, 310)
        Me.ocmProcess.Name = "ocmProcess"
        Me.ocmProcess.Size = New System.Drawing.Size(100, 30)
        Me.ocmProcess.TabIndex = 7
        Me.ocmProcess.Tag = "2;ดำเนินการ;Process"
        Me.ocmProcess.Text = "Process"
        Me.ocmProcess.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(505, 310)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(100, 30)
        Me.ocmClose.TabIndex = 8
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'olaStaExport
        '
        Me.olaStaExport.AutoSize = True
        Me.olaStaExport.BackColor = System.Drawing.Color.Transparent
        Me.olaStaExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaStaExport.Location = New System.Drawing.Point(12, 264)
        Me.olaStaExport.Name = "olaStaExport"
        Me.olaStaExport.Size = New System.Drawing.Size(51, 16)
        Me.olaStaExport.TabIndex = 9
        Me.olaStaExport.Tag = "2;สถานะ ; Status"
        Me.olaStaExport.Text = "Status :"
        '
        'obgGroudWord
        '
        Me.obgGroudWord.WorkerReportsProgress = True
        '
        'wExports2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(618, 350)
        Me.Controls.Add(Me.olaStaExport)
        Me.Controls.Add(Me.ocmProcess)
        Me.Controls.Add(Me.ocmClose)
        Me.Controls.Add(Me.opgFrmExp)
        Me.Controls.Add(Me.ogdExport)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExports2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออก;Export"
        Me.Text = "Export"
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ogdExport As DataGridView
    Friend WithEvents opgFrmExp As ProgressBar
    Friend WithEvents ocmProcess As Button
    Friend WithEvents ocmClose As Button
    Friend WithEvents olaStaExport As Label
    Friend WithEvents obgGroudWord As System.ComponentModel.BackgroundWorker
End Class
