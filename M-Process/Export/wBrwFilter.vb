﻿Imports System.Text

Public Class wBrwFilter

    Property oW_Datatable As DataTable
    Property tW_Result As String = ""

    'Private Const tW_CaptionFilter As String = "Code,Name"
    'Private Const tW_CaptionFilterThai As String = "รหัส,ชื่อ"
    Private oW_FilterRow As cFilterRow

    Private Sub W_SETxCaptionGridFilter()
        Dim tCapFilter As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '1:thai
            'tCapFilter = tW_CaptionFilter
            tCapFilter = cCNVB.tVB_CaptionFilter '*CH 05-12-2014
        Else
            'tCapFilter = tW_CaptionFilterThai
            tCapFilter = cCNVB.tVB_CaptionFilterThai '*CH 05-12-2014
        End If

        Dim nCol As Integer = 0 'Start Column 0
        For Each tCap In tCapFilter.Split(",")
            With Me.ogdForm
                .Cols(nCol).TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
                .Cols(nCol).Caption = tCap
            End With
            nCol += 1
        Next

        Select Case tCapFilter.Split(",").Length
            Case 2
                Me.ogdForm.Cols(0).Width = 150
                Me.ogdForm.Cols(1).Width = 350
            Case 3
                Me.ogdForm.Cols(0).Width = 200
                Me.ogdForm.Cols(1).Width = 150
                Me.ogdForm.Cols(2).Width = 200
            Case 4
                Me.ogdForm.Cols(0).Width = 150
                Me.ogdForm.Cols(1).Width = 150
                Me.ogdForm.Cols(2).Width = 100
                Me.ogdForm.Cols(3).Width = 100
                Me.ogdForm.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
        End Select
        Me.ogdForm.ShowCursor = True

    End Sub

    Private Sub wBrwFilter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cCNSP.SP_FrmSetCapControl(Me)
        Me.ogdForm.DataSource = oW_Datatable
        Me.oW_FilterRow = New cFilterRow(Me.ogdForm)
        Me.W_SETxCaptionGridFilter()
        SendKeys.Send("{TAB}")
    End Sub

    Private Sub ocmOK_Click(sender As System.Object, e As System.EventArgs) Handles ocmOK.Click
        If Me.ogdForm.RowSel > 1 Then
            tW_Result = Me.ogdForm.Item(Me.ogdForm.RowSel, 0)
            If tW_Result.Length > 0 Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End If
        End If
    End Sub

    Private Sub ocmRef_Click(sender As System.Object, e As System.EventArgs) Handles ocmRef.Click
        'Default Row=1,Col=0
        oW_FilterRow.Clear()
        Me.ogdForm.Select(1, 0)
    End Sub

    Private Sub ogdForm_BeforeEdit(sender As Object, e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdForm.BeforeEdit
        If e.Row > 1 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub ocmCancel_Click(sender As System.Object, e As System.EventArgs) Handles ocmCancel.Click
        Me.Close()
    End Sub

End Class