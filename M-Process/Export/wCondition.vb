Imports System.Data.SqlClient

Class wCondition

    Property oW_Item As cExportTemplate.cItem
    Private oW_Database As New cDatabaseLocal

    Sub New(opItem As cExportTemplate.cItem)
        oW_Item = opItem
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Select Case oW_Item.nMode '0:All,1:Interval,2:Select
            Case 0
                Me.orbSellectAll.Checked = True
            Case 1
                Me.orbCustom.Checked = True
                Me.orbDataInterval.Checked = True
                Me.otbFrom.Text = oW_Item.tBchFrom
                Me.otbTo.Text = oW_Item.tBchTo
            Case 2
                Me.orbCustom.Checked = True
                Me.orbSelect.Checked = True
                Me.otbSelect.Text = oW_Item.tBchSelect
        End Select

        If IsDate(oW_Item.tDateFrom) And IsDate(oW_Item.tDateTo) Then
            Me.ockDateInterval.Checked = True
            Me.odtDateFrom.Value = CDate(oW_Item.tDateFrom)
            Me.odtDateTo.Value = CDate(oW_Item.tDateTo)
        Else
            Me.ockDateInterval.Checked = False
        End If

    End Sub

    Private Sub ocmOk_Click(sender As System.Object, e As System.EventArgs) Handles ocmOk.Click

        If Me.orbCustom.Checked Then
            If Me.orbDataInterval.Checked Then
                If Me.otbFrom.Text.Length = 0 Or Me.otbTo.Text.Length = 0 Then
                    cCNSP.SP_MSGnShowing(cCNMS.tMS_CN105, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
                    Exit Sub
                End If
            Else
                If Me.otbSelect.Text.Length = 0 Then
                    cCNSP.SP_MSGnShowing(cCNMS.tMS_CN106, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
                    Exit Sub
                End If
            End If
        End If

        With oW_Item
            If Me.orbSellectAll.Checked Then '0:All,1:Interval,2:Select
                .nMode = 0
                .tBchFrom = ""
                .tBchTo = ""
                .tBchSelect = ""
            Else
                If Me.orbDataInterval.Checked Then
                    .nMode = 1
                    .tBchFrom = Me.otbFrom.Text
                    .tBchTo = Me.otbTo.Text
                    .tBchSelect = ""
                Else
                    .nMode = 2
                    .tBchFrom = ""
                    .tBchTo = ""
                    .tBchSelect = Me.otbSelect.Text
                End If
            End If

            If Me.ockDateInterval.Checked Then
                .tDateFrom = Format(Me.odtDateFrom.Value, "yyyy-MM-dd")
                .tDateTo = Format(Me.odtDateTo.Value, "yyyy-MM-dd")
                .tConditon = cExportTemplate.C_GETtFmtCondition(Format(Me.odtDateFrom.Value, "dd/MM/yyyy"), Format(Me.odtDateTo.Value, "dd/MM/yyyy"), .tBchFrom, .tBchTo, .tBchSelect)
            Else
                .tConditon = cExportTemplate.C_GETtFmtCondition(ptBchFrom:=.tBchFrom, ptBchTo:=.tBchTo, ptBchSelect:=.tBchSelect)
                .tDateFrom = ""
                .tDateTo = ""
            End If

        End With

        Me.Close()
    End Sub

    Private Sub orbCustom_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles orbCustom.CheckedChanged
        Select Case Me.orbCustom.Checked
            Case True
                Me.ogbCode.Enabled = True
            Case False
                Me.ogbCode.Enabled = False
        End Select
    End Sub

    Private Sub ockDateInterval_CheckedChanged(sender As Object, e As System.EventArgs) Handles ockDateInterval.CheckedChanged
        Select Case Me.ockDateInterval.Checked
            Case True
                Me.odtDateFrom.Enabled = True
                Me.odtDateTo.Enabled = True
            Case False
                Me.odtDateFrom.Enabled = False
                Me.odtDateTo.Enabled = False
        End Select
    End Sub

    Private Sub ocmCancel_Click(sender As System.Object, e As System.EventArgs) Handles ocmCancel.Click
        Me.Close()
    End Sub

    Private Sub wCondition_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cCNSP.SP_FrmSetCapControl(Me)
    End Sub

    Private Sub ocmSelect_Click(sender As System.Object, e As System.EventArgs) Handles ocmSelect.Click, ocmToCode.Click, ocmFromCode.Click

        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
            Exit Sub
        End If

        Dim oDataTemp = New DataTable
        Select Case oW_Item.tTable
            Case "TCNMCst"
                oDataTemp.TableName = "TCNMCst"
                Dim tSql As String = "SELECT FTCstCode,FTCstName FROM TCNMCst"
                oDataTemp = oW_Database.C_CALoExecuteReader(tSql, oDataTemp.TableName)

                Dim oFrmBrw As New wBrwFilter
                With oFrmBrw
                    .Tag = "2;�١���;Customer"
                    .oW_Datatable = oDataTemp
                    .ShowDialog()
                    If .DialogResult = Windows.Forms.DialogResult.OK Then
                        Select Case sender.name
                            Case "ocmFromCode"
                                Me.otbFrom.Text = .tW_Result
                            Case "ocmToCode"
                                Me.otbTo.Text = .tW_Result
                            Case "ocmSelect"
                                Me.otbSelect.Text = .tW_Result
                        End Select
                    End If
                    oDataTemp.Dispose()
                    .Dispose()
                End With
            Case "TPSTSalHD"
                'oDataTemp.TableName = "TCNMBranch"
                'Dim tSql As String = "SELECT FTBchCode,FTBchName FROM TCNMBranch"
                oDataTemp.TableName = "TPSTSalHD"
                Dim tSql As String = "SELECT DISTINCT FTShdDocNo,FDShdDocDate,FTShdDocTime,CONVERT(varchar, CAST(FCShdGrand AS money), 1) AS FCShdGrand "
                tSql &= "FROM TPSTSalHD "
                tSql &= " WHERE FTShdDocType IN ('1','9')"  'CHAIYA 2016-08-02
                'tSql &= "WHERE (TPSTSalHD.FNShdStaDocAct='1') AND (TPSTSalHD.FTShdStaPrcDoc='1') AND NOT(TPSTSalHD.FTShdDocVatFull='' OR TPSTSalHD.FTShdDocVatFull Is Null)"
                oDataTemp = oW_Database.C_CALoExecuteReader(tSql, oDataTemp.TableName)

                Dim oFrmBrw As New wBrwFilter
                With oFrmBrw
                    .Tag = "2;��â��;Sale and Return"
                    .oW_Datatable = oDataTemp
                    .ShowDialog()
                    If .DialogResult = Windows.Forms.DialogResult.OK Then
                        Select Case sender.name
                            Case "ocmFromCode"
                                Me.otbFrom.Text = .tW_Result
                            Case "ocmToCode"
                                Me.otbTo.Text = .tW_Result
                            Case "ocmSelect"
                                Me.otbSelect.Text = .tW_Result
                        End Select
                    End If
                    oDataTemp.Dispose()
                    .Dispose()
                End With
            Case "TCNTPmtHD"
                oDataTemp.TableName = "TCNTPmtHD"
                Dim tSql As String = "SELECT FTPmhCode,FTPmhName FROM TCNTPmtHD WHERE ISNULL(FTPmhStaPrcDoc,'')='1' AND ISNULL(FNPmhStaAct,0)=1"
                'If ockDateInterval.Checked = True Then
                '    tSql &= " AND CONVERT(VARCHAR(10),FDPmhDStart,120) BETWEEN '" & Format(Me.odtDateFrom.Value, "yyyy-MM-dd") & "' AND '" & Format(Me.odtDateTo.Value, "yyyy-MM-dd") & "'"
                'End If
                tSql &= " ORDER BY FTPmhCode"
                oDataTemp = oW_Database.C_CALoExecuteReader(tSql, oDataTemp.TableName)

                Dim oFrmBrw As New wBrwFilter
                With oFrmBrw
                    .Tag = "2;�������;Promotion"
                    .oW_Datatable = oDataTemp
                    .ShowDialog()
                    If .DialogResult = Windows.Forms.DialogResult.OK Then
                        Select Case sender.name
                            Case "ocmFromCode"
                                Me.otbFrom.Text = .tW_Result
                            Case "ocmToCode"
                                Me.otbTo.Text = .tW_Result
                            Case "ocmSelect"
                                Me.otbSelect.Text = .tW_Result
                        End Select
                    End If
                    oDataTemp.Dispose()
                    .Dispose()
                End With
            Case "EOD"
                oDataTemp.TableName = "TPSTSalHD"
                Dim tSql As String = "SELECT DISTINCT Shd.FTLogCode,Shd.FDShdDocDate,Bch.FTBchName "
                tSql &= "FROM TPSTSalHD Shd INNER JOIN TCNMBranch Bch ON Shd.FTBchCode = Bch.FTBchCode "
                oDataTemp = oW_Database.C_CALoExecuteReader(tSql, oDataTemp.TableName)

                Dim oFrmBrw As New wBrwFilter
                With oFrmBrw
                    .Tag = "2;�ͺ��â��;Shift sale"
                    .oW_Datatable = oDataTemp
                    .ShowDialog()
                    If .DialogResult = Windows.Forms.DialogResult.OK Then
                        Select Case sender.name
                            Case "ocmFromCode"
                                Me.otbFrom.Text = .tW_Result
                            Case "ocmToCode"
                                Me.otbTo.Text = .tW_Result
                            Case "ocmSelect"
                                Me.otbSelect.Text = .tW_Result
                        End Select
                    End If
                    oDataTemp.Dispose()
                    .Dispose()
                End With
            Case "TCNTPdtAjp" 'Price off '*CH 21-01-2015
                oDataTemp.TableName = "TCNTPdtAjpHD"
                Dim tSql As String = "SELECT FTIphDocNo,FDIphAffect "
                tSql &= "FROM TCNTPdtAjpHD WHERE FTIphPriType='2' ORDER BY FTIphDocNo DESC"
                oDataTemp = oW_Database.C_CALoExecuteReader(tSql, oDataTemp.TableName)

                Dim oFrmBrw As New wBrwFilter
                With oFrmBrw
                    .Tag = "2;������� (price off);Promotion (price off)"
                    .oW_Datatable = oDataTemp
                    .ShowDialog()
                    If .DialogResult = Windows.Forms.DialogResult.OK Then
                        Select Case sender.name
                            Case "ocmFromCode"
                                Me.otbFrom.Text = .tW_Result
                            Case "ocmToCode"
                                Me.otbTo.Text = .tW_Result
                            Case "ocmSelect"
                                Me.otbSelect.Text = .tW_Result
                        End Select
                    End If
                    oDataTemp.Dispose()
                    .Dispose()
                End With
            Case "Deposit"
                'oDataTemp.TableName = "TCNMBranch"
                'Dim tSql As String = "SELECT FTBchCode,FTBchName FROM TCNMBranch"
                oDataTemp.TableName = "TPSTSalHD"
                Dim tSql As String = "SELECT DISTINCT FTShdDocNo,FDShdDocDate,FTShdDocTime,CONVERT(varchar, CAST(FCShdGrand AS money), 1) AS FCShdGrand "
                tSql &= "FROM TPSTSalHD "
                tSql &= " WHERE FTShdAEType IN ('15','16','19','17','18','20')"
                'tSql &= "WHERE (TPSTSalHD.FNShdStaDocAct='1') AND (TPSTSalHD.FTShdStaPrcDoc='1') AND NOT(TPSTSalHD.FTShdDocVatFull='' OR TPSTSalHD.FTShdDocVatFull Is Null)"
                oDataTemp = oW_Database.C_CALoExecuteReader(tSql, oDataTemp.TableName)

                Dim oFrmBrw As New wBrwFilter
                With oFrmBrw
                    .Tag = "2;Deposit;Deposit"
                    .oW_Datatable = oDataTemp
                    .ShowDialog()
                    If .DialogResult = Windows.Forms.DialogResult.OK Then
                        Select Case sender.name
                            Case "ocmFromCode"
                                Me.otbFrom.Text = .tW_Result
                            Case "ocmToCode"
                                Me.otbTo.Text = .tW_Result
                            Case "ocmSelect"
                                Me.otbSelect.Text = .tW_Result
                        End Select
                    End If
                    oDataTemp.Dispose()
                    .Dispose()
                End With
        End Select

    End Sub

    Private Sub orbDataInterval_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles orbDataInterval.CheckedChanged
        If Me.orbDataInterval.Checked = True Then
            Me.ocmSelect.Enabled = False
            Me.ocmFromCode.Enabled = True
            Me.ocmToCode.Enabled = True
            Me.otbSelect.Text = ""
        End If
    End Sub

    Private Sub orbSelect_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles orbSelect.CheckedChanged
        If Me.orbSelect.Checked = True Then
            Me.ocmSelect.Enabled = True
            Me.ocmFromCode.Enabled = False
            Me.ocmToCode.Enabled = False
            Me.otbFrom.Text = ""
            Me.otbTo.Text = ""
        End If
    End Sub

    Private Sub orbSellectAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles orbSellectAll.CheckedChanged
        Me.otbFrom.Text = ""
        Me.otbTo.Text = ""
        Me.otbSelect.Text = ""
    End Sub

    Private Sub odtDateFrom_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles odtDateFrom.ValueChanged
        If DateDiff(DateInterval.Day, odtDateFrom.Value.Date, odtDateTo.Value.Date) < 1 Then
            odtDateTo.Value = odtDateFrom.Value
        End If
    End Sub

    Private Sub odtDateTo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles odtDateTo.ValueChanged
        If DateDiff(DateInterval.Day, odtDateTo.Value.Date, odtDateFrom.Value.Date) > 1 Then
            odtDateFrom.Value = odtDateTo.Value
        End If
    End Sub

End Class