<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wCondition
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XpGradientPanel1 = New System.Windows.Forms.Panel()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.ockDateInterval = New System.Windows.Forms.CheckBox()
        Me.ogbCondition = New System.Windows.Forms.GroupBox()
        Me.orbSellectAll = New System.Windows.Forms.RadioButton()
        Me.orbCustom = New System.Windows.Forms.RadioButton()
        Me.ocmOk = New System.Windows.Forms.Button()
        Me.ogbCode = New System.Windows.Forms.GroupBox()
        Me.ocmSelect = New System.Windows.Forms.Button()
        Me.ocmToCode = New System.Windows.Forms.Button()
        Me.ocmFromCode = New System.Windows.Forms.Button()
        Me.olaToCode = New System.Windows.Forms.Label()
        Me.olaFromCode = New System.Windows.Forms.Label()
        Me.otbSelect = New System.Windows.Forms.TextBox()
        Me.otbTo = New System.Windows.Forms.TextBox()
        Me.otbFrom = New System.Windows.Forms.TextBox()
        Me.orbSelect = New System.Windows.Forms.RadioButton()
        Me.orbDataInterval = New System.Windows.Forms.RadioButton()
        Me.ogbDateInerval = New System.Windows.Forms.GroupBox()
        Me.odtDateTo = New System.Windows.Forms.DateTimePicker()
        Me.odtDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.olaDateTo = New System.Windows.Forms.Label()
        Me.olaDateFrom = New System.Windows.Forms.Label()
        Me.XpGradientPanel1.SuspendLayout()
        Me.ogbCondition.SuspendLayout()
        Me.ogbCode.SuspendLayout()
        Me.ogbDateInerval.SuspendLayout()
        Me.SuspendLayout()
        '
        'XpGradientPanel1
        '
        Me.XpGradientPanel1.Controls.Add(Me.ocmCancel)
        Me.XpGradientPanel1.Controls.Add(Me.ockDateInterval)
        Me.XpGradientPanel1.Controls.Add(Me.ogbCondition)
        Me.XpGradientPanel1.Controls.Add(Me.ocmOk)
        Me.XpGradientPanel1.Controls.Add(Me.ogbCode)
        Me.XpGradientPanel1.Controls.Add(Me.ogbDateInerval)
        Me.XpGradientPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XpGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.XpGradientPanel1.Name = "XpGradientPanel1"
        Me.XpGradientPanel1.Size = New System.Drawing.Size(501, 312)
        Me.XpGradientPanel1.TabIndex = 0
        '
        'ocmCancel
        '
        Me.ocmCancel.BackColor = System.Drawing.Color.Transparent
        Me.ocmCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmCancel.Location = New System.Drawing.Point(387, 272)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(100, 30)
        Me.ocmCancel.TabIndex = 8
        Me.ocmCancel.Tag = "2;¡��ԡ;Cancel"
        Me.ocmCancel.Text = "Cancel"
        Me.ocmCancel.UseVisualStyleBackColor = False
        '
        'ockDateInterval
        '
        Me.ockDateInterval.AutoSize = True
        Me.ockDateInterval.Location = New System.Drawing.Point(22, 72)
        Me.ockDateInterval.Name = "ockDateInterval"
        Me.ockDateInterval.Size = New System.Drawing.Size(15, 14)
        Me.ockDateInterval.TabIndex = 0
        Me.ockDateInterval.Tag = ""
        Me.ockDateInterval.UseVisualStyleBackColor = True
        '
        'ogbCondition
        '
        Me.ogbCondition.BackColor = System.Drawing.Color.Transparent
        Me.ogbCondition.Controls.Add(Me.orbSellectAll)
        Me.ogbCondition.Controls.Add(Me.orbCustom)
        Me.ogbCondition.Location = New System.Drawing.Point(13, 9)
        Me.ogbCondition.Name = "ogbCondition"
        Me.ogbCondition.Size = New System.Drawing.Size(474, 53)
        Me.ogbCondition.TabIndex = 0
        Me.ogbCondition.TabStop = False
        Me.ogbCondition.Tag = ""
        Me.ogbCondition.Text = "Condition branch"
        '
        'orbSellectAll
        '
        Me.orbSellectAll.AutoSize = True
        Me.orbSellectAll.Checked = True
        Me.orbSellectAll.Location = New System.Drawing.Point(14, 21)
        Me.orbSellectAll.Name = "orbSellectAll"
        Me.orbSellectAll.Size = New System.Drawing.Size(81, 20)
        Me.orbSellectAll.TabIndex = 0
        Me.orbSellectAll.TabStop = True
        Me.orbSellectAll.Tag = "2;���͡������;Select all"
        Me.orbSellectAll.Text = "Select all"
        Me.orbSellectAll.UseVisualStyleBackColor = True
        '
        'orbCustom
        '
        Me.orbCustom.AutoSize = True
        Me.orbCustom.Location = New System.Drawing.Point(231, 21)
        Me.orbCustom.Name = "orbCustom"
        Me.orbCustom.Size = New System.Drawing.Size(71, 20)
        Me.orbCustom.TabIndex = 0
        Me.orbCustom.Tag = "2;��˹��ͧ;Custom"
        Me.orbCustom.Text = "Custom"
        Me.orbCustom.UseVisualStyleBackColor = True
        '
        'ocmOk
        '
        Me.ocmOk.BackColor = System.Drawing.Color.Transparent
        Me.ocmOk.Location = New System.Drawing.Point(280, 272)
        Me.ocmOk.Name = "ocmOk"
        Me.ocmOk.Size = New System.Drawing.Size(100, 30)
        Me.ocmOk.TabIndex = 9
        Me.ocmOk.Tag = "2;��ŧ;OK"
        Me.ocmOk.Text = "Ok"
        Me.ocmOk.UseVisualStyleBackColor = False
        '
        'ogbCode
        '
        Me.ogbCode.BackColor = System.Drawing.Color.Transparent
        Me.ogbCode.Controls.Add(Me.ocmSelect)
        Me.ogbCode.Controls.Add(Me.ocmToCode)
        Me.ogbCode.Controls.Add(Me.ocmFromCode)
        Me.ogbCode.Controls.Add(Me.olaToCode)
        Me.ogbCode.Controls.Add(Me.olaFromCode)
        Me.ogbCode.Controls.Add(Me.otbSelect)
        Me.ogbCode.Controls.Add(Me.otbTo)
        Me.ogbCode.Controls.Add(Me.otbFrom)
        Me.ogbCode.Controls.Add(Me.orbSelect)
        Me.ogbCode.Controls.Add(Me.orbDataInterval)
        Me.ogbCode.Enabled = False
        Me.ogbCode.Location = New System.Drawing.Point(230, 70)
        Me.ogbCode.Name = "ogbCode"
        Me.ogbCode.Size = New System.Drawing.Size(257, 190)
        Me.ogbCode.TabIndex = 5
        Me.ogbCode.TabStop = False
        Me.ogbCode.Tag = ""
        Me.ogbCode.Text = "Branch"
        '
        'ocmSelect
        '
        Me.ocmSelect.Location = New System.Drawing.Point(214, 152)
        Me.ocmSelect.Name = "ocmSelect"
        Me.ocmSelect.Size = New System.Drawing.Size(28, 23)
        Me.ocmSelect.TabIndex = 2
        Me.ocmSelect.Text = "..."
        Me.ocmSelect.UseVisualStyleBackColor = True
        '
        'ocmToCode
        '
        Me.ocmToCode.Location = New System.Drawing.Point(214, 95)
        Me.ocmToCode.Name = "ocmToCode"
        Me.ocmToCode.Size = New System.Drawing.Size(28, 23)
        Me.ocmToCode.TabIndex = 2
        Me.ocmToCode.Text = "..."
        Me.ocmToCode.UseVisualStyleBackColor = True
        '
        'ocmFromCode
        '
        Me.ocmFromCode.Location = New System.Drawing.Point(214, 55)
        Me.ocmFromCode.Name = "ocmFromCode"
        Me.ocmFromCode.Size = New System.Drawing.Size(28, 23)
        Me.ocmFromCode.TabIndex = 2
        Me.ocmFromCode.Text = "..." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.ocmFromCode.UseVisualStyleBackColor = True
        '
        'olaToCode
        '
        Me.olaToCode.AutoSize = True
        Me.olaToCode.Location = New System.Drawing.Point(30, 80)
        Me.olaToCode.Name = "olaToCode"
        Me.olaToCode.Size = New System.Drawing.Size(59, 16)
        Me.olaToCode.TabIndex = 1
        Me.olaToCode.Tag = "2;�֧����;To code"
        Me.olaToCode.Text = "To code"
        '
        'olaFromCode
        '
        Me.olaFromCode.AutoSize = True
        Me.olaFromCode.Location = New System.Drawing.Point(30, 39)
        Me.olaFromCode.Name = "olaFromCode"
        Me.olaFromCode.Size = New System.Drawing.Size(73, 16)
        Me.olaFromCode.TabIndex = 1
        Me.olaFromCode.Tag = "2;�ҡ����;Form code"
        Me.olaFromCode.Text = "Form code"
        '
        'otbSelect
        '
        Me.otbSelect.BackColor = System.Drawing.Color.NavajoWhite
        Me.otbSelect.Location = New System.Drawing.Point(33, 153)
        Me.otbSelect.Name = "otbSelect"
        Me.otbSelect.ReadOnly = True
        Me.otbSelect.Size = New System.Drawing.Size(180, 22)
        Me.otbSelect.TabIndex = 1
        '
        'otbTo
        '
        Me.otbTo.BackColor = System.Drawing.Color.NavajoWhite
        Me.otbTo.Location = New System.Drawing.Point(33, 96)
        Me.otbTo.Name = "otbTo"
        Me.otbTo.ReadOnly = True
        Me.otbTo.Size = New System.Drawing.Size(180, 22)
        Me.otbTo.TabIndex = 1
        '
        'otbFrom
        '
        Me.otbFrom.BackColor = System.Drawing.Color.NavajoWhite
        Me.otbFrom.Location = New System.Drawing.Point(33, 55)
        Me.otbFrom.Name = "otbFrom"
        Me.otbFrom.ReadOnly = True
        Me.otbFrom.Size = New System.Drawing.Size(180, 22)
        Me.otbFrom.TabIndex = 1
        '
        'orbSelect
        '
        Me.orbSelect.AutoSize = True
        Me.orbSelect.Location = New System.Drawing.Point(14, 127)
        Me.orbSelect.Name = "orbSelect"
        Me.orbSelect.Size = New System.Drawing.Size(98, 20)
        Me.orbSelect.TabIndex = 0
        Me.orbSelect.TabStop = True
        Me.orbSelect.Tag = "2;���͡����;Select code"
        Me.orbSelect.Text = "Select code"
        Me.orbSelect.UseVisualStyleBackColor = True
        '
        'orbDataInterval
        '
        Me.orbDataInterval.AutoSize = True
        Me.orbDataInterval.Checked = True
        Me.orbDataInterval.Location = New System.Drawing.Point(14, 16)
        Me.orbDataInterval.Name = "orbDataInterval"
        Me.orbDataInterval.Size = New System.Drawing.Size(99, 20)
        Me.orbDataInterval.TabIndex = 0
        Me.orbDataInterval.TabStop = True
        Me.orbDataInterval.Tag = "2;��ǧ������;Interval data"
        Me.orbDataInterval.Text = "Interval data"
        Me.orbDataInterval.UseVisualStyleBackColor = True
        '
        'ogbDateInerval
        '
        Me.ogbDateInerval.BackColor = System.Drawing.Color.Transparent
        Me.ogbDateInerval.Controls.Add(Me.odtDateTo)
        Me.ogbDateInerval.Controls.Add(Me.odtDateFrom)
        Me.ogbDateInerval.Controls.Add(Me.olaDateTo)
        Me.ogbDateInerval.Controls.Add(Me.olaDateFrom)
        Me.ogbDateInerval.Location = New System.Drawing.Point(13, 70)
        Me.ogbDateInerval.Name = "ogbDateInerval"
        Me.ogbDateInerval.Size = New System.Drawing.Size(211, 190)
        Me.ogbDateInerval.TabIndex = 6
        Me.ogbDateInerval.TabStop = False
        Me.ogbDateInerval.Tag = "2;      �ѹ���;      Date"
        Me.ogbDateInerval.Text = "      Date"
        '
        'odtDateTo
        '
        Me.odtDateTo.Enabled = False
        Me.odtDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDateTo.Location = New System.Drawing.Point(15, 96)
        Me.odtDateTo.Name = "odtDateTo"
        Me.odtDateTo.Size = New System.Drawing.Size(157, 22)
        Me.odtDateTo.TabIndex = 2
        '
        'odtDateFrom
        '
        Me.odtDateFrom.Enabled = False
        Me.odtDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDateFrom.Location = New System.Drawing.Point(15, 55)
        Me.odtDateFrom.Name = "odtDateFrom"
        Me.odtDateFrom.Size = New System.Drawing.Size(157, 22)
        Me.odtDateFrom.TabIndex = 2
        '
        'olaDateTo
        '
        Me.olaDateTo.AutoSize = True
        Me.olaDateTo.Location = New System.Drawing.Point(12, 80)
        Me.olaDateTo.Name = "olaDateTo"
        Me.olaDateTo.Size = New System.Drawing.Size(55, 16)
        Me.olaDateTo.TabIndex = 1
        Me.olaDateTo.Tag = "2;�֧�ѹ���;To date"
        Me.olaDateTo.Text = "To date"
        '
        'olaDateFrom
        '
        Me.olaDateFrom.AutoSize = True
        Me.olaDateFrom.Location = New System.Drawing.Point(12, 39)
        Me.olaDateFrom.Name = "olaDateFrom"
        Me.olaDateFrom.Size = New System.Drawing.Size(69, 16)
        Me.olaDateFrom.TabIndex = 1
        Me.olaDateFrom.Tag = "2;�ҡ�ѹ���;From date"
        Me.olaDateFrom.Text = "From date"
        '
        'wCondition
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmCancel
        Me.ClientSize = New System.Drawing.Size(501, 312)
        Me.Controls.Add(Me.XpGradientPanel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wCondition"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;���͹�;Condition"
        Me.Text = "Condition"
        Me.XpGradientPanel1.ResumeLayout(False)
        Me.XpGradientPanel1.PerformLayout()
        Me.ogbCondition.ResumeLayout(False)
        Me.ogbCondition.PerformLayout()
        Me.ogbCode.ResumeLayout(False)
        Me.ogbCode.PerformLayout()
        Me.ogbDateInerval.ResumeLayout(False)
        Me.ogbDateInerval.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XpGradientPanel1 As System.Windows.Forms.Panel
    Friend WithEvents odtDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents olaDateTo As System.Windows.Forms.Label
    Friend WithEvents odtDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents orbSelect As System.Windows.Forms.RadioButton
    Friend WithEvents orbDataInterval As System.Windows.Forms.RadioButton
    Friend WithEvents ocmCancel As System.Windows.Forms.Button
    Friend WithEvents ocmOk As System.Windows.Forms.Button
    Friend WithEvents ogbDateInerval As System.Windows.Forms.GroupBox
    Friend WithEvents olaDateFrom As System.Windows.Forms.Label
    Friend WithEvents ockDateInterval As System.Windows.Forms.CheckBox
    Friend WithEvents ogbCondition As System.Windows.Forms.GroupBox
    Friend WithEvents orbSellectAll As System.Windows.Forms.RadioButton
    Friend WithEvents orbCustom As System.Windows.Forms.RadioButton
    Friend WithEvents ocmSelect As System.Windows.Forms.Button
    Friend WithEvents ocmToCode As System.Windows.Forms.Button
    Friend WithEvents ocmFromCode As System.Windows.Forms.Button
    Friend WithEvents olaToCode As System.Windows.Forms.Label
    Friend WithEvents olaFromCode As System.Windows.Forms.Label
    Friend WithEvents otbSelect As System.Windows.Forms.TextBox
    Friend WithEvents otbTo As System.Windows.Forms.TextBox
    Friend WithEvents otbFrom As System.Windows.Forms.TextBox
    Friend WithEvents ogbCode As System.Windows.Forms.GroupBox
End Class
