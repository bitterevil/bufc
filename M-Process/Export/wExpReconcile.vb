﻿Imports C1.Win.C1FlexGrid
Imports System.IO
Imports System.ComponentModel

Public Class wExpReconcile

    Private bW_FrmLoad As Boolean = False
    Private tW_CaptionExport As String = "List Export,List Export,Condition"
    Private tW_CaptionExportThai As String = "รายการส่งออก,รายการส่งออก,เงื่อนไข"
    Public Shared oW_BackgroudWord As BackgroundWorker '*CH 19-12-2014
    Private dW_StartTime As DateTime
    Private dW_EndTime As DateTime
    Public Shared bW_StaProcess As Boolean = False

    Private Sub W_SETxCaptionGridExport()
        Dim tCapImport As String = ""
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then '1:thai
            tCapImport = tW_CaptionExport
        Else
            tCapImport = tW_CaptionExportThai
        End If

        Dim nCol As Integer = 2 'Start Column 2
        For Each tCap In tCapImport.Split(",")
            With Me.ogdFormExp06
                .Cols(nCol).Caption = tCap
            End With
            nCol += 1
        Next
    End Sub

    Private Sub W_SETxListExport()
        With Me.ogdFormExp06
            Dim oItemExport = cExportTemplate.C_GETaItemExport("", "")
            .DataSource = oItemExport
            .Row = 0 'Default Row 0
            Me.W_SETxCaptionGridExport()

            Dim dDateNow As Date = Date.Now
            'Add Default Condition
            For Each oItem In oItemExport
                oItem.nMode = 0 'Branch 0:All,1:Interval,2:Select
                oItem.tDateFrom = Format(dDateNow, "yyyy-MM-dd")
                oItem.tDateTo = Format(dDateNow, "yyyy-MM-dd")
                oItem.tConditon = cExportTemplate.C_GETtFmtCondition(Format(dDateNow, "dd/MM/yyyy"), Format(dDateNow, "dd/MM/yyyy"))
            Next

            'Set Seq No
            For nRow As Integer = 1 To .Rows.Count - 1
                .Item(nRow, 0) = nRow
            Next

            If AdaConfig.cConfig.oApplication.nLanguage = 1 Then '1:thai
                .Cols("tItem").Visible = False
                .Cols("tItemThai").Visible = True
            End If

        End With

    End Sub

#Region "Event Form"

    Private Sub wExports_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Me.Cursor = Cursors.WaitCursor Then
            e.Cancel = True
        End If
    End Sub

    Private Sub wExports_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cCNSP.SP_FrmSetCapControl(Me)

        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN102, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
            Exit Sub
        End If

        cExportTemplate.C_CALxClear()
        cCNVB.tVB_Export = cCNVB.tVB_ZSDINT006
        Me.W_SETxListExport() 'Load List Export
        Me.bW_FrmLoad = True
        Me.ogdFormExp06.Row = 1 'Fix Load รายการแรก
    End Sub

#End Region

#Region "Grid"

    Private Sub ogdForm_CellButtonClick(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdFormExp06.CellButtonClick
        With ogdFormExp06
            Select Case .Item(e.Row, .Cols("tCode").Index)
                Case cExportTemplate.cTableCode.ZSDINT006
                    Dim oItemExport = cExportTemplate.C_GETaItemExport("", "").Where(Function(c) c.tCode = cExportTemplate.cTableCode.ZSDINT006)
                    Dim oFrmCondi As New wCondDate(oItemExport.First)
                    oFrmCondi.ShowDialog()
                    .DataSource = oItemExport
            End Select
        End With

    End Sub

    Private Sub ogdForm_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdFormExp06.AfterEdit
        With Me.ogdFormExp06
            If .MouseRow > 0 And .MouseCol = 2 Then '.MouseCol = 2 = "เงื่อนไข;Condition"
                Me.ottForm.SetToolTip(Me.ogdFormExp06, .Item(.MouseRow, .MouseCol))
            End If
        End With
    End Sub
#End Region

#Region "Button"

    Private Sub ocmProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmProcess.Click
        Me.Cursor = Cursors.WaitCursor
        Me.ocmProcess.Enabled = False
        Me.ocmClose.Enabled = False
        Try
            '*CH 19-12-2014
            oW_BackgroudWord = New BackgroundWorker
            oW_BackgroudWord.WorkerReportsProgress = True
            oW_BackgroudWord.WorkerSupportsCancellation = True
            AddHandler oW_BackgroudWord.DoWork, AddressOf C_RUNxDoWork
            AddHandler oW_BackgroudWord.ProgressChanged, AddressOf C_RUNxWordChanged
            AddHandler oW_BackgroudWord.RunWorkerCompleted, AddressOf C_RUNxWorkerCompleted

            Me.opgForm.Maximum = 100
            Me.opgForm.Value = 0
            bW_StaProcess = False

            oW_BackgroudWord.RunWorkerAsync()

        Catch ex As Exception
        Finally
            'Me.ocmProcess.Enabled = True
            'Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub C_RUNxDoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        'Dim oWorker As BackgroundWorker = CType(sender, BackgroundWorker)
        dW_StartTime = Now
        cExportTemplate.oC_Progress = Me.opgForm
        cExportTemplate.C_CALxProcessAll()
    End Sub
    Private Sub C_RUNxWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim tLogAda As String = ""
        Me.opgForm.Value = 100

        dW_EndTime = Now
        Dim dDiffTime As TimeSpan = dW_EndTime - dW_StartTime

        tLogAda = "[Start : " & Format(dW_StartTime, "HH:mm:ss") & "], "
        tLogAda &= "[End : " & Format(dW_EndTime, "HH:mm:ss") & "], "
        tLogAda &= "[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]"
        cLog.C_CALxWriteLog("wExport > Process " & tLogAda)
        Me.ocmProcess.Enabled = True
        Me.ocmClose.Enabled = True
        Me.Cursor = Cursors.Default

        oW_BackgroudWord = Nothing
        'cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        If bW_StaProcess Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        Else
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN121, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        End If
    End Sub
    Private Sub C_RUNxWordChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        Me.opgForm.Value = e.ProgressPercentage
        Me.olaValStaExp.Text = cExportTemplate.tC_Status & " : " & e.ProgressPercentage & " %"
    End Sub
#End Region

    Private Sub opnForm_Paint(sender As Object, e As PaintEventArgs) Handles opnForm.Paint

    End Sub
End Class