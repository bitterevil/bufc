﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wIndex
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ocmProcess = New System.Windows.Forms.Button()
        Me.opgFrom = New System.Windows.Forms.ProgressBar()
        Me.olaStatus = New System.Windows.Forms.Label()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ocmProcess
        '
        Me.ocmProcess.Location = New System.Drawing.Point(262, 95)
        Me.ocmProcess.Name = "ocmProcess"
        Me.ocmProcess.Size = New System.Drawing.Size(100, 30)
        Me.ocmProcess.TabIndex = 0
        Me.ocmProcess.Tag = "2;ดำเนินการ;Process"
        Me.ocmProcess.Text = "Process"
        Me.ocmProcess.UseVisualStyleBackColor = True
        '
        'opgFrom
        '
        Me.opgFrom.Location = New System.Drawing.Point(12, 53)
        Me.opgFrom.Name = "opgFrom"
        Me.opgFrom.Size = New System.Drawing.Size(456, 23)
        Me.opgFrom.TabIndex = 1
        '
        'olaStatus
        '
        Me.olaStatus.AutoSize = True
        Me.olaStatus.Location = New System.Drawing.Point(12, 34)
        Me.olaStatus.Name = "olaStatus"
        Me.olaStatus.Size = New System.Drawing.Size(51, 16)
        Me.olaStatus.TabIndex = 2
        Me.olaStatus.Tag = "2;สถานะ :;Status :"
        Me.olaStatus.Text = "Status :"
        '
        'ocmCancel
        '
        Me.ocmCancel.Location = New System.Drawing.Point(368, 95)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(100, 30)
        Me.ocmCancel.TabIndex = 3
        Me.ocmCancel.Tag = "2;ยกเลิก;Cancel"
        Me.ocmCancel.Text = "Cancel"
        Me.ocmCancel.UseVisualStyleBackColor = True
        '
        'wIndex
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(480, 137)
        Me.Controls.Add(Me.ocmCancel)
        Me.Controls.Add(Me.olaStatus)
        Me.Controls.Add(Me.opgFrom)
        Me.Controls.Add(Me.ocmProcess)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wIndex"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;สร้างดัชนีข้อมูลใหม่;Rebuild Index"
        Me.Text = "Rebuild Index"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ocmProcess As System.Windows.Forms.Button
    Friend WithEvents opgFrom As System.Windows.Forms.ProgressBar
    Friend WithEvents olaStatus As System.Windows.Forms.Label
    Friend WithEvents ocmCancel As System.Windows.Forms.Button
End Class
