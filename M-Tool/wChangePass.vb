Option Explicit On
Imports System.IO
Public Class wChangePass

#Region "Variable"

#End Region

#Region "function"

#End Region

#Region "Event"

#Region "Button"

    Private Sub otb_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
       Handles otbCodeNew.KeyDown, otbPwdNew.KeyDown, otbPwdOld.KeyDown
        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{Tab}")
        End If
    End Sub

    Private Sub ocmCancel_Click(sender As System.Object, e As System.EventArgs) Handles ocmCancel.Click
        Me.Close()
    End Sub

    Private Sub otbPwdConfirm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbPwdConfirm.KeyDown
        If e.KeyCode = Keys.Enter Then
            ocmOk.PerformClick()
        End If
    End Sub

    Private Sub ocmOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmOk.Click
        If Me.otbCodeNew.Text.Trim = "" Or Me.otbPwdNew.Text.Trim = "" Or Me.otbPwdConfirm.Text.Trim = "" Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN010, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.otbPwdNew.Focus()
            Me.DialogResult = Windows.Forms.DialogResult.None
            Exit Sub
        End If

        If AdaConfig.cConfig.oApplication.tPass <> Me.otbPwdOld.Text.Trim Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN010, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.otbPwdOld.Focus()
            Me.otbPwdOld.SelectAll()
            Me.DialogResult = Windows.Forms.DialogResult.None
            Exit Sub
        End If

        If Me.otbPwdNew.Text.Trim <> Me.otbPwdConfirm.Text.Trim Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN010, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.otbPwdConfirm.Focus()
            Me.otbPwdConfirm.SelectAll()
            Me.DialogResult = Windows.Forms.DialogResult.None
            Exit Sub
        End If

        'Update User,Password
        AdaConfig.cConfig.oApplication.tUser = Me.otbCodeNew.Text.Trim
        AdaConfig.cConfig.oApplication.tPass = Me.otbPwdNew.Text.Trim
        AdaConfig.cConfig.C_SETbUpdateXml()

        cLog.C_CALxWriteLog("wChangePass > Save")
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
#End Region

#Region "Form"
    Private Sub wChangePass_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.otbCodeNew.Text = AdaConfig.cConfig.oApplication.tUser
        cCNSP.SP_FrmSetCapControl(Me)
    End Sub
#End Region

#Region "TextBox"

    Private Sub otbPwdOld_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles otbPwdOld.KeyPress, otbCodeNew.KeyPress
        e.Handled = cCNSP.SP_CNKBbChkKeyAscii(Asc(e.KeyChar))
    End Sub

#End Region

#End Region

End Class