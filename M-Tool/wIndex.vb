﻿Imports System.Data.SqlClient

Public Class wIndex

    Private Delegate Sub showProgressDelegate()
    Private bW_Start As Boolean = True
    Private bW_Stop As Boolean = False

    Private Function W_CALbExecSQL(pbUseSqlTransaction As Boolean, ParamArray aSql As String()) As Boolean
        Dim nLoop As Integer = 0
        Dim tErrCmd As String = ""

        W_CALbEXECSQL = False
        Using oConnSQL As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oConnSQL.Open()
            Select Case pbUseSqlTransaction
                Case True
                    Using oSQLCmd As New SqlCommand
                        Try
                            oSQLCmd.Connection = oConnSQL
                            oSQLCmd.Transaction = oConnSQL.BeginTransaction(IsolationLevel.ReadCommitted)
                            For Each tSql In aSql
                                tErrCmd = tSql
                                oSQLCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                                oSQLCmd.CommandText = tSql
                                oSQLCmd.ExecuteNonQuery()
                                nLoop += 1
                            Next
                            oSQLCmd.Transaction.Commit()
                            W_CALbExecSQL = True
                        Catch ex As Exception
                            'MsgBox(tErrCmd)
                            oSQLCmd.Transaction.Rollback()
                            Me.W_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, "", ex.Message.Replace("'", "*"), tErrCmd.Replace("'", "*")))
                        End Try
                    End Using

                Case Else
                    Try
                        Using oSQLCmd As New SqlCommand
                            Try
                                oSQLCmd.Connection = oConnSQL
                                For Each tSql In aSql
                                    tErrCmd = tSql
                                    oSQLCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                                    oSQLCmd.CommandText = tSql
                                    oSQLCmd.ExecuteNonQuery()
                                    nLoop += 1
                                Next
                                W_CALbExecSQL = True
                            Catch ex As Exception
                                'MsgBox(tErrCmd)
                                Me.W_CALbExecSQL(False, String.Format(cApp.tSQLCmdLog, 1, "", ex.Message.Replace("'", "*"), tErrCmd.Replace("'", "*")))
                            End Try
                        End Using
                        W_CALbExecSQL = True
                    Catch ex As Exception

                    End Try

            End Select
        End Using
    End Function

    Private Sub W_CALxShowProgress()
        If bW_Start = True Then
            If Me.opgFrom.Value = 100 Then
                Me.opgFrom.Value = 1
            End If
            Me.opgFrom.Value += 1
        Else
            Me.opgFrom.Value = 100
            Me.Cursor = Cursors.Default
            Me.ocmCancel.Enabled = True
        End If
        Me.Refresh()
    End Sub

    Sub w_CALxRun()
        Try
            Dim aSql As New List(Of String)
            Dim tSql As String = ""
            tSql = "SELECT 'ALTER INDEX ['+name+'] ON TCNMPdt REBUILD' FROM sys.indexes"
            tSql &= " WHERE object_id = (select object_id from sys.objects where name = 'TCNMPdt')"
            tSql &= " AND type=2"
            Using oSQLConn As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSQLConn.Open()
                Dim oSQLCmdWhe As New SqlCommand(tSql, oSQLConn)
                Dim oSQLRdWhe As SqlDataReader = oSQLCmdWhe.ExecuteReader()
                While oSQLRdWhe.Read()
                    aSql.Add(oSQLRdWhe(0))
                End While
            End Using
            W_CALbExecSQL(False, aSql.ToArray)
            bW_Stop = True
            bW_Start = False
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN109, cCNEN.eEN_MSGStyle.nEN_MSGInfo)
        Catch ex As Exception
            cCNSP.SP_MSGShowError(ex.Message)
        Finally
            bW_Stop = True
        End Try

        bW_Start = False
    End Sub

    Sub w_CALxProgess()
        Try
            While bW_Start
                Me.Invoke(New showProgressDelegate(AddressOf W_CALxShowProgress))
                Threading.Thread.Sleep(100)
            End While
            Me.Invoke(New showProgressDelegate(AddressOf W_CALxShowProgress))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub wIndex_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Me.Cursor <> Cursors.Default Then
            e.Cancel = True
        End If
    End Sub

    Private Sub wIndex_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If Me.Cursor = Cursors.Default Then
            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Close()
            End Select
        End If
    End Sub

    Private Sub ocmProcess_Click(sender As System.Object, e As System.EventArgs) Handles ocmProcess.Click
        Me.Cursor = Cursors.WaitCursor
        Me.ocmCancel.Enabled = False
        Me.ocmProcess.Enabled = False
        cLog.C_CALxWriteLog("wIndex > Process")
        Dim oThreadProgess As New Threading.Thread(AddressOf w_CALxProgess)
        oThreadProgess.Start()
        Dim oThreadRebuild As New Threading.Thread(AddressOf w_CALxRun)
        oThreadRebuild.Start()
    End Sub

    Private Sub ocmCancel_Click(sender As System.Object, e As System.EventArgs) Handles ocmCancel.Click
        Me.Close()
    End Sub

    Private Sub wIndex_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cCNSP.SP_FrmSetCapControl(Me)
    End Sub

End Class