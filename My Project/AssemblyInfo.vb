﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("AdaLinkBUFC")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Adasoft")>
<Assembly: AssemblyProduct("AdaLinkBUFC")>
<Assembly: AssemblyCopyright("Copyright © Adasoft  2018")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1E43A60C-DA2B-4317-9641-A7EAD0E070D2")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.6101.0.32")>
<Assembly: AssemblyFileVersion("1.6101.0.32")>
