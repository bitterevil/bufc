<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wBrowse
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wBrowse))
        Me.ogdFilter = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.ocmSelect = New System.Windows.Forms.Button()
        Me.ocmExit = New System.Windows.Forms.Button()
        Me.ogbFilter = New System.Windows.Forms.GroupBox()
        Me.olaFilter12 = New System.Windows.Forms.Label()
        Me.olaFilter11 = New System.Windows.Forms.Label()
        Me.olaFilter10 = New System.Windows.Forms.Label()
        Me.olaFilter09 = New System.Windows.Forms.Label()
        Me.olaFilter08 = New System.Windows.Forms.Label()
        Me.olaFilter07 = New System.Windows.Forms.Label()
        Me.olaFilter06 = New System.Windows.Forms.Label()
        Me.olaFilter05 = New System.Windows.Forms.Label()
        Me.olaFilter04 = New System.Windows.Forms.Label()
        Me.olaFilter03 = New System.Windows.Forms.Label()
        Me.olaFilter02 = New System.Windows.Forms.Label()
        Me.olaFilter01 = New System.Windows.Forms.Label()
        Me.olaFilter00 = New System.Windows.Forms.Label()
        Me.otbFilter12 = New System.Windows.Forms.TextBox()
        Me.otbFilter11 = New System.Windows.Forms.TextBox()
        Me.otbFilter10 = New System.Windows.Forms.TextBox()
        Me.otbFilter09 = New System.Windows.Forms.TextBox()
        Me.otbFilter08 = New System.Windows.Forms.TextBox()
        Me.otbFilter07 = New System.Windows.Forms.TextBox()
        Me.otbFilter06 = New System.Windows.Forms.TextBox()
        Me.otbFilter05 = New System.Windows.Forms.TextBox()
        Me.otbFilter04 = New System.Windows.Forms.TextBox()
        Me.otbFilter03 = New System.Windows.Forms.TextBox()
        Me.otbFilter02 = New System.Windows.Forms.TextBox()
        Me.otbFilter01 = New System.Windows.Forms.TextBox()
        Me.otbFilter00 = New System.Windows.Forms.TextBox()
        Me.ocmKeyboard = New System.Windows.Forms.Button()
        CType(Me.ogdFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ogbFilter.SuspendLayout()
        Me.SuspendLayout()
        '
        'ogdFilter
        '
        Me.ogdFilter.AllowEditing = False
        Me.ogdFilter.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D
        Me.ogdFilter.ColumnInfo = "10,1,0,0,0,95,Columns:0{Width:20;}" & Global.Microsoft.VisualBasic.ChrW(9)
        Me.ogdFilter.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ogdFilter.ExtendLastCol = True
        Me.ogdFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogdFilter.Location = New System.Drawing.Point(5, 4)
        Me.ogdFilter.Margin = New System.Windows.Forms.Padding(4)
        Me.ogdFilter.Name = "ogdFilter"
        Me.ogdFilter.Rows.Count = 6
        Me.ogdFilter.Rows.DefaultSize = 19
        Me.ogdFilter.Rows.MinSize = 26
        Me.ogdFilter.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.ogdFilter.Size = New System.Drawing.Size(545, 345)
        Me.ogdFilter.StyleInfo = resources.GetString("ogdFilter.StyleInfo")
        Me.ogdFilter.TabIndex = 0
        Me.ogdFilter.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'ocmSelect
        '
        Me.ocmSelect.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmSelect.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmSelect.Image = CType(resources.GetObject("ocmSelect.Image"), System.Drawing.Image)
        Me.ocmSelect.Location = New System.Drawing.Point(342, 445)
        Me.ocmSelect.Margin = New System.Windows.Forms.Padding(4)
        Me.ocmSelect.Name = "ocmSelect"
        Me.ocmSelect.Size = New System.Drawing.Size(100, 30)
        Me.ocmSelect.TabIndex = 2
        Me.ocmSelect.Text = "&OK"
        Me.ocmSelect.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ocmSelect.UseVisualStyleBackColor = True
        '
        'ocmExit
        '
        Me.ocmExit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmExit.Image = CType(resources.GetObject("ocmExit.Image"), System.Drawing.Image)
        Me.ocmExit.Location = New System.Drawing.Point(450, 445)
        Me.ocmExit.Margin = New System.Windows.Forms.Padding(4)
        Me.ocmExit.Name = "ocmExit"
        Me.ocmExit.Size = New System.Drawing.Size(100, 30)
        Me.ocmExit.TabIndex = 3
        Me.ocmExit.Text = "&Exit"
        Me.ocmExit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ocmExit.UseVisualStyleBackColor = True
        '
        'ogbFilter
        '
        Me.ogbFilter.BackColor = System.Drawing.Color.Transparent
        Me.ogbFilter.Controls.Add(Me.olaFilter12)
        Me.ogbFilter.Controls.Add(Me.olaFilter11)
        Me.ogbFilter.Controls.Add(Me.olaFilter10)
        Me.ogbFilter.Controls.Add(Me.olaFilter09)
        Me.ogbFilter.Controls.Add(Me.olaFilter08)
        Me.ogbFilter.Controls.Add(Me.olaFilter07)
        Me.ogbFilter.Controls.Add(Me.olaFilter06)
        Me.ogbFilter.Controls.Add(Me.olaFilter05)
        Me.ogbFilter.Controls.Add(Me.olaFilter04)
        Me.ogbFilter.Controls.Add(Me.olaFilter03)
        Me.ogbFilter.Controls.Add(Me.olaFilter02)
        Me.ogbFilter.Controls.Add(Me.olaFilter01)
        Me.ogbFilter.Controls.Add(Me.olaFilter00)
        Me.ogbFilter.Controls.Add(Me.otbFilter12)
        Me.ogbFilter.Controls.Add(Me.otbFilter11)
        Me.ogbFilter.Controls.Add(Me.otbFilter10)
        Me.ogbFilter.Controls.Add(Me.otbFilter09)
        Me.ogbFilter.Controls.Add(Me.otbFilter08)
        Me.ogbFilter.Controls.Add(Me.otbFilter07)
        Me.ogbFilter.Controls.Add(Me.otbFilter06)
        Me.ogbFilter.Controls.Add(Me.otbFilter05)
        Me.ogbFilter.Controls.Add(Me.otbFilter04)
        Me.ogbFilter.Controls.Add(Me.otbFilter03)
        Me.ogbFilter.Controls.Add(Me.otbFilter02)
        Me.ogbFilter.Controls.Add(Me.otbFilter01)
        Me.ogbFilter.Controls.Add(Me.otbFilter00)
        Me.ogbFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ogbFilter.Location = New System.Drawing.Point(5, 357)
        Me.ogbFilter.Margin = New System.Windows.Forms.Padding(4)
        Me.ogbFilter.Name = "ogbFilter"
        Me.ogbFilter.Padding = New System.Windows.Forms.Padding(4)
        Me.ogbFilter.Size = New System.Drawing.Size(545, 80)
        Me.ogbFilter.TabIndex = 1
        Me.ogbFilter.TabStop = False
        Me.ogbFilter.Text = "Filter ..."
        '
        'olaFilter12
        '
        Me.olaFilter12.Location = New System.Drawing.Point(465, 19)
        Me.olaFilter12.Name = "olaFilter12"
        Me.olaFilter12.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter12.TabIndex = 25
        Me.olaFilter12.Text = "Filter12"
        Me.olaFilter12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter12.Visible = False
        '
        'olaFilter11
        '
        Me.olaFilter11.Location = New System.Drawing.Point(430, 19)
        Me.olaFilter11.Name = "olaFilter11"
        Me.olaFilter11.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter11.TabIndex = 24
        Me.olaFilter11.Text = "Filter11"
        Me.olaFilter11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter11.Visible = False
        '
        'olaFilter10
        '
        Me.olaFilter10.Location = New System.Drawing.Point(399, 19)
        Me.olaFilter10.Name = "olaFilter10"
        Me.olaFilter10.Size = New System.Drawing.Size(29, 19)
        Me.olaFilter10.TabIndex = 23
        Me.olaFilter10.Text = "Filter10"
        Me.olaFilter10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter10.Visible = False
        '
        'olaFilter09
        '
        Me.olaFilter09.Location = New System.Drawing.Point(365, 19)
        Me.olaFilter09.Name = "olaFilter09"
        Me.olaFilter09.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter09.TabIndex = 22
        Me.olaFilter09.Text = "Filter09"
        Me.olaFilter09.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter09.Visible = False
        '
        'olaFilter08
        '
        Me.olaFilter08.Location = New System.Drawing.Point(330, 19)
        Me.olaFilter08.Name = "olaFilter08"
        Me.olaFilter08.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter08.TabIndex = 21
        Me.olaFilter08.Text = "Filter08"
        Me.olaFilter08.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter08.Visible = False
        '
        'olaFilter07
        '
        Me.olaFilter07.Location = New System.Drawing.Point(296, 19)
        Me.olaFilter07.Name = "olaFilter07"
        Me.olaFilter07.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter07.TabIndex = 20
        Me.olaFilter07.Text = "Filter07"
        Me.olaFilter07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter07.Visible = False
        '
        'olaFilter06
        '
        Me.olaFilter06.Location = New System.Drawing.Point(261, 19)
        Me.olaFilter06.Name = "olaFilter06"
        Me.olaFilter06.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter06.TabIndex = 19
        Me.olaFilter06.Text = "Filter06"
        Me.olaFilter06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter06.Visible = False
        '
        'olaFilter05
        '
        Me.olaFilter05.Location = New System.Drawing.Point(228, 19)
        Me.olaFilter05.Name = "olaFilter05"
        Me.olaFilter05.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter05.TabIndex = 18
        Me.olaFilter05.Text = "Filter05"
        Me.olaFilter05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter05.Visible = False
        '
        'olaFilter04
        '
        Me.olaFilter04.Location = New System.Drawing.Point(193, 19)
        Me.olaFilter04.Name = "olaFilter04"
        Me.olaFilter04.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter04.TabIndex = 17
        Me.olaFilter04.Text = "Filter04"
        Me.olaFilter04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter04.Visible = False
        '
        'olaFilter03
        '
        Me.olaFilter03.Location = New System.Drawing.Point(158, 19)
        Me.olaFilter03.Name = "olaFilter03"
        Me.olaFilter03.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter03.TabIndex = 16
        Me.olaFilter03.Text = "Filter03"
        Me.olaFilter03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter03.Visible = False
        '
        'olaFilter02
        '
        Me.olaFilter02.Location = New System.Drawing.Point(124, 19)
        Me.olaFilter02.Name = "olaFilter02"
        Me.olaFilter02.Size = New System.Drawing.Size(28, 19)
        Me.olaFilter02.TabIndex = 15
        Me.olaFilter02.Text = "Filter02"
        Me.olaFilter02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter02.Visible = False
        '
        'olaFilter01
        '
        Me.olaFilter01.Location = New System.Drawing.Point(81, 19)
        Me.olaFilter01.Name = "olaFilter01"
        Me.olaFilter01.Size = New System.Drawing.Size(38, 19)
        Me.olaFilter01.TabIndex = 14
        Me.olaFilter01.Text = "Filter01"
        Me.olaFilter01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter01.Visible = False
        '
        'olaFilter00
        '
        Me.olaFilter00.Location = New System.Drawing.Point(9, 19)
        Me.olaFilter00.Name = "olaFilter00"
        Me.olaFilter00.Size = New System.Drawing.Size(66, 19)
        Me.olaFilter00.TabIndex = 0
        Me.olaFilter00.Text = "Filter00"
        Me.olaFilter00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.olaFilter00.Visible = False
        '
        'otbFilter12
        '
        Me.otbFilter12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter12.Location = New System.Drawing.Point(468, 42)
        Me.otbFilter12.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter12.Name = "otbFilter12"
        Me.otbFilter12.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter12.TabIndex = 13
        Me.otbFilter12.Visible = False
        '
        'otbFilter11
        '
        Me.otbFilter11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter11.Location = New System.Drawing.Point(433, 42)
        Me.otbFilter11.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter11.Name = "otbFilter11"
        Me.otbFilter11.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter11.TabIndex = 12
        Me.otbFilter11.Visible = False
        '
        'otbFilter10
        '
        Me.otbFilter10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter10.Location = New System.Drawing.Point(403, 42)
        Me.otbFilter10.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter10.Name = "otbFilter10"
        Me.otbFilter10.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter10.TabIndex = 11
        Me.otbFilter10.Visible = False
        '
        'otbFilter09
        '
        Me.otbFilter09.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter09.Location = New System.Drawing.Point(368, 42)
        Me.otbFilter09.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter09.Name = "otbFilter09"
        Me.otbFilter09.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter09.TabIndex = 10
        Me.otbFilter09.Visible = False
        '
        'otbFilter08
        '
        Me.otbFilter08.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter08.Location = New System.Drawing.Point(333, 42)
        Me.otbFilter08.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter08.Name = "otbFilter08"
        Me.otbFilter08.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter08.TabIndex = 9
        Me.otbFilter08.Visible = False
        '
        'otbFilter07
        '
        Me.otbFilter07.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter07.Location = New System.Drawing.Point(299, 42)
        Me.otbFilter07.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter07.Name = "otbFilter07"
        Me.otbFilter07.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter07.TabIndex = 8
        Me.otbFilter07.Visible = False
        '
        'otbFilter06
        '
        Me.otbFilter06.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter06.Location = New System.Drawing.Point(264, 42)
        Me.otbFilter06.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter06.Name = "otbFilter06"
        Me.otbFilter06.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter06.TabIndex = 7
        Me.otbFilter06.Visible = False
        '
        'otbFilter05
        '
        Me.otbFilter05.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter05.Location = New System.Drawing.Point(231, 42)
        Me.otbFilter05.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter05.Name = "otbFilter05"
        Me.otbFilter05.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter05.TabIndex = 6
        Me.otbFilter05.Visible = False
        '
        'otbFilter04
        '
        Me.otbFilter04.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter04.Location = New System.Drawing.Point(196, 42)
        Me.otbFilter04.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter04.Name = "otbFilter04"
        Me.otbFilter04.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter04.TabIndex = 5
        Me.otbFilter04.Visible = False
        '
        'otbFilter03
        '
        Me.otbFilter03.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter03.Location = New System.Drawing.Point(161, 42)
        Me.otbFilter03.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter03.Name = "otbFilter03"
        Me.otbFilter03.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter03.TabIndex = 4
        Me.otbFilter03.Visible = False
        '
        'otbFilter02
        '
        Me.otbFilter02.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter02.Location = New System.Drawing.Point(127, 42)
        Me.otbFilter02.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter02.Name = "otbFilter02"
        Me.otbFilter02.Size = New System.Drawing.Size(25, 26)
        Me.otbFilter02.TabIndex = 3
        Me.otbFilter02.Visible = False
        '
        'otbFilter01
        '
        Me.otbFilter01.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter01.Location = New System.Drawing.Point(82, 42)
        Me.otbFilter01.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter01.Name = "otbFilter01"
        Me.otbFilter01.Size = New System.Drawing.Size(37, 26)
        Me.otbFilter01.TabIndex = 2
        Me.otbFilter01.Visible = False
        '
        'otbFilter00
        '
        Me.otbFilter00.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbFilter00.Location = New System.Drawing.Point(9, 42)
        Me.otbFilter00.Margin = New System.Windows.Forms.Padding(4)
        Me.otbFilter00.Name = "otbFilter00"
        Me.otbFilter00.Size = New System.Drawing.Size(65, 26)
        Me.otbFilter00.TabIndex = 1
        Me.otbFilter00.Visible = False
        '
        'ocmKeyboard
        '
        Me.ocmKeyboard.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ocmKeyboard.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmKeyboard.Image = CType(resources.GetObject("ocmKeyboard.Image"), System.Drawing.Image)
        Me.ocmKeyboard.Location = New System.Drawing.Point(5, 444)
        Me.ocmKeyboard.Name = "ocmKeyboard"
        Me.ocmKeyboard.Size = New System.Drawing.Size(73, 30)
        Me.ocmKeyboard.TabIndex = 4
        Me.ocmKeyboard.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ocmKeyboard.UseVisualStyleBackColor = True
        '
        'wBrowse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.ocmExit
        Me.ClientSize = New System.Drawing.Size(557, 478)
        Me.Controls.Add(Me.ocmExit)
        Me.Controls.Add(Me.ocmSelect)
        Me.Controls.Add(Me.ocmKeyboard)
        Me.Controls.Add(Me.ogbFilter)
        Me.Controls.Add(Me.ogdFilter)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wBrowse"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "wBrowser"
        CType(Me.ogdFilter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ogbFilter.ResumeLayout(False)
        Me.ogbFilter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ogdFilter As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents ocmSelect As System.Windows.Forms.Button
    Friend WithEvents ocmExit As System.Windows.Forms.Button
    Friend WithEvents ogbFilter As System.Windows.Forms.GroupBox
    Friend WithEvents otbFilter02 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter01 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter00 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter12 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter11 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter10 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter09 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter08 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter07 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter06 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter05 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter04 As System.Windows.Forms.TextBox
    Friend WithEvents otbFilter03 As System.Windows.Forms.TextBox
    Friend WithEvents ocmKeyboard As System.Windows.Forms.Button
    Friend WithEvents olaFilter00 As System.Windows.Forms.Label
    Friend WithEvents olaFilter01 As System.Windows.Forms.Label
    Friend WithEvents olaFilter12 As System.Windows.Forms.Label
    Friend WithEvents olaFilter11 As System.Windows.Forms.Label
    Friend WithEvents olaFilter10 As System.Windows.Forms.Label
    Friend WithEvents olaFilter09 As System.Windows.Forms.Label
    Friend WithEvents olaFilter08 As System.Windows.Forms.Label
    Friend WithEvents olaFilter07 As System.Windows.Forms.Label
    Friend WithEvents olaFilter06 As System.Windows.Forms.Label
    Friend WithEvents olaFilter05 As System.Windows.Forms.Label
    Friend WithEvents olaFilter04 As System.Windows.Forms.Label
    Friend WithEvents olaFilter03 As System.Windows.Forms.Label
    Friend WithEvents olaFilter02 As System.Windows.Forms.Label
End Class
